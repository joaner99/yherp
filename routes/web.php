<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['auth', 'badges']], function () {
    //首頁
    Route::get('/', ['as' => 'Home', 'uses' => 'HomeController@index']);
    Route::get('/home', ['as' => 'Home', 'uses' => 'HomeController@index']);
    //主倉儲位
    Route::group(['prefix' => 'stock', 'as' => 'stock.'], function () {
        Route::get('index', ['as' => 'index', 'uses' => 'StockController@index']);
        Route::get('log', ['as' => 'log', 'uses' => 'StockController@log']);
        Route::get('create/{s_id}/{p_id}', ['as' => 'create', 'uses' => 'StockController@create']);
        Route::post('store/{s_id}/{p_id}', ['as' => 'store', 'uses' => 'StockController@store']);
    });
    //優居
    Route::group(['middleware' => ['role:YoHouse']], function () {
        Route::resource('roles', 'RoleController');
        Route::get('users/ajax_detail', ['as' => 'user.ajax_detail', 'uses' => 'UserController@ajax_detail']);
        Route::resource('users', 'UserController');
        Route::resource('sys_config', 'SysConfigController');
        //客製化訂單
        Route::get('order_customized/export/{id}', ['as' => 'order_customized.export', 'uses' => 'OrderCustomizedController@export']);
        Route::get('order_customized/create_by_cc/{id}', ['as' => 'order_customized.create_by_cc', 'uses' => 'OrderCustomizedController@create_by_cc']);
        Route::get('order_customized/create_by_quotation/{id}', ['as' => 'order_customized.create_by_quotation', 'uses' => 'OrderCustomizedController@create_by_quotation']);
        Route::post('order_customized/complete/{id}', ['as' => 'order_customized.complete', 'uses' => 'OrderCustomizedController@complete']);
        Route::resource('order_customized', 'OrderCustomizedController');
        Route::get('PoseinCheckDataLog/{type}', ['as' => 'PoseinCheckDataLog', 'uses' => 'PoseinCheckDataLogController@index']);

        Route::get('check_log_detail/{id}', ['as' => 'check_log_detail', 'uses' => 'PoseinCheckDataLogController@detail']);

        Route::get('order_detail/{id}', ['as' => 'order_detail', 'uses' => 'PoseinCheckDataLogController@order_detail']);

        Route::get('basket_detail', ['as' => 'basket_detail', 'uses' => 'PoseinCheckDataLogController@basket_detail']);

        Route::get('stockAll', ['as' => 'stockAll', 'uses' => 'PoseinCheckDataLogController@stockAll']);

        Route::get('itemp/{stock?}', ['as' => 'itemp', 'uses' => 'PoseinCheckDataLogController@itemp']);

        Route::get('order/{table}', ['as' => 'order', 'uses' => 'PoseinCheckDataLogController@order']);

        Route::get('orderWeight', ['as' => 'orderWeight', 'uses' => 'PoseinCheckDataLogController@orderWeight']);

        Route::get('order_tax_id', ['as' => 'order_tax_id', 'uses' => 'PoseinCheckDataLogController@order_tax_id']);

        Route::get('order_remark', ['as' => 'order_remark', 'uses' => 'PoseinCheckDataLogController@order_remark']);

        Route::get('order_all', ['as' => 'order_all', 'uses' => 'PoseinCheckDataLogController@order_all']);

        Route::get('check_search', ['as' => 'check_search', 'uses' => 'PoseinCheckDataLogController@check_search']);

        Route::get('check_shipment', ['as' => 'check_shipment', 'uses' => 'PoseinCheckDataLogController@check_shipment']);

        Route::get('overweight', ['as' => 'overweight', 'uses' => 'PoseinCheckDataLogController@overweight']);

        Route::get('sales_list', ['as' => 'sales_list', 'uses' => 'PoseinCheckDataLogController@sales_list']);

        Route::get('sales_unchecked', ['as' => 'sales_unchecked', 'uses' => 'PoseinCheckDataLogController@sales_unchecked']);

        Route::get('pre_order', ['as' => 'pre_order', 'uses' => 'PreOrderController@index']);

        //折讓清單
        Route::group(['prefix' => 'credit_note', 'as' => 'credit_note.'], function () {
            Route::get('index', ['as' => 'index', 'uses' => 'CreditNoteController@index']);
            Route::post('import', ['as' => 'import', 'uses' => 'CreditNoteController@import']);
            Route::post('create_return', ['as' => 'create_return', 'uses' => 'CreditNoteController@create_return']);
            Route::post('confirm', ['as' => 'confirm', 'uses' => 'CreditNoteController@confirm']);
        });

        //未完成訂單總表
        Route::group(['prefix' => 'sales_lost_check', 'as' => 'sales_lost_check.'], function () {
            Route::get('/', ['as' => 'index', 'uses' => 'SalesLostCheckController@index']);
            Route::post('ajax_update', ['as' => 'ajax_update', 'uses' => 'SalesLostCheckController@ajax_update']);
            Route::post('ajax_get_remark', ['as' => 'ajax_get_remark', 'uses' => 'SalesLostCheckController@ajax_get_remark']);
            Route::post('complete', ['as' => 'complete', 'uses' => 'SalesLostCheckController@complete']);
        });

        Route::get('sales_comparison', ['as' => 'sales_comparison', 'uses' => 'PoseinCheckDataLogController@sales_comparison']);

        Route::get('no_print', ['as' => 'no_print', 'uses' => 'PoseinCheckDataLogController@no_print']);

        Route::get('stock_unusual', ['as' => 'stock_unusual', 'uses' => 'PoseinCheckDataLogController@stock_unusual']);

        Route::get('stock_can_sale', ['as' => 'stock_can_sale', 'uses' => 'PoseinCheckDataLogController@stock_can_sale']);

        Route::get('stock_country', ['as' => 'stock_country', 'uses' => 'PoseinCheckDataLogController@stock_country']);

        Route::get('floor_count', ['as' => 'floor_count', 'uses' => 'PoseinCheckDataLogController@floor_count']);

        Route::get('order_plan', ['as' => 'order_plan', 'uses' => 'PoseinCheckDataLogController@order_plan']);

        //排程
        Route::get('schedule_log', ['as' => 'schedule_log', 'uses' => 'ScheduleController@schedule_log']);

        //匯出
        Route::post('export/table', ['as' => 'export_table', 'uses' => 'ExportController@table']);
        //群撿查詢
        Route::get('group_picking', ['as' => 'group_picking', 'uses' => 'PoseinCheckDataLogController@group_picking']);

        //客訴單
        Route::group(['prefix' => 'customer_complaint', 'as' => 'customer_complaint.'], function () {
            Route::get('/', ['as' => 'index', 'uses' => 'CustomerComplaintController@index']);
            Route::post('create', ['as' => 'create', 'uses' => 'CustomerComplaintController@create']);
            Route::post('update', ['as' => 'update', 'uses' => 'CustomerComplaintController@update']);
            Route::post('update_status', ['as' => 'update_status', 'uses' => 'CustomerComplaintController@update_status']);
            Route::get('ajax_get_order', ['as' => 'ajax_get_order', 'uses' => 'CustomerComplaintController@ajax_get_order']);
        });
        //安庫表
        Route::group(['prefix' => 'safety_stock', 'as' => 'safety_stock.'], function () {
            Route::get('/', ['as' => 'index', 'uses' => 'SafetyStockController@index']);
            Route::post('update', ['as' => 'update', 'uses' => 'SafetyStockController@update']);
            Route::post('check', ['as' => 'check', 'uses' => 'SafetyStockController@check']);
        });
        //特殊訂單
        Route::group(['prefix' => 'order_sample', 'as' => 'order_sample.'], function () {
            Route::get('convert', ['as' => 'convert', 'uses' => 'OrderSampleController@convert']);
            Route::get('download_page', ['as' => 'download_page', 'uses' => 'OrderSampleController@download_page']);
            Route::post('confirm', ['as' => 'confirm', 'uses' => 'OrderSampleController@confirm']);
            Route::post('cancel_confirm', ['as' => 'cancel_confirm', 'uses' => 'OrderSampleController@cancel_confirm']);
            Route::post('redo', ['as' => 'redo', 'uses' => 'OrderSampleController@redo']);
            Route::post('update', ['as' => 'update', 'uses' => 'OrderSampleController@update']);
            Route::post('delete', ['as' => 'delete', 'uses' => 'OrderSampleController@delete']);
            Route::post('import', ['as' => 'import', 'uses' => 'OrderSampleController@import']);
            Route::post('order_export', ['as' => 'order_export', 'uses' => 'OrderSampleController@order_export']);
            Route::get('shopee_trans', ['as' => 'shopee_trans', 'uses' => 'OrderSampleController@shopee_trans']);
            Route::post('shopee_trans_convert', ['as' => 'shopee_trans_convert', 'uses' => 'OrderSampleController@shopee_trans_convert']);
            Route::post('shopee_trans_export', ['as' => 'shopee_trans_export', 'uses' => 'OrderSampleController@shopee_trans_export']);
            Route::post('ajax_update_order', ['as' => 'ajax_update_order', 'uses' => 'OrderSampleController@ajax_update_order']);
        });
        //物流
        Route::group(['prefix' => 'transport', 'as' => 'transport.'], function () {
            Route::get('/', ['as' => 'index', 'uses' => 'TransportController@index']);
            Route::get('index_edit', ['as' => 'index_edit', 'uses' => 'TransportController@index_edit']);
            Route::post('index_batch_update', ['as' => 'index_batch_update', 'uses' => 'TransportController@index_batch_update']);
            Route::get('suggest', ['as' => 'suggest', 'uses' => 'TransportController@suggest']);
            Route::post('import_api', ['as' => 'import_api', 'uses' => 'TransportController@import_api']);
            Route::post('ship', ['as' => 'ship', 'uses' => 'TransportController@ship']);
            Route::post('export', ['as' => 'export', 'uses' => 'TransportController@export']);
            Route::patch('index_update', ['as' => 'index_update', 'uses' => 'TransportController@index_update']);
            Route::patch('index_batch_confirm', ['as' => 'index_batch_confirm', 'uses' => 'TransportController@index_batch_confirm']);
            Route::patch('index_batch_status_confirm', ['as' => 'index_batch_status_confirm', 'uses' => 'TransportController@index_batch_status_confirm']);
            Route::patch('index_check', ['as' => 'index_check', 'uses' => 'TransportController@index_check']);
            Route::patch('index_note_check', ['as' => 'index_note_check', 'uses' => 'TransportController@index_note_check']);
            Route::get('ajax_get_order', ['as' => 'ajax_get_order', 'uses' => 'TransportController@ajax_get_order']);
            Route::get('ajax_get_unshipped', ['as' => 'ajax_get_unshipped', 'uses' => 'TransportController@ajax_get_unshipped']);
            Route::post('ajax_set_personally_failed', ['as' => 'ajax_set_personally_failed', 'uses' => 'TransportController@ajax_set_personally_failed']);
            Route::get('freight_index', ['as' => 'freight_index', 'uses' => 'TransportController@freight_index']);
            Route::post('import_freight', ['as' => 'import_freight', 'uses' => 'TransportController@import_freight']);
        });
        //新竹物流
        Route::group(['prefix' => 'hct_transport', 'as' => 'hct_transport.'], function () {
            Route::get('remote_index', ['as' => 'remote_index', 'uses' => 'HctTransportController@remote_index']);
        });
        //音速物流
        Route::post('ycs_transport/import', ['as' => 'ycs_transport.import', 'uses' => 'YcsTransportController@import']);
        Route::get('ycs_transport/export_tms', ['as' => 'ycs_transport.export_tms', 'uses' => 'YcsTransportController@export_tms']);
        Route::post('ycs_transport/api', ['as' => 'ycs_transport.api', 'uses' => 'YcsTransportController@api']);
        Route::post('ycs_transport/batch_delete', ['as' => 'ycs_transport.batch_delete', 'uses' => 'YcsTransportController@batch_delete']);
        Route::resource('ycs_transport', 'YcsTransportController');
        //親送總表
        Route::group(['prefix' => 'personal_transport', 'as' => 'personal_transport.'], function () {
            Route::get('index', ['as' => 'index', 'uses' => 'PersonalTransportController@index']);
            Route::post('update_trans', ['as' => 'update_trans', 'uses' => 'PersonalTransportController@update_trans']);
            Route::post('export_shipment', ['as' => 'export_shipment', 'uses' => 'PersonalTransportController@export_shipment']);
            Route::post('ajax_update', ['as' => 'ajax_update', 'uses' => 'PersonalTransportController@ajax_update']);
            Route::post('ajax_update_sche', ['as' => 'ajax_update_sche', 'uses' => 'PersonalTransportController@ajax_update_sche']);
            Route::get('ship_day', ['as' => 'ship_day', 'uses' => 'PersonalTransportController@ship_day']);
            Route::get('overview', ['as' => 'overview', 'uses' => 'PersonalTransportController@overview']);
        });
        //人工總表
        Route::group(['prefix' => 'other_transport', 'as' => 'other_transport.'], function () {
            Route::get('index', ['as' => 'index', 'uses' => 'OtherTransportController@index']);
            Route::post('ajax_update', ['as' => 'ajax_update', 'uses' => 'OtherTransportController@ajax_update']);
        });
        //商品
        Route::group(['prefix' => 'items', 'as' => 'items.'], function () {
            Route::get('summary', ['as' => 'summary', 'uses' => 'ItemsController@summary']);
            Route::get('sales', ['as' => 'sales', 'uses' => 'ItemsController@sales']);
            Route::get('log', ['as' => 'log', 'uses' => 'ItemsController@log']);
            Route::get('detail', ['as' => 'detail', 'uses' => 'ItemsController@detail']);
            Route::get('stock_history', ['as' => 'stock_history', 'uses' => 'ItemsController@stock_history']);
            Route::get('config', ['as' => 'config', 'uses' => 'ItemsController@config']);
            Route::get('config_sync', ['as' => 'config_sync', 'uses' => 'ItemsController@config_sync']);
            Route::get('image_management', ['as' => 'image_management', 'uses' => 'ItemsController@image_management']);
            Route::get('place', ['as' => 'place', 'uses' => 'ItemsController@place']);
            Route::post('config_update', ['as' => 'config_update', 'uses' => 'ItemsController@config_update']);
            Route::post('image_update', ['as' => 'image_update', 'uses' => 'ItemsController@image_update']);
            Route::post('ajax_upload_image', ['as' => 'ajax_upload_image', 'uses' => 'ItemsController@ajax_upload_image']);
            Route::post('update_config', ['as' => 'update_config', 'uses' => 'ItemsController@update_config']);
            Route::post('import', ['as' => 'import', 'uses' => 'ItemsController@import']);
            Route::group(['prefix' => 'convert', 'as' => 'convert.'], function () {
                Route::get('index', ['as' => 'index', 'uses' => 'items\convertController@index']);
                Route::get('create', ['as' => 'create', 'uses' => 'items\convertController@create']);
                Route::post('store', ['as' => 'store', 'uses' => 'items\convertController@store']);
                Route::get('edit/{id}', ['as' => 'edit', 'uses' => 'items\convertController@edit']);
                Route::patch('update/{id}', ['as' => 'update', 'uses' => 'items\convertController@update']);
                Route::delete('destroy/{id}', ['as' => 'destroy', 'uses' => 'items\convertController@destroy']);
                Route::get('list', ['as' => 'list', 'uses' => 'items\convertController@list']);
            });
        });
        //廣告報表
        Route::group(['prefix' => 'ad', 'as' => 'ad.'], function () {
            Route::get('cost', ['as' => 'cost', 'uses' => 'AdController@cost']);
            Route::get('profit', ['as' => 'profit', 'uses' => 'AdController@profit']);
            Route::get('config', ['as' => 'config', 'uses' => 'AdController@config']);
            Route::get('config_cost', ['as' => 'config_cost', 'uses' => 'AdController@config_cost']);
            Route::get('config_cost/create', ['as' => 'config_cost_create', 'uses' => 'AdController@config_cost_create']);
            Route::post('config_cost_store', ['as' => 'config_cost_store', 'uses' => 'AdController@config_cost_store']);
            Route::post('config_cost_store_freight', ['as' => 'config_cost_store_freight', 'uses' => 'AdController@config_cost_store_freight']);
            Route::get('config_cost/{id}', ['as' => 'config_cost_edit', 'uses' => 'AdController@config_cost_edit']);
            Route::patch('config_cost/{id}', ['as' => 'config_cost_update', 'uses' => 'AdController@config_cost_update']);
            Route::delete('config_cost_destroy/{id}', ['as' => 'config_cost_destroy', 'uses' => 'AdController@config_cost_destroy']);
            Route::get('simulation', ['as' => 'simulation', 'uses' => 'AdController@simulation']);
            Route::post('import_profit', ['as' => 'import_profit', 'uses' => 'AdController@import_profit']);
            Route::post('import_item', ['as' => 'import_item', 'uses' => 'AdController@import_item']);
            Route::get('ajax_get_ad_sales', ['as' => 'ajax_get_ad_sales', 'uses' => 'AdController@ajax_get_ad_sales']);
        });
        //銷貨報表
        Route::group(['prefix' => 'sales', 'as' => 'sales.'], function () {
            Route::get('report', ['as' => 'report', 'uses' => 'SalesController@report']);
            Route::get('performance', ['as' => 'performance', 'uses' => 'SalesController@performance']);
            Route::get('cust_item', ['as' => 'cust_item', 'uses' => 'SalesController@cust_item']);
            Route::get('item_kind_sales', ['as' => 'item_kind_sales', 'uses' => 'SalesController@item_kind_sales']);
            Route::get('item_kind3_sales', ['as' => 'item_kind3_sales', 'uses' => 'SalesController@item_kind3_sales']);
            Route::get('item_sales', ['as' => 'item_sales', 'uses' => 'SalesController@item_sales']);
            Route::get('floor', ['as' => 'floor', 'uses' => 'SalesController@floor']);
            Route::get('shopee_trans', ['as' => 'shopee_trans', 'uses' => 'SalesController@shopee_trans']);
            Route::get('shopee_discount', ['as' => 'shopee_discount', 'uses' => 'SalesController@shopee_discount']);
            Route::post('import', ['as' => 'import', 'uses' => 'SalesController@import']);
        });
        //保固網站管理
        Route::group(['prefix' => 'warranty_web', 'as' => 'warranty_web.'], function () {
            Route::get('search', ['as' => 'search', 'uses' => 'WarrantyWebController@search']);
            Route::get('video_config', ['as' => 'video_config', 'uses' => 'WarrantyWebController@video_config']);
            Route::get('video_config/create', ['as' => 'video_config_create', 'uses' => 'WarrantyWebController@video_config_create']);
            Route::post('video_config', ['as' => 'video_config_store', 'uses' => 'WarrantyWebController@video_config_store']);
            Route::get('video_config/{id}', ['as' => 'video_config_edit', 'uses' => 'WarrantyWebController@video_config_edit']);
            Route::patch('video_config/{id}', ['as' => 'video_config_update', 'uses' => 'WarrantyWebController@video_config_update']);
            Route::delete('video_config/{id}', ['as' => 'video_config_destroy', 'uses' => 'WarrantyWebController@video_config_destroy']);
            Route::get('item_config', ['as' => 'item_config', 'uses' => 'WarrantyWebController@item_config']);
            Route::post('item_import', ['as' => 'item_import', 'uses' => 'WarrantyWebController@item_import']);
        });

        //幫助中心
        Route::group(['prefix' => 'help', 'as' => 'help.'], function () {
            Route::get('index', ['as' => 'index', 'uses' => 'HelpController@index']);
            Route::get('search', ['as' => 'search', 'uses' => 'HelpController@search']);
            Route::get('create', ['as' => 'create', 'uses' => 'HelpController@create']);
            Route::get('edit_index', ['as' => 'edit_index', 'uses' => 'HelpController@edit_index']);
            Route::get('edit/{id}', ['as' => 'edit', 'uses' => 'HelpController@edit']);
            Route::patch('edit/{id}', ['as' => 'update', 'uses' => 'HelpController@update']);
            Route::patch('restore/{id}', ['as' => 'restore', 'uses' => 'HelpController@restore']);
            Route::post('store', ['as' => 'store', 'uses' => 'HelpController@store']);
            Route::delete('destroy/{id}', ['as' => 'destroy', 'uses' => 'HelpController@destroy']);
            Route::delete('force_destroy/{id}', ['as' => 'force_destroy', 'uses' => 'HelpController@force_destroy']);
            Route::get('ajax_get_ans', ['as' => 'ajax_get_ans', 'uses' => 'HelpController@ajax_get_ans']);
        });

        //商品行銷設定
        Route::group(['prefix' => 'item_marketing', 'as' => 'item_marketing.'], function () {
            Route::get('index', ['as' => 'index', 'uses' => 'ItemMarketingController@index']);
            Route::get('create', ['as' => 'create', 'uses' => 'ItemMarketingController@create']);
            Route::post('store', ['as' => 'store', 'uses' => 'ItemMarketingController@store']);
            Route::get('edit/{id}', ['as' => 'edit', 'uses' => 'ItemMarketingController@edit']);
            Route::patch('edit/{id}', ['as' => 'update', 'uses' => 'ItemMarketingController@update']);
            Route::delete('destroy/{id}', ['as' => 'destroy', 'uses' => 'ItemMarketingController@destroy']);
        });

        //物流地區註記
        Route::group(['prefix' => 'logistics_addr_mark', 'as' => 'logistics_addr_mark.'], function () {
            Route::get('index', ['as' => 'index', 'uses' => 'LogisticsAddrMarkController@index']);
            Route::get('create', ['as' => 'create', 'uses' => 'LogisticsAddrMarkController@create']);
            Route::post('store', ['as' => 'store', 'uses' => 'LogisticsAddrMarkController@store']);
            Route::delete('destroy/{id}', ['as' => 'destroy', 'uses' => 'LogisticsAddrMarkController@destroy']);
        });

        //計算機
        Route::group(['prefix' => 'tool', 'as' => 'tool.'], function () {
            Route::get('index', ['as' => 'index', 'uses' => 'ToolController@index']);
            Route::get('floor_sales', ['as' => 'floor_sales', 'uses' => 'ToolController@floor_sales']);
            Route::get('personal_transport_count', ['as' => 'personal_transport_count', 'uses' => 'ToolController@personal_transport_count']);
        });
        //包裝作業
        Route::group(['prefix' => 'package', 'as' => 'package.'], function () {
            Route::get('index', ['as' => 'index', 'uses' => 'PackageController@index']);
            Route::get('check_start', ['as' => 'check_start', 'uses' => 'PackageController@check_start']);
            Route::post('check_store/{sale_no}', ['as' => 'check_store', 'uses' => 'PackageController@check_store']);
            Route::get('start', ['as' => 'start', 'uses' => 'PackageController@start']);
            Route::get('end', ['as' => 'end', 'uses' => 'PackageController@end']);
            Route::get('list', ['as' => 'list', 'uses' => 'PackageController@list']);
            Route::get('order_packager', ['as' => 'order_packager', 'uses' => 'PackageController@order_packager']);
        });
        //產品紀錄
        Route::group(['prefix' => 'product', 'as' => 'product.'], function () {
            Route::group(['prefix' => 'sample', 'as' => 'sample.'], function () {
                Route::post('confirm/{id}', ['as' => 'confirm', 'uses' => 'Product\SampleController@confirm']);
                Route::get('index', ['as' => 'index', 'uses' => 'Product\SampleController@index']);
                Route::get('create', ['as' => 'create', 'uses' => 'Product\SampleController@create']);
                Route::post('store', ['as' => 'store', 'uses' => 'Product\SampleController@store']);
                Route::get('edit/{id}', ['as' => 'edit', 'uses' => 'Product\SampleController@edit']);
                Route::patch('update/{id}', ['as' => 'update', 'uses' => 'Product\SampleController@update']);
                Route::delete('destroy/{id}', ['as' => 'destroy', 'uses' => 'Product\SampleController@destroy']);
                Route::get('detail/{id}', ['as' => 'detail', 'uses' => 'Product\SampleController@detail']);
                Route::get('detail_create/{id}', ['as' => 'detail_create', 'uses' => 'Product\SampleController@detail_create']);
                Route::post('detail_store/{id}', ['as' => 'detail_store', 'uses' => 'Product\SampleController@detail_store']);
                Route::get('detail_edit/{id}', ['as' => 'detail_edit', 'uses' => 'Product\SampleController@detail_edit']);
                Route::patch('detail_update/{id}', ['as' => 'detail_update', 'uses' => 'Product\SampleController@detail_update']);
                Route::delete('detail_destroy/{id}', ['as' => 'detail_destroy', 'uses' => 'Product\SampleController@detail_destroy']);
            });
            Route::group(['prefix' => 'erp', 'as' => 'erp.'], function () {
                Route::post('confirm/{id}', ['as' => 'confirm', 'uses' => 'Product\ErpController@confirm']);
                Route::get('index', ['as' => 'index', 'uses' => 'Product\ErpController@index']);
                Route::get('detail/{id}', ['as' => 'detail', 'uses' => 'Product\ErpController@detail']);
                Route::get('detail_edit/{id}', ['as' => 'detail_edit', 'uses' => 'Product\ErpController@detail_edit']);
                Route::patch('detail_update/{id}', ['as' => 'detail_update', 'uses' => 'Product\ErpController@detail_update']);
                Route::post('export', ['as' => 'export', 'uses' => 'Product\ErpController@export']);
                Route::post('print', ['as' => 'print', 'uses' => 'Product\ErpController@print']);
            });
            //商品提案
            Route::group(['prefix' => 'proposal', 'as' => 'proposal.'], function () {
                Route::get('index', ['as' => 'index', 'uses' => 'Product\ProposalController@index']);
                Route::get('create', ['as' => 'create', 'uses' => 'Product\ProposalController@create']);
                Route::post('store', ['as' => 'store', 'uses' => 'Product\ProposalController@store']);
                Route::get('edit/{id}', ['as' => 'edit', 'uses' => 'Product\ProposalController@edit']);
                Route::patch('update/{id}', ['as' => 'update', 'uses' => 'Product\ProposalController@update']);
                Route::delete('destroy/{id}', ['as' => 'destroy', 'uses' => 'Product\ProposalController@destroy']);
                Route::post('return/{id}', ['as' => 'return', 'uses' => 'Product\ProposalController@return']);
                Route::get('ajax_check_repeat', ['as' => 'ajax_check_repeat', 'uses' => 'Product\ProposalController@ajax_check_repeat']);
            });
        });
        //調轉確認作業
        Route::group(['prefix' => 'order_confirm', 'as' => 'order_confirm.'], function () {
            Route::get('index', ['as' => 'index', 'uses' => 'OrderConfirmController@index']);
            Route::post('store', ['as' => 'store', 'uses' => 'OrderConfirmController@store']);
        });
        //盤點作業
        Route::group(['prefix' => 'inventory', 'as' => 'inventory.'], function () {
            Route::get('index', ['as' => 'index', 'uses' => 'InventoryController@index']);
            Route::get('create', ['as' => 'create', 'uses' => 'InventoryController@create']);
            Route::get('edit/{id}', ['as' => 'edit', 'uses' => 'InventoryController@edit']);
            Route::patch('update/{id}', ['as' => 'update', 'uses' => 'InventoryController@update']);
            Route::post('inventory_done/{id}', ['as' => 'inventory_done', 'uses' => 'InventoryController@inventory_done']);
            Route::post('complete/{id}', ['as' => 'complete', 'uses' => 'InventoryController@complete']);
            Route::post('store', ['as' => 'store', 'uses' => 'InventoryController@store']);
            Route::delete('destroy/{id}', ['as' => 'destroy', 'uses' => 'InventoryController@destroy']);
            Route::post('export/{id}', ['as' => 'export', 'uses' => 'InventoryController@export']);
        });
        //收料作業
        Route::group(['prefix' => 'receipt', 'as' => 'receipt.'], function () {
            Route::get('index', ['as' => 'index', 'uses' => 'ReceiptController@index']);
            Route::post('store', ['as' => 'store', 'uses' => 'ReceiptController@store']);
            Route::get('ajax_get_item', ['as' => 'ajax_get_item', 'uses' => 'ReceiptController@ajax_get_item']);
        });
        //退貨檢驗
        Route::group(['prefix' => 'receipt_return', 'as' => 'receipt_return.'], function () {
            Route::get('index', ['as' => 'index', 'uses' => 'ReceiptReturnController@index']);
            Route::get('create', ['as' => 'create', 'uses' => 'ReceiptReturnController@create']);
            Route::post('store', ['as' => 'store', 'uses' => 'ReceiptReturnController@store']);
            Route::post('update', ['as' => 'update', 'uses' => 'ReceiptReturnController@update']);
            Route::delete('destroy', ['as' => 'destroy', 'uses' => 'ReceiptReturnController@destroy']);
        });
        //外掛倉儲
        Route::group(['prefix' => 'stock', 'as' => 'stock.'], function () {
            Route::get('safety_stock', ['as' => 'safety_stock', 'uses' => 'StockController@safety_stock']);
            Route::get('create_index/{s_id}/{shelf}', ['as' => 'create_index', 'uses' => 'StockController@create_index']);
            Route::get('transfer', ['as' => 'transfer', 'uses' => 'StockController@transfer']);
            Route::post('export/{s_id}', ['as' => 'export', 'uses' => 'StockController@export']);
            Route::post('china_safety_stock_export', ['as' => 'china_safety_stock_export', 'uses' => 'StockController@china_safety_stock_export']);
        });
        //調撥
        Route::group(['prefix' => 'transfer', 'as' => 'transfer.'], function () {
            Route::get('index', ['as' => 'index', 'uses' => 'TransferController@index']);
            Route::get('check_index', ['as' => 'check_index', 'uses' => 'TransferController@check_index']);
            Route::get('export_index', ['as' => 'export_index', 'uses' => 'TransferController@export_index']);
            Route::post('check', ['as' => 'check', 'uses' => 'TransferController@check']);
            Route::post('export', ['as' => 'export', 'uses' => 'TransferController@export']);
        });
        //蝦皮退貨退款訂單
        Route::group(['prefix' => 'order_return_refund', 'as' => 'order_return_refund.'], function () {
            Route::get('index', ['as' => 'index', 'uses' => 'OrderReturnRefundController@index']);
            Route::post('import', ['as' => 'import', 'uses' => 'OrderReturnRefundController@import']);
        });
        //報價單
        Route::group(['prefix' => 'quotation', 'as' => 'quotation.'], function () {
            Route::get('index', ['as' => 'index', 'uses' => 'QuotationController@index']);
            Route::get('create', ['as' => 'create', 'uses' => 'QuotationController@create']);
            Route::post('store', ['as' => 'store', 'uses' => 'QuotationController@store']);
            Route::get('edit/{id}', ['as' => 'edit', 'uses' => 'QuotationController@edit']);
            Route::post('update/{id}', ['as' => 'update', 'uses' => 'QuotationController@update']);
            Route::delete('destroy/{id}', ['as' => 'destroy', 'uses' => 'QuotationController@destroy']);
            Route::post('export/{id}', ['as' => 'export', 'uses' => 'QuotationController@export']);
        });
        //蝦皮訂單欄位設定
        Route::group(['prefix' => 'order_columns', 'as' => 'order_columns.'], function () {
            Route::get('index', ['as' => 'index', 'uses' => 'OrderColumnsController@index']);
            Route::post('update', ['as' => 'update', 'uses' => 'OrderColumnsController@update']);
            Route::post('update_idx', ['as' => 'update_idx', 'uses' => 'OrderColumnsController@update_idx']);
        });
        //國定假日設定
        Route::group(['prefix' => 'holiday', 'as' => 'holiday.'], function () {
            Route::get('index', ['as' => 'index', 'uses' => 'HolidayController@index']);
            Route::get('update', ['as' => 'update', 'uses' => 'HolidayController@update']);
        });
        //貨櫃進口
        Route::group(['prefix' => 'container_import', 'as' => 'container_import.'], function () {
            Route::get('index', ['as' => 'index', 'uses' => 'ContainerImportController@index']);
            Route::get('create', ['as' => 'create', 'uses' => 'ContainerImportController@create']);
            Route::post('store', ['as' => 'store', 'uses' => 'ContainerImportController@store']);
            Route::get('edit/{id}', ['as' => 'edit', 'uses' => 'ContainerImportController@edit']);
            Route::post('update/{id}', ['as' => 'update', 'uses' => 'ContainerImportController@update']);
            Route::delete('destroy/{id}', ['as' => 'destroy', 'uses' => 'ContainerImportController@destroy']);
            Route::post('finish/{id}', ['as' => 'finish', 'uses' => 'ContainerImportController@finish']);
        });
        //帳號管理
        Route::group(['prefix' => 'account', 'as' => 'account.'], function () {
            Route::get('index', ['as' => 'index', 'uses' => 'AccountController@index']);
            Route::get('create', ['as' => 'create', 'uses' => 'AccountController@create']);
            Route::post('store', ['as' => 'store', 'uses' => 'AccountController@store']);
            Route::delete('destroy/{id}', ['as' => 'destroy', 'uses' => 'AccountController@destroy']);
            Route::post('import', ['as' => 'import', 'uses' => 'AccountController@import']);
        });
        //內部採購
        Route::group(['prefix' => 'purchase', 'as' => 'purchase.'], function () {
            Route::get('index', ['as' => 'index', 'uses' => 'PurchaseController@index']);
        });
        //出勤
        Route::group(['prefix' => 'attendance', 'as' => 'attendance.'], function () {
            Route::get('index', ['as' => 'index', 'uses' => 'AttendanceController@index']);
            Route::post('ajax_check_in', ['as' => 'ajax_check_in', 'uses' => 'AttendanceController@ajax_check_in']);
        });
        //行事曆
        Route::group(['prefix' => 'calendar', 'as' => 'calendar.'], function () {
            Route::get('index', ['as' => 'index', 'uses' => 'CalendarController@index']);
            Route::get('create', ['as' => 'create', 'uses' => 'CalendarController@create']);
            Route::post('store', ['as' => 'store', 'uses' => 'CalendarController@store']);
            Route::get('edit/{id}', ['as' => 'edit', 'uses' => 'CalendarController@edit']);
            Route::patch('update/{id}', ['as' => 'update', 'uses' => 'CalendarController@update']);
            Route::delete('destroy/{id}', ['as' => 'destroy', 'uses' => 'CalendarController@destroy']);
        });
    });
    //其他公司
    Route::group(['middleware' => ['role:YoHouse2']], function () {
        Route::group(['prefix' => 'erp', 'as' => 'erp.'], function () {
            //銷貨作業
            Route::group(['prefix' => 'sales', 'as' => 'sales.'], function () {
                Route::get('index', ['as' => 'index', 'uses' => 'Erp\SalesController@index']);
                Route::get('create', ['as' => 'create', 'uses' => 'Erp\SalesController@create']);
                Route::post('store', ['as' => 'store', 'uses' => 'Erp\SalesController@store']);
                Route::get('edit/{id}', ['as' => 'edit', 'uses' => 'Erp\SalesController@edit']);
                Route::patch('update/{id}', ['as' => 'update', 'uses' => 'Erp\SalesController@update']);
                Route::delete('destroy/{id}', ['as' => 'destroy', 'uses' => 'Erp\SalesController@destroy']);
            });
        });
    });
});
//真桂森林
Route::group(['prefix' => 'cinforest', 'as' => 'cinforest.'], function () {
    Route::get('index', ['as' => 'index', 'uses' => 'CinForestController@index']);
    Route::patch('update', ['as' => 'update', 'uses' => 'CinForestController@update']);
});
//權限
Auth::routes(['register' => false, 'reset' => false]);
