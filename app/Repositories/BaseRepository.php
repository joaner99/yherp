<?php

namespace App\Repositories;

use App\User;
use DateTime;
use Exception;
use Carbon\Carbon;
use App\Models\TMS\CUST;
use App\Models\TMS\Item;
use App\Models\TMS\ITYP;
use App\Models\TMS\PART;
use App\Models\TMS\SALE;
use App\Models\TMS\SISE;
use App\Models\TMS\STOC;
use App\Models\TMS\ITEM1;
use App\Models\TMS\ITEM4;
use App\Models\TMS\ITEMP;
use App\Models\TMS\Order;
use App\Models\TMS\Histin;
use App\Models\TMS\Posein;
use App\Models\TMS\HCTData;
use App\Models\yherp\AdCost;
use App\Models\yherp\AdItem;
use App\Services\BaseService;
use App\Models\TMS\OrderDetail;
use App\Models\yherp\ItemPlaceD;
use App\Models\yherp\SafetyStock;
use App\Models\yherp\YcsTransport;
use Illuminate\Support\Facades\DB;
use App\Models\TMS\ApiShopeeProduct;
use App\Models\yherp\OrderSampleFile;

class BaseRepository
{
    //排除的特殊客戶。面交、邱大哥、亞銘燈飾、外包設計師、包材、半成品領料、其他
    public const excludeedCustomers = ['C001', 'D002', 'D001', 'F001', 'x001', 'x002', 'Z001'];
    //排除產品的大類。其他、包材
    public const excludedSalesType = ['98', '99'];
    //贈品的料號
    public const bulbCode = ['10010009001', '10010009002', '10003009012'];

    /**
     * 取得所有訂單細項(未取消、未出貨)
     *
     * @return \Illuminate\Support\Collection
     */
    public function GetOrderRequire()
    {
        $result = OrderDetail::select('OCOD1', 'OICOD', 'OINAM', 'ONNO')
            ->withWhereHas('Main', function ($q) {
                $q
                    ->select('OCOD1', 'OCOD4')
                    ->where('ODAT2', '') //未有出貨日期
                    ->where('OCANC', '已核示'); //非訂單取消、待核准
            })
            ->whereHas('Item', function ($q) {
                $q->select(DB::raw('1'))
                    ->where('ICONS', 'N') //只取一般型態
                    ->whereNotIn('ITCOD', ['98', '99']); //排除網路運費、折扣
            })
            ->whereIn('OINYN', ['N', '']) //只取未出貨的訂單細項及連帶子項預設為空白
            ->whereNotNull('ONNO')
            ->whereNotNull('OINUM');

        return $result->get();
    }

    /**
     * 取的指定日期的所有訂單
     *
     * @param string $sDate 民國起日。yyy.mm.dd
     * @param string $eDate 民國訖日。yyy.mm.dd
     * @return \Illuminate\Support\Collection
     */
    public function GetOrderAll(string $sDate, string $eDate)
    {
        $result = Order::select('OCOD1', 'OCOD4', 'OBAK1', 'OCANC', 'OCNAM', 'ODATE', 'TransportCode')
            ->with([
                'OrderDetail:OCOD1,OICOD,OINAM,OINUM',
                'OrderDetail.Stoc:STCOD,SITEM,SNUMB',
                'OrderDetail.Item:ICODE,ITCOD',
                'OrderDetail.ITEM1:ICODE1,ITCO3',
            ])
            ->doesnthave('Posein')
            ->whereBetween('ODATE', [$sDate, $eDate])
            ->whereNotIn('OCCOD', self::excludeedCustomers)
            ->where('OCANC', '!=', '訂單取消')
            ->get();
        $result->map(function ($item) {
            $details = $item->OrderDetail;
            //訂單是否含地板
            $IsFloor = 0;
            if (!empty($details) && count($details) > 0) {
                foreach ($details as $d) {
                    if ((!empty($d->Item) && $d->Item->ITCOD == '11' && !empty($d->ITEM1) && in_array($d->ITEM1->ITCO3, ['014', '046']))) {
                        $IsFloor = 1;
                        break;
                    }
                }
            }
            $item->IsFloor = $IsFloor;
            //訂單是否滿足出貨
            $CanPrint = true;
            foreach ($details->groupBy('OICOD') as $item_no => $group) {
                $orderNeedNum = $group->sum('OINUM');
                $outStockNum = $group->first()->Stoc->where('STCOD', 'A001')->sum('SNUMB');
                if ($orderNeedNum > $outStockNum) {
                    $CanPrint = false;
                    break;
                }
            }
            $item->CanPrint = $CanPrint;
            return $item;
        });
        return $result;
    }

    /**
     * 取得指定區間，有備註的訂單
     *
     * @param string $sDate 民國起日。yyy.mm.dd
     * @param string $eDate 民國訖日。yyy.mm.dd
     * @return \Illuminate\Support\Collection 
     */
    public function GetOrderRemark(string $sDate, string $eDate)
    {
        $result = Order::select('OCOD1', 'OCOD4', 'OCCOD', 'OCNAM', 'OBAK1', 'OBAK2', 'OBAK3', 'InsideNote')
            ->whereBetween('ODATE', [$sDate, $eDate])
            ->whereIn('OCCOD', ['R001', 'S001', 'S002', 'S003', 'S004', 'S005', 'S006'])
            ->where(function ($query) {
                $query
                    ->where('OBAK1', 'LIKE', '%備註%') //備註1含有"備註"關鍵字
                    ->orWhere('OBAK2', '!=', '') //備註2有資料
                    ->orWhere('InsideNote', '!=', ''); //內部備註有資料
            });

        return $result->get();
    }

    /**
     * 取得指定料號、區間下的訂單銷售額
     *
     * @param string $sDate 民國起日。yyy.mm.dd
     * @param string $eDate 民國訖日。yyy.mm.dd
     * @param string $occod 客代
     * @param array $items 料號清單
     * @return \Illuminate\Support\Collection 
     */
    public function GetOrderByItems(string $sDate, string $eDate, string $occod, array $items)
    {
        $result = OrderDetail::select('ODATE', DB::raw('SUM(OTOTA) AS OTOTA'))
            ->where('OCCOD', $occod)
            ->whereIn('OICOD', $items)
            ->whereBetween('ODATE', [$sDate, $eDate])
            ->where('OTOTA', '!=', 0)
            ->groupBy('ODATE');
        return $result->get();
    }

    /**
     * 取得所有倉庫代碼
     *
     * @return \Illuminate\Support\Collection
     */
    public function GetStockType()
    {
        $result = ITYP::select('ITCOD', 'ITNAM')
            ->where('ITYPE', '1');

        return $result->get();
    }

    /**
     * 取得各庫存資料
     * 
     * @param array $sitemList 料號清單
     * @return \Illuminate\Support\Collection
     */
    public function GetStock(array $sitemList = null, array $stockList = null)
    {
        $result = STOC::selectRaw('SITEM, STNAM, SUM(SNUMB) AS SNUMB')
            ->groupBy('STCOD', 'SITEM', 'STNAM')
            ->orderBy('SITEM')
            ->orderBy('STNAM');

        if (!empty($sitemList)) {
            $result->whereIn('SITEM', $sitemList);
        }
        if (!empty($stockList)) {
            $result->whereIn('STCOD', $stockList);
        }

        return $result->get();
    }

    /**
     * 取得安全庫存
     *
     * @param string $sDate 民國起日。yyy.mm.dd
     * @param string $eDate 民國訖日。yyy.mm.dd
     * @return \Illuminate\Support\Collection
     */
    public function GetSafetyStock()
    {
        $excludeItem = ['11010014045', '11010014046', '11010014047', '11010014048', '11010014055', '11010014056', '11010014063']; //欲排除料號
        //盤點子查詢
        $inventoryWorkSub = DB::table('InventoryWorkDetails')
            ->selectRaw('ROW_NUMBER() OVER(PARTITION BY ICODE ORDER BY UpdateDate DESC) AS ROWNUM')
            ->addSelect('ICODE', 'UpdateDate', 'Quantity');

        $result = DB::table('ITEM AS I')
            ->select('I.IPNAM', 'I.ICODE', 'I.INAME', DB::raw("(SELECT SUM(SNUMB) FROM STOC WHERE STCOD = 'A001' AND SITEM = I.ICODE) AS SNUMB"))
            ->addSelect(DB::raw('CONVERT(VARCHAR, IW.UpdateDate, 120) AS UpdateDate'), 'IW.Quantity', 'I4.T11_11')
            ->whereExists(function ($q) { //排除產品註記暫停出貨
                $q
                    ->select(DB::raw(1))
                    ->from('ITEM1 AS I1')
                    ->whereColumn('I1.ICODE1', 'I.ICODE')
                    ->where('I1.ISTOP', 0);
            })
            ->whereExists(function ($q) { //排除產品註記廠商停產
                $q
                    ->select(DB::raw(1))
                    ->from('ITEM3 AS I3')
                    ->whereColumn('I3.ICODE3', 'I.ICODE')
                    ->where('I3.IPSTOP', 0);
            })
            ->leftJoinSub($inventoryWorkSub, 'IW', function ($join) {
                $join
                    ->on('IW.ICODE', 'I.ICODE')
                    ->where('IW.ROWNUM', 1);
            })
            ->leftJoin('ITEM4 AS I4', function ($join) {
                $join
                    ->on('I4.ICODE4', 'I.ICODE');
            })
            ->whereNotIn('I.ITCOD', self::excludedSalesType)
            ->orderBy('I.ICODE');

        //排除料號
        if (!empty($excludeItem)) {
            $result->whereNotIn('I.ICODE', $excludeItem);
        }

        return $result->get();
    }

    /**
     * 取得安全庫存紀錄
     *
     * @param string $date YYYY-MM-DD
     * @return \Illuminate\Support\Collection
     */
    public function GetSafetyStockHistory(string $date, $designation = false)
    {
        $result = SafetyStock::whereBetween('created_at', [$date . ' 00:00:00', $date . ' 23:59:59']);
        if ($designation) {
            $result->where('item_remark', 'china');
        }

        return $result->get();
    }

    /**
     * 取得所有庫存
     *
     * @return \Illuminate\Support\Collection
     */
    public function GetSaleStockAll()
    {
        //使用ORM會超過參數限制
        $result = DB::table('STOC AS S')
            ->select('I.ICODE', 'I.INAME', 'I.ITCOD', 'I.ITNAM', 'I1.ISTOP', 'I3.IPSTOP', 'S.STNAM', 'S.SNUMB', DB::raw('S.SNUMB*I.ISOUR AS inventory_amt'))
            ->addSelect('I1.ITCO3')
            ->join('ITEM AS I', 'I.ICODE', 'S.SITEM')
            ->join('ITEM1 AS I1', function ($join) { //排除暫停出貨的料號
                $join
                    ->on('I1.ICODE1', 'I.ICODE');
            })
            ->join('ITEM3 AS I3', function ($join) { //排除產品註記廠商停產
                $join
                    ->on('I3.ICODE3', 'I.ICODE');
            })
            ->where('S.STCOD', 'A001')
            ->orderByDesc('S.STNAM');

        return $result->get();
    }

    /**
     * 取得庫存異常
     *
     * @return \Illuminate\Support\Collection
     */
    public function GetStockUnusual()
    {
        $result = STOC::select('STCOD', 'SITEM', 'STNAM', 'SNUMB')
            ->withWhereHas('Item', function ($q) {
                $q->select('ICODE', 'INAME')->whereNotIn('ITCOD', self::excludedSalesType);
            })
            ->whereHas('ITEM1', function ($q) { //排除暫停出貨的料號
                $q->where('ISTOP', 0);
            })
            ->whereHas('ITEM3', function ($q) { //排除產品註記廠商停產
                $q->where('IPSTOP', 0);
            })
            ->where('SNUMB', '<', 0);

        return $result->get();
    }

    /**
     * 取得庫存可銷售量
     *
     * @param string|null $items 料號
     * @return \Illuminate\Support\Collection
     */
    public function GetStockCanSale($items)
    {
        //訂單需求子查詢
        $orderSub = DB::table('ORDET AS M')
            ->selectRaw('D.OICOD, SUM(D.OINUM) AS OrderNeed')
            ->join('ORSUB AS D', 'D.OCOD1', 'M.OCOD1')
            ->leftJoin('POSEIN AS P', 'P.PCOD2', 'M.OCOD1')
            ->where('M.OCANC', '!=', '訂單取消')
            ->where('D.OINYN', 'N')
            ->whereNotNull('D.OINUM')
            ->whereIn('D.OICOD', $items)
            ->groupBy('D.OICOD');
        $result = DB::table('ITEM AS I')
            ->select('I.ICODE', 'I.INAME', 'O.OrderNeed')
            ->addSelect(DB::raw("(select SUM(SNUMB) from STOC where STCOD not in ('D001', 'I001') AND SITEM = I.ICODE) AS 'Stock'"))
            ->addSelect('I.ITNAM', 'I1.ITNA2', 'ITNA3')
            ->leftJoinSub($orderSub, 'O', 'O.OICOD', 'I.ICODE')
            ->join('ITEM1 AS I1', function ($join) { //排除暫停出貨的料號
                $join
                    ->on('I1.ICODE1', 'I.ICODE')
                    ->where('I1.ISTOP', 0);
            })
            ->whereExists(function ($q) { //排除產品註記廠商停產
                $q
                    ->select(DB::raw(1))
                    ->from('ITEM3 AS I3')
                    ->whereColumn('I3.ICODE3', 'I.ICODE')
                    ->where('I3.IPSTOP', 0);
            })
            ->whereNotIn('I.ITCOD', self::excludedSalesType)
            ->whereIn('I.ICODE', $items)
            ->orderBy('I.ICODE');

        return $result->get();
    }

    /**
     * 取得中國商品庫存
     *
     * @return \Illuminate\Support\Collection
     */
    public function GetStockCountry()
    {
        $result = ITEM4::select('ICODE4')
            ->with(['Item' => function ($q) {
                $q->select('ICODE', 'INAME', 'ITNAM');
            }])
            ->with(['STOC' => function ($q) {
                $q->select('SITEM', 'STCOD', 'STNAM', 'SNUMB')
                    ->where('STCOD', 'A001');
            }])
            ->whereHas('ITEM1', function ($q) { //排除暫停出貨的料號
                $q->where('ISTOP', 0);
            })
            ->whereHas('ITEM3', function ($q) { //排除產品註記廠商停產
                $q->where('IPSTOP', 0);
            })
            ->where('T11_11', 'china')
            ->orderBy('ICODE4');

        return $result->get();
    }

    /**
     * 取得儲位所有產品資料
     *
     * @return \Illuminate\Support\Collection
     */
    public function GetSaleStorageAll()
    {
        $result = ITEMP::select('IICOD', 'ICODE')
            ->with(['STOC' => function ($q) {
                $q->select('SITEM', 'SNUMB')
                    ->where('STCOD', 'A001');
            }])
            ->withWhereHas('Item', function ($q) {
                $q->select('ICODE', 'INAME', 'ITNAM')->where('ITCOD', '!=', '98');
            })
            ->whereHas('ITEM1', function ($q) { //排除暫停出貨的料號
                $q->where('ISTOP', 0);
            })
            ->whereHas('ITEM3', function ($q) { //排除產品註記廠商停產
                $q->where('IPSTOP', 0);
            })
            ->where('ITCOD', 'A001')
            ->orderBy('IICOD')
            ->orderBy('ICODE');

        return $result->get();
    }

    /**
     * 取得產品所有詳細資料
     *
     * @param array $icodeList 產品料號清單
     * @return \Illuminate\Support\Collection
     */
    public function GetItemAllDetail(array $icodeList)
    {
        $result = Item::select('ICODE', 'INAME', 'IUNIT', 'ITNAM', 'IPCOD', 'IPNAM', 'ITCOD')
            ->with(['ITEM1' => function ($q) {
                $q->select('ICODE1', 'ISTOP', 'ENA13', 'CO128', 'ICOD2', 'ITNA2', 'ITNA3', 'IWEIGHT', 'ITCO2', 'ITCO3');
            }])
            ->with(['ITEM3' => function ($q) {
                $q->select('ICODE3', 'IPSTOP');
            }])
            ->with(['ITEM4' => function ($q) {
                $q->select('ICODE4', 'T11_11', 'PDepth', 'PWidth', 'PHeight', 'Box_Weight', 'Box_Depth', 'Box_Width', 'Box_Height', 'Box_CUFT', 'Box_CBM');
            }])
            ->whereIn('ICODE', $icodeList);

        return $result->get();
    }

    /**
     * 取得類別
     *
     * @return \Illuminate\Support\Collection
     */
    public function GetItemType()
    {
        //使用ORM會超過參數限制
        $result = DB::table('ITEM AS I')
            ->select('I.ITCOD', 'I.ITNAM', 'I1.ITCO2', 'I1.ITNA2', 'I1.ITCO3', 'I1.ITNA3')
            ->join('ITEM1 AS I1', 'I1.ICODE1', 'I.ICODE')
            ->where('ITCOD', '!=', '')
            ->where('ITCO2', '!=', '')
            ->where('ITCO3', '!=', '')
            ->distinct();

        return $result->get();
    }

    /**
     * 取得供應商
     *
     * @return \Illuminate\Support\Collection
     */
    public function GetItemSupply()
    {
        $result = PART::select('PCODE', 'PNAM2')
            ->orderBy('PNAM2')
            ->distinct();

        return $result->get();
    }

    /**
     * 取得產品儲位資料
     *
     * @return array
     */
    public function GetItemp($icodeList = null)
    {
        $sale_place = ITEMP::select('IICOD', 'ICODE')
            ->where('ITCOD', 'A001')
            ->orderBy('IICOD')
            ->orderBy('ICODE');
        $main_place = ItemPlaceD::select('m_id', 'item_no')
            ->whereHas('Main', function ($q) {
                $q->where('s_id', 'B001');
            })
            ->where('qty', '!=', 0);
        if (!empty($icodeList) && count($icodeList) > 0) {
            $sale_place->whereIn('ICODE', $icodeList);
            $main_place->whereIn('item_no', $icodeList);
        }
        $sale_place = $sale_place->get();
        $main_place = $main_place->get();
        $result = [];
        foreach ($sale_place as $s) {
            $result[$s->ICODE][] = $s->IICOD;
        }
        foreach ($main_place as $m) {
            $result[$m->item_no][] = $m->m_id;
        }

        return $result;
    }

    /**
     * 取得指定料號的未進貨採購資料
     *
     * @param array $icodeList
     * @return \Illuminate\Support\Collection
     */
    public function GetPurchaseNotIn(array $icodeList = [])
    {
        $result = SISE::select('SICOD', 'SINUM', 'SONUM', 'SCOD1', 'SDAT1', 'SPNAM', DB::raw('(SINUM-SONUM) AS "remain"'))
            ->where('SINYN', 'N') //Y=進貨完畢、N=未進貨完畢、C=取消
            ->orderByDesc('SDAT1');
        if (!empty($icodeList) && count($icodeList) > 0) {
            $result->whereIn('SICOD', $icodeList);
        }
        return $result->get();
    }

    /**
     * 取得指定日期未進貨品項
     *
     * @param string $date
     * @return \Illuminate\Support\Collection
     */
    public function GetPurchaseNotInItems()
    {
        $result = SISE::select('SICOD', 'SINAM')
            ->where('SINYN', 'N')
            ->distinct();

        return $result->get();
    }

    /**
     * 取得當日銷貨單
     *
     * @param string $tw_today
     * @param array $extra
     * @return \Illuminate\Support\Collection
     */
    public function GetSalesToday($tw_today, array $extra = null)
    {
        $result = DB::table('POSEIN AS M')
            ->select('M.PCOD1', 'M.TransportCode', 'M.TransportQty', 'M.PPRIN')
            ->addSelect(DB::raw("(SELECT TOP(1) 1 FROM HISTIN AS D JOIN ITEM1 AS I1 ON I1.ICODE1=D.HICOD WHERE D.HCOD1=M.PCOD1 AND D.HYCOD = '11' AND I1.ITCO3 IN ('014','046')) AS 'IsFloor'"))
            ->addSelect(DB::raw("(SELECT TOP(1) CREATE_Date FROM PoseinCheckDataLog AS L WHERE L.PCOD1=M.PCOD1) AS 'CheckTime'"))
            ->addSelect(DB::raw("(SELECT TOP(1) Quantity FROM HCT_Data WHERE PCOD1=M.PCOD1 ORDER BY CREATE_DATE DESC) AS 'HctQty'"))
            ->where('M.PDAT1', $tw_today);
        if (!empty($extra)) {
            $result->orwhereIn('M.PCOD1', $extra);
        }

        return $result->get();
    }

    /**
     * 取得指定訂單出貨紀錄
     *
     * @param string $id 銷貨單號/原始訂單號/備註1~3/託運單號
     * @return \Illuminate\Support\Collection
     */
    public function GetOrderCheck(string $keyword)
    {
        $result = Order::select('OCOD1', 'OCOD4', 'OCANC', 'TransportCode', 'TransportName')
            ->with(['Posein' => function ($q) {
                $q->select('PCOD1', 'PCOD2', 'PTXCO', 'TransportCode', 'ConsignTran');
            }])
            ->with(['Posein.PoseinCheckDataLog' => function ($q) {
                $q->select('PCOD1', 'CheckCount', 'CREATE_Date');
            }])
            ->with(['Posein.LogPackage' => function ($q) {
                $q->select('sale_no', 'created_at');
            }])
            ->with(['Posein.LogShipment' => function ($q) {
                $q->select('PCOD1', 'created_at');
            }])
            ->where('OCOD1', $keyword) //訂單單號
            ->orWhere('OCOD4', $keyword) //原始訂單號
            ->orWhere('OBAK1', 'LIKE', "%{$keyword}%") //備註1
            ->orWhere('OBAK2', 'LIKE', "%{$keyword}%") //備註2
            ->orWhere('OBAK3', 'LIKE', "%{$keyword}%") //備註3
            ->orWhere('InsideNote', 'LIKE', "%{$keyword}%") //備註3
            ->orWhereHas('Posein', function ($q) use ($keyword) { //銷貨單號、託運單號
                $q->where('PCOD1', $keyword)->orWhere('ConsignTran', $keyword);
            })
            ->orderBy('OCOD1', 'DESC');

        return $result->get();
    }

    /**
     * 取得指定區間驗貨最新一筆資料
     *
     * @param string $sDate 起日
     * @param string $eDate 訖日
     * @return \Illuminate\Support\Collection
     */
    public function GetSalesLastCheck(string $sDate, string $eDate)
    {
        $sub = DB::table('PoseinCheckDataLog')
            ->select(DB::raw('ROW_NUMBER() OVER (PARTITION BY PCOD1 ORDER BY CREATE_Date DESC) AS ROWNUM, PCOD1, SCODE, SNAME, CREATE_Date'))
            ->whereBetween('CREATE_Date', [$sDate, $eDate]);
        $result = DB::table(DB::raw("({$this->GetSqlWithParameter($sub)}) AS T"))
            ->select('T.PCOD1', 'T.SCODE', 'T.SNAME', 'T.CREATE_Date', DB::raw('CONVERT(VARCHAR,T.CREATE_Date,111) AS CheckDate'))
            ->where('ROWNUM', 1)
            ->orderBy('CheckDate')
            ->orderBy('PCOD1');

        return $result->get();
    }

    /**
     * 取得昨日一個月訂單，排除訂單取消、退貨紀錄，未完成訂單來源
     *
     * @param string $sDate 民國起日。yyymmdd
     * @param string $eDate 民國訖日。yyymmdd
     * @param array $track_order 定期追蹤的訂單
     * @return \Illuminate\Support\Collection
     */
    public function GetSalesUnchecked(string $sDate, string $eDate, array $track_order = null)
    {
        $result = Order::select('ODATE', 'OCCOD', 'OCNAM', 'OCOD1', 'OCOD4', 'OBAK1', 'OBAK2', 'OBAK3', 'InsideNote', 'TransportCode')
            ->with('OrderRemarkM', 'OrderRemarkM.Details', 'OrderRemarkM.Details.User')
            ->with(['Posein' => function ($q) {
                $q->select('PCOD2', 'PCOD1', 'PPRIN', 'PPTIM', 'TransportCode', 'TransportName', 'PBAK1', 'PBAK2', 'PBAK3', 'InsideNote');
            }])
            ->with(['Posein.LogPickingPrint' => function ($q) {
                $q->select('PCOD1', 'created_at');
            }])
            ->with(['Posein.PoseinCheckDataLog' => function ($q) {
                $q->select('PCOD1', 'CREATE_Date');
            }])
            ->with(['Posein.LogPackage' => function ($q) {
                $q->select('sale_no', 'created_at');
            }])
            ->with(['Posein.LogShipment' => function ($q) {
                $q->select('PCOD1', 'created_at');
            }])
            ->whereBetween('ODATE', [$sDate, $eDate])
            ->where('OCANC', '已核示')
            ->whereNotIn('OCCOD', ['x001', 'x002', 'C001'])
            ->orderBy('OCOD1');
        if (!empty($track_order) && count($track_order) > 0) {
            $result->orWhereIn('OCOD1', $track_order);
        }

        return $result->get();
    }

    /**
     * 取得指定時間內，未驗貨明細
     *
     * @param string $sDate 民國起日。yyy.mm.dd
     * @param string $eDate 民國訖日。yyy.mm.dd
     * @param array $items 料號清單
     * @return \Illuminate\Support\Collection
     */
    public function GetSalesUncheckedDetail(string $sDate, string $eDate, array $items)
    {
        $result = Histin::select('HCOD1', 'HICOD', 'HINUM')
            ->whereBetween('HDAT1', [$sDate, $eDate])
            ->whereIn('HICOD', $items)
            ->doesnthave('PoseinCheckDataLog')
            ->orderBy('HCOD1');

        return $result->get();
    }

    /**
     * 取得商品成長比較表
     *
     * @param string $a_sDate A區間民國起日。yyy.mm.dd
     * @param string $a_eDate A區間民國訖日。yyy.mm.dd
     * @param string $b_sDate B區間民國起日。yyy.mm.dd
     * @param string $b_eDate B區間民國訖日。yyy.mm.dd
     * @param string $id 指定料號
     * @param string $type 指定大類
     * @param string $type2 指定中類
     * @param string $type3 指定小類
     * @param string $customer 指定客戶代碼
     * @return \Illuminate\Support\Collection
     */
    public function GetSalesComparison(string $a_sDate, string $a_eDate, string $b_sDate, string $b_eDate, string $id = null, string $type = null, string $type2 = null, string $type3 = null, string $customer = null)
    {
        //網路銷售
        $webSub = DB::table('HISTIN')->select('HICOD', 'HTOTA', 'HINUM');
        $aWebSub = (clone $webSub)->whereBetween('HDAT1', [$a_sDate, $a_eDate]);
        $bWebSub = (clone $webSub)->whereBetween('HDAT1', [$b_sDate, $b_eDate]);
        //POS銷售
        $posSub = DB::table('PosDetailsTemp')->select('ICODE AS HICOD', 'SaleMoneyTotal AS HTOTA', 'Quantity AS HINUM');
        $aPosSub = (clone $posSub)->whereBetween('CREATE_Date', [$this->ConvertTWToAD($a_sDate) . ' 00:00:00', $this->ConvertTWToAD($a_eDate) . ' 23:59:59']);
        $bPosSub = (clone $posSub)->whereBetween('CREATE_Date', [$this->ConvertTWToAD($b_sDate) . ' 00:00:00', $this->ConvertTWToAD($b_eDate) . ' 23:59:59']);
        //篩選，特定客戶代碼
        if (empty($customer)) { //不篩選
            $aWebSub->unionAll($aPosSub);
            $bWebSub->unionAll($bPosSub);
        } elseif ($customer == 'Z002') { //門市
            $aWebSub = $aPosSub;
            $bWebSub = $bPosSub;
        } else { //非門市指定客戶
            $aWebSub->where('HPCOD', $customer);
            $bWebSub->where('HPCOD', $customer);
        }
        //依料號分組計算銷售、數量
        $aSub = DB::table(DB::raw("({$this->GetSqlWithParameter($aWebSub)}) AS T"))
            ->select('T.HICOD', DB::raw('SUM(T.HTOTA) AS HTOTA'), DB::raw('SUM(T.HINUM) AS HINUM'))
            ->groupBy('T.HICOD');
        $bSub = DB::table(DB::raw("({$this->GetSqlWithParameter($bWebSub)}) AS T"))
            ->select('T.HICOD', DB::raw('SUM(T.HTOTA) AS HTOTA'), DB::raw('SUM(T.HINUM) AS HINUM'))
            ->groupBy('T.HICOD');

        $result = DB::table(DB::raw("({$this->GetSqlWithParameter($aSub)}) AS A"))
            ->select('I.ICODE', 'I.INAME', 'I.ITNAM', 'I.ISOUR', 'A.HTOTA AS A_HTOTA', 'B.HTOTA AS B_HTOTA', 'A.HINUM AS A_HINUM', 'B.HINUM AS B_HINUM')
            ->joinSub($this->GetSqlWithParameter($bSub), 'B',  'B.HICOD', '=', 'A.HICOD', 'full')
            ->join('ITEM AS I', function ($join) {
                $join
                    ->on('I.ICODE', 'A.HICOD')
                    ->orOn('I.ICODE', 'B.HICOD');
            })
            ->join('ITEM1 AS I1', function ($join) { //排除暫停出貨的料號
                $join
                    ->on('I1.ICODE1', 'I.ICODE')
                    ->where('I1.ISTOP', 0);
            })
            ->whereExists(function ($q) { //排除產品註記廠商停產
                $q
                    ->select(DB::raw(1))
                    ->from('ITEM3 AS I3')
                    ->whereColumn('I3.ICODE3', 'I.ICODE')
                    ->where('I3.IPSTOP', 0);
            })
            ->orderByDesc('A_HTOTA');
        //篩選
        if (!empty($id)) { //特定料號
            $result->where('I.ICODE', $id);
        }
        if (!empty($type)) { //特定大類
            $result->where('I.ITCOD', $type);
        }
        if (!empty($type2)) { //特定中類
            $result->where('I1.ITCO2', $type2);
        }
        if (!empty($type3)) { //特定小類
            $result->where('I1.ITCO3', $type3);
        }

        return $result->get();
    }

    /**
     * 取得商品成長月份比較表
     *
     * @param string $a_sDate A區間民國起日。yyy.mm.dd
     * @param string $a_eDate A區間民國訖日。yyy.mm.dd
     * @param string $b_sDate B區間民國起日。yyy.mm.dd
     * @param string $b_eDate B區間民國訖日。yyy.mm.dd
     * @param string|null $id 指定料號
     * @param string|null $type 指定大類
     * @param string|null $type2 指定中類
     * @param string|null $type3 指定小類
     * @param string|null $customer 指定客戶代碼
     * @return \Illuminate\Support\Collection
     */
    public function GetSalesComparisonMonth(string $a_sDate, string $a_eDate, string $b_sDate, string $b_eDate, string $id = null, string $type = null, $type2 = null, $type3 = null, string $customer = null)
    {
        $aSub = $this->GetSalesComparisonMonthSub($a_sDate, $a_eDate, $id, $type, $type2, $type3, $customer);
        $bSub = $this->GetSalesComparisonMonthSub($b_sDate, $b_eDate, $id, $type, $type2, $type3, $customer);

        $result = DB::table(DB::raw("({$this->GetSqlWithParameter($aSub)}) AS A"))
            ->select(DB::raw('CASE WHEN A.HDAT1 IS NULL THEN B.HDAT1 ELSE A.HDAT1 END AS HDAT1'))
            ->addSelect('A.HTOTA AS A_HTOTA', 'B.HTOTA AS B_HTOTA', 'A.HINUM AS A_HINUM', 'B.HINUM AS B_HINUM', 'A.COST AS A_COST', 'B.COST AS B_COST')
            ->joinSub($this->GetSqlWithParameter($bSub), 'B',  'B.HDAT1', '=', 'A.HDAT1', 'full')
            ->orderByDesc('A_HTOTA');

        return $result->get();
    }

    /**
     * 取得指定區間的商品資料
     *
     * @param string $sDate 區間民國起日。yyy.mm.dd
     * @param string $eDate 區間民國訖日。yyy.mm.dd
     * @param string|null $id 指定料號
     * @param string|null $type 指定大類
     * @param string|null $type2 指定中類
     * @param string|null $type3 指定小類
     * @param string|null $customer 指定客戶代碼
     * @return \Illuminate\Database\Query\Builder
     */
    private function GetSalesComparisonMonthSub($sDate, $eDate, string $id = null, string $type = null, string $type2 = null, string $type3 = null, string $customer = null)
    {
        $webSub = DB::table('HISTIN')
            ->select(DB::raw('SUBSTRING(HDAT1,5,2) AS HDAT1'), 'HTOTA', 'HINUM', 'HICOD')
            ->whereBetween('HDAT1', [$sDate, $eDate]);
        $posSub = DB::table('PosDetailsTemp')
            ->select(DB::raw('SUBSTRING(CONVERT(varchar,CREATE_Date,101),1,2) AS HDAT1'), 'SaleMoneyTotal AS HTOTA', 'Quantity AS HINUM', 'ICODE AS HICOD')
            ->whereBetween('CREATE_Date', [$this->ConvertTWToAD($sDate) . ' 00:00:00', $this->ConvertTWToAD($eDate) . ' 23:59:59']);
        //篩選，特定客戶代碼
        if (empty($customer)) { //不篩選
            $webSub->unionAll($posSub);
        } elseif ($customer == 'Z002') { //門市
            $webSub = $posSub;
        } else { //非門市指定客戶
            $webSub->where('HPCOD', $customer);
        }
        //排除暫停出貨、停產
        $webSub = DB::table(DB::raw("({$this->GetSqlWithParameter($webSub)}) AS T"))
            ->select('T.HDAT1', 'T.HTOTA', 'T.HINUM', DB::raw('(I.ISOUR * T.HINUM) AS COST'))
            ->join('ITEM AS I', function ($join) {
                $join
                    ->on('I.ICODE', 'T.HICOD')
                    ->orOn('I.ICODE', 'T.HICOD');
            })
            ->join('ITEM1 AS I1', function ($join) { //排除暫停出貨的料號
                $join
                    ->on('I1.ICODE1', 'T.HICOD')
                    ->where('I1.ISTOP', 0);
            })
            ->join('ITEM3 AS I3', function ($join) { //排除產品註記廠商停產
                $join
                    ->on('I3.ICODE3', 'T.HICOD')
                    ->where('I3.IPSTOP', 0);
            });
        if (!empty($id)) { //特定料號
            $webSub->where('I.ICODE', $id);
        }
        if (!empty($type)) { //特定大類
            $webSub->where('I.ITCOD', $type);
        }
        if (!empty($type2)) { //特定中類
            $webSub->where('I1.ITCO2', $type2);
        }
        if (!empty($type3)) { //特定小類
            $webSub->where('I1.ITCO3', $type3);
        }

        $result = DB::table(DB::raw("({$this->GetSqlWithParameter($webSub)}) AS T"))
            ->select('T.HDAT1', DB::raw('SUM(T.HTOTA) AS HTOTA'), DB::raw('SUM(T.HINUM) AS HINUM'), DB::raw('SUM(T.COST) AS COST'))
            ->groupBy('T.HDAT1');


        return $result;
    }

    /**
     * 取得銷售明細
     *
     * @param string $id 銷貨單號
     * @return mix
     */
    public function GetSalesDetail(string $id)
    {
        $result = Posein::select('PCOD1', 'PCOD2', 'PJONO', 'PPCOD', 'PPNAM', 'PTOTA', 'PBAK1', 'PBAK2', 'PBAK3', 'InsideNote', 'PTXCO', 'RecieveCTCOD', 'TransportCode', 'TransportName', 'ConsignTran')
            ->with('Histin:HCOD1,HICOD,HINAM,HUNIT,HINUM,HTOTA')
            ->where('PCOD1', $id);
        return $result->first();
    }

    //取得銷貨明細
    public function GetSalesDetailCount(array $sale_no_list)
    {
        $result = Histin::select('HCOD1', 'HICOD', 'HINAM', 'HINUM')
            ->whereIn('HCOD1', $sale_no_list)
            ->whereNotIn('HYCOD', ['98', '99']);

        return $result->get();
    }

    /**
     * 取得指定區間的客戶銷售額，未含稅
     *
     * @param string $sDate 民國起日。yyy.mm.dd
     * @param string $eDate 民國訖日。yyy.mm.dd
     * @return \Illuminate\Support\Collection
     */
    public function GetSalesOfCustomer(string $sDate, string $eDate)
    {
        $result = Histin::select('HPCOD', DB::raw('SUM(HTOTA) AS HTOTA'))
            ->whereBetween('HDAT1', [$sDate, $eDate])
            ->groupBy('HPCOD');

        return $result->get();
    }

    /**
     * 取得指定區間的商品訂單銷售額
     *
     * @param string $sDate 民國起日。yyy.mm.dd
     * @param string $eDate 民國訖日。yyy.mm.dd
     * @param string $OCCOD 客代
     * @return \Illuminate\Support\Collection
     */
    public function GetOrderOfItem(string $sDate, string $eDate, string $OCCOD)
    {
        $result = DB::table('ORSUB AS D')
            ->select('OICOD', DB::raw('SUM(D.OTOTA) AS OTOTA'), DB::raw('SUM(D.OINUM*I.ISOUR) AS COST'), DB::raw('SUM(D.OINUM) AS OINUM'))
            ->join('ITEM AS I', 'I.ICODE', 'D.OICOD')
            ->whereBetween('D.ODATE', [$sDate, $eDate])
            ->where('D.OCCOD', $OCCOD)
            ->groupBy('D.OICOD');

        return $result->get();
    }


    /**
     * 取得距離最近銷貨日180天內的商品銷售額
     *
     * @param array $itemList 料號清單
     * @return \Illuminate\Support\Collection
     */
    public function GetSalesDetailOfItem(array $itemList = null)
    {
        $webSub = DB::table('HISTIN AS D')
            ->select('D.HICOD', 'D.HINUM', 'D.HDAT1', DB::raw('(SELECT MAX(HDAT1) FROM HISTIN WHERE HICOD=D.HICOD) AS RECENTLY'))
            ->where('D.HINUM', '!=', 0);

        $posSub = DB::table('PosDetailsTemp AS P')
            ->select('P.ICODE AS HICOD', 'P.Quantity AS HINUM')
            ->addSelect(DB::raw("CONVERT(VARCHAR(3),CONVERT(VARCHAR(4),P.CREATE_Date,20) - 1911) + '.' +SUBSTRING(CONVERT(VARCHAR(10),P.CREATE_Date,20),6,2) + '.' +SUBSTRING(CONVERT(VARCHAR(10),P.CREATE_Date,20),9,2) AS HDAT1"))
            ->addSelect(DB::raw("(SELECT CONVERT(VARCHAR(3),CONVERT(VARCHAR(4),MAX(CREATE_Date),20) - 1911) + '.' +SUBSTRING(CONVERT(VARCHAR(10),MAX(CREATE_Date),20),6,2) + '.' +SUBSTRING(CONVERT(VARCHAR(10),MAX(CREATE_Date),20),9,2) FROM PosDetailsTemp WHERE ICODE=P.ICODE) AS RECENTLY"))
            ->where('P.Quantity', '!=', 0);

        $result = DB::table(DB::raw("({$this->GetSqlWithParameter($webSub->unionAll($posSub))}) AS T"))
            ->select('T.HICOD', 'T.HINUM', 'T.HDAT1', 'T.RECENTLY', DB::raw("DATEDIFF(DAY,CONCAT(SUBSTRING(T.HDAT1,1,3)+1911,SUBSTRING(T.HDAT1,4,6)),CONCAT(SUBSTRING(T.RECENTLY,1,3)+1911,SUBSTRING(T.RECENTLY,4,6))) AS ELAPSED"))
            ->where(DB::raw("DATEDIFF(DAY,CONCAT(SUBSTRING(T.HDAT1,1,3)+1911,SUBSTRING(T.HDAT1,4,6)),CONCAT(SUBSTRING(T.RECENTLY,1,3)+1911,SUBSTRING(T.RECENTLY,4,6)))"), '<=', 180);
        if (!empty($itemList) && count($itemList) > 0) {
            $result->whereIn('T.HICOD', $itemList);
        }

        return $result->get();
    }

    /**
     * 取得距離最近銷貨日60天內的採購單
     *
     * @param array $itemList 料號清單
     * @return \Illuminate\Support\Collection
     */
    public function GetSalesRecentlyPurchase(array $itemList)
    {
        $salesSub = DB::table('HISTIN')
            ->select('HICOD', DB::raw('MAX(HDAT1) AS RECENTLY'))
            ->whereIn('HICOD', $itemList)
            ->groupBy('HICOD');
        $purchaseQuery = "SELECT TOP(1) S.SCOD1 FROM SISE AS S WHERE S.SICOD=T.HICOD AND DATEDIFF(DAY,CONCAT(SUBSTRING(S.SDAT1,1,3)+1911,SUBSTRING(S.SDAT1,4,6)),CONCAT(SUBSTRING(T.RECENTLY,1,3)+1911,SUBSTRING(T.RECENTLY,4,6)))<=60 ORDER BY SDAT1 DESC";
        $result = DB::table(DB::raw("({$this->GetSqlWithParameter($salesSub)}) AS T"))
            ->select('T.HICOD', DB::raw("({$purchaseQuery}) AS SCOD1"));

        return $result->get();
    }

    /**
     * 取得指定區間的客戶銷售額，含稅
     *
     * @param string $sDate 民國起日。yyy.mm.dd
     * @param string $eDate 民國訖日。yyy.mm.dd
     * @return mixed
     */
    public function GetSalesTotalTax(string $sDate, string $eDate)
    {
        $result = Posein::select('PTOTA')
            ->whereBetween('PDAT1', [$sDate, $eDate]);

        return $result->sum('PTOTA');
    }

    /**
     * 取得未有退貨的銷貨資料
     *
     * @param string $orderNo 銷貨單號
     * @return \Illuminate\Support\Collection
     */
    public function GetSalesNotReturned(string $orderNo)
    {
        $result = Posein::select('PCOD1', 'PCOD2', 'PJONO', 'PDAT1', 'PBAK1', 'TransportCode', 'PPCOD', 'PTOT1')
            ->with(['Histin' => function ($q) {
                $q->select('HCOD1', 'HICOD', 'HINAM', 'HINUM');
            }])
            ->with(['Order:OCOD1,OPERS,OTOT1,OEARN', 'Order.OrderDetail:OCOD1,OICOD,OTOTA'])
            ->with(['PoseinCheckDataLog' => function ($q) {
                $q->select('PCOD1', 'CREATE_Date')
                    ->where('CheckCount', '1');
            }])
            ->where('PCOD1', $orderNo)
            ->whereNotExists(function ($q) { //排除退貨
                $q->select(DB::raw(1))
                    ->from('POSEOU AS R')
                    ->whereRaw("(R.PCOD2 != '' AND R.PCOD2 = POSEIN.PCOD2)");
            });

        return $result->first();
    }

    /**
     * 取得新竹貨運的託運客代
     *
     * @param string $orderNo 銷貨單號
     * @return mix
     */
    public function GetHCTAccountID(string $orderNo)
    {
        $result = HCTData::select('AccountID')
            ->where('PCOD1', $orderNo)
            ->first()
            ->AccountID ?? '';

        return $result;
    }

    /**
     * 取得銷貨託運資料
     *
     * @param string $sDate 民國起日。yyymmdd
     * @param string $eDate 民國訖日。yyymmdd
     * @param bool $only_tax 只顯示有開立發票
     * @param array $delay_orders 預計出貨銷貨單
     * @param array $personal_trans_order 親送單
     * @param mixed $exclude_gpm 排除毛利檢查的品項
     * @return \Illuminate\Support\Collection
     */
    public function GetTransport(string $sDate, string $eDate, bool $only_tax = false, array $delay_orders = null, array $personal_trans_order = null, $exclude_gpm = null)
    {
        $hct_sub = "SELECT ROW_NUMBER() OVER(PARTITION BY PCOD1 ORDER BY CREATE_DATE DESC) AS ROWNUM, PCOD1, Quantity, Weight FROM HCT_Data WHERE AccountID='05172170002'";
        $subQuery = 'SELECT TOP(1) F.FNum FROM FreightNote AS F WHERE F.PCOD1=P.PCOD1 ORDER BY F.CREATE_DATE DESC';
        $result = DB::table('POSEIN AS P')
            ->select('P.TransportCode', 'P.TransportName', 'P.PCOD1', 'P.PPCOD', 'P.PPNAM', 'P.ConsignTran', 'P.PTOT1', 'P.PJONO', 'P.PDAT1')
            ->addSelect('P.PCOD2', 'P.PCMAN', 'P.PTELE', 'P.PBAK1', 'P.PBAK2', 'P.PBAK3', 'P.InsideNote', 'P.RecieveCTCOD', DB::raw("({$subQuery}) AS FNum"))
            ->addSelect('O.OPERS', 'O.OTOT1', 'O.OINYN', 'O.OEARN')
            ->addSelect(DB::raw("(SELECT TOP(1) 1 FROM HISTIN AS D JOIN ITEM AS I ON I.ICODE=D.HICOD WHERE D.HCOD1=P.PCOD1 AND I.ITCOD='11') AS HEAVY"))
            ->addSelect(DB::raw("(SELECT TOP(1) 1 FROM HISTIN AS D WHERE D.HCOD1=P.PCOD1 AND D.HICOD IN ({$this->GetSqlInParam($exclude_gpm)})) AS ExcludeGPM"))
            ->addSelect(DB::raw("(SELECT TOP(1) OD.OTOTA FROM ORSUB AS OD WHERE OD.OCOD1=O.OCOD1 AND OD.OICOD='XX126A') AS web_discount"))
            ->addSelect('H.Quantity', 'H.Weight')
            ->join('ORDET AS O', 'O.OCOD1', 'P.PCOD2')
            ->leftJoinSub($hct_sub, 'H', function ($q) {
                $q
                    ->on('H.PCOD1', 'P.PCOD1')
                    ->where('H.ROWNUM', 1);
            })
            ->whereBetween('P.PDAT1', [$sDate, $eDate])
            ->where('P.PDAT1', '>=', '110.11.26') //只取上車檢驗功能上線後
            ->whereNotExists(function ($q) { //排除退貨
                $q->select(DB::raw(1))
                    ->from('POSEOU AS R')
                    ->whereRaw("(R.PCOD2 != '' AND R.PCOD2 = P.PCOD2)");
            })
            ->whereNotIn('PPCOD', ['x001', 'x002', 'F001']) //排除指定客代
            ->where('P.TransportCode', '!=', 'COM')
            ->orderBy('P.TransportCode')
            ->orderBy('P.PCOD1');
        if ($only_tax) {
            $result->where('P.PTXCO', '!=', '');
        }
        if (!empty($delay_orders) && count($delay_orders) > 0) {
            $result->orWhere(function ($q) use ($delay_orders, $only_tax) {
                $q->whereIn('P.PCOD1', $delay_orders);
                if ($only_tax) {
                    $q->where('P.PTXCO', '!=', '');
                }
            });
        }
        //親送
        if (!empty($personal_trans_order) && count($personal_trans_order) > 0) {
            $result->orWhere(function ($q) use ($personal_trans_order, $only_tax) {
                $q->whereIn('P.PJONO', $personal_trans_order);
                if ($only_tax) {
                    $q->where('P.PTXCO', '!=', '');
                }
            });
        }

        return $result->get();
    }

    /**
     * 取得指定時間的銷貨單號住址
     *
     * @param string $sDate 民國起日。yyy.mm.dd
     * @param string $eDate 民國訖日。yyy.mm.dd
     * @return \Illuminate\Support\Collection
     */
    public function GetTransportSuggest(string $sDate, string $eDate)
    {
        $result = Posein::with('Histin:HCOD1,HICOD', 'OriginalOrder:order_no,receiver_address')
            ->select('PCOD1', 'PJONO', 'PPNAM', 'PBAK3', 'TransportName')
            ->whereBetween('PDAT1', [$sDate, $eDate])
            ->whereIn('PPCOD', ['S001', 'S003', 'S004', 'S005', 'R001', 'I001', 'M001', 'M002', 'Y001', 'S002', 'S006', 'C007', 'c003', 'D007'])
            ->whereNotIn('TransportCode', array_merge(BaseService::shop_trans, ['MF', 'COM', 'SC'])) //排除原物流為 五大超商 超取、廠商直季
            ->where('ConsignTran', '')
            ->where(function ($q) {
                $q
                    ->where('PBAK1', 'Not LIKE', '%棧板%')
                    ->where('PBAK2', 'Not LIKE', '%棧板%')
                    ->where('PBAK3', 'Not LIKE', '%棧板%')
                    ->where('InsideNote', 'Not LIKE', '%棧板%');
            });

        return $result->get();
    }

    /**
     * 取得指定時間、新竹物流的銷貨單號判斷偏遠地區
     *
     * @param string $sDate 民國起日。yyy.mm.dd
     * @param string $eDate 民國訖日。yyy.mm.dd
     * @return \Illuminate\Support\Collection
     */
    public function GetHctRemote(string $sDate, string $eDate)
    {
        $result = Order::select('OCOD1', 'OCOD4', 'OCNAM', 'OBAK3')
            ->with(['Posein:PCOD2,TransportCode', 'OrderSample:order_no,receiver_address'])
            ->where('OCANC', '!=', '訂單取消')
            ->whereBetween('ODATE', [$sDate, $eDate])
            ->whereNotIn('OCCOD', ['C005', 'C007'])
            ->where(function ($q) {
                $q->where('TransportCode', 'HCT')
                    ->orWhereHas('Posein', function ($q2) {
                        $q2
                            ->select('PCOD2', 'TransportCode')
                            ->where('TransportCode', 'HCT');
                    });
            })
            ->orderBy('OCOD1');

        return $result->get();
    }

    /**
     * 取得指定日期的音速物流總表
     *
     * @param string $date
     * @return \Illuminate\Support\Collection
     */
    public function GetTransportYCS(string $date)
    {
        $result = YcsTransport::with('shipment')->where('settle_date', $date)->get();
        $sales_db = Posein::select('PCOD1', 'PJONO', 'ConsignTran')->whereIn('PCOD1', $result->pluck('sale_no')->toArray())->get();
        foreach ($sales_db as $sd) {
            $sale = $result->firstWhere('sale_no', $sd->PCOD1);
            $sale->tran_no = $sd->ConsignTran;
            $sale->order_no = $sd->PJONO;
        }

        return $result;
    }

    /**
     * 取得指定區間的總件數
     *
     * @param string $sDate 起日
     * @param string $eDate 訖日
     * @return int
     */
    public function GetTransportYCSTotal(string $sDate, string $eDate)
    {
        $result = YcsTransport::whereBetween('settle_date', [$sDate, $eDate])->sum('qty');
        return $result;
    }

    /**
     * 取得土方的訂單
     *
     * @param string $sDate 民國起日。yyy.mm.dd
     * @param string $eDate 民國訖日。yyy.mm.dd
     * @param array $items 關注的商品
     * @return \Illuminate\Support\Collection
     */
    public function GetHeavyOrder(string $sDate, string $eDate, array $items)
    {
        $result = OrderDetail::select('OCOD1', 'OICOD', 'OINAM', 'OINUM')
            ->withWhereHas('Main', function ($q) {
                $q
                    ->select('OCOD1', 'OCOD4', 'TransportCode')
                    ->whereIn('TransportCode', BaseService::shop_trans) //超取
                    ->where('OCANC', '!=', '訂單取消');
            })
            ->doesnthave('Poseou')
            ->with(['Posein' => function ($q) {
                $q->select('PCOD1', 'PCOD2');
            }])
            ->whereBetween('ODATE', [$sDate, $eDate])
            ->whereIn('OICOD', $items)
            ->whereNotIn('OCCOD', self::excludeedCustomers)
            ->orderBy('OCOD1');

        return $result->get();
    }

    /**
     * 取得訂單的備註
     *
     * @param string $sDate 民國起日。yyy.mm.dd
     * @param string $eDate 民國訖日。yyy.mm.dd
     * @return \Illuminate\Support\Collection
     */
    public function GetOrderTaxRemark(string $sDate, string $eDate)
    {
        $result = Order::select('OCOD1', 'OCOD4', 'OCNAM', 'OBAK1', 'OBAK2', 'OBAK3')
            ->with(['Sinvo' => function ($q) {
                $q->select('Invono', 'Ctcod');
            }])
            ->with(['Posein' => function ($q) {
                $q->select('PCOD1', 'PCOD2', 'PTXCO', 'UseExtendInvoiceInfo', 'RecieveCTCOD');
            }])
            ->whereBetween('ODATE', [$sDate, $eDate])
            ->where(function ($query) { //有備註資料
                $query
                    ->where('OBAK1', '!=', '')
                    ->orWhere('OBAK2', '!=', '')
                    ->orWhere('OBAK3', '!=', '');
            })
            ->whereNotIn('OCCOD', self::excludeedCustomers)
            ->whereNotIn('OCCOD', ['S001', 'S003', 'S004', 'S005'])
            ->orderByDesc('OCOD1');

        return $result->get();
    }

    /**
     * 取得訂單詳細資料
     *
     * @param string $id 訂單單號
     * @return mix
     */
    public function GetOrderDetail(string $id)
    {
        $result = Order::select('OCOD1', 'OCOD4', 'OCANC', 'OCNAM', 'OCCOD', 'OTOT2', 'OBAK1', 'OBAK2', 'OBAK3', 'UseExtendInvoiceInfo', 'RecieveCTCOD', 'InsideNote', 'TransportCode', 'TransportName')
            ->withWhereHas('OrderDetail.STOC', function ($q) {
                $q->select('SITEM', 'SNUMB')
                    ->where('STCOD', 'A001');
            })
            //withwherehas bug 無法限制中間層欄位，須將code放在之後
            ->with(['OrderDetail' => function ($q) {
                $q->select('OCOD1', 'OICOD', 'OINAM', 'OUNIT', 'OINUM', 'OYNO', 'ONNO', 'OTOTA', 'OINYN');
            }])
            ->where('OCOD1', $id);

        return $result->first();
    }

    //取得LVT數量
    public function GetLvtCount($sales)
    {
        $result = Histin::select('HCOD1', DB::raw('SUM(HINUM) AS qty'))
            ->whereHas('ITEM1', function ($q) {
                $q->where('ITCO3', '046');
            })
            ->whereIn('HCOD1', $sales)
            ->groupBy('HCOD1');

        return $result->get();
    }

    /**
     * 紀錄特殊訂單匯出紀錄
     *
     * @param string $sDate 起日
     * @param string $eDate 訖日
     * @param mixed $src 平台來源
     * @param string $type 匯出類型
     * @param string $order_type 訂單類型
     * @param string $file 匯出內容JSON
     * @return void
     */
    public function CreateOrderSampleExportLog(string $sDate, string $eDate, $src, string $type, string $file)
    {
        OrderSampleFile::create([
            'start_date' => $sDate,
            'end_date' => $eDate,
            'src' => $src,
            'convert_type' => $type,
            'convert_file' => $file,
        ]);
    }

    /**
     * 取得指定訂單單號，銷貨單內有放贈品的訂單單號
     *
     * @param array $idList 訂單單號清單
     * @return array
     */
    public function HasBulb(array $idList)
    {
        if (empty($idList)) {
            return array();
        }
        $result = Posein::select('PCOD2')
            ->whereHas('Histin', function ($q) {
                $q->whereIn('HICOD', self::bulbCode);
            })
            ->whereIn('PCOD2', $idList)
            ->distinct();

        return $result->pluck('PCOD2')->toArray();
    }

    /**
     * 取得地板產品
     *
     * @return \Illuminate\Support\Collection
     */
    public function GetFloorItem()
    {
        $result = Item::select('ICODE', 'INAME')
            ->whereHas('ITEM1', function ($q) {
                $q->whereIn('ITCO3', ['014', '046']);
            })
            ->where('ITCOD', '11');

        return $result->get();
    }

    /**
     * 取得商品總表
     *
     * @return \Illuminate\Support\Collection
     */
    public function GetItemSummary($show_hidden)
    {
        $result = DB::table('ITEM AS I')
            ->select('I.ICODE AS item_no', 'I.INAME AS item_name', 'I.ITCOD AS kind', 'I.ITNAM AS kind_name', 'I.IPCOD AS supply', 'I.IPNAM AS supply_name', 'I.ISOUR AS erp_cost')
            ->addSelect('I1.ENA13', 'I1.CO128', 'I1.ICOD2', 'I1.ITCO2 AS kind2', 'I1.ITNA2 AS kind2_name', 'I1.ITCO3 AS kind3', 'I1.ITNA3 AS kind3_name', 'I1.IWEIGHT', 'I1.INSTOP', 'I1.ISTOP', 'I1.STOPSALE')
            ->addSelect('I3.IPSTOP')
            ->addSelect('I4.T11_11 AS country', 'I4.PDepth', 'I4.PWidth', 'I4.PHeight', 'I4.Box_Weight', 'I4.Box_Depth', 'I4.Box_Width', 'I4.Box_Height', 'I4.Box_CUFT', 'I4.Box_CBM')
            ->join('ITEM1 AS I1', 'I1.ICODE1', 'I.ICODE')
            ->join('ITEM3 AS I3', 'I3.ICODE3', 'I.ICODE')
            ->join('ITEM4 AS I4', 'I4.ICODE4', 'I.ICODE')
            ->orderBy('I.ICODE');
        if (!$show_hidden) {
            $result->where('I1.INSTOP', '0')
                ->orWhere('I1.ISTOP', '0')
                ->orWhere('I1.STOPSALE', '0');
        }

        return $result->get();
    }

    /**
     * 根據大中小類找尋相關料號
     *
     * @param string $kind 大類
     * @param string $kind2 中類
     * @param string $kind3 小類
     * @return array
     */
    public function GetItemNoList(string $kind = null, string $kind2 = null, string $kind3 = null)
    {
        $result = ITEM::select('ICODE');
        if (!empty($kind)) {
            $result->where('ITCOD', $kind);
        }
        if (!empty($kind2) || !empty($kind3)) {
            $result->whereHas('ITEM1', function ($q) use ($kind2, $kind3) {
                if (!empty($kind2)) {
                    $q->where('ITCO2', $kind2);
                }
                if (!empty($kind3)) {
                    $q->where('ITCO3', $kind3);
                }
            });
        }

        return $result->pluck('ICODE')->toArray();
    }

    /**
     * 取得產品履歷
     *
     * @param string $tw_sDate 民國起日。yyy.mm.dd
     * @param string $tw_eDate 民國訖日。yyy.mm.dd
     * @param string $sDate 西元起日。yyyy.mm.dd
     * @param string $eDate 西元訖日。yyyy.mm.dd
     * @param string $id 產品料號
     * @return \Illuminate\Support\Collection
     */
    public function GetItemLog(string $tw_sDate, string $tw_eDate, string $sDate, string $eDate, string $id)
    {
        //進貨
        $purchase = DB::table('VISEIN AS D')
            ->select('D.VDAT1 AS Date', 'D.VTIME AS Time', DB::raw("'進貨' AS Type"), 'D.VCOD1 AS Id', DB::raw("'' AS Cust"), 'D.VTCOD AS StockCode', 'D.VTNAM AS Stock', 'D.VINUM AS Num')
            ->addSelect('M.BUSER AS UserName')
            ->leftJoin('BUYSIN AS M', 'M.BCOD1', 'D.VCOD1')
            ->whereBetween('D.VDAT1', [$tw_sDate, $tw_eDate])
            ->where('D.VICOD', $id);
        //進貨退回
        $rPurchase = DB::table('VISEOU AS D')
            ->select('D.VDAT1 AS Date', 'D.VTIME AS Time', DB::raw("'進貨退回' AS Type"), 'D.VCOD1 AS Id', DB::raw("'' AS Cust"), 'D.VTCOD AS StockCode', 'D.VTNAM AS Stock', 'D.VINUM AS Num')
            ->addSelect('M.BUSER AS UserName')
            ->leftJoin('BUYSOU AS M', 'M.BCOD1', 'D.VCOD1')
            ->whereBetween('D.VDAT1', [$tw_sDate, $tw_eDate])
            ->where('D.VICOD', $id);
        //銷貨
        $sales = DB::table('HISTIN AS D')
            ->select('D.HDAT1 AS Date', 'D.HTIME AS Time', DB::raw("'銷貨' AS Type"), 'D.HCOD1 AS Id', 'D.HPNAM AS Cust', 'D.HTCOD AS StockCode', 'D.HTNAM AS Stock', 'D.HINUM AS Num')
            ->addSelect('M.PUSER AS UserName')
            ->leftJoin('POSEIN AS M', 'M.PCOD1', 'D.HCOD1')
            ->whereBetween('D.HDAT1', [$tw_sDate, $tw_eDate])
            ->where('D.HICOD', $id);
        //銷貨退回
        $rSales = DB::table('HISTOU AS D')
            ->select('D.HDAT1 AS Date', 'D.HTIME AS Time', DB::raw("'銷貨退回' AS Type"), 'D.HCOD1 AS Id', 'D.HPNAM AS Cust', 'D.HTCOD AS StockCode', 'D.HTNAM AS Stock', 'D.HINUM AS Num')
            ->addSelect('M.PUSER AS UserName')
            ->leftJoin('POSEOU AS M', 'M.PCOD1', 'D.HCOD1')
            ->whereBetween('D.HDAT1', [$tw_sDate, $tw_eDate])
            ->where('D.HICOD', $id);
        //POS銷貨
        $pos_sales = DB::table('PosDetailsTemp AS D')
            ->select(DB::raw("CONVERT(VARCHAR(3),CONVERT(VARCHAR(4),D.CREATE_Date,20) - 1911) + '.' +SUBSTRING(CONVERT(VARCHAR(10),D.CREATE_Date,20),6,2) + '.' +SUBSTRING(CONVERT(VARCHAR(10),D.CREATE_Date,20),9,2)"))
            ->addSelect('D.CREATE_Date AS Date', DB::raw("'POS銷貨' AS Type"), 'M.RCOD1 AS Id', DB::raw("'' AS Cust"), DB::raw("'A001' AS StockCode"), DB::raw("'出貨倉' AS Stock"), 'D.Quantity AS Num')
            ->addSelect('M.UserName AS UserName')
            ->leftJoin('PosMainTemp AS M', 'M.MainID', 'D.MainID')
            ->whereBetween('D.CREATE_Date', [$sDate, $eDate])
            ->where('D.ICODE', $id);
        //POS銷貨退回
        $rPos_sales = DB::table('PosReturnDetailsTemp AS D')
            ->select(DB::raw("CONVERT(VARCHAR(3),CONVERT(VARCHAR(4),D.CREATE_Date,20) - 1911) + '.' +SUBSTRING(CONVERT(VARCHAR(10),D.CREATE_Date,20),6,2) + '.' +SUBSTRING(CONVERT(VARCHAR(10),D.CREATE_Date,20),9,2)"))
            ->addSelect('D.CREATE_Date AS Date', DB::raw("'POS銷貨退回' AS Type"), DB::raw("(SELECT TOP(1) RT.RCOD3 FROM RETAIL AS RT WHERE RT.RCOD1=D.RCOD1 AND RCOD3 != '') AS Id"), DB::raw("'' AS Cust"), DB::raw("'A001' AS StockCode"), DB::raw("'出貨倉' AS Stock"), 'D.ReturnQuantity AS Num')
            ->addSelect('M.UserName AS UserName')
            ->leftJoin('PosReturnMainTemp AS M', 'M.ReturnMainID', 'D.ReturnMainID')
            ->whereBetween('D.CREATE_Date', [$sDate, $eDate])
            ->where('D.ICODE', $id);
        //調撥
        $tran = DB::table('TRAN1 AS T')
            ->select('T.TDATE AS Date', 'T.TTIME AS Time', DB::raw("'調撥' AS Type"), 'T.TCCOD AS Id', DB::raw("'' AS Cust"), 'T.TSCO2 AS StockCode', DB::raw("(I1.ITNAM+'→'+I2.ITNAM) AS Stock"), 'T.TNUMB AS Num')
            ->addSelect('M.MUSER AS UserName')
            ->leftJoin('MTRAN AS M', 'M.MCODE', 'T.TCCOD')
            ->leftJoin('ITYP AS I1', 'I1.ITCOD', 'T.TSCO1')
            ->leftJoin('ITYP AS I2', 'I2.ITCOD', 'T.TSCO2')
            ->whereBetween('T.TDATE', [$tw_sDate, $tw_eDate])
            ->where('T.TICOD', $id);
        //盤點盈虧
        $inventory = DB::table('PDYM AS D')
            ->select('D.PDATE AS Date', 'D.PTIME AS Time', 'D.PTNAM AS Type', 'D.PCODE AS Id', DB::raw("'' AS Cust"), 'D.PSCOD AS StockCode', 'D.PSNAM AS Stock', 'D.PNUMB AS Num')
            ->addSelect('M.DNAME AS UserName')
            ->leftJoin('DAYM AS M', 'M.DCODE', 'D.PCODE')
            ->whereBetween('D.PDATE', [$tw_sDate, $tw_eDate])
            ->where('D.PICOD', $id);

        $result = $purchase
            ->unionAll($rPurchase)
            ->unionAll($sales)
            ->unionAll($rSales)
            ->unionAll($pos_sales)
            ->unionAll($rPos_sales)
            ->unionAll($tran)
            ->unionAll($inventory)
            ->orderBy('Time');

        return $result->get();
    }

    /**
     * 取得產品庫存紀錄
     *
     * @param string $sDate 西元起日。yyyy-mm-dd hh:mm:ss
     * @param string $eDate 西元訖日。yyyy-mm-dd hh:mm:ss
     * @param string $id 產品料號
     * @return \Illuminate\Support\Collection
     */
    public function GetItemHistory(string $sDate, string $eDate, string $id)
    {
        try {
            $result = DB::connection('mysql')
                ->table('log_item')
                ->select('ICODE', 'INUMB', DB::raw('DATE(created_at) AS created_at'))
                ->where('ICODE', $id)
                ->whereBetween('created_at', [$sDate, $eDate])
                ->orderBy('ICODE')
                ->orderBy('created_at')
                ->distinct('ICODE', 'created_at');

            return $result->get();
        } catch (Exception $ex) {
            return collect();
        }
    }

    /**
     * 取得所有產品料號
     *
     * @param bool $includeStop 是否包含停用
     * @return \Illuminate\Support\Collection
     */
    public function GetItemAll(bool $includeStop = false)
    {
        $result = Item::select('ICODE', 'INAME', 'IUNIT', 'ISOUR')
            ->orderBy('ICODE')
            ->distinct();
        if (!$includeStop) {
            $result->whereHas('ITEM1', function ($q) {
                $q->where('ISTOP', 0);
            })->whereHas('ITEM3', function ($q) {
                $q->where('IPSTOP', 0);
            });
        }
        return $result->get();
    }

    /**
     * 取得廣告花費
     *
     * @param string $sDate yyyy/mm/dd
     * @param string $eDate yyyy/mm/dd
     * @return \Illuminate\Support\Collection
     */
    public function GetAdCost(string $sDate, string $eDate)
    {
        return AdCost::with('Channel')->whereBetween('src_date', [$sDate, $eDate])->get();
    }

    /**
     * 取得參數
     *
     * @param string $kind
     * @return \Illuminate\Support\Collection
     */
    public function GetConfig(string $kind)
    {
        try {
            $result = DB::connection('mysql')
                ->table('sys_config')
                ->select('config')
                ->where('enabled', 'Y')
                ->where('kind', $kind);

            return $result->get();
        } catch (Exception $ex) {
            return collect();
        }
    }

    /**
     * 取得客戶清單
     *
     * @return \Illuminate\Support\Collection
     */
    public function GetCustomer()
    {
        $result = CUST::select('CCODE', 'CNAM2')
            ->where('PAUSE', '0') //排除暫停出貨
            ->distinct();

        return $result->get();
    }

    /**
     * 取得銷貨員清單
     *
     * @return \Illuminate\Support\Collection
     */
    public function GetSalesman()
    {
        $result = SALE::select('SCODE', 'SNAME')->distinct();
        return $result->get();
    }

    /**
     * 取得指定日期的中國商品銷售
     *
     * @return \Illuminate\Support\Collection
     */
    public function GetChinaSales()
    {
        $year = (new DateTime)->format('Y');
        $webSub = DB::table('ITEM4 AS I4')
            ->select('I.ITNAM', 'I1.ITNA3', 'I4.ICODE4', 'I.INAME', 'I.INUMB', DB::raw('SUBSTRING(H.HDAT1,1,6) AS DT'), 'H.HINUM AS NUM')
            ->addSelect(DB::raw("(SELECT SUM(SINUM)-SUM(SONUM) FROM SISE AS S WHERE S.SICOD=I4.ICODE4 AND SINYN='N') AS purchase_undone"))
            ->join('ITEM AS I', 'I.ICODE', 'I4.ICODE4')
            ->join('ITEM1 AS I1', 'I1.ICODE1', 'I4.ICODE4')
            ->leftJoin('HISTIN AS H', 'H.HICOD', 'I4.ICODE4')
            ->where('I.ICONS', 'N')
            ->where('I4.T11_11', 'china')
            ->where(function ($q) use ($year) {
                $roc = $year - 1911;
                $q->whereBetween('HDAT1', [$roc . '.01.01', $roc . '.12.31'])
                    ->orWhereBetween('HDAT1', [($roc - 1) . '.01.01', ($roc - 1) . '.12.31']);
            });
        $posSub = DB::table('ITEM4 AS I4')
            ->select('I.ITNAM', 'I1.ITNA3', 'I4.ICODE4', 'I.INAME', 'I.INUMB', DB::raw("CAST(YEAR(CREATE_Date)-1911 AS nvarchar(3))+'.'+SUBSTRING(CONVERT(varchar,CREATE_Date,101),1,2) AS DT"), 'Quantity AS NUM')
            ->addSelect(DB::raw("(SELECT SUM(SINUM)-SUM(SONUM) FROM SISE AS S WHERE S.SICOD=I4.ICODE4 AND SINYN='N') AS purchase_undone"))
            ->join('ITEM AS I', 'I.ICODE', 'I4.ICODE4')
            ->join('ITEM1 AS I1', 'I1.ICODE1', 'I4.ICODE4')
            ->leftJoin('PosDetailsTemp AS P', 'P.ICODE', 'I4.ICODE4')
            ->where('I.ICONS', 'N')
            ->where('I4.T11_11', 'china')
            ->where(function ($q) use ($year) {
                $q->whereBetween('CREATE_Date', [$year . '-01-01', $year . '-12-31'])
                    ->orWhereBetween('CREATE_Date', [($year - 1) . '-01-01', ($year - 1) . '-12-31']);
            });
        $webSub->unionAll($posSub);

        return $webSub->get();
    }

    /**
     * 取得保固資料
     *
     * @param string|null $sale_no 銷貨單號
     * @param string|null $original_no 原始訂單編號
     * @param string|null $user_id 買家ID
     * @param string|null $start_date 銷售起日
     * @param string|null $end_date 銷售訖日
     * @param string|null $customer 客戶代碼
     * @return \Illuminate\Support\Collection
     */
    public function GetOrderWarranty($sale_no = null, $original_no = null, $user_id = null, $start_date = null, $end_date = null, $customer = null)
    {
        $result = DB::connection('aws')->table('order_m');
        if (!empty($sale_no)) {
            $result->where('id', $sale_no);
        } else {
            if (!empty($original_no)) {
                $result->where('original_no', $original_no);
            }
            if (!empty($user_id)) {
                $result->where('user_id', $user_id);
            }
            if (!empty($start_date)) {
                $result->where('sale_date', '>=', $start_date);
            }
            if (!empty($end_date)) {
                $result->where('sale_date', '<=', $end_date);
            }
            if (!empty($customer)) {
                $result->where('sale_src', $customer);
            }
        }

        return $result->get();
    }

    /**
     * 取得API蝦皮商品資訊
     *
     * @param string $shop_id 賣場ID
     * @param string $item_id 商品ID
     * @return \Illuminate\Support\Collection
     */
    public function GetShopeeProduct($shop_id = null, $item_id = null)
    {
        $result = ApiShopeeProduct::select('ShopId', 'item_id', 'ICODE')
            ->whereIn('status', ['NORMAL', 'UNLIST']);
        $shop2 = AdItem::select('ShopId', 'item_id', 'ICODE');
        if (!empty($shop_id)) {
            $result->where('ShopId', $shop_id);
            $shop2->where('ShopId', $shop_id);
        }
        if (!empty($item_id)) {
            $result->where('item_id', $item_id);
            $shop2->where('item_id', $item_id);
        }

        return $result->get()->concat($shop2->get());
    }

    /**
     * 取得POS保固資料
     *
     * @param string|null $sale_no 結帳單號
     * @param string|null $start_date 銷售起日
     * @param string|null $end_date 銷售訖日
     * @return \Illuminate\Support\Collection
     */
    public function GetPosOrderWarranty($sale_no = null, $start_date = null, $end_date = null)
    {
        $result = DB::connection('aws')->table('pos_order_m');
        if (!empty($sale_no)) {
            $result->where('id', $sale_no);
        } else {
            if (!empty($start_date)) {
                $result->where('sale_date', '>=', $start_date);
            }
            if (!empty($end_date)) {
                $result->where('sale_date', '<=', $end_date);
            }
        }

        return $result->get();
    }

    /**
     * 各代碼轉換成TMS產品代號
     *
     * @param string $id 產品代號/EAN13碼/Code128/外部碼
     * @return string
     */
    public function ConvertToItemNo($id)
    {
        if (empty($id)) {
            return '';
        }
        $result = ITEM1::where('ICODE1', $id) //產品代號
            ->orWhere('ENA13', $id) //EAN13碼
            ->orWhere('CO128', $id) //Code128
            ->orWhere('ICOD2', $id) //外部碼
            ->value('ICODE1');

        return $result;
    }

    /**
     * 取得所有電子郵件
     *
     * @return \Illuminate\Support\Collection
     */
    public function GetUsersEmail()
    {
        return User::select('name', 'email')->get();
    }

    //取得各大類銷售排序
    public function GetTopSale($s_date, $e_date, $tw_s_date, $tw_e_date)
    {
        $pos_sale = DB::table('PosDetailsTemp')
            ->select('ICODE', 'Quantity')
            ->whereBetween('CREATE_Date', [$s_date->format('Y-m-d'), $e_date->format('Y-m-d')])
            ->whereNotNull('Quantity');
        $sale = DB::table('HISTIN')
            ->select('HICOD', 'HINUM')
            ->whereBetween('HDAT1', [$tw_s_date, $tw_e_date])
            ->whereNotNull('HINUM')
            ->unionAll($pos_sale);
        $sale_group = DB::table(DB::raw("({$this->GetSqlWithParameter($sale)}) AS T"))
            ->select('T.HICOD', DB::raw('SUM(T.HINUM) AS HINUM'))
            ->groupBy('T.HICOD');
        $result = DB::table(DB::raw("({$this->GetSqlWithParameter($sale_group)}) AS T"))
            ->select(DB::raw('ROW_NUMBER() OVER(PARTITION BY I.ITCOD ORDER BY T.HINUM DESC) AS ROWNUM'))
            ->addSelect('I.ITCOD', 'I.ITNAM', 'I.ICODE', 'I.INAME', 'T.HINUM')
            ->join('ITEM AS I', 'I.ICODE', 'T.HICOD')
            ->whereNotIn('I.ITCOD', ['98', '99']);

        return $result->get();
    }

    /**
     * 取得各平台指定商品銷售
     *
     * @param string $s_date
     * @param string $e_date
     * @param string $tw_s_date
     * @param string $tw_e_date
     * @param array $select_items
     * @return \Illuminate\Support\Collection
     */
    public function GetCustItem($s_date, $e_date, $tw_s_date, $tw_e_date, $select_items)
    {
        $pos = DB::table('PosDetailsTemp')
            ->select(DB::raw("CAST(YEAR(CREATE_Date)-1911 AS nvarchar(3))+'.'+SUBSTRING(CONVERT(varchar,CREATE_Date,101),1,2) AS DT"), DB::raw("'POS' AS PPNAM"), 'Quantity AS qty', 'SaleMoneyTotal AS sales_total')
            ->whereIn('ICODE', $select_items)
            ->whereBetween('CREATE_Date', [$s_date, $e_date]);
        $result = DB::table('POSEIN AS M')
            ->select(DB::raw("SUBSTRING(M.PDAT1,1,6) AS DT"), 'M.PPNAM', 'D.HINUM AS qty', 'D.HTOTA AS sales_total')
            ->join('HISTIN AS D', 'D.HCOD1', 'M.PCOD1')
            ->whereIn('D.HICOD', $select_items)
            ->whereBetween('M.PDAT1', [$tw_s_date, $tw_e_date])
            ->unionAll($pos);

        return $result->get();
    }

    /**
     * 取得帶參數的SQL語法
     *
     * @param mix $builder
     * @return string
     */
    public function GetSqlWithParameter($builder)
    {
        if (empty($builder)) {
            return '';
        }
        $bindings = $builder->getBindings();
        $sql = str_replace('?', "'%s'", $builder->toSql());
        return sprintf($sql, ...$bindings);
    }

    /**
     * 將民國日期格式轉成西元Y-M-D
     *
     * @param string $twDate 民國日期格式。YYY.MM.DD
     * @return DateTime
     */
    private function ConvertTWToAD(string $twDate)
    {
        try {
            $tmp = explode('.', $twDate);
            $y = $tmp[0] + 1911;
            $m = $tmp[1];
            $d = $tmp[2];
            return "{$y}-{$m}-{$d}";
        } catch (\Throwable $th) {
            return null;
        }
    }

    /**
     * 參數轉SQL IN格式
     *
     * @param mix $data
     * @return string
     */
    private function GetSqlInParam($data)
    {
        $result = '';
        foreach ($data as $d) {
            if (!empty($result)) {
                $result .= ',';
            }
            $result .= "'{$d}'";
        }
        return $result;
    }
}
