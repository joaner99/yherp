<?php

namespace App\Imports;

use Exception;
use App\Models\yherp\AdItem;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class AdItemImport extends BaseImport implements ToCollection
{
    public $msg = '';
    private $file_name = '';

    public function __construct($file_name)
    {
        $this->file_name = $file_name;
    }

    public function collection(Collection $rows)
    {
        $this->msg = '';
        try {
            $ShopId = $this->GetShopID();
            if (empty($ShopId)) {
                $this->msg = '商品資訊匯入錯誤。原因:無法解析賣場ID';
                return;
            } else {
                AdItem::where('ShopId', $ShopId)->delete();
                foreach ($rows->slice(4) as $row) {
                    $item_id = $row[0];
                    if (!empty($row[6])) { //先取商品選項貨號
                        $ICODE = $row[6];
                    } elseif (!empty($row[4])) { //再取主商品貨號
                        $ICODE = $row[4];
                    } else {
                        continue;
                    }
                    //驗證TMS料號，數字、長度<=16
                    if (!is_numeric($ICODE) || strlen($ICODE) > 16) {
                        continue;
                    }

                    AdItem::create([
                        'ShopId' => $ShopId,
                        'item_id' => $item_id,
                        'ICODE' => $ICODE,
                    ]);
                }
            }
        } catch (Exception $ex) {
            $this->msg = '商品資訊匯入錯誤。原因:' . $ex->getMessage();
        }
    }

    /**
     * 取得賣場ID
     *
     * @return string
     */
    private function GetShopID()
    {
        if (empty($this->file_name)) {
            return '';
        } else {
            return explode('_', $this->file_name)[4] ?? '';
        }
    }
}
