<?php

namespace App\Imports;

use Exception;
use App\Models\yherp\SysConfig;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\ToCollection;

class OrderColumnsImport implements ToCollection
{
    public $msg = '';
    protected $type;

    public function __construct($type)
    {
        $this->type = $type;
    }

    public function collection(Collection $rows)
    {
        $this->msg = '';
        DB::connection('mysql')->beginTransaction();
        try {
            $this->ExcludeNull($rows);
            $header = $rows->first()->toArray();
            $this->UpdateColumns($header, $this->type);
            DB::connection('mysql')->commit();
        } catch (Exception $ex) {
            DB::connection('mysql')->rollBack();
            $this->msg = '更新網路訂單欄位發生無法預期的錯誤。原因:' . $ex->getMessage();
        }
    }

    /**
     * 排除標頭為Null的資料
     *
     * @param Collection $rows
     * @return void
     */
    private function ExcludeNull(Collection &$rows)
    {
        if (!empty($rows)) {
            //取得Null的欄位索引
            $nullColumns = [];
            $header = $rows->first()->toArray();
            foreach ($header as $key => $value) {
                if ($value == null) {
                    $nullColumns[] = $key;
                }
            }
            //有空白的欄位
            if (!empty($nullColumns)) {
                foreach ($rows as $key => $row) {
                    //清空該欄位的每一列資料
                    foreach ($nullColumns as $index) {
                        $row->forget($index);
                    }
                    //reset index
                    $rows[$key] = $row->values();
                }
            }
        }
    }

    /**
     * 更新訂單匯出欄位設定
     *
     * @param array $header
     * @return void
     */
    private function UpdateColumns(array $header, $type)
    {
        $db = SysConfig::where('kind', 'order_convert_columns')->first();
        $config = json_decode($db->config);
        switch ($type) {
            case 1: //蝦皮
                $config[0] = $header; //蝦皮不分店(棄用)
                $config[2] = $header; //蝦皮1店
                $config[3] = $header; //蝦皮2店
                $config[4] = $header; //蝦皮3店
                break;
            case 2: //Shopline
                $config[1] = $header; //Shopline
            case 3: //BV SHOP
                $config[5] = $header; //BV SHOP
                break;
            default:
                break;
        }
        $db->config = json_encode($config);
        $db->save();
    }
}
