<?php

namespace App\Imports;

use Exception;
use App\Models\yherp\Freight;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\ToArray;

class FreightImport extends BaseImport implements ToArray
{
    public $msg = '';
    private $type = null;

    public function __construct($type)
    {
        $this->type = $type;
    }

    public function array(array $rows)
    {
        $this->msg = '';
        DB::connection('mysql')->beginTransaction();
        try {
            switch ($this->type) {
                case 'HCT':
                    $columns = $this->GetColumns($rows[0]);
                    if ($columns === false) {
                        $this->msg = '找不到指定欄位';
                        return;
                    }
                    if (count($columns) != 0) {
                        foreach ($rows as $idx => $row) {
                            if ($idx == 0) {
                                continue;
                            }
                            $sale_no = $row[$columns['sale_no']];
                            $trans_no = $row[$columns['trans_no']];
                            if (empty($trans_no)) {
                                continue;
                            } else  if (empty(Freight::where('sale_no', $sale_no)->where('trans_no', $trans_no)->first())) {
                                $ship_date = $row[$columns['ship_date']];
                                $ship_date = substr_replace($ship_date, '-', 4, 0);
                                $ship_date = substr_replace($ship_date, '-', 7, 0);
                                $qty = $row[$columns['qty']];
                                $weight = $row[$columns['weight']];
                                $freight = $row[$columns['freight']];
                                $combined_freight = $row[$columns['combined_freight']];
                                $other_freight = $row[$columns['other_freight_1']] + $row[$columns['other_freight_2']] + $row[$columns['other_freight_3']] + $row[$columns['other_freight_4']];
                                $trans_cust =  $row[$columns['trans_cust']];
                                Freight::create([
                                    'sale_no' => $sale_no,
                                    'trans_no' => $trans_no,
                                    'trans' => $this->type,
                                    'trans_cust' => $trans_cust,
                                    'ship_date' => $ship_date,
                                    'qty' => $qty,
                                    'weight' => $weight,
                                    'freight' => $freight,
                                    'combined_freight' => $combined_freight,
                                    'other_freight' => $other_freight,
                                ]);
                            }
                        }
                    }
                    break;
                case 'YCS':
                    $columns = $this->GetColumns($rows[3]);
                    if ($columns === false) {
                        $this->msg = '找不到指定欄位';
                        return;
                    }
                    if (count($columns) != 0) {
                        foreach (collect($rows)->slice(4)->groupBy($columns['sale_no']) as $sale_no => $group) {
                            $row = $group->first();
                            $trans_no = $row[$columns['trans_no']];
                            if (empty($trans_no)) {
                                continue;
                            } else if (empty(Freight::where('sale_no', $sale_no)->where('trans_no', $trans_no)->first())) {
                                $ship_date = $row[$columns['ship_date']];
                                $qty = explode('/', $row[$columns['qty']])[1];
                                $weight = $row[$columns['weight']] * $qty;
                                $freight = $row[$columns['freight']] * $qty;
                                $combined_freight = $row[$columns['combined_freight']] ?? 0;
                                $other_freight = $row[$columns['other_freight_1']] + $row[$columns['other_freight_2']] + $row[$columns['other_freight_3']];
                                $trans_cust = '';
                                Freight::create([
                                    'sale_no' => $sale_no,
                                    'trans_no' => $trans_no,
                                    'trans' => $this->type,
                                    'trans_cust' => $trans_cust,
                                    'ship_date' => $ship_date,
                                    'qty' => $qty,
                                    'weight' => $weight,
                                    'freight' => $freight,
                                    'combined_freight' => $combined_freight,
                                    'other_freight' => $other_freight,
                                ]);
                            }
                        }
                    }
                    break;
            }
            DB::connection('mysql')->commit();
        } catch (Exception $ex) {
            DB::connection('mysql')->rollback();
            $this->msg = '運費匯入錯誤。原因:' . $ex->getMessage();
        }
    }

    //取得欄位索引
    private function GetColumns(array $column)
    {
        if (empty($column) || count($column) == 0) {
            return false;
        }
        switch ($this->type) {
            case 'HCT':
                return [
                    'sale_no' => array_search('客戶用傳票_管理號碼', $column),
                    'trans_no' => array_search('明細表號', $column),
                    'trans_cust' => array_search('請款客代', $column),
                    'ship_date' => array_search('發送日', $column),
                    'qty' => array_search('總件數', $column),
                    'weight' => array_search('總重量KG', $column),
                    'freight' => array_search('本款', $column),
                    'combined_freight' => array_search('聯運費', $column),
                    'other_freight_1' => array_search('接費', $column),
                    'other_freight_2' => array_search('送費', $column),
                    'other_freight_3' => array_search('特服費', $column),
                    'other_freight_4' => array_search('附加費用', $column),
                ];
            case 'YCS':
                return [
                    'sale_no' => array_search('訂單編號', $column),
                    'trans_no' => array_search('提單號碼', $column),
                    'ship_date' => array_search('取件日期', $column),
                    'qty' => array_search("件數\n(不可修改) ", $column),
                    'weight' => array_search("重量\n(以第一件為主) ", $column),
                    'freight' => array_search('運費', $column),
                    'combined_freight' => array_search('配困', $column),
                    'other_freight_1' => array_search('超區', $column),
                    'other_freight_2' => array_search('超重', $column),
                    'other_freight_3' => array_search('超材', $column),
                ];
            default:
                return false;
        }
    }
}
