<?php

namespace App\Imports;

use Exception;
use App\Models\yherp\YcsTransport;
use App\Services\BaseService;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class YcsTransportImport implements ToCollection
{
    public $msg = '';
    private $settle_date;

    public function __construct($settle_date)
    {
        $this->settle_date = $settle_date;
    }

    public function collection(Collection $rows)
    {
        $this->msg = '';
        try {
            foreach ($rows as $key => $row) {
                if ($key == 0) { //略過標題
                    continue;
                }

                $sale_no = $row[0]; //訂單號碼
                $qty = $row[1]; //寄件件數
                $size = $row[2]; //尺寸
                $weight = $row[3]; //重量
                $name = $row[4]; //品名
                $receiver = $row[6]; //收件人
                $addr = $row[7]; //收件地址
                $tel = $row[8]; //收件電話
                $remark = $row[13]; //備註

                if (
                    !isset($this->settle_date) || !isset($sale_no) ||  !isset($qty) ||  !isset($size) ||  !isset($weight) ||
                    !isset($receiver) || !isset($addr) ||  !isset($tel)
                ) {
                    continue;
                } elseif (YcsTransport::where('sale_no', $sale_no)->whereNull('deleted_at')->get()->count() > 0) { //重複訂單號碼，略過
                    continue;
                } else {
                    YcsTransport::create(
                        [
                            'settle_date' => BaseService::AllTrim($this->settle_date),
                            'sale_no' => BaseService::AllTrim($sale_no),
                            'qty' => BaseService::AllTrim($qty),
                            'size' => BaseService::AllTrim($size),
                            'weight' => BaseService::AllTrim($weight),
                            'name' => BaseService::AllTrim($name),
                            'receiver' => BaseService::AllTrim($receiver),
                            'addr' => BaseService::AllTrim($addr),
                            'tel' => BaseService::AllTrim($tel),
                            'remark' => BaseService::AllTrim($remark),
                        ]
                    );
                }
            }
        } catch (Exception $ex) {
            $this->msg = '音速物流匯入發生無法預期的錯誤。原因:' . $ex->getMessage();
        }
    }
}
