<?php

namespace App\Imports;

use Exception;
use App\Models\yherp\CreditNoteM;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\SkipsUnknownSheets;

class CreditNoteInvalidImport implements ToCollection, SkipsUnknownSheets
{
    public $msg = '';

    public function collection(Collection $rows)
    {
        $this->msg = '';
        try {
            $header = $rows->first();
            $columns = (new CreditNoteImport())->GetColumns($header);
            $order_list = $rows->splice(1)->where($columns['order_no'], '!=', '')->pluck($columns['order_no'])->toArray();
            CreditNoteM::whereIn('order_no', $order_list)->delete();
        } catch (Exception $ex) {
            $this->msg = '解析折讓作廢發生無法預期的錯誤。原因:' . $ex->getMessage();
        }
    }

    public function onUnknownSheet($sheetName)
    {
        $this->msg = "找不到{$sheetName}工作表";
    }
}
