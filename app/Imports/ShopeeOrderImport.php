<?php

namespace App\Imports;

use DateTime;
use Exception;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class ShopeeOrderImport implements ToCollection
{
    public $msg = '';
    public $data = [];

    public function __construct()
    {
    }

    public function collection(Collection $rows)
    {
        $this->msg = '';
        try {
            $header = $rows->first();
            //遍歷所有訂單資料，依訂單編號分組
            foreach ($rows->slice(1)->groupBy(0) as $orderNo => $group) {
                $first_row = $group->first();
                $order_status = $first_row[1];
                $order_time = $first_row[5];
                $order_date = substr($order_time, 0, 10);
                $trans = $first_row[42];
                $pay_time = $first_row[48];
                $s_ship_time = $first_row[49];
                if ($order_status == '不成立' || empty($s_ship_time)) {
                    continue;
                }
                $order_to_ship = 0;
                $pay_to_ship = 0;
                if (!empty($s_ship_time)) {
                    $diff = (new DateTime($s_ship_time))->diff(new DateTime($order_time));
                    $order_to_ship = $diff->d * 24 + $diff->h;
                    $diff = (new DateTime($s_ship_time))->diff(new DateTime($pay_time));
                    $pay_to_ship = $diff->d * 24 + $diff->h;
                }
                $this->data[] = [
                    'order_no' => $orderNo,
                    'order_date' => $order_date,
                    'trans' => $trans,
                    'order_time' => $order_time,
                    'pay_time' => $pay_time,
                    's_ship_time' => $s_ship_time,
                    'order_to_ship' => $order_to_ship,
                    'pay_to_ship' => $pay_to_ship,
                ];
            }
        } catch (Exception $ex) {
            $this->msg = '蝦皮訂單匯入發生無法預期的錯誤。原因:' . $ex->getMessage();
        }
    }
}
