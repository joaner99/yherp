<?php

namespace App\Imports;

use Exception;
use App\Models\yherp\SysConfig;
use Illuminate\Support\Collection;
use App\Services\OrderSampleService;
use App\Models\yherp\OrderReturnRefund;
use Maatwebsite\Excel\Concerns\ToCollection;

class OrderReturnRefundImport extends BaseImport implements ToCollection
{
    public $msg = '';
    private $cust;
    private $orderSampleService;

    public function __construct($cust)
    {
        $this->cust = $cust;
        $this->orderSampleService = new OrderSampleService();
    }

    public function collection(Collection $rows)
    {
        $this->msg = '';
        try {
            $header = $rows->first()->toArray();
            //$this->create_config($header);
            $columns = $this->orderSampleService->GetOrderReturnRefundColumns($header);
            if (empty($columns)) {
                $this->msg = '錯誤，匯入的資料當中，找不到特定欄位';
                return;
            }
            foreach ($rows->slice(1)->groupBy($columns['return_no']) as $return_no => $group) {
                $row = $group->first();
                $order_no = $row[$columns['order_no']];
                $order_time = $row[$columns['order_time']] . ':00';
                $return_time = $row[$columns['return_time']] . ':00';
                $return_status = $row[$columns['return_status']];
                $dispute_status = $row[$columns['dispute_status']];

                OrderReturnRefund::updateOrCreate(
                    [
                        'id' => $return_no,
                    ],
                    [
                        'cust' => $this->cust,
                        'order_no' => $order_no,
                        'order_time' => $order_time,
                        'return_time' => $return_time,
                        'return_status' => $return_status,
                        'dispute_status' => $dispute_status ?? '',
                    ]
                );
            }
        } catch (Exception $ex) {
            $this->msg = '錯誤。原因:' . $ex->getMessage();
        }
    }

    private function create_config($header)
    {
        SysConfig::updateOrCreate(
            [
                'kind' => 'order_return_refund_columns'
            ],
            [
                'config' => json_encode($header),
                'note' => '蝦皮退貨退款欄位',
            ]
        );
    }
}
