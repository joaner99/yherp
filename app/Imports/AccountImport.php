<?php

namespace App\Imports;

use Exception;
use App\Models\yherp\Account;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\ToCollection;

class AccountImport extends BaseImport implements ToCollection
{
    public $msg = '';

    public function collection(Collection $rows)
    {
        $this->msg = '';
        DB::connection('mysql')->beginTransaction();
        try {
            foreach ($rows->slice(1) as $row) {
                $name = $row[0];
                $account = $row[1] ?? '';
                $password = $row[2];
                $remark = $row[3] ?? '';
                Account::create([
                    'name' => $name,
                    'account' => $account,
                    'password' => $password,
                    'remark' => $remark,
                ]);
            }
        } catch (Exception $ex) {
            DB::connection('mysql')->rollback();
            $this->msg = '帳號匯入錯誤。原因:' . $ex->getMessage();
        }
        DB::connection('mysql')->commit();
    }
}
