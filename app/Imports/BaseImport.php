<?php

namespace App\Imports;

use Exception;

class BaseImport
{
    private $msg = '';

    /**
     * 驗證
     *
     * @param array $header
     * @return bool
     */
    public function Verify(array $header, $config)
    {
        if (empty($header) || empty($config)) {
            $this->msg = '錯誤，『匯入的標題欄位為空白』或者『欄位的設定檔未正確設定』';
            return false;
        } elseif (count($header) != count($config)) {
            $this->msg = '錯誤，『匯入的標題欄位數量與設定不一致』';
            return false;
        } else {
            $unnecessaryMsg = '';
            $diffMsg = '';
            foreach ($header as $key => $value) {
                if (empty($config[$key])) { //未有設定的欄位
                    if (!empty($unnecessaryMsg)) { //添加斷點
                        $unnecessaryMsg .= '、';
                    }
                    $unnecessaryMsg .= "『{$value}』";
                } else {
                    if ($value != $config[$key]) { //與設定的欄位不同
                        if (!empty($diffMsg)) { //添加斷點
                            $diffMsg .= '、';
                        }
                        $diffMsg .= "『{$value}』必須為『{$config[$key]}』";
                    }
                }
            }
            if (!empty($diffMsg) || !empty($unnecessaryMsg)) {
                $this->msg = '錯誤，';
                if (!empty($diffMsg)) {
                    $this->msg .= '以下欄位與設定不同，' . $diffMsg . '。';
                }
                if (!empty($unnecessaryMsg)) {
                    $this->msg .= '以下為多餘欄位，' . $unnecessaryMsg . '。';
                }
                return false;
            } else {
                return true;
            }
        }
    }

    /**
     * 匯入的csv轉collection
     *
     * @return \Illuminate\Support\Collection
     */
    public function import_csv($path)
    {
        $result = [];
        try {
            $file = fopen($path, 'r');
            while (($data = $this->__fgetcsv($file))) {
                $result[] = $data;
            }
            fclose($file);
        } catch (Exception $ex) {
            $this->msg = $ex->getMessage();
        }
        return collect($result);
    }

    /**
     * 取得錯誤訊息
     *
     * @return void
     */
    public function get_error_msg()
    {
        return $this->msg;
    }

    /**
     * 複寫fgetcsv。因中文字無法正常切割逗號
     */
    private function __fgetcsv(&$handle, $length = null, $d = ",", $e = '"')
    {
        $d = preg_quote($d);
        $e = preg_quote($e);
        $_line = "";
        $eof = false;
        while ($eof != true) {
            $_line .= (empty($length) ? fgets($handle) : fgets($handle, $length));
            $itemcnt = preg_match_all('/' . $e . '/', $_line, $dummy);
            if ($itemcnt % 2 == 0)
                $eof = true;
        }
        $_csv_line = preg_replace('/(?: |[ ])?$/', $d, trim($_line));
        $_csv_pattern = '/(' . $e . '[^' . $e . ']*(?:' . $e . $e . '[^' . $e . ']*)*' . $e . '|[^' . $d . ']*)' . $d . '/';
        preg_match_all($_csv_pattern, $_csv_line, $_csv_matches);
        $_csv_data = $_csv_matches[1];


        for ($_csv_i = 0; $_csv_i < count($_csv_data); $_csv_i++) {
            $_csv_data[$_csv_i] = preg_replace("/^" . $e . "(.*)" . $e . "$/s", "$1", $_csv_data[$_csv_i]);
            $_csv_data[$_csv_i] = str_replace($e . $e, $e, $_csv_data[$_csv_i]);
        }
        return empty($_line) ? false : $_csv_data;
    }
}
