<?php

namespace App\Imports\warranty_web;

use Exception;
use App\Services\BaseService;
use Illuminate\Support\Collection;
use App\Models\warranty_web\ItemConfig;
use Maatwebsite\Excel\Concerns\ToCollection;

class ItemConfigImport implements ToCollection
{
    public $msg = '';

    public function collection(Collection $rows)
    {
        $this->msg = '';
        try {
            foreach ($rows as $key => $row) {
                if ($key == 0) { //略過標題
                    continue;
                }

                $id = $row[0]; //商品料號
                $warranty_y = $row[2]; //保固年數
                $warranty_m = $row[3]; //保固月數
                $warranty_d = $row[4]; //保固天數
                $warranty_url = $row[5]; //原場保固網址
                if (empty($id)) {
                    continue;
                } else {
                    ItemConfig::updateOrCreate(
                        [
                            'id' => BaseService::AllTrim($id),
                        ],
                        [
                            'warranty_y' => $warranty_y ?? 0,
                            'warranty_m' => $warranty_m ?? 0,
                            'warranty_d' => $warranty_d ?? 0,
                            'warranty_url' => $warranty_url ?? 0,
                        ]
                    );
                }
            }
        } catch (Exception $ex) {
            $this->msg = '商品保固匯入發生無法預期的錯誤。原因:' . $ex->getMessage();
        }
    }
}
