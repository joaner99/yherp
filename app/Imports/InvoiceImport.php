<?php

namespace App\Imports;

use Exception;
use App\Models\yherp\OrderInvoice;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\SkipsUnknownSheets;

class InvoiceImport implements ToCollection, SkipsUnknownSheets
{
    public $msg = '';

    public function __construct()
    {
    }

    public function collection(Collection $rows)
    {
        $this->msg = '';
        try {
            $header = $rows->first();
            $columns = [];
            foreach ($header as $header_idx => $h) {
                if (preg_replace('/\s(?=)/', '', $h) == preg_replace('/\s(?=)/', '', '訂單編號')) { //去空白比較
                    $columns['order_no'] = $header_idx;
                } else if (preg_replace('/\s(?=)/', '', $h) == preg_replace('/\s(?=)/', '', '發票號碼')) { //去空白比較
                    $columns['invoice_no'] = $header_idx;
                }
            }
            foreach ($rows->splice(1)->where($columns['invoice_no'], '!=', '')->groupBy($columns['invoice_no']) as $invoice_no => $group) {
                $first_row = $group->first();
                $order_no = $this->GetOrderNo($first_row[$columns['order_no']]);
                OrderInvoice::firstOrCreate([
                    'invoice_no' => $invoice_no,
                    'order_no' => $order_no
                ]);
            }
        } catch (Exception $ex) {
            $this->msg = '解析發票開立發生無法預期的錯誤。原因:' . $ex->getMessage();
        }
    }

    public function onUnknownSheet($sheetName)
    {
        $this->msg = "找不到{$sheetName}工作表";
    }

    private function GetOrderNo($val)
    {
        if (empty($val)) {
            return '';
        }
        return explode('_', $val)[0];
    }
}
