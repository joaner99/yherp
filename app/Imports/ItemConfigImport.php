<?php

namespace App\Imports;

use Exception;
use App\Models\yherp\ItemConfig;
use App\Services\BaseService;
use Illuminate\Support\Collection;
use App\Models\yherp\InventoryTurnoverRating;
use Maatwebsite\Excel\Concerns\ToCollection;

class ItemConfigImport implements ToCollection
{
    public $msg = '';

    public function collection(Collection $rows)
    {
        $this->msg = '';
        try {
            $rating_config = InventoryTurnoverRating::all();
            foreach ($rows as $key => $row) {
                if ($key == 0) { //略過標題
                    continue;
                }

                $id = $row[1]; //商品料號
                $name = $row[2]; //商品名稱
                $main_ss = $row[3]; //主倉安全庫存量
                $box_qty = $row[4]; //一箱數量
                $cost = $row[5]; //一箱數量
                $rating = $row[6]; //庫存周轉率評等
                if (
                    !isset($id) || (!empty($main_ss) && !is_numeric($main_ss)) || (!empty($box_qty) && !is_numeric($box_qty))
                    || (!empty($cost) && !is_numeric($cost))
                ) {
                    continue;
                } else {
                    //庫存週轉率評等
                    if (!empty($rating) && !empty($rating_config)) {
                        $rating_id = $rating_config->firstWhere('rating', $rating)->id ?? null;
                    } else {
                        $rating_id = null;
                    }
                    ItemConfig::updateOrCreate(
                        [
                            'id' => BaseService::AllTrim($id),
                        ],
                        [
                            'name' => $name ?? '',
                            'main_ss' => $main_ss ?? 0,
                            'box_qty' => $box_qty ?? 0,
                            'cost' => $cost ?? 0,
                            'rating_id' => $rating_id,
                        ]
                    );
                }
            }
        } catch (Exception $ex) {
            $this->msg = '商品參數匯入發生無法預期的錯誤。原因:' . $ex->getMessage();
        }
    }
}
