<?php

namespace App\Imports;

use DateTime;
use Exception;
use App\Models\yherp\AdProfitM;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class AdConfigImport extends BaseImport implements ToCollection
{
    public $msg = '';

    public function collection(Collection $rows)
    {
        $this->msg = '';
        try {
            $src_date = $this->GetDate($rows);
            $shop_id = $rows[3][1] ?? '';
            if (empty($src_date) || empty($shop_id)) {
                $this->msg = '廣告效益匯入錯誤。原因:無法取得『期間』、『賣場ID』';
                return;
            } else {
                $m = AdProfitM::updateOrCreate(
                    ['src_date' => $src_date, 'shop_id' => $shop_id],
                    ['updated_at' => new DateTime()]
                );
                $m->Details()->delete();
                foreach ($rows->slice(8) as $row) {
                    $seq = intval($row[0]); //順序
                    $item_name = trim($row[1]); //廣告名稱
                    $_status = trim($row[2]); //狀態
                    $item_id = trim($row[4]); //商品ID
                    $ad_type = trim($row[3]); //廣告類型
                    $keyword = trim($row[8]); //版位
                    $s_time = trim($row[9]); //開始日期
                    $e_time = trim($row[10]); //結束日期
                    $view_count = intval($row[11]); //瀏覽數
                    $click_count = intval($row[12]); //點擊數
                    $click_rate = floatval($row[13]) / 100; //點擊率
                    $convert1 = intval($row[14]); //轉換數
                    $convert2 = intval($row[15]); //直接轉換數
                    $convert1_rate = floatval($row[16]) / 100; //轉換率
                    $convert2_rate = floatval($row[17]) / 100; //直接轉換率
                    $convert1_cost = floatval($row[18]); //每一筆轉換的成本
                    $convert2_cost = floatval($row[19]); //每一筆直接轉換的成本
                    $sales1_qty = intval($row[20]); //銷售數
                    $sales2_qty = intval($row[21]); //直接銷售數
                    $sales1_amt = intval($row[22]); //銷售金額
                    $sales2_amt = intval($row[23]); //直接銷售新額
                    $cost = floatval($row[24]); //花費
                    $roas1 = floatval($row[25]); //投入產出比
                    $roas2 = floatval($row[26]); //直接投入產出比
                    $acos1 = floatval($row[27]) / 100; //成本收入比率
                    $acos2 = floatval($row[28]) / 100; //直接成本收入比率
                    $item_view_count = trim($row[29]); //商品瀏覽數
                    $item_click_count = trim($row[30]); //商品點擊數
                    $item_click_rate = trim($row[31]); //商品點擊率
                    $m->Details()->create(
                        [
                            'seq' => $seq,
                            'item_name' => $item_name,
                            '_status' => $_status,
                            'item_id' => $item_id,
                            'ad_type' => $ad_type,
                            'keyword' => $keyword,
                            's_time' => $s_time,
                            'e_time' => $e_time == '無限制' ? null : $e_time,
                            'view_count' => $view_count,
                            'click_count' => $click_count,
                            'click_rate' => $click_rate,
                            'convert1' => $convert1,
                            'convert2' => $convert2,
                            'convert1_rate' => $convert1_rate,
                            'convert2_rate' => $convert2_rate,
                            'convert1_cost' => $convert1_cost,
                            'convert2_cost' => $convert2_cost,
                            'sales1_qty' => $sales1_qty,
                            'sales2_qty' => $sales2_qty,
                            'sales1_amt' => $sales1_amt,
                            'sales2_amt' => $sales2_amt,
                            'cost' => $cost,
                            'roas1' => $roas1,
                            'roas2' => $roas2,
                            'acos1' => $acos1,
                            'acos2' => $acos2,
                            'item_view_count' => $item_view_count == 'N/A' ? null : intval($item_view_count),
                            'item_click_count' => $item_click_count == 'N/A' ? null : intval($item_click_count),
                            'item_click_rate' => $item_click_rate == 'N/A' ? null : floatval($item_click_rate) / 100,
                        ]
                    );
                }
            }
        } catch (Exception $ex) {
            $this->msg = '廣告效益匯入錯誤。原因:' . $ex->getMessage();
        }
    }

    /**
     * 解析廣告報表的期間日期
     *
     * @param collection $data
     * @return string
     */
    private function GetDate($rows)
    {
        if (empty($rows)) {
            return '';
        } else {
            try {
                $row = $rows[5][1];
                if (empty($row)) {
                    return '';
                }
                $split = explode('-',  $row);
                $s_date = new DateTime($split[0]);
                $e_date = new DateTime($split[1]);
                if (empty($s_date) || empty($e_date)) {
                    $this->msg = '廣告效益分析期間時錯誤。原因:期間的日期格式錯誤';
                    return '';
                } elseif ($s_date != $e_date) {
                    $this->msg = '廣告效益分析期間時錯誤。原因:期間必須是一天的區間';
                    return '';
                } else {
                    return $s_date->format('Y-m-d');
                }
            } catch (Exception $ex) {
                $this->msg = '廣告效益分析期間時錯誤。原因:' . $ex->getMessage();
                return '';
            }
        }
    }
}
