<?php

namespace App\Imports;

use Exception;
use App\Models\yherp\CreditNoteM;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\SkipsUnknownSheets;

class CreditNoteImport implements ToCollection, SkipsUnknownSheets
{
    public $msg = '';

    public function __construct()
    {
    }

    public function collection(Collection $rows)
    {
        $this->msg = '';
        try {
            $header = $rows->first();
            $columns = $this->GetColumns($header);
            $m = null;
            foreach ($rows->splice(1) as $row) {
                $order_no = $row[$columns['order_no']];
                //主檔
                if (!empty($order_no)) {
                    $order_date = $row[$columns['order_date']];
                    $invoice_no = $row[$columns['invoice_no']];
                    $invoice_date = $row[$columns['invoice_date']];
                    $m_tax = $row[$columns['m_tax']];
                    $m_total = $row[$columns['m_total']];
                    $m = CreditNoteM::firstOrCreate([
                        'order_no' => $order_no,
                        'order_date' => $order_date,
                        'invoice_no' => $invoice_no,
                        'invoice_date' => $invoice_date,
                        'tax' => $m_tax,
                        'total' => $m_total,
                    ]);
                    $m->Details()->delete();
                }
                //明細
                if (!empty($m)) {
                    $item_name = $row[$columns['item_name']];
                    $qty = $row[$columns['qty']];
                    $unit = $row[$columns['unit']];
                    $d_tax = $row[$columns['d_tax']];
                    $d_total = $row[$columns['d_total']];
                    $m->Details()->create([
                        'item_name' => $item_name,
                        'qty' => $qty,
                        'unit' => $unit,
                        'tax' => $d_tax,
                        'total' => $d_total,
                    ]);
                }
            }
        } catch (Exception $ex) {
            $this->msg = '解析折讓開立發生無法預期的錯誤。原因:' . $ex->getMessage();
        }
    }

    public function onUnknownSheet($sheetName)
    {
        $this->msg = "找不到{$sheetName}工作表";
    }

    //取得欄位索引
    public function GetColumns($header)
    {
        $config = [
            //主檔
            'order_no' => '折讓單號碼',
            'order_date' => '折讓日期',
            'invoice_no' => '原發票號碼',
            'invoice_date' => '原發票日期',
            'm_tax' => '營業稅額',
            'm_total' => '未稅總計',
            //明細
            'item_name' => '明細_品名',
            'qty' => '明細_數量',
            'unit' => '明細_未稅單價',
            'd_tax' => '明細_營業稅額',
            'd_total' => '明細_未稅小計',
        ];
        $result = [];
        foreach ($config as $key => $idx) {
            $result[$key] = false;
            foreach ($header as $header_idx => $h) {
                if (preg_replace('/\s(?=)/', '', $h) == preg_replace('/\s(?=)/', '', $idx)) { //去空白比較
                    $result[$key] = $header_idx;
                    $header->forget($header_idx);
                    continue 2;
                }
            }
        }
        return $result;
    }
}
