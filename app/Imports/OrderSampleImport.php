<?php

namespace App\Imports;

use Exception;
use Carbon\Carbon;
use App\Models\TMS\Item;
use App\Models\yherp\Holiday;
use App\Services\BaseService;
use App\Services\ConfigService;
use App\Models\yherp\OrderSampleM;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use App\Models\yherp\OtherTransport;
use App\Repositories\BaseRepository;
use App\Services\OrderSampleService;
use App\Models\yherp\PersonalTransport;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use Maatwebsite\Excel\Concerns\ToCollection;

class OrderSampleImport implements ToCollection
{
    public $msg = '';

    protected $type;
    protected $configService;
    protected $orderSampleService;
    protected $remote_config;

    public function __construct($type)
    {
        $baseRepository = new BaseRepository();
        $configService = new ConfigService($baseRepository);
        $this->type = $type;
        $this->configService = $configService;
        $this->orderSampleService = new OrderSampleService($baseRepository, $configService);
        $this->remote_config = OrderSampleService::GetLogisticsAddrMark('2');
    }

    public function collection(Collection $rows)
    {
        $this->msg = '';
        DB::connection('mysql')->beginTransaction();
        try {
            $this->ExcludeNull($rows);
            $header = $rows->first()->toArray();
            //驗證欄位
            if (!$this->Verify($header, $this->type)) {
                return;
            }
            $columns = $this->orderSampleService->GetOrderSampleColumns($header, $this->type);
            if (empty($columns)) {
                $this->msg = '錯誤，匯入的資料當中，找不到特定欄位';
                return;
            }
            if (
                $this->type == 0 &&
                count($rows->slice(1)->whereNotIn($columns['transport'], BaseService::original_home_trans[$this->type])->where($columns['transport_no'], null)) > 0
            ) { //蝦皮
                $this->msg = '錯誤，匯入的資料當中，超商物流沒有託運單號';
                return;
            }
            //預計出貨日
            $holiday_config = Holiday::get();
            $today = BaseService::GetWorkDay(Carbon::today()->format('Y-m-d'), $holiday_config);
            $next_day = BaseService::GetWorkDay((new Carbon($today))->addDays(1)->format('Y-m-d'), $holiday_config);
            $sp_item_config = $this->configService->GetSysConfig('sp_item')->pluck('value1')->toArray(); //需確認品項
            $ycs_exclude_items = $this->configService->GetSysConfig('ycs_exclude_items')->pluck('value1')->toArray() ?? []; //音速物流無法配送的商品
            $logistics_addr_mark = OrderSampleService::GetLogisticsAddrMark('1');
            $floor_items = Item::select('ICODE')
                ->whereHas('Item1', function ($q) {
                    $q->whereIn('ITCO3', ['014', '020', '046']); //SPC、收編條、LVT
                })
                ->where('ITCOD', '11')
                ->pluck('ICODE')
                ->toArray();
            $check_store = in_array($this->type, [2, 3, 4]);
            //遍歷所有訂單資料，依訂單編號分組
            foreach ($rows->slice(1)->groupBy($columns['order_no']) as $orderNo => $group) {
                if ($this->type == 1) { //Shopline 需特別處理格式
                    foreach ($group as &$g) {
                        $g[$columns['order_date']] = Date::excelToDateTimeObject($g[$columns['order_date']])->format('Y-m-d H:i:s');
                        $g[$columns['pay_date']] = Date::excelToDateTimeObject($g[$columns['pay_date']])->format('Y-m-d H:i:s');
                    }
                }
                $row = $group->first(); //第一筆資料
                $items = $group->pluck($columns['item_no'])->toArray();
                $order_status = $row[$columns['order_status']];
                if ($order_status == '不成立' || mb_strpos($order_status, '取消') !== false) { //排除不成立訂單
                    continue;
                }
                $include_floor = count(array_intersect($floor_items, $items)) > 0;
                if ($check_store) { //蝦皮1~3店檢查有無匯錯檔案
                    if ($include_floor && $this->type != 3) { //有地板料號
                        DB::connection('mysql')->rollBack();
                        $this->msg = '內含地板訂單卻匯入非2店平台';
                        return;
                    }
                }
                //判斷訂單確認類型
                if (!empty($row[$columns['remark']]) || !empty($row[$columns['customer_remark']])) { //要確認。買家/賣家有備註
                    $order_type = 1;
                } else if (count(array_intersect($sp_item_config, $items)) > 0) { //要確認。特殊商品
                    $order_type = 1;
                } else if ($include_floor) { //地板
                    $order_type = 1;
                } else { //免確認，正常訂單
                    $order_type = 0;
                }
                //物流建議判斷，非超商，非排除商品
                $trans = '';
                $receiver_name = '';
                $receiver_tel = '';
                $receiver_address = '';
                if (key_exists($this->type, BaseService::original_shop_trans) && !in_array($row[$columns['transport']], BaseService::original_shop_trans[$this->type])) { //宅配
                    $receiver_name = $row[$columns['receiver_name']];
                    $receiver_tel = $row[$columns['receiver_tel']];
                    $receiver_address = $row[$columns['receiver_address']];
                    //取得地址
                    if (in_array($this->type, [0, 2, 3, 4, 5])) { //蝦皮、BV SHOP
                        $receiver_address = $row[$columns['receiver_address']];
                    } else { //shopline
                        if (mb_strpos($row[$columns['pay_way']], '貨到付款') === false) {
                            $receiver_address = $row[$columns['receiver_city']] . $row[$columns['receiver_district']] . $row[$columns['receiver_address']];
                        } else { //shopline 宅配 選擇貨到付款只能新竹送
                            $receiver_address = '';
                            $trans = 'HCT';
                        }
                    }
                    //親送判斷
                    if ($this->CanPersonalTransport($group, $columns)) {
                        $trans = 'COM';
                    } else if (!empty($receiver_address)) { //有地址才能判斷
                        $suggest = $this->orderSampleService->GetAddrTransport($receiver_address, $logistics_addr_mark);
                        $ycs_exclude = count(array_intersect($ycs_exclude_items, $items)) > 0; //音速拒絕送
                        if (!empty($suggest) && ($suggest != 'YCS' || !$ycs_exclude)) { //音速需額外判斷不送的品項
                            $trans = $suggest;
                        }
                    }
                }
                //預計出貨日判斷
                switch ($order_type) {
                    case 0: //免確認
                    default:
                        $estimated_date = $today;
                        break;
                    case 1: //需確認
                        $estimated_date = $next_day;
                        break;
                }
                $originalData = json_encode($group); //原始資料轉JSON
                $tmp = [
                    'header' => json_encode($header),
                    'order_date' => $row[$columns['order_date']], //訂單成立時間
                    'order_type' => $order_type, //訂單確認類型
                    'original_data' => $originalData,
                    'receiver_name' => $receiver_name, //收件姓名
                    'receiver_tel' => $receiver_tel, //收件電話
                    'receiver_address' => $receiver_address, //收件地址
                ];
                $exists = OrderSampleM::where('src', $this->type)->where('order_no', $orderNo)->first();
                if (!empty($exists)) { //更新
                    $exists->update($tmp);
                } else { //新增
                    $tmp['src'] = $this->type;
                    $tmp['order_no'] = $orderNo;
                    $tmp['trans'] = $trans;
                    $tmp['estimated_date'] = $estimated_date;
                    $tmp['remark'] = $row[$columns['customer_remark']] . $row[$columns['remark']]; //撿貨單備註，預設帶入買賣家備註
                    OrderSampleM::create($tmp);
                    if ($trans == 'COM') { //親送總表
                        if (empty(PersonalTransport::where('original_no', $tmp['order_no'])->first())) {
                            PersonalTransport::create([
                                'original_no' => $tmp['order_no'],
                                'remark' => $tmp['remark'],
                            ]);
                        }
                    } else if ($include_floor) { //人工總表，將地板加入人工總表
                        if (empty(OtherTransport::where('original_no', $tmp['order_no'])->first())) {
                            OtherTransport::create([
                                'original_no' => $tmp['order_no'],
                                'ship_date' => $estimated_date,
                                'remark' => $tmp['remark'],
                            ]);
                        }
                    }
                }
            }
            DB::connection('mysql')->commit();
        } catch (Exception $ex) {
            DB::connection('mysql')->rollBack();
            $this->msg = '特殊訂單匯入發生無法預期的錯誤。原因:' . $ex->getMessage();
        }
    }

    /**
     * 驗證
     *
     * @param array $header
     * @return bool
     */
    private function Verify(array $header, $type)
    {
        $config = $this->configService->GetConfig('order_convert_columns')[$type] ?? null;
        if (empty($header) || empty($config)) {
            $this->msg = '錯誤，『匯入的標題欄位為空白』或者『欄位的設定檔未正確設定』';
            return false;
        } elseif (count($header) != count($config)) {
            $this->msg = '錯誤，『匯入的標題欄位數量與設定不一致』';
            return false;
        } else {
            $unnecessaryMsg = '';
            $diffMsg = '';
            foreach ($header as $key => $value) {
                if (empty($config[$key])) { //未有設定的欄位
                    if (!empty($unnecessaryMsg)) { //添加斷點
                        $unnecessaryMsg .= '、';
                    }
                    $unnecessaryMsg .= "『{$value}』";
                } else {
                    if ($value != $config[$key]) { //與設定的欄位不同
                        if (!empty($diffMsg)) { //添加斷點
                            $diffMsg .= '、';
                        }
                        $diffMsg .= "『{$value}』必須為『{$config[$key]}』";
                    }
                }
            }
            if (!empty($diffMsg) || !empty($unnecessaryMsg)) {
                $this->msg = '錯誤，';
                if (!empty($diffMsg)) {
                    $this->msg .= '以下欄位與設定不同，' . $diffMsg . '。';
                }
                if (!empty($unnecessaryMsg)) {
                    $this->msg .= '以下為多餘欄位，' . $unnecessaryMsg . '。';
                }
                return false;
            } else {
                return true;
            }
        }
    }

    /**
     * 排除標頭為Null的資料
     *
     * @param Collection $rows
     * @return void
     */
    private function ExcludeNull(Collection &$rows)
    {
        if (!empty($rows)) {
            //取得Null的欄位索引
            $nullColumns = [];
            $header = $rows->first()->toArray();
            foreach ($header as $key => $value) {
                if ($value == null) {
                    $nullColumns[] = $key;
                }
            }
            //有空白的欄位
            if (!empty($nullColumns)) {
                foreach ($rows as $key => $row) {
                    //清空該欄位的每一列資料
                    foreach ($nullColumns as $index) {
                        $row->forget($index);
                    }
                    //reset index
                    $rows[$key] = $row->values();
                }
            }
        }
    }

    //判斷是否需要親送
    private function CanPersonalTransport($group, $columns)
    {
        $first_row = $group->first();
        //賣家備註關鍵字"指定"
        if (mb_strpos($first_row[$columns['remark']], '指定') !== false) {
            return true;
        }
        //取得地址
        $addr = OrderSampleService::GetAddr($this->type, $first_row, $columns);
        $addr = str_replace('台', '臺', $addr);
        //過遠地區不送
        foreach (['屏東', '臺東', '花蓮', '宜蘭', '基隆'] as $keyword) {
            if (mb_strpos($addr, $keyword) !== false) {
                return false;
            }
        }
        //新竹偏遠地區不送
        $is_hct_remote = $this->orderSampleService->IsHctRemote($this->remote_config, $addr);
        if ($is_hct_remote) {
            return false;
        }
        //訂單品項計算
        $item_qty_total = 0;
        foreach ($group as $row) {
            $item_no = $row[$columns['item_no']];
            $item_kind = $this->orderSampleService->GetKind($item_no);
            if (empty($item_kind)) {
                return false;
            }
            $kind = $item_kind['kind'];
            $kind3 = $item_kind['kind3'];
            $seq = $item_kind['seq'];
            $item_qty = $row[$columns['item_quantity']];
            if ($kind == '11') {
                switch ($kind3) {
                    case '014': //SPC。1:1包
                        $item_qty_total += $item_qty;
                        break;
                    case '046': //LVT。10:1包
                        $item_qty_total += ($item_qty / 10);
                        break;
                    case '047': //塑木。11:1包
                        if (!in_array($seq, ['015', '016', '017', '018', '019'])) { //排除吸音板
                            $item_qty_total += ($item_qty / 11);
                        }
                        break;
                    default:
                        break;
                }
            }
        }
        return $item_qty_total >= 6;
    }
}
