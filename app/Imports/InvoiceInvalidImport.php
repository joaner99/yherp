<?php

namespace App\Imports;

use Exception;
use App\Models\yherp\OrderInvoice;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\SkipsUnknownSheets;

class InvoiceInvalidImport implements ToCollection, SkipsUnknownSheets
{
    public $msg = '';

    public function collection(Collection $rows)
    {
        $this->msg = '';
        try {
            $header = $rows->first();
            $columns = [];
            foreach ($header as $header_idx => $h) {
                if (preg_replace('/\s(?=)/', '', $h) == preg_replace('/\s(?=)/', '', '發票號碼')) { //去空白比較
                    $columns['invoice_no'] = $header_idx;
                }
            }
            $invoice_list = $rows->splice(1)->where($columns['invoice_no'], '!=', '')->pluck($columns['invoice_no'])->toArray();
            OrderInvoice::whereIn('invoice_no', $invoice_list)->update(['_type' => 1]);
        } catch (Exception $ex) {
            $this->msg = '解析發票作廢發生無法預期的錯誤。原因:' . $ex->getMessage();
        }
    }

    public function onUnknownSheet($sheetName)
    {
        $this->msg = "找不到{$sheetName}工作表";
    }
}
