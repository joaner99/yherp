<?php

namespace App\Imports;

use Exception;
use App\Models\TMS\Posein;
use App\Models\yherp\LogShipment;
use App\Models\yherp\OrderSampleM;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class TransportConvertImport implements ToCollection
{
    public $msg = '';
    public $data = [];

    public function collection(Collection $rows)
    {
        $this->msg = '';
        try {
            $order_no_idx = array_search('訂單編號', $rows[0]->toArray());
            $trans_no_idx = array_search('包裹查詢號碼', $rows[0]->toArray());
            if ($order_no_idx === false || $trans_no_idx === false) {
                $this->msg = '找不到【訂單編號】或【包裹查詢號碼】欄位';
                return;
            }
            $order_nos = $rows->slice(1)->pluck($order_no_idx)->toArray();
            $order_db = OrderSampleM::select('order_no')->whereIn('order_no', $order_nos)->get();
            $sale_db = Posein::select('PCOD1', 'PJONO', 'PDAT1', 'ConsignTran')->whereIn('PJONO', $order_nos)->get();
            $exclude_order = [];
            foreach ($rows->slice(1) as $row) {
                $order_no = $row[$order_no_idx];
                $order_data = $order_db->where('order_no', $order_no)->first();
                if (empty($order_data)) { //無訂單資料
                    $exclude_order[] = $order_no;
                    continue;
                }
                $sale_data = $sale_db->where('PJONO', $order_no)->first();
                if (empty($sale_data)) { //無銷貨資料
                    continue;
                }
                if (empty(LogShipment::where('PCOD1', $sale_data->PCOD1)->first())) { //無上車紀錄
                    continue;
                }
                $trans_no = $sale_data->ConsignTran;
                if (!empty($trans_no)) {
                    $row[$trans_no_idx] = $trans_no;
                }
            }
            $this->data = $rows->whereNotIn($order_no_idx, $exclude_order)->toArray();
        } catch (Exception $ex) {
            $this->msg = '蝦皮宅配物流轉檔發生無法預期的錯誤。原因:' . $ex->getMessage();
        }
    }
}
