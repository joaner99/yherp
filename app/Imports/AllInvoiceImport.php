<?php

namespace App\Imports;

use App\Imports\InvoiceImport;
use App\Imports\CreditNoteImport;
use App\Imports\InvoiceInvalidImport;
use App\Imports\CreditNoteInvalidImport;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class AllInvoiceImport implements WithMultipleSheets
{
    public $invoice_import;
    public $invoice_invalid_import;
    public $credit_note_import;
    // public $credit_note_invalid_import;

    public function __construct()
    {
        $this->invoice_import = new InvoiceImport();
        $this->invoice_invalid_import = new InvoiceInvalidImport();
        $this->credit_note_import = new CreditNoteImport();
        // $this->credit_note_invalid_import = new CreditNoteInvalidImport();
    }

    public function sheets(): array
    {
        return [
            '發票開立' => $this->invoice_import,
            '發票作廢' => $this->invoice_invalid_import,
            '折讓開立' => $this->credit_note_import,
            // '折讓作廢' => $this->credit_note_invalid_import,
        ];
    }

    public function GetMsg()
    {
        $result = '';
        if (!empty($this->invoice_import->msg)) {
            $result .= "解析『發票開立』時，發生問題。原因：{$this->invoice_import->msg}。";
        }
        if (!empty($this->invoice_invalid_import->msg)) {
            $result .= "解析『發票作廢』時，發生問題。原因：{$this->invoice_invalid_import->msg}。";
        }
        if (!empty($this->credit_note_import->msg)) {
            $result .= "解析『折讓開立』時，發生問題。原因：{$this->credit_note_import->msg}。";
        }
        // if (!empty($this->credit_note_invalid_import->msg)) {
        //     $result .= "解析『折讓作廢』時，發生問題。原因：{$this->credit_note_invalid_import->msg}。";
        // }
        return $result;
    }
}
