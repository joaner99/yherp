<?php

namespace App\Providers;

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //fix SQLSTATE[42000] error
        Schema::defaultStringLength(191);
        //with and whereHas
        \Illuminate\Database\Eloquent\Builder::macro(
            'withWhereHas',
            function ($relation, $constraint) {
                return $this->whereHas($relation, $constraint)->with([$relation => $constraint]);
            }
        );
    }
}
