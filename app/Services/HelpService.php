<?php

namespace App\Services;

use Illuminate\Http\Request;
use App\Services\BaseService;
use App\Models\yherp\HelpShops;
use App\Models\yherp\HelpActions;
use App\Models\yherp\HelpQuestions;

class HelpService extends BaseService
{
    /**
     * 取得問題頁面資料
     *
     * @return array
     */
    public function GetIndexViewData()
    {
        $shops = HelpShops::select('id', 'name')->get();
        $actions = HelpActions::select('id', 'name')->get();
        $types = $this->GetItemType();
        $items = $this->GetItemAllList();

        return  [
            'shops' => $shops, 'actions' => $actions, 'types' => $types, 'items' => $items,
        ];
    }

    //查詢問題
    public function GetSearchResult(Request $request)
    {
        $input_shop = $request->get('shop', '1');
        $input_action = $request->get('action', '1');
        $input_type = $request->get('type');
        $input_kind = $request->get('kind');
        $input_kind2 = $request->get('kind2');
        $input_kind3 = $request->get('kind3');
        $input_item_no = $request->get('id');
        $item_name = '';

        $shops = HelpShops::select('id', 'name')->get();
        $actions = HelpActions::select('id', 'name')->get();
        $types = $this->GetItemType();
        $items = $this->GetItemAllList();
        $data = HelpQuestions::with('Kinds')->with('Items')->with('Group')->with('Shops')->with('Actions');
        //銷售平台
        $data->whereHas('Shops', function ($query) use ($input_shop) {
            $query->where('shop_id', $input_shop);
        });
        //行為
        $data->whereHas('Actions', function ($query) use ($input_action) {
            $query->where('action_id', $input_action);
        });
        $universalData = (clone $data)->doesntHave('Kinds')->doesntHave('Items')->get(); //通用類問題
        $kindsData = collect(); //指定大中小類問題
        $itemsData = collect(); //指定料號問題
        if (!empty($input_item_no)) { //查詢指定料號
            $itemsData = (clone $data)->whereHas('Items', function ($query) use ($input_item_no) {
                $query->where('item_no', $input_item_no);
            })->get();
            $item_detail = $this->GetItemAllDetail([$input_item_no])->first() ?? null;
            if (!empty($item_detail)) {
                $item_name = $item_detail->item_name;
                $kind = $item_detail->kind;
                $kind2 = $item_detail->kind2;
                $kind3 = $item_detail->kind3;
                $kindsData = (clone $data)->whereHas('Kinds', function ($query) use ($kind, $kind2, $kind3) {
                    $query->where(function ($q) use ($kind, $kind2, $kind3) {
                        $q->where('kind', $kind)->where('kind2', $kind2)->where('kind3', $kind3);
                    })->orWhere(function ($q) use ($kind, $kind2) {
                        $q->where('kind', $kind)->where('kind2', $kind2)->where('kind3', '');
                    })->orWhere(function ($q) use ($kind) {
                        $q->where('kind', $kind)->where('kind2', '')->where('kind3', '');
                    });
                })->get();
            }
        } elseif (!empty($input_kind) || !empty($input_kind2) || !empty($input_kind3)) { //查詢指定大中小類
            $kindsData = (clone $data)->whereHas('Kinds', function ($query) use ($input_kind, $input_kind2, $input_kind3) {
                if (!empty($input_kind)) {
                    $query->where('kind', $input_kind);
                }
                if (!empty($input_kind2)) {
                    $query->where('kind2', $input_kind2);
                }
                if (!empty($input_kind3)) {
                    $query->where('kind3', $input_kind3);
                }
            })->get();
            $item_no_list = $this->GetItemNoList($input_kind, $input_kind2, $input_kind3);
            if (count($item_no_list) > 0) {
                $itemsData = (clone $data)->whereHas('Items', function ($query) use ($item_no_list) {
                    $query->whereIn('item_no', $item_no_list);
                })->get();
            }
        }
        //查詢條件整理
        $shop_name = $shops->find($input_shop)->name ?? '';
        $action_name = $actions->find($input_action)->name ?? '';
        $type_name = '';
        switch ($input_type) {
            case 0:
                $type_name = '通用類';
                break;
            case 1:
                $type_name = '指定大中小類';
                break;
            case 2:
                $type_name = '指定料號';
                break;
        }
        $kind_name = $types[1][$input_kind] ?? '';
        $kind2_name = $types[2][$input_kind2] ?? '';
        $kind3_name = $types[3][$input_kind3] ?? '';

        return  [
            'input_data' => [
                'shop' => $shop_name, 'action' => $action_name, 'type' => $input_type,
                'type_name' => $type_name, 'item_no' => $input_item_no, 'item_name' => $item_name,
                'kind' => $input_kind, 'kind_name' => $kind_name, 'kind2' => $input_kind2, 'kind2_name' => $kind2_name, 'kind3' => $input_kind3, 'kind3_name' => $kind3_name
            ],
            'types' => $types, 'items' => $items,
            'data' => ['universal' => $universalData, 'kind' => $kindsData, 'item' => $itemsData],
        ];
    }
}
