<?php

namespace App\Services;

use Carbon\Carbon;
use App\Models\TMS\Histin;
use App\Models\yherp\SalesM;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class ErpService extends BaseService
{
    //取得銷售紀錄
    public function GetSales(Request $request)
    {
        $company = $request->get('company');
        $database = $this->GetDatabase($company);
        $s_date = $request->get('s_date', Carbon::today()->format('Y-m-d'));
        $e_date = $request->get('e_date', Carbon::today()->format('Y-m-d'));
        Session::put('erp_sales_index_s_date', $s_date);
        Session::put('erp_sales_index_e_date', $e_date);
        $db = SalesM::from($database . '.sales_m')
            ->with([
                'Details' => function ($q) use ($database) {
                    $q->from($database . '.sales_d');
                }
            ])
            ->whereBetween('sale_date', [$s_date, $e_date])
            ->get()
            ->sortByDesc('id');
        return ['data' => $db, 'company' => $company, 'companies' => array_keys(self::connections)];
    }

    //取得還在銷售的SPC、LVT、收編條
    public function GetFloorItem($company)
    {
        $database = $this->GetDatabase($company);
        $date = Carbon::today()->addMonths(-6);
        $date = ($date->year - 1911) . $date->format('.m.d');
        $result = Histin::select('HICOD', 'HINAM')
            ->with([
                'ItemPlaceD' => function ($q) use ($database) {
                    $q->from($database . '.item_place_d');
                }
            ])
            ->whereHas('ITEM1', function ($q) {
                $q->whereIn('ITCO3', ['014', '046', '020']);
            })
            ->where('HDAT1', '>=', $date)
            ->where('HYCOD', '11')
            ->whereNotIn('HICOD', [ //排除一些沒再賣的
                '11010014094',
                '11010014095',
                '11010020006',
                '11010020019',
                '11010046001',
                '11010046007',
            ])
            ->distinct()->orderBy('HINAM')->get();
        return $result;
    }
}
