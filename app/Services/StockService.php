<?php

namespace App\Services;

use DateTime;
use Exception;
use Carbon\Carbon;
use App\Models\TMS\ITEM4;
use App\Models\yherp\Stock;
use Illuminate\Http\Request;
use App\Services\BaseService;
use App\Models\yherp\LogStockD;
use App\Models\yherp\LogStockM;
use App\Models\yherp\ItemConfig;
use App\Models\yherp\ItemPlaceD;
use App\Models\yherp\ItemPlaceM;
use App\Models\yherp\LogItemSale;
use App\Models\yherp\OrderConfirmD;
use Illuminate\Support\Facades\Auth;
use App\Services\OrderConfirmService;
use App\Models\yherp\LogSafetyStockExport;

class StockService extends BaseService
{
    public const log_type = ['1' => '入庫作業', '2' => '出庫作業', '3' => '首次入庫(可建立儲位)'];

    /**
     * 根據產品清單找出所有儲位
     *
     * @param array $items 料號清單
     * @param string $database 
     * @param string $s_id 主倉ID
     * @return \Illuminate\Support\Collection
     */
    public function GetPlacesByItems($items, $database, $s_id)
    {
        if (empty($items) || count($items) == 0) {
            return collect();
        }
        //由庫存少得先扣
        $db = ItemPlaceD::from($database . '.item_place_d')
            ->withWhereHas('Main', function ($q) use ($database, $s_id) {
                $q->from($database . '.item_place_m')->where('s_id', $s_id);
            })
            ->whereIn('item_no', $items)
            ->where('qty', '>', 0)
            ->orderBy('qty')
            ->get();

        return $db;
    }

    //主倉儲位
    public function GetStockPlace(Request $request)
    {
        $company = $request->get('company');
        $database = $this->GetDatabase($company);
        if (empty($database)) {
            $db = collect();
        } else {
            $db = Stock::from($database . '.stock')
                ->with([
                    'Places' => function ($q) use ($database) {
                        $q->from($database . '.item_place_m');
                    },
                    'Places.Details' => function ($q) use ($database) {
                        $q->from($database . '.item_place_d');
                    }
                ])
                ->where('id', '!=', 'A001')
                ->get();
        }
        return ['stocks' => $db, 'company' => $company, 'companies' => array_keys(self::connections)];
    }

    /**
     * 取得低於安庫的商品
     *
     * @param array|null $items 指定商品
     * @return \Illuminate\Support\Collection
     */
    public function GetUnsafetyStock(array $items = null)
    {
        $config = ItemConfig::select('id', 'name', 'main_ss')
            ->with(['STOC' => function ($q) {
                $q->select('SITEM', 'SNUMB')->where('STCOD', 'A001');
            }])
            ->with(['ItemPlaceD' => function ($q) {
                $q->select('item_no', 'qty')->where('m_id', 'LIKE', 'B%');
            }])
            ->with(['LogOrderRequire' => function ($q) {
                $q->select('item_no', 'qty');
            }])
            ->where('disable_ss', 0)
            ->where('main_ss', '>', 0);
        //特定商品
        if (!empty($items) && count($items) > 0) {
            $config->whereIn('id', $items);
        }
        $config = $config->get();
        $config_list = $config->pluck('id')->toArray();
        $china_list = ITEM4::select('ICODE4')->whereIn('ICODE4', $config_list)->where('T11_11', 'china')->pluck('ICODE4')->toArray();
        $unchina_list = array_diff($config_list, $china_list);
        $result = [];
        foreach ($config as $cfg) {
            $item_no = $cfg->id;
            $order_require = $cfg->LogOrderRequire->sum('qty');
            $sale_stock = $cfg->STOC->sum('SNUMB');
            $main_stock = $cfg->ItemPlaceD->sum('qty');
            $stock = $sale_stock + $main_stock;
            $main_ss = $cfg->main_ss;
            if ($stock - $order_require >= $main_ss) { //主倉+出貨-訂單需裘
                continue;
            }
            $result[] = [
                'country' => in_array($item_no, $unchina_list) ? '0' : '1',
                'item_no' => $item_no,
                'item_name' => $cfg->name,
                'm_stock' => $main_stock,
                's_stock' => $sale_stock,
                'stock' => $stock,
                'order_require' => $order_require,
                'safety' => $main_ss,
                'status' => '不足安全庫存',
                'exported_at' => $cfg->LogSafetyStockExport()->orderBy('created_at', 'DESC')->first()->created_at ?? ''
            ];
        }
        $ignore_items = ItemConfig::select('id')->where('disable_ss', 1)->get()->pluck('id')->toArray(); //停止安庫
        //處理未建立設定檔商品
        $extra_items = LogItemSale::select('id')
            ->with(['STOC' => function ($q) {
                $q->select('SITEM', 'SNUMB')->where('STCOD', 'A001');
            }])
            ->with(['ItemPlaceD' => function ($q) {
                $q->select('item_no', 'qty')->where('m_id', 'LIKE', 'B%');
            }])
            ->with(['LogOrderRequire' => function ($q) {
                $q->select('item_no', 'qty');
            }])
            ->with(['SafetyStock' => function ($q) {
                $q->select('item_no', 'item_name', 'item_remark', 'daily_sales');
            }])
            ->whereHas('SafetyStock')
            ->whereNotIn('id', array_merge($config_list, $ignore_items))
            ->where('ratio', '>', '0.000678');
        //特定商品
        if (!empty($items) && count($items) > 0) {
            $extra_items->whereIn('id', $items);
        }
        $extra_items = $extra_items->get();
        $logs = LogSafetyStockExport::select('item_no', 'created_at')->whereIn('item_no', $extra_items->pluck('id')->toArray())->get();
        foreach ($extra_items as $item) {
            $item_no = $item->id;
            $item_name = $item->SafetyStock->item_name;
            $is_china = strcasecmp($item->SafetyStock->item_remark, 'china') == 0;
            $order_require = $item->LogOrderRequire->sum('qty');
            $sale_stock = $item->STOC->sum('SNUMB');
            $main_stock = $item->ItemPlaceD->sum('qty');
            $stock = $sale_stock + $main_stock;
            $daily_sales = $item->SafetyStock->daily_sales;
            $main_ss = 40 * $daily_sales;
            if ($stock - $order_require >= $main_ss) { //主倉+出貨-訂單需裘
                continue;
            }
            $result[] = [
                'country' => $is_china ? '1' : '0',
                'item_no' => $item_no,
                'item_name' => $item_name,
                'm_stock' => $main_stock,
                's_stock' => $sale_stock,
                'stock' => $stock,
                'order_require' => $order_require,
                'safety' => $main_ss,
                'status' => '建議追加安全庫存',
                'exported_at' => $logs->where('item_no', $item_no)->sortByDesc('created_at')->first()->created_at ?? ''
            ];
        }
        asort($result);
        return collect($result);
    }

    //取得主倉異動紀錄
    public function GetStockLog(Request $request)
    {
        $result = [];
        $company = $request->get('company');
        $database = $this->GetDatabase($company);
        $s_date = $request->get('start_date', Carbon::today()->format('Y-m-d'));
        $e_date = $request->get('end_date', Carbon::today()->format('Y-m-d'));
        $places = ItemPlaceM::from($database . '.item_place_m')->select('id', 'name')->where('s_id', 'B001')->get();
        $items = ItemPlaceD::from($database . '.item_place_d')->select('item_no', 'item_name')->whereHas('Main', function ($q) use ($database) {
            $q->from($database . '.item_place_m')->where('s_id', 'B001');
        })->distinct()->orderBy('item_no')->get();
        $type = $request->get('type', 1);
        if ($type == 1) { //依儲位
            $place = $request->get('place');
            $db = LogStockM::from($database . '.log_stock_m')
                ->with([
                    'Details' => function ($q) use ($database) {
                        $q->from($database . '.log_stock_d');
                    },
                    'Place' => function ($q) use ($database) {
                        $q->from($database . '.item_place_m');
                    },
                    'User'
                ])
                ->whereBetween('created_at', [$s_date . ' 00:00:00', $e_date . ' 23:59:59']);
            if (!empty($place)) {
                $request = $db->where('p_id', $place);
            }
            $result = $db->get()->sortByDesc('created_at');
        } else if ($type == 2) { //依商品
            $item = $request->get('item');
            if (!empty($item)) {
                $result = LogStockM::from($database . '.log_stock_m')
                    ->with(['User', 'Place' => function ($q) use ($database) {
                        $q->from($database . '.item_place_m');
                    }])
                    ->withWhereHas('Details', function ($q) use ($item, $database) {
                        $q->from($database . '.log_stock_d')->where('item_no', $item);
                    })
                    ->whereBetween('created_at', [$s_date . ' 00:00:00', $e_date . ' 23:59:59'])
                    ->get()
                    ->sortByDesc('created_at');
            } else {
                $result = LogStockM::from($database . '.log_stock_m')
                    ->whereBetween('created_at', [$s_date . ' 00:00:00', $e_date . ' 23:59:59'])
                    ->get()
                    ->sortByDesc('created_at');
            }
        }
        return [
            'places' => $places,
            'items' => $items,
            'type' => $type,
            'data' => $result,
            'companies' => array_keys(self::connections)
        ];
    }

    /**
     * 搓合調撥總表
     *
     * @return array
     */
    public function GetStockTransfer()
    {
        $config = ItemConfig::where('main_ss', '!=', 0)
            ->orWhere('sale_ss', '!=', 0)
            ->orWhere('box_qty', '!=', 0)
            ->orWhere('purchase_qty', '!=', 0)
            ->get();
        $orderRequire = $this->GetOrderRequire();
        //協商換貨
        $order_list = [];
        foreach (collect($orderRequire)->pluck('orders') as $orders) {
            foreach ($orders as $order) {
                $order_list[] = $order['order_no'];
            }
        }
        $order_list = array_unique($order_list);
        $confirm_list = OrderConfirmD::whereHas('Main', function ($q) {
            $q->where('order_type', '13');
        })
            ->whereIn('order_no', $order_list)
            ->pluck('order_no')
            ->toArray();
        $itemNoList = array_unique(array_merge(array_keys($orderRequire), $config->pluck('id')->toArray())); //結合訂單需求，及安庫設定檔
        //數字轉字串
        foreach ($itemNoList as &$item) {
            $item = (string)$item;
        }
        $items = $this->GetItemAllDetail($itemNoList);
        $stock = $this->GetStock($itemNoList);
        $purchaseOrders = $this->baseRepository->GetPurchaseNotIn($itemNoList);
        $result = [];
        foreach ($items as $item) {
            //排除99包材類、SPC、LVT地板
            if ($item->kind == '99') {
                continue;
            }
            //基本資料
            $itemNo = $item->item_no;
            $itemName = $item->item_name;
            $itemKind3 = $item->ITEM1->kind3;
            $itemSupplier = $item->supplier;
            $itemSupplierName = $item->supplier_name;
            $is_china = strcasecmp($item->ITEM4->T11_11, 'china') == 0;
            //訂單需求資料
            if (key_exists($itemNo, $orderRequire)) {
                $order = $orderRequire[$itemNo];
                $need = $order['need'];
                $orders = $order['orders'];
                foreach ($orders as &$o) {
                    $o['change_item'] = in_array($o['order_no'], $confirm_list) ? 1 : 0;
                }
                ksort($orders);
            } else {
                $need = 0;
                $orders = [];
            }
            //庫存資料
            $main_stock = $stock[$itemNo]['主倉儲'] ?? 0;
            $sale_stock = $stock[$itemNo]['出貨倉'] ?? 0;
            //安庫、箱數
            $cfg = $config->find($itemNo);
            $main_ss = $cfg->main_ss ?? 0;
            $sale_ss = $cfg->sale_ss ?? 0;
            $box_qty = $cfg->box_qty ?? 0;
            $purchase_qty = $cfg->purchase_qty ?? 0;
            $main_ss = 0; //暫時不考慮主倉安庫
            $sale_ss = 0; //暫時不考慮出貨安庫
            //計算出貨倉需調撥數量
            $sale_need = 0; //出貨倉實際調撥數
            $sale_suggest = 0; //出貨倉建議調撥數
            $sale_left = $sale_stock - $need - $sale_ss; //出貨倉剩餘量
            if ($sale_left < 0) { //有缺
                $sale_need = abs($sale_left);
                if ($box_qty > 0) { //有一箱數量，取整箱
                    $sale_suggest = $box_qty * ceil($sale_need / $box_qty);
                } else { //無一箱數量，取單件
                    $sale_suggest = $sale_need;
                }
            }
            //採購單未進貨
            $purchase_undone = 0;
            $purchase_orders = [];
            $purchaseOrder = $purchaseOrders->where('SICOD', $itemNo);
            if (count($purchaseOrder) > 0) {
                foreach ($purchaseOrder as $order) {
                    $purchase = $order->SINUM; //採購量
                    $purchase_done = $order->SONUM; //已進貨量
                    $purchase_orders[] = [
                        'order_no' => $order->SCOD1,
                        'date' => $order->SDAT1,
                        'supplier' => $order->SPNAM,
                        'purchase' => number_format($purchase),
                        'done' => number_format($purchase_done),
                    ];
                    $purchase_undone += ($purchase - $purchase_done); //採購未進貨量=採購量-已進貨量
                }
            }
            //計算主倉需進貨數量
            $main_need = 0; //主倉實際採購數
            $main_suggest = 0; //主倉建議採購數
            $main_left = $main_stock - $sale_need - $main_ss + $purchase_undone; //主倉剩餘量
            if ($main_left < 0) { //有缺
                $main_need = abs($main_left);
                if ($purchase_qty > 0) { //有一箱數量，取整箱
                    $main_suggest = $purchase_qty * ceil($main_need / $purchase_qty);
                } else { //無一箱數量，取單件
                    $main_suggest = $main_need;
                }
            }
            //出貨倉、主倉不須調撥/採購
            if ($sale_suggest == 0 && $main_suggest == 0) {
                continue;
            }

            $result[] = [
                'item_no' => $itemNo,
                'item_name' => $itemName,
                'item_kind3' => $itemKind3,
                'item_supplier' => $itemSupplier,
                'item_supplier_name' => $itemSupplierName,
                'is_china' => $is_china,
                'box_qty' => $box_qty,
                'purchase_qty' => $purchase_qty,
                'main_stock' => $main_stock,
                'sale_stock' => $sale_stock,
                'main_ss' => $main_ss,
                'sale_ss' => $sale_ss,
                'need' => $need,
                'sale_need' => $sale_need,
                'sale_suggest' => $sale_suggest,
                'main_need' => $main_need,
                'main_suggest' => $main_suggest,
                'purchase_undone' => $purchase_undone,
                'orders' => json_encode($orders),
                'purchase_orders' => json_encode($purchase_orders),
            ];
        }
        return $result;
    }

    /**
     * 取得搓合調撥總表，採購申請單
     *
     * @param array $data
     * @param array $is_china 中國商品
     * @return string
     */
    public function GetStockPurchaseExport($data, $is_china)
    {
        if (empty($data)) {
            return '';
        }
        //Excel header，實際數量為參考用
        $result = [
            ['採購日期', '廠商代號', '外幣幣別', '產品代號', '產品名稱', '實際數量', '數量', '單價', '外幣單價', '備註1', '備註2', '備註3']
        ];
        //Excel body
        $todayTW = $this->GetTWDateStr(new DateTime());
        foreach (collect($data)->where('is_china', $is_china)->where('main_suggest', '>', 0) as $d) {
            $item_no = $d['item_no'];
            $item_name = $d['item_name'];
            $item_supplier = $d['item_supplier'];
            $main_need = $d['main_need'];
            $main_suggest = $d['main_suggest'];

            $result[] = [
                $todayTW,
                $item_supplier,
                '',
                $item_no,
                $item_name,
                $main_need,
                $main_suggest,
                '',
                '',
                '',
                '',
                ''
            ];
        }

        return json_encode($result);
    }

    /**
     * 倉庫異動作業
     *
     * @param string $connection
     * @param string $database
     * @param int $s_id 主倉ID
     * @param int $p_id 儲位ID
     * @param array $items 商品
     * @param int $type 異動類型。1=入庫、2=出庫
     * @param int $log_kind 入庫時。1=移轉：出庫時。1=移轉、2=調撥
     * @return string
     */
    public function StockModify($connection, $database, $s_id, $p_id, $items, $type, $log_kind = 0)
    {
        if (empty($items) || count($items) == 0) {
            return '';
        }
        try {
            $place = ItemPlaceM::from($database . '.item_place_m')
                ->with(['Details' => function ($q) use ($database) {
                    $q->from($database . '.item_place_d');
                }])
                ->where('id', $p_id)
                ->where('s_id', $s_id)
                ->get()->first();
            if (empty($place)) {
                return "找不到{$p_id}儲位";
            }
            //安庫未確認禁止調撥出庫，僅限優居
            if ($database == 'yherp') {
                $unsafety_stock = (new OrderConfirmService())->GetUnsafetyStock(collect($items)->pluck('no')->toArray());
                if ($type == 2 && count($unsafety_stock) > 0) {
                    $items_name = join('、', collect($unsafety_stock)->pluck('item_name')->toArray());
                    return "因未覆核商品安庫，禁止出庫/調撥作業。商品：" . $items_name;
                }
            }
            //倉庫異動紀錄主檔
            $log_m = LogStockM::on($connection)->create([
                'user_id' => Auth::user()->id,
                'p_id' => $p_id,
                'log_type' => $type,
                'log_kind' => $log_kind
            ]);
            foreach ($items as $item) {
                $item_no = $item->no;
                $item_name = $item->name;
                $item_qty = $item->qty;
                $i = $place->Details->where('item_no', $item_no)->first();
                if (empty($i)) { //無該商品庫存
                    if ($type != 3) { //非首次入庫，為異常
                        continue;
                    }
                    //新建儲位
                    ItemPlaceD::on($connection)->create([
                        'm_id' => $place->id,
                        'item_no' => $item_no,
                        'item_name' => $item_name,
                        'qty' => $item_qty,
                    ]);
                } else { //調整庫存數量
                    switch ($type) {
                        case 1: //入庫
                            $i->qty += $item_qty;
                            break;
                        case 2: //出庫
                            $i->qty -= $item_qty;
                            break;
                    }
                    //更新庫存數
                    ItemPlaceD::on($connection)->where('id', $i->id)->first()->update([
                        'qty' => $i->qty
                    ]);
                }
                //log
                LogStockD::on($connection)->create([
                    'm_id' => $log_m->id,
                    'item_no' => $item_no,
                    'item_name' => $item_name,
                    'qty' => $item_qty,
                ]);
            }
            return '';
        } catch (Exception $ex) {
            return $ex->getMessage();
        }
    }

    /**
     * 取得訂單未出貨商品
     *
     * @return array
     */
    private function GetOrderRequire()
    {
        $db = $this->baseRepository->GetOrderRequire();

        //取得各料號、品名，的訂單需求量、訂單號
        $result = [];
        foreach ($db as $value) {
            $itemNo = $value->item_no; //料號
            $itemName = $value->item_name; //品名
            $orderNo = $value->Main->order_no; //訂單號
            $orderNeedNum = ceil($value->ONNO); //訂單未出需求數量
            $originalNo = $value->Main->original_no; //原始訂單號
            $item = &$result[$itemNo];
            if (empty($item)) {
                $item['name'] = $itemName;
                $item['need'] = $orderNeedNum;
            } else {
                $item['need'] += $orderNeedNum;
            }
            $item['orders'][] = [
                'order_no' => $orderNo,
                'original_no' => $originalNo,
                'need' => $orderNeedNum
            ];
        }

        return $result;
    }
}
