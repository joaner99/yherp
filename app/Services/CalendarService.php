<?php

namespace App\Services;

use DateTime;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\yherp\Holiday;
use App\Services\BaseService;
use App\Models\yherp\Calendar;

class CalendarService extends BaseService
{
    public const calendar_kind = [1 => '請假', 2 => '公差', 99 => '其他'];

    public function GetCalendar(Request $request)
    {
        $s_date = new DateTime($request->get('s_date'));
        $previous = (new Carbon($s_date))->addMonths(-1)->format('Y-m');
        $next = (new Carbon($s_date))->addMonths(1)->format('Y-m');
        $current = (new Carbon($s_date))->format('Y年m月');
        $week_title = ['日', '一', '二', '三', '四', '五', '六'];
        $each_day = $this->GetEachDayByMonth($s_date);
        $first_day = collect($each_day)->first();
        $last_day = collect($each_day)->last();
        $holidays = Holiday::whereBetween('date', [$first_day, $last_day])->get();
        $all_events = Calendar::with('User')->where(function ($q) use ($first_day, $last_day) {
            $q
                ->where('s_date', '<=', $last_day)
                ->orWhere('e_date', '>=', $first_day);
        })->get();
        $result = [];
        foreach ($each_day as $day) {
            $day_s_date = $day;
            $day_e_date = $day;
            $day_events = $all_events->where('s_date', '<=', $day_e_date)->where('e_date', '>=', $day_s_date);
            $events = [];
            $is_holiday = false;
            $holiday = $holidays->where('date', $day)->first();
            if (!empty($holiday)) {
                $is_holiday = true;
                $events[] = [
                    'id' => '-1',
                    'kind' => '0',
                    'interval' => '整日',
                    'user' => '',
                    'content' => $holiday->name ?? $holiday->holidaycategory,
                    'is_holiday' => true,
                ];
            }
            foreach ($day_events as $event) {
                $id = $event->id;
                $interval = '';
                if (empty($event->s_time) || empty($event->e_time)) {
                    $interval .= '整日 ';
                } else {
                    if (!empty($event->s_time)) {
                        $interval .= (new DateTime($event->s_time))->format('H:i');
                    }
                    if (!empty($event->e_time)) {
                        $interval .= '~' . (new DateTime($event->e_time))->format('H:i');
                    }
                }
                $user = '';
                $content = '';
                $kind = $event->kind;
                switch ($kind) {
                    case 1: //請假
                        $user = $event->User->name2;
                        $content = '休假';
                        break;
                    case 2: //出差
                        $user = $event->User->name2;
                        $content = '出差';
                        break;
                    case 99: //其他
                        $content = $event->content;
                        break;
                }
                $events[] = [
                    'id' => $id,
                    'kind' => $kind,
                    'interval' => $interval,
                    'user' => $user,
                    'content' => $content,
                    'is_holiday' => false,
                ];
            }
            //排序
            usort($events, function ($a, $b) {
                return strcmp($a['interval'], $b['interval']);
            });
            $day_of_week =  (new Carbon($day))->dayOfWeek;
            $result[$day] = [
                'day_of_week' => $day_of_week,
                'events' => $events,
                'is_holiday' => $is_holiday,
            ];
        }
        return [
            'week_title' => $week_title,
            'days' => $result,
            'current' => $current,
            'previous' => $previous,
            'next' => $next
        ];
    }
}
