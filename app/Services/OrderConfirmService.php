<?php

namespace App\Services;

use DateTime;
use Carbon\Carbon;
use App\Models\TMS\Posein;
use App\Services\BaseService;
use App\Models\yherp\ItemReceiptM;
use App\Models\yherp\OrderConfirmM;

class OrderConfirmService extends BaseService
{
    private $s_date = '';
    private $e_date = '';

    //取得index view data
    public function GetIndexVD()
    {
        $range = 7;
        $this->s_date = Carbon::today()->addDays(-$range)->format('Y-m-d 00:00:00');
        $this->e_date = Carbon::today()->format('Y-m-d 23:59:59');

        $confirmed = OrderConfirmM::with('Details')
            ->whereBetween('created_at', [$this->s_date, $this->e_date])
            ->get();
        $receipt = $this->GetReceipt($confirmed);
        $extra_order = $this->GetExtraOrder($confirmed);
        $unsafety_stock = $this->GetUnsafetyStock();
        $purchase_undone = $this->GetPurchaseUndone();

        return [
            'receipt' => $receipt,
            'extra_order' => $extra_order,
            'unsafety_stock' => $unsafety_stock,
            'purchase_undone' => $purchase_undone
        ];
    }

    //取得額外加單
    public function GetExtraOrder($confirmed)
    {
        $confirmed_list = $confirmed->where('order_type', 8)->pluck('Details')->collapse()->pluck('order_no')->unique()->toArray();
        $db = Posein::select('PCOD1')
            ->where('PTIME', '>=', Carbon::today()->format('Y-m-d 15:00:00'))
            ->get();
        foreach ($db as $key => $d) {
            if (in_array($d->sale_no, $confirmed_list)) {
                $db->forget($key);
            }
        }
        return $db;
    }

    //取得收料作業
    public function GetReceipt($confirmed)
    {
        $confirmed_list = $confirmed->where('order_type', 4)->pluck('Details')->collapse()->pluck('order_no')->unique()->toArray();
        $db = ItemReceiptM::with('Details')
            ->with(['Details.Item' => function ($q) {
                $q->select('ICODE', 'IPCOD', 'IPNAM');
            }])
            ->whereBetween('created_at', [$this->s_date, $this->e_date])
            ->get();
        foreach ($db as $key => $d) {
            if (in_array($d->id, $confirmed_list)) {
                $db->forget($key);
            }
        }
        return $db;
    }

    //取得未確認的低於安庫商品
    public function GetUnsafetyStock(array $items = null)
    {
        $data = (new StockService())->GetUnsafetyStock($items)
            ->filter(function ($item, $idx) {
                $order_require = $item['order_require'];
                $s_stock = $item['s_stock'];
                return  $order_require > 0 && $order_require > $s_stock; //有訂單需求且須調撥
            });
        //確認有效期限6天，1號確認，8號需再確認
        $date = new DateTime();
        $date->modify('-6 day');
        $date = $date->format('Y-m-d 00:00:00');
        $confirmed_list = OrderConfirmM::with('Details')
            ->where('order_type', 9)
            ->where('created_at', '>=', $date)
            ->get()
            ->pluck('Details')->collapse()->pluck('order_no')->unique()->toArray();

        return $data->whereNotIn('item_no', $confirmed_list);
    }

    //應採購未採購覆核
    public function GetPurchaseUndone()
    {
        $s_date = (new DateTime())->format('Y-m-d 00:00:00');
        $e_date = (new DateTime())->format('Y-m-d 23:59:59');
        return OrderConfirmM::where('order_type', 12)->whereBetween('created_at', [$s_date, $e_date])->count() > 0;
    }
}
