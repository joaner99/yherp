<?php

namespace App\Services;

use DateTime;
use Carbon\Carbon;
use App\Models\TMS\Posein;
use Illuminate\Http\Request;
use App\Services\BaseService;
use App\Models\yherp\OrderSampleM;
use Illuminate\Support\Facades\DB;
use App\Services\OrderSampleService;

class ToolService extends BaseService
{
    public function get_floor_sales(Request $request)
    {
        $s_date = $request->get('start_date', Carbon::today()->modify('-30 day')->format('Y-m-d'));
        $e_date = $request->get('end_date', Carbon::today()->format('Y-m-d'));
        $tw_s_date = $this->GetTWDateStr(new DateTime($s_date));
        $tw_e_date = $this->GetTWDateStr(new DateTime($e_date));
        $itme_sub = DB::table('ITEM AS I')
            ->select('I.ICODE')
            ->join('ITEM1 AS I1', 'I1.ICODE1', 'I.ICODE')
            ->where('I.ITCOD', '11')
            ->where(function ($q) {
                $q
                    ->where('I1.ITCO3', '046') //LVT
                    ->orWhere(function ($q2) { //SPC
                        $q2
                            ->where('I1.ITCO3', '014')
                            ->where('I.INAME', 'LIKE', '%優居-SPC石晶卡扣地板-8片(整包)%');
                    });
            });
        $web_sub = DB::table('HISTIN AS D')
            ->select('I1.ITCO3', 'D.HINUM AS QTY')
            ->join('ITEM1 AS I1', 'I1.ICODE1', 'D.HICOD')
            ->join('POSEIN AS M', 'M.PCOD1', 'D.HCOD1')
            ->leftJoin('POSEOU AS MR', function ($q) {
                $q
                    ->on('MR.PCOD2', 'M.PCOD2')
                    ->where('MR.PCOD2', '!=', '');
            })
            ->whereBetween('D.HDAT1', [$tw_s_date, $tw_e_date])
            ->whereNull('MR.PCOD1')
            ->whereIn('D.HICOD', $itme_sub);
        $pos_sub = DB::table('PosDetailsTemp AS D')
            ->select('I1.ITCO3', 'D.Quantity AS QTY')
            ->join('ITEM1 AS I1', 'I1.ICODE1', 'D.ICODE')
            ->join('PosMainTemp AS M', 'M.MainID', 'D.MainID')
            ->leftJoin('PosReturnMainTemp AS MR', 'MR.MainID', 'M.MainID')
            ->whereBetween('D.CREATE_Date', [$s_date, $e_date])
            ->whereNull('MR.ReturnMainID')
            ->whereIn('D.ICODE', $itme_sub);
        $db = $web_sub->unionAll($pos_sub)->get();
        $data = [];
        foreach ($db->groupBy('ITCO3') as $ITCO3 => $sales) {
            if (empty($ITCO3)) {
                continue;
            } else {
                $data[$ITCO3] = $sales->sum('QTY');
            }
        };
        $floors = $itme_sub->addSelect('I.INAME', 'I1.ITCO3')->get();
        return ['data' => $data, 'floors' => $floors];
    }

    public function get_floor_color_area()
    {
        $orderSampleService = new OrderSampleService();
        $floor_short_name_config = $orderSampleService->configService->GetSysConfig('floor_short_name');
        $result = ['新竹&桃園&台北&新北&基隆' => ['SPC' => [], 'LVT' => []], '台南&高雄&屏東' => ['SPC' => [], 'LVT' => []]];
        $db = DB::table('POSEIN AS M')
            ->select('M.PCOD1', 'M.PJONO', 'M.PADDR', 'D.HICOD', 'D.HINAM', 'D.HINUM', 'I1.ITCO3')
            ->join('HISTIN AS D', 'D.HCOD1', 'M.PCOD1')
            ->join('ITEM1 AS I1', 'I1.ICODE1', 'D.HICOD')
            ->whereBetween('M.PDAT1', ['113.01.01', '113.08.31'])
            ->where('D.HYCOD', 11)
            ->whereIn('I1.ITCO3', ['014', '046']);
        $db_2 = clone $db;
        $db = $db->where('M.PADDR', '!=', '')->get();
        foreach ($db as $d) {
            //取得地址
            $addr = $d->PADDR;
            if (empty($addr)) {
                continue;
            }
            $addr = str_replace('台', '臺', $addr);
            //判斷地區
            $area = '';
            if (mb_strpos($addr, '新竹') !== false || mb_strpos($addr, '桃園') !== false || mb_strpos($addr, '台北') !== false || mb_strpos($addr, '新北') !== false || mb_strpos($addr, '基隆') !== false) { //北
                $area = '新竹&桃園&台北&新北&基隆';
            } else if (mb_strpos($addr, '台南') !== false || mb_strpos($addr, '高雄') !== false || mb_strpos($addr, '屏東') !== false) {
                $area = '台南&高雄&屏東';
            } else {
                continue;
            }
            //判斷類型
            switch ($d->ITCO3) {
                case '014':
                    $kind = 'SPC';
                    break;
                case '046':
                    $kind = 'LVT';
                    break;
                default:
                    $kind = '??';
                    break;
            }
            //判斷顏色
            $qty = round($d->ITCO3 == '046' ? $d->HINUM / 10 : $d->HINUM, 2);
            $item_name = $this->GetFloorColor($d->HINAM, $floor_short_name_config); //取得地板名稱縮寫
            if (key_exists($item_name, $result[$area][$kind])) {
                $result[$area][$kind][$item_name] += $qty;
            } else {
                $result[$area][$kind][$item_name] = $qty;
            }
            arsort($result[$area][$kind]);
        }
        $db_2 = $db_2->where('M.PADDR', '')->get();
        $order_list = $db_2->pluck('PJONO')->toArray();
        $os_db = OrderSampleM::whereIn('order_no', $order_list)->get();
        foreach ($db_2 as $d) {
            //取得地址
            $order_sample = $os_db->where('order_no', $d->PJONO)->first();
            if (empty($order_sample)) {
                continue;
            }
            $columns = $orderSampleService->GetOrderSampleColumns(json_decode($order_sample->header), $order_sample->src);
            $original_data = json_decode($order_sample->original_data);
            $addr = OrderSampleService::GetAddr($order_sample->src, $original_data[0], $columns);
            $addr = str_replace('台', '臺', $addr);
            //判斷地區
            $area = '';
            if (mb_strpos($addr, '新竹') !== false || mb_strpos($addr, '桃園') !== false || mb_strpos($addr, '台北') !== false || mb_strpos($addr, '新北') !== false || mb_strpos($addr, '基隆') !== false) { //北
                $area = '北';
            } else if (mb_strpos($addr, '台南') !== false || mb_strpos($addr, '高雄') !== false || mb_strpos($addr, '屏東') !== false) {
                $area = '南';
            } else {
                continue;
            }
            //判斷類型
            switch ($d->ITCO3) {
                case '014':
                    $kind = 'SPC';
                    break;
                case '046':
                    $kind = 'LVT';
                    break;
                default:
                    $kind = '??';
                    break;
            }
            //判斷顏色
            $qty = round($d->ITCO3 == '046' ? $d->HINUM / 10 : $d->HINUM, 2);
            $item_name = $this->GetFloorColor($d->HINAM, $floor_short_name_config); //取得地板名稱縮寫
            if (key_exists($item_name, $result[$area][$kind])) {
                $result[$area][$kind][$item_name] += $qty;
            } else {
                $result[$area][$kind][$item_name] = $qty;
            }
            arsort($result[$area][$kind]);
        }
        return ['data' => $result];
    }

    //親送、司機地區品項加總
    public function get_personal_transport_count()
    {
        $result = [];
        $orderSampleService = new OrderSampleService();
        $transportService = new TransportService();
        $db = Posein::select('PCOD1', 'PJONO')
            ->with(['Histin:HCOD1,HICOD,HINAM,HINUM,HYCOD', 'Histin.ITEM1:ICODE1,ITCO3', 'OriginalOrder', 'PersonalTransport', 'PersonalTransport.User'])
            ->whereBetween('PDAT1', ['113.12.31', '113.12.31'])
            ->where('TransportCode', 'COM')
            ->get();
        foreach ($db as $d) {
            //判斷司機
            $pt = $d->PersonalTransport;
            $driver_name = '';
            if (empty($pt)) {
                $driver_name = '林翔逸';
            } else {
                $driver_name = $pt->User->name ?? '';
                if ($driver_name == '實習1') {
                    $driver_name = '蕭建興';
                } elseif ($driver_name == '實習2' || $driver_name == '實習3') {
                    $driver_name = '廖振皓';
                } elseif (empty($driver_name)) {
                    continue;
                }
            }
            if (!key_exists($driver_name, $result)) {
                $result[$driver_name] = [];
            }
            //判斷地區
            if (!empty($pt) && !empty($pt->additional_addr)) { //額外地址為主
                $receiver_address = $pt->additional_addr;
            } else if (!empty($d->OriginalOrder)) {
                $order_sample = $d->OriginalOrder;
                $columns = $orderSampleService->GetOrderSampleColumns(json_decode($order_sample->header), $order_sample->src);
                $original_data = json_decode($order_sample->original_data);
                $receiver_address = OrderSampleService::GetAddr($order_sample->src, $original_data[0], $columns);
            } else {
                $receiver_address = '';
            }
            if (empty($receiver_address)) {
                $area = '其他';
            } else {
                $area = $transportService->GetArea($receiver_address);
            }
            if (!key_exists($area, $result[$driver_name])) {
                $result[$driver_name][$area] = [];
            }
            //判斷內容物
            foreach ($d->Histin as $h) {
                //只取SPC、LVT、塑木地板、貓砂
                if (!in_array($h->HYCOD, ['11', '17'])) {
                    continue;
                } elseif ($h->HYCOD == '11' && in_array($h->HICOD, ['11010047015', '11010047016', '11010047017', '11010047018', '11010047019'])) {
                    continue;
                } elseif ($h->HYCOD == '17' && !in_array($h->HICOD, ['17010041038', '17010041039', '17010041040', '17010041063', '17010041064', '17010041065'])) {
                    continue;
                }
                $qty = round($h->HINUM);
                $floor_kind = '';
                if ($h->HYCOD == '11') {
                    switch ($h->ITEM1->ITCO3) {
                        case '014':
                            $floor_kind = 'SPC';
                            break;
                        case '046':
                            $floor_kind = 'LVT';
                            $qty = round($qty / 10);
                            break;
                        case '047':
                            $floor_kind = '塑木';
                            $qty = round($qty / 11);
                            break;
                    }
                } elseif ($h->HYCOD == '17') {
                    $floor_kind = '貓砂';
                    $qty = round($qty / 8);
                }
                if (empty($floor_kind)) {
                    continue;
                }
                if (!key_exists($floor_kind, $result[$driver_name][$area])) {
                    $result[$driver_name][$area][$floor_kind] = $qty;
                } else {
                    $result[$driver_name][$area][$floor_kind] += $qty;
                }
            }
        }

        return ['data' => $result];
    }
}
