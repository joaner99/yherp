<?php

namespace App\Services;

use Carbon\Carbon;
use App\Mail\TableMail;
use Illuminate\Http\Request;
use App\Services\BaseService;
use App\Models\yherp\ScheduleLog;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class ScheduleService extends BaseService
{

    /**
     * 紀錄排程執行成功LOG
     *
     * @param string $cmd 指令
     * @param string $name 排程名稱
     * @return void
     */
    public function CreateOnSuccessLog(string $cmd, string $name)
    {
        $scheduleLog = new ScheduleLog();
        $scheduleLog->cmd = $cmd;
        $scheduleLog->name = $name;
        $scheduleLog->status = 'T';
        $scheduleLog->save();
    }

    /**
     * 紀錄排程執行錯誤LOG
     *
     * @param string $cmd 指令
     * @param string $name 排程名稱
     * @param string $status 排程執行狀態
     * @param string $msg 訊息
     * @return void
     */
    public function CreateOnFailureLog(string $cmd, string $name, string $msg)
    {
        $scheduleLog = new ScheduleLog();
        $scheduleLog->cmd = $cmd;
        $scheduleLog->name = $name;
        $scheduleLog->status = 'F';
        $scheduleLog->msg = $msg;
        $scheduleLog->save();

        $mailData =
            [
                'subject' => '排程執行錯誤通知',
                'caption' => '錯誤內容',
                'thead' => ['cmd', 'name', 'msg'],
            ];
        $mailData['tbody'] = [
            [$cmd, $name, $msg]
        ];
        Mail::to(['dante@sunstar-dt.com'])->send(new TableMail($mailData));
    }

    /**
     * 取得排程紀錄
     *
     * @param Request $request
     * @return array
     */
    public function GetLog(Request $request)
    {
        $sDate = $request->get('start_date', Carbon::today()->format('Y-m-d')) . ' 00:00:00';
        $eDate = $request->get('end_date', Carbon::today()->format('Y-m-d')) . ' 23:59:59';
        $db = ScheduleLog::whereBetween('created_at', [$sDate, $eDate])->orderBy('created_at', 'DESC')->get();

        return ['data' => $db];
    }

    //取得商品銷售
    public function GetAllItemsSale()
    {
        $e_date = Carbon::today()->addDays(-1);
        $s_date = Carbon::today()->addDays(-1)->addYears(-1);
        $tw_e_date = $this->GetTWDateStr($e_date);
        $tw_s_date = $this->GetTWDateStr($s_date);
        $pos_sub = DB::table('PosDetailsTemp')
            ->select('ICODE', DB::raw("Quantity*SaleNoTax AS 'Total'"))
            ->where('SaleNoTax', '>', 0)
            ->whereBetween('CREATE_Date', [$s_date, $e_date]);
        $web_sub = DB::table('HISTIN')
            ->select('HICOD', 'HTOTA')
            ->where('HTOTA', '>', 0)
            ->whereBetween('HDAT1', [$tw_s_date, $tw_e_date]);
        $result = DB::table(DB::raw("({$this->baseRepository->GetSqlWithParameter($web_sub->unionAll($pos_sub))}) AS T"))
            ->select('T.HICOD AS item_no', DB::raw("SUM(T.HTOTA) AS 'sales'"))
            ->whereExists(function ($q) {
                $q
                    ->select(DB::raw(1))
                    ->from('ITEM')
                    ->whereColumn('ICODE', 'T.HICOD')
                    ->where('ITCOD', '!=', 98);
            })
            ->groupBy('T.HICOD');
        return $result->get();
    }
}
