<?php

namespace App\Services;

use App\Models\TMS\Posein;
use App\Services\BaseService;
use Illuminate\Support\Facades\Auth;

class CinForestService extends BaseService
{
    public const status = [];

    public function GetOrder()
    {
        $editable = Auth::check() && Auth::user()->can('order_modify');
        $db = Posein::select('PCOD1', 'PJONO', 'PDAT1', 'PBAK3', 'InsideNote', 'ConsignTran')
            ->with(['Histin' => function ($q) {
                $q->select('HCOD1', 'HINAM', 'HINUM')
                    ->whereNotIn('HYCOD', ['98', '99']); //排除雜類
            }, 'Cinforest'])
            ->whereHas('Histin', function ($q) {
                $q->where('HICOD', '20025025001'); //產品料號
            })
            ->where('TransportCode', 'MF') //廠商直寄
            ->where('ConsignTran', '')
            ->get();

        foreach ($db as &$d) {
            if (empty($d->Cinforest)) { //廠商尚未填寫物流單號
                $status = 1;
            } else { //廠務尚未回填物流單號
                $status = 2;
            }
            $d->status = $status;
        }

        return ['data' => $db, 'editable' => $editable];
    }
}
