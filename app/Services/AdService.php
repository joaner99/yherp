<?php

namespace App\Services;

use DateTime;
use Exception;
use Carbon\Carbon;
use App\Models\yherp\AdCost;
use Illuminate\Http\Request;
use App\Services\BaseService;
use App\Models\yherp\AdChannel;
use App\Models\yherp\AdProfitD;
use App\Models\yherp\AdProfitM;
use Illuminate\Support\Facades\DB;

class AdService extends BaseService
{
    /**
     * shop_id對應平台平稱
     *
     * @var array
     */
    public const shopee_shop = ['16890831' => '蝦皮1店', '210034311' => '蝦皮2店', '457889190' => '蝦皮3店'];

    /**
     * shop_id對應客代
     *
     * @var array
     */
    private const shopee_cust = ['16890831' => 'S001', '210034311' => 'S003', '457889190' => 'S004'];

    /**
     * 取得廣告花費資料
     *
     * @param Request $request
     * @return array
     */
    public function GetAdCost(Request $request)
    {
        $result = array();

        $now = new DateTime('now');
        $date = new DateTime($request->date);
        if ($date->format('Y-m') == $now->format('Y-m')) { //目前月份
            $now->modify('-1 day'); //因當天資料不齊，故不取
            $sDate = new DateTime($now->format('Y/m/01'));
            $eDate = new DateTime($now->format('Y/m/d')); //取到當天
        } else {
            $sDate = new DateTime($date->format('Y/m/01'));
            $eDate = new DateTime($date->format('Y/m/t')); //取到當月最後一天
        }
        $twSDate = $this->GetTWDateStr($sDate);
        $twEDate = $this->GetTWDateStr($eDate);

        //設定檔
        $config = $this->configService->GetAdCost($date);
        $salesTarget =  $config->salesTarget ?? 4000000; //營業額目標
        $adThreshold = $config->adThreshold ?? 0.13; //廣告門檻
        $chargeThreshold = $config->chargeThreshold ?? 0.035; //手續費門檻
        $activityThresholdConfig = $config->activityThreshold; //活動費門檻
        $bonusConfig = $config->bonus ?? 0.1; //獎金比例
        $customer = $this->configService->GetSysConfig('customer'); //客戶代碼

        //營業額
        $sales = $this->baseRepository->GetSalesOfCustomer($twSDate, $twEDate);
        $salesTotal = $this->baseRepository->GetSalesTotalTax($twSDate, $twEDate); //已達成的營業額
        $salesDiff = $salesTotal >= $salesTarget ? 0 : ($salesTarget - $salesTotal); //未達成的營業額

        //廣告花費
        $adCost = array();
        $adCostDb = $this->baseRepository->GetAdCost($this->GetLogDateStr($sDate), $this->GetLogDateStr($eDate));
        foreach ($adCostDb->groupBy('ad_channel_id') as $ad) {
            $adCost[] = ['name' => $ad->first()->Channel->name . ' 廣告費', 'cost' => $ad->sum('ad')];
        }

        //手續費
        $adCost[] = ['name' => '手續費', 'cost' => $salesTotal * $chargeThreshold];

        //活動費
        if (!empty($activityThresholdConfig)) {
            foreach ($activityThresholdConfig as $value) {
                $code = $value->key;
                $val = $value->value;

                //取得客戶姓名
                $custConfig = $customer->firstWhere('value1', $code);
                if (empty($custConfig)) {
                    $name = '未知';
                } else {
                    $name = $custConfig->value2;
                }
                $value->text = $name;
                //費用
                $target = $sales->firstWhere('HPCOD', $code);
                if (!empty($target)) {
                    $cost = $target->HTOTA * $val;
                    $adCost[] = ['name' => $name . ' 活動費', 'cost' => $cost];
                }
            }
        }

        //廣告花費統計
        $adCostTotal = collect($adCost)->sum('cost');

        //廣告預算
        $adBudget =  $salesTarget * $adThreshold;
        $adRemainingBudget = $adBudget - $adCostTotal;

        //獎金
        $bonus = 0;
        $bonusLeft = 0;
        if ($salesDiff == 0 && $adRemainingBudget > 0) {
            $bonus = $adRemainingBudget * $bonusConfig;
            $bonusLeft = $adRemainingBudget * (1 - $bonusConfig);
        }

        //ViewData
        $result = [
            'sDate' => $this->GetLogDateStr($sDate),
            'eDate' => $this->GetLogDateStr($eDate),
            'sales' => [
                'total' => $salesTotal,
                'diff' => $salesDiff
            ],
            'adCost' => $adCost,
            'adCostTotal' => $adCostTotal,
            'adBudget' => ['budget' => $adBudget, 'remaining' => $adRemainingBudget],
            'bonus' => ['value' => $bonus, 'left' => $bonusLeft],
            'config' => [
                'salesTarget' => $salesTarget,
                'adThreshold' => $adThreshold,
                'chargeThreshold' => $chargeThreshold,
                'activityThreshold' => $activityThresholdConfig,
                'bonus' => $bonusConfig
            ],
        ];

        return $result;
    }

    /**
     * 取得廣告花費
     *
     * @param Request $request
     * @return void
     */
    public function GetAdConfigCost(Request $request)
    {
        $date = $request->get('date', (new Datetime)->modify('-1 month')->format('Y-m'));
        $s_date = $date . '-01';
        $e_date = $date . '-' . (new Datetime($date))->format('t');
        $db = $this->baseRepository->GetAdCost($s_date, $e_date);
        return $db;
    }

    /**
     * 取得各平台物流運費分攤
     *
     * @param Request $request
     * @return array
     */
    public function GetChannelSales(Request $request)
    {
        $freight = $request->get('freight');
        $date = $request->get('src_date');
        $s_date = $date . '-01';
        $e_date = $date . (new DateTime($date))->format('-t');
        $tw_s_date = $this->GetTWDateStr(new DateTime($s_date));
        $tw_e_date = $this->GetTWDateStr(new DateTime($e_date));
        $pos_sub = DB::table('PosDetailsTemp AS D')
            ->select(DB::raw("'POS' AS HPCOD"), 'D.SaleMoneyTotal AS HTOTA')
            ->join('ITEM AS I', 'I.ICODE', 'D.ICODE')
            ->whereBetween('D.CREATE_Date', [$s_date, $e_date])
            ->whereNotIn('I.ITCOD', ['98', '99'])
            ->whereNotExists(function ($q) { //排除退貨
                $q->select(DB::raw(1))
                    ->from('PosReturnMainTemp AS R')
                    ->whereRaw('R.MainID = D.MainID');
            });
        $db = DB::table('HISTIN AS D')
            ->select('D.HPCOD', 'D.HTOTA')
            ->join('POSEIN AS M', 'M.PCOD1', 'D.HCOD1')
            ->whereBetween('D.HDAT1', [$tw_s_date, $tw_e_date])
            ->whereNotIn('D.HYCOD', ['98', '99'])
            ->whereIn('D.HPCOD', ['S001', 'S003', 'S004', 'S005', 'S002', 'S006', 'C001']) //蝦皮1、2、3、官網、面交
            ->whereNotExists(function ($q) { //排除退貨
                $q->select(DB::raw(1))
                    ->from('POSEOU AS R')
                    ->whereRaw("(R.PCOD2 != '' AND R.PCOD2 = M.PCOD2)");
            })
            ->unionAll($pos_sub)
            ->get();
        $total = $db->sum('HTOTA');
        if (empty($total)) {
            return [];
        }
        $result = [];
        foreach (
            $db->groupBy(function ($item, $key) {
                $src = '';
                if (in_array($item->HPCOD, ['S002', 'S006', 'C001', 'POS'])) { //官網、面交、POS
                    $src = '官網';
                } else { //'S001', 'S003', 'S004', 'S005'
                    $src = self::shopee_shop[array_search($item->HPCOD, self::shopee_cust)] ?? '';
                }
                return $src;
            }) as $src => $gd
        ) {
            $channel_id = AdChannel::where('name', $src)->first()->id ?? '';
            if (empty($channel_id)) {
                continue;
            }
            $result[$channel_id] = round($freight * $gd->sum('HTOTA') / $total);
        }
        return $result;
    }

    /**
     * 取得廣告效益
     *
     * @param Request $request
     * @return array
     */
    public function GetProfit(Request $request)
    {
        $date = $request->get('date', Carbon::today()->format('Y-m-d'));
        $shop_id = $request->get('shop_id', array_key_first(self::shopee_shop));
        $shop = self::shopee_shop[$shop_id] ?? '';
        $cust = self::shopee_cust[$shop_id] ?? '';
        $result = [];
        $ad_profit = AdProfitM::with(['Details' => function ($q) { //狀態=進行中、廣告類型=商品搜尋廣告
            $q->where('_status', '進行中')->where('ad_type', '商品搜尋廣告');
        }])->where('src_date', $date)->where('shop_id', $shop_id)->first();
        if (!empty($ad_profit)) {
            $tw_s_date = $this->GetTWDateStr(new DateTime($date));
            $tw_e_date = $this->GetTWDateStr(new DateTime($date));
            $orders = $this->baseRepository->GetOrderOfItem($tw_s_date, $tw_e_date, $cust);
            foreach ($ad_profit->Details->groupBy('item_id') as $ads) {
                foreach ($ads as $ad) {
                    //排除：對應不到TMS
                    if (count($ad->TMSItems) == 0) {
                        continue;
                    }
                    $idArray =  $ad->TMSItems->pluck('ICODE')->toArray();
                    $cost = $ad->cost; //花費

                    //ERP
                    $order = $orders->whereIn('OICOD', $idArray);
                    $erp_sales_qty = $order->sum('OINUM'); //ERP銷售數量
                    $erp_sales_amt = $order->sum('OTOTA'); //ERP銷售金額
                    if ($erp_sales_qty > 0) { //ERP平均成本
                        $erp_item_cost = $order->sum('COST') / $erp_sales_qty; //ERP商品總成本/ERP銷售數量
                    } else {
                        $erp_item_cost = null; //除0
                    }
                    if ($cost > 0) { //ERP roas
                        $erp_roas = $erp_sales_amt / $cost; //ERProas=ERP銷售金額/花費
                    } else {
                        $erp_roas = null;
                    }
                    if ($erp_item_cost != 0) { //ERP ROI
                        $erp_roi = ($erp_sales_amt - $erp_item_cost) / $erp_item_cost * 100; //ERPROI = (ERP銷售金額-ERP平均成本)/ERP平均成本*100%
                    } else {
                        $erp_roi = null;
                    }
                    //廣告
                    $sales_qty = $ad->sales2_qty; //廣告銷售數量=直接銷售數量
                    $sales_amt = $ad->sales2_amt; //廣告銷售金額=直接銷售金額
                    if ($cost > 0) { //廣告銷售roas
                        $roas = $sales_amt / $cost; //廣告銷售roas=廣告銷售金額/花費
                    } else {
                        $roas = null;
                    }
                    if ($erp_item_cost != 0) { //廣告銷售ROI
                        $roi = ($sales_amt - ($erp_item_cost * $sales_qty)) / $erp_item_cost * 100; //廣告銷售ROI = (廣告銷售金額-(ERP平均成本*廣告銷售數量))/ERP平均成本*100%
                    } else {
                        $roi = null;
                    }
                    //自然
                    $n_sales_qty = $erp_sales_qty - $sales_qty; //自然流量銷售數量=ERP銷售數量-廣告銷售數量
                    $n_sales_amt = $erp_sales_amt - $sales_amt; //自然流量銷售金額=ERP銷售金額-廣告銷售金額
                    if ($cost > 0) { //自然流量roas
                        $n_roas = $n_sales_amt / $cost; //自然流量roas=自然流量銷售金額/花費
                    } else {
                        $n_roas = null;
                    }
                    if ($erp_item_cost != 0) { //自然流量ROI
                        $n_roi = ($n_sales_amt - ($erp_item_cost * $n_sales_qty)) / $erp_item_cost * 100; //自然流量ROI = (自然流量金額-(ERP平均成本*自然流量銷售數量))/ERP平均成本*100%
                    } else {
                        $n_roi = null;
                    }

                    $result[] = [
                        'item_id' => $ad->item_id, //商品ID
                        'shop_id' => $shop_id,
                        'shop' => $shop, //渠道
                        'name' => $ad->item_name, //名稱
                        'cost' => $cost, //花費
                        'view_count' => $ad->view_count, //瀏覽數
                        'click_count' => $ad->click_count, //點擊數
                        'click_rate' => $ad->click_rate * 100, //點擊率
                        'erp_item_cost' => $erp_item_cost, //ERP平均成本
                        'convert2_rate' => $ad->convert2_rate * 100, //直接轉換率
                        'convert1_cost' => $ad->convert1_cost, //每一筆轉換的成本
                        'sales_qty' => $sales_qty, //廣告銷售數量=直接銷售數量
                        'sales_amt' => $sales_amt, //廣告銷售金額=直接銷售金額
                        'roas' => $roas, //廣告銷售roas
                        'roi' => $roi, //廣告銷售roi
                        'n_sales_qty' => $n_sales_qty, //自然流量銷售數量=ERP銷售數量-廣告銷售數量
                        'n_sales_amt' => $n_sales_amt, //自然流量銷售金額=ERP銷售金額-廣告銷售金額
                        'n_roas' => $n_roas, //自然流量roas
                        'n_roi' => $n_roi, //自然流量roi
                        'erp_sales_qty' => $erp_sales_qty, //自然流量銷售數量=ERP銷售數量-廣告銷售數量
                        'erp_sales_amt' => $erp_sales_amt, //自然流量銷售金額=ERP銷售金額-廣告銷售金額
                        'erp_roas' => $erp_roas, //自然流量roas
                        'erp_roi' => $erp_roi, //自然流量roi
                    ];
                }
            }
        }

        return ['data' => $result, 'shops' => self::shopee_shop];
    }

    /**
     * 取得指定商品的銷售趨勢
     *
     * @param Request $request
     * @return array
     */
    public function GetAdSales(Request $request)
    {
        $result = [];
        $msg = '';
        try {
            $shop_id = $request->get('shop_id');
            $item_id = $request->get('item_id');
            $start_date = $request->get('start_date');
            $end_date = $request->get('end_date');
            if (empty($item_id) || empty($start_date) || empty($end_date)) {
                $msg = '錯誤。起日、訖日、商品ID任一為空';
            } else {
                $days = $this->GetEachDay($start_date, $end_date);
                $tms_items = $this->baseRepository->GetShopeeProduct($shop_id, $item_id)->pluck('ICODE')->toArray();
                if (count($tms_items) == 0) {
                    $msg = '錯誤。找不到任何對應的TMS商品';
                } else {
                    $cost = ['花費'];
                    $sales = ['銷售額'];
                    //取得個商品銷售
                    $tw_s_date = $this->GetTWDateStr(new DateTime($start_date));
                    $tw_e_date = $this->GetTWDateStr(new DateTime($end_date));
                    $orders = $this->baseRepository->GetOrderByItems($tw_s_date, $tw_e_date, self::shopee_cust[$shop_id] ?? '', $tms_items);
                    $profits = AdProfitM::withWhereHas('Details', function ($q) use ($item_id) {
                        $q->select('cost')->where('item_id', $item_id)->where('_status', '進行中')->where('ad_type', '商品搜尋廣告');
                    })
                        ->whereBetween('src_date', [$start_date, $end_date])->orderBy('src_date')->get();
                    foreach ($days as $day) {
                        //廣告花費
                        $profit = $profits->where('src_date', $day)->first();
                        if (empty($profit)) {
                            $cost[] = '0';
                        } else {
                            $cost[] = $profit->Details->first()->cost;
                        }
                        //商品銷售額
                        $tw_day = $this->GetTWDateStr(new DateTime($day));
                        $order = $orders->firstWhere('ODATE', $tw_day);
                        if (empty($order)) {
                            $sales[] = '0';
                        } else {
                            $sales[] = round($order->OTOTA);
                        }
                    }
                    $result['x'] = $days; //X軸
                    $result['y'] = [$cost, $sales]; //Y軸
                }
            }
        } catch (Exception $ex) {
            $msg =  $ex->getMessage();
        }
        return ['data' => $result, 'msg' => $msg];
    }

    /**
     * 取得廣告模擬資料
     *
     * @param Request $request
     * @return array
     */
    public function GetSimulation(Request $request)
    {
        $groups = json_decode($request->get('groups', ''));
        $s_date = (new DateTime($request->get('start_date', Carbon::today()->modify('-1 month')->format('Y-m-d'))))->format('Y-m-01');
        $e_date = (new DateTime($request->get('end_date', Carbon::today()->modify('-1 month')->format('Y-m-d'))))->format('Y-m-t');
        $tw_s_date = $this->GetTWDateStr(new DateTime($s_date));
        $tw_e_date = $this->GetTWDateStr(new DateTime($e_date));
        $db = $this->GetItemSales($s_date, $e_date, $tw_s_date, $tw_e_date);
        $this->SetCost($s_date, $e_date, $db);
        $result = [];
        foreach ($db->groupBy('src') as $gd) {
            if (empty($groups) || count($groups) == 0) {
                $result[''][] = $this->GetGroupedData($gd);
            } else {
                foreach ($groups as $group) {
                    $g_key = join(',', $group);
                    $g_count = count($group);
                    if ($g_count >= 1) { //分1類
                        foreach ($gd->groupBy($group[0]) as $gd0) {
                            if ($g_count >= 2) { //分2類
                                foreach ($gd0->groupBy($group[1]) as $gd1) {
                                    if ($g_count >= 3) { //分3類
                                        foreach ($gd1->groupBy($group[2]) as $gd2) {
                                            $result[$g_key][] = $this->GetGroupedData($gd2);
                                        }
                                    } else {
                                        $result[$g_key][] = $this->GetGroupedData($gd1);
                                    }
                                }
                            } else {
                                $result[$g_key][] = $this->GetGroupedData($gd0);
                            }
                        }
                    } else {
                        continue;
                    }
                }
            }
        }

        return $result;
    }

    //取得各渠道的商品銷售、成本
    private function GetItemSales($s_date, $e_date, $tw_s_date, $tw_e_date)
    {
        //POS資料
        $pos_sub = DB::table('PosDetailsTemp AS D')
            ->select(DB::raw("'POS' AS HPCOD"))
            ->addSelect('I.ITCOD', DB::raw('RTRIM(LTRIM(I.ITNAM))'), 'I1.ITCO2', DB::raw('RTRIM(LTRIM(I1.ITNA2)) AS ITNA2'), 'I1.ITCO3', DB::raw('RTRIM(LTRIM(I1.ITNA3)) AS ITNA3'))
            ->addSelect('D.ICODE', 'D.SaleMoneyTotal AS HTOTA', DB::raw('(D.Quantity*I.ISOUR) AS HSOUR'))
            ->join('ITEM AS I', 'I.ICODE', 'D.ICODE')
            ->join('ITEM1 AS I1', 'I1.ICODE1', 'D.ICODE')
            ->whereBetween('D.CREATE_Date', [$s_date, $e_date])
            ->whereNotIn('I.ITCOD', ['98', '99'])
            ->whereNotExists(function ($q) { //排除退貨
                $q->select(DB::raw(1))
                    ->from('PosReturnMainTemp AS R')
                    ->whereRaw('R.MainID = D.MainID');
            });
        //WEB資料並結合POS
        $db = DB::table('HISTIN AS D')
            ->select('D.HPCOD')
            ->addSelect('D.HYCOD AS ITCOD', DB::raw('RTRIM(LTRIM(D.HYNAM)) AS ITNAM'), 'I1.ITCO2', DB::raw('RTRIM(LTRIM(I1.ITNA2)) AS ITNA2'), 'I1.ITCO3', DB::raw('RTRIM(LTRIM(I1.ITNA3)) AS ITNA3'))
            ->addSelect('D.HICOD AS ICODE', 'D.HTOTA', 'D.HSOUR')
            ->join('POSEIN AS M', 'M.PCOD1', 'D.HCOD1')
            ->join('ITEM1 AS I1', 'I1.ICODE1', 'D.HICOD')
            ->whereBetween('D.HDAT1', [$tw_s_date, $tw_e_date])
            ->whereNotIn('D.HYCOD', ['98', '99'])
            ->whereIn('D.HPCOD', ['S001', 'S003', 'S004', 'S005', 'S002', 'S006', 'C001']) //蝦皮1、2、3、官網、面交
            ->whereNotExists(function ($q) { //排除退貨
                $q->select(DB::raw(1))
                    ->from('POSEOU AS R')
                    ->whereRaw("(R.PCOD2 != '' AND R.PCOD2 = M.PCOD2)");
            })
            ->unionAll($pos_sub)
            ->get();
        //分組
        $result = [];
        foreach (
            $db->groupBy(function (&$item, $key) {
                if (in_array($item->HPCOD, ['S002', 'S006', 'C001', 'POS'])) { //官網、面交、POS
                    $item->src = '官網';
                    $item->src2 = '官網';
                } else { //'S001', 'S003', 'S004', 'S005'
                    $item->src = '蝦皮';
                    $item->src2 = self::shopee_shop[array_search($item->HPCOD, self::shopee_cust)] ?? '';
                }
                return $item->src2 . $item->ICODE;
            }) as $gd
        ) {
            $tmp = $gd->first();
            $tmp->HTOTA = $gd->sum('HTOTA');
            $tmp->HSOUR = $gd->sum('HSOUR');
            $result[] = $tmp;
        }
        return collect($result);
    }

    //設定廣告費、活動費、手續費、蝦皮運費、物流運費
    private function SetCost($s_date, $e_date, &$data)
    {
        //蝦皮廣告費
        $shopee_cost = [];
        $ad_profit =  AdProfitD::with('TMSItems', 'Shopee2TMSItems')->select('item_id', 'cost')->whereHas('Main', function ($q) use ($s_date, $e_date) {
            $q->whereBetween('src_date', [$s_date, $e_date]);
        })->whereNotIn('item_id', ['', 'N/A'])->where('cost', '>', 0)->get();
        foreach ($ad_profit->groupBy('item_id') as $ap) {
            $tms_items = $ap->first()->TMSItems;
            if (count($tms_items) == 0) { //蝦皮2店在TMS上找不到對應，需改來源
                $tms_items = $ap->first()->Shopee2TMSItems;
                if (count($tms_items) == 0) { //找不到任何對應料號
                    continue;
                }
            }
            $avg_cost = $ap->sum('cost') / count($tms_items);
            foreach ($tms_items as $tms) {
                if (empty($tms->ICODE)) {
                    continue;
                }
                if (key_exists($tms->ICODE, $shopee_cost)) {
                    $shopee_cost[$tms->ICODE] += $avg_cost;
                } else {
                    $shopee_cost[$tms->ICODE] = $avg_cost;
                }
            }
        }
        $ad_cost = AdCost::whereBetween('src_date', [$s_date, $e_date])->get(); //當月廣告花費
        foreach ($data->groupBy('src2') as $src2 => $gd) {
            $total = $gd->sum('HTOTA'); //該客代總銷售額
            $channel_id = AdChannel::where('name', $src2)->first()->id ?? '';
            $ac = $ad_cost->where('ad_channel_id', $channel_id)->first();
            if (!empty($ac) && !empty($total)) { //無花費 或 無銷售
                foreach ($gd as $d) {
                    $p = $d->HTOTA / $total; //占比
                    $d->p = $p;
                    $d->event = $ac->event * $p; //活動費
                    $d->handling = $ac->handling * $p; //手續費
                    $d->freight_shopee = $ac->freight_shopee * $p; //蝦皮運費
                    $d->freight = $ac->freight * $p; //物流運費'
                    if ($src2 == '官網') { //官網廣告費
                        $d->ad = $ac->ad * $p;
                    } else { //蝦皮廣告費
                        $d->ad = $shopee_cost[$d->ICODE] ?? 0;
                    }
                }
            }
        }
    }

    //取得模擬分組資料
    private function GetGroupedData($data)
    {
        $sales = $data->sum('HTOTA');
        $cost = $data->sum('HSOUR');
        $gross_profit = $sales - $cost;
        $gross_profit_rate = $sales == 0 ? null : $gross_profit / $sales * 100;
        $ad = $data->sum('ad');
        $event = $data->sum('event');
        $handling = $data->sum('handling');
        $freight_shopee = $data->sum('freight_shopee');
        $freight = $data->sum('freight');
        $net_income = $gross_profit - $ad - $event - $handling - $freight_shopee - $freight;
        $net_income_rate = $sales == 0 ? null : $net_income / $sales * 100;
        $ad_rate = $ad == 0 ? null : $sales / $ad;
        return [
            'src' => $data->first()->src,
            'kind1' => $data->first()->ITNAM,
            'kind2' => $data->first()->ITNA2,
            'kind3' => $data->first()->ITNA3,
            'sales' => $sales,
            'cost' => $cost,
            'gross_profit' => $gross_profit,
            'gross_profit_rate' => $gross_profit_rate,
            'ad' => $ad,
            'event' => $event,
            'handling' => $handling,
            'freight_shopee' => $freight_shopee,
            'freight' => $freight,
            'net_income' => $net_income,
            'net_income_rate' => $net_income_rate,
            'ad_rate' => $ad_rate,
        ];
    }
}
