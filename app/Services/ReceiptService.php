<?php

namespace App\Services;

use App\Models\TMS\Item;
use App\Services\BaseService;

class ReceiptService extends BaseService
{
    public const return_standard = 2500;

    //取得產品資訊
    public function GetItem($id)
    {
        $item_no = $this->baseRepository->ConvertToItemNo($id);
        if (empty($item_no)) {
            return null;
        } else {
            return Item::select('ICODE', 'INAME')->where('ICODE', $item_no)->first();
        }
    }

    //取得未進貨品項
    public function GetPurchaseNotInItems()
    {
        $items = $this->baseRepository->GetPurchaseNotInItems();
        return $items;
    }
}
