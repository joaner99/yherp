<?php

namespace App\Services;

use App\Models\TMS\Item;
use App\Services\BaseService;

class ProductService extends BaseService
{
    public $msg = '';
    public const negative_review = [
        1 => 'CP值',
        2 => '外觀',
        3 => '耐用度',
        4 => 'DIY操作難度',
        5 => '出貨檢驗',
        6 => '認證',
        7 => '影片解說',
        99 => '無',
    ];

    //廠務交付給財物前驗證
    public function ValidERPConfirm($data): bool
    {
        if (empty($data)) {
            $this->msg = '無任何資料可以交付';
            return false;
        }
        foreach ($data->Details as $d) {
            if (!isset($d->warranty_y) || !isset($d->warranty_m) || !isset($d->warranty_d)) {
                $this->msg = '保固時間未填寫';
                return false;
            } elseif (!empty(Item::where('INAME', $d->INAME)->first())) {
                $this->msg = '產品名稱『' . $d->INAME . '』重複';
                return false;
            }
        }
        return true;
    }

    //財務交付給行銷前驗證
    public function ValidERPConfirm2($data): bool
    {
        if (empty($data)) {
            $this->msg = '無任何資料可以交付';
            return false;
        }
        foreach ($data->Details as $d) {
            if (empty(Item::where('INAME', $d->INAME)->first())) {
                $this->msg = '產品名稱『' . $d->INAME . '』尚未匯入TMS';
                return false;
            }
        }
        return true;
    }
}
