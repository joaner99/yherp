<?php

namespace App\Services;

use App\User;
use DateTime;
use Carbon\Carbon;
use App\Models\TMS\STOC;
use App\Models\TMS\Order;
use App\Models\TMS\Posein;
use App\Models\TMS\Poseou;
use Illuminate\Http\Request;
use App\Services\BaseService;
use App\Models\yherp\ItemPlaceD;
use App\Models\yherp\SysConfigM;
use App\Models\yherp\LogShipment;
use App\Models\yherp\OrderSampleM;
use Illuminate\Support\Facades\DB;
use App\Models\yherp\OrderColumnIdx;
use App\Models\yherp\OrderSampleFile;
use App\Models\yherp\LogisticsAddrMark;

class OrderSampleService extends BaseService
{
    //平台來源
    public const SrcType = [1 => 'Shopline', 2 => '蝦皮1店', 3 => '蝦皮2店', 4 => '蝦皮3店', 5 => 'BV SHOP'];
    //特殊訂單匯出類型
    public const ExportType = ['1' => '廠務可隨時出貨訂單(A)', '2' => '業務同意可出貨訂單(B)', '3' => '出貨訂單(A+B)'];
    //物流類型
    public const TransportType = ['YCS' => '音速', 'HCT' => '新竹', 'COM' => '親送', 'SC' => '自取', 'MF' => '廠商直寄', 'BH' => '回頭車'];
    //訂單類別
    public const OrderKind = ['0' => '無', '1' => '預購', '2' => '棧板'];
    //舊版本的訂單
    public $old_ver_order = [];

    //取得網路訂單的地址
    public static function GetAddr($type, $row, $columns)
    {
        if (in_array($type, [0, 2, 3, 4, 5])) { //蝦皮、BV SHOP
            return $row[$columns['receiver_address']];
        } else { //shopline
            return $row[$columns['receiver_city']] . $row[$columns['receiver_district']] . $row[$columns['receiver_address']];
        }
    }

    /**
     * 取得物流地區標記
     *
     * @param string $kind 1=音速配送地區、2=新竹偏遠地區
     * @return array
     */
    public static function GetLogisticsAddrMark($kind)
    {
        $db = LogisticsAddrMark::with('County')->with('Area')->where('kind', $kind)->get();
        $result = [];
        foreach ($db as $d) {
            $county  = $d->County->name;
            $area = $d->Area->name;
            $result[$county][] = $area;
        }
        return $result;
    }

    /**
     * 取得已轉檔的特殊訂單
     *
     * @param string|null $sDate 起日
     * @param string|null $eDate 訖日
     * @param int $src 平台
     * @param $include_imported 是否包含已匯入之訂單
     * @return array
     */
    public function GetOrderSampleConvert(string $sDate = null, string $eDate = null, int $src = null, $include_imported = false)
    {
        $this->old_ver_order = [];
        $result = array();
        $db = OrderSampleM::where('completed', 0);
        if (!empty($sDate) && !empty($eDate)) {
            $db->whereBetween('order_date', [$sDate, $eDate]);
        }
        if (isset($src)) {
            $db->where('src', $src);
        }
        $db = $db->get();
        //該訂單編號，已有訂單單號，進行排除
        if (!$include_imported) {
            $exists_orders = Order::select('OCOD4')->whereIn('OCOD4', $db->pluck('order_no')->unique()->toArray())->pluck('OCOD4')->toArray();
            foreach ($db as $key => $value) {
                //TMS內有訂單資訊
                if (in_array($value->order_no, $exists_orders)) {
                    unset($db[$key]);
                }
            }
        }
        $item_all = $this->GetItemAllList()->pluck('INAME', 'ICODE')->toArray();
        //取得標題欄位設定
        $headerData = $this->configService->GetConfig('order_convert_columns');
        $hct_remote_config = OrderSampleService::GetLogisticsAddrMark('2');
        $today = new DateTime('midnight');
        $convert_items = (new ItemService())->GetItemConvert();
        $item_no_list = [];
        $require_qty = [];
        //遍歷所有訂單
        foreach ($db as $order) {
            $src = $order->src;
            if (empty($order->headerData)) { //無標題
                continue;
            } else if ($order->headerData !== $headerData[$src]) { //標題與目前設定不同
                $this->old_ver_order[] = $order->order_no;
                continue;
            } else {
                $header = $headerData[$src];
            }
            $columns = $this->GetOrderSampleColumns($header, $src);
            if (empty($columns)) {
                continue;
            }
            $orderArray = $order->toArray();
            $time_diff = $today->diff(new DateTime(substr($order->order_date, 0, 10)));
            $orderArray['expired'] = $time_diff->days >= 2;
            $orderArray['src_name'] = $order->src_name;
            $orderArray['original_data'] = $order->original_data; //toArray()不會解密資料，需手動解密並覆蓋
            $originalData = json_decode($order->original_data);
            if (empty($originalData)) { //無標頭資料不能正確分析
                continue;
            }
            //取第一筆原始資料的備註分析
            $firstRow = $originalData[0];
            if (count($firstRow) != count($header)) { //資料與標題欄位長度不一致
                continue;
            }
            //訂單類型判斷
            $orderArray['order_kind'] = $order->order_kind;
            $orderArray['customer_remark'] = $firstRow[$columns['customer_remark']];
            $orderArray['seller_remark'] = $firstRow[$columns['remark']];
            $orderArray['receiver_name'] = $firstRow[$columns['receiver_name']];
            $orderArray['receiver_tel'] = $firstRow[$columns['receiver_tel']];
            $orderArray['receiver_address'] = $firstRow[$columns['receiver_address']];
            $orderArray['receiver_city'] = key_exists('receiver_city', $columns) ? ($firstRow[$columns['receiver_city']] ?? '') : ''; //城市
            $orderArray['receiver_district'] = key_exists('receiver_district', $columns) ? ($firstRow[$columns['receiver_district']] ?? '') : ''; //行政區
            $orderArray['receiver_postal'] = key_exists('receiver_postal', $columns) ? ($firstRow[$columns['receiver_postal']] ?? '') : ''; //郵遞區號

            $item_total = 0;
            $genuine_item = array();
            foreach ($originalData as $item) {
                $item_total += $item[$columns['item_price']] * $item[$columns['item_quantity']];
                $genuine_item[] = [
                    'item_no' =>  $item[$columns['item_no']],
                    'item_name' => $item[$columns['item_name']],
                    'item_option_name' => $item[$columns['item_option_name']],
                    'item_price' => $item[$columns['item_price']],
                    'item_discount' => $item[$columns['item_discount']],
                    'item_quantity' => $item[$columns['item_quantity']],
                ];
            }
            //物流建議判斷，非超商，非排除商品
            $trans = '';
            $hct_remote = false;
            if (!in_array($firstRow[$columns['transport']], self::original_shop_trans[$src])) { //宅配
                //取得地址
                $addr = $this->GetAddr($src, $firstRow, $columns);
                if ($this->IsHctRemote($hct_remote_config, $addr)) { //新竹偏遠地區判斷
                    $hct_remote = true;
                }
                if (!empty($order->trans)) { //有指定物流
                    $trans = $order->trans;
                } else { //無指定物流，預設宅配為新竹
                    $trans = 'HCT';
                }
            } else { //超商禁止編輯
                $trans = -1;
            }
            $orderArray['trans'] = $trans;
            $orderArray['trans_name'] = self::TransportType[$trans] ?? '';
            $orderArray['hct_remote'] = $hct_remote;
            $orderArray['item_total'] = $item_total; //商品原價*數量
            $orderArray['genuine_item'] = json_encode($genuine_item);
            foreach ($genuine_item as $item) {
                //料號轉換，待優化
                if (array_key_exists($item['item_no'], $convert_items)) {
                    $orderArray['item_no'] = $convert_items[$item['item_no']];
                } else {
                    $orderArray['item_no'] = $item['item_no'];
                }
                $item_no_list[] = (string)$orderArray['item_no'];
                if (key_exists($orderArray['item_no'], $item_all)) {
                    $orderArray['item_name'] = $item_all[$orderArray['item_no']];
                } else {
                    $orderArray['item_name'] = $item['item_name'];
                }
                $orderArray['item_option_name'] = $item['item_option_name'];
                $orderArray['item_quantity'] = $item['item_quantity'];
                //加總需求
                if (key_exists($item['item_no'], $require_qty)) {
                    $require_qty[$item['item_no']] += $item['item_quantity'];
                } else {
                    $require_qty[$item['item_no']] = $item['item_quantity'];
                }
                $result[] = $orderArray;
            }
        }
        array_multisort(array_column($result, 'order_date'), $result); //訂單成立日期排序
        //超賣檢查
        $item_no_list = array_unique($item_no_list);
        //以TMS總庫存為主
        $stock_can_sale = $this->baseRepository->GetStockCanSale($item_no_list);
        foreach ($result as $key => $r) {
            $scs = $stock_can_sale->where('ICODE', $r['item_no'])->first();
            if (empty($scs)) {
                $result[$key]['out_of_stock'] = true;
                continue;
            }
            $stock = $scs->Stock;
            $order_need = $scs->OrderNeed ?? 0;
            $basket_need = $require_qty[$r['item_no']] ?? 0;
            $out_of_stock = ($stock - $order_need - $basket_need) < 0;
            $result[$key]['out_of_stock'] = $out_of_stock;
        }
        return $result;
    }

    /**
     * 特殊訂單轉檔下載
     *
     * @return array
     */
    public function GetDownloadView()
    {
        $result = OrderSampleFile::orderBy('created_at', 'DESC')->paginate(10);
        foreach ($result as $item) {
            $start_date = (new Carbon($item->start_date))->format('Y-m-d');
            $end_date = (new Carbon($item->end_date))->format('Y-m-d');
            $fileName = $start_date . '_' . $end_date;
            $item->start_date = $start_date;
            $item->end_date = $end_date;
            $item->color = '';
            if (key_exists($item->convert_type,  OrderSampleService::ExportType)) {
                $typeName = OrderSampleService::ExportType[$item->convert_type];
                $item->convert_type_name = $typeName;
                $fileName .= $typeName;
                switch ($item->convert_type) {
                    case '1':
                        $item->color = '';
                        break;
                    case '2':
                        $item->color = 'table-primary';
                        break;
                    case '3':
                        $item->color = 'table-success';
                        break;
                }
            }
            $item->convert_file_name = $fileName;
        }

        return [
            'data' => $result,
            'srcTypeList' => OrderSampleService::SrcType,
            'exportTypeList' => OrderSampleService::ExportType,
            'transportTypeList' => OrderSampleService::TransportType,
        ];
    }

    /**
     * 取的特殊訂單匯出資料
     *
     * @param string $sDate 起日
     * @param string $eDate 訖日
     * @param mixed $src 賣場來源
     * @param string $type 類型。1:廠務可隨時出貨訂單(A)、2:業務同意可出貨訂單(B)
     * @return array
     */
    public function GetExport(string $sDate, string $eDate, int $src, string $type)
    {
        $result = array();
        $orderSampleConvert = collect($this->GetOrderSampleConvert($sDate, $eDate, $src, false));
        $msg = '';
        if (count($this->old_ver_order) > 0) {
            foreach ($this->old_ver_order as $o) {
                $msg .= "『{$o}』";
            }
            $msg .= '以上訂單，因欄位版本不同將排除在外，請重新匯入訂單更新';
        }
        if (empty($orderSampleConvert) || $orderSampleConvert->count() == 0) {
            $this->CreateOrderSampleExportLog($sDate, $eDate, $src, $type, $result);
            return ['data' => $result, 'msg' => $msg];
        }
        $headerData = $this->configService->GetConfig('order_convert_columns')[$src];
        $result[] = $headerData;
        $columns = $this->GetOrderSampleColumns($headerData, $src);
        if ($type == 1 || $type == 3) { //廠務可隨時出貨訂單(A)、A+B
            //以訂單編號分組
            foreach ($orderSampleConvert->where('order_type', 0)->groupBy('order_no') as $group) {
                $firstOrder = $group->first();
                $originalData = json_decode($firstOrder['original_data']);
                $result = array_merge($result, $originalData);
            }
        }
        if ($type == 2 || $type == 3) { //業務同意可出貨訂單(B)、A+B
            //以訂單編號分組
            foreach ($orderSampleConvert->where('order_type', 1)->where('confirmed', 1)->groupBy('order_no') as $group) {
                $firstOrder = $group->first();
                $originalData = json_decode($firstOrder['original_data']);
                $genuine_item = json_decode($firstOrder['genuine_item']);
                //修改訂單單頭，取第一筆原始資料作為共通欄位的參考
                $firstOriginalData = $originalData[0];
                //修改訂單單身
                foreach ($genuine_item as $g) {
                    $firstOriginalData[$columns['item_no']] = $g->item_no; //商品選項貨號=正貨料號
                    $firstOriginalData[$columns['item_name']] = $g->item_name; //商品名稱(品)=正貨名稱
                    $firstOriginalData[$columns['item_option_name']] = $g->item_name; //商品選項名稱(品)=正貨名稱
                    $firstOriginalData[$columns['item_price']] = $g->item_price; //商品原價 (品)=正貨售價
                    $firstOriginalData[$columns['item_discount']] = $g->item_discount; //商品活動價格 (品)=正貨售價
                    $firstOriginalData[$columns['item_quantity']] = $g->item_quantity; //商品數量=正貨數量
                    $result[] = $firstOriginalData;
                }
            }
        }
        $convert_items = (new ItemService())->GetItemConvert();
        //依訂單分組處理
        //料號轉換、修改備註、修改物流
        foreach ($result as $key => &$order_data) {
            if ($key == 0) { //略過標題
                continue;
            }
            $orderNo = $order_data[$columns['order_no']];
            $order_convert = $orderSampleConvert->where('order_no', $orderNo)->first();
            if (array_key_exists($order_data[$columns['item_no']], $convert_items)) { //料號轉換
                $order_data[$columns['item_no']] = $convert_items[$order_data[$columns['item_no']]];
            }
            if (key_exists('user_account', $columns) && mb_strpos($order_data[$columns['remark']], '買家ID:') === false) { //備註補上買家ID
                $user_account = $order_data[$columns['user_account']];
                $order_data[$columns['remark']] .= "/買家ID:{$user_account}";
            }
            $order_data[$columns['remark']] .= "/外掛備註:";
            if ($order_convert['order_type'] == 0) { //免確認訂單加上備註
                $order_data[$columns['remark']] .= '【免確認】';
            }
            if ($order_convert['order_kind'] == 2) { //棧板訂單加上備註
                $order_data[$columns['remark']] .= '【棧板】';
            }
            if (!empty($order_convert['remark'])) { //補上撿貨單備註
                $order_data[$columns['remark']] .= $order_convert['remark'];
            }
            //指定物流覆寫
            $transport = $order_convert['trans'] ?? '';
            switch ($transport) {
                case 'YCS':
                    $transport_name = '音速物流';
                    break;
                case 'COM':
                    $transport_name = '親送';
                    break;
                case 'BH':
                    $transport_name = '回頭車';
                    break;
                default:
                    $transport_name = '';
                    break;
            }
            if (!empty($transport_name)) {
                $order_data[$columns['transport']] = $transport_name;
            }
            //BV SHOP 物流單號覆寫，因無資料預設為-導致系統誤判
            if ($src == 5 && $order_data[$columns['transport_no']] == '-') {
                $order_data[$columns['transport_no']] = '';
            }
        }
        //配件包處裡
        $acc_config = $this->configService->GetSysConfig('accessories_item'); //配件包料號
        if (!empty($acc_config) && count($acc_config) > 0) {
            $acc_config = $this->baseRepository->GetItemAllDetail($acc_config->pluck('value1')->toArray());
        }
        $need_acc_config = $this->configService->GetConfig('item_need_accessories'); //需配件的料號清單
        if (!empty($need_acc_config) && !empty($acc_config)) {
            foreach (collect($result)->slice(1)->groupBy($columns['order_no']) as $orderNo => $group) {
                //判斷訂單購買內容，是否有需要放置配件包
                $needAccessories = $group->filter(function ($item) use ($columns, $need_acc_config) {
                    $isNeed = false;
                    foreach ($need_acc_config as $cfg) {
                        $item_quantity = $cfg->key;
                        $item_no = $cfg->value;
                        //指地的料號 且 數量達到，放配件包
                        if (in_array($item[$columns['item_no']], $item_no) && $item[$columns['item_quantity']] >= $item_quantity) {
                            $isNeed = true;
                            break;
                        }
                    }
                    return $isNeed;
                });
                //需放配件包
                if ($needAccessories->count() > 0) {
                    $discount = 0;
                    $accessoriesData = $group->last();
                    $index = array_search($accessoriesData, $result);
                    if ($index !== false) { //找到原資料，後面插入配件包資料
                        foreach ($acc_config as $acc_cfg) {
                            $item_no = $acc_cfg->ICODE;
                            $item_name = $acc_cfg->INAME;
                            $item_price = $acc_cfg->IUNIT;
                            $discount += $item_price;
                            $accessoriesData[$columns['item_no']] = $item_no;
                            $accessoriesData[$columns['item_name']] = $item_name;
                            $accessoriesData[$columns['item_option_name']] = $item_name;
                            $accessoriesData[$columns['item_price']] = $item_price;
                            $accessoriesData[$columns['item_discount']] = $item_price;
                            $accessoriesData[$columns['item_quantity']] = 1;
                            $result = $this->array_insert($result, $index, $accessoriesData);
                            $index++;
                        }
                        //修改賣家折扣，以抵銷配件包價錢
                        $result = collect($result)->map(function ($item) use ($columns, $orderNo, $discount) {
                            if ($item[$columns['order_no']] == $orderNo) {
                                $item[$columns['discount']] += $discount;
                            }
                            return $item;
                        })->toArray();
                    }
                }
            }
        }
        //更新匯出時間
        $order_no_list = [];
        foreach (collect($result)->slice(1) as $r) {
            $order_no = $r[$columns['order_no']];
            if (!in_array($order_no, $order_no_list)) {
                $order_no_list[] = (string)$order_no;
            }
        }
        OrderSampleM::whereIn('order_no', $order_no_list)->update(['exported_at' => new DateTime()]);
        return ['data' => $result, 'msg' => $msg];
    }

    /**
     * 紀錄匯出資料
     *
     * @param string $sDate 起日
     * @param string $eDate 訖日
     * @param mixed $src 來源
     * @param string $type 匯出類型
     * @param array $data 匯出的資料
     * @return void
     */
    public function CreateOrderSampleExportLog(string $sDate, string $eDate, $src, string $type, $data)
    {
        $this->baseRepository->CreateOrderSampleExportLog($sDate, $eDate, $src, $type, json_encode($data));
    }

    /**
     * 取得特殊訂單轉檔畫面資料
     *
     * @return array
     */
    public function GetOrderSampleConvertView(Request $request)
    {
        $include_imported = $request->get('include_imported', 0) == 1;
        $data = $this->GetOrderSampleConvert(null, null, null, $include_imported);
        $items = $this->baseRepository->GetItemAll();
        $unique_data = collect($data)->unique('order_no');
        $expired_count = collect($data)->unique('order_no')->where('expired', true)->count();
        $statistics = [];
        foreach ($unique_data->groupBy('order_type_name') as $order_type_name => $group) {
            $statistics[$order_type_name] = $group->count();
        }
        return [
            'data' => $data,
            'old_ver_order' => $this->old_ver_order,
            'items' => $items,
            'statistics' => $statistics,
            'expired_count' => $expired_count,
            'srcTypeList' => OrderSampleService::SrcType,
            'kindList' => OrderSampleService::OrderKind,
            'transportTypeList' => OrderSampleService::TransportType,
            'config_url' => route('sys_config.edit', SysConfigM::where('_key', 'sp_item')->first()->id),
        ];
    }

    /**
     * 取得指定標頭的欄位索引
     *
     * @param mix $header 標頭陣列資料
     * @param mix $type 來源
     * @param boolean $return_error 是否回傳錯誤資料
     * @return array
     */
    public function GetOrderSampleColumns($header, $type, $return_error = false)
    {
        if (empty($header)) {
            return null;
        }
        $config = OrderColumnIdx::all();
        foreach ($config as $cfg) {
            switch ($type) {
                case 0: //蝦皮
                case 2: //蝦皮1店
                case 3: //蝦皮2店
                case 4: //蝦皮3店
                    $idx = $cfg->shopee_idx;
                    break;
                case 1: //shopline
                    $idx = $cfg->shopline_idx;
                    break;
                case 5: //BV SHOP
                    $idx = $cfg->bvshop_idx;
                    break;
                default:
                    $idx = null;
                    break;
            }
            if (empty($idx) || $idx == '\0') {
                continue;
            } else {
                $result[$cfg->key] = false; //預設false
                foreach ($header as $header_idx => $h) {
                    if (preg_replace('/\s(?=)/', '', $h) == preg_replace('/\s(?=)/', '', $idx)) { //去空白比較
                        $result[$cfg->key] = $header_idx;
                        continue 2;
                    }
                }
            }
        }
        foreach ($result as $r) {
            if ($r === false && $return_error == false) {
                return null;
            }
        }

        return $result;
    }

    /**
     * 取得指定標頭的欄位索引
     *
     * @param array $header 標頭陣列資料
     * @return array
     */
    public function GetOrderReturnRefundColumns(array $header)
    {
        if (empty($header)) {
            return null;
        }
        $result = [
            'return_no' => array_search('退貨編號', $header),
            'order_no' => array_search('訂單編號', $header),
            'order_time' => array_search('訂單成立時間', $header),
            'return_time' => array_search('退貨申請時間', $header),
            'return_status' => array_search('退貨 / 退款狀態', $header),
            'dispute_status' => array_search("爭議狀態\n(僅適用僅退款/快速退款流程之退貨)", $header),
        ];
        foreach ($result as $r) {
            if ($r === false) {
                return null;
            }
        }

        return $result;
    }


    /**
     * 取得特殊訂單超過時間未匯入資料
     *
     * @return \Illuminate\Support\Collection
     */
    public function GetSpOrderCheck()
    {
        //未完成的訂單
        $db = OrderSampleM::where('completed', 0)->get();
        if (count($db) == 0) {
            return collect();
        }
        //有銷貨紀錄且有開立發票且有上車
        $orderList = $db->pluck('order_no')->toArray();
        $completedOrderList = Posein::select('PCOD1', 'PJONO')
            ->whereHas('Order', function ($q) {
                $q->where('OINYN', 'Y')->where('OCANC', '已核示');
            })
            ->whereIn('PJONO', $orderList)
            ->where(function ($q) {
                //月底開立、不開立 或者 有開立發票
                $q->whereIn('POFFE', ['1', '2'])->orWhere('PTXCO', '!=', '');
            })
            ->pluck('PCOD1', 'PJONO')
            ->toArray();
        $shipOrderList = LogShipment::whereIn('PCOD1', array_values($completedOrderList))->pluck('PCOD1')->toArray();
        foreach ($completedOrderList as $orderNo => $sale_no) {
            if (in_array($sale_no, $shipOrderList)) {
                $order = $db->firstWhere('order_no', $orderNo);
                $order->completed = 1;
                $order->save();
            }
        }
        //重新整理，排除新的已完成
        $db = $db->where('completed', 0);
        //1.有退貨紀錄。回壓完成註記2.訂單取消
        $returnOrderList = Poseou::select('PJONO')->whereIn('PJONO', $orderList)->pluck('PJONO')->toArray();
        $cancelOrderList = Order::select('OCOD4')->whereIn('OCOD4', $orderList)->where('OCANC', '訂單取消')->pluck('OCOD4')->toArray();
        foreach (array_unique(array_merge($returnOrderList, $cancelOrderList)) as $orderNo) {
            $order = $db->firstWhere('order_no', $orderNo);
            if (!empty($order)) {
                $order->completed = 1;
                $order->save();
            }
        }
        //重新整理，排除新的已完成
        $db = $db->where('completed', 0);
        //取出超過兩天(含)的訂單
        foreach ($db as $key => $d) {
            $elapsedDays = (new DateTime())->diff(new DateTime($d->order_date))->days; //現在時間-訂單成立時間
            if ($elapsedDays < 2) { //排除小於兩天內的訂單
                $db->forget($key);
            } else {
                $d->elapsedDays = $elapsedDays;
            }
        }

        return $db;
    }
}
