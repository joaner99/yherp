<?php

namespace App\Services;

use App\User;
use DateTime;
use Exception;
use DateInterval;
use Carbon\Carbon;
use App\Models\TMS\Item;
use App\Models\TMS\STOC;
use App\Models\TMS\Order;
use App\Models\TMS\Posein;
use App\Models\yherp\Stock;
use Illuminate\Support\Str;
use App\Models\yherp\County;
use Illuminate\Http\Request;
use App\Models\yherp\Holiday;
use App\Models\yherp\ItemImg;
use App\Models\yherp\LogItem;
use App\Services\ConfigService;
use App\Models\yherp\CountyArea;
use App\Models\yherp\ItemConfig;
use App\Models\yherp\ItemPlaceD;
use App\Models\yherp\LogPackage;
use App\Models\yherp\SysConfigM;
use App\Models\yherp\LogShipment;
use App\Models\yherp\OrderRemarkM;
use App\Models\yherp\OrderSampleM;
use App\Models\yherp\YcsTransport;
use Illuminate\Support\Facades\DB;
use App\Models\yherp\OrderConfirmD;
use App\Models\yherp\OrderConfirmM;
use App\Repositories\BaseRepository;
use App\Services\OrderSampleService;
use Illuminate\Support\Facades\Auth;
use App\Models\yherp\LogPickingPrint;
use App\Models\yherp\CustomerComplaint;
use App\Models\yherp\OrderShipmentStatusM;

class BaseService
{
    public const week = ['日', '一', '二', '三', '四', '五', '六'];
    //樣片包地板
    public const sample_floor = ['99010014001', '99010014002', '99010014003', '99010014004'];
    //超商託運代碼，7-11、全家、萊爾富、OK、蝦皮店到店
    public const shop_trans = ['Shop-7', 'Shop-F', 'Shop-L', 'Shop-OK', 'Shop-S'];
    //宅配託運代碼，新竹、音速
    public const home_trans = ['HCT', 'OTHER2'];
    //平台宅配名稱
    public const original_home_trans = [
        0 => ['賣家宅配', '賣家宅配：箱購', '賣家宅配：大型/超重物品運送'],
        1 => ['地板宅配(新竹物流)'], //Shopline
        2 => ['賣家宅配', '賣家宅配：箱購', '賣家宅配：大型/超重物品運送'], //蝦皮1店
        3 => ['賣家宅配', '賣家宅配：箱購', '賣家宅配：大型/超重物品運送'], //蝦皮2店
        4 => ['賣家宅配', '賣家宅配：箱購', '賣家宅配：大型/超重物品運送'], //蝦皮3店
        5 => ['地板宅配'], //BV SHOP
    ];
    //平台超取名稱
    public const original_shop_trans = [
        '0' => ['7-ELEVEN', '全家', '萊爾富', 'OK Mart', '蝦皮店到店', '店到家宅配'],
        '1' => ['7-11純取貨', '7-11取貨付款', '全家純取貨', '全家取貨付款'], //Shopline
        '2' => ['7-ELEVEN', '全家', '萊爾富', 'OK Mart', '蝦皮店到店', '店到家宅配'], //蝦皮1店
        '3' => ['7-ELEVEN', '全家', '萊爾富', 'OK Mart', '蝦皮店到店', '店到家宅配'], //蝦皮2店
        '4' => ['7-ELEVEN', '全家', '萊爾富', 'OK Mart', '蝦皮店到店', '店到家宅配'], //蝦皮3店
        '5' => ['超商取貨 - 統一超商交貨便'], //BV SHOP
    ];
    public const order_status = [0 => '壓單', 1 => '未列印', 2 => '未驗貨', 3 => '未包裝', 4 => '未上車', 5 => '已出貨'];
    public const connections = ['YoHouse' => 'mysql', 'YoHouse2' => 'YoHouse2'];
    public const databases = ['YoHouse' => 'yherp', 'YoHouse2' => 'yherp2'];
    //貨幣
    public const currency = ['TWD' => '新台幣', 'CNY' => '人民幣', 'USD' => '美元', 'JPY' => '日圓'];
    public $configService;
    public $baseRepository;

    public function __construct()
    {
        $this->baseRepository = new BaseRepository;
        $this->configService = new ConfigService($this->baseRepository);
    }

    //取得對應公司的database
    public function GetConnection($company)
    {
        $user = Auth::user();
        if (empty($user)) {
            return '';
        } else {
            if ($user->hasAllRoles(array_keys(self::connections))) { //跨公司
                if (empty($company)) {
                    return self::connections['YoHouse'];
                } elseif (key_exists($company, self::connections)) {
                    return self::connections[$company];
                } else {
                    return '';
                }
            } else { //單一公司
                foreach (self::connections as $com => $conn) {
                    if ($user->hasRole($com)) {
                        return $conn;
                    }
                }
                return '';
            }
        }
    }

    //取得對應公司的database
    public function GetDatabase($company)
    {
        $user = Auth::user();
        if (empty($user)) {
            return '';
        } else {
            if ($user->hasAllRoles(array_keys(self::databases))) { //跨公司
                if (empty($company)) {
                    return self::databases['YoHouse'];
                } elseif (key_exists($company, self::databases)) {
                    return self::databases[$company];
                } else {
                    return '';
                }
            } else { //單一公司
                foreach (self::databases as $com => $db) {
                    if ($user->hasRole($com)) {
                        return $db;
                    }
                }
                return '';
            }
        }
    }

    //取得兩經緯度之間距離(公里整數)
    public function GetDistanceBetweenPointsNew($latitude1, $longitude1, $latitude2, $longitude2)
    {
        $theta = $longitude1 - $longitude2;
        $distance = (sin(deg2rad($latitude1)) * sin(deg2rad($latitude2))) + (cos(deg2rad($latitude1)) * cos(deg2rad($latitude2)) * cos(deg2rad($theta)));
        $distance = acos($distance);
        if (is_nan($distance)) {
            return 0;
        }
        $distance = rad2deg($distance);
        $distance = $distance * 60 * 1.1515;
        $distance = $distance * 1.609344; //轉公里
        return $distance;
    }

    //取得指定月份下的每週區間
    public static function GetWeekInterval($s_date, $e_date)
    {
        $result = [];
        $s_date = new DateTime($s_date . '-01 00:00:00');
        $week = $s_date->format('w');
        if ($week == 0) { //星期天
            $s_date->modify('-6 days');
        } else if ($week != 1) {
            $s_date->modify('-' . ($week - 1) . ' days');
        }
        $e_date = new DateTime((new Carbon($e_date . '-01'))->endOfMonth()->format('Y-m-d 23:59:59'));
        while ($s_date <= $e_date) {
            $tmp = clone $s_date;
            $tmp->modify('6 days');
            $result[] = [$s_date->format('Y-m-d 00:00:00'), $tmp->format('Y-m-d 23:59:59')];
            $s_date->modify('7 days');
        }
        return $result;
    }

    //取的最近工作日
    public static function GetWorkDay($day, $holiday_config)
    {
        do {
            if (key_exists($day, $holiday_config)) {
                $is_holiday = $holiday_config[$day];
            } else {
                $is_holiday = in_array((new Carbon($day))->dayOfWeek, [0, 6]);
            }
            if ($is_holiday) {
                $day = (new Carbon($day))->addDays(1)->format('Y-m-d');
            }
        } while ($is_holiday);
        return $day;
    }

    /**
     * 紀錄縣市鄉鎮市區資料
     *
     * @return void
     */
    public static function ResetCounty()
    {
        County::query()->delete();
        CountyArea::query()->delete();
        //縣市清單https://data.gov.tw/dataset/101905
        $county_xml = simplexml_load_string(file_get_contents('https://api.nlsc.gov.tw/other/ListCounty'));
        $county_json = json_encode($county_xml);
        $countys = json_decode($county_json);
        foreach ($countys->countyItem as $county) {
            $county_code = $county->countycode;
            $county_name = $county->countyname;
            $m = County::create(['_code' => $county_code, 'name' => $county_name]);
            //鄉政市區清單(戶政)https://data.gov.tw/dataset/102011
            $area_xml = simplexml_load_string(file_get_contents('https://api.nlsc.gov.tw/other/ListTown1/' . $county_code));
            $area_json = json_encode($area_xml);
            $areas = json_decode($area_json);
            if (is_array($areas->townItem)) {
                foreach ($areas->townItem as $area) {
                    $area_code = $area->towncode01;
                    $area_name = $area->townname;

                    $m->Areas()->create(['_code' => $area_code, 'name' => $area_name]);
                }
            }
        }
    }

    public static function AllTrim($str)
    {
        $search = array(" ", "　", "\n", "\r", "\t");
        $replace = array("", "", "", "", "");
        return str_replace($search, $replace, $str);
    }

    /**
     * 國際碼轉特殊格式 09XX-XXX-XXX
     *
     * @param string $tel
     * @return string
     */
    public static function GetTWTel($tel)
    {
        if (empty($tel) || preg_match("/\D/", $tel)) {
            return '';
        } else {
            //886->TW
            $result = preg_replace('/886/', '0', $tel, 1);
            $result = substr_replace($result, '-', 4, 0);
            $result = substr_replace($result, '-', 8, 0);
            return $result;
        }
    }

    //物流代碼統一轉成特定大小寫
    public static function ConvertTransCode($transport_code)
    {
        $keywords = [
            'BH',
            'COM',
            'HCT',
            'KER',
            'MF',
            'OTHER',
            'OTHER2',
            'OTHER3',
            'OTHER4',
            'POST',
            'SC',
            'Shop-7',
            'ShopeeHD',
            'Shop-F',
            'Shop-L',
            'Shop-OK',
            'Shop-S',
        ];
        foreach ($keywords as $keyword) {
            if (strcasecmp($transport_code, $keyword) == 0) {
                return $keyword;
            }
        }
        return $transport_code;
    }

    /**
     * 秒數轉小時分鐘
     *
     * @param float $s 秒數
     * @return string H 小時 M 分鐘
     */
    public function SToHM($s)
    {
        if (empty($s)) {
            return '';
        }
        $hh = floor($s / 3600);
        $s %= 3600;
        $mm = floor($s / 60);
        $hh = str_pad($hh, 2, '0', STR_PAD_LEFT);
        $mm = str_pad($mm, 2, '0', STR_PAD_LEFT);
        return "{$hh} 小時 {$mm} 分鐘";
    }

    /**
     * 取得民國日期格式。YYY.MM.DD
     *
     * @param DateTime $date
     * @return string
     */
    public function GetTWDateStr(?DateTime $date): string
    {
        $tmp = isset($date) ?  clone $date : new DateTime('now');
        $y = $tmp->format('Y') - 1911;
        $m = $tmp->format('m');
        $d = $tmp->format('d');
        return $y . '.' . $m . '.' . $d;
    }

    /**
     * 將民國日期格式轉成DateTime
     *
     * @param string $twDate 民國日期格式。YYY.MM.DD
     * @return DateTime
     */
    public function ConvertTWToDate(string $twDate)
    {
        try {
            $tmp = explode('.', $twDate);
            $y = $tmp[0] + 1911;
            $m = $tmp[1];
            $d = $tmp[2];
            return new DateTime("{$y}/{$m}/{$d} 00:00:00");
        } catch (\Throwable $th) {
            return null;
        }
    }

    /**
     * 取得Log日期格式。YYYY/MM/DD
     *
     * @param DateTime $date
     * @return string
     */
    public function GetLogDateStr(DateTime $date): string
    {
        $tmp = isset($date) ?  clone $date : new DateTime('now');
        return $tmp->format("Y/m/d");
    }

    /**
     * 取的忽略毫秒的時間字串
     *
     * @param string|null $time
     * @return string|null
     */
    public function GetTimeIgnoreMS(?string $time)
    {
        //驗貨時間
        if (!empty($time)) {
            return (new DateTime($time))->format('Y-m-d H:i:s');
        } else {
            return $time;
        }
    }

    /**
     * 取得所有電子郵件
     *
     * @return \Illuminate\Support\Collection
     */
    public function GetUsersEmail()
    {
        return $this->baseRepository->GetUsersEmail();
    }

    //根據地址判斷物流
    public function GetAddrTransport($addr, $logistics_addr_mark)
    {
        $suggest = '';
        $addr = str_replace('台', '臺', $addr);
        //音速物流判斷
        foreach ($logistics_addr_mark as $city => $areaList) {
            if (mb_strpos($addr, $city, 0, 'utf-8') !== false) {
                foreach ($areaList as $area) {
                    if (mb_strpos($addr, $area, 0, 'utf-8') !== false) {
                        $suggest = 'YCS';
                        break 2;
                    }
                }
            }
        }
        return $suggest;
    }

    //取得預購訂單
    public function GetPreOrder()
    {
        $orderSampleService = new OrderSampleService();
        $item_data = $this->GetItemDetail();
        //取半年內的預購單
        $tw_date = $this->GetTWDateStr((new Carbon())->addMonths(-6));
        $result = [];
        $statistics = [];
        $basket_db = OrderSampleM::with('Order:OCOD1,OCOD4,OCNAM')
            ->where('order_kind', 1)
            ->where('completed', 0)
            ->get();
        $basket_list = [];
        foreach ($basket_db as $basket) {
            if (!empty($basket->Order) && count($basket->Order) > 0) {
                $cust_name = $basket->Order->first()->cust_name;
                $basket_list = array_merge($basket_list, $basket->Order->pluck('OCOD1')->toArray());
                continue;
            } else {
                $cust_name = $basket->src_name;
                $columns = $orderSampleService->GetOrderSampleColumns(json_decode($basket->header), $basket->src);
                $original_data = json_decode($basket->original_data);
                $items = [];
                foreach ($original_data as $item) {
                    $item_no = $item[$columns['item_no']];
                    $qty = $item[$columns['item_quantity']];
                    if (empty($qty)) {
                        continue;
                    }
                    if (key_exists($item_no, $item_data)) {
                        $item_kind1 = $item_data[$item_no]['kind1'];
                        if (in_array($item_kind1, ['98', '99'])) {
                            continue;
                        }
                        $item_name = $item_data[$item_no]['name'];
                    } else {
                        $item_name = '';
                    }
                    $items[] = [
                        'item_no' => $item_no,
                        'item_name' => $item_name,
                        'qty' => $qty,
                    ];
                    if (key_exists($item_no, $statistics)) {
                        $statistics[$item_no]['qty'] += $qty;
                    } else {
                        $statistics[$item_no]['name'] = $item_name;
                        $statistics[$item_no]['qty'] = $qty;
                    }
                }
                $result[] = [
                    'cust_name' => $cust_name,
                    'original_no' => $basket->order_no,
                    'order_no' => '',
                    'items' => $items,
                ];
            }
        }
        $basket_list = array_merge($basket_list, $basket_db->pluck('order_no')->toArray());
        array_unique($basket_list);
        $tms_db = Order::select('OCOD1', 'OCOD4', 'OCNAM')
            ->with(['OrderDetail:OCOD1,OICOD,OINAM,OINUM', 'OrderDetail.Item:ICODE,ITCOD', 'Posein:PCOD1,PCOD2', 'Posein.LogShipment'])
            ->where(function ($q) use ($tw_date) {
                $q->where(function ($q) {
                    $q
                        ->where('OBAK1', 'LIKE', '%預購%')
                        ->orWhere('OBAK2', 'LIKE', '%預購%')
                        ->orWhere('OBAK3', 'LIKE', '%預購%')
                        ->orWhere('InsideNote', 'LIKE', '%預購%');
                })
                    ->where('ODATE', '>=', $tw_date)
                    ->where('OCANC', '!=', '訂單取消');
            })
            ->orWhereIn('OCOD1', $basket_list)
            ->get();
        foreach ($tms_db as $tms) {
            if (count($tms->Posein) > 0) {
                foreach ($tms->Posein as $sale) {
                    if (count($sale->LogShipment) > 0) {
                        continue 2;
                    }
                }
            }
            $cust_name = $tms->cust_name;
            $order_no = $tms->order_no;
            $original_no = $tms->original_no;
            $items = [];
            foreach ($tms->OrderDetail as $detail) {
                if (in_array($detail->Item->ITCOD, ['98', '99'])) {
                    continue;
                }
                $item_no = $detail->item_no;
                $item_name = $detail->item_name;
                $qty = $detail->qty;
                $items[] = [
                    'item_no' => $item_no,
                    'item_name' => $item_name,
                    'qty' => $qty,
                ];
                if (key_exists($item_no, $statistics)) {
                    $statistics[$item_no]['qty'] += $qty;
                } else {
                    $statistics[$item_no]['name'] = $item_name;
                    $statistics[$item_no]['qty'] = $qty;
                }
            }
            $result[] = [
                'cust_name' => $cust_name,
                'original_no' => $original_no,
                'order_no' => $order_no,
                'items' => $items,
            ];
        }

        return ['data' => $result, 'statistics' => $statistics];
    }

    /**
     * 取得訂單狀況。未覆核、未轉單、已過期
     *
     * @param Request $request
     * @return array
     */
    public function GetOrderAll()
    {
        //erp訂單日期格式
        $date = new DateTime("now");
        //今日
        $eDate = $this->GetTWDateStr($date);
        //今日往前15天
        $date->modify("-15 day");
        $sDate = $this->GetTWDateStr($date);

        $orderAllDB = $this->baseRepository->GetOrderAll($sDate, $eDate);
        $result = array();
        $todayUnixTime = strtotime(str_replace('.', '-', $eDate));
        foreach ($orderAllDB as $value) {
            $orderStatus = $value->OCANC;
            if ($orderStatus == '待核准') {
                $result['unchecked'][] = $value; //未覆核
            } elseif ($orderStatus == '已核示') {
                $result['unprinted'][] = $value; //未轉單
            }
            if (floor(($todayUnixTime - strtotime(str_replace('.', '-', $value->ODATE))) / 86400) >= 2) { //距今>=2天
                $result['expired'][] = $value; //已過期
            }
        }
        if (key_exists('unprinted', $result)) {
            $result['canPrint'] = count(collect($result['unprinted'])->where('CanPrint', true));
        } else {
            $result['canPrint'] = 0;
        }

        return $result;
    }

    //訂單總表
    public function GetOrderAllStatus(Request $request)
    {
        $all_status1 = [
            1 => '助理審單',
            2 => 'TMS中',
            3 => '現場處裡',
        ];
        $all_status2 = [
            1 => [
                1 => '免確認',
                2 => '未確認',
                3 => '已確認',
            ],
            2 => [
                1 => '未覆核',
                2 => '已取消',
                3 => '可轉單',
                4 => '待調撥',
                5 => '需採購',
                6 => '未進貨',
                7 => '協商換貨',
            ],
            3 => [
                1 => '未撿貨',
                2 => '未驗貨',
                3 => '未包裝',
                4 => '未上車',
                5 => '已出貨',
                6 => '已退貨',
            ],
        ];
        $order_kind_status = [
            1 => '預購',
            2 => '未完全出貨',
            3 => '新竹偏遠',
        ];
        //統計資料初始化
        $statistics = [];
        foreach ($all_status1 as $status1 => $status1_name) {
            $statistics[$status1] = [];
            foreach ($all_status2[$status1] as $status2 => $status2_name) {
                $statistics[$status1][$status2] = 0;
            }
        }
        $data = [];
        $result = ['data' => collect($data), 'all_status1' => $all_status1, 'all_status2' => $all_status2, 'statistics' => $statistics];
        $search_type = $request->get('search_type', 1);
        $start_date = $request->get('start_date');
        $end_date = $request->get('end_date');
        $original_no = $request->get('original_no');
        $order_no = $request->get('order_no');
        $sale_no = $request->get('sale_no');
        $trans_no = $request->get('trans_no');
        switch ($search_type) {
            case 1: //依訂單成立日期
                if (empty($start_date) || empty($end_date)) {
                    return $result;
                }
                break;
            case 2: //依原始訂單編號
                if (empty($original_no)) {
                    return $result;
                }
                break;
            case 3: //依訂單單號
                if (empty($order_no)) {
                    return $result;
                }
                break;
            case 4: //依銷貨單號
                if (empty($sale_no)) {
                    return $result;
                }
                break;
            case 5: //依託運單號
                if (empty($trans_no)) {
                    return $result;
                }
                break;
        }
        $tw_start_date = $this->GetTWDateStr(new DateTime($start_date));
        $tw_end_date = $this->GetTWDateStr(new DateTime($end_date));
        $holiday_config = Holiday::whereBetween('year', [Carbon::today()->year - 1, Carbon::today()->year + 1])->get();
        $now = new DateTime();
        $tomorrow_ship_date = new DateTime(Carbon::today()->addDays(1)->format('Y-m-d 18:00:00'));
        $ship_hour = $this->GetTotalHours($tomorrow_ship_date->diff($now)); //下次出貨所需小時
        $purchase_change = OrderConfirmD::select('order_no')
            ->whereHas('Main', function ($q) {
                $q->where('order_type', 13);
            })
            ->get()
            ->pluck('order_no')->toArray();
        $hct_remote_config = OrderSampleService::GetLogisticsAddrMark('2');
        $basket_order = OrderSampleM::select('src', 'order_date', 'order_no', 'order_kind', 'order_type', 'confirmed', 'estimated_date', 'receiver_address', DB::raw('0 AS from_tms'))
            ->with([
                'Order:OCOD4,OCOD1,OCANC,OINYN,TransportName,OBAK1,OBAK2,OBAK3,InsideNote',
                'Order.OrderDetail:OCOD1,OICOD,OINUM',
                'Order.Posein:PCOD1,PCOD2,PJONO,PPRIN,TransportCode,TransportName,ConsignTran',
                'Order.Posein.LogPickingPrint:PCOD1,created_at',
                'Order.Posein.PoseinCheckDataLog:PCOD1,CREATE_Date',
                'Order.Posein.LogPackage:sale_no,completed_at',
                'Order.Posein.LogShipment:PCOD1,created_at',
                'Order.Posein.OrderShipmentStatusM',
                'Order.Posein.OrderShipmentStatusM.Details',
                'Order.Posein.Poseou:PCOD2,PCOD1',
                'Order.PersonalTransport',
                'Order.OtherTransport'
            ]);
        $basket_empty = false;
        switch ($search_type) {
            case 1: //依訂單成立日期
                $basket_order->whereBetween('order_date', [$start_date . ' 00:00:00', $end_date . ' 23:59:59']);
                break;
            case 2: //依原始訂單編號
                $basket_order->where('order_no', $original_no);
                break;
            case 3: //依訂單單號
                $tmp = Order::select('OCOD4')->where('OCOD1', $order_no)->first()->OCOD4 ?? '';
                if (empty($tmp)) {
                    $basket_empty = true;
                } else {
                    $basket_order->where('order_no', $tmp);
                }
                break;
            case 4: //依銷貨單號
                $tmp = Posein::select('PJONO')->where('PCOD1', $sale_no)->first()->PJONO ?? '';
                if (empty($tmp)) {
                    $basket_empty = true;
                } else {
                    $basket_order->where('order_no', $tmp);
                }
                break;
            case 5: //依託運單號
                $tmp = Posein::select('PJONO')->where('ConsignTran', $trans_no)->first()->PJONO ?? '';
                if (empty($tmp)) {
                    $basket_empty = true;
                } else {
                    $basket_order->where('order_no', $tmp);
                }
                break;
        }
        if ($basket_empty) {
            $basket_order = collect();
        } else {
            $basket_order = $basket_order->get();
        }
        $sale_stock_db = STOC::select('SITEM', 'SNUMB')->where('STCOD', 'A001')->where('SNUMB', '>', 0)->get()->pluck('SNUMB', 'SITEM')->toArray();
        $main_stock_db = ItemPlaceD::select('item_no', DB::raw('SUM(qty) AS qty'))
            ->whereHas('Main', function ($q) {
                $q->where('s_id', 'B001');
            })
            ->where('qty', '>', 0)
            ->groupBy('item_no')
            ->get()
            ->pluck('qty', 'item_no')
            ->toArray();
        $purchase_not_in = $this->baseRepository->GetPurchaseNotIn()
            ->groupBy('SICOD')
            ->map(function ($item, $item_no) {
                $remain = $item->sum('remain');
                return ['item_no' => $item_no, 'remain' => $remain];
            })
            ->pluck('remain', 'item_no')
            ->toArray();
        $basket_order_list = $basket_order->pluck('order_no')->toArray();
        $tms_order = Order::select('OCOD1', 'OCOD4', 'OCANC', 'OINYN', 'OCCOD', 'OCNAM', 'TransportName', 'OBAK1', 'OBAK2', 'OBAK3', 'InsideNote')
            ->with(['OrderDetail:OCOD1,OICOD,OINUM', 'OrderSample'])
            ->whereNotIn('OCOD4', $basket_order_list);
        switch ($search_type) {
            case 1: //依訂單成立日期
                $tms_order->whereBetween('ODATE', [$tw_start_date, $tw_end_date]);
                break;
            case 2: //依原始訂單編號
                $tms_order->where('OCOD4', $original_no);
                break;
            case 3: //依訂單單號
                $tms_order->where('OCOD1', $order_no);
                break;
            case 4: //依銷貨單號
                $tms_order->whereHas('POSEIN', function ($q) use ($sale_no) {
                    $q->where('PCOD1', $sale_no);
                });
                break;
            case 5: //依託運單號
                $tms_order->whereHas('POSEIN', function ($q) use ($trans_no) {
                    $q->where('ConsignTran', $trans_no);
                });
                break;
        }
        $tms_order = $tms_order->get();
        $basket_order = $basket_order->toBase()->merge($tms_order);
        foreach ($basket_order as $o) {
            if (is_a($o, Order::class)) { //tms來源的訂單
                if ($search_type == 1 && !empty($o->OrderSample)) { //當查詢條件是訂單成立日期時，排除位於籃子內
                    continue;
                }
                $osm = new OrderSampleM;
                $osm->from_tms = 1;
                $osm->OINYN = $o->OINYN;
                $osm->cust_code = $o->OCCOD;
                $osm->cust_name = $o->OCNAM;
                $osm->order_no = $o->OCOD4;
                $osm->estimated_date = '';
                $osm->Order = collect([$o]);
                $o = $osm;
            }
            $from_tms = $o->from_tms;
            if ($from_tms) { //TMS
                $cust_name = $o->cust_name;
                $order_date = '';
                $order_expired = '';
                $addr = '';
            } else { //籃子
                $cust_name = $o->src_name;
                $cust = $o->cust;
                $order_date = $o->order_date;
                $order_hours = ceil($this->GetOrderElapsedHour(new DateTime($order_date), $now, $holiday_config));
                $total_hours = $order_hours + $ship_hour;
                $order_expired = $this->GetOrderExpired($cust, $total_hours);
                $addr = $o->receiver_address;
            }
            $original_no = $o->order_no;
            //預購
            if ($o->order_kind == 1) {
                $order_kind_from_baket = [1 => '預購'];
            } else {
                $order_kind_from_baket = [];
            }
            if (!empty($o->PersonalTransport)) {
                $trans_kind = '本公司親送';
                $estimated_date = $o->PersonalTransport->ship_date ?? '未安排';
                $estimated_date_status = $o->PersonalTransport->cust_confirmed ? 2 : 1;
            } elseif (!empty($o->OtherTransport)) {
                $trans_kind = '';
                $estimated_date = $o->OtherTransport->ship_date ?? '未安排';
                $estimated_date_status = 1;
            } else {
                $trans_kind = '';
                $estimated_date = $o->estimated_date;
                $estimated_date_status = 1;
            }
            $estimated_date_status_name = $this->GetEstimatedDateStatusName($estimated_date_status);
            $basket_result = [
                'src' => $from_tms ? 'TMS' : '籃子',
                'order_date' => $order_date,
                'order_expired' => $order_expired,
                'cust_name' => $cust_name,
                'original_no' => $original_no,
                'order_kind' => $order_kind_from_baket,
                'shipment_status' => [],
                'trans_kind' => $trans_kind,
                'trans_no' => '',
                'estimated_date' => $estimated_date,
                'estimated_date_status' => $estimated_date_status,
                'estimated_date_status_name' => $estimated_date_status_name,
                'status1' => '',
                'status1_name' => '',
                'status2' => '',
                'status2_name' => '',
                'order_no' => '',
                'sale_no' => '',
                'return_no' => '',
                'pick_time' => '',
                'check_time' => '',
                'package_time' => '',
                'shipment_time' => '',
            ];
            if (empty($o->Order) || count($o->Order) == 0) { //沒有TMS訂單
                $status1 = 1;
                $basket_result['status1'] = $status1;
                $basket_result['status1_name'] = $all_status1[$status1];
                if ($o->confirmed) { //已確認
                    $status2 = 3;
                } else { //未確認
                    switch ($o->order_type) {
                        case 0:
                            $status2 = 1;
                            break;
                        case 1:
                            $status2 = 2;
                            break;
                    }
                }
                $basket_result['status2'] = $status2;
                $basket_result['status2_name'] = $all_status2[$status1][$status2];
                $data[] = $basket_result;
            } else {
                $order_result = $basket_result;
                $status1 = 2;
                $order_result['status1'] = $status1;
                $order_result['status1_name'] = $all_status1[$status1];
                foreach ($o->Order as $order) {
                    //訂單類型
                    $order_result['order_kind'] = $order_kind_from_baket;
                    if (!key_exists(1, $order_result['order_kind'])) {
                        foreach (['預購'] as $keyword) {
                            if ((mb_strpos($order->bak1, $keyword) !== false || mb_strpos($order->bak2, $keyword) !== false || mb_strpos($order->bak3, $keyword) !== false
                                || mb_strpos($order->InsideNote, $keyword) !== false)) {
                                $order_result['order_kind'][1] = $keyword;
                            }
                        }
                    }
                    //物流類型
                    if (!empty($order->PersonalTransport)) { //籃子未安排親送 且 訂單有安排親送
                        $order_result['trans_kind'] = '本公司親送';
                        $order_result['estimated_date'] = $o->PersonalTransport->ship_date ?? '未安排';
                        $order_result['estimated_date_status'] = $o->PersonalTransport->cust_confirmed ? 2 : 1;
                        $order_result['estimated_date_status_name'] = $this->GetEstimatedDateStatusName($order_result['estimated_date_status']);
                    } elseif (!empty($order->OtherTransport)) { //籃子未安排人工 且 訂單有安排人工
                        $order_result['trans_kind'] = $order->trans_name;
                        $order_result['estimated_date'] = $o->OtherTransport->ship_date ?? '未安排';
                    } else {
                        $order_result['trans_kind'] = $order->trans_name;
                    }
                    $order_result['order_no'] = $order->order_no;
                    if ($order->OCANC == '待核准') {
                        $status2 = 1;
                        $order_result['status2'] = $status2;
                        $order_result['status2_name'] = $all_status2[$status1][$status2];
                        $data[] = $order_result;
                    } else if ($order->OCANC == '訂單取消') {
                        $status2 = 2;
                        $order_result['status2'] = $status2;
                        $order_result['status2_name'] = $all_status2[$status1][$status2];
                        $data[] = $order_result;
                    } else {
                        if (empty($order->Posein) || count($order->Posein) == 0) { //沒有TMS銷單
                            //訂單明細
                            $order_need = [];
                            //整理訂單內容
                            foreach ($order->OrderDetail->groupBy('OICOD') as $item_no => $group) {
                                $order_need[$item_no] = $group->sum('qty');
                            }
                            //出貨倉數量檢查是否可以轉單
                            $can_sale = true;
                            foreach ($order_need as $item_no => $need) {
                                $sale_stock = $sale_stock_db[$item_no] ?? 0;
                                if ($sale_stock < $need) { //出貨倉不足
                                    $order_need[$item_no] -= $sale_stock; //扣掉出貨倉數量
                                    $can_sale = false;
                                } else { //出貨倉足夠
                                    unset($order_need[$item_no]);
                                }
                            }
                            if ($can_sale) { //可轉而未轉
                                $status2 = 3;
                                $order_result['status2'] = $status2;
                                $order_result['status2_name'] = $all_status2[$status1][$status2];
                                $data[] = $order_result;
                            } else { //需採購或需調撥
                                $need_transfer = true;
                                foreach ($order_need as $item_no => $need) {
                                    $main_stock = $main_stock_db[$item_no] ?? 0;
                                    if ($main_stock < $need) { //主倉庫存不足，無法調撥
                                        $order_need[$item_no] -= $main_stock; //扣掉主倉數量
                                        $need_transfer = false;
                                    } else {
                                        unset($order_need[$item_no]);
                                    }
                                }
                                if ($need_transfer) {
                                    $status2 = 4;
                                    $order_result['status2'] = $status2;
                                    $order_result['status2_name'] = $all_status2[$status1][$status2];
                                    $data[] = $order_result;
                                } else {
                                    $need_purchase = true;
                                    //都採購，未進貨判斷
                                    if (!empty($purchase_not_in) && count($purchase_not_in) > 0) { //有採購未進貨
                                        $all_purchase = true; //全採購
                                        foreach ($order_need as $item_no => $need) {
                                            $purchase = $purchase_not_in[$item_no] ?? 0;
                                            if ($purchase < $need) { //未進貨數量小於需求
                                                $all_purchase = false;
                                            }
                                        }
                                        $need_purchase = !$all_purchase;
                                    }
                                    if ($need_purchase) { //需採購
                                        $status2 = in_array($order_result['order_no'], $purchase_change) ? 7 : 5;
                                    } else { //未進貨
                                        $status2 = 6;
                                    }
                                    $order_result['status2'] = $status2;
                                    $order_result['status2_name'] = $all_status2[$status1][$status2];
                                    $data[] = $order_result;
                                }
                            }
                        } else { //有TMS銷單
                            $sale_result = $order_result;
                            if ($order->OINYN != 'Y') { //有銷貨單、卻未完全出貨
                                $sale_result['order_kind'][2] = '未完全出貨';
                            }
                            $status1 = 3;
                            $sale_result['status1'] = $status1;
                            $sale_result['status1_name'] = $all_status1[$status1];
                            foreach ($order->Posein as $sale) {
                                $sale_result['sale_no'] = $sale->sale_no;
                                $sale_result['pick_time'] = '';
                                $sale_result['check_time'] = '';
                                $sale_result['package_time'] = '';
                                $sale_result['shipment_time'] = '';
                                //出貨狀態，有延遲出貨
                                $ship_status_m = $sale->OrderShipmentStatusM;
                                if (!empty($ship_status_m)) {
                                    if ($ship_status_m->Details->where('_status', 1)->count() > 0) {
                                        $sale_result['shipment_status'][] = $ship_status_m->Details->where('_status', 1)->first()->status_name;
                                    }
                                    if ($ship_status_m->Details->where('_status', 2)->count() > 0) {
                                        $sale_result['estimated_date_status'] = 3;
                                        $sale_result['estimated_date_status_name'] = $this->GetEstimatedDateStatusName($sale_result['estimated_date_status']);
                                        $sale_result['estimated_date'] = $ship_status_m->estimated_date;
                                    }
                                }
                                //物流
                                $sale_result['trans_kind'] = $sale->trans_name;
                                $sale_result['trans_no'] = $sale->trans_no;
                                //新竹偏遠地區判斷
                                if ($sale->TransportCode == 'HCT' && $this->IsHctRemote($hct_remote_config, $addr)) {
                                    $sale_result['order_kind'][3] = '新竹偏遠';
                                }
                                //撿貨
                                if (!empty($sale->LogPickingPrint) && count($sale->LogPickingPrint) > 0) {
                                    $sale_result['pick_time'] = $sale->LogPickingPrint->max('created_at')->format('Y-m-d H:i:s');
                                } else if ($sale->PPRIN > 0) {
                                    $sale_result['pick_time'] = substr($sale->print_time, 0, 19);
                                } else {
                                    $status2 = 1;
                                    goto push;
                                }
                                //驗貨
                                if (empty($sale->PoseinCheckDataLog) || count($sale->PoseinCheckDataLog) == 0) {
                                    $status2 = 2;
                                    goto push;
                                } else {
                                    $sale_result['check_time'] = substr($sale->PoseinCheckDataLog->sortByDesc('CREATED_Date')->first()->CREATE_Date, 0, 19);
                                }
                                //包裝
                                if (empty($sale->LogPackage) || count($sale->LogPackage) == 0) {
                                    $status2 = 3;
                                    goto push;
                                } else {
                                    $sale_result['package_time'] = $sale->LogPackage->first()->completed_at;
                                }
                                //上車
                                if (empty($sale->LogShipment) || count($sale->LogShipment) == 0) {
                                    $status2 = 4;
                                    goto push;
                                } else {
                                    $sale_result['shipment_time'] = $sale->LogShipment->first()->created_at;
                                }
                                $status2 = 5;
                                push:
                                //有退貨，覆寫狀態
                                if (!empty($sale->Poseou)) {
                                    $sale_result['return_no'] = $sale->Poseou->PCOD1;
                                    $status2 = 6;
                                }
                                $sale_result['status2'] = $status2;
                                $sale_result['status2_name'] = $all_status2[$status1][$status2];
                                $data[] = $sale_result;
                            }
                        }
                    }
                }
            }
        }
        array_multisort(array_column($data, 'status1'), array_column($data, 'status2'), $data);
        $data = collect($data);
        //新竹偏遠已覆核
        $hct_remote_list = $data->filter(function ($item) {
            return in_array('新竹偏遠', $item['order_kind']);
        })->pluck('original_no')->toArray();
        if (count($hct_remote_list) > 0) {
            $confirm_list = OrderConfirmD::whereHas('Main', function ($q) {
                $q->where('order_type', '11');
            })
                ->whereIn('order_no', $hct_remote_list)
                ->pluck('order_no')
                ->toArray();
            if (count($confirm_list) > 0) {
                $data = $data->map(function ($item, $key) use ($confirm_list) {
                    if (in_array($item['original_no'], $confirm_list)) {
                        unset($item['order_kind'][3]);
                    }
                    return $item;
                });
            }
        }
        $result['data'] = $data;
        //統計數量
        foreach (
            collect($data)->groupBy(function ($item, $key) {
                return $item['status1'] . $item['status2'];
            }) as $group
        ) {
            $first_row = $group->first();
            $status1 = $first_row['status1'];
            $status2 = $first_row['status2'];
            $statistics[$status1][$status2] = count($group);
        }
        $result['statistics'] = $statistics;
        return $result;
    }

    /**
     * 取得訂單目標及已轉數
     *
     * @return array
     */
    public function GetOrderSchedule()
    {
        $today =  Carbon::today()->format('Y-m-d');
        $tw_today = $this->GetTWDateStr(new DateTime($today));
        $s_date = new DateTime($today);
        $s_date->modify('-15 day');
        $tw_s_date = $this->GetTWDateStr($s_date);
        $order = [0 => 0, 1 => 0, 2 => 0, 3 => 0, 4 => 0];
        $order_db = $this->baseRepository->GetOrderAll($tw_s_date, $tw_today);
        foreach ($order_db as $o) {
            //判斷類型。地板/宅配/超商
            if (empty($o->IsFloor)) {
                if (in_array($o->trans, BaseService::home_trans)) { //宅配
                    $type = 3;
                } elseif (in_array($o->trans, self::shop_trans)) { //超商
                    $type = 2;
                } else { //其他
                    $type = 4;
                }
            } else { //地板
                $type = 1;
            }
            $qty = 1;
            $order[$type] += $qty;
            $order[0] += $qty;
        }
        //取得已匯入系統卻尚未匯入TMS的訂單
        $original_order = OrderSampleM::where('created_at', '>=', Carbon::today()->format('Y-m-d 00:00:00'))->get();
        $original_order_list = Order::whereIn('OCOD4', $original_order->pluck('order_no')->toArray())->pluck('OCOD4')->toArray();
        $floor_items = $this->baseRepository->GetFloorItem()->pluck('ICODE')->toArray();
        foreach ($original_order as $key => $o) {
            if (in_array($o->order_no, $original_order_list)) {
                $original_order->forget($key);
            } else {
                $o->type = $o->GetType($floor_items);
            }
        }
        //取得過去延遲，今天預計出貨的
        $shipment_confirm = OrderShipmentStatusM::select('sales_no')
            ->where('shipment_check', 1)
            ->where('estimated_date', $today)
            ->pluck('sales_no')->toArray();
        //取得當日銷貨數
        $sales = $this->baseRepository->GetSalesToday($tw_today, $shipment_confirm);
        $sales_list = $sales->pluck('PCOD1')->toArray();
        //取得當日延遲出貨
        $shipment_delay = OrderShipmentStatusM::with('Details')
            ->select('sales_no')
            ->whereIn('sales_no', $sales_list)
            ->where('shipment_check', 0) //未出貨覆核
            ->whereHas('Details', function ($q) {
                $q->where('_status', 2);
            })
            ->pluck('sales_no')->toArray();
        $picking_print = LogPickingPrint::whereIn('PCOD1', $sales_list)->get();
        $package = LogPackage::whereIn('sale_no', $sales_list)->get(); //取得已包裝銷貨
        $shipment = LogShipment::select('PCOD1')->whereIn('PCOD1', $sales_list)->pluck('PCOD1')->toArray(); //取得已上車銷貨
        $ycs_trans = YcsTransport::select('sale_no', 'qty')->whereIn('sale_no', $sales_list)->get(); //取得音速件數
        foreach ($sales as $key => $s) {
            $package_time = $package->where('sale_no', $s->PCOD1)->first()->created_at ?? '';
            //延遲出貨
            if (count($shipment_delay) > 0 && in_array($s->PCOD1, $shipment_delay)) {
                $status = 0;
            } elseif ($s->PPRIN == 0 && empty($picking_print->where('PCOD1', $s->PCOD1)->first())) { //未列印
                $status = 1;
            } elseif (empty($s->CheckTime)) { //未驗貨
                $status = 2;
            } elseif (empty($package_time)) { //已驗貨未包裝
                $status = 3;
            } elseif (!in_array($s->PCOD1, $shipment)) { //已包裝未上車
                $status = 4;
            } else { //已出貨
                $status = 5;
            }
            //判斷類型、件數。地板/宅配/超商
            $trans = self::ConvertTransCode($s->TransportCode);
            if (empty($s->IsFloor)) {
                if (in_array($trans, BaseService::home_trans)) { //宅配
                    $type = 3;
                    $qty = 1;
                } elseif (in_array($trans, self::shop_trans)) { //超商
                    $type = 2;
                    $qty = 1;
                } else { //未知
                    $type = 4;
                    $qty = 1;
                }
            } else { //地板
                $type = 1;
                if ($trans == 'HCT') { //新竹
                    $qty = $s->HctQty ?? 1;
                } elseif ($trans == 'OTHER2') { //音速
                    $qty = $ycs_trans->where('sale_no', $s->PCOD1)->first()->qty ?? 1;
                } else { //未知件數
                    $qty = 1;
                }
            }
            $s->status = $status;
            $s->type = $type;
            $s->qty = $qty;
            $s->PackageTime = $package_time;
        }

        return ['order' => $order, 'sales' => $sales, 'original_order' => $original_order];
    }

    //取得訂單規劃
    public function GetOrderPlan(Request $request)
    {
        $date = $request->get('date');
        if (empty($date)) {
            $date = Carbon::today()->format('Y-m-d');
        }
        $focus_kind = $request->get('kind');
        $exclude_heavy_item = ["11010047015", "11010047016", "11010047017", "11010047018", "11010047019"];
        $orders = Order::select('OCOD1', 'OCOD4', 'OCCOD', 'TransportCode')
            ->with(['OrderDetail' => function ($q) { //訂單明細
                $q->select('OCOD1', 'OICOD', 'OINAM', 'OINUM');
            }])
            ->with(['OrderDetail.Item' => function ($q) { //訂單明細
                $q->select('ICODE', 'ITCOD');
            }])
            ->with(['OrderDetail.Item1' => function ($q) { //訂單明細
                $q->select('ICODE1', 'ITCO3', 'ITNA3');
            }])
            ->with(['OrderSample' => function ($q) { //籃子資訊
                $q->select('order_no', 'order_date', 'order_kind', 'trans');
            }])
            ->with('PersonalTransport')
            ->where('OCANC', '!=', '訂單取消')
            ->whereNotIn('OCCOD', BaseRepository::excludeedCustomers)
            ->doesnthave('Posein')
            ->get();
        $holiday_config = Holiday::whereBetween('year', [Carbon::today()->year - 1, Carbon::today()->year + 1])->get();
        $now = new DateTime();
        $tomorrow_ship_date = new DateTime(Carbon::today()->addDays(1)->format('Y-m-d 18:00:00'));
        $ship_hour = $this->GetTotalHours($tomorrow_ship_date->diff($now)); //下次出貨所需小時
        $result = [];
        foreach ($orders as $order) {
            $order_no = $order->order_no;
            $order_kind = $this->GetOrderKind($order);
            $order_date = $order->OrderSample->order_date ?? '';
            $cust = $order->cust;
            //訂單過期計算
            $order_expired = '';
            $order_hours = '';
            if (!empty($order_date)) {
                //訂單經過時間計算
                $order_hours = ceil($this->GetOrderElapsedHour(new DateTime($order_date), $now, $holiday_config));
                $total_hours = $order_hours + $ship_hour;
                $order_expired = $this->GetOrderExpired($cust, $total_hours);
            }
            //親送
            if ($order_kind == 1) {
                $ship_date = $order->PersonalTransport->ship_date ?? '';
                if (empty($ship_date) || (!empty($ship_date) && $ship_date != $date)) {
                    continue;
                }
            }
            $repeat = false; //已納入一單計算
            foreach ($order->OrderDetail as $item) {
                $item_no = $item->item_no;
                $item_name = $item->item_name;
                $unit = '';
                $qty = intval($item->OINUM);
                $kind1 = $item->Item->ITCOD; //大類
                $kind3 = $item->Item1->ITCO3; //小類
                $kind3_name = $item->Item1->ITNA3; //小類名稱
                $item_qty = 0;
                if ($kind1 == '98' || ($kind1 == '99' && $kind3 != '014')) { //排除其他、包材(不含樣片)
                    continue;
                }
                if ($kind1 == "11" && $kind3 == "014") { //SPC
                    $unit = '包';
                    $item_qty = $qty;
                } else if ($kind1 == "11" && $kind3 == "046") { //LVT
                    $unit = '包';
                    $item_qty = $qty / 10;
                } else {
                    $unit = '單';
                    if (!$repeat) {
                        $item_qty = 1;
                        $repeat = true;
                    } else {
                        $item_qty = 0;
                    }
                }
                $item_kind = $order_kind;
                if ($order_kind == 3 && !in_array($item_no, $exclude_heavy_item)) { //輕重物區分
                    if (($kind1 == '19' && $kind3 == '030') || //學習桌
                        ($kind1 == '11' && $kind3 == '014') || //SPC
                        ($kind1 == '11' && $kind3 == '046') || //LVT
                        ($kind1 == '11' && $kind3 == '047') || //塑木地板
                        ($kind1 == '20' && $kind3 == '018') //草皮
                    ) {
                        $item_kind = 4;
                    }
                }
                if (!empty($focus_kind) && $item_kind != $focus_kind) {
                    continue;
                }
                switch ($item_kind) {
                    case 1:
                        $item_kind_name = '親送';
                        break;
                    case 2:
                        $item_kind_name = '棧板(物流)';
                        break;
                    case 3:
                        $item_kind_name = '輕物包裝';
                        break;
                    case 4:
                        $item_kind_name = '重物包裝';
                        break;
                    default:
                        $item_kind_name = '未定義';
                        break;
                }
                $result[] = [
                    'item_no' => $item_no,
                    'item_name' => $item_name,
                    'item_kind3' => $kind3,
                    'item_kind3_name' => $kind3_name,
                    'item_kind' => $item_kind,
                    'item_kind_name' => $item_kind_name,
                    'item_qty' => $item_qty,
                    'item_unit' => $unit,
                    'order_no' => $order_no,
                    'order_expired' => $order_expired,
                    'order_hours' => $order_hours,
                    'order_date' => $order_date
                ];
            }
        }
        $result = collect($result)->sortByDesc('order_expired');
        //訂單單號清單
        $order_no_list = $result->pluck('order_no')->toArray();
        $order_no_list = array_unique($order_no_list);
        sort($order_no_list);
        //出貨方式統計
        $kind_group = [];
        foreach ($result->sortBy('item_kind')->groupBy('item_kind_name') as $item_kind_name => $group) {
            $kind_group[] = [
                'name' => $item_kind_name,
                'qty' => $group->sum('item_qty'),
            ];
        }
        //小類統計
        $kind3_group = [];
        foreach ($result->groupBy('item_kind3') as $group) {
            $qty =  $group->sum('item_qty');
            if (empty($qty)) {
                continue;
            }
            $kind3_group[] = [
                'name' => $group->first()['item_kind3_name'],
                'qty' => $qty,
            ];
        }

        return ['all_data' => $result, 'order_no_list' => $order_no_list, 'kind_group' => $kind_group, 'kind3_group' => $kind3_group];
    }

    //取得訂單類型，1=親送、2=棧板、3=輕重物
    private function GetOrderKind($order)
    {
        if ($order->trans == 'COM') { //親送
            return 1;
        } else {
            $order_sample = $order->OrderSample;
            if (!empty($order_sample) && $order_sample->order_kind == 2) { //棧板
                return 2;
            } else {
                return 3;
            }
        }
    }

    /**
     * 取得訂單詳細資料
     *
     * @param Request $request
     * @return \Illuminate\Support\Collection
     */
    public function GetOrderDetail(Request $request)
    {
        $id = $request->id;
        if (empty($id)) {
            return collect();
        }
        $db = $this->baseRepository->GetOrderDetail($id);
        $db->buyer_note = $this->GetBuyerNotes($db->OCCOD, $db->OBAK1);
        return $db;
    }

    //籃子訂單明細
    public function GetBasketDetail(Request $request)
    {
        $order_no = $request->get('order_no');
        $result = [];
        $m = OrderSampleM::where('order_no', $order_no)->first();
        if (empty($m)) {
            return null;
        }
        $orderSampleService = new OrderSampleService();
        $headerData = $this->configService->GetConfig('order_convert_columns');
        $columns = $orderSampleService->GetOrderSampleColumns($headerData[$m->src], $m->src);
        $originalData = json_decode($m->original_data);
        $first_row = $originalData[0];
        $detail = [];
        $item_all_db = $this->baseRepository->GetItemAll();
        foreach ($originalData as $od) {
            $item_no = $od[$columns['item_no']];
            $detail[] = [
                'item_no' => $item_no,
                'item_name' => $item_all_db->where('ICODE',  $item_no)->first()->INAME ?? '',
                'item_price' => $od[$columns['item_price']],
                'item_quantity' => $od[$columns['item_quantity']],
            ];
        }
        $result = [
            'order_no' => $m->order_no,
            'cust' => $m->cust,
            'src_name' => $m->src_name,
            'status' => $m->order_type_name,
            'order_total' => $first_row[$columns['order_total']],
            'trans' => $m->trans,
            'trans_name' => $m->trans_name,
            'customer_remark' => $first_row[$columns['customer_remark']],
            'seller_remark' => $first_row[$columns['remark']],
            'detail' => $detail,
        ];
        return $result;
    }

    /**
     * 取得指定區間，有備註訂單
     *
     * @param Request $request
     * @return array
     */
    public function GetOrderRemark(Request $request)
    {
        $today = Carbon::today()->format('Y-m-d');
        $sDate = $this->GetTWDateStr(new DateTime($request->get('start_date', $today)));
        $eDate = $this->GetTWDateStr(new DateTime($request->get('end_date', $today)));
        $result = $this->baseRepository->GetOrderRemark($sDate, $eDate);
        foreach ($result as $key => &$item) {
            $newRemark = $this->GetBuyerNotes($item->cust, $item->bak1);
            if (empty($newRemark)) {
                if (empty($item->bak2) && empty($item->InsideNote)) { //解取完的備註1、備註2、內部備註皆為空，則移除資料
                    $result->forget($key);
                } else {
                    continue;
                }
            } else { //設定新備註
                $item->OBAK1 = $newRemark;
            }
        }
        //自訂排序
        $sortRule = ['R001', 'S001', 'S003', 'S004', 'S005', 'S002', 'S006'];
        $result = $result->sortBy(function ($model) use ($sortRule) {
            return array_search($model->cust, $sortRule);
        });
        $result = $result->values(); //重新排序index
        $config = $this->configService->GetSysConfig('remark_keyword')->pluck('value1')->toArray();
        $cfg_id = SysConfigM::where('_key', 'remark_keyword')->first()->id;
        return ['data' => $result, 'keyword' => $config, 'cfg_id' => $cfg_id];
    }

    /**
     * 取得儲位總表
     *
     * @param mix $stock 倉庫代碼
     * @return array
     */
    public function GetStorageAll($stock = null)
    {
        $stockType = $this->baseRepository->GetStockType()->whereIn('ITNAM', ['主倉儲', '出貨倉']);
        $result = [];
        $sale_stock = []; //出貨倉
        if (empty($stock) || $stock == 'A001') {
            $sale_stock = $this->baseRepository->GetSaleStorageAll();
            foreach (
                $sale_stock->groupBy(function ($item, $key) {
                    return $item->place . $item->item_no;
                }) as $key => $group
            ) {
                $first = $group->first();
                $result[] = [
                    'place' => $first->place,
                    'item_no' => $first->item_no,
                    'item_name' => $first->Item->item_name ?? '',
                    'qty' => $first->STOC->sum('SNUMB'),
                ];
            }
        }
        $main_stock = []; //主倉
        if (empty($stock) || $stock == 'B001') {
            $main_stock = ItemPlaceD::select('m_id AS place', 'item_no', 'item_name', 'qty')
                ->whereHas('Main', function ($q) {
                    $q->where('s_id', 'B001');
                })->get()->toArray();
        }
        $result = collect(array_merge($result, $main_stock));
        //商品圖片
        $imgs = [];
        $itemImgs = ItemImg::whereIn('icode', $result->pluck('item_no')->toArray())->get();
        foreach ($itemImgs as $itemImg) {
            $key = $itemImg->icode;
            $val = $itemImg->path;
            if (file_exists(public_path('storage/' . $itemImg->path))) {
                $imgs[$key][] = $val;
            }
        }
        return ['data' => $result->groupBy('place'), 'stockType' => $stockType, 'imgs' => $imgs];
    }

    /**
     * 取得安全庫存
     *
     * @param Request $request
     * @return \Illuminate\Support\Collection
     */
    public function GetSafetyStock()
    {
        $safetyStockDB = $this->baseRepository->GetSafetyStock();
        //主倉庫存
        $main_stock_db = ItemPlaceD::select('item_no', 'qty')
            ->whereHas('Main', function ($q) {
                $q->where('s_id', 'B001');
            })
            ->where('qty', '>', 0)
            ->get();
        $safetyStockDB->whereIn('ICODE', $main_stock_db->unique('item_no')->pluck('item_no')->toArray())->map(function ($item) use ($main_stock_db) {
            $qty = $main_stock_db->where('item_no', $item->ICODE)->sum('qty');
            if (!empty($qty)) {
                $item->SNUMB += $qty;
            }
        });
        $itemList = $safetyStockDB->unique('ICODE')->pluck('ICODE')->toArray();
        $salesDetail = $this->GetAvgDailySales();
        $purchaseDB = $this->baseRepository->GetSalesRecentlyPurchase($itemList);

        foreach ($safetyStockDB as $key => &$value) {
            $item_no = $value->ICODE;
            //每日平均銷售
            if (array_key_exists($item_no, $salesDetail)) {
                $dailySales = $salesDetail[$item_no]['dailySales'];
                $saleDate = $salesDetail[$item_no]['saleDate'];
            } else {
                unset($safetyStockDB[$key]);
                continue;
            }
            //60日最近採購
            $value->SCOD1 = $purchaseDB->firstWhere('HICOD', $item_no)->SCOD1 ?? null;
            //庫存剩餘天數
            if ($dailySales == 0) {
                $remainingDay = 0;
            } else {
                $remainingDay = round(($value->SNUMB) / $dailySales);
            }

            $value->DailySales = $dailySales; //平均每日銷售量
            $value->RemainingDay = $remainingDay; //庫存剩餘天數
            $value->SuggestBuy = $dailySales * 30; //建議採買量30天
            $value->ElapsedDay = (new DateTime())->diff($saleDate)->format('%a'); //最後銷貨日期距經天數
        }

        return $safetyStockDB->sortBy('RemainingDay');
    }

    /**
     * 取得指定料號的平均每日銷售量
     *
     * @param array $idList 料號清單
     * @param int $days 天數
     * @param bool $deep 無銷售是否往後面日子計算
     * @return array
     */
    public function GetAvgDailySales(array $idList = null, int $days = 30, bool $deep = true)
    {
        $result = [];
        if ($days <= 0) {
            return $result;
        }
        $salesDetailDB = $this->baseRepository->GetSalesDetailOfItem($idList);
        foreach ($salesDetailDB->groupBy('HICOD') as $item_no => $group) {
            $recently = $group->first()->RECENTLY;
            //取30日平均->60日平均->180日平均
            $dailySales = round($group->where('ELAPSED', '<=', $days)->sum('HINUM') / $days);
            if ($dailySales == 0 && $deep) {
                $dailySales = round($group->where('ELAPSED', '<=', '60')->sum('HINUM') / 60);
                if ($dailySales == 0 && $deep) {
                    $dailySales = round($group->where('ELAPSED', '<=', '180')->sum('HINUM') / 180);
                }
            }
            if ($dailySales > 0) { //排除最近銷貨日180天內無銷售紀錄
                $result[$item_no] =  [
                    'saleDate' => $this->ConvertTWToDate($recently),
                    'dailySales' => $dailySales
                ];
            }
        }

        return $result;
    }

    //今日地板數量
    public function GetFloorCount()
    {
        $floor = [];
        $order_schedule = $this->GetOrderSchedule();
        $sale_no_list = $order_schedule['sales']->where('IsFloor', 1)->pluck('PCOD1')->toArray();
        $db = $this->baseRepository->GetSalesDetailCount($sale_no_list);
        $package_db = LogPackage::select('sale_no')->whereIn('sale_no', $sale_no_list)->pluck('sale_no')->toArray();
        foreach ($db->groupBy('HICOD') as $item_no => $group) {
            $floor[] = [
                'item_no' => $item_no,
                'item_name' => $group->first()->item_name,
                'qty' => $group->sum('HINUM'),
                'package' => $group->whereIn('HCOD1', $package_db)->sum('HINUM'),
                'sale_orders' => $group->pluck('HINUM', 'HCOD1')->toArray(),
            ];
        }
        return ['floor' => $floor];
    }

    /**
     * 取得安全庫存紀錄
     *
     * @param Request $request
     * @return \Illuminate\Support\Collection
     */
    public function GetSafetyStockHistory(Request $request)
    {
        $designation = empty($request) ? false : $request->has('designation');
        $date = $request->get('date', Carbon::today()->format('Y-m-d'));
        $db = $this->baseRepository->GetSafetyStockHistory($date, $designation);
        $days = $request->get('days', 30);
        if ($days != 30) { //非30天需重整表
            $avg_sales = $this->GetAvgDailySales($db->pluck('item_no')->toArray(), $days, false);
            foreach ($db as $key => &$d) {
                if (key_exists($d->item_no, $avg_sales)) {
                    $daily_sales = $avg_sales[$d->item_no]['dailySales'];
                    //庫存剩餘天數
                    if ($daily_sales == 0) {
                        $remaining_day = 0;
                    } else {
                        $remaining_day = round($d->stock / $daily_sales);
                    }
                    $d->daily_sales = $daily_sales; //平均每日銷售量
                    $d->remaining_day = $remaining_day; //庫存剩餘天數
                    $d->suggest = $daily_sales * 30; //建議採買量30天

                } else { //排除無銷售
                    $db->forget($key);
                }
            }
        }
        return $db;
    }

    /**
     * 取得庫存總表
     *
     * @return array
     */
    public function GetStockAll(Request $request)
    {
        $date = $request->get('date');
        $result = array();
        //整理產品庫存數量
        $dbSaleStockAll = $this->baseRepository->GetSaleStockAll();
        //加入主倉
        $dbMainStockAll = ItemPlaceD::select('item_no', 'item_name', 'qty')
            ->with(['Item' => function ($q) {
                $q->select('ICODE', 'ITCOD', 'ITNAM', 'ISOUR');
            }])
            ->with(['ITEM1' => function ($q) {
                $q->select('ICODE1', 'ISTOP', 'ITCO3');
            }])
            ->with(['ITEM3' => function ($q) {
                $q->select('ICODE3', 'IPSTOP');
            }])
            ->whereHas('Main', function ($q) {
                $q->where('s_id', 'B001');
            })
            ->get();
        foreach ($dbMainStockAll->groupBy('item_no') as $item_no => $group) {
            $first = $group->first();
            $stock = $group->sum('qty');
            $tmp = (object)[
                'ICODE' => $item_no,
                'INAME' => $first->item_name,
                'ITCOD' => $first->Item->ITCOD,
                'ITNAM' => $first->Item->ITNAM,
                'ITCO3' => $first->Item1->ITCO3,
                'ISTOP' => $first->Item1->ISTOP,
                'IPSTOP' => $first->Item3->IPSTOP,
                'STNAM' => '主倉儲',
                'SNUMB' => $stock,
                'inventory_amt' => $first->Item->ISOUR * $stock,
            ];
            $dbSaleStockAll->push($tmp);
        }
        //庫存金額
        $inventory_amt = array();
        if (!empty($date) && $date != (new DateTime())->format('Y-m-d')) { //指定日期
            $log_item = LogItem::with(['ITEM' => function ($q) {
                $q->select('ICODE', 'ITNAM');
            }])
                ->where('INUMB', '!=', 0)
                ->whereBetween('created_at', [$date . ' 00:00:00', $date . ' 23:59:59'])
                ->get();
            foreach ($log_item as $item) {
                $kind_name = $item->ITEM->kind_name;
                $i_amt = $item->INUMB * $item->ISOUR;
                if (key_exists($kind_name, $inventory_amt)) {
                    $inventory_amt[$kind_name] += $i_amt;
                } else {
                    $inventory_amt[$kind_name] = $i_amt;
                }
            }
        } else { //目前
            foreach ($dbSaleStockAll->whereIn('STNAM', ['主倉儲', '出貨倉'])->groupBy('ITCOD') as $kind1 => $i) {
                $inventory_amt[$i->first()->ITNAM] = $i->sum('inventory_amt');
            }
        }
        arsort($inventory_amt);
        $floor_item = [];
        foreach ($dbSaleStockAll->where('ISTOP', 0)->where('IPSTOP', 0) as $value) {
            $itemID = (string)$value->ICODE;
            $itemName = $value->INAME;
            $stockName = $value->STNAM;
            $stockNum = (float)$value->SNUMB;
            $kind3 = $value->ITCO3;
            if (!array_key_exists($itemID, $result)) {
                $result[$itemID]['Name'] = $itemName;
                $result[$itemID]['TotalStock'] = 0;
            }
            $result[$itemID]['Stock'][$stockName] = number_format($stockNum);
            $result[$itemID]['TotalStock'] += $stockNum;
            if ((in_array($kind3, ['014', '020', '046'])) && $stockName == '出貨倉' && !empty($stockNum)) {
                $floor_item[] = $value;
            }
        }
        //整理產品儲位
        $dbItemp = $this->baseRepository->GetItemp();
        foreach ($dbItemp as $itemID => $value) {
            if (key_exists((string)$itemID, $result)) {
                $result[(string)$itemID]['Storage'] = $value;
            }
        }

        return ['data' => $result, 'inventory_amt' => $inventory_amt, 'floor_item' => $floor_item];
    }

    /**
     * 取得庫存異常
     *
     * @return \Illuminate\Support\Collection
     */
    public function GetStockUnusual()
    {
        return $this->baseRepository->GetStockUnusual();
    }

    /**
     * 取得庫存不同步清單
     *
     * @return array
     */
    public function GetStockDiff()
    {
        $result = [];
        $stock_list = Stock::where('id', '!=', 'A001')->get()->pluck('name', 'id')->toArray();
        //外掛
        foreach ($stock_list as $stock_id => $stock_name) {
            $db = ItemPlaceD::select('item_no', DB::raw('SUM(qty) AS qty'))
                ->whereHas('Main', function ($q) use ($stock_id) {
                    $q->where('s_id', $stock_id);
                })
                ->where('item_no', 'NOT LIKE', '99%')
                ->groupBy('item_no')
                ->get()
                ->pluck('qty', 'item_no')->toArray();
            //TMS
            $tms_db = STOC::select('SITEM', DB::raw('SUM(SNUMB) AS SNUMB'))
                ->whereExists(function ($q) { //排除產品註記暫停出貨
                    $q
                        ->select(DB::raw(1))
                        ->from('ITEM1 AS I1')
                        ->whereColumn('I1.ICODE1', 'SITEM')
                        ->where('I1.ISTOP', 0);
                })
                ->whereExists(function ($q) { //排除產品註記廠商停產
                    $q
                        ->select(DB::raw(1))
                        ->from('ITEM3 AS I3')
                        ->whereColumn('I3.ICODE3', 'SITEM')
                        ->where('I3.IPSTOP', 0);
                })
                ->whereHas('Item', function ($q) {
                    $q->whereNotIn('ITCOD', ['98', '99']);
                })
                ->where('STCOD', $stock_id) //主倉+不良
                ->groupBy('SITEM')
                ->get()
                ->pluck('SNUMB', 'SITEM')->toArray();
            $item_data = $this->GetItemsName();
            $item_no_list = array_keys($db + $tms_db);
            sort($item_no_list);
            foreach ($item_no_list as $item_no) {
                $tms_stock = $tms_db[$item_no] ?? 0;
                $stock = $db[$item_no] ?? 0;
                if ($tms_stock == $stock) {
                    continue;
                }
                $item_name = $item_data[$item_no] ?? '';
                $result[] = [
                    'item_no' => $item_no,
                    'item_name' => $item_name,
                    'stock_name' => $stock_name,
                    'tms_stock' => $tms_stock,
                    'stock' => $stock,
                ];
            }
        }
        return $result;
    }

    /**
     * 取得庫存可銷售量
     *
     * @param Request $request
     * @return array
     */
    public function GetStockCanSale(Request $request)
    {
        $itemList = $this->GetItemAllList();
        $types = $this->GetItemType();
        $type = $request->get('type');
        //取得要查詢的料號清單
        $items = [];
        switch ($type) {
            case '1': //大中小類
                $kind = $request->get('kind');
                $kind2 = $request->get('kind2');
                $kind3 = $request->get('kind3');
                if (!empty($kind) || !empty($kind2) || !empty($kind3)) {
                    $item_db = Item::select('ICODE');
                    if (!empty($kind)) {
                        $item_db->where('ITCOD', $kind);
                    }
                    if (!empty($kind2) || !empty($kind3)) {
                        $item_db->whereHas('ITEM1', function ($q) use ($kind2, $kind3) {
                            if (!empty($kind2)) {
                                $q->where('ITCO2', $kind2);
                            }
                            if (!empty($kind3)) {
                                $q->where('ITCO3', $kind3);
                            }
                        });
                    }
                    $items = $item_db->get()->pluck('item_no')->toArray();
                }
                break;
            case '2': //指定料號
                $items[] = $request->get('id');
                break;
            default:
                break;
        }
        if (count($items) == 0) {
            return ['data' => [],  'types' => $types, 'itemList' => $itemList, 'msg' => '找不到商品'];
        } else if (count($items) >= 2000) {
            return ['data' => [],  'types' => $types, 'itemList' => $itemList, 'msg' => '商品搜尋範圍過大，請增加搜尋條件'];
        }
        //取得TMS訂單需求
        $db = $this->AnalyzeCanSale($items);
        return ['data' => $db,  'types' => $types, 'itemList' => $itemList];
    }

    public function AnalyzeCanSale($items)
    {
        //取得TMS訂單需求
        $db = $this->baseRepository->GetStockCanSale($items);
        //取得未撿/未驗
        $date = new DateTime("now");
        $date->modify("1 month");
        $eDate = $this->GetTWDateStr($date);
        $date->modify("-2 month");
        $sDate = $this->GetTWDateStr($date);
        $uncheckedDb = $this->baseRepository->GetSalesUncheckedDetail($sDate, $eDate, $items);
        $headerData = $this->configService->GetConfig('order_convert_columns');
        $order_sample_service = (new OrderSampleService());
        $basket_order_db = OrderSampleM::with(['Order' => function ($q) {
            $q->select('OCOD1', 'OCOD4');
        }])
            ->where('completed', 0)
            ->get()
            ->filter(function ($item) {
                return empty($item->Order) || count($item->Order) == 0;
            });
        $basket_order_need = [];
        foreach ($basket_order_db as $order) {
            $src = $order->src;
            if (empty($order->headerData) || $order->headerData !== $headerData[$src]) { //無標題
                continue;
            } else {
                $header = $headerData[$src];
            }
            $columns = $order_sample_service->GetOrderSampleColumns($header, $src);
            if (empty($columns)) {
                continue;
            }
            $originalData = json_decode($order->original_data);
            foreach ($originalData as $item) {
                $item_no = $item[$columns['item_no']];
                $item_quantity = $item[$columns['item_quantity']];
                if (key_exists($item_no, $basket_order_need)) {
                    $basket_order_need[$item_no] += $item_quantity;
                } else {
                    $basket_order_need[$item_no] = $item_quantity;
                }
            }
        }
        $db->map(function ($item) use ($uncheckedDb, $basket_order_need) {
            $item->BasketNeed = $basket_order_need[$item->ICODE] ?? 0;
            $item->CanSale = $item->Stock - $item->OrderNeed - $item->BasketNeed;
            $item->UncheckedList = $uncheckedDb->where('HICOD', $item->ICODE);
        });

        return $db;
    }

    /**
     * 取得中國商品庫存
     *
     * @return array
     */
    public function GetStockCountry()
    {
        $result = [];
        $db = $this->baseRepository->GetStockCountry();
        $main_stock_db = ItemPlaceD::select('item_no', 'qty')
            ->whereHas('Main', function ($q) {
                $q->where('s_id', 'B001');
            })
            ->whereIn('item_no', $db->pluck('ICODE4')->toArray())
            ->get();
        foreach ($db as $d) {
            $item_no = $d->item_no;
            $name = $d->Item->item_name;
            $kind_name = $d->Item->kind_name;
            $main_stock = $main_stock_db->where('item_no', $item_no)->sum('qty');
            $sale_stock = empty($d->STOC) ? 0 : $d->STOC->sum('SNUMB');
            $result[$item_no] = [
                'name' => $name,
                'kind' => $kind_name,
                'main_stock' => $main_stock,
                'sale_stock' => $sale_stock,
            ];
        }

        return $result;
    }

    /**
     * 取得指定銷貨單的驗貨、託運紀錄
     *
     * @param Request $request
     * @return array
     */
    public function GetSalesCheck(Request $request)
    {
        $result = array();
        $id = $request->id; //銷貨單號/原始訂單號/備註1~3/託運單號
        if (empty($id)) {
            return $result;
        }
        $db = $this->baseRepository->GetOrderCheck($id);
        if (!empty($db)) {
            foreach ($db as $value) {
                $order_no = $value->order_no;
                $order_status = $value->status;
                $original_no = $value->original_no;
                if (count($value->Posein) == 0) {
                    $result[] = [
                        'order_no' => $order_no,
                        'order_status' => $order_status,
                        'sale_no' => '',
                        'original_no' => $original_no,
                        'tax' => '',
                        'TransportName' => '',
                        'ShopURL' => '',
                        'CheckDate' => '',
                        'PackageDate' =>  '',
                        'ShipDate' => '',
                        'TransportID' => '',
                    ];
                } else {
                    foreach ($value->Posein as $sales) {
                        $sale_no = $sales->sale_no;
                        $tax = $sales->tax;
                        $transportCode = $sales->trans;
                        $transportName = $sales->trans_name;
                        $transportID = $sales->ConsignTran;
                        $checkDate = $sales->PoseinCheckDataLog->where('CheckCount', 1)->first()->CREATE_Date ?? '';
                        $packageDate = $sales->LogPackage->first()->created_at ?? '';
                        $shipDate = $sales->LogShipment->first()->created_at ?? '';
                        //取得託運類別的包裹查詢網站
                        switch ($transportCode) {
                            case 'Shop-7':
                                $shopUrl = 'https://eservice.7-11.com.tw/e-tracking/search.aspx';
                                break;
                            case 'Shop-F':
                                $shopUrl = 'https://www.famiport.com.tw/Web_Famiport/page/process.aspx';
                                break;
                            case 'Shop-L':
                                $shopUrl = 'http://www.hilife.com.tw/serviceInfo_search.aspx';
                                break;
                            case 'Shop-OK':
                                $shopUrl = 'https://ecservice.okmart.com.tw/Tracking/Search';
                                break;
                            default:
                                $shopUrl = '';
                                break;
                        }
                        $result[] = [
                            'order_no' => $order_no,
                            'order_status' => $order_status,
                            'sale_no' => $sale_no,
                            'original_no' => $original_no,
                            'tax' => $tax,
                            'TransportName' => $transportName,
                            'ShopURL' => $shopUrl,
                            'CheckDate' => $this->GetTimeIgnoreMS($checkDate),
                            'PackageDate' => $packageDate,
                            'ShipDate' => $shipDate,
                            'TransportID' => $transportID,
                        ];
                    }
                }
            }
        }
        //卡在籃子的訂單
        $order_sample_db = OrderSampleM::select('order_no')->where('order_no', $id)->first();
        if (!empty($order_sample_db)) {
            $original_no = $order_sample_db->order_no;
            if (collect($result)->where('original_no', $original_no)->count() == 0) {
                $result[] = [
                    'order_no' => '',
                    'order_status' => '',
                    'sale_no' => '',
                    'original_no' => $original_no,
                    'tax' => '',
                    'TransportName' => '',
                    'ShopURL' => '',
                    'CheckDate' => '',
                    'PackageDate' =>  '',
                    'ShipDate' => '',
                    'TransportID' => '',
                ];
            }
        }

        return $result;
    }

    /**
     * 取得商品成長比較表
     *
     * @param Request $request
     * @return array
     */
    public function GetSalesComparison(Request $request)
    {
        $result = array();
        //輸入參數解析
        $group_type = $request->get('group_type', ''); //分組方式
        $id = $request->get('id', null); //料號篩選
        $type = $request->get('type', null); //大類篩選
        $type2 = $request->get('type2', null); //中類篩選
        $type3 = $request->get('type3', null); //小類篩選
        $customer = $request->get('customer', null); //客戶篩選

        switch ($group_type) {
            case 'month':
                $a_year = $request->get('a_year', Carbon::today()->format('Y'));
                $b_year = $request->get('b_year', Carbon::today()->format('Y'));
                $a_start_date = $this->GetTWDateStr(new DateTime($a_year . '-01-01'));
                $a_end_date = $this->GetTWDateStr(new DateTime($a_year . '-12-31'));
                $b_start_date = $this->GetTWDateStr(new DateTime($b_year . '-01-01'));
                $b_end_date = $this->GetTWDateStr(new DateTime($b_year . '-12-31'));

                $db =  $this->baseRepository->GetSalesComparisonMonth($a_start_date, $a_end_date, $b_start_date, $b_end_date, $id, $type, $type2, $type3, $customer);
                foreach ($db as $value) {
                    $month = intval($value->HDAT1);
                    $aTotal = floatval($value->A_HTOTA);
                    $bTotal = floatval($value->B_HTOTA);
                    $aNum = floatval($value->A_HINUM);
                    $bNum = floatval($value->B_HINUM);
                    $aCost = floatval($value->A_COST);
                    $bCost = floatval($value->B_COST);
                    $diff = $aTotal - $bTotal; //A-B
                    $diffNum = $aNum - $bNum;
                    if (empty($bTotal)) {
                        $diffPercent = '';
                    } else {
                        $diffPercent = number_format(($diff / $bTotal) * 100) . '%'; //(A-B)/B*100%
                    }
                    if (empty($bNum)) {
                        $diffNumPercent = '';
                    } else {
                        $diffNumPercent = number_format(($diffNum / $bNum) * 100) . '%'; //(A-B)/B*100%
                    }

                    $result[$month]['aTotal'] = number_format($aTotal);
                    $result[$month]['aCost'] = number_format($aCost);
                    $result[$month]['aNP'] = number_format($aTotal - $aCost);
                    $result[$month]['aNum'] = number_format($aNum);
                    $result[$month]['bTotal'] = number_format($bTotal);
                    $result[$month]['bCost'] = number_format($bCost);
                    $result[$month]['bNP'] = number_format($bTotal - $bCost);
                    $result[$month]['bNum'] = number_format($bNum);
                    $result[$month]['diff'] = number_format($diff);
                    $result[$month]['diffPercent'] = $diffPercent;
                    $result[$month]['diffNum'] = number_format($diffNum);
                    $result[$month]['diffNumPercent'] = $diffNumPercent;
                }
                break;
            default:
                $a_start_date = $this->GetTWDateStr(new DateTime($request->a_start_date));
                $a_end_date = $this->GetTWDateStr(new DateTime($request->a_end_date));
                $b_start_date = $this->GetTWDateStr(new DateTime($request->b_start_date));
                $b_end_date = $this->GetTWDateStr(new DateTime($request->b_end_date));

                $db =  $this->baseRepository->GetSalesComparison($a_start_date, $a_end_date, $b_start_date, $b_end_date, $id, $type, $type2, $type3, $customer);
                $aTotalNum = $db->sum('A_HINUM'); //A區間總數量
                $bTotalNum = $db->sum('B_HINUM'); //B區間總數量
                foreach ($db as $value) {
                    $itemNo = (string)$value->ICODE;
                    $itemName = $value->INAME;
                    $itemType = $value->ITNAM;
                    $itemSrc = $value->ISOUR;
                    $aTotal = floatval($value->A_HTOTA);
                    $bTotal = floatval($value->B_HTOTA);
                    $aNum = floatval($value->A_HINUM);
                    $bNum = floatval($value->B_HINUM);
                    $diff = $aTotal - $bTotal; //A-B
                    $diffNum = $aNum - $bNum;
                    $aCost = $aNum * $itemSrc;
                    $bCost = $bNum * $itemSrc;
                    if (empty($bTotal)) {
                        $diffPercent = '';
                    } else {
                        $diffPercent = number_format(($diff / $bTotal) * 100) . '%'; //(A-B)/B*100%
                    }
                    if (empty($bNum)) {
                        $diffNumPercent = '';
                    } else {
                        $diffNumPercent = number_format(($diffNum / $bNum) * 100) . '%'; //(A-B)/B*100%
                    }
                    if ($aTotalNum == 0) { //A區間總數量占比
                        $aProportion = '';
                    } else {
                        $aProportion = number_format(($aNum / $aTotalNum) * 100) . '%';
                    }
                    if ($bTotalNum == 0) { //B區間總數量占比
                        $bProportion = '';
                    } else {
                        $bProportion = number_format(($bNum / $bTotalNum) * 100) . '%';
                    }

                    $result[$itemNo]['name'] = $itemName;
                    $result[$itemNo]['type'] = $itemType;
                    $result[$itemNo]['aTotal'] = number_format($aTotal);
                    $result[$itemNo]['aCost'] = number_format($aCost);
                    $result[$itemNo]['aNP'] = number_format($aTotal - $aCost);
                    $result[$itemNo]['aNum'] = number_format($aNum);
                    $result[$itemNo]['aProportion'] = $aProportion;
                    $result[$itemNo]['bTotal'] = number_format($bTotal);
                    $result[$itemNo]['bCost'] = number_format($bCost);
                    $result[$itemNo]['bNP'] = number_format($bTotal - $bCost);
                    $result[$itemNo]['bNum'] = number_format($bNum);
                    $result[$itemNo]['bProportion'] = $bProportion;
                    $result[$itemNo]['diff'] = number_format($diff);
                    $result[$itemNo]['diffPercent'] = $diffPercent;
                    $result[$itemNo]['diffNum'] = number_format($diffNum);
                    $result[$itemNo]['diffNumPercent'] = $diffNumPercent;
                }
                break;
        }

        $aRange = $a_start_date . '-' . $a_end_date;
        $bRange = $b_start_date . '-' . $b_end_date;
        $itemList = $this->GetItemAllList();
        $typeList = $this->GetItemType();
        $customerList = $this->baseRepository->GetCustomer();

        return [
            'groupType' => $group_type,
            'data' => $result,
            'itemList' => $itemList,
            'typeList' => $typeList,
            'customerList' => $customerList,
            'aRange' => $aRange,
            'bRange' => $bRange,
        ];
    }

    /**
     * 取得銷貨明細
     *
     * @param Request $request
     * @return \Illuminate\Support\Collection
     */
    public function GetSalesDetail(Request $request)
    {
        $id = $request->id;
        if (empty($id)) {
            return collect();
        } else {
            $db = $this->baseRepository->GetSalesDetail($id);
            if (!empty($db)) {
                $db->buyer_note = $this->GetBuyerNotes($db->cust, $db->bak1);
            }
            return $db;
        }
    }

    //取得銷貨清單
    public function GetSalesList(array $sales_no)
    {
        $result = Posein::select('PCOD1', 'PCOD2', 'PJONO', 'PPNAM', 'TransportCode', 'TransportName', 'ConsignTran', 'PCMAN', 'PTELE')
            ->with(['OriginalOrder' => function ($q) {
                $q->select('order_no', 'order_date');
            }])
            ->addSelect('PBAK1', 'PBAK2', 'PBAK3', 'InsideNote')
            ->whereIn('PCOD1', $sales_no)
            ->get()
            ->sortBy(function ($item, $idx) {
                $sort = 1;
                if (in_array($item->trans, ['HCT', 'OTHER2'])) {
                    $sort = 0;
                } else {
                    $sort = 1;
                }
                $time = $item->OriginalOrder->order_date ?? '2100-12-31 23:59:59';
                return $sort . $time;
            });

        return $result;
    }

    /**
     * 取得新竹偏遠地區
     *
     * @param Request $request
     * @return array
     */
    public function GetHctRemote(Request $request)
    {
        $sDate = $this->GetTWDateStr(new DateTime($request->start_date));
        $eDate = $this->GetTWDateStr(new DateTime($request->end_date));
        $db = $this->baseRepository->GetHctRemote($sDate, $eDate);
        $confirm_list = OrderConfirmD::whereHas('Main', function ($q) {
            $q->where('order_type', '11');
        })
            ->whereIn('order_no', $db->pluck('OCOD4')->toArray())
            ->pluck('order_no')
            ->toArray();
        $config = OrderSampleService::GetLogisticsAddrMark('2');
        foreach ($db as $key => &$d) {
            if (!empty($d->Posein) && $d->Posein->where('TransportCode', 'HCT')->count() == 0) { //有銷貨單的情況下，排除未以新竹出貨的單
                $db->forget($key);
                continue;
            }
            $d->confirm = in_array($d->OCOD4, $confirm_list);
            $addr = $d->OrderSample->receiver_address ?? '';
            if (empty($addr)) {
                $db->forget($key);
            } else {
                $d->addr = $addr;
                $d->isRemote = $this->IsHctRemote($config, $addr);
            }
        }

        return ['data' => $db->sortByDesc('isRemote')];
    }

    /**
     * 新竹偏遠地區判斷
     *
     * @param [type] $config 設定檔
     * @param string $addr 地址
     * @return void
     */
    public function IsHctRemote($config, $addr)
    {
        if (empty($config) || count($config) == 0 || empty($addr)) {
            return false;
        }
        $result = false;
        $addr = str_replace('台', '臺', $addr);
        foreach ($config as $county => $areas) {
            if (mb_strpos($addr, $county, 0, 'utf-8') !== false) {
                if (count($areas) > 0) {
                    foreach ($areas as $area) {
                        if (mb_strpos($addr, $area, 0, 'utf-8') !== false) {
                            $result = true;
                            break 2;
                        }
                    }
                } else {
                    $result = true;
                    break;
                }
            }
        }
        return $result;
    }

    /**
     * 取得上車檢驗資料
     *
     * @param Request $request
     * @return array
     */
    public function GetShipmentCheck(Request $request)
    {
        $sendCode = '';
        $status = 0;
        $msg = '';
        $msg2 = '';
        $data = collect();
        $orderNo = $request->get('id');
        if (empty($orderNo)) {
            return [
                'status' => 0,
                'msg' => '請使用手持式掃描機上車',
                'data' => collect()
            ];
        }
        //解密銷貨單號
        $orderNo = hexdec($orderNo);
        //驗證銷貨單號格式
        if (!$this->VerifySaleNo($orderNo)) {
            return [
                'status' => 1,
                'msg' => '包裝條碼格式錯誤'
            ];
        }
        $data = $this->baseRepository->GetSalesNotReturned($orderNo);
        if (!empty($data)) {
            //驗貨紀錄
            if (empty($data->PoseinCheckDataLog)) {
                return [
                    'status' => 1,
                    'msg' => '訂單無驗貨紀錄',
                    'orderNo' => $orderNo,
                ];
            }
            //包裝紀錄
            if (empty(LogPackage::where('sale_no', $orderNo)->first())) {
                return [
                    'status' => 1,
                    'msg' => '訂單無包裝紀錄',
                    'orderNo' => $orderNo,
                ];
            }
            //狀態、毛利、備註檢查
            $shipment_status = [];
            $config = $this->configService->GetSysConfig('sche_gross_profit');
            $exclude_gpm = ItemConfig::select('id')->where('disable_gpm', 1)->pluck('id')->toArray();
            $standard_max = $config->firstWhere('value1', 'standard_max')->value2 ?? 50; //績效標準
            $standard_min = $config->firstWhere('value1', 'standard_min')->value2 ?? 20; //銷售量區間
            //備註檢查
            $note_error = !empty($this->GetBuyerNotes($data->cust, $data->bak1));
            //特定客代無須判斷毛利異常
            if ($data->Histin->whereIn('HICOD', $exclude_gpm)->count() > 0 || in_array($data->cust, ['C010', 'x001', 'x002', 'F001'])) {
                $gpm_error = false;
            } else if (empty($data->Order)) {
                return [
                    'status' => 1,
                    'msg' => '此銷貨單沒有對應的訂單',
                    'orderNo' => $orderNo,
                ];
            } else {
                $web_discount = $data->Order->OrderDetail->where('OICOD', 'XX126A')->first()->OTOTA ?? 0;
                if (!empty($web_discount)) {
                    $gpm = ($data->Order->OEARN + abs($web_discount)) / ($data->Order->OTOT1 + abs($web_discount)) * 100;
                } else {
                    $gpm = $data->Order->gross_margin;
                }
                $gpm_error = $gpm < $standard_min || $gpm > $standard_max || $data->Order->sales_total != $data->sales_total; //毛利率異常
            }
            //託運狀態判定
            $status = OrderShipmentStatusM::with('Details')->where('sales_no', $orderNo)->first();
            if (empty($status)) {
                if ($note_error) {
                    $shipment_status[] = '買家備註未覆核';
                }
                if ($gpm_error) {
                    $shipment_status[] = '毛利異常';
                }
            } else {
                if ($note_error && !$status->note_check) {
                    $shipment_status[] = '買家備註未覆核';
                }
                if ($gpm_error && !$status->gpm_check) {
                    $shipment_status[] = '毛利異常';
                }
                if (!$status->shipment_check) {
                    foreach ($status->Details as $s) {
                        if ($s->confirm) { //該狀態已覆核
                            continue;
                        }
                        $shipment_status[] = $s->status_name;
                    }
                }
            }
            //有狀態異常
            if (count($shipment_status) > 0) {
                return [
                    'status' => 1,
                    'msg' => '訂單有特殊狀態，禁止上車',
                    'orderNo' => $orderNo,
                    'shipment_status' => $shipment_status,
                ];
            }
            //取得寄件代號
            $hctAccountID = $this->baseRepository->GetHCTAccountID($orderNo);
            if (empty($hctAccountID)) { //非新竹貨運
                //備註有註記音速物流/空白物流/OTHER2
                if (empty($data->trans) || $data->trans == 'OTHER2') {
                    $sendCode = OrderSampleService::TransportType['YCS'];
                } else {
                    $sendCode = '超商';
                }
            } else { //新竹貨運
                if ($hctAccountID == '77063910002') { //客代==77063910002
                    $sendCode = '小件';
                } else { //客代!=77063910002
                    $sendCode = '多件';
                }
                //新竹偏遠地區判斷
                $remote_confirm = OrderConfirmD::where('order_no', $data->original_no)
                    ->whereHas('Main', function ($q) {
                        $q->where('order_type', '11');
                    })
                    ->get()->count() > 0;
                if (!$remote_confirm && !in_array($data->PPCOD, ['C005', 'C007'])) { //指定客代不判斷偏遠
                    $config = OrderSampleService::GetLogisticsAddrMark('2');
                    $addr = OrderSampleM::select('receiver_address')->where('order_no', $data->original_no)->first()->receiver_address ?? '';
                    $is_remote = $this->IsHctRemote($config, $addr);
                    if ($is_remote) {
                        return [
                            'status' => 1,
                            'msg' => '新竹偏遠地區未確認',
                            'orderNo' => $orderNo,
                        ];
                    }
                }
            }
            //判斷狀態
            if (LogShipment::where('PCOD1', $orderNo)->count() > 0) {
                $status = 2;
                $msg = '上車時間：' . LogShipment::where('PCOD1', $orderNo)->first()->created_at;
                $msg2 = '已經上車過，請確認是否重複出貨';
            } else {
                $status = 3;
                $msg = '上車';
                LogShipment::Create(
                    [
                        'PDATE' => $data->sale_date,
                        'PCOD1' => $orderNo
                    ]
                );
            }
        } else {
            $status = 1;
            $msg = '銷單不存在 或 已有退貨單，請勿出貨';
        }

        return [
            'sendCode' => $sendCode,
            'status' => $status,
            'msg' => $msg,
            'msg2' => $msg2,
            'orderNo' => $orderNo,
            'detail' => $data->Histin ?? null
        ];
    }

    /**
     * 取得商品總表
     *
     * @return \Illuminate\Support\Collection
     */
    public function GetItemSummary(Request $request)
    {
        $show_hidden = $request->get('show_hidden', false);
        $item_config = ItemConfig::select('cost')->where('cost', '!=', 0)->get();
        $data = $this->baseRepository->GetItemSummary($show_hidden)->map(function ($item) use ($item_config) {
            $item->cost = $item_config->firstWhere('id', $item->item_no)->cost ?? 0;
            return $item;
        });
        $item_kind = $this->configService->GetSysConfig('item_kind');

        return ['data' => $data, 'item_kind' => $item_kind];
    }

    /**
     * 根據大中小類找尋相關料號
     *
     * @param string $kind 大類
     * @param string $kind2 中類
     * @param string $kind3 小類
     * @return array
     */
    public function GetItemNoList(string $kind = null, string $kind2 = null, string $kind3 = null)
    {
        return $this->baseRepository->GetItemNoList($kind, $kind2, $kind3);
    }

    public function GetItemsName()
    {
        return Item::select('ICODE', 'INAME')->get()->pluck('INAME', 'ICODE')->toArray();
    }

    /**
     * 取得所有產品料號對應品名
     *
     * @return \Illuminate\Support\Collection
     */
    public function GetItemAllList(bool $includeStop = false)
    {
        return $this->baseRepository->GetItemAll($includeStop);
    }

    /**
     * 取得產品詳細資料
     *
     * @param array $icodeList
     * @return \Illuminate\Support\Collection
     */
    public function GetItemAllDetail(array $icodeList)
    {
        return $this->baseRepository->GetItemAllDetail($icodeList);
    }

    /**
     * 取得產品類別對應
     *
     * @return array
     */
    public function GetItemType()
    {
        $result = [];
        $db = $this->baseRepository->GetItemType();
        foreach ($db->sortBy('ITCOD')->groupBy('ITCOD') as $value => $group) {
            $name = $group->first()->ITNAM;
            $result[1][$value] = $name;
        }
        foreach ($db->sortBy('ITCO2')->groupBy('ITCO2') as $value => $group) {
            $name = $group->first()->ITNA2;
            $result[2][$value] = $name;
        }
        foreach ($db->sortBy('ITCO3')->groupBy('ITCO3') as $value => $group) {
            $name = $group->first()->ITNA3;
            $result[3][$value] = $name;
        }
        return $result;
    }

    /**
     * 取得產品供應商
     *
     * @return \Illuminate\Support\Collection
     */
    public function GetItemSupply()
    {
        return $this->baseRepository->GetItemSupply();
    }

    //取得地板顏色
    public function GetFloorColor($name, $config)
    {
        if (!empty($name)) {
            foreach ($config->sortByDesc('value3') as $cfg) {
                $key = $cfg->value1;
                $value = $cfg->value2;
                if (mb_strpos($name, $key) !== false) { //找到關鍵字
                    return $value;
                }
            }
        }
        return $name;
    }

    /**
     * 取得未完成訂單總表
     *
     * @return \Illuminate\Support\Collection
     */
    public function GetSalesLostCheck()
    {
        $home_trans = array_merge(self::home_trans, ['COM']);
        //erp訂單日期格式
        $date = new DateTime("now");
        //今日
        $eDate = $this->GetTWDateStr($date);
        //昨日往前14天
        $date->modify("-10 day");
        $sDate = $this->GetTWDateStr($date);
        $track_order = OrderRemarkM::select('OCOD1')->whereNull('completed_at')->where('track', 1)->pluck('OCOD1')->toArray();
        $result = [];
        $db = $this->baseRepository->GetSalesUnchecked($sDate, $eDate, $track_order);
        $keywords = ['預購', '合併', '親送', '缺'];
        foreach ($db as $order) {
            //備註
            $remark_user = '';
            $remark_track = $order->OrderRemarkM->track ?? 0;
            $remark = '';
            $remark_time = '';
            $remark_completed = !empty($order->OrderRemarkM->completed_at);
            if ($remark_completed) {
                continue;
            }
            if (!empty($order->OrderRemarkM) && count($order->OrderRemarkM->Details) > 0) {
                $last_r = $order->OrderRemarkM->Details->sortByDesc('created_at')->first();
                $remark_user = $last_r->User->name;
                $remark = $last_r->content;
                $remark_time = $last_r->created_at;
            }
            $status = '無';
            //訂單類型
            foreach ($keywords as $keyword) {
                if (
                    mb_strpos($order->bak1, $keyword) !== false || mb_strpos($order->bak2, $keyword) !== false || mb_strpos($order->bak3, $keyword) !== false
                    || mb_strpos($order->InsideNote, $keyword) !== false || mb_strpos($remark, $keyword)
                ) {
                    $status = $keyword == '缺' ? '缺貨' : $keyword;
                    break;
                }
            }
            //蝦皮宅配出貨
            if ($order->ShopeeTrans->count() > 0) {
                $shopee_ship_time = (new Carbon($order->ShopeeTrans->first()->created_at))->format('Y-m-d H:i:s');
            } else {
                $shopee_ship_time = '';
            }
            //訂單超過2天
            $order_date = $this->ConvertTWToDate($order->date);
            $is_expired = (new DateTime())->diff($order_date)->days > 2;
            $tmp = [
                'status' => $status,
                'cust_name' => $order->cust_name,
                'order_no' => $order->order_no,
                'OCOD4' => $order->original_no,
                'sale_no' => '',
                'transport' => '',
                'picking_src' => '',
                'picking_time' => '',
                'check_time' => '',
                'package_time' => '',
                'ship_time' => '',
                'shopee_ship_time' => $shopee_ship_time,
                'remark_user' => $remark_user, //最後一個備註的使用者
                'remark_track' => $remark_track,
                'remark' => $remark, //最後一個備註內容
                'remark_time' => $remark_time, //最後一個備註時間
                'undone' => true,
                'is_expired' => $is_expired,
            ];
            if (count($order->Posein) == 0) {
                $result[] = $tmp;
            } else {
                foreach ($order->Posein as $sale) {
                    if ($sale->trans == 'OTHER4') { //退貨物流
                        continue;
                    }
                    //銷單類型
                    foreach ($keywords as $keyword) {
                        if (
                            mb_strpos($sale->bak1, $keyword) !== false || mb_strpos($sale->bak2, $keyword) !== false || mb_strpos($sale->bak3, $keyword) !== false
                            || mb_strpos($sale->InsideNote, $keyword) !== false
                        ) {
                            $tmp['status'] = $keyword;
                            break;
                        }
                    }
                    $tmp['sale_no'] = $sale->sale_no;
                    $tmp['transport'] = $sale->trans_name;
                    $trans = $sale->trans;
                    //檢貨紀錄
                    if ($sale->print_count > 0) {
                        $tmp['picking_src'] = 'TMS印單';
                        $tmp['picking_time'] = (new Carbon($sale->print_time))->format('Y-m-d H:i:s');
                    } elseif ($sale->LogPickingPrint->count() > 0) {
                        $tmp['picking_src'] = '外掛印單';
                        $tmp['picking_time'] = (new Carbon($sale->LogPickingPrint->Max('created_at')))->format('Y-m-d H:i:s');
                    }
                    //驗貨紀錄
                    if ($sale->PoseinCheckDataLog->count() > 0) {
                        $tmp['check_time'] = (new Carbon($sale->PoseinCheckDataLog->Max('CREATE_Date')))->format('Y-m-d H:i:s');
                    }
                    //包裝紀錄
                    if ($sale->LogPackage->count() > 0) {
                        $tmp['package_time'] = (new Carbon($sale->LogPackage->Max('created_at')))->format('Y-m-d H:i:s');
                    }
                    //上車紀錄
                    if ($sale->LogShipment->count() > 0) {
                        $tmp['ship_time'] = (new Carbon($sale->LogShipment->Max('created_at')))->format('Y-m-d H:i:s');
                    }
                    //撿貨/驗貨/包裝/上車任一未完成
                    $undone = empty($tmp['picking_time']) || empty($tmp['check_time']) || empty($tmp['package_time']) || empty($tmp['ship_time'])
                        || (empty($tmp['shopee_ship_time']) && $order->cust == 'S003' && in_array($trans, $home_trans) && $order->date >= '113.01.18');
                    if ($undone || (!empty($remark_track) && !$remark_completed)) {
                        $tmp['undone'] = $undone;
                        $result[] = $tmp;
                    }
                    if (!$undone && empty($remark_track)) { //已完成 且 沒追蹤
                        OrderRemarkM::updateOrCreate(
                            ['OCOD1' => $order->order_no],
                            ['completed_at' => new DateTime()]
                        );
                    }
                }
            }
        }
        //辦公室電腦顯示警示
        $alert = false;
        // if (Auth::user()->id == '37') {
        //     $alert = collect($result)->filter(function ($item) {
        //         $remark_time = $item['remark_time'];
        //         return $remark_time == '' || $remark_time < Carbon::today();
        //     })->count() > 0;
        // }
        return ['sDate' => $sDate, 'eDate' => $eDate, 'data' => $result, 'alert' => $alert];
    }

    //取得產品的大、小類資訊
    public function GetItemDetail()
    {
        $result = [];
        $db = DB::table('ITEM AS I')
            ->select('I.ICODE', 'I.INAME', 'I.ITCOD', 'I1.ITCO3', 'I1.ITNA3')
            ->join('ITEM1 AS I1', 'I1.ICODE1', 'I.ICODE')
            ->whereExists(function ($q) { //排除產品註記暫停出貨
                $q
                    ->select(DB::raw(1))
                    ->from('ITEM1 AS I1')
                    ->whereColumn('I1.ICODE1', 'I.ICODE')
                    ->where('I1.ISTOP', 0);
            })
            ->whereExists(function ($q) { //排除產品註記廠商停產
                $q
                    ->select(DB::raw(1))
                    ->from('ITEM3 AS I3')
                    ->whereColumn('I3.ICODE3', 'I.ICODE')
                    ->where('I3.IPSTOP', 0);
            })
            ->get();
        foreach ($db as $d) {
            $result[$d->ICODE] = [
                'name' => $d->INAME, //品名
                'kind1' => $d->ITCOD, //大類代碼
                'kind3' => $d->ITCO3, //小類代碼
                'kind3_name' => $d->ITNA3, //小類名稱
            ];
        }
        return $result;
    }

    /**
     * 取得過重的訂單
     *
     * @return array
     */
    public function GetOverweightOrder()
    {
        //erp訂單日期格式
        $date = new DateTime("now");
        //今日
        $eDate = $this->GetTWDateStr($date);
        //今日往前15天
        $date->modify("-15 day");
        $sDate = $this->GetTWDateStr($date);
        $shop_max_weight = 5000;
        $shopee_max_weight = 9500;
        $config_items = [];
        $items_name = $this->GetItemsName();
        $heavy_config = $this->configService->GetConfig("heavy_item");
        $big_config = $this->configService->GetConfig("big_item");
        $heavy_items = [];
        if (!empty($heavy_config) && count($heavy_config) > 0) {
            foreach ($heavy_config as $config) {
                foreach ($config->items as $item) {
                    $config_items[] = (string)$item;
                    $heavy_items[$item] = [
                        'weight' => $config->weight,
                        'name' => $items_name[$item] ?? ''
                    ];
                }
            }
        }
        $big_items = [];
        if (!empty($big_config) && count($big_config) > 0) {
            foreach ($big_config as $config) {
                $items = [];
                foreach ($config->items as $item) {
                    $items[$item] = $items_name[$item];
                }
                $config_items = array_merge($config_items, $config->items);
                $big_items[] = [
                    'qty' => $config->qty,
                    'items' => $items
                ];
            }
        }
        $db = $this->baseRepository->GetHeavyOrder($sDate, $eDate, $config_items);
        //取得訂單的總重量
        $result = [];
        foreach ($db->groupBy('OCOD1') as $order_no => $group) {
            $first_data = $group->first();
            $original_no = $first_data->Main->original_no ?? '';
            $transport_code = $first_data->Main->trans ?? '';
            $sales_no = $first_data->Posein->pluck('sale_no')->toArray();
            $total_weight = 0;
            $status = '';
            //過重計算
            foreach ($group as $item) {
                $item_no = $item->item_no;
                $item_qty = $item->qty;
                $weight = 0;
                if (key_exists($item_no, $heavy_items)) {
                    $weight = $item_qty * $heavy_items[$item_no]['weight'];
                    $total_weight += $weight;
                }
            }
            if (($transport_code == 'Shop-S' && $total_weight > $shopee_max_weight) ||
                ($transport_code != 'Shop-S' && $total_weight > $shop_max_weight)
            ) {
                $status .= '過重';
            }
            //過大計算
            foreach ($big_items as $big_item) {
                $total_qty = $group->whereIn('OICOD', array_keys($big_item['items']))->sum('OINUM');
                if ($total_qty > $big_item['qty']) {
                    if (!empty($status)) {
                        $status .= ',';
                    }
                    $status .= '過大';
                    break;
                }
            }
            if (!empty($status)) {
                $result[] = [
                    'order_no' => $order_no,
                    'original_no' => $original_no,
                    'sales_no' => $sales_no,
                    'status' => $status,
                ];
            }
        }

        return [
            'data' => $result,
            'heavy_items' => $heavy_items,
            'big_items' => $big_items,
            'shop_max_weight' => $shop_max_weight,
            'shopee_max_weight' => $shopee_max_weight
        ];
    }

    /**
     * 取得訂單含有統編需求
     *
     * @param Request $request
     * @return array
     */
    public function GetOrderTaxID(Request $request)
    {
        if (empty($request->start_date) || empty($request->end_date)) {
            $nowTWDate = $this->GetTWDateStr(new DateTime("now"));
            $sDate = $nowTWDate;
            $eDate = $nowTWDate;
        } else {
            $sDate = $this->GetTWDateStr(new DateTime($request->start_date));
            $eDate = $this->GetTWDateStr(new DateTime($request->end_date));
        }
        $db = $this->baseRepository->GetOrderTaxRemark($sDate, $eDate);
        $result = array();
        if (!empty($db)) {
            foreach ($db as $value) {
                $orderNo = (string)$value->order_no;
                $remark1 = $value->bak1;
                $remark2 = $value->bak2;
                $remark3 = $value->bak3;
                if ($this->IsContainsTaxID($remark1, $remark2, $remark3)) { //需開立統編
                    if (count($value->Posein) > 0 && $value->Posein->first()->UseExtendInvoiceInfo && !empty($value->Posein->first()->RecieveCTCOD)) { //已啟用額外發票資訊 且 有輸入統一編號
                        continue;
                    } elseif (!empty($value->Sinvo->first()->company_no)) { //已壓統一編號
                        continue;
                    } else {
                        $result[$orderNo] = $value;
                    }
                }
            }
        }
        return $result;
    }

    /**
     * 取得需給的贈品清單
     *
     * @param array $config 贈品設定檔
     * @param \Illuminate\Support\Collection $items 購買明細
     * @return array
     */
    public function GetOrderGift($config, $items)
    {
        $result = [];
        foreach ($config as $cfg) {
            $repeat = $cfg->repeat;
            $require_qty = $cfg->require_qty;
            $require_item = $cfg->require_item;
            $gift_item = $cfg->gift_item;
            if ($require_qty <= 0 || empty($require_item) || empty($gift_item)) {
                continue;
            }
            $order_qty = $items->whereIn('HICOD', $require_item)->sum('HINUM');
            if ($order_qty >= $require_qty) {
                if ($repeat) {
                    $qty = floor($order_qty / $require_qty);
                } else {
                    $qty = 1;
                }
                $result[] = [
                    'items' => $gift_item,
                    'qty' => $qty
                ];
            }
        }
        return $result;
    }

    /**
     * 取得客訴單
     *
     * @param Request $request
     * @return array
     */
    public function GetCustomerComplaint(Request $request)
    {
        $only_undone = $request->get('only_undone', 0);
        $sDate = $request->get('start_date', Carbon::today()->addDays(-15)->format('Y-m-d')) . ' 00:00:00';
        $eDate = $request->get('end_date', Carbon::today()->format('Y-m-d')) . ' 23:59:59';
        $data = collect();
        try {
            $data = CustomerComplaint::with(['Order:OCOD1,OCOD4,OCCOD,OCNAM', 'Order.POSEIN:PCOD1,PCOD2'])->orderby('urgent', 'DESC')->orderby('updated_at', 'DESC');
            $orderNo = $request->get('order_no');
            if (!empty($orderNo)) { //搜尋指定，含作廢
                $data->where(function ($query) use ($orderNo) {
                    $query
                        ->where('order_no', $orderNo)
                        ->orWhere('origin_no', $orderNo);
                });
            } elseif ($only_undone) {
                $data->whereNotIn('status',  ['0', '99']);
            } else { //搜尋全部區間，不含作廢
                $data->whereBetween("updated_at", [$sDate, $eDate])->where('status', '!=', '99');
                //是否含結案
                $includeDone = $request->get('include_done', 0);
                if (empty($includeDone)) {
                    $data->where('status', '!=', '0');
                }
            }
            $data = $data->get();
        } catch (Exception $ex) {
        }
        //狀態、特殊註記代碼
        $config = $this->configService->GetSysConfig('customer_complaint_status');
        $markConfig = $this->configService->GetSysConfig('customer_complaint_mark');
        $undefineStr = 'Undefined';
        foreach ($data as $d) {
            //狀態
            $status_name = $undefineStr;
            if (count($config) > 0 && $config->contains('value1', $d->status)) {
                $status_name = $config->firstWhere('value1', $d->status)->value2;
            }
            //特殊註記
            $mark_names = [];
            if (!empty($d->mark)) {
                $marks = explode(',', $d->mark);
                foreach ($marks as $mark) {
                    if (count($markConfig) > 0 && $markConfig->contains('value1', $mark)) {
                        $mCfg = $markConfig->firstWhere('value1', $mark);
                        $mCfg->count++; //單數累加
                        $mark_names[] = ['name' => $mCfg->value2, 'color' => $mCfg->value3];
                    } else {
                        $mark_names[] = ['name' => $undefineStr, 'color' => 'white'];
                    }
                }
            }
            $d->status_name = $status_name;
            $d->mark_names = $mark_names;
            $d->elapsed_days = Carbon::today()->diff($d->updated_at)->days;
            $d->is_timeout = $d->status != 0 && $d->status != 99 && $d->elapsed_days > 3;
        }

        return ['data' => $data, 'config' => $config, 'markConfig' => $markConfig];
    }

    /**
     * 取得保固資料
     *
     * @param Request $request
     * @return array
     */
    public function GetOrderWarranty(Request $request)
    {
        //網址
        $csURL = $this->configService->GetSysConfig('cs_url')->first()->value1 ?? '';
        //搜尋訂單
        $tms_sys = $request->get('tms_sys');
        $sale_no = $request->get('sale_no');
        $original_no = $request->get('original_no');
        $user_id = $request->get('user_id');
        $start_date = $request->get('start_date');
        $end_date = $request->get('end_date');
        $customer = $request->get('customer');
        $customers = $this->baseRepository->GetCustomer();
        //有起訖日期，且日期相反，則互換
        if (!empty($start_date) && !empty($end_date) && new DateTime($start_date) > new DateTime($end_date)) {
            $tmp = $start_date;
            $start_date = $end_date;
            $end_date = $tmp;
        }
        if (!empty($sale_no) || !empty($original_no) || !empty($user_id) || !empty($start_date) || !empty($end_date) || !empty($customer)) {
            if ($tms_sys == 'pos') {
                $db = $this->baseRepository->GetPosOrderWarranty($sale_no, $start_date, $end_date);
            } else {
                $db = $this->baseRepository->GetOrderWarranty($sale_no, $original_no, $user_id, $start_date, $end_date, $customer);
                foreach ($db as $d) {
                    $d->sale_src = $customers->firstWhere('CCODE', $d->sale_src)->CNAM2 ?? $d->sale_src;
                }
            }
        } else {
            $db = collect([]);
        }

        return ['orders' => $db, 'csURL' => $csURL, 'customers' => $customers];
    }

    /**
     * array insert
     *
     * @param array $array
     * @param integer $index
     * @param [type] $insertData
     * @return array
     */
    protected function array_insert(array $array, int $index, $insertData)
    {
        if (empty($array) || $index === false) {
            return $array;
        }
        return array_merge(array_slice($array, 0, $index + 1), [$insertData], array_slice($array, $index + 1));
    }

    /**
     * 取得料號在各庫存的數量
     *
     * @param array $item_no_list 料號清單
     * @return array
     */
    public function GetStock(array $item_no_list)
    {
        $main_stock_db = ItemPlaceD::select('item_no', DB::raw("SUM(qty) AS qty"))
            ->whereHas('Main', function ($q) {
                $q->where('s_id', 'B001');
            })
            ->whereIn('item_no', $item_no_list)
            ->groupBy('item_no')
            ->get()
            ->pluck('qty', 'item_no')
            ->toArray();
        //出貨倉,退貨倉
        $db = $this->baseRepository->GetStock($item_no_list, ['A001', 'D001', 'H001']);
        $result = array();
        foreach ($db as $value) {
            $itemNo = (string)$value->SITEM; //料號
            $itemNum = $value->SNUMB; //庫存量
            $wareHouseName = $value->STNAM; //倉庫名稱

            //加總
            $total = &$result[$itemNo]['total'];
            if (empty($total)) {
                $total = $itemNum;
            } else {
                $total += $itemNum;
            }
            //加入主倉
            $main_stock = &$result[$itemNo]['主倉儲'];
            if (empty($main_stock)) {
                $qty = $main_stock_db[$itemNo] ?? 0;
                $main_stock = $qty;
                $total += $qty;
            }

            $result[$itemNo][$wareHouseName] = $itemNum;
        }

        return $result;
    }

    /**
     * 取得指定日期的中國商品進貨紀錄
     *
     * @return \Illuminate\Support\Collection
     */
    public function GetChinaSales()
    {
        $result = [];
        $year = (new DateTime)->format('Y') - 1911;
        $last_year = $year - 1;
        $db = $this->baseRepository->GetChinaSales();
        foreach (
            $db->groupBy(function ($item, $key) {
                return $item->ITNAM . $item->ITNA3 . $item->ICODE4;
            }) as $g
        ) {
            $kind = $g->first()->ITNAM;
            $kind3 = $g->first()->ITNA3;
            $item_no = $g->first()->ICODE4;
            $item_name = $g->first()->INAME;
            $stock = $g->first()->INUMB;
            $purchase_undone = $g->first()->purchase_undone;
            $tmp = [
                'kind' => $kind,
                'kind3' => $kind3,
                'item_no' => $item_no,
                'item_name' => $item_name,
                'stock' => $stock,
                'purchase_undone' => $purchase_undone ?? 0,
            ];
            $sales = [];
            $last_sales = [];
            $last_total_sale = 0;
            $last_total_month = 0;
            for ($i = 1; $i <= 12; $i++) {
                $sales['sales_' . $i] = $g->where('DT', $year . '.' . str_pad($i, 2, '0', STR_PAD_LEFT))->sum('NUM');
                $last_sale = $g->where('DT', $last_year . '.' . str_pad($i, 2, '0', STR_PAD_LEFT))->sum('NUM');
                $last_total_sale += $last_sale;
                $last_total_month++;
                $last_sales['sales_' . $i . '_last'] = $last_sale;
            }
            $tmp = array_merge($tmp, $sales, $last_sales);
            $tmp['avg_sales'] = round($last_total_sale /  $last_total_month);
            $result[$kind][$kind3][] = $tmp;
        }

        return $result;
    }

    /**
     * 取得買家備註
     *
     * @param string $cust 客代
     * @param string $remark 備註
     * @return string
     */
    public function GetBuyerNotes(string $cust, string $remark)
    {
        if (empty($cust) || empty($remark)) {
            return '';
        }
        $sKeyword = '備註:';
        $eKeyword = '';
        switch ($cust) {
            case 'S003': //蝦皮2
            case 'S005': //蝦皮不分店
                $sKeyword = '備註:';
                $eKeyword = '/買家ID';
                break;
            case 'S001': //蝦皮1，API
            case 'S004': //蝦皮3，API
                $sKeyword = '備註:';
                $eKeyword = Str::contains($remark, '外掛備註') ? '/買家ID' : '/賣家蝦幣';
                break;
            case 'S002': //shopline
                $sKeyword = '備註:';
                $eKeyword = '/物流';
                break;
            case 'S006':
                $sKeyword = ' /';
                $eKeyword = '/物流:';
                break;
            default:
                break;
        }
        if (!Str::contains($remark, $sKeyword) || empty($sKeyword)) { //未含有備註關鍵字
            return '';
        }
        $sIdx = strpos($remark, $sKeyword);
        $eIdx = empty($eKeyword) ? false : strrpos($remark, $eKeyword);
        $offset = $sIdx + strlen($sKeyword);
        if ($eIdx === false) { //找不到結束關鍵字，擷取到最後
            $length = null;
        } else {
            $length = $eIdx - $offset;
        }
        return substr($remark, $offset, $length);
    }

    //second to H:i:s
    public function ConvertSecondToTime($sec)
    {
        if (empty($sec)) {
            return '';
        } else {
            return sprintf('%02d:%02d:%02d', ($sec / 3600), ($sec / 60 % 60), $sec % 60);
        }
    }

    /**
     * 取得指定月份的每天日期
     *
     * @param DateTime $date
     * @return array
     */
    public function GetEachDayByMonth(DateTime $date)
    {
        $result = array();

        $startTime = new DateTime($date->format('Y-m-01'));
        $endTime = new DateTime($date->format('Y-m-t'));
        while ($startTime <= $endTime) {
            $result[] = $startTime->format('Y-m-d');
            $startTime->modify('1 day');
        }

        return $result;
    }

    /**
     * 取得指定區間的每天日期
     *
     * @param string $sDate 起日。yyyy/mm/dd hh:mm:ss
     * @param string $eDate 訖日。yyyy/mm/dd hh:mm:ss
     * @return array
     */
    public function GetEachDay(string $sDate, string $eDate)
    {
        $result = array();
        if (empty($sDate) || empty($eDate)) {
            return $result;
        }
        $start = new DateTime($sDate);
        $end = new DateTime($eDate);
        if ($start > $end) { //起訖相反，互換日期
            $tmp = $start;
            $start = $end;
            $end = $tmp;
        }

        while ($start <= $end) {
            $result[] = $start->format('Y-m-d');
            $start->modify('+1 day');
        }

        return $result;
    }

    /**
     * 取得指定區間的每月日期
     *
     * @param string $sDate 起日。yyyy/mm/dd hh:mm:ss
     * @param string $eDate 訖日。yyyy/mm/dd hh:mm:ss
     * @return array
     */
    public function GetEachMonth(string $sDate, string $eDate)
    {
        $result = [];
        if (empty($sDate) || empty($eDate)) {
            return $result;
        }
        $start = new DateTime($sDate);
        $end = new DateTime($eDate);
        if ($start > $end) { //起訖相反，互換日期
            $tmp = $start;
            $start = $end;
            $end = $tmp;
        }


        while ($start <= $end) {
            $result[] = $start->format('Y-m');
            $start->modify('+1 month');
        }

        return $result;
    }

    /**
     * 取得星期幾
     *
     * @param string $date
     * @return string
     */
    public function GetWeekday($date)
    {
        $w = date('w', strtotime($date));
        return self::week[$w];
    }

    /**
     * 判斷字串陣列是否有統編敘述
     *
     * @param string $remark1
     * @param string $remark2
     * @param string $remark3
     * @return bool
     */
    private function IsContainsTaxID($remark1, $remark2, $remark3)
    {
        $pattern = "/[\D][\d]{8}[\D]/";
        foreach ([$remark1, $remark2, $remark3] as $value) {
            if (strstr($value, '統編') || strstr($value, '統一編號')) {
                return true;
            } elseif (preg_match_all($pattern, $value, $matches)) {
                foreach ($matches[0] as $match) {
                    $num = substr($match, 1, 8);
                    if ($this->VerifyTaxID($num)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * 民國112年4月1號前，統編驗證規則。規則參考 https://www.fia.gov.tw/singlehtml/3?cntId=c4d9cff38c8642ef8872774ee9987283
     *
     * @param string $taxId
     * @return bool
     */
    private function VerifyTaxID($taxId)
    {
        //空白、非數字、非8碼
        if (empty($taxId) || !is_numeric($taxId) || strlen($taxId) != 8) {
            return false;
        }
        $cx = [1, 2, 1, 2, 1, 2, 4, 1];
        $ary = str_split($taxId);

        if ($ary[6] == 7) { //第7位數為7
            $sum = 0;
            for ($i = 0; $i < 8; $i++) {
                if ($i == 6) {
                    continue;
                }
                $multiply = $ary[$i] * $cx[$i];
                $sum += floor($multiply / 10) + $multiply % 10;
            }
            return $sum % 10 == 0 || ($sum + 1) % 10 == 0;
        } else { //第7位數非7
            $sum = 0;
            for ($i = 0; $i < 8; $i++) {
                $multiply = $ary[$i] * $cx[$i];
                $sum += floor($multiply / 10) + $multiply % 10;
            }
            return $sum % 10 == 0;
        }
    }

    //轉換物流代碼
    public function ConvertTransport(&$TransportCode, &$TransportName, $ConsignTran)
    {
        $TransportCode = self::ConvertTransCode($TransportCode);
        //託運類別代碼
        if (empty($TransportCode)) { //物流空白且有託運單號=音速
            if (empty($ConsignTran)) {
                $TransportCode = 'Unknow';
                $TransportName = '無物流';
            } else {
                $TransportCode = 'YCS';
                $TransportName = '音速';
            }
        } elseif ($TransportCode == 'OTHER2') { //其他2=音速
            $TransportCode = 'YCS';
            $TransportName = '音速';
        }
    }

    //訂單是否為棧板
    public function IsPallet($bak1, $bak2, $bak3, $inbak)
    {
        $key = '棧板';
        if (
            mb_strpos($bak1, $key, 0, 'utf-8') !== false ||
            mb_strpos($bak2, $key, 0, 'utf-8') !== false ||
            mb_strpos($bak3, $key, 0, 'utf-8') !== false ||
            mb_strpos($inbak, $key, 0, 'utf-8') !== false
        ) {
            return true;
        } else {
            return false;
        }
    }

    //驗證銷貨單號格式
    public function VerifySaleNo(string $sale_no)
    {
        if (empty($sale_no) || strlen($sale_no) != 11) {
            return false;
        } else {
            return true;
        }
    }

    //驗證新竹物流重量件數
    public function VerifyHCT($qty, $weight)
    {
        if (empty($qty) && empty($weight)) {
            return true;
        } elseif ($weight / $qty != 30) {
            return false;
        } else {
            return true;
        }
    }

    //驗證新竹LVT件數
    public function VerifyHCT_LVT($qty, $lvt_qty)
    {
        if (empty($lvt_qty)) {
            return true;
        } else {
            $tmp = (floor($lvt_qty / 10) + ($lvt_qty % 10 >= 4 ? 1 : 0));
            return $tmp == $qty;
        }
    }

    //取得訂單過期狀態
    public function GetOrderExpired($cust, $total_hours)
    {
        if (empty($cust) || empty($total_hours) || !in_array($cust, ['S001', 'S003', 'S004'])) { //非蝦皮
            return '';
        }
        if ($total_hours >= 48) {
            return '訂單過期';
        } elseif ($total_hours >= 28 && $cust != 'S003') { //1、3店優選
            return '優選過期';
        } else {
            return '';
        }
    }

    //取得總小時
    public function GetTotalHours(DateInterval $time)
    {
        return ($time->d * 24) + $time->h + ($time->i / 60);
    }

    //根據料號取得大小類
    public function GetKind($item_no)
    {
        if (empty($item_no) || strlen($item_no) != 11) {
            return null;
        }
        $kind = substr($item_no, 0, 2);
        $kind3 = substr($item_no, 5, 3);
        $seq = substr($item_no, 8, 3);
        return ['kind' => $kind, 'kind3' => $kind3, 'seq' => $seq];
    }

    /**
     * 取得訂單經過時間
     *
     * @param DateTime $order_time 訂單成立時間
     * @param DateTime $end_time 截止時間
     * @param \Illuminate\Support\Collection $holiday_config 國定假日設定檔
     * @return float 經過的小時
     */
    public function GetOrderElapsedHour(DateTime $order_time, DateTime $end_time, \Illuminate\Support\Collection $holiday_config)
    {
        $start = $order_time;
        $end = $end_time;
        $result = 0;
        while ($start < $end) {
            $config = $holiday_config->where('date', $start->format('Y-m-d'))->first();
            $is_holiday = false;
            if (empty($config)) { //正常節日
                $day_of_week = $start->format('w'); //0=星期日、6=星期六
                $is_holiday = in_array($day_of_week, [0, 6]); //六日
            } else { //特殊節日
                $is_holiday = $config->isholiday == 1;
            }
            //非國定假日
            if (!$is_holiday) {
                $dt_end = $end;
                if ($end->format('Y-m-d') == $start->format('Y-m-d')) { //同一天，取當下時間
                    $dt_end = $end;
                } else {
                    $dt_end = new DateTime($start->format('Y-m-d 23:59:59'));
                }
                $result += $this->GetTotalHours($dt_end->diff($start));
            }
            $tomorrow = clone $start;
            $tomorrow->modify('+1 day');
            $start = new DateTime($tomorrow->format('Y-m-d 00:00:00'));
        }
        return $result;
    }

    //群撿查詢
    public function GetGroupPicking()
    {
        $config = $this->configService->GetSysConfig('group_picking');
        $item_list = $config->pluck('value2')->toArray();
        $s_date = $this->GetTWDateStr(Carbon::today()->addDays(-7));
        $e_date = $this->GetTWDateStr(Carbon::today());
        $db = Posein::select('PCOD1', 'TransportCode')
            ->with(['PoseinCheckDataLog', 'LogPackage'])
            ->withWhereHas('Histin', function ($q) use ($item_list) {
                $q->select('HCOD1', 'HICOD', 'HINAM', 'HINUM')
                    ->whereIn('HICOD', $item_list);
            })
            ->whereBetween('PDAT1', [$s_date, $e_date])
            ->get();
        $result = [];
        foreach ($db as $d) {
            if (!empty($d->LogPackage) && count($d->LogPackage) > 0) { //排除已包裝
                continue;
            }
            $is_check = !empty($d->PoseinCheckDataLog) && count($d->PoseinCheckDataLog) > 0;
            $sale_no = $d->sale_no;
            //物流類型
            $trans = $d->trans;
            $type = '';
            if (in_array($trans, self::shop_trans)) {
                $type = '超商';
            } else if (in_array($trans, self::home_trans)) {
                $type = '宅配';
            } else {
                $type = '其他';
            }
            //明細
            foreach ($d->Histin as $detail) {
                $item_no = $detail->item_no;
                $item_name = $detail->item_name;
                $cfg = $config->where('value2', $item_no)->first();
                $group_name = $cfg->value1;
                $box = $cfg->value3;
                if (empty($box) || $box < 0) {
                    $box = 1;
                }
                $qty = $detail->qty;
                $check_qty = $is_check ? $qty : 0;
                $tmp = [
                    'group_name' => $group_name,
                    'box' => $box,
                    'item_name' => $item_name,
                    'qty' => $qty,
                    'check_qty' => $check_qty,
                    'sales' => [$sale_no => ['qty' => $qty, 'is_check' => $is_check]],
                ];
                if (key_exists($item_no, $result)) {
                    if (key_exists($type, $result[$item_no])) {
                        $result[$item_no][$type]['qty'] += $qty;
                        $result[$item_no][$type]['check_qty'] += $check_qty;
                        $result[$item_no][$type]['sales'][$sale_no] = ['qty' => $qty, 'is_check' => $is_check];
                    } else {
                        $result[$item_no][$type] = $tmp;
                    }
                } else {
                    $result[$item_no] = [];
                    $result[$item_no][$type] = $tmp;
                }
                if ($box == 1) {
                    $box_qty = '';
                    $check_box_qty = '';
                } else {
                    $box_qty = floor($result[$item_no][$type]['qty'] / $box) . '箱 ' . ($result[$item_no][$type]['qty'] % $box) . '包';
                    $check_box_qty = floor($result[$item_no][$type]['check_qty'] / $box) . '箱 ' . ($result[$item_no][$type]['check_qty'] % $box) . '包';
                }
                $result[$item_no][$type]['box_qty'] = $box_qty;
                $result[$item_no][$type]['check_box_qty'] = $check_box_qty;
            }
        }

        return ['data' => $result, 'config_url' => route('sys_config.edit', SysConfigM::where('_key', 'group_picking')->first()->id)];
    }

    private function GetEstimatedDateStatusName($key)
    {
        switch ($key) {
            case 1: //預排
                return '預定';
            case 2: //已確定
                return '確定';
            case 3: //延遲出貨
                return '延遲';
        }
    }
}
