<?php

namespace App\Services;

use DateTime;
use Carbon\Carbon;
use App\Models\TMS\Order;
use App\Models\TMS\Histin;
use Illuminate\Http\Request;
use App\Services\BaseService;
use App\Models\TMS\OrderDetail;
use App\Services\ConfigService;
use App\Models\yherp\ItemConfig;
use App\Models\yherp\OrderSampleM;
use Illuminate\Support\Facades\DB;
use App\Models\yherp\ItemMarketingD;
use App\Repositories\BaseRepository;
use App\Services\OrderSampleService;
use App\Models\yherp\InventoryTurnoverRating;

class SaleService extends BaseService
{
    public function GetReport(Request $request)
    {
        $start_date = $request->get('start_date', Carbon::today()->format('Y-m-d'));
        $end_date = $request->get('end_date', Carbon::today()->format('Y-m-d'));
        $sTWDate = $this->GetTWDateStr(new DateTime($start_date));
        $eTWDate = $this->GetTWDateStr(new DateTime($end_date));
        //籃子未進系統統計
        $notImportSum = 0;
        $not_import = OrderSampleM::select('order_no', 'src', 'header', 'original_data')
            ->whereBetween('order_date', [$start_date . ' 00:00:00', $end_date . ' 23:59:59']) //訂單成立日期
            ->where('completed', 0)
            ->get();
        $exists_orders = Order::select('OCOD4')->whereIn('OCOD4', $not_import->pluck('order_no')->unique()->toArray())->pluck('OCOD4')->toArray();
        $orderSampleService = new OrderSampleService();
        $headerData = (new ConfigService(new BaseRepository()))->GetConfig('order_convert_columns');
        foreach ($not_import as $o) {
            $order_no = $o->order_no;
            if (in_array($order_no, $exists_orders)) {
                continue;
            }
            $src = $o->src;
            if (empty($o->headerData)) { //無標題
                continue;
            } else if ($o->headerData !== $headerData[$src]) { //標題與目前設定不同
                continue;
            } else {
                $header = $headerData[$src];
            }
            $columns = $orderSampleService->GetOrderSampleColumns($header, $src);
            $original_data = collect(json_decode($o->original_data));
            $notImportSum += $original_data[0][$columns['order_total']]; //商品總價
        }
        //銷貨單
        $salesDB = DB::table('POSEIN')
            ->select('PPCOD', 'PPNAM', 'PTOT1', 'PDAT1')
            ->where('PTOT1', '!=', 0)
            ->whereBetween('PDAT1', [$sTWDate, $eTWDate])
            ->whereNotIn('PPCOD', ['x001', 'x002'])
            ->get();
        $salesSum = $salesDB->sum('PTOT1'); //出貨營業額(未稅)小計
        //未轉銷貨單
        $unsalesSum = DB::table('ORDET AS O')
            ->select('OTOT1')
            ->leftJoin('POSEIN AS P', 'P.PCOD2', 'O.OCOD1')
            ->whereBetween('O.ODATE', [$sTWDate, $eTWDate])
            ->where('O.OCANC', '!=', '訂單取消')
            ->whereNull('P.PCOD2')
            ->orderBy('O.OCOD1')
            ->sum('OTOT1');
        //銷貨退回
        $returnDB = DB::table('POSEOU')
            ->select('PPCOD', 'PPNAM', 'PTOT1', 'PDAT1')
            ->where('PTOT1', '!=', 0)
            ->whereBetween('PDAT1', [$sTWDate, $eTWDate])
            ->get();
        $returnSum = $returnDB->sum('PTOT1');
        //銷貨明細，排除其他、包材類
        $salesDetailsDB = DB::table('HISTIN')
            ->select(DB::raw('(HYCOD+HYNAM) AS item_type'), DB::raw('SUM(HTOTA) AS sales'))
            ->whereBetween('HDAT1', [$sTWDate, $eTWDate])
            ->whereNotIn('HYCOD', ['98', '99'])
            ->groupBy('HYCOD', 'HYNAM')
            ->orderBy('item_type');
        $salesDetails = $salesDetailsDB->pluck('sales', 'item_type')->toArray();
        //pos機收入
        $posSalesDB = DB::table('PosMainTemp')->whereBetween('create_date', [$start_date . ' 00:00:00', $end_date . ' 23:59:59']);
        //pos機退貨
        $posReturnDB = DB::table('PosReturnMainTemp AS R')
            ->select('R.RCOD1', DB::raw("CASE WHEN InvoiceNo = '' THEN ReturnTotalMoney ELSE ROUND(ReturnTotalMoney/1.05,0) END AS ReturnPayMoney"))
            ->whereExists(function ($q) use ($start_date, $end_date) {
                $q
                    ->select(DB::raw(1))
                    ->from('PosMainTemp AS M')
                    ->whereColumn('M.MainID', 'R.MainID')
                    ->whereBetween('M.create_date', [$start_date . ' 00:00:00', $end_date . ' 23:59:59']);
            });
        //pos機退貨總計
        $posReturnSum = $posReturnDB->get()->sum('ReturnPayMoney');
        //pos機總額
        $posSales = round($posSalesDB->sum('SaleTotalMoney') / 1.05);
        //pos機收入明細
        $posDetailsDB = DB::table('PosDetailsTemp AS D')
            ->select(DB::raw('(I.ITCOD+I.ITNAM) AS item_type'), DB::raw('SUM(D.SaleMoneyTotal) AS sales'))
            ->leftJoin('ITEM AS I', 'I.ICODE', 'D.ICODE')
            ->whereBetween('CREATE_Date', [$start_date . ' 00:00:00', $end_date . ' 23:59:59'])
            ->whereIn('I.ITCOD', ['10', '11', '12', '13', '14', '20'])
            ->groupBy('I.ITCOD', 'I.ITNAM')
            ->orderBy('item_type')
            ->get();
        $posDetails = $posDetailsDB->pluck('sales', 'item_type')->toArray();
        //總產品
        $totalDetails = [];
        foreach (array_keys($salesDetails + $posDetails) as $currency) {
            $totalDetails[$currency] = (isset($salesDetails[$currency]) ? $salesDetails[$currency] : 0) + (isset($posDetails[$currency]) ? $posDetails[$currency] : 0);
        }
        //平台營業額小計
        $salesWebSum = [];
        foreach ($salesDB->whereIn('PPCOD', ['S001', 'S002', 'S006', 'S003', 's004', 'S005', 'C001', 'R001'])->groupBy('PPCOD') as $key => $group) {
            switch ($key) {
                case 'S001':
                    $name = "蝦皮1店";
                    break;
                case 'S003':
                    $name = "蝦皮2店";
                    break;
                case 's004':
                    $name = "蝦皮3店";
                    break;
                case 'S005':
                    $name = "蝦皮";
                    break;
                case 'S002':
                    $name = "官網Shopline";
                    break;
                case 'S006':
                    $name = "官網BV SHOP";
                    break;
                case 'R001':
                    $name = "露天";
                    break;
                case 'C001':
                    $name = "面交";
                    break;
            }
            //銷貨-退貨
            $salesWebSum[$name] = $group->sum('PTOT1') - ($returnDB->where('PPCOD', $key)->sum('PTOT1'));
        }
        //面交加上POS資料
        if (key_exists("面交", $salesWebSum)) {
            $salesWebSum["面交"] += ($posSales - $posReturnSum);
        } else {
            $salesWebSum["面交"] = ($posSales - $posReturnSum);
        }
        asort($salesWebSum);
        //每日營業額(總計)
        $salesEachDay = [];
        $eachDays = $this->GetEachDay($start_date, $end_date);
        foreach ($eachDays as $day) {
            $twDay =  $this->GetTWDateStr(new DateTime($day));
            $ss = (clone $salesDB)->where('PDAT1', $twDay)->sum('PTOT1'); //出貨營業額(未稅)小計
            $ps = (clone $posSalesDB)->whereBetween('create_date', [$day . ' 00:00:00', $day . ' 23:59:59'])->sum('SaleTotalMoney') -
                (clone $posReturnDB)->whereBetween('create_date', [$day . ' 00:00:00', $day . ' 23:59:59'])->get()->sum('ReturnPayMoney');
            $rs = (clone $returnDB)->where('PDAT1', $twDay)->sum('PTOT1'); //銷貨退回小計
            $salesEachDay[$day] = $ss + $ps - $rs;
        }

        return [
            'salesDetails' => $salesDetails,
            'posDetails' => $posDetails,
            'totalDetails' => $totalDetails,
            'returnSum' => $returnSum, //銷貨退回小計
            'posReturnSum' => $posReturnSum, //pos退回
            'salesSum' => $salesSum, //出貨營業額小計
            'salesWebSum' => $salesWebSum,
            'unsalesSum' => $unsalesSum,
            'posSales' => $posSales, //POS機營業額(面交)小計
            'start_date' => $start_date,
            'end_date' => $end_date,
            'salesEachDay' => $salesEachDay,
            'notImportSum' => $notImportSum, //籃子未匯入小計
            'total' => $salesSum + $posSales - $returnSum - $posReturnSum, //營業額總計
            'total_include_unsale' => $notImportSum + $unsalesSum + $salesSum + $posSales - $returnSum - $posReturnSum //營業額總計(含未出)
        ];
    }
    /**
     * 取得庫存周轉率
     *
     * @param Request $request
     * @return \Illuminate\Support\Collection
     */
    public function GetSalesPerformance(Request $request)
    {
        set_time_limit(120);
        $now = new DateTime();
        $eDate = $request->get('end_date', Carbon::today()->modify('-1 day')->format('Y-m-d'));
        $sDate = $request->get('start_date', Carbon::today()->modify('-31 day')->format('Y-m-d'));
        $eDate2 = Carbon::today()->modify('-1 day')->format('Y-m-d');
        $sDate2 = (new DateTime($eDate2))->modify('-7 day')->format('Y-m-d');
        $eDate3 = Carbon::today()->modify('-1 day')->format('Y-m-d');
        $sDate3 = (new DateTime($eDate2))->modify('-14 day')->format('Y-m-d');
        $sTWDate = $this->GetTWDateStr(new DateTime($sDate));
        $eTWDate = $this->GetTWDateStr(new DateTime($eDate));
        $sTWDate2 = $this->GetTWDateStr(new DateTime($sDate2));
        $eTWDate2 = $this->GetTWDateStr(new DateTime($eDate2));
        $sTWDate3 = $this->GetTWDateStr(new DateTime($sDate3));
        $eTWDate3 = $this->GetTWDateStr(new DateTime($eDate3));

        //庫存
        $stockDB = DB::connection('mysql')
            ->table('log_item')
            ->select('ICODE', 'INUMB', DB::raw('DATE(created_at) AS DT'))
            ->whereIn(DB::raw('DATE(created_at)'), [$sDate, $eDate, $sDate2, $eDate2, $sDate3, $eDate3])
            ->distinct('ICODE', 'DT');
        if ($request->get('marketing_item', false)) {
            $marketing_item = ItemConfig::select('id')->whereNotNull('rating_id')->pluck('id')->toArray();
            $stockDB->whereIn('ICODE', $marketing_item);
        }
        $stockDB = $stockDB->get();
        //銷售
        $salesDB = DB::table("ITEM AS I")
            ->select('I.ICODE', 'I.INAME', 'I.ISOUR', 'I.INUMB')
            ->addSelect(DB::raw("(SELECT SUM(HINUM) FROM HISTIN WHERE HICOD=I.ICODE AND HDAT1 BETWEEN '{$sTWDate}' AND '{$eTWDate}') AS HINUM"))
            ->addSelect(DB::raw("(SELECT SUM(Quantity) FROM PosDetailsTemp WHERE ICODE=I.ICODE AND CREATE_Date BETWEEN '{$sDate} 00:00:00' AND '{$eDate} 23:59:59') AS QTY"))
            ->addSelect(DB::raw("(SELECT SUM(HINUM) FROM HISTIN WHERE HICOD=I.ICODE AND HDAT1 BETWEEN '{$sTWDate2}' AND '{$eTWDate2}') AS HINUM2"))
            ->addSelect(DB::raw("(SELECT SUM(Quantity) FROM PosDetailsTemp WHERE ICODE=I.ICODE AND CREATE_Date BETWEEN '{$sDate2} 00:00:00' AND '{$eDate2} 23:59:59') AS QTY2"))
            ->addSelect(DB::raw("(SELECT SUM(HINUM) FROM HISTIN WHERE HICOD=I.ICODE AND HDAT1 BETWEEN '{$sTWDate3}' AND '{$eTWDate3}') AS HINUM3"))
            ->addSelect(DB::raw("(SELECT SUM(Quantity) FROM PosDetailsTemp WHERE ICODE=I.ICODE AND CREATE_Date BETWEEN '{$sDate3} 00:00:00' AND '{$eDate3} 23:59:59') AS QTY3"))
            ->addSelect(DB::raw("(SELECT TOP(1) 1 FROM SISE WHERE SICOD=I.ICODE AND 
            (SDAT1 BETWEEN '{$sTWDate}' AND '{$eTWDate}' OR SDAT1 BETWEEN '{$sTWDate2}' AND '{$eTWDate2}' OR SDAT1 BETWEEN '{$sTWDate3}' AND '{$eTWDate3}' )) AS BUY"))
            ->whereNotIn('I.ITCOD', ['98', '99'])
            ->where('I.ISOUR', '>', 0)
            ->get();
        $rating_config = InventoryTurnoverRating::all();
        $result = [];
        foreach ($stockDB->groupBy('ICODE') as $item_no => $stock_log) {
            //平均庫存=(期初+期末)/2
            $stock = $stock_log->whereIn('DT', [$sDate, $eDate])->sum('INUMB') / 2;
            $stock2 = $stock_log->whereIn('DT', [$sDate2, $eDate2])->sum('INUMB') / 2;
            $stock3 = $stock_log->whereIn('DT', [$sDate3, $eDate3])->sum('INUMB') / 2;
            $d = $salesDB->firstWhere('ICODE', $item_no);
            if (empty($d)) {
                continue;
            }
            $now_stock = $d->INUMB;
            $item_name = $d->INAME;
            $cost = $d->ISOUR;
            //庫存金額
            $now_cost_total = $cost * $now_stock;
            $cost_total = $cost *  $stock;
            $cost_total2 = $cost *  $stock2;
            $cost_total3 = $cost *  $stock3;
            //銷售量計算
            $sales = $d->HINUM + $d->QTY;
            $sales2 = $d->HINUM2 + $d->QTY2;
            $sales3 = $d->HINUM3 + $d->QTY3;
            //庫存周轉率
            if ($stock != 0) {
                $performance = $sales * $cost / ($stock * $cost);
                $cfg = $rating_config->where('minimum', '<=', $performance)->where('maximum', '>=', $performance)->first();
                $sys_rating_code = $cfg->id ?? '';
                $sys_rating = $cfg->rating ?? '';
                $performance  = number_format($performance, 4) . '/次';
            } else {
                $performance = '';
                $sys_rating_code = '';
                $sys_rating = '';
            }
            if ($stock2 != 0) {
                $performance2 = number_format($sales2 * $cost / ($stock2 * $cost), 4) . '/次';
            } else {
                $performance2 = '';
            }
            if ($stock3 != 0) {
                $performance3 = number_format($sales3 * $cost / ($stock3 * $cost), 4) . '/次';
            } else {
                $performance3 = '';
            }

            $result[$item_no] = [
                'buy' => $d->BUY,
                'marketing' => [],
                'sys_rating' => $sys_rating ?? '',
                'sys_rating_code' => $sys_rating_code,
                'rating' => '',
                'rating_code' => '',
                'item_name' => $item_name,
                'cost' => $cost,
                'now_stock' => $now_stock,
                'now_cost_total' => $now_cost_total,
                'stock' =>  $stock,
                'stock2' =>  $stock2,
                'stock3' =>  $stock3,
                'cost_total' => $cost_total,
                'cost_total2' => $cost_total2,
                'cost_total3' => $cost_total3,
                'sales' => $sales,
                'sales2' => $sales2,
                'sales3' => $sales3,
                'performance' => $performance ?? '',
                'performance2' => $performance2 ?? '',
                'performance3' => $performance3 ?? '',
            ];
        }
        //行銷
        $marketing_config = $this->configService->GetSysConfig('marketing');
        $item_marketing = ItemMarketingD::with('Main')->whereHas('Main', function ($query) use ($now) {
            $query->where('s_dt', '<=', $now)->where('e_dt', '>=', $now);
        })->get();
        foreach ($item_marketing->groupBy('item_no') as $item_no => $marketing) {
            if (key_exists($item_no, $result)) {
                foreach ($marketing as $m) {
                    $m_id = $m->Main->marketing;
                    $result[$item_no]['marketing'][$m_id] = $marketing_config->firstWhere('value1', $m_id)->value2 ?? '';
                }
            }
        }
        //行銷評等
        $item_config = ItemConfig::with('Rating')->select('id', 'rating_id')->whereNotNull('rating_id')->get();
        foreach ($item_config as $cfg) {
            $item_no = $cfg->id;
            if (key_exists($item_no, $result)) {
                $result[$item_no]['rating_code'] = $cfg->Rating->id;
                $result[$item_no]['rating'] = $cfg->Rating->rating;
            }
        }
        //系統評等『區間庫存金額』總計
        $rating_total = [];
        $rating_now_total = [];
        $all_cost_total = collect($result)->sum('cost_total');
        $all_now_cost_total = collect($result)->sum('now_cost_total');
        foreach (collect($result)->sortBy('sys_rating')->groupBy('sys_rating') as $sys_rating => $group) {
            $sum =  $group->sum('cost_total');
            $rating_total[$sys_rating] = [
                'sum' => $sum,
                'proportion' => round($sum / $all_cost_total * 100),
            ];
            $now_sum = $group->sum('now_cost_total');
            $rating_now_total[$sys_rating] = [
                'sum' => $now_sum,
                'proportion' => round($now_sum / $all_now_cost_total * 100),
            ];
        }

        return ['data' => $result, 'rating_total' => $rating_total, 'rating_now_total' => $rating_now_total, 'marketing' => $marketing_config, 'rating' => $rating_config];
    }

    //平台銷售
    public function GetCustItem(Request $request)
    {
        $items = $this->GetItemAllList();
        $s_date = new DateTime($request->get('start_date'));
        $e_date = new DateTime($request->get('end_date'));
        $tw_s_date = $this->GetTWDateStr($s_date);
        $tw_e_date = $this->GetTWDateStr($e_date);
        $s_year = $s_date->format('Y') - 1911;
        $e_year = $e_date->format('Y') - 1911;
        $x = [];
        $y = [];
        $select_items =  json_decode($request->get('items', '[]'));
        if (empty($select_items)) {
            return ['selected_items' => [], 'items' => $items, 'chart' => [
                'x' => $x,
                'y' => $y,
            ]];
        }
        //取得X軸，月份
        if ($s_year != $e_year) { //跨年份
            for ($i = $s_date->format('m'); $i <= $e_date->format('m') + 12; $i++) {
                if ($i > 12) {
                    $m = str_pad($i - 12, 2, '0', STR_PAD_LEFT);
                } else {
                    $m = str_pad($i, 2, '0', STR_PAD_LEFT);
                }
                $x[] = $m;
            }
        } else {
            for ($i = $s_date->format('m'); $i <= $e_date->format('m'); $i++) {
                $x[] = str_pad($i, 2, '0', STR_PAD_LEFT);
            }
        }
        $s_date = $s_date->format('Y-m-d H:i:s');
        $e_date = $e_date->format('Y-m-d H:i:s');
        $last_s_date = (new Carbon($s_date))->addYears(-1)->format('Y-m-d H:i:s');
        $last_e_date = (new Carbon($e_date))->addYears(-1)->format('Y-m-d H:i:s');
        $last_tw_s_date = $this->GetTWDateStr(new DateTime($last_s_date));
        $last_tw_e_date = $this->GetTWDateStr(new DateTime($last_e_date));
        $last_s_year = substr($last_tw_s_date, 0, 3);
        $last_e_year = substr($last_tw_e_date, 0, 3);
        $db = $this->baseRepository->GetCustItem($s_date, $e_date, $tw_s_date, $tw_e_date, $select_items);
        $last_db = $this->baseRepository->GetCustItem($last_s_date, $last_e_date, $last_tw_s_date, $last_tw_e_date, $select_items);
        //各平台加總
        $cust_sale = [];

        //今年資料
        foreach ($db->groupBy('PPNAM') as $cust_name => $group) {
            $qty = $group->sum('qty');
            $sales_total = $group->sum('sales_total');
            $cust_sale[$cust_name] = [
                'cust_name' => $cust_name,
                'qty' => $qty,
                'sales_total' => $sales_total
            ];
            $each_month = ['當年'];
            $across_year = false; //跨年
            foreach ($x as $month) {
                $dt = $across_year ? ($e_year . '.' . $month) : ($s_year . '.' . $month);
                $each_month[] = round($group->where('DT', $dt)->sum('sales_total'));
                if ($month == 12) {
                    $across_year = true;
                }
            }
            $y[$cust_name]['當年'] = $each_month;
        }
        //去年資料
        foreach (array_keys($y) as $cust_name) {
            $group = $last_db->where('PPNAM', $cust_name);
            $sales_total = $group->sum('sales_total');
            $cust_sale[$cust_name]['last_sales_total'] = $sales_total;
            $each_month = ['去年'];
            $across_year = false; //跨年
            foreach ($x as $month) {
                $dt = $across_year ? ($last_e_year . '.' . $month) : ($last_s_year . '.' . $month);
                $each_month[] = round($group->where('DT', $dt)->sum('sales_total'));
                if ($month == 12) {
                    $across_year = true;
                }
            }
            $y[$cust_name]['去年'] = $each_month;
        }

        return [
            'selected_items' => $select_items,
            'items' => $items,
            'cust_sale' => $cust_sale,
            'chart' => [
                'x' => $x,
                'y' => $y,
            ]
        ];
    }

    //商品大類年度銷售
    public function GetItemKindSales(Request $request)
    {
        $typeList = $this->GetItemType();
        $x = [];
        $y = [];
        $chart = [];
        $s_year = $request->get('s_year', Carbon::today()->format('Y'));
        $e_year = $request->get('e_year', Carbon::today()->format('Y'));
        $selected_types = json_decode($request->get('types', '[]'));
        if (empty($selected_types)) {
            $chart = [
                'x' => $x,
                'y' => $y,
                'data_name' => '',
            ];
            return ['chart' => $chart, 'typeList' => $typeList, 'selected_types' => []];
        }
        $tw_s_date = $this->GetTWDateStr(new DateTime($s_year . '-01-01'));
        $tw_e_date = $this->GetTWDateStr(new DateTime($e_year . '-12-31'));
        //未稅銷售
        $pos_sub = DB::table('PosDetailsTemp')
            ->select(DB::raw('YEAR(CREATE_Date)-1911 AS y'), DB::raw('Quantity*SaleNoTax AS HTOTA'))
            ->whereExists(function ($q) use ($selected_types) {
                $q
                    ->select(DB::raw(1))
                    ->from('ITEM')
                    ->whereRaw('ITEM.ICODE = PosDetailsTemp.ICODE')
                    ->whereIn('ITCOD', $selected_types);
            })
            ->whereBetween('CREATE_Date', ["{$s_year}-01-01 00:00:00", "{$e_year}-12-31 23:59:59"]);
        $sub = DB::table('HISTIN')
            ->select(DB::raw('SUBSTRING(HDAT1,1,3) AS y'), 'HTOTA')
            ->whereIn('HYCOD', $selected_types)
            ->whereBetween('HDAT1', [$tw_s_date, $tw_e_date])
            ->unionAll($pos_sub);
        $sales = DB::table(DB::raw("({$this->baseRepository->GetSqlWithParameter($sub)}) AS T"))
            ->select('T.y', DB::raw('SUM(T.HTOTA) AS HTOTA'))
            ->groupBy('T.y')
            ->orderBy('T.y')
            ->get();
        for ($year = $s_year; $year <= $e_year; $year++) {
            $roc_year = $year - 1911;
            $sale = round($sales->where('y', $roc_year)->first()->HTOTA ?? 0);
            $x[] = $year;
            $y[] = $sale;
        }
        $data_name = '';
        foreach ($selected_types as $t) {
            if (!empty($data_name)) {
                $data_name .= ',';
            }
            $data_name .= trim($typeList[1][$t]);
        }
        $chart = [
            'x' => $x,
            'y' => $y,
            'data_name' => $data_name
        ];

        return ['chart' => $chart, 'typeList' => $typeList, 'selected_types' => $selected_types];
    }

    //商品小類年度銷售
    public function GetItemKind3Sales(Request $request)
    {
        $typeList = $this->GetItemType();
        $x = [];
        $y = [];
        $chart = [];
        $s_year = $request->get('s_year', Carbon::today()->format('Y'));
        $e_year = $request->get('e_year', Carbon::today()->format('Y'));
        $selected_types = json_decode($request->get('types', '[]'));
        if (empty($selected_types)) {
            $chart = [
                'x' => $x,
                'y' => $y,
                'data_name' => '',
            ];
            return ['chart' => $chart, 'typeList' => $typeList, 'selected_types' => []];
        }
        $tw_s_date = $this->GetTWDateStr(new DateTime($s_year . '-01-01'));
        $tw_e_date = $this->GetTWDateStr(new DateTime($e_year . '-12-31'));
        //未稅銷售
        $pos_sub = DB::table('PosDetailsTemp')
            ->select(DB::raw('YEAR(CREATE_Date)-1911 AS y'), DB::raw('Quantity*SaleNoTax AS HTOTA'))
            ->whereExists(function ($q) use ($selected_types) {
                $q
                    ->select(DB::raw(1))
                    ->from('ITEM1')
                    ->whereRaw('ITEM1.ICODE1 = PosDetailsTemp.ICODE')
                    ->whereIn('ITCO3', $selected_types);
            })
            ->whereBetween('CREATE_Date', ["{$s_year}-01-01 00:00:00", "{$e_year}-12-31 23:59:59"]);
        $sub = DB::table('HISTIN')
            ->select(DB::raw('SUBSTRING(HDAT1,1,3) AS y'), 'HTOTA')
            ->join('ITEM1 AS I1', 'I1.ICODE1', 'HISTIN.HICOD')
            ->whereIn('I1.ITCO3', $selected_types)
            ->whereBetween('HDAT1', [$tw_s_date, $tw_e_date])
            ->unionAll($pos_sub);
        $sales = DB::table(DB::raw("({$this->baseRepository->GetSqlWithParameter($sub)}) AS T"))
            ->select('T.y', DB::raw('SUM(T.HTOTA) AS HTOTA'))
            ->groupBy('T.y')
            ->orderBy('T.y')
            ->get();
        for ($year = $s_year; $year <= $e_year; $year++) {
            $roc_year = $year - 1911;
            $sale = round($sales->where('y', $roc_year)->first()->HTOTA ?? 0);
            $x[] = $year;
            $y[] = $sale;
        }
        $data_name = '';
        foreach ($selected_types as $t) {
            if (!empty($data_name)) {
                $data_name .= ',';
            }
            $data_name .= trim($typeList[3][$t]);
        }
        $chart = [
            'x' => $x,
            'y' => $y,
            'data_name' => $data_name
        ];

        return ['chart' => $chart, 'typeList' => $typeList, 'selected_types' => $selected_types];
    }

    //商品類別銷售
    public function GetItemSales(Request $request)
    {
        $typeList = $this->GetItemType();
        $items = $this->GetItemsName();
        $x = [];
        $y = [];
        $chart = [];
        $s_date = $request->get('s_date', Carbon::today()->format('Y-m-d'));
        $e_date = $request->get('e_date', Carbon::today()->format('Y-m-d'));
        $x_type = $request->get('x_type', 1);
        $y_type = $request->get('y_type', 1);
        $y_unit = $request->get('y_unit', 1);
        $item_merge = $request->get('item_merge', 0);
        $selected_types = json_decode($request->get('types', '[]'));
        $units = collect(json_decode($request->get('units', '[]')))->pluck('val', 'kind')->toArray() ?? [];
        if (empty($selected_types)) {
            $chart = [
                'x' => $x,
                'y' => $y,
                'data_name' => '',
            ];
            return ['chart' => $chart, 'typeList' => $typeList, 'selected_types' => []];
        }
        $tw_s_date = $this->GetTWDateStr(new DateTime($s_date));
        $tw_e_date = $this->GetTWDateStr(new DateTime($e_date));
        //未稅銷售
        $pos_sub = DB::table('PosDetailsTemp')
            ->select(DB::raw('SUBSTRING(CONVERT(varchar,CREATE_Date,120),0,11) AS d'), DB::raw('Quantity*SaleNoTax AS HTOTA'), 'Quantity AS qty')
            ->whereBetween('CREATE_Date', ["{$s_date} 00:00:00", "{$e_date} 23:59:59"]);
        $sub = DB::table('HISTIN')
            ->select(DB::raw('DATEFROMPARTS(SUBSTRING(HDAT1,1,3)+1911,SUBSTRING(HDAT1,5,2),SUBSTRING(HDAT1,8,2)) AS d'), 'HTOTA', 'HINUM AS qty')
            ->whereBetween('HDAT1', [$tw_s_date, $tw_e_date]);
        switch ($y_type) {
            case 1: //大類
                $pos_sub->join('ITEM AS I', function ($q) use ($selected_types) {
                    $q->on('I.ICODE', 'PosDetailsTemp.ICODE')->whereIn('ITCOD', $selected_types);
                })->addSelect('I.ITCOD AS kind');
                $sub->join('ITEM AS I', function ($q) use ($selected_types) {
                    $q->on('I.ICODE', 'HISTIN.HICOD')->whereIn('ITCOD', $selected_types);
                })->addSelect('I.ITCOD AS kind');
                break;
            case 3: //小類
                $pos_sub->join('ITEM1 AS I1', function ($q) use ($selected_types) {
                    $q->on('I1.ICODE1', 'PosDetailsTemp.ICODE')->whereIn('ITCO3', $selected_types);
                })->addSelect('I1.ITCO3 AS kind');
                $sub->join('ITEM1 AS I1', function ($q) use ($selected_types) {
                    $q->on('I1.ICODE1', 'HISTIN.HICOD')->whereIn('ITCO3', $selected_types);
                })->addSelect('I1.ITCO3 AS kind');
                break;
            case 4: //料號
                $pos_sub->whereIn('PosDetailsTemp.ICODE', $selected_types)
                    ->addSelect('PosDetailsTemp.ICODE AS kind');
                $sub->whereIn('HISTIN.HICOD', $selected_types)
                    ->addSelect('HISTIN.HICOD AS kind');
                break;
        }
        $sub->unionAll($pos_sub);
        $sales = DB::table(DB::raw("({$this->baseRepository->GetSqlWithParameter($sub)}) AS T"))
            ->select('T.d', 'T.kind', DB::raw('SUM(T.HTOTA) AS HTOTA'), DB::raw('SUM(T.qty) AS qty'))
            ->groupBy('T.d', 'T.kind')
            ->orderBy('T.d')
            ->get();
        $title = '';
        $axis_x = '';
        $axis_y = '';
        //序列名稱
        $data_name = '';
        foreach ($selected_types as $t) {
            if (!empty($data_name)) {
                $data_name .= ',';
            }
            if ($y_type == 4) {
                $data_name .= trim($items[$t]);
            } else {
                $data_name .= trim($typeList[$y_type][$t]);
            }
        }
        $col = '';
        switch ($y_unit) {
            case 1:
                $col = 'HTOTA';
                break;
            case 2:
                $col = 'qty';
                break;
        }
        //單位換算
        if ($y_type == 3 && count($units) > 0) { //小類、且有單位換算設定
            $sales->whereIn('kind', array_keys($units))->map(function ($item, $idx) use ($units) {
                if (key_exists($item->kind, $units)) {
                    $item->qty = round($item->qty / $units[$item->kind], 2);
                }
                return $item;
            });
        }
        switch ($x_type) {
            case 1: //年
                $s_year = substr($s_date, 0, 4);
                $e_year = substr($e_date, 0, 4);
                if (empty($item_merge)) {
                    foreach ($selected_types as $kind) {
                        $x_is_empty = count($x) == 0;
                        if ($y_type == 4) {
                            $tmp[] = trim($items[$kind]);
                        } else {
                            $tmp[] = trim($typeList[$y_type][$kind]);
                        }
                        for ($year = $s_year; $year <= $e_year; $year++) {
                            $val = round($sales->where('kind', $kind)->whereBetween('d', [$year . '-01-01', $year . '-12-31'])->sum($col) ?? 0);
                            if ($x_is_empty) {
                                $x[] = $year;
                            }
                            $tmp[] = $val;
                        }
                        $y[] = $tmp;
                        unset($tmp);
                    }
                } else {
                    $tmp[] = $data_name;
                    for ($year = $s_year; $year <= $e_year; $year++) {
                        $val = round($sales->whereBetween('d', [$year . '-01-01', $year . '-12-31'])->sum($col) ?? 0);
                        $x[] = $year;
                        $tmp[] = $val;
                    }
                    $y[] = $tmp;
                }
                $title = '每年總';
                $axis_x = '年度';
                break;
            case 2: //月
                $each_month = $this->GetEachMonth($s_date, $e_date);
                if (empty($item_merge)) {
                    foreach ($selected_types as $kind) {
                        $x_is_empty = count($x) == 0;
                        if ($y_type == 4) {
                            $tmp[] = trim($items[$kind]);
                        } else {
                            $tmp[] = trim($typeList[$y_type][$kind]);
                        }
                        foreach ($each_month as $m) {
                            $val = round($sales->where('kind', $kind)->whereBetween('d', [$m . '-01', $m . '-31'])->sum($col) ?? 0);
                            if ($x_is_empty) {
                                $x[] = $m;
                            }
                            $tmp[] = $val;
                        }
                        $y[] = $tmp;
                        unset($tmp);
                    }
                } else {
                    $tmp[] = $data_name;
                    foreach ($each_month as $m) {
                        $val = round($sales->whereBetween('d', [$m . '-01', $m . '-31'])->sum($col) ?? 0);
                        $x[] = $m;
                        $tmp[] = $val;
                    }
                    $y[] = $tmp;
                }
                $title = '每月總';
                $axis_x = '月份';
                break;
        }
        switch ($y_unit) {
            case 1:
                $title .= '營業額';
                $axis_y = '營業額(未稅)';
                break;
            case 2:
                $title .= '銷售數';
                $axis_y = '銷售數';
                break;
        }
        $chart = [
            'x' => $x,
            'y' => $y,
            'data_name' => $data_name,
            'title' => $title,
            'axis_x' => $axis_x,
            'axis_y' => $axis_y,
        ];
        return ['chart' => $chart, 'typeList' => $typeList, 'y_type' => $y_type, 'selected_types' => $selected_types, 'units' => $units, 'items' => $items];
    }

    //人工地板銷貨統計
    public function GetFloor(Request $request)
    {
        $date = new DateTime($request->get('date'));
        $tw_date = $this->GetTWDateStr($date);
        $db = Histin::select('HCOD1', 'HICOD', 'HINAM', 'HINUM')
            ->withWhereHas('Posein', function ($q) {
                $q->select('PCOD1', 'PBAK1', 'PBAK2', 'PBAK3', 'InsideNote')
                    ->where('TransportCode', '!=', 'COM')
                    ->where('PPCOD', '!=', 'C001');
            })
            ->withWhereHas('ITEM1', function ($q) {
                $q->select('ICODE1', 'ITCO3')->whereIn('ITCO3', ['014', '046']);
            })
            ->where('HYCOD', '11')
            ->where('HDAT1', $tw_date)
            ->get();
        $order_count = $db->groupBy('HCOD1')->count();
        $statistics = [
            '棧板' => ['SPC' => 0, 'SPC(V3)' => 0, 'SPC(厚寬)' => 0, 'LVT' => 0],
            '非棧板' => ['SPC' => 0, 'SPC(V3)' => 0, 'SPC(厚寬)' => 0, 'LVT' => 0],
            '總計' => ['SPC' => 0, 'SPC(V3)' => 0, 'SPC(厚寬)' => 0, 'LVT' => 0],
        ];
        $result = [];
        foreach ($db->groupBy('HICOD') as $item_no => $group) {
            $first_data = $group->first();
            $item_name = $first_data->item_name;
            $kind3 = $first_data->ITEM1->ITCO3;
            $total = $group->sum('HINUM');
            foreach ($group as $g) {
                $qty = $g->qty;
                $is_pallet = strpos($g->Posein->PBAK1, '棧板') !== false
                    || strpos($g->Posein->PBAK2, '棧板') !== false
                    || strpos($g->Posein->PBAK3, '棧板') !== false
                    || strpos($g->Posein->InsideNote, '棧板') !== false;
                $key = $is_pallet ? '棧板' : '非棧板';
                if ($kind3 == '014') {
                    if (strpos($item_name, 'V3') !== false) {
                        $statistics[$key]['SPC(V3)'] += $qty;
                        $statistics['總計']['SPC(V3)'] += $qty;
                    } else if (strpos($item_name, '極厚寬版') !== false) {
                        $statistics[$key]['SPC(厚寬)'] += $qty;
                        $statistics['總計']['SPC(厚寬)'] += $qty;
                    } else {
                        $statistics[$key]['SPC'] += $qty;
                        $statistics['總計']['SPC'] += $qty;
                    }
                } elseif ($kind3 == '046') {
                    $statistics[$key]['LVT'] += $qty;
                    $statistics['總計']['LVT'] += $qty;
                }
            }
            $result[] = [
                'item_no' => $item_no,
                'item_name' => $item_name,
                'total' => $total,
                'orders' => $group->pluck('HINUM', 'HCOD1')->toArray()
            ];
        }
        array_multisort(array_column($result, 'item_name'), $result);
        $statistics['棧板']['LVT'] = round($statistics['棧板']['LVT'] / 10);
        $statistics['非棧板']['LVT'] = round($statistics['非棧板']['LVT'] / 10);
        $statistics['總計']['LVT'] = round($statistics['總計']['LVT'] / 10);

        return ['data' => $result, 'statistics' => $statistics, 'order_count' => $order_count];
    }

    //蝦幣折抵查詢
    public function GetShopeeDiscount(Request $request)
    {
        $s_date = new DateTime($request->get('start_date'));
        $e_date = new DateTime($request->get('end_date'));
        $tw_s_date = $this->GetTWDateStr($s_date);
        $tw_e_date = $this->GetTWDateStr($e_date);
        $result = OrderDetail::select('OCOD1', DB::raw("ROUND(OTOTA,0) AS 'OTOTA'"))
            ->withWhereHas('Main', function ($q) {
                $q->select('OCOD1', 'OCOD4', 'OCNAM')
                    ->where('OCANC', '!=', '訂單取消');
            })
            ->where('OICOD', 'XX126A')
            ->whereIn('OCCOD', ['S001', 'S003', 'S004'])
            ->whereBetween('ODATE', [$tw_s_date, $tw_e_date])
            ->orderBy('OCOD1')
            ->get();

        return ['data' => $result];
    }
}
