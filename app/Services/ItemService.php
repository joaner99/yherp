<?php

namespace App\Services;

use DateTime;
use stdClass;
use Exception;
use Carbon\Carbon;
use App\Models\TMS\Item;
use App\Models\TMS\Histin;
use App\Models\TMS\Histou;
use App\Models\TMS\Visein;
use App\Models\TMS\Viseou;
use Illuminate\Http\Request;
use App\Models\yherp\ItemImg;
use App\Services\BaseService;
use App\Models\ItemPlaceModel;
use App\Models\TMS\OrderDetail;
use App\Models\yherp\SysConfig;
use App\Models\yherp\ItemConfig;
use App\Models\yherp\ItemPlaceD;
use App\Models\yherp\SysConfigM;
use App\Models\TMS\PosDetailsTemp;
use App\Models\yherp\ItemConvertD;
use Illuminate\Support\Facades\DB;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\Storage;

class ItemService extends BaseService
{
    //圖片路徑
    public const ItemsPath = 'img/items';

    //料號轉換設定檔
    public function GetItemConvert()
    {
        $today = Carbon::today()->format('Y-m-d');
        $db = ItemConvertD::whereHas('Main', function ($q) use ($today) {
            $q->where('enabled', true)->where('s_date', '<=', $today)->where('e_date', '>=', $today);
        })
            ->get();
        $result = [];
        foreach ($db->groupBy('src_item_no') as $src_item_no => $group) {
            if (count($group) != 1) { //重複設定
                continue;
            }
            $result[$src_item_no] = $group->first()->item_no;
        }
        return $result;
    }

    //取得商品建議儲位
    public function GetPlace(Request $request)
    {
        $stock = 'A';
        $shelf = $request->get('shelf', 20);
        $layer = $request->get('layer', 4);
        $new_items = $this->GetItemNew($this->GetTWDateStr(Carbon::today()->addMonths(-3))); //3個月內的新品
        $config = $this->configService->GetSysConfig('item_place_ban')->where('value1', '!=', '');
        $ban_items = $config->where('value2', '=', '')->pluck('value1')->toArray();
        $ban_items = array_merge($ban_items, $new_items->pluck('ICODE')->toArray());
        $items = $this->GetItemRank($ban_items);
        $place = $this->GetItemPlace($stock, $shelf, $layer);
        $cfg_id = SysConfigM::where('_key', 'item_place_ban')->first()->id;
        //將一般商品放入儲位
        foreach ($items as $key => $item) {
            $cfg = $config->where('value1', $item->ICODE)->first();
            $ban_l = -1;
            if (!empty($cfg) && !empty($cfg->value2)) { //有黑名單
                $ban_l = $cfg->value2;
            }
            foreach ([2, 3, 4, 1] as $l) {
                if ($ban_l != -1 && $ban_l >= $l) {
                    continue;
                }
                $p = $place->filter(function ($place) use ($l, $item) {
                    return $place->layer == $l && $place->CanAdd($item);
                })->sortBy(function ($value, $key) {
                    return count($value->items);
                })->first();
                if (!empty($p)) {
                    $item->shelf = $p->shelf;
                    $item->layer = $p->layer;
                    $item->place = $p->GetPlace();
                    $p->Add($item);
                    continue 2;
                }
            }
        }
        //將新品放入儲位
        $new_item_place = new ItemPlaceModel($stock, $shelf + 1, 0);
        $new_item_place->remark = '新品架';
        foreach ($new_items as $item) {
            $item->place = $new_item_place->GetPlace();
            $new_item_place->Add($item);
        }
        $place->push($new_item_place);
        $items = $items->concat($new_items);
        $place->sortBy('shelf')->sortBy('layer'); //依照架層排序

        return ['items' => $items, 'place' => $place, 'cfg_id' => $cfg_id];
    }

    /**
     * 取得產品銷售量
     *
     * @param Request $request
     * @return \Illuminate\Support\Collection
     */
    public function GetSales(Request $request)
    {
        $item_all = $this->GetItemAllList();
        $id = $request->get('id');
        if (empty($id)) {
            return ['data' => [], 'item_all' => $item_all];
        }
        $sDate = $request->get('start_date', (new DateTime())->modify('-1 year')->format('Y-m-d'));
        $eDate = $request->get('end_date', (new DateTime())->format('Y-m-d'));
        $twSDate = $this->GetTWDateStr(new DateTime($sDate));
        $twEDate = $this->GetTWDateStr(new DateTime($eDate));
        $gpm = $this->GetItemSalesGPM($sDate, $eDate, $twSDate, $twEDate, $id);
        $orders = $this->GetItemOrders($twSDate, $twEDate, $id);
        $sales = $this->GetItemSales($twSDate, $twEDate, $id);
        $pos_sales = $this->GetItemPosSales($sDate, $eDate, $id);
        $sales_return = $this->GetItemSalesReturn($id);
        $pos_sales_return = $this->GetItemPosSalesReturn($id);
        $purchase = $this->GetItemPurchase($id);
        $purchase_return = $this->GetItemPurchaseReturn($id);

        return  [
            'gpm' => $gpm,
            'orders' => $orders,
            'sales' => $sales,
            'pos_sales' => $pos_sales,
            'sales_return' => $sales_return,
            'pos_sales_return' => $pos_sales_return,
            'purchase' => $purchase,
            'purchase_return' => $purchase_return,
            'item_all' => $item_all
        ];
    }

    //商品參數同步
    public function ConfigSync()
    {
        $exists = ItemConfig::select('id')->pluck('id')->toArray();
        $tms_items_db = Item::select('ICODE', 'INAME')
            ->whereHas('ITEM1', function ($q) {
                $q->where('ISTOP', 0);
            })
            ->whereHas('ITEM3', function ($q) {
                $q->where('IPSTOP', 0);
            })
            ->where('ITCOD', '!=', '98') //排除折扣
            ->whereNotIn('ICODE', ['XXXYY001'])
            ->get();
        $tms_exists = $tms_items_db->pluck('ICODE')->toArray();
        //刪除暫停出貨、廠商停產
        foreach ($exists as $item_no) {
            if (!in_array($item_no, $tms_exists)) {
                ItemConfig::find($item_no)->delete();
            }
        }
        //新增商品
        foreach ($tms_items_db as $item) {
            $item_no = $item->item_no;
            $item_name = $item->item_name;
            //更新產品名稱
            if (in_array($item_no, $exists)) {
                ItemConfig::find($item_no)->update(['name' => $item_name]);
                continue;
            }
            ItemConfig::create([
                'id' => $item_no,
                'name' => $item_name,
            ]);
        }
    }

    /**
     * 取得產品履歷
     *
     * @param Request $request
     * @return array
     */
    public function GetLog(Request $request)
    {
        $item_all = $this->GetItemAllList();
        if (empty($request) || empty($request->id)) {
            return ['item_all' => $item_all];
        }
        $id = $request->id;
        //取得指定日期並轉換格式
        $sDate = $request->start_date;
        if (empty($sDate)) {
            $tmp = new DateTime("now");
            $tmp->modify("-1 month");
        } else {
            $tmp = new DateTime($sDate);
        }
        $sDate = $tmp->format('Y-m-d');
        $tw_sDate = $this->GetTWDateStr($tmp);
        $eDate = $request->end_date;
        if (empty($eDate)) {
            $tmp = new DateTime("now");
        } else {
            $tmp = new DateTime($eDate);
        }
        $eDate = $tmp->format('Y-m-d');
        $tw_eDate = $this->GetTWDateStr($tmp);

        $result = array();
        $subtotal = 0;
        $db = $this->baseRepository->GetItemLog($tw_sDate, $tw_eDate, $sDate, $eDate, $id);
        foreach ($db as $value) {
            $num = $value->Num;
            $type = $value->Type;
            //設定類別CSS、小計計算
            switch ($type) {
                case '進貨':
                    $typeClass = 'Purchase';
                    $subtotal += $num;
                    break;
                case '進貨退回':
                    $typeClass = 'RPurchase';
                    $subtotal -= $num;
                    break;
                case '銷貨':
                    $typeClass = 'Sales';
                    $subtotal -= $num;
                    break;
                case '銷貨退回':
                    $typeClass = 'RSales';
                    $subtotal += $num;
                    break;
                case 'POS銷貨':
                    $typeClass = 'POSSales';
                    $subtotal -= $num;
                    break;
                case 'POS銷貨退回':
                    $typeClass = 'RPOSSales';
                    $subtotal += $num;
                    break;
                case '調撥':
                    $typeClass = 'Transfer';
                    break;
                case '盤盈':
                    $typeClass = 'InventoryProfit';
                    $subtotal += $num;
                    break;
                case '盤虧':
                    $typeClass = 'InventoryLoss';
                    $subtotal -= $num;
                    break;
                default:
                    $typeClass = '';
                    break;
            }
            //設定數量CSS
            if ($type == '進貨退回' || $type == '銷貨' || $type == 'POS銷貨' || $type == '盤虧') {
                $numClass = 'Reduce';
            } else {
                $numClass = '';
            }

            $result[] = [
                'Date' => $value->Date,
                'Time' => $this->GetTimeIgnoreMS($value->Time),
                'Id' => (string)$value->Id,
                'Type' => $type,
                'TypeClass' => $typeClass,
                'Cust' => $value->Cust,
                'Stock' => $value->Stock,
                'StockClass' => $value->StockCode,
                'Num' => $num,
                'NumClass' => $numClass,
                'UserName' => $value->UserName
            ];
        }
        //庫存
        $main_stock = ItemPlaceD::whereHas('Main', function ($q) {
            $q->where('s_id', 'B001');
        })->where('item_no', $id)->sum('qty');
        if ($main_stock > 0) {
            $stock = [[
                'name' => '外掛主倉',
                'qty' => $main_stock,
            ]];
        } else {
            $stock = [];
        }
        $s_db = $this->baseRepository->GetStock([$id]);
        foreach ($s_db as $s) {
            $qty = $s->SNUMB;
            if ($qty == 0) {
                continue;
            }
            $name = $s->STNAM;
            if ($name == '主倉儲') {
                $name = 'TMS主倉';
            }
            $stock[] = [
                'name' => $name,
                'qty' => $qty,
            ];
        }
        $item_name = Item::select('INAME')->where('ICODE', $id)->first()->INAME ?? '';
        //分組統計數量
        $group_data = [];
        foreach (collect($result)->groupBy(function ($q) {
            return $q['Type'] . $q['Id'];
        }) as $g) {
            $first_row = $g->first();
            $first_row['Num'] = $g->sum('Num');
            $group_data[] = $first_row;
        }
        return ['datas' => $group_data, 'stock' => $stock, 'subtotal' => $subtotal, 'item_name' => $item_name, 'item_all' => $item_all];
    }

    /**
     * 取得產品庫存紀錄
     *
     * @param Request $request
     * @return array
     */
    public function GetStockHistory(Request $request)
    {
        $result = array();
        $result['chart'] = ['x' => ['x'], 'y' => [''], 'title' => ''];
        $result['ItemList'] = $this->GetItemAllList();
        if (empty($request) || empty($request->id)) {
            return $result;
        }

        $id = $request->id;
        //取得指定日期並轉換格式
        $min = new DateTime('2021-06-29');
        $sDate = $request->start_date;
        if (empty($sDate)) {
            $tmp = new DateTime("now");
            $tmp->modify("-1 month");
        } else {
            $tmp = new DateTime($sDate);
        }
        if ($tmp < $min) {
            $tmp = clone $min;
        }
        $sDate = $this->GetLogDateStr($tmp) . ' 00:00:00';
        $eDate = $request->end_date;
        if (empty($eDate)) {
            $tmp = new DateTime("now");
        } else {
            $tmp = new DateTime($eDate);
        }
        if ($tmp < $min) {
            $tmp = clone $min;
        }
        $eDate = $this->GetLogDateStr($tmp) . ' 23:59:59';

        $xAxis = $this->GetEachDay($sDate, $eDate);
        $yAxis[] = $id;

        $db = $this->baseRepository->GetItemHistory($sDate, $eDate, $id);
        foreach ($xAxis as $value) {
            $target = $db->filter(function ($item) use ($value) {
                return $value == (new DateTime($item->created_at))->format('Y-m-d');
            })->first();
            $y = empty($target) ? 0 : intval($target->INUMB);
            array_push($yAxis, $y);
        }
        array_unshift($xAxis, 'x');
        $result['chart'] = [
            'x' => $xAxis,
            'y' => $yAxis,
            'title' => substr($sDate, 0, 10) . '～' . substr($eDate, 0, 10)
        ];

        return $result;
    }

    /**
     * 取得商品詳細資訊
     *
     * @param Request $request
     * @return array
     */
    public function GetDetail(Request $request)
    {
        $id = $request->get('id1');
        if (empty($id)) {
            $id = $request->get('id2');
            if (empty($id)) {
                return [];
            }
        }
        $itemNo = $this->baseRepository->ConvertToItemNo(trim($id));
        if (empty($itemNo)) { //找不到對應產品代號
            return ['id' => $id];
        }
        $result['id'] = $itemNo;
        //商品詳細資料
        $detailDB = $this->GetItemAllDetail([$itemNo])->first();
        if (!empty($detailDB)) {
            $result['pause'] = $detailDB->ITEM1->ISTOP ?? '';
            $result['stop'] = $detailDB->ITEM3->IPSTOP ?? '';
            $result['name'] = $detailDB->item_name;
            $result['kind'] = $detailDB->kind_name;
            $result['kind2'] = $detailDB->kind2_name;
            $result['kind3'] = $detailDB->kind3_name;
            $result['origin'] = $detailDB->ITEM4->T11_11 ?? '';
            $result['ena13'] = $detailDB->ITEM1->ENA13 ?? '';
            $result['co128'] = $detailDB->ITEM1->CO128 ?? '';
            $result['id2'] = $detailDB->ITEM1->ICOD2 ?? '';
            $result['weight'] = $detailDB->ITEM1->IWEIGHT ?? ''; //單品重量(公斤)
            $result['depth'] = $detailDB->ITEM4->PDepth ?? ''; //單品體積長
            $result['width'] = $detailDB->ITEM4->PWidth ?? ''; //單品體積寬
            $result['height'] = $detailDB->ITEM4->PHeight ?? ''; //單品體積高
            $result['box_weight'] = $detailDB->ITEM4->Box_Weight ?? ''; //包裝重量(公斤)
            $result['box_depth'] = $detailDB->ITEM4->Box_Depth ?? ''; //包裝體積長
            $result['box_width'] = $detailDB->ITEM4->Box_Width ?? ''; //包裝體積寬
            $result['box_height'] = $detailDB->ITEM4->Box_Height ?? ''; //包裝體積高
            $result['box_cuft'] = $detailDB->ITEM4->Box_CUFT ?? ''; //包裝材積CUFT
            $result['box_cbm'] = $detailDB->ITEM4->Box_CBM ?? ''; //包裝材積CBM
        }
        //商品圖片
        $pathList = ItemImg::where('icode', $itemNo)->pluck('path')->toArray();
        foreach ($pathList as $key => $path) {
            if (!file_exists(public_path('storage/' . $path))) {
                $pathList[$key] = false;
            }
        }
        $result['img'] = $pathList;
        //商品儲位
        $itempDB = $this->baseRepository->GetItemp([$itemNo]);
        $storage = '';
        if (!empty($itempDB) && count($itempDB) > 0) {
            $storage = join('、',  array_values($itempDB)[0]);
        }
        $result['storage'] = $storage;
        //商品庫存
        $stockDB = $this->GetStock([$itemNo]);
        if (count($stockDB) > 0) {
            foreach (current($stockDB) as $stock => $amount) {
                if ($stock == 'total') {
                    $stock = '總庫存';
                }
                $result['stock'][] = ['name' => $stock, 'amount' => number_format($amount)];
            }
        } else {
            $result['stock'] = [];
        }

        return $result;
    }

    /**
     * 更新商品照片索引
     *
     * @param Request $request
     * @return mix
     */
    public function UpdateItemImage(Request $request)
    {
        $icode = $request->get('icode');
        if (empty($icode)) {
            return 'icode is empty';
        }
        //更新索引
        ItemImg::where('icode', $icode)->delete();
        $img_path = json_decode($request->get('img_path'));
        $msg = $this->CreateItemImage($icode, $img_path);
        //刪除檔案
        $files = Storage::disk('public')->allFiles(self::ItemsPath . '/' . $icode);
        $deleteFiles = [];
        foreach ($files as $file) {
            if (!in_array($file, $img_path)) {
                $deleteFiles[] = $file;
            }
        }
        Storage::disk('public')->delete($deleteFiles);

        return $msg;
    }

    /**
     * 建立指定商品照片索引
     *
     * @param string $icode
     * @param mixed $img_path
     * @return string
     */
    public function CreateItemImage($icode, $img_path)
    {
        try {
            foreach ($img_path as $path) {
                ItemImg::create([
                    'icode' => $icode,
                    'path' => $path
                ]);
            }
            return '';
        } catch (Exception $ex) {
            return $ex->getMessage();
        }
    }

    /**
     * 取得目前圖片索引
     *
     * @param string $id 料號
     * @return int
     */
    public function GetImageFileIndex($id)
    {
        $files = Storage::disk('public')->allFiles(self::ItemsPath . '/' . $id);
        if (empty($files)) {
            return 1;
        }
        $filesIndex = [];
        foreach ($files as $path) {
            $pathList = explode('/', $path);
            if (count($pathList) != 4) { //img,items,123456,123456.jpg
                continue;
            }
            $name = $pathList[3];
            $sIdx = strpos($name, $id);
            $eIdx = strrpos($name, '.');
            $offset = $sIdx + strlen($id) + 1;
            $length = $eIdx - $offset;
            $filesIndex[] = substr($name, $offset, $length);
        }
        if (empty($filesIndex)) {
            return 1;
        } else {
            return collect($filesIndex)->max() + 1;
        }
    }

    /**
     * 取得一年內有銷售，卻無商品圖片的商品
     *
     * @return \Illuminate\Support\Collection
     */
    public function GetItemNoImage()
    {
        $today = new DateTime("now");
        $eDate = $this->GetTWDateStr($today);
        $today->modify("-1 year");
        $sDate = $this->GetTWDateStr($today);
        $sales = Histin::select('HICOD', 'HINAM', DB::raw('SUM(HINUM) AS HINUM'))
            ->whereBetween('HDAT1', [$sDate, $eDate])
            ->whereNotIn('HYCOD', BaseRepository::excludedSalesType)
            ->groupBy('HICOD', 'HINAM')
            ->orderByDesc('HINUM')
            ->get();
        $itemsImg = ItemImg::select('icode')->whereIn('icode', $sales->pluck('HICOD')->toArray())->distinct()->pluck('icode')->toArray();
        foreach ($sales as $key => $sale) {
            if (in_array($sale->HICOD, $itemsImg) || $sale->HINUM <= 0) {
                $sales->forget($key);
            }
        }

        return $sales;
    }

    /**
     * 刪除資料，並將已上傳圖片檔建立資料索引
     *
     * @return void
     */
    public function ReBuildImgIndex()
    {
        $imageExtensions = ['jpg', 'jpeg', 'gif', 'png', 'bmp'];
        try {
            $insertData = [];
            $files = Storage::disk('public')->allFiles(self::ItemsPath);
            foreach ($files as $path) {
                $pathList = explode('/', $path);
                if (count($pathList) != 4) { //img,items,123456,123456.jpg
                    continue;
                }
                $code = $pathList[2];
                $extension = $this->GetFilePathExtension($pathList[3]);
                if (empty($extension) || !in_array($extension, $imageExtensions)) {
                    continue;
                } else {
                    $insertData[] = ['icode' => $code, 'path' => $path];
                }
            }
            if (count($insertData) > 0) {
                ItemImg::truncate();
                ItemImg::insert($insertData);
            }
        } catch (Exception $ex) {
            echo $ex->getMessage();
        }
    }

    /**
     * 取得儲位資料
     *
     * @return \Illuminate\Support\Collection
     */
    private function GetItemPlace($stock, $shelf, $layer)
    {
        $place = []; //儲位
        for ($s = 1; $s <= $shelf; $s++) {
            for ($l = 1; $l <= $layer; $l++) {
                $place[] = new ItemPlaceModel($stock, $s, $l);
            }
        }

        return collect($place);
    }

    /**
     * 取得商品銷售排名
     *
     * @return \Illuminate\Support\Collection
     */
    private function GetItemRank($new_items = null)
    {
        $s_date = Carbon::today()->addYears(-1)->format('Y-m-d');
        $e_date = Carbon::today()->format('Y-m-d');
        $s_tw_date = $this->GetTWDateStr(new DateTime($s_date));
        $e_tw_date = $this->GetTWDateStr(new DateTime($e_date));
        $web_sale = DB::table('HISTIN')->select('HICOD AS ICODE', 'HINUM AS qty')->whereBetween('HDAT1', [$s_tw_date, $e_tw_date])->where('HINUM', '>', 0);
        $pos_sale = DB::table('PosDetailsTemp')->select('ICODE', 'Quantity AS qty')->whereBetween('CREATE_Date', [$s_date, $e_date])->where('Quantity', '>', 0);
        $web_sale->unionAll($pos_sale);
        $sale = DB::table(DB::raw("({$this->baseRepository->GetSqlWithParameter($web_sale)}) AS T"))
            ->select('T.ICODE', DB::raw('SUM(T.qty) AS qty'))
            ->groupBy('T.ICODE');
        if (!empty($new_items) && count($new_items) > 0) {
            $sale->whereNotIn('ICODE', $new_items);
        }
        $items = DB::table(DB::raw("({$this->baseRepository->GetSqlWithParameter($sale)}) AS T"))
            ->select('T.ICODE', 'T.qty', 'I.INAME', 'I.ITCOD', 'I.ITNAM', 'I1.ITCO2', 'I1.ITNA2', 'I1.ITCO3', 'I1.ITNA3')
            ->addSelect(DB::raw("(SELECT SUM(SNUMB) FROM STOC WHERE SITEM=I.ICODE AND STCOD NOT IN ('D001', 'I001', 'B002', 'B001')) AS stock"))
            ->join('ITEM AS I', 'I.ICODE', 'T.ICODE')
            ->join('ITEM1 AS I1', 'I1.ICODE1', 'T.ICODE')
            ->whereNotIn('I.ITCOD', ['98', '99']) //排除其他、包材
            ->whereNotIn('I1.ITCO3', ['018', '014', '046', '020', '043']) //排除草皮、spc、免膠、收編條、椅子
            ->where('I.INAME', 'NOT LIKE', '%未包裝半成品%')
            ->where('I.INAME', 'NOT LIKE', '%AR11%')
            ->orderBy('T.qty', 'DESC')
            ->orderBy('T.ICODE')
            ->get();
        $item_no_list = $items->pluck('ICODE')->toArray();
        //主倉庫存數結合
        $stock = ItemPlaceD::select('item_no', 'qty')
            ->whereHas('Main', function ($q) {
                $q->where('s_id', 'B001');
            })
            ->whereIn('item_no', $item_no_list)
            ->get();
        $items->map(function ($item) use ($stock) {
            $qty = $stock->where('item_no', $item->ICODE)->sum('qty');
            if (!empty($qty)) {
                $item->stock += $qty;
            }
        });
        //篩選過去一年內無庫存商品
        $config = $this->configService->GetConfig('item_no_empty');
        if (empty($config) || new DateTime($config->date) != Carbon::today()) {
            $log_stock = DB::connection('mysql')
                ->table('log_item')
                ->select('ICODE')
                ->where('created_at', '>=', $s_date)
                ->where('INUMB', '>', 0)
                ->whereIn('ICODE', $item_no_list)
                ->distinct()
                ->pluck('ICODE')
                ->toArray();
            //建立產品有庫存的暫存參數
            $cfg = new stdClass();
            $cfg->date = Carbon::today()->format('Y-m-d');
            $cfg->items = $log_stock;
            SysConfig::updateOrCreate([
                'kind' => 'item_no_empty'
            ], [
                'config' => json_encode($cfg),
            ]);
        } else {
            $log_stock = $config->items;
        }
        $items = $items->filter(function ($value, $key) use ($log_stock) {
            return in_array($value->ICODE, $log_stock);
        })->values()->all();

        return collect($items);
    }

    /**
     * 取得指定日期內進貨的新品
     *
     * @param string $date
     * @return \Illuminate\Support\Collection
     */
    private function GetItemNew($date)
    {
        $sub = DB::table('VISEIN')
            ->select(DB::raw("ROW_NUMBER() OVER (PARTITION BY VICOD ORDER BY VDAT1) AS rownum"))
            ->addSelect('VICOD', 'VDAT1');
        $items = DB::table(DB::raw("({$this->baseRepository->GetSqlWithParameter($sub)}) AS T"))
            ->select('T.VICOD AS ICODE', 'I.INAME', 'I.ITCOD', 'I.ITNAM', 'I1.ITCO2', 'I1.ITNA2', 'I1.ITCO3', 'I1.ITNA3')
            ->addSelect(DB::raw("(SELECT SUM(SNUMB) FROM STOC WHERE SITEM=T.VICOD AND STCOD NOT IN ('D001', 'I001', 'B002','B001')) AS stock"))
            ->join('ITEM AS I', 'I.ICODE', 'T.VICOD')
            ->join('ITEM1 AS I1', 'I1.ICODE1', 'T.VICOD')
            ->whereNotIn('I.ITCOD', ['98', '99']) //排除其他、包材
            ->whereNotIn('I1.ITCO3', ['018', '014', '046', '047', '020']) //排除草皮、spc、免膠、拼接地板、收編條
            ->where('T.rownum', 1)
            ->where('VDAT1', '>=', $date)
            ->get();
        $item_no_list = $items->pluck('ICODE')->toArray();
        //主倉庫存數結合
        $stock = ItemPlaceD::select('item_no', 'qty')
            ->whereHas('Main', function ($q) {
                $q->where('s_id', 'B001');
            })
            ->whereIn('item_no', $item_no_list)
            ->get();
        $items->map(function ($item) use ($stock) {
            $qty = $stock->where('item_no', $item->ICODE)->sum('qty');
            if (!empty($qty)) {
                $item->stock += $qty;
            }
        });
        return $items;
    }

    /**
     * 取得銷貨單個毛利
     *
     * @param string $sDate 起日yyyy-mm-dd
     * @param string $eDate 訖日yyyy-mm-dd
     * @param string $twSDate 民國起日yyy.mm.dd
     * @param string $twEDate 民國訖日yyy.mm.dd
     * @param string $id 料號
     * @return \Illuminate\Support\Collection
     */
    private function GetItemSalesGPM($sDate, $eDate, $twSDate, $twEDate, $id)
    {
        //銷貨單的單個毛利計算
        $web = DB::table('HISTIN AS D')
            ->select('D.HICOD AS item_no', 'D.HINAM AS item_name', DB::raw("'WEB' AS src"), 'M.PCOD2 AS order_no', 'D.HCOD1 AS sales_no')
            ->addSelect(DB::raw("CASE WHEN D.HCONS='*' THEN D.HINU2 ELSE D.HINUM END AS qty"))
            ->addSelect(DB::raw("CASE WHEN D.HCONS='*' THEN D.HUNI2 ELSE D.HUNIT END AS sales"))
            ->addSelect(DB::raw("CASE WHEN D.HCONS='*' THEN D.HTOT2 ELSE D.HTOTA END AS sales_total"))
            ->addSelect(DB::raw("D.HSOUR/CASE WHEN D.HCONS='*' THEN D.HINU2 ELSE D.HINUM END AS cost"))
            ->join('POSEIN AS M', 'M.PCOD1', 'D.HCOD1')
            ->where('D.HICOD', $id)
            ->whereBetween('D.HDAT1', [$twSDate, $twEDate]);
        $pos = DB::table('PosDetailsTemp AS D')
            ->select('D.ICODE AS item_no', 'D.INAME AS item_name', DB::raw("'POS' AS src"), DB::raw("'' AS order_no"), 'M.RCOD1 AS sales_no')
            ->addSelect('D.Quantity AS qty', 'D.SaleMoney AS sales', 'D.SaleMoneyTotal AS sales_total', DB::raw("I.ISOUR AS cost"))
            ->join('PosMainTemp AS M', 'M.MainID', 'D.MainID')
            ->join('ITEM AS I', 'I.ICODE', 'D.ICODE')
            ->where('D.ICODE', $id)
            ->whereBetween('D.CREATE_Date', [$sDate . ' 00:00:00', $eDate . ' 23:59:59'])
            ->where('D.Quantity', '>', 0);
        $db = $web->unionAll($pos)->get();
        foreach ($db as $d) {
            $sales =  $d->sales;
            $cost = $d->cost;
            if ($sales > 0) {
                $d->gpm = ($sales - $cost) / $sales * 100;
            } else {
                $d->gpm = null;
            }
        }
        return $db;
    }

    /**
     * 取得訂單
     *
     * @param string $sDate 民國起日yyy.mm.dd
     * @param string $eDate 民國訖日yyy.mm.dd
     * @param string $id
     * @return \Illuminate\Support\Collection
     */
    private function GetItemOrders($sDate, $eDate, $id)
    {
        $db = OrderDetail::with('Main')
            ->select('OCOD1', 'OCNAM', 'ODATE', 'OINUM', 'OUNIT', 'OTOTA')
            ->where('OICOD', $id)
            ->whereBetween('ODATE', [$sDate, $eDate])
            ->orderBy('ODATE', 'DESC')
            ->get();

        return $db;
    }

    /**
     * 取得銷貨
     *
     * @param string $sDate 民國起日yyy.mm.dd
     * @param string $eDate 民國訖日yyy.mm.dd
     * @param string $id
     * @return \Illuminate\Support\Collection
     */
    private function GetItemSales($sDate, $eDate, $id)
    {
        $db = Histin::select('HCOD1', 'HPNAM', 'HDAT1', 'HINUM', 'HUNIT', 'HTOTA')
            ->where('HICOD', $id)
            ->whereBetween('HDAT1', [$sDate, $eDate])
            ->orderBy('HDAT1', 'DESC')
            ->get();

        return $db;
    }

    /**
     * 取得POS訂單
     *
     * @param string $sDate 起日yyyy-mm-dd
     * @param string $eDate 訖日yyyy-mm-dd
     * @param string $id
     * @return \Illuminate\Support\Collection
     */
    private function GetItemPosSales($sDate, $eDate, $id)
    {
        $db = PosDetailsTemp::with('Main')
            ->where('ICODE', $id)
            ->whereBetween('CREATE_Date', [$sDate, $eDate])
            ->orderBy('CREATE_Date', 'DESC')
            ->get();

        return $db;
    }

    /**
     * 取得銷貨退回
     *
     * @param string $id
     * @return \Illuminate\Support\Collection
     */
    private function GetItemSalesReturn($id)
    {
        $db = Histou::select('HCOD1', 'HPNAM', 'HDAT1', 'HINUM', 'HUNIT', 'HTOTA')
            ->where('HICOD', $id)
            ->orderBy('HDAT1', 'DESC')
            ->get();

        return $db;
    }

    /**
     * 取得POS銷貨退回
     *
     * @param string $id
     * @return \Illuminate\Support\Collection
     */
    private function GetItemPosSalesReturn($id)
    {
        $db = DB::table('PosReturnDetailsTemp AS R')
            ->select(DB::raw("(SELECT TOP(1) RT.RCOD3 FROM RETAIL AS RT WHERE RT.RCOD1=R.RCOD1 AND RCOD3 != '') AS RCOD3"), 'R.CREATE_Date', 'R.ReturnQuantity', 'R.ReturnMoney', 'R.ReturnMoneyTotal')
            ->where('R.ICODE', $id)
            ->orderBy('R.CREATE_Date', 'DESC')
            ->get();

        return $db;
    }

    /**
     * 取得進貨
     *
     * @param string $id
     * @return \Illuminate\Support\Collection
     */
    private function GetItemPurchase($id)
    {
        $db = Visein::select('VCOD1', 'VPNAM', 'VDAT1', 'VINUM', 'VUNIT', 'VTOTA')
            ->where('VICOD', $id)
            ->orderBy('VDAT1', 'DESC')
            ->get();

        return $db;
    }

    /**
     * 取得進貨退回
     *
     * @param string $id
     * @return \Illuminate\Support\Collection
     */
    private function GetItemPurchaseReturn($id)
    {
        $db = Viseou::select('VCOD1', 'VPNAM', 'VDAT1', 'VINUM', 'VUNIT', 'VTOTA')
            ->where('VICOD', $id)
            ->orderBy('VDAT1', 'DESC')
            ->get();

        return $db;
    }

    /**
     * 取得路徑下的檔案副檔名
     *
     * @param string $path
     * @return void
     */
    private function GetFilePathExtension(string $path)
    {
        if (empty($path)) {
            return null;
        } else {
            $tmp = explode('.', $path);
            if (count($tmp) >= 2) {
                return $tmp[1];
            } else {
                return null;
            }
        }
    }
}
