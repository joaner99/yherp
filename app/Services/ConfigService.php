<?php

namespace App\Services;

use DateTime;
use App\Models\yherp\SysConfigM;
use App\Repositories\BaseRepository;

class ConfigService
{
    private $baseRepository;

    public function __construct(BaseRepository $baseRepository)
    {
        $this->baseRepository = $baseRepository;
    }

    /**
     * 取得設定檔
     *
     * @param string $key 代碼
     * @return mixed
     */
    public function GetSysConfig(string $key)
    {
        if (empty($key)) {
            return null;
        }
        $db = SysConfigM::with('Details')->where('_key', $key)->first()->Details ?? null;
        return $db;
    }

    /**
     * 取得動態設定檔
     *
     * @param string $kind
     * @return mixed
     */
    public function GetConfig(string $kind)
    {
        if (empty($kind)) {
            return null;
        }
        $db = $this->baseRepository->GetConfig($kind);
        if ($db->count() == 0) {
            return null;
        } else {
            return json_decode($db->first()->config);
        }
    }

    /**
     * 取得廣告花費設定檔
     *
     * @param DateTime $date
     * @return \Illuminate\Support\Collection
     */
    public function GetAdCost(DateTime $date)
    {
        $db = $this->baseRepository->GetConfig('ad_cost');
        if ($db->count() == 0) {
            return collect();
        } else {
            $config = json_decode($db->first()->config);
            if (empty($config->salesTarget)) {
                $config->salesTarget = $config->salesTargetDefault;
            } else { //指定月份設定值，或預設值
                $tmp = array_filter($config->salesTarget, function ($filter) use ($date) {
                    return $filter->year == $date->format('Y') && $filter->month == $date->format('m');
                });
                if (empty($tmp)) {
                    $config->salesTarget = $config->salesTargetDefault;
                } else {
                    $config->salesTarget = $tmp[0]->value;
                }
            }
            return $config;
        }
    }
}
