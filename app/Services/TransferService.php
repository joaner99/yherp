<?php

namespace App\Services;

use App\Services\BaseService;
use App\Models\yherp\LogStockD;
use App\Models\yherp\ItemConfig;
use App\Models\yherp\ItemPlaceD;
use App\Models\yherp\LogOrderRequire;

class TransferService extends BaseService
{
    /**
     * 取得調撥作業清單
     *
     * @return \Illuminate\Support\Collection
     */
    public function GetTransfer($request)
    {
        $include_empty_stock = $request->get('include_empty_stock', 0);
        $date = $request->get('date');
        $config = ItemConfig::select('id', 'name', 'main_ss')
            ->where('main_ss', '>', 0)
            ->get();
        $order_require = LogOrderRequire::with('Config')
            ->with(['Order' => function ($q) {
                $q->select('OCOD1', 'OCOD4', 'TransportCode');
            }])
            ->with(['Order.PersonalTransport' => function ($q) {
                $q->select('original_no', 'ship_date');
            }])
            ->with(['Item' => function ($q) {
                $q->select('ICODE', 'ITNAM');
            }])
            ->with(['Item1' => function ($q) {
                $q->select('ICODE1', 'ITCO3');
            }])
            ->with(['LogStock' => function ($q) {
                $q->select('item_no', 'qty')
                    ->whereHas('Main', function ($q2) { //出庫作業
                        $q2->where('log_type', 2)->where('log_kind', 2);
                    })
                    ->doesnthave('TransferCheck'); //未清點
            }])
            ->with(['STOC' => function ($q) {
                $q->select('SITEM', 'SNUMB')->where('STCOD', 'A001');
            }])
            ->orderBy('updated_at')
            ->get();
        if (!empty($date)) { //根據日期篩選訂單
            foreach ($order_require as $key => $or) {
                $order = $or->Order;
                if (!empty($order)) {
                    $pt = $order->PersonalTransport;
                    if (!empty($pt)) {
                        $ship_date = $pt->ship_date;
                        //親送日期為空，或不同指地日期
                        if (empty($ship_date) || !empty($ship_date) && $ship_date != $date) {
                            $order_require->forget($key);
                        }
                    } else {
                        $order_require->forget($key);
                    }
                } else {
                    $order_require->forget($key);
                }
            }
        }
        $items = $order_require->pluck('item_no')->toArray();
        $stock_db = ItemPlaceD::whereIn('item_no', $items)
            ->whereHas('Main', function ($q) {
                $q->where('s_id', 'B001');
            })
            ->get();
        $unsafety_stock = (new StockService())->GetUnsafetyStock($items);
        $result = [
            '地板' => [],
            '非地板' => []
        ];
        foreach ($order_require->groupBy('item_no') as $item_no => $group) {
            //篩選訂單取消、刪除的需求
            $group = $group->filter(function ($item, $key) {
                return !empty($item->Order);
            });
            if (count($group) == 0) { //無任何訂單
                continue;
            }
            $orders = $group->map(function ($item, $key) {
                return [
                    'order_no' => $item->order_no,
                    'qty' => $item->qty,
                    'is_self' => $item->Order->trans == 'COM'
                ];
            });
            $stock = $stock_db->where('item_no', $item_no);
            $total_stock = $stock->sum('qty'); //主倉庫存
            $item = $group->first();
            $item_type1 = $item->Item->ITNAM ?? ''; //大類名稱
            $isfloor = in_array($item->Item1->ITCO3, ['014', '046', '020']); //SPC、LVT、收邊條
            $item_name = $item->item_name;
            $item_box = $item->Config->box_qty ?? 0;
            $total_require = $group->sum('qty'); //訂單需求
            $total_sale_stock = $item->STOC->sum('SNUMB'); //出貨倉庫存
            $need = $total_require - $total_sale_stock; //調撥量=訂單需求-出貨倉庫存
            $unchecked_stock = $item->LogStock->sum('qty') ?? 0; //已出庫未清點
            $safety_stock = $config->find($item_no)->main_ss ?? 0; //主倉安庫
            $safety_warn = count($unsafety_stock->where('item_no', $item_no)) > 0;
            if ($total_require <= $total_sale_stock || (!$include_empty_stock && empty($total_stock))) { //出貨倉滿足訂單需求 或 主倉無庫存，無須調撥
                continue;
            } else {
                $suggest = $this->GetTransferSuggest($need, $total_stock, $item_box);
                $item->stock = $stock;
                $item->item_box = $item_box;
                $item->total_require = $total_require;
                $item->total_sale_stock = $total_sale_stock;
                $item->total_stock = $total_stock;
                $item->need = $need;
                $item->suggest = $suggest;
                $item->unchecked_stock = $unchecked_stock;

                $result[$isfloor ? '地板' : '非地板'][] = [
                    'orders' => $orders,
                    'stock' => $stock,
                    'item_type1' => $item_type1,
                    'item_no' => $item_no,
                    'item_name' => $item_name,
                    'item_box' => $item_box,
                    'total_require' => $total_require,
                    'total_sale_stock' => $total_sale_stock,
                    'total_stock' => $total_stock,
                    'safety_stock' => $safety_stock,
                    'safety_warn' => $safety_warn,
                    'need' => $need,
                    'suggest' => $suggest,
                    'unchecked_stock' => $unchecked_stock,
                    'updated_at' => $group->max('updated_at')
                ];
            }
        }
        arsort($result);
        //統計
        $floor_total = [];
        foreach ($result['地板'] as $floor_item) {
            $item_no = $floor_item['item_no'];
            $item_name = $floor_item['item_name'];
            $self_qty = collect($floor_item['orders'])->where('is_self', true)->sum('qty');
            $unself_qty = collect($floor_item['orders'])->where('is_self', false)->sum('qty');
            $floor_total[] = [
                'item_no' => $item_no,
                'item_name' => $item_name,
                'self_qty' => $self_qty,
                'unself_qty' => $unself_qty
            ];
        }
        return ['datas' => $result, 'floor_total' => $floor_total];
    }

    /**
     * 取得調撥清點作業，已出庫未清點項目
     *
     * @return void
     */
    public function GetTransferCheck()
    {
        $stock_out_db = LogStockD::whereHas('Main', function ($q) {
            $q
                ->where('log_type', 2) //出庫作業
                ->where('log_kind', 2) //調撥作業
                ->where('created_at', '>=', '2023-10-03 10:30:00'); //系統上線後
        })
            ->doesnthave('TransferCheck')
            ->get()
            ->groupBy(function ($item) {
                return $item->created_at->format('Y-m-d');
            })
            ->sortBy('item_no');
        return $stock_out_db;
    }

    /**
     * 取得調撥建議量
     *
     * @param integer $need 調撥量
     * @param integer $stock 庫存量
     * @param integer $unit 最小單位
     * @return integer 建議量
     */
    private function GetTransferSuggest(int $need, int $stock, int $unit)
    {
        if ($need == 0 || $stock == 0) { //無調撥輛 或 無庫存量，不調撥
            return 0;
        }
        if ($need > $stock) { //庫存不足，有多少調多少
            return $stock;
        } else if ($unit <= 0) { //無最小單位，要多少調多少
            return $need;
        } else { //有庫存 且 有最小單位
            $suggest = ceil($need / $unit) * $unit;
            return $suggest > $stock ? $stock : $suggest; //庫存不足有多少調多少
        }
    }
}
