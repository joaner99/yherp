<?php

namespace App\Services;

use App\User;
use DateTime;
use Exception;
use Carbon\Carbon;
use App\Models\TMS\Order;
use App\Models\TMS\Posein;
use App\Models\yherp\Cars;
use Illuminate\Http\Request;
use App\Models\yherp\Freight;
use App\Models\yherp\Holiday;
use App\Services\BaseService;
use App\Models\TMS\OrderDetail;
use App\Models\yherp\ItemConfig;
use App\Models\yherp\LogPackage;
use App\Models\yherp\SysConfigM;
use App\Models\yherp\LogShipment;
use App\Models\yherp\OrderSampleM;
use App\Models\yherp\YcsTransport;
use Illuminate\Support\Facades\DB;
use App\Models\yherp\OrderConfirmD;
use App\Services\OrderSampleService;
use App\Models\yherp\PersonalTransport;
use App\Models\yherp\OrderShipmentStatusD;
use App\Models\yherp\OrderShipmentStatusM;
use App\Models\yherp\PersonalTransportSche;

class TransportService extends BaseService
{
    //地區判斷
    private const Area = [
        '北' => ['基隆', '臺北', '新北市', '桃園', '新竹'],
        '中' => ['苗栗', '臺中', '彰化', '南投'],
        '南' => ['雲林', '嘉義', '臺南', '高雄', '屏東'],
    ];

    private $orderSampleService;

    public function __construct()
    {
        parent::__construct();
        $this->orderSampleService = new OrderSampleService;
    }

    /**
     * 取得銷貨託運資料
     *
     * @param Request $request
     * @return array
     */
    public function GetTransport(Request $request)
    {
        $result = array();
        $s_date = $request->get('start_date') ?? (new DateTime())->format('Y-m-d');
        $e_date = $request->get('end_date') ?? (new DateTime())->format('Y-m-d');
        $tw_s_date = $this->GetTWDateStr(new DateTime($s_date));
        $tw_e_date = $this->GetTWDateStr(new DateTime($e_date));
        $only_tax = $request->get('only_tax', false);
        $delay_orders = OrderShipmentStatusM::whereBetween('estimated_date', [$s_date, $e_date])->pluck('sales_no')->toArray();
        $personal_trans_order = PersonalTransport::with('User')->whereBetween('ship_date', [$s_date, $e_date])->get();
        $disable_gpm = ItemConfig::select('id')->where('disable_gpm', 1)->pluck('id')->toArray();
        $db = $this->baseRepository->GetTransport($tw_s_date, $tw_e_date, $only_tax, $delay_orders, $personal_trans_order->pluck('original_no')->toArray(), $disable_gpm);
        $sales = $db->pluck('PCOD1')->toArray();
        $hct_remote_confirm_list = OrderConfirmD::whereHas('Main', function ($q) {
            $q->where('order_type', '11');
        })
            ->whereIn('order_no', $db->pluck('PJONO')->toArray())
            ->pluck('order_no')
            ->toArray();
        $order_date_db = OrderSampleM::select('order_no', 'order_date', 'receiver_address')->whereIn('order_no', $db->pluck('PJONO')->toArray())->get()->keyBy('order_no');
        $shipmentOrder = LogShipment::select('PCOD1')->whereIn('PCOD1', $sales)->distinct()->pluck('PCOD1')->toArray();
        $config = $this->configService->GetSysConfig('sche_gross_profit');
        $standard_max = $config->firstWhere('value1', 'standard_max')->value2 ?? 50; //績效標準
        $standard_min = $config->firstWhere('value1', 'standard_min')->value2 ?? 20; //銷售量區間
        $shipment_status_db = OrderShipmentStatusM::with('Details')->whereIn('sales_no', $sales)->get();
        $lvt_count = $this->baseRepository->GetLvtCount($sales);
        $holiday_config = Holiday::whereBetween('year', [Carbon::today()->year - 1, Carbon::today()->year + 1])->get();
        $now = new DateTime();
        $tomorrow_ship_date = new DateTime(Carbon::today()->addDays(1)->format('Y-m-d 18:00:00'));
        $ship_hour = $this->GetTotalHours($tomorrow_ship_date->diff($now)); //下次出貨所需小時
        $hct_remote_config = OrderSampleService::GetLogisticsAddrMark('2');
        //轉換格式
        foreach ($db as $value) {
            //訂單快過期狀態
            $original_no = $value->PJONO;
            if ($order_date_db->has($original_no)) {
                $addr = $order_date_db[$original_no]->receiver_address;
                $order_date = $order_date_db[$original_no]->order_date;
                $order_hours = ceil($this->GetOrderElapsedHour(new DateTime($order_date), $now, $holiday_config));
                $total_hours = $order_hours + $ship_hour;
                $order_expired = $this->GetOrderExpired($value->PPCOD, $total_hours);
            } else {
                $addr = '';
                $order_expired = '';
            }
            $trans = BaseService::ConvertTransCode($value->TransportCode);
            //取得買家備註
            $value->buyer_notes = $this->GetBuyerNotes($value->PPCOD, $value->PBAK1);
            //特定客代無須判斷毛利異常
            if ($value->ExcludeGPM || in_array($value->PPCOD, ['C010', 'x001', 'x002', 'F001'])) {
                $value->gpm_error = false;
            } else {
                $gpm = $value->OPERS;
                if (!empty($value->web_discount)) { //有平台自辦折扣，需重新計算毛利率，(利潤+平台自辦折扣)/(未稅總金額+平台自辦折扣)
                    $gpm = ($value->OEARN + abs($value->web_discount)) / ($value->OTOT1 + abs($value->web_discount)) * 100;
                }
                $value->gpm_error = $gpm < $standard_min || $gpm > $standard_max || $value->OTOT1 != $value->PTOT1; //毛利率異常
            }
            $lvt = $lvt_count->where('HCOD1', $value->PCOD1)->first()->qty ?? 0;
            //親送司機判斷
            if ($trans == 'COM') {
                $value->driver = $personal_trans_order->where('original_no', $original_no)->first()->User->name ?? '';
            } else {
                $value->driver = '';
            }
            //新竹託運判斷
            if ($trans == 'HCT' && !$this->IsPallet($value->PBAK1, $value->PBAK2, $value->PBAK3, $value->InsideNote) && !empty($value->Quantity) && !empty($value->Weight)) {
                $value->hct_tran_error = !$this->VerifyHCT($value->Quantity, $value->Weight) || !$this->VerifyHCT_LVT($value->Quantity, $lvt);
            } else {
                $value->hct_tran_error = false;
            }
            //新竹偏遠地區判斷
            if ($trans == 'HCT' && !in_array($value->PJONO, $hct_remote_confirm_list) && $this->IsHctRemote($hct_remote_config, $addr)) {
                $value->hct_remote = true;
            } else {
                $value->hct_remote = false;
            }
            //取得狀態
            $note_check = false;
            $gpm_check = false;
            $gpm_check_result = 0;
            $shipment_check = false;
            $estimated_date = '';
            $status = [];
            $status_data = $shipment_status_db->where('sales_no', $value->PCOD1)->first();
            if (!empty($status_data)) {
                $note_check = $status_data->note_check;
                $gpm_check = $status_data->gpm_check;
                $gpm_check_result = $status_data->gpm_check_result;
                $estimated_date = $status_data->estimated_date;
                $shipment_check = $status_data->shipment_check;
                foreach ($status_data->Details as $s) {
                    if (!$s->confirm) { //未覆核
                        $status[$s->_status] = ['name' => $s->status_name];
                    }
                }
            }
            $value->order_expired = $order_expired; //訂單快過期狀態
            $value->status = $status;
            $value->note_check = $note_check; //備註覆核
            $value->gpm_check = $gpm_check; //毛利覆核
            $value->gpm_check_result = $gpm_check_result; //毛利覆核原因
            $value->estimated_date = $estimated_date; //預計出貨日
            $value->shipment_check = $shipment_check; //出貨覆核
            $isDone = in_array($value->PCOD1, $shipmentOrder); //有無上車紀錄

            //如果沒有上車紀錄則為空
            $value->isDone = $isDone;
            //託運類別代碼
            $this->ConvertTransport($value->TransportCode, $value->TransportName, $value->ConsignTran);
            $tCode = $value->TransportCode;

            if (empty($result[$tCode])) {
                $result[$tCode]['All'] = 1;
                $result[$tCode]['Done'] = $isDone ? 1 : 0;
                $result[$tCode]['Undone'] = $isDone ? 0 : 1;
                $result[$tCode]['Data'][] = $value;
            } else {
                $result[$tCode]['All'] += 1;
                $result[$tCode]['Done'] += ($isDone ? 1 : 0);
                $result[$tCode]['Undone'] += ($isDone ? 0 : 1);
                array_push($result[$tCode]['Data'], $value);
            }
        }
        //各物流狀態統計
        foreach ($result as &$r) {
            $detail = collect($r['Data']);
            $r['buyer_notes'] = $detail->where('buyer_notes', '!=', '')->where('note_check', false)->count();
            $r['gpm_error'] = $detail->where('gpm_error', true)->where('gpm_check', false)->count();
            $r['hct_tran_error'] = $detail->where('hct_tran_error', true)->count();
            $r['hct_remote'] = $detail->where('hct_remote', true)->count();
            $status_count = [];
            foreach ($detail->where('shipment_check', false) as $d) {
                foreach ($d->status as $key => $value) {
                    if (key_exists($key, $status_count)) {
                        $status_count[$key]['count'] += 1;
                    } else {
                        $status_count[$key] = [
                            'status_data' => $value,
                            'count' => 1
                        ];
                    }
                }
            }
            $r['status'] = $status_count;
        }

        $result = collect($result)->sortBy('Undone')->reverse()->toArray();

        return  ['data' => $result, 'status_list' => OrderShipmentStatusD::status_list, 'min' => $standard_min, 'max' => $standard_max];
    }

    /**
     * 取得銷單建議物流
     *
     * @param Request $request
     * @return \Illuminate\Support\Collection
     */
    public function GetTransportSuggest(Request $request)
    {
        $sDate = $this->GetTWDateStr(new DateTime($request->start_date));
        $eDate = $this->GetTWDateStr(new DateTime($request->end_date));
        $db = $this->baseRepository->GetTransportSuggest($sDate, $eDate);
        $ycs_exclude_items = $this->configService->GetSysConfig('ycs_exclude_items')->pluck('value1')->toArray() ?? []; //音速物流無法配送的商品
        $logistics_addr_mark = OrderSampleService::GetLogisticsAddrMark('1');
        foreach ($db as $key => &$d) {
            $addr = $d->OriginalOrder->receiver_address ?? '';
            if (empty($addr)) {
                $db->forget($key);
                continue;
            }
            $d->addr = $addr;
            $items = $d->Histin->pluck('HICOD')->toArray();
            $ycs_exclude = count(array_intersect($ycs_exclude_items, $items)) > 0; //音速拒絕送
            $suggest = $this->GetAddrTransport($addr, $logistics_addr_mark);
            if (empty($suggest) || ($suggest == 'YCS' && $ycs_exclude)) {
                $db->forget($key);
            } else {
                $d->suggest = OrderSampleService::TransportType[$suggest];
            }
        }
        return $db;
    }

    //取得親送概況
    public function GetPersonalTransportOverview(Request $request)
    {
        $start_date = $request->get('start_date', Carbon::today()->format('Y-m-d'));
        $end_date = $request->get('end_date', Carbon::today()->addDays(7)->format('Y-m-d'));
        $floor_short_name_config = $this->configService->GetSysConfig('floor_short_name');
        $config = $this->configService->GetSysConfig('p_item_weight');
        $item_data = $this->GetItemDetail();
        $db = PersonalTransport::with(['User', 'Car', 'OrderSample', 'Order:OCOD1,OCOD4'])->whereBetween('ship_date', [$start_date, $end_date])->get();
        $sche_db = PersonalTransportSche::whereBetween('ship_date', [$start_date, $end_date])->get();
        $result = [];
        foreach ($db->sortBy('ship_date')->groupBy('ship_date') as $ship_date => $group) {
            $tmp = [];
            foreach ($group->groupBy('driver') as $driver_id => $group2) {
                $areas = [];
                $weight = 0;
                foreach ($group2 as $o) {
                    if (!empty($o->Order)) { //先以TMS訂單資料為主
                        //計算重量
                        foreach ($o->Order->OrderDetail as $item) {
                            $item_no = $item->item_no;
                            $qty = $item->qty;
                            if (empty($qty)) {
                                continue;
                            }
                            $item_kind1 = $item->Item->kind;
                            if (in_array($item_kind1, ['98', '99'])) {
                                continue;
                            }
                            $item_kind3 =  $item->Item1->kind3;
                            $short_name = $this->GetShortName($item_kind1, $item_kind3, $item->Item1->kind3_name, $item->item_name, $floor_short_name_config);
                            $item_kind3_name = $short_name['item_kind3_name'];
                            $weight += ($config->where('value1', $item_kind3_name)->first()->value2 ?? 0) * $qty;
                        }
                    } else if (!empty($o->OrderSample)) { //再以籃子訂單為參考
                        $order_sample = $o->OrderSample;
                        $columns = $this->orderSampleService->GetOrderSampleColumns(json_decode($order_sample->header), $order_sample->src);
                        $original_data = json_decode($order_sample->original_data);
                        //計算重量
                        foreach ($original_data as $item) {
                            $item_no = $item[$columns['item_no']];
                            $qty = $item[$columns['item_quantity']];
                            if (empty($qty)) {
                                continue;
                            }
                            if (key_exists($item_no, $item_data)) {
                                $item_kind1 = $item_data[$item_no]['kind1'];
                                if (in_array($item_kind1, ['98', '99'])) {
                                    continue;
                                }
                                $item_kind3 = $item_data[$item_no]['kind3'];
                                $short_name = $this->GetShortName($item_kind1, $item_kind3, $item_data[$item_no]['kind3_name'], $item_data[$item_no]['name'], $floor_short_name_config);
                                $item_kind3_name = $short_name['item_kind3_name'];
                            } else {
                                $item_kind3_name = '';
                            }
                            $weight += ($config->where('value1', $item_kind3_name)->first()->value2 ?? 0) * $qty;
                        }
                    } else { //TMS、籃子皆無資料
                        continue;
                    }
                    //判斷地區
                    if (!empty($o->additional_addr)) { //額外地址為主
                        $receiver_address = $o->additional_addr;
                    } else if (!empty($o->OrderSample)) {
                        $order_sample = $o->OrderSample;
                        $columns = $this->orderSampleService->GetOrderSampleColumns(json_decode($order_sample->header), $order_sample->src);
                        $original_data = json_decode($order_sample->original_data);
                        $receiver_address = OrderSampleService::GetAddr($order_sample->src, $original_data[0], $columns);
                    } else { //籃子、親送皆無資料
                        continue;
                    }
                    $areas[] = $this->GetArea($receiver_address);
                }
                $areas = array_unique($areas);
                sort($areas);
                $area = join(',', $areas);
                $order_count = count($group2);
                $upload_duration = $driver_id == 42 ? 0 : $order_count * 20 * 60;
                $driver_name = $group2->first()->User->name ?? '';
                $weight = floor($weight / 1000); //公克轉公斤
                $car = $group2->first()->Car->full_name ?? '';
                $weight_limit = $group2->first()->Car->weight_limit ?? 0;
                $weight_level = 0;
                if (!empty($weight_limit)) { //判斷超重狀況
                    if ($weight > $weight_limit) {
                        $weight_level = 1;
                    } else if ($weight > $weight_limit * 0.9) {
                        $weight_level = 2;
                    }
                }
                $sche = $sche_db->where('ship_date', $ship_date)->where('driver', $driver_id)->first();
                if ($driver_id == 42 || empty($sche)) {
                    $duration = 0;
                    $distance = '';
                    $updated_at = '';
                } else {
                    $duration = $sche->duration;
                    $distance = number_format(floor($sche->distance / 1000));
                    $updated_at = $sche->updated_at;
                }
                $tmp[$driver_id] = [
                    'driver' => $driver_name,
                    'car' => $car,
                    'area' => $area,
                    'order_count' => $order_count,
                    'weight' => number_format($weight),
                    'duration' => $this->SToHM($duration),
                    'upload_duration' => $this->SToHM($upload_duration),
                    'total_duration' => $this->SToHM($duration + $upload_duration),
                    'distance' => $distance,
                    'updated_at' => $updated_at,
                    'weight_level' => $weight_level,
                ];
            }
            $result[$ship_date] = $tmp;
        }
        $cars = Cars::all();
        return ['data' => $result, 'cars' => $cars];
    }

    //親送總表
    public function GetPersonalTransport(Request $request)
    {
        $search_type = $request->get('search_type', 1);
        $date = $request->get('date');
        $driver = $request->get('driver');
        if ($search_type == 1) { //未安排
            $caption = '未安排';
        } else {
            $caption = empty($date) ? '全部' : $date;
        }
        if (!empty($driver)) {
            $caption .= ' ' . User::find($driver)->name;
        }
        $config = $this->configService->GetSysConfig('p_item_weight');
        //取得TMS、籃子內的親送
        $result = $this->GetCOM($search_type, $request, $config);
        sort($result);
        $order_group = collect($result)->groupBy('original_no');
        $count = ['北' => 0, '中' => 0, '南' => 0, '其他' => 0];
        foreach ($order_group as $group) {
            $first = $group->first();
            $count[$first['area']]++;
        }
        //地板顏色統計
        $statistics = [];
        $statistics_columns = [];
        foreach (array_column($result, 'detail') as $items) {
            foreach ($items as $item) {
                $item_name = $item['item_name'];
                $item_kind3_name = $item['item_kind3_name'];
                $qty = intval($item['qty']);
                $statistics_columns[] = $item_kind3_name; //小類名稱
                if (key_exists($item_name, $statistics)) { //加總
                    if (key_exists($item_kind3_name, $statistics[$item_name])) {
                        $statistics[$item_name][$item_kind3_name] += $qty;
                    } else {
                        $statistics[$item_name][$item_kind3_name] = $qty;
                    }
                } else { //初始化
                    $statistics[$item_name] = [$item_kind3_name => $qty];
                }
            }
        }
        ksort($statistics);
        $statistics_columns = array_unique($statistics_columns);
        sort($statistics_columns);
        //統計
        $statistics_total = ['數量統計' => [], '重量統計(KG)' => ['統計' => 0]];
        foreach ($statistics_columns as $col) {
            $cfg = $config->where('value1', $col)->first()->value2 ?? 0;
            $sum = array_sum(array_column($statistics, $col));
            $weight = $sum * $cfg * 0.001;
            $statistics_total['數量統計'][$col] = $sum;
            $statistics_total['重量統計(KG)'][$col] = $weight;
            $statistics_total['重量統計(KG)']['統計'] += $weight;
        }
        $statistics_columns[] = '統計';
        $drivers = User::select('id', 'name')->role('親送司機')->get();
        $cars = Cars::all();
        return [
            'data' => $result,
            'count' => $count,
            'statistics' => $statistics,
            'statistics_columns' => $statistics_columns,
            'statistics_total' => $statistics_total,
            'drivers' => $drivers,
            'cars' => $cars,
            'config' => $config,
            'config_url' => route('sys_config.edit', SysConfigM::where('_key', 'p_item_weight')->first()->id),
            'floor_short_name_config_url' => route('sys_config.edit', SysConfigM::where('_key', 'floor_short_name')->first()->id),
            'caption' => $caption,
            'ship_date' => $date,
        ];
    }

    //取得親送天數明細
    public function GetPTShipDay(Request $request)
    {
        $month = $request->get('month');
        $driver = $request->get('driver');
        $data = [];
        if (!empty($month)) {
            $db = PersonalTransport::select('ship_date', 'driver')
                ->where('ship_date', 'LIKE', $month . '%');
            if (empty($driver)) {
                $db->whereNotNull('driver');
            } else {
                $db->where('driver', $driver);
            }
            $db = $db->get()
                ->sortBy('ship_date');
            foreach ($db->groupBy('ship_date') as $ship_date => $d) {
                $data[$ship_date] = array_unique($d->pluck('User.name')->toArray());
            }
        }
        $drivers = User::select('id', 'name')->role('親送司機')->get();
        return [
            'drivers' => $drivers,
            'data' => $data,
        ];
    }

    /**
     * 取得音速物流總表
     *
     * @return array
     */
    public function GetTransportYcs(Request $request)
    {
        if ($request->session()->has('date')) {
            $date = $request->session()->get('date');
            $request->session()->forget('date');
        } else {
            $date = $request->get('date', Carbon::today()->format('Y-m-d'));
        }
        $result = [];
        $db = $this->baseRepository->GetTransportYCS($date);
        foreach ($db as $d) {
            $status = 0;
            $status_name = '';
            if (empty($d->systemNumber)) {
                $status = 1;
                $status_name = 'API未上傳';
            } elseif (empty($d->tran_no)) {
                $status = 2;
                $status_name = 'TMS未上傳';
            } else {
                $status = 3;
                $status_name = '已完成';
            }
            $result[] = [
                'id' => $d->id,
                'status' => $status,
                'status_name' => $status_name,
                'sale_no' => $d->sale_no,
                'order_no' => $d->order_no,
                'qty' => $d->qty,
                'size' => $d->size,
                'weight' => $d->weight,
                'name' => $d->name,
                'receiver' => $d->receiver,
                'addr' => $d->addr,
                'tel' => $d->tel,
                'remark' => $d->remark,
                'shipment' => $d->shipment->count() > 0,
                'tran_no' => $d->tran_no ?? '',
                'systemNumber' => $d->systemNumber
            ];
        }
        $sDate = (new DateTime($date))->format('Y-m-1');
        $eDate = (new DateTime($date))->format('Y-m-t');
        $total = $this->baseRepository->GetTransportYCSTotal($sDate, $eDate);

        return ['data' => collect($result)->sortBy('status'), 'date' => $date, 'total' => $total ?? 0];
    }

    /**
     * 將指定銷單轉到音速物流
     *
     * @param $settle_date 日期
     * @param array $sales_no_list 銷單清單
     * @return string
     */
    public function ConvertYcs($settle_date, array $sales_no_list)
    {
        $msg = '';
        if (empty($sales_no_list) || count($sales_no_list) == 0) {
            $msg = '未選取任何要轉檔的銷單';
        } else {
            $db = Posein::select('PCOD1', 'PJONO', 'PTOTA', 'PBAK1', 'PBAK3')
                ->addSelect('RETOLL', 'RETOLL_Price')
                ->with('OriginalOrder:order_no,remark,receiver_name,receiver_tel,receiver_address')
                ->where('ConsignTran', '') //無物流單
                ->whereIn('PCOD1', $sales_no_list)
                ->get();
            foreach ($db as $d) {
                $sale_no = $d->PCOD1;
                $count = 1; //預設1件
                $size = 150; //預設
                $weight = 18; //預設
                //取得收件者、收件電話
                if (!empty($d->OriginalOrder)) { //取自外掛ERP
                    $remark = $d->OriginalOrder->remark;
                    $receiver = $d->OriginalOrder->receiver_name;
                    $tel = $d->OriginalOrder->receiver_tel;
                    $addr = $d->OriginalOrder->receiver_address;
                } else { //無法取得
                    $remark = '送貨前請先電聯/管理室代收';
                    $receiver = '';
                    $tel = '';
                    $addr = '';
                }
                if (YcsTransport::where('sale_no', $sale_no)->get()->count() == 0) { //無重複銷貨單號
                    YcsTransport::create([
                        'settle_date' => $settle_date,
                        'sale_no' => BaseService::AllTrim($sale_no),
                        'qty' => $count,
                        'size' => $size,
                        'weight' => $weight,
                        'name' => '',
                        'receiver' => $receiver,
                        'addr' => $addr,
                        'tel' => $tel,
                        'remark' => $remark,
                    ]);
                }
            }
        }
        return $msg;
    }

    /**
     * 取得毛利覆核所需的訂單資訊
     *
     * @param Request $request
     * @return array
     */
    public function GetOrder(Request $request)
    {
        $result = [];
        $msg = '';
        $sales_no = $request->get('sales_no');
        if (empty($sales_no)) {
            return $result;
        }
        try {
            $db = Posein::select('PCOD1', 'PCOD2', 'PTOT1', 'PBAK1', 'PBAK2', 'PBAK3', 'InsideNote')
                ->with([
                    'Histin' => function ($q) {
                        $q->select('HCOD1', 'HTOTA', 'HINUM', 'HSOUR', 'HICOD', 'HINAM', 'HUNIT');
                    },
                    'Order' => function ($q) {
                        $q->select('OCOD1', 'OTOT1');
                    }
                ])
                ->where('PCOD1', $sales_no)
                ->get();
            foreach ($db as $d) {
                $result = [
                    'sales_no' => $sales_no,
                    'order_amount' => number_format($d->Order->OTOT1),
                    'sales_amount' => number_format($d->PTOT1),
                    'bak1' => $d->bak1,
                    'bak2' => $d->bak2,
                    'bak3' => $d->bak3,
                    'InsideNote' => $d->InsideNote,
                ];
                foreach ($d->Histin as $item) {
                    $sales = $item->HTOTA;
                    $qty = $item->HINUM;
                    if ($qty == 0) {
                        $cost = null;
                    } else {
                        $cost = $item->HSOUR / $qty;
                    }
                    if ($sales > 0 && isset($cost)) {
                        $gpm = number_format(($sales - ($cost * $qty)) / $sales * 100, 2) . '%';
                    } else {
                        $gpm = '';
                    }
                    $result['items'][] = [
                        'no' => $item->HICOD,
                        'name' => $item->HINAM,
                        'unit' => number_format($item->HUNIT, 2),
                        'cost' => isset($cost) ? number_format($cost, 2) : '',
                        'gpm' => $gpm,
                    ];
                }
            }
        } catch (Exception $ex) {
            $msg = '取得銷單資料時發生錯誤。原因：' . $ex->getMessage();
        }
        return ['data' => $result, 'msg' => $msg];
    }

    //取得已包裝未上車
    public function GetUnshipped()
    {
        $msg = '';
        try {
            //已包裝未上車
            $result = LogPackage::select('sale_no')
                ->where('package_date', '!=', Carbon::today())
                ->where('printed', '>', 0)
                ->doesntHave('shipment')
                ->get();
            $sales = Posein::from('POSEIN AS P')
                ->select('PCOD1')
                ->whereNotIn('TransportCode', ['BH', 'COM']) //排除回頭、親送
                ->whereNotIn('PPCOD', ['C001'])
                ->whereIn('PCOD1', $result->pluck('sale_no')->toArray())
                ->whereNotExists(function ($q) { //排除退貨
                    $q->select(DB::raw(1))
                        ->from('POSEOU AS R')
                        ->whereRaw("(R.PCOD2 != '' AND R.PCOD2 = P.PCOD2)");
                })
                ->pluck('PCOD1')
                ->toArray();
            foreach ($result as $key => $val) {
                if (!in_array($val->sale_no, $sales)) {
                    $result->forget($key);
                }
            }
        } catch (Exception $ex) {
            $msg = '取得未上車資料時發生錯誤。原因：' . $ex->getMessage();
        }
        return ['data' => $result->pluck('sale_no')->toArray(), 'msg' => $msg];
    }

    //取得匯入TMS物流資料
    public function GetYcsTmsExport(Request $request)
    {
        $ids = json_decode($request->get('ids'));
        if (empty($ids) || count($ids) == 0) {
            return;
        }
        $result[] = ['銷貨單號', '客戶訂單單號', '託運單號', '物流公司代號'];
        foreach (YcsTransport::whereIn('id', $ids)->whereNotNull('systemNumber')->get() as $y) {
            $result[] = [
                $y->sale_no, //銷貨單號
                '', //客戶訂單單號
                $y->systemNumber, //託運單號
                'OTHER2' //物流公司代號
            ];
        }
        return $result;
    }

    //Api上傳
    public function YCSApi(Request $request)
    {
        $ids = json_decode($request->get('ids'));
        if (empty($ids) || count($ids) == 0) {
            return;
        }
        $data = [];
        foreach (YcsTransport::whereNull('systemNumber')->whereIn('id', $ids)->get() as $order) {
            $data[] = [
                "trackingNumber" => $order->sale_no, //訂單號碼(沒填為音速系統自動產生編號)
                "numOfPackage" => $order->qty, //數量
                "weight" =>  $order->weight, //重量
                "size" => $order->size, //尺寸
                "remarks" => $order->remark, //備註
                "name" => $order->receiver, //收件人
                "address" => $order->addr, //收件人地址
                "phone" => $order->tel, //收件人電話
            ];
        }
        $curl = curl_init();
        $url = 'https://system.ycs-express.com/api/web/createYCSCargo';
        $account_number = 'c001810';
        $key = "SVtVCHaVJDdJaTjh1CwcDe2QaOVe1xl9";
        $iv = '0759759460032004';
        $data = $this->aesEncrypt($data, $key, $iv);
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => array('account_number' => $account_number, 'data' => $data),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        $result = json_decode($response);
        if ($result->success) {
            foreach ($result->data as $order_no => $d) {
                $m =  YcsTransport::where('sale_no', $order_no)->first();
                if (!empty($m)) {
                    $m->systemNumber = $d->systemNumber;
                    $m->save();
                }
            }
            return '';
        } else {
            return $result->msg;
        }
    }

    //取得人工地板
    public function GetOtherTransport(Request $request)
    {
        $date = $request->get('date');
        $include_ship = $request->get('include_ship', 0) == 1;
        $unsched = $request->get('unsched', 0);
        if ($unsched) {
            $caption = '未安排';
        } else {
            $caption = empty($date) ? '全部' : $date;
        }
        if ($include_ship) {
            $caption .= '【包含已上車】';
        }
        $s_date = new DateTime();
        $s_date->modify('-45 day');
        $tw_date = $this->GetTWDateStr($s_date);
        $item_data = $this->GetItemDetail();
        //免確認、非親送
        $basket_db = OrderSampleM::with(['Order:OCOD1,OCOD4,OCANC', 'Order.OrderDetail:OCOD1,OICOD,OINAM,OINUM', 'Order.OrderDetail.Item:ICODE,ITCOD', 'Order.OrderDetail.Item1:ICODE1,ITCO3,ITNA3', 'OtherTransport', 'Posein:PCOD1,PJONO', 'Posein.LogShipment:PCOD1'])
            ->whereIn('src', [1, 3, 5]) //官網、蝦皮2店、BVSHOP
            ->whereNotIn('trans', ['COM', 'BH']) //非親送、回頭車
            ->where('order_type', '!=', 0) //未確認、已確認
            ->where(function ($q) use ($s_date) {
                $q->where('order_date', '>=', $s_date)->orWhere('completed', 0);
            })
            ->get();
        $tms_db = Order::select('OCOD1', 'OCOD4', 'OBAK1', 'OBAK2', 'OBAK3', 'InsideNote')
            ->with(['OrderDetail' => function ($q) {
                $q->select('OCOD1', 'OICOD', 'OINAM', 'OINUM');
            }])
            ->with(['OrderDetail.Item' => function ($q) {
                $q->select('ICODE', 'ITCOD');
            }])
            ->with(['OrderDetail.Item1' => function ($q) {
                $q->select('ICODE1', 'ITCO3', 'ITNA3');
            }])
            ->with(['Posein' => function ($q) {
                $q->select('PCOD1', 'PCOD2', 'POFFE', 'PTXCO');
            }])
            ->with(['OtherTransport', 'OrderSample', 'Posein.LogShipment'])
            ->whereExists(function ($q) {
                $q->select('OCOD1')
                    ->from('ORSUB')
                    ->join('ITEM AS I', 'I.ICODE', 'ORSUB.OICOD')
                    ->join('ITEM1 AS I1', 'I1.ICODE1', 'ORSUB.OICOD')
                    ->whereColumn('ORSUB.OCOD1', 'ORDET.OCOD1')
                    ->where('I.ITCOD', '11')
                    ->whereIn('I1.ITCO3', ['014', '046']);
            })
            ->where('OCOD4', '!=', '')
            ->whereNotIn('OCCOD', ['S002', 'S003', 'S006'])
            ->where('OCANC', '!=', '訂單取消')
            ->whereNotIn('TransportCode', ['COM', 'BH']) //非親送、回頭車
            ->where('ODATE', '>=', $tw_date)
            ->get();
        $db = $basket_db->toBase()->merge($tms_db);
        $floor_short_name_config = $this->configService->GetSysConfig('floor_short_name');
        $result = [];
        foreach ($db as $d) {
            //人工資料
            if (empty($d->OtherTransport)) {
                $ship_date = null;
                $remark = null;
            } else {
                $ship_date = $d->OtherTransport->ship_date;
                $remark = $d->OtherTransport->remark;
            }
            if ($unsched) { //查詢未安排
                if (!empty($ship_date)) {
                    continue;
                }
            } else { //排除非指定出車日期的訂單
                if (!empty($date) && $date != $ship_date) {
                    continue;
                }
            }
            //訂單資料
            if (is_a($d, OrderSampleM::class)) { //籃子
                if (empty($remark)) {
                    $remark = $d->remark; //預設取籃子撿貨單備註
                }
                $src = 1;
                $original_no = $d->order_no;
                $order_no = $d->Order->pluck('order_no')->toArray() ?? [];
                if (!$d->confirmed) {
                    $unconfirmed_id = $d->id;
                } else {
                    $unconfirmed_id = null;
                }
                $order_sample = $d;
                if (!empty($d->Order) && count($d->Order) > 0) {
                    if ($d->Order->first()->OCANC == '訂單取消') { //TMS內取消訂單
                        continue;
                    }
                    $tmp = collect();
                    foreach ($d->Order as $order_db) {
                        $tmp = $tmp->toBase()->merge($order_db->OrderDetail);
                    }
                    $detail_data = $tmp;
                } else {
                    $detail_data = json_decode($order_sample->original_data);
                }
                $is_pallet = $d->order_kind == 2;
            } else { //tms
                $src = 2;
                $original_no = $d->original_no;
                $order_no = [$d->order_no];
                $order_sample = $d->OrderSample;
                $detail_data = $d->OrderDetail;
                $unconfirmed_id = null;
                $is_pallet = $this->IsPallet($d->bak1, $d->bak2, $d->bak3, $d->InsideNote);
            }
            //銷貨資料
            if (empty($d->Posein)) {
                $sale_no = [];
            } else {
                $sale_no = [];
                $all_ship = true;
                foreach ($d->Posein as $sale_db) {
                    $sale_no[] = $sale_db->sale_no;
                    if (empty($sale_db->LogShipment) || count($sale_db->LogShipment) == 0) {
                        $all_ship = false;
                    }
                }
                //排除已上車，未安排不排除
                if (!$unsched && count($sale_no) > 0 && $all_ship && !$include_ship) {
                    continue;
                }
            }
            if (!empty($order_sample)) {
                $columns = $this->orderSampleService->GetOrderSampleColumns(json_decode($order_sample->header), $order_sample->src);
            }
            //訂單明細資料
            $detail = [];
            foreach ($detail_data as $item) {
                if (is_a($item, OrderDetail::class)) {
                    $item_no = $item->item_no;
                    $qty = $item->qty;
                    if (empty($qty)) {
                        continue;
                    }
                    $item_kind1 = $item->Item->kind;
                    if (in_array($item_kind1, ['98', '99'])) {
                        continue;
                    }
                    $item_kind3 =  $item->Item1->kind3;
                    $short_name = $this->GetShortName($item_kind1, $item_kind3, $item->Item1->kind3_name, $item->item_name, $floor_short_name_config);
                } else {
                    $item_no = $item[$columns['item_no']];
                    $qty = $item[$columns['item_quantity']];
                    if (empty($qty) || !key_exists($item_no, $item_data)) {
                        continue;
                    }
                    $item_kind1 = $item_data[$item_no]['kind1'];
                    if (in_array($item_kind1, ['98'])) {
                        continue;
                    }
                    $item_kind3 = $item_data[$item_no]['kind3'];
                    $short_name = $this->GetShortName($item_kind1, $item_kind3, $item_data[$item_no]['kind3_name'], $item_data[$item_no]['name'], $floor_short_name_config);
                }
                $item_name = $short_name['item_name'];
                $item_kind3_name = $short_name['item_kind3_name'];
                if ($item_kind1 != '11' || !in_array($item_kind3, ['014', '046'])) { //非地板
                    continue;
                }
                $detail[] = [
                    'item_kind1' => $item_kind1,
                    'item_kind3' => $item_kind3,
                    'item_kind3_name' => $item_kind3_name,
                    'item_no' => $item_no,
                    'item_name' => $item_name,
                    'qty' => $qty,
                ];
            }
            if (count($detail) == 0) { //沒有任何地板
                continue;
            }
            $result[] = [
                'src' => $src,
                'ship_date' => $ship_date,
                'is_pallet' => $is_pallet,
                'original_no' => $original_no,
                'order_no' => $order_no,
                'sale_no' => $sale_no,
                'remark' => $remark,
                'detail' => $detail,
                'unconfirmed_id' => $unconfirmed_id,
            ];
        }
        //地板顏色統計
        $statistics = [];
        $statistics_columns = [];
        foreach (array_column($result, 'detail') as $items) {
            foreach ($items as $item) {
                $item_name = $item['item_name'];
                $item_kind3_name = $item['item_kind3_name'];
                $qty = intval($item['qty']);
                $statistics_columns[] = $item_kind3_name; //小類名稱
                if (key_exists($item_name, $statistics)) { //加總
                    if (key_exists($item_kind3_name, $statistics[$item_name])) {
                        $statistics[$item_name][$item_kind3_name] += $qty;
                    } else {
                        $statistics[$item_name][$item_kind3_name] = $qty;
                    }
                } else { //初始化
                    $statistics[$item_name] = [$item_kind3_name => $qty];
                }
            }
        }
        ksort($statistics);
        $statistics_columns = array_unique($statistics_columns);
        sort($statistics_columns);
        //統計
        $statistics_total = ['數量統計' => []];
        $package_total = 0;
        foreach ($statistics_columns as $col) {
            $sum = array_sum(array_column($statistics, $col));
            $statistics_total['數量統計'][$col] = $sum;
            //總包數
            if ($col == 'LVT') {
                $package_total += round($sum / 10, 2);
            } else {
                $package_total += $sum;
            }
        }
        $statistics_columns[] = '總包數';
        $statistics_total['數量統計']['總包數'] = $package_total;
        return [
            'data' => $result,
            'statistics' => $statistics,
            'statistics_columns' => $statistics_columns,
            'statistics_total' => $statistics_total,
            'caption' => $caption
        ];
    }

    /**
     * AES-256加密
     *
     * @param array $data 
     * @param string $key
     * @param string $iv
     * @return string
     */
    private function aesEncrypt(array $data, string $key, string $iv)
    {
        $json = json_encode($data);
        $encrypt = openssl_encrypt($json, 'AES-256-CBC', $key, true, $iv);
        $result = base64_encode($encrypt);

        return $result;
    }
    //取得親送資料
    private function GetCOM($search_type, Request $request, $config)
    {
        $unsched = $search_type == 1;
        $s_date = new DateTime();
        $s_date->modify('-45 day');
        $tw_date = $this->GetTWDateStr($s_date);
        $item_data = $this->GetItemDetail();
        //取得籃子內的親送
        $basket_db = OrderSampleM::select('id', 'src', 'order_no', 'confirmed', 'header', 'original_data', 'receiver_name', 'receiver_tel', 'receiver_address')
            ->with(['Posein' => function ($q) {
                $q->select('PCOD1', 'PJONO', 'POFFE', 'PTXCO');
            }])
            ->with(['Order:OCOD1,OCOD4,OTOT2,OCANC', 'Order.OrderDetail:OCOD1,OICOD,OINAM,OINUM', 'PersonalTransport', 'PersonalTransport.User'])
            ->whereIn('src', [1, 3, 5]) //取官網、蝦皮2店
            ->whereIn('trans', ['COM', 'BH']);
        switch ($search_type) {
            case 3: //依原始訂單編號
                $input_original_no = $request->get('original_no');
                $basket_db = $basket_db->where('order_no', $input_original_no);
                break;
            default:
                $basket_db = $basket_db
                    ->where(function ($q) use ($s_date) {
                        $q->where('order_date', '>=', $s_date)->orWhere('completed', 0);
                    });
                break;
        }
        $basket_db = $basket_db->get();
        //取得TMS內的親送
        $tms_db = Order::select('OCOD1', 'OCOD4', 'OTOT2')
            ->with(['OrderDetail' => function ($q) {
                $q->select('OCOD1', 'OICOD', 'OINAM', 'OINUM');
            }])
            ->with(['OrderDetail.Item' => function ($q) {
                $q->select('ICODE', 'ITCOD');
            }])
            ->with(['OrderDetail.Item1' => function ($q) {
                $q->select('ICODE1', 'ITCO3', 'ITNA3');
            }])
            ->with(['Posein' => function ($q) {
                $q->select('PCOD1', 'PCOD2', 'POFFE', 'PTXCO');
            }])
            ->with(['PersonalTransport', 'PersonalTransport.User', 'OrderSample', 'Posein.LogShipment'])
            ->whereNotIn('OCCOD', ['S002', 'S003', 'S006'])
            ->where('OCANC', '!=', '訂單取消')
            ->whereIn('TransportCode', ['COM', 'BH']);
        switch ($search_type) {
            case 3: //依原始訂單編號
                $input_original_no = $request->get('original_no');
                $tms_db = $tms_db->where('OCOD4', $input_original_no);
                break;
            default:
                $tms_db = $tms_db->where('OCOD4', '!=', '')
                    ->where('ODATE', '>=', $tw_date);
                break;
        }
        $tms_db = $tms_db->get();
        $db = $basket_db->toBase()->merge($tms_db);
        $floor_short_name_config = $this->configService->GetSysConfig('floor_short_name');
        $result = [];
        foreach ($db as $d) {
            //親送資料
            if (empty($d->PersonalTransport)) {
                $ship_date = null;
                $driver = null;
                $driver_name = null;
                $car = null;
                $car_name = null;
                $car_no = null;
                $remark = null;
                $cust_confirmed = 0;
                $additional_addr = null;
            } else {
                $ship_date = $d->PersonalTransport->ship_date;
                $driver = $d->PersonalTransport->User->id ?? null;
                $driver_name = $d->PersonalTransport->User->name ?? null;
                $car = $d->PersonalTransport->Car->id ?? null;
                $car_name = $d->PersonalTransport->Car->name ?? null;
                $car_no = $d->PersonalTransport->Car->no ?? null;
                $remark = $d->PersonalTransport->remark;
                $cust_confirmed = $d->PersonalTransport->cust_confirmed;
                $additional_addr = $d->PersonalTransport->additional_addr;
            }
            if ($unsched) { //查詢未安排
                if (!empty($ship_date)) {
                    continue;
                }
            } else { //排除非指定出車日期的訂單
                $date = $request->get('date');
                $input_driver = $request->get('driver');
                if (!empty($date) && $date != $ship_date) {
                    continue;
                } elseif (!empty($input_driver) && $input_driver != $driver) {
                    continue;
                }
            }
            //訂單資料
            if (is_a($d, OrderSampleM::class)) { //籃子
                if (empty($remark)) {
                    $remark = $d->remark; //預設取籃子撿貨單備註
                }
                $src = 1;
                $original_no = $d->order_no;
                $order_no = $d->Order->pluck('order_no')->toArray() ?? [];
                $total_tax =  $d->Order->first()->OTOT2 ?? 0;
                if (!$d->confirmed) {
                    $unconfirmed_id = $d->id;
                } else {
                    $unconfirmed_id = null;
                }
                $order_sample = $d;
                if (!empty($d->Order) && count($d->Order) > 0) { //有TMS訂單資料，以TMS訂單內容為主
                    if ($d->Order->first()->OCANC == '訂單取消') { //TMS內取消訂單
                        continue;
                    }
                    $tmp = collect();
                    foreach ($d->Order as $order_db) {
                        $tmp = $tmp->toBase()->merge($order_db->OrderDetail);
                    }
                    $detail_data = $tmp;
                } else {
                    $detail_data = json_decode($order_sample->original_data);
                }
            } else { //tms
                $src = 2;
                $original_no = $d->original_no;
                $order_no = [$d->order_no];
                $total_tax = $d->sales_total_tax;
                $order_sample = $d->OrderSample;
                $detail_data = $d->OrderDetail;
                $unconfirmed_id = null;
            }
            //銷貨資料
            if (empty($d->Posein)) {
                $sale_no = [];
            } else {
                $sale_no = $d->Posein->pluck('sale_no')->toArray();
            }
            //客戶資料
            if (empty($order_sample)) {
                $receiver_name = '';
                $receiver_address = '';
                $receiver_tel = '';
            } else {
                $columns = $this->orderSampleService->GetOrderSampleColumns(json_decode($order_sample->header), $order_sample->src);
                //收件者資料
                $receiver_name = $order_sample->receiver_name;
                $receiver_address = $order_sample->receiver_address;
                $receiver_tel = $order_sample->receiver_tel;
            }
            //地區
            if (empty($additional_addr)) {
                $area = $this->GetArea($receiver_address);
            } else {
                $area = $this->GetArea($additional_addr);
            }
            //訂單明細資料
            $detail = [];
            foreach ($detail_data as $item) {
                if (is_a($item, OrderDetail::class)) {
                    $item_no = $item->item_no;
                    $qty = $item->qty;
                    if (empty($qty)) {
                        continue;
                    }
                    $item_kind1 = $item->Item->kind;
                    if (in_array($item_kind1, ['98', '99'])) {
                        continue;
                    }
                    $item_kind3 =  $item->Item1->kind3;
                    $short_name = $this->GetShortName($item_kind1, $item_kind3, $item->Item1->kind3_name, $item->item_name, $floor_short_name_config);
                    $item_name = $short_name['item_name'];
                    $item_kind3_name = $short_name['item_kind3_name'];
                } else {
                    $item_no = $item[$columns['item_no']];
                    $qty = $item[$columns['item_quantity']];
                    if (empty($qty)) {
                        continue;
                    }
                    if (empty($total_tax)) {
                        $total_tax = $item[$columns['order_total']];
                    }
                    if (key_exists($item_no, $item_data)) {
                        $item_kind1 = $item_data[$item_no]['kind1'];
                        if (in_array($item_kind1, ['98', '99'])) {
                            continue;
                        }
                        $item_kind3 = $item_data[$item_no]['kind3'];
                        $short_name = $this->GetShortName($item_kind1, $item_kind3, $item_data[$item_no]['kind3_name'], $item_data[$item_no]['name'], $floor_short_name_config);
                        $item_name = $short_name['item_name'];
                        $item_kind3_name = $short_name['item_kind3_name'];
                    } else {
                        $item_name = '';
                        $item_kind1 = '';
                        $item_kind3 = '';
                        $item_kind3_name = '';
                    }
                }
                //重量處裡
                $w = ($config->where('value1', $item_kind3_name)->first()->value2 ?? 0) * $qty;
                $detail[] = [
                    'item_kind1' => $item_kind1,
                    'item_kind3' => $item_kind3,
                    'item_kind3_name' => $item_kind3_name,
                    'item_no' => $item_no,
                    'item_name' => $item_name,
                    'qty' => $qty,
                    'weight' => $w,
                ];
            }
            $total_weight = collect($detail)->sum('weight') / 1000;
            sort($detail);
            $result[] = [
                'src' => $src,
                'ship_date' => $ship_date,
                'area' => $area,
                'driver' => $driver,
                'driver_name' => $driver_name,
                'car' => $car,
                'car_name' => $car_name,
                'car_no' => $car_no,
                'original_no' => $original_no,
                'order_no' => $order_no,
                'sale_no' => $sale_no,
                'total_tax' => $total_tax,
                'total_weight' => $total_weight,
                'remark' => $remark,
                'cust_confirmed' => $cust_confirmed,
                'receiver_name' => $receiver_name,
                'receiver_address' => $receiver_address,
                'receiver_tel' => $receiver_tel,
                'additional_addr' => $additional_addr,
                'detail' => $detail,
                'unconfirmed_id' => $unconfirmed_id,
            ];
        }
        return $result;
    }

    //取得運費
    public function GetFreight(Request $request)
    {
        $start_date = $request->get('start_date');
        $end_date = $request->get('end_date');
        $type = $request->get('type');
        if (empty($start_date) || empty($end_date)) {
            return ['data' => collect(), 'kind1_analyze' => [], 'total_freight_sum' => 0];
        }
        $kind1_analyze = ['未知' => ['freight' => 0, 'qty' => 0, 'order' => 0]];
        $db = Freight::with('Histin:HCOD1,HICOD,HYCOD,HYNAM,HINAM,HINUM')
            ->whereBetween('ship_date', [$start_date, $end_date]);
        if (!empty($type)) {
            $db->where('trans', $type);
        }
        $db = $db->get();
        foreach ($db as &$d) {
            $total_freight = $d->freight + $d->combined_freight + $d->other_freight; //運費+聯運費+其他運費
            $d->avg_freight = $d->freight / $d->qty; //單件運費
            $d->total_freight = $total_freight;
            $kind1_list = []; //銷貨單內含大類
            if (!empty($d->Histin) && count($d->Histin) > 0) {
                $detail = [];
                foreach ($d->Histin as $item) {
                    $item_no = $item->item_no;
                    $kind1 = $item->HYCOD;
                    $qty = $item->qty;
                    if (empty($qty) || $kind1 == '98' || ($kind1 == '99' && $item_no >= 99010022000)) { //排除折扣、非樣片包材
                        continue;
                    }
                    $kind1_name = $item->HYNAM;
                    if (!in_array($kind1_name, $kind1_list)) {
                        $kind1_list[] = $kind1_name;
                        if (key_exists($kind1_name, $kind1_analyze)) {
                            $kind1_analyze[$kind1_name]['freight'] += $total_freight;
                            $kind1_analyze[$kind1_name]['qty'] += $d->qty;
                            $kind1_analyze[$kind1_name]['order']++;
                        } else {
                            $kind1_analyze[$kind1_name] = [
                                'freight' => $total_freight,
                                'qty' => $d->qty,
                                'order' => 1,
                            ];
                        }
                    }
                    if (key_exists($item_no, $detail)) {
                        $detail[$item_no]['qty'] += $qty;
                    } else {
                        $detail[$item_no] = [
                            'item_name' => $item->item_name,
                            'qty' => $qty,
                        ];
                    }
                }
                $d->detail = $detail;
            } else {
                $kind1_analyze['未知']['freight'] += $total_freight;
                $kind1_analyze['未知']['qty'] += $d->qty;
                $kind1_analyze['未知']['order']++;
            }
            $d->kind1_list = $kind1_list;
        }
        ksort($kind1_analyze);

        return ['data' => $db, 'kind1_analyze' => $kind1_analyze];
    }

    //取得簡寫的小類、商品名稱
    private function GetShortName($kind1, $kind3, $kind3_name, $name, $floor_short_name_config)
    {
        $item_kind3_name = $kind3_name;
        switch ($kind1) {
            case '11': //地板大類
            case '20': //園藝資材
                $item_name = $this->GetFloorColor($name, $floor_short_name_config); //取得地板名稱縮寫
                switch ($kind3) {
                    case '014': //卡扣地板
                        if (strpos($name, 'V3') !== false) {
                            $item_kind3_name = 'SPC(V3)';
                        } else if (strpos($name, '極厚寬版') !== false) {
                            $item_kind3_name = 'SPC(厚寬)';
                        } else {
                            $item_kind3_name = 'SPC';
                        }
                        break;
                    case '020': //收編條
                        $item_kind3_name = '收編條';
                        break;
                    case '046': //免澆地板
                        $item_kind3_name = 'LVT';
                        break;
                }
                break;
            default:
                $item_name = $name;
                break;
        }
        return [
            'item_name' => $item_name,
            'item_kind3_name' => $item_kind3_name,
        ];
    }

    //取得地區
    public function GetArea($addr)
    {
        if (empty($addr)) {
            return '其他';
        }
        $addr = str_replace('台', '臺', $addr);
        foreach (self::Area as $area => $county_list) {
            foreach ($county_list as $county) {
                if (mb_strpos($addr, $county) !== false) {
                    return $area;
                }
            }
        }
        return '其他';
    }
}
