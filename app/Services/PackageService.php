<?php

namespace App\Services;

use App\User;
use DateTime;
use Carbon\Carbon;
use App\Models\TMS\Histin;
use App\Models\TMS\Posein;
use App\Models\TMS\HCTData;
use Illuminate\Http\Request;
use App\Services\BaseService;
use App\Models\yherp\LogPackage;
use App\Models\yherp\LogShipment;
use App\Models\yherp\YcsTransport;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Models\TMS\PoseinCheckDataLog;
use App\Models\yherp\CustomerComplaint;
use App\Models\yherp\PersonalTransport;
use App\Models\yherp\OrderShipmentStatusM;

class PackageService extends BaseService
{
    private const package_type = ['免包裝', '非地板', '地板'];
    private const com_item_type = ['基本單數', 'SPC(包)', 'LVT(包)', '塑木地板(箱)'];

    //取得指定日期的包裝清單
    public function GetPackageList(Request $request)
    {
        $s_date = $request->get('s_date', Carbon::today()->format('Y-m-d')) . ' 00:00:00';
        $e_date = $request->get('e_date', Carbon::today()->format('Y-m-d')) . ' 23:59:59';
        $db = LogPackage::whereBetween('created_at', [$s_date, $e_date])->get()
            ->filter(function ($q) { //排除無銷貨單的包裝紀錄
                return !empty($q->Posein);
            })
            ->map(function ($item) {
                if (empty($item->completed_at)) {
                    $item->interval = '';
                    $item->interval_s = 0;
                } else {
                    $s = new DateTime($item->created_at);
                    $e = new DateTime($item->completed_at);
                    $item->interval = $e->diff($s);
                    $item->interval_s = $e->getTimestamp() - $s->getTimestamp();
                }
                $item->type = self::package_type[$item->package_type];
                return $item;
            });
        //每日統計
        $total_all = [];
        $statistics = [];
        $days = $this->GetEachDay($s_date, $e_date);
        foreach ($days as $day) {
            foreach ($db->whereIn('package_type', [1, 2])->where('package_date', $day)->sortBy('user_id')->groupBy('user_id') as $user_id => $group) {
                $user_name = $group->first()->User->name;
                $shop_db = $group->whereIn('tran_code', BaseService::shop_trans);
                $home_db = $group->whereIn('tran_code', BaseService::home_trans);
                $other = $group->whereNotIn('tran_code', array_merge(BaseService::shop_trans, BaseService::home_trans))->sum('tran_qty');
                $shop = $shop_db->sum('tran_qty');
                $home = $home_db->sum('tran_qty');
                if (empty($shop)) {
                    $shop_time = '';
                } else {
                    $shop_time = $this->ConvertSecondToTime($shop_db->sum('interval_s') / $shop);
                }
                if (empty($home)) {
                    $home_time = '';
                } else {
                    $home_time = $this->ConvertSecondToTime($home_db->sum('interval_s') / $home);
                }
                $miss = $group->where('completed_at', null)->sum('tran_qty');
                $total = $group->sum('tran_qty');
                $statistics[$day][$user_id] = [
                    'user_name' => $user_name,
                    'shop' => $shop,
                    'home' => $home,
                    'other' => $other,
                    'total' => $total,
                    'miss' => $miss,
                    'shop_time' => $shop_time,
                    'home_time' => $home_time,
                ];
                if (key_exists($user_id, $total_all)) {
                    $total_all[$user_id]['shop'] += $shop;
                    $total_all[$user_id]['home'] += $home;
                    $total_all[$user_id]['other'] += $other;
                    $total_all[$user_id]['total'] += $total;
                    $total_all[$user_id]['miss'] += $miss;
                    $total_all[$user_id]['shop_time'] += $shop_db->sum('interval_s');
                    $total_all[$user_id]['home_time'] += $home_db->sum('interval_s');
                } else {
                    $total_all[$user_id] = [
                        'user_name' => $user_name,
                        'shop' => $shop,
                        'home' => $home,
                        'other' => $other,
                        'total' => $total,
                        'miss' => $miss,
                        'shop_time' => $shop_db->sum('interval_s'),
                        'home_time' => $home_db->sum('interval_s'),
                    ];
                }
            }
        }
        //統計轉換時間格式
        foreach ($total_all as $user_id => &$data) {
            $data['shop_time'] = empty($data['shop']) ? '' : $this->ConvertSecondToTime($data['shop_time'] / $data['shop']);
            $data['home_time'] = empty($data['home']) ? '' : $this->ConvertSecondToTime($data['home_time'] / $data['home']);
        }
        //親送統計
        $com_total = [];
        $com_db = $db->whereIn('tran_code', ['COM', 'com']);
        $com_original_no = $com_db->where('POSEIN.PJONO', '!=', '')->pluck('POSEIN.PJONO')->toArray();
        $com_sales_failed = PersonalTransport::whereIn('original_no', $com_original_no)->where('undelivered', 1)->pluck('original_no')->toArray(); //親送未送達
        $com_db = $com_db->whereNotIn('POSEIN.PJONO', $com_sales_failed);
        $com_sales = $com_db->pluck('sale_no')->toArray();
        $sale_db = Histin::select('HCOD1', 'HYCOD', 'HICOD', 'HINUM')
            ->with(['ITEM1' => function ($q) {
                $q->select('ICODE1', 'ITCO3');
            }])
            ->whereIn('HCOD1', $com_sales)
            ->whereNotIn('HYCOD', ['98', '99']) //排除98、99
            ->where('HINUM', '!=', 0)
            ->get();
        foreach ($db->whereIn('tran_code', ['COM', 'com'])->whereNotIn('POSEIN.PJONO', $com_sales_failed) as $package) {
            $user_name = $package->User->name;
            if (!key_exists($user_name, $com_total)) {
                foreach (self::com_item_type as $key => $t) {
                    $com_total[$user_name][$key] = 0;
                }
            }
            $com_total[$user_name][0] += 1; //基本單數
            foreach ($sale_db->where('HCOD1', $package->sale_no) as $detail) {
                $item_no = $detail->HICOD;
                $qty = $detail->HINUM;
                $kind1 = $detail->HYCOD;
                $kind3 = $detail->ITEM1->ITCO3;
                //bouns
                if (in_array($item_no, ['11010014037', '11010014038', '11010014039', '11010014040', '11010014051', '11010014052', '11010014061', '11010014064'])) { //SPC 8片
                    $type = 1;
                } else if ('11010014072' <= $item_no && $item_no <= '11010014095') { //SPC V3、極厚寬版
                    $type = 1;
                } else if ($kind1 == '11' && $kind3 == '046') { //免膠地板
                    $type = 2;
                } else if (('11010047001' <= $item_no && $item_no <= '11010047014') || $item_no == '11010047020') { //塑木地板
                    $type = 3;
                } else {
                    continue;
                }
                $com_total[$user_name][$type] += $qty; //bouns
            }
        }
        //換算單位
        foreach ($com_total as $user_name => &$items) {
            $items[2] /= 10; //LVT
            $items[3] /= 11; //塑木地板
        }

        return ['data' => $db, 'statistics' => $statistics, 'total_all' => $total_all, 'com_total' => $com_total, 'com_item_type' => self::com_item_type];
    }

    //包裝前驗貨
    public function start_checking(Request $request)
    {
        try {
            $user_id = $request->get('user_id');
            $package_no = $request->get('package_no');
            $result = [
                'user_id' => $user_id,
                'msg' => ''
            ];
            //驗證
            if (empty($package_no)) {
                $result['msg'] = '條碼為空';
                return $result;
            } elseif (empty($user_id)) {
                $result['msg'] = '尚未登入使用者';
                return $result;
            }
            //取得銷貨資料
            $db = Posein::select('PCOD1', 'POFFE', 'PTXCO', 'PBAK1', 'PBAK2', 'PBAK3', 'InsideNote', 'TransportCode', 'ConsignTran', 'PTIME')
                ->with(['Histin' => function ($q) {
                    $q->select('HCOD1', 'HYCOD',  'HICOD', 'HINAM', 'HINUM')
                        ->where('HINUM', '>', 0);
                }])
                ->with(['Item1' => function ($q) {
                    $q->select('ICODE1', 'ITCO3');
                }]);
            //解密銷貨單號，16進位長度比11小
            if (strlen($package_no) < 11 && ctype_xdigit($package_no)) {
                $package_no = hexdec($package_no);
            }
            //檢驗條碼內容長度超過11為銷單、15為發票
            if (strlen($package_no) == 11) { //以銷貨單搜尋
                $db->where('PCOD1', $package_no);
            } elseif (strlen($package_no) >= 15) { //以發票號碼搜尋
                $tax = $this->GetTax($package_no);
                $db->where('PTXCO', $tax);
            } else {
                $result['msg'] = '條碼格式錯誤';
                return $result;
            }

            $db = $db->first();
            $extra_time =  new DateTime(Carbon::today()->format('Y-m-d 14:00:00'));
            if (empty($db)) {
                $result['msg'] = '找不到銷貨資料';
                return $result;
            }
            /*else if (new DateTime($db->PTIME) >= $extra_time && !User::find($user_id)->can('package_extra_order')) {
                $result['msg'] = '此單為加單，請辦公室人員自行包裝';
                return $result;
            }*/ else {
                $sale_no = $db->sale_no;
                $transport_code = strtoupper($db->trans);
                $transport_no = $db->trans_no;
                $tax_type = $db->tax_type;
                $tax = $db->tax;
                if (empty($tax) && $tax_type == '0') { //無發票 且 隨後附發票
                    $result['msg'] = '此單隨後附發票且未開發票，請抽單';
                    return $result;
                } else if (empty($transport_code)) { //沒有物流方式
                    $result['msg'] = '此單未設定物流方式，請抽單';
                    return $result;
                } else if (empty($transport_no) && (in_array($transport_code, BaseService::shop_trans) || in_array($transport_code, BaseService::home_trans))) { //宅配超商無物流單號
                    $result['msg'] = '此單未產出物流單號，請抽單';
                    return $result;
                } else if ($transport_code == "HCT" && $this->IsTransError($sale_no)) { //驗證新竹件數重量是否符合標準
                    $result['msg'] =  "銷貨單號：{$sale_no}，託運件數、重量異常，請抽單";
                    return $result;
                } else if ( //免包裝前驗貨
                    in_array($transport_code, ['BH', 'COM']) || //回頭車、親送
                    (count($db->Histin->whereNotIn('HYCOD', ['98', '99'])) == 0 && count($db->Histin->whereIn('HICOD', self::sample_floor)) == 0) || //不含商品
                    (in_array($transport_code, ['HCT', 'OTHER2']) && count($db->Histin->whereBetween('HICOD', ['17010041038', '17010041040'])) > 0) || //宅配豆腐砂
                    (count($db->Histin->where('HYCOD', '11')) > 0 && count($db->Item1->whereIn('ITCO3', ['014', '046'])) > 0) //SPC、LVT
                ) {
                    $result['package_no'] = $sale_no;
                    $result['skip'] = 1;
                    return $result;
                } else { //排除不須包裝驗貨的品項，98、99、樣片，
                    $db->Histin = $db->Histin->filter(function ($item) {
                        return !in_array($item->HYCOD, ['98', '99']) || in_array($item->HICOD, self::sample_floor);
                    });
                }
                //託運狀態處理
                $ship = OrderShipmentStatusM::with('Details')->where('sales_no', $sale_no)->first();
                $status = 0;
                // if ($this->ValidItems($db) && (empty($ship) || $ship->Details->where('_status', 3)->count() == 0)) { //特殊商品二次檢驗
                //     $m = OrderShipmentStatusM::firstOrCreate(['sales_no' => $sale_no]);
                //     $m->estimated_date = Carbon::today()->format('Y-m-d');
                //     // //更新狀態
                //     $m->Details()->firstOrCreate(['_status' => $request->get('status', 3)]);
                //     $m->save();
                //     return [
                //         'user_id' => $user_id,
                //         'msg' => "銷貨單號：{$sale_no}，需二次檢驗，請抽單"
                //     ];
                // } 
                if ($this->ValidFragileItems($db) && (empty($ship) || $ship->Details->where('_status', 4)->count() == 0)) { //易碎商品包裝監督
                    $m = OrderShipmentStatusM::firstOrCreate(['sales_no' => $sale_no]);
                    $m->estimated_date = Carbon::today()->format('Y-m-d');
                    // //更新狀態
                    $m->Details()->firstOrCreate(['_status' => $request->get('status', 4)]);
                    $m->save();
                    return [
                        'user_id' => $user_id,
                        'msg' => "銷貨單號：{$sale_no}，需包裝監督，請抽單"
                    ];
                }
                //無任何託運異常狀態 或 已出貨覆核 或 無二次檢驗狀態
                if (empty($ship) || $ship->shipment_check || $ship->Details->whereIn('_status', [3, 4])->count() == 0) {
                    $double_check = false;
                } else if ($ship->Details->where('_status', 3)->count() > 0) { //二次檢驗
                    $double_check = !$ship->Details->where('_status', 3)->first()->confirm;
                    $status = 3;
                } else if ($ship->Details->where('_status', 4)->count() > 0) { //包裝監督
                    $double_check = !$ship->Details->where('_status', 4)->first()->confirm;
                    $status = 4;
                } else {
                    $double_check = true;
                }
                //需贈品判斷
                $config = $this->configService->GetConfig('item_need_gift'); //需贈品設定檔
                $order_gift = $this->GetOrderGift($config, $db->Histin);
                $result['gift'] = $order_gift;
                $result['detail'] = $db->Histin;
                $result['user_name'] = User::find($user_id)->name ?? '';
                $result['package_no'] = $sale_no;
                $result['double_check'] = $double_check ? 1 : 0;
                $result['status'] = $status;
                return $result;
            }
        } catch (\Exception $ex) {
            $result['msg'] = '例外:' . $ex->getMessage();
            return $result;
        }
    }

    //包裝開始
    public function start_packing(Request $request)
    {
        try {
            $user_id = $request->get('user_id');
            $package_no = $request->get('package_no');
            //驗證
            if (empty($package_no)) {
                return ['msg' => '銷貨條碼為空'];
            } elseif (empty($user_id)) {
                return ['msg' => '尚未登入使用者'];
            }
            //驗證驗貨紀錄
            $sale_no = $package_no;
            if (PoseinCheckDataLog::where('PCOD1', $sale_no)->get()->count() == 0) {
                return ['msg' => '找不到驗貨紀錄'];
            }
            //取得銷貨資料
            $db = DB::table('POSEIN AS M')
                ->select('M.PCOD1', 'M.PDAT1', 'M.PPNAM', 'M.TransportCode', 'M.TransportName', 'M.TransportQty', 'M.ConsignTran', 'D.HYCOD', 'D.HICOD', 'D.HINAM', 'D.HINUM')
                ->addSelect('I1.ITCO3')
                ->addSelect(DB::raw("(SELECT TOP(1) Quantity FROM HCT_Data WHERE PCOD1=M.PCOD1 ORDER BY CREATE_DATE DESC) AS 'HctQty'"))
                ->join('HISTIN AS D', 'D.HCOD1', 'M.PCOD1')
                ->join('ITEM1 AS I1', 'D.HICOD', 'I1.ICODE1')
                ->where('M.PCOD1', $sale_no)
                ->get();
            if (empty($db) || count($db) == 0) {
                return ['msg' => '找不到銷貨資料'];
            } else {
                $ship_status = OrderShipmentStatusM::with('Details')->where('sales_no', $sale_no)->has('Details')->first();
                if (!empty($ship_status)) { //過大/過重、二次檢驗更新覆核
                    $ship_status->Details()->whereIn('_status', ['3', '4'])->update(['confirm' => 1]);
                }
                //主檔
                $first = $db->first();
                $cust_name = $first->PPNAM;
                $tran_code = BaseService::ConvertTransCode($first->TransportCode);
                $tran_name = $first->TransportName;
                $tran_no = $first->ConsignTran;
                $sale_date = $first->PDAT1;
                if ($tran_code == 'OTHER2') {
                    $tran_name = '音速物流';
                    $tran_qty = YcsTransport::where('sale_no', $sale_no)->first()->qty ?? ($first->TransportQty ?? 0);
                } elseif ($tran_code == 'HCT') {
                    $tran_qty = $first->HctQty ?? 0;
                } elseif (in_array($tran_code, BaseService::shop_trans)) {
                    $tran_qty = 1;
                } else {
                    $tran_qty = ($first->TransportQty ?? 0);
                }
                //明細
                $detail = [];
                foreach ($db as $d) {
                    $detail[] = [
                        'item_no' => $d->HICOD,
                        'name' => $d->HINAM,
                        'qty' => number_format($d->HINUM)
                    ];
                }
                //判斷貨物內容
                if (
                    in_array($tran_code, ['SC', 'COM', 'BH', 'MF']) ||
                    (in_array($tran_code, ['HCT', 'OTHER2']) && count($db->whereBetween('HICOD', ['17010041038', '17010041040'])) > 0) ||
                    count($db->where('HYCOD', '11')->whereIn('ITCO3', ['014', '046'])) > 0
                ) { //自取、本公司親送、回頭車、廠商直寄，宅配豆腐砂免包裝
                    $package_type = 0; //免包裝
                } else if (count($db->whereNotIn('HYCOD', ['98', '99'])) > 0) { //內含正常貨物
                    if (count($db->where('HYCOD', '11')->whereIn('ITCO3', ['014', '046'])) > 0) {
                        $package_type = 2; //地板類
                    } else {
                        $package_type = 1; //其他類
                    }
                } else {
                    if (count($db->whereIn('HICOD', BaseService::sample_floor)) > 0) { //樣片包
                        $package_type = 1; //其他類
                    } else { //無貨物
                        $package_type = 0; //免包裝
                    }
                }
                //重複包裝
                $id = '';
                $log = LogPackage::where('sale_no', $sale_no)->first();
                if (!empty($log)) {
                    if (empty($log->completed_at)) { //有包裝紀錄，未完成，繼續包裝
                        $id = $log->id;
                    } else {
                        return ['msg' => '此單已經包裝過了，請勿重複包裝'];
                    }
                } else {
                    $package_date = Carbon::today();
                    DB::connection('mysql')->beginTransaction();
                    try {
                        $db = LogPackage::lockForUpdate()
                            ->where('package_date', $package_date)
                            ->where('tran_code', $tran_code)
                            ->orderBy('seq', 'desc');
                        if ($package_type == 0 || $package_type == 1) { //免包裝 或 其他 使用同流水號
                            $db->whereIn('package_type', ['0', '1']);
                        } else { //地板
                            $db->where('package_type', '2');
                        }
                        $seq = $db->first()->seq ?? 0;
                        $logPackage = [
                            'sale_no' => $sale_no,
                            'user_id' => $user_id,
                            'machine_user_id' => Auth::user()->id,
                            'package_date' => $package_date,
                            'package_type' => $package_type,
                            'tran_code' => $tran_code,
                            'tran_qty' => $tran_qty,
                            'seq' => ($seq + 1),
                        ];
                        if ($package_type == 0) { //免包裝直接壓完成時間
                            $logPackage['completed_at'] = new DateTime();
                        }
                        if (in_array($tran_code, ['COM', 'BH'])) { //親送、回頭車直接壓已列印
                            $logPackage['printed'] = 1;
                        }
                        $id = LogPackage::create($logPackage)->id;
                        //親送、自取同步上車
                        if (in_array($tran_code, ['COM', 'BH'])) {
                            //上車紀錄
                            LogShipment::create([
                                'PCOD1' => $sale_no,
                                'PDATE' => $sale_date,
                            ]);
                        }
                    } catch (\Exception $e) {
                        DB::connection('mysql')->rollback();
                        return ['msg' => '建立包裝紀錄時，發生錯誤。原因：' . $e->getMessage()];
                    }
                    DB::connection('mysql')->commit();
                }
                return [
                    'data' => [
                        'id' => $id,
                        'user_id' => $user_id,
                        'user_name' => User::find($user_id)->name ?? '',
                        'sale_no' => $sale_no,
                        'cust_name' => $cust_name,
                        'tran_no' => $tran_no,
                        'tran_name' => $tran_name,
                        'tran_qty' => $tran_qty,
                        'detail' => $detail,
                    ],
                    'type' => $package_type,
                    'type_name' => self::package_type[$package_type],
                    'msg' => '',
                ];
            }
        } catch (\Exception $ex) {
            return ['msg' => '例外:' . $ex->getMessage()];
        }
    }

    //包裝結束
    public function end_packing(Request $request)
    {
        try {
            $id = $request->get('id');
            $user_id = $request->get('user_id');
            $sale_no = $request->get('sale_no');
            //驗證
            if (empty($id) || empty($sale_no)) {
                return ['msg' => '包裝條碼為空'];
            }
            //解密銷貨單號
            if (ctype_xdigit($sale_no)) {
                $sale_no = hexdec($sale_no);
            } else {
                return ['msg' => '包裝條碼無法解密'];
            }
            //驗證銷貨單號格式
            if (!$this->VerifySaleNo($sale_no)) {
                return ['msg' => '包裝條碼格式錯誤'];
            }
            $log = LogPackage::find($id);
            if (empty($log)) {
                return ['msg' => '此銷貨條碼尚未開始包裝'];
            } elseif ($log->sale_no != $sale_no) {
                return ['msg' => '此銷貨條碼並非目前包裝'];
            } else {
                $now = new DateTime();
                $pass = (strtotime(Carbon::now()) - strtotime($log->created_at));
                if ($pass < 10) {
                    return ['msg' => '包裝時間小於10秒，視為無效'];
                } else {
                    $log->update(['completed_at' => $now]);
                    return ['msg' => '', 'user_id' => $user_id, 'pass' => $pass];
                }
            }
        } catch (\Exception $ex) {
            return ['msg' => '例外:' . $ex->getMessage()];
        }
    }

    /**
     * 取得指定區間包裝人員
     *
     * @param Request $request
     * @return array
     */
    public function GetPackager(Request $request)
    {
        $sDate = $request->get('start_date', Carbon::today()->format('Y-m-d')) . ' 00:00:00';
        $eDate = $request->get('end_date', Carbon::today()->format('Y-m-d')) . ' 23:59:59';
        $data = [];
        //取的指定區間，有特殊註記(人為疏失)的客訴單
        $customerComplaintDB = CustomerComplaint::select('order_no', 'origin_no', 'mark', 'remark')
            ->whereBetween('created_at', [$sDate, $eDate])
            ->where('mark', '!=', '')
            ->where('mark', 'LIKE', '%1%')
            ->get();
        $order_list = $customerComplaintDB->pluck('order_no')->toArray();
        $original_list = $customerComplaintDB->pluck('origin_no')->toArray();
        if (count($order_list) > 0 || count($original_list) > 0) {
            //取得驗貨時間
            $poseinDB = Posein::with('PoseinCheckDataLog')
                ->select('PCOD1', 'PCOD2', 'PJONO')
                ->where(function ($q) use ($order_list, $original_list) {
                    $q
                        ->whereIn('PCOD2', $order_list)
                        ->orWhereIn('PJONO', $original_list);
                })->get();
            foreach ($customerComplaintDB as $d) {
                $order_no = $d->order_no;
                $origin_no = $d->origin_no;
                $sale_db = $poseinDB->filter(function ($item, $key) use ($order_no, $origin_no) {
                    return $item->order_no == $order_no || $item->PJONO == $origin_no;
                });
                $sales = [];
                foreach ($sale_db as $sale_d) {
                    $sales_no = $sale_d->PCOD1;
                    //驗貨資訊
                    $checkDataLog = $sale_d->PoseinCheckDataLog()->orderByDesc('CREATE_Date')->get();
                    if (count($checkDataLog) > 0) { //最後驗貨時間
                        $log = $checkDataLog->first();
                        $check_time = $log->CREATE_Date;
                        if (!empty($check_time)) {
                            $check_time = substr($check_time, 0, 19);
                        }
                        $check_machine = $log->SNAME;
                    } else {
                        $check_time = '';
                        $check_machine = '';
                    }
                    //包裝資訊
                    $packageLog = LogPackage::where('sale_no', $sales_no)->first();
                    if (empty($packageLog)) {
                        $package_machine = '';
                        $package_time = '';
                        $packager = '';
                    } else {
                        $package_machine = $packageLog->Machine_User->name;
                        $package_time = $packageLog->created_at;
                        $packager = $packageLog->User->name;
                    }
                    $sales[] = [
                        'sales_no' => $sales_no,
                        'check_machine' => $check_machine,
                        'check_time' => $check_time,
                        'package_machine' => $package_machine,
                        'package_time' => $package_time,
                        'packager' => $packager,
                    ];
                }
                $data[] = [
                    'order_no' => $order_no,
                    'origin_no' => $origin_no,
                    'remark' => $d->remark ?? '',
                    'sales' => $sales,
                ];
            }
        }

        return $data;
    }

    /**
     * 取得其他資訊
     *
     * @param Request $request
     * @return array
     */
    public function GetOrderPackagerView(Request $request)
    {
        $start_date = $request->get('start_date', Carbon::today()->format('Y-m-d'));
        $end_date = $request->get('end_date', Carbon::today()->format('Y-m-d'));
        $sTWDate = $this->GetTWDateStr(new DateTime($start_date));
        $eTWDate = $this->GetTWDateStr(new DateTime($end_date));
        $order_count = count(Posein::select('PCOD1')->whereBetween('PDAT1', [$sTWDate, $eTWDate])->whereNotIn('PPCOD', ['x001', 'x002'])->get());
        $salesCheckDB = $this->baseRepository->GetSalesLastCheck($start_date . ' 00:00:00', $end_date . ' 23:59:59');
        //依照機台分組
        $machine_check = [];
        foreach ($salesCheckDB->groupBy('SCODE') as $group) {
            $machine_check[] = [
                'machine' => $group->first()->SNAME,
                'count' => count($group)
            ];
        }
        $machine_check = collect($machine_check)->sortBy('machine')->toArray();
        //依照日期分組
        $in_work_time = 0;
        $work_day = 0;
        foreach ($salesCheckDB->groupBy('CheckDate') as $date => $group) {
            $lastCheckTime = new Carbon($group->sortByDesc('CREATE_Date')->first()->CREATE_Date);
            if ($lastCheckTime < new Carbon($date . ' 17:30:00.000')) {
                $in_work_time++;
            }
            $work_day++;
        }
        //
        return [
            'order_count' => $order_count,
            'machine_check' => $machine_check,
            'in_work_time' => $in_work_time,
            'work_day' => $work_day,
        ];;
    }

    //分析發票條碼
    private function GetTax(string $package_no)
    {
        //5碼=發票期、10碼發票號碼、4碼隨機碼
        return substr($package_no, 5, 10);
    }

    //驗證易碎物品須確認包裝
    private function ValidFragileItems($data)
    {
        if ($data->Histin->whereBetween('HICOD', ['20010027001', '20010027012'])->count() > 0) { //盆器類
            return true;
        } elseif ($data->Histin->whereBetween('HICOD', ['10003005001', '10003005004'])->count() > 0) { //舞光-螺旋燈泡-23W
            return true;
        } elseif ($data->Histin->whereBetween('HICOD', ['10003004112', '10003004114'])->count() > 0) { //舞光-蛋糕燈
            return true;
        } elseif ($data->Histin->whereBetween('HICOD', ['10003004182', '10003004185'])->count() > 0) { //舞光-蛋糕燈
            return true;
        } elseif ($data->Histin->where('ITCO3', '008')->count() > 0) { //小類燈管
            return true;
        } else {
            return false;
        }
    }

    //驗證特殊商品處理
    private function ValidItems($data)
    {
        if ($data->Histin->whereBetween('HICOD', ['19010030001', '19010030005'])->count() > 0) { //學習桌
            return true;
        } else if ($data->Histin->whereIn('HICOD', ['10010003133'])->count() > 0) { //優居-馬卡龍軌道燈-大-黑-B款
            return true;
        } else if (count($data->filter(function ($item) {
            $key = '廠商直';
            return mb_strpos($item->PBAK1, $key, 0, 'utf-8') !== false || mb_strpos($item->PBAK2, $key, 0, 'utf-8') !== false
                || mb_strpos($item->PBAK3, $key, 0, 'utf-8') !== false || mb_strpos($item->InsideNote, $key, 0, 'utf-8') !== false;
        })) > 0) {
            return true;
        } else {
            return false;
        }
    }

    //驗證託運是否錯誤
    private function IsTransError($sale_no)
    {
        $db = HCTData::select('Quantity', 'Weight')
            ->whereHas('POSEIN', function ($q) {
                $q
                    ->select(DB::raw('1'))
                    ->where('PBAK1', 'NOT LIKE', '%棧板%')
                    ->where('PBAK2', 'NOT LIKE', '%棧板%')
                    ->where('PBAK3', 'NOT LIKE', '%棧板%')
                    ->where('InsideNote', 'NOT LIKE', '%棧板%');
            })
            ->where('PCOD1', $sale_no)
            ->where('AccountID', '05172170053')
            ->orderByDesc('CREATE_DATE')
            ->first();

        $lvt_qty = $this->baseRepository->GetLvtCount([$sale_no])->first()->qty ?? 0;
        if (empty($db)) {
            return false;
        } else {
            $q = $db->Quantity;
            $w = $db->Weight;
            if (empty($q) || empty($w)) {
                return true;
            } elseif (!$this->VerifyHCT($q, $w)) {
                return true;
            } elseif (!$this->VerifyHCT_LVT($q, $lvt_qty)) {
                return true;
            } else {
                return false;
            }
        }
    }
}
