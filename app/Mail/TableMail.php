<?php

namespace App\Mail;

use DateTime;
use App\Exports\TableExport;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class TableMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if (
            !array_key_exists('subject', $this->data) || !array_key_exists('caption', $this->data) ||
            !array_key_exists('thead', $this->data) || !array_key_exists('tbody', $this->data)
        ) {
            return;
        }

        //storing raw contents
        $content = Excel::raw(
            new TableExport(
                $this->data['caption'],
                $this->data['thead'],
                $this->data['tbody']
            ),
            \Maatwebsite\Excel\Excel::XLSX
        );

        $today = new DateTime("now");

        return $this
            ->view('layouts.table')
            ->subject("【系統通知信件】{$today->format('Y/m/d')} {$this->data['subject']}") //標題
            ->with( //view參數
                [
                    'caption' => $this->data['caption'],
                    'thead' => $this->data['thead'],
                    'tbody' => $this->data['tbody']
                ]
            )->attachData( //excel附件
                $content,
                $today->format('Y_m_d') . $this->data['caption'] . '.' .  \Maatwebsite\Excel\Excel::XLSX
            );
    }
}
