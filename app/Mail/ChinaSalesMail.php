<?php

namespace App\Mail;

use DateTime;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use App\Exports\ChinaSalesExport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Queue\SerializesModels;

class ChinaSalesMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $today = new DateTime("now");
        $result = $this
            ->view('layouts.table')
            ->subject("【系統通知信件】{$today->format('Y/m/d')} 中國商品銷售對比"); //標題
        foreach ($this->data as $kind => $g) {
            //storing raw contents
            $content = Excel::raw(
                new ChinaSalesExport($g),
                \Maatwebsite\Excel\Excel::XLSX
            );
            $result->attachData( //excel附件
                $content,
                $today->format('Y_m_d') . $kind . '.' .  \Maatwebsite\Excel\Excel::XLSX
            );
        }

        return $result;
    }
}
