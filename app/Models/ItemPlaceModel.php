<?php

namespace App\Models;

/**
 * 儲位模型
 */
class ItemPlaceModel
{
    /**
     * 備註
     *
     * @var string
     */
    public $remark = '';

    /**
     * 倉別
     *
     * @var string
     */
    public $stock = '';

    /**
     * 架
     *
     * @var integer
     */
    public $shelf = 0;

    /**
     * 層
     *
     * @var integer
     */
    public $layer = 0;

    /**
     * 商品
     *
     * @var array
     */
    public $items = [];

    function __construct($stock, $shelf, $layer)
    {
        $this->stock = $stock;
        $this->shelf = $shelf;
        $this->layer = $layer;
    }

    public function GetPlace()
    {
        if (empty($this->remark)) {
            //倉別1碼+架子2碼+層數1碼
            return $this->stock . str_pad($this->shelf, 2, '0', STR_PAD_LEFT)  . $this->layer;
        } else {
            return $this->remark;
        }
    }

    /**
     * 儲位是否可以放
     *
     * @return boolean
     */
    public function CanAdd($item)
    {
        //層數上限檢查
        if ($this->layer == 2 && count($this->items) >= 1) { //2層最多1樣
            return false;
        } else if ($this->layer == 3 && count($this->items) >= 6) { //3層最多6樣
            return false;
        } else if ($this->layer == 4 && count($this->items) >= 8) { //4層最多8樣
            return false;
        }
        //長條商品必須放在四層
        if (($this->IsLong($item) || $this->IsEarth($item)) && $this->layer != 4) {
            return false;
        }
        if ($this->IsRepeat($item)) {
            return false;
        }
        //第四層只能放一種土方
        if ($this->layer == 4 && collect($this->items)->where('ITCO3', '023')->count() > 0) {
            return false;
        }
        return true;
    }

    /**
     * 儲位新增商品
     *
     * @param [type] $item
     * @return void
     */
    public function Add($item)
    {
        $this->items[] = $item;
    }

    /**
     * 是否有重複類型商品
     *
     * @param [type] $item
     * @return boolean
     */
    private function IsRepeat($item)
    {
        if (count($this->items) == 0) {
            return false;
        }
        return collect($this->items)
            ->where('ITCOD', $item->ITCOD)
            ->where('ITCO2', $item->ITCO2)
            ->where('ITCO3', $item->ITCO3)
            ->count() > 0;
    }

    /**
     * 是否為長形商品
     *
     * @param [type] $item
     * @return boolean
     */
    private function IsLong($item)
    {
        if (in_array($item->ITCO3, ['002', '006', '008'])) { //層板燈、軌道條、燈管
            return true;
        } elseif (mb_strpos($item->INAME, 'T5', 0, 'utf-8') !== false) { //名稱含T5
            return true;
        } elseif (mb_strpos($item->INAME, 'T8', 0, 'utf-8') !== false) { //名稱含T8
            return true;
        } elseif (mb_strpos($item->INAME, '山形', 0, 'utf-8') !== false) { //名稱含山形
            return true;
        } else {
            return false;
        }
    }

    /**
     * 是否為土方
     *
     * @param [type] $item
     * @return boolean
     */
    private function IsEarth($item)
    {
        if (in_array($item->ITCO3, ['023'])) { //
            return true;
        } else {
            return false;
        }
    }
}
