<?php

namespace App\Models\yherp;

use App\Models\yherp\ItemConvertD;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ItemConvertM extends Model
{
    use SoftDeletes;

    protected $connection = 'mysql';
    protected $table = 'item_convert_m';

    protected $fillable = [
        'enabled',
        's_date',
        'e_date',
        'remark',
    ];

    public function Details()
    {
        return $this->hasMany(ItemConvertD::class, 'm_id', 'id');
    }
}
