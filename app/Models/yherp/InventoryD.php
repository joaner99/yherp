<?php

namespace App\Models\yherp;

use App\Models\TMS\STOC;
use App\Models\TMS\ITEMP;
use App\Models\yherp\InventoryM;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class InventoryD extends Model
{
    use SoftDeletes;

    protected $connection = 'mysql';
    protected $table = 'inventory_d';
    protected $appends = ['main_diff', 'sale_diff'];

    protected $fillable = [
        'item_no',
        'item_name',
        'main_stock',
        'sale_stock',
        'tms_main_stock',
        'tms_sale_stock',
    ];

    public function getMainDiffAttribute()
    {
        return $this->main_stock - $this->tms_main_stock;
    }

    public function getSaleDiffAttribute()
    {
        return $this->sale_stock - $this->tms_sale_stock;
    }

    public function Main()
    {
        return $this->belongsTo(InventoryM::class, 'm_id', 'id');
    }

    public function Stoc()
    {
        return $this->hasMany(STOC::class, 'SITEM', 'item_no');
    }

    public function Itemp()
    {
        return $this->hasMany(ITEMP::class, 'ICODE', 'item_no');
    }
}
