<?php

namespace App\Models\yherp;

use App\Models\yherp\Stock;
use App\Models\yherp\ItemPlaceD;
use Illuminate\Database\Eloquent\Model;

class ItemPlaceM extends Model
{
    protected $connection = 'mysql';
    protected $table = 'item_place_m';
    protected $keyType = 'string';

    protected $fillable = [
        'name',
        'shelf',
        'layer',
    ];

    public function Stock()
    {
        return $this->belongsTo(Stock::class, 's_id', 'id');
    }

    public function Details()
    {
        return $this->hasMany(ItemPlaceD::class, 'm_id', 'id');
    }
}
