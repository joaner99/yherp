<?php

namespace App\Models\yherp;

use App\Models\TMS\Order;
use App\Models\yherp\OrderRemarkD;
use Illuminate\Database\Eloquent\Model;

class OrderRemarkM extends Model
{
    protected $connection = 'mysql';
    protected $table = 'order_remark_m';

    protected $fillable = [
        'OCOD1',
        'track',
        'completed_at'
    ];

    public function Details()
    {
        return $this->hasMany(OrderRemarkD::class, 'm_id', 'id');
    }

    public function Order()
    {
        return $this->belongsTo(Order::class, 'OCOD1', 'OCOD1');
    }
}
