<?php

namespace App\Models\yherp;

use App\Models\yherp\ItemPlaceM;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Stock extends Model
{
    use SoftDeletes;

    protected $connection = 'mysql';
    protected $table = 'stock';
    protected $keyType = 'string';

    protected $fillable = [
        'name'
    ];

    public function Places()
    {
        return $this->hasMany(ItemPlaceM::class, 's_id', 'id');
    }
}
