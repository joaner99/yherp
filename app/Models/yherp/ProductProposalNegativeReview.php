<?php

namespace App\Models\yherp;

use App\Services\ProductService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductProposalNegativeReview extends Model
{
    use SoftDeletes;

    protected $connection = 'mysql';
    protected $table = 'product_proposal_negative_review';
    protected $appends = ['text'];

    protected $fillable = ['val'];

    public function getTextAttribute()
    {
        return ProductService::negative_review[$this->val];
    }
}
