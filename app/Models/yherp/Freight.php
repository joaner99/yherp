<?php

namespace App\Models\yherp;

use App\Models\TMS\Histin;
use App\Services\OrderSampleService;
use Illuminate\Database\Eloquent\Model;

class Freight extends Model
{
    protected $connection = 'mysql';
    protected $table = 'freight';

    protected $fillable = [
        'sale_no',
        'trans_no',
        'trans',
        'trans_cust',
        'ship_date',
        'qty',
        'weight',
        'freight',
        'combined_freight',
        'other_freight',
    ];

    protected $appends = ['trans_name'];

    public function getTransNameAttribute()
    {
        switch ($this->trans) {
            case 'HCT':
                return "【{$this->trans_cust}】" . OrderSampleService::TransportType[$this->trans];
            default:
                return OrderSampleService::TransportType[$this->trans] ?? '';
        }
    }

    public function Histin()
    {
        return $this->hasMany(Histin::class, 'HCOD1', 'sale_no');
    }
}
