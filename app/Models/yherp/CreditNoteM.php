<?php

namespace App\Models\yherp;

use App\Models\TMS\Poseou;
use App\Models\yherp\CreditNoteD;
use App\Models\yherp\OrderInvoice;
use Illuminate\Database\Eloquent\Model;

class CreditNoteM extends Model
{
    protected $connection = 'mysql';
    protected $table = 'credit_note_m';

    protected $fillable = ['order_no', 'order_date', 'return_no', 'tms_return_no', 'invoice_no', 'invoice_date', 'tax', 'total', 'status'];

    protected $appends = ['status_name'];

    public function getStatusNameAttribute()
    {
        switch ($this->status) {
            case 0:
                return '未確認';
            case 1:
                if (empty($this->return_no)) {
                    return '已確認，未建立外掛退貨單';
                } elseif (empty($this->Poseou->order_no)) {
                    return '已確認，未建立TMS退貨單';
                } else {
                    return '已建立';
                }
            default:
                return '未定義';
        }
    }

    public function Details()
    {
        return $this->hasMany(CreditNoteD::class, 'm_id', 'id');
    }

    public function Invoice()
    {
        return $this->belongsTo(OrderInvoice::class, 'invoice_no', 'invoice_no');
    }

    //退貨
    public function Poseou()
    {
        return $this->hasOne(Poseou::class, 'PJONO', 'return_no');
    }
}
