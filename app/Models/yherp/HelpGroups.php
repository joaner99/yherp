<?php

namespace App\Models\yherp;

use App\Models\yherp\HelpQuestions;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class HelpGroups extends Model
{
    use SoftDeletes;

    protected $connection = 'mysql';
    protected $table = 'help_groups';

    protected $fillable = [
        'name',
    ];

    public function Questions()
    {
        return $this->hasMany(HelpQuestions::class, 'group_id', 'id');
    }
}
