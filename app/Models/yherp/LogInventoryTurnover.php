<?php

namespace App\Models\yherp;

use Illuminate\Database\Eloquent\Model;

class LogInventoryTurnover extends Model
{
    protected $connection = 'mysql';
    protected $table = 'log_inventory_turnover';
    public $timestamps = false;

    protected $fillable = [
        'rating',
        'sys_rating',
        'item_no',
        'item_name'
    ];
}
