<?php

namespace App\Models\yherp;

use App\Models\yherp\CountyArea;
use Illuminate\Database\Eloquent\Model;

class County extends Model
{
    protected $connection = 'mysql';
    protected $table = 'county';

    protected $fillable = [
        '_code', 'name'
    ];

    public function Areas()
    {
        return $this->hasMany(CountyArea::class, 'county_id', 'id');
    }
}
