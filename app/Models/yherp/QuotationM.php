<?php

namespace App\Models\yherp;

use App\User;
use App\Traits\Encrypt;
use App\Models\yherp\QuotationD;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class QuotationM extends Model
{
    use SoftDeletes;
    use Encrypt;

    protected $connection = 'mysql';
    protected $table = 'quotation_m';

    protected $encrypt = ['contact_phone', 'contact_email', 'company_phone', 'company_fax', 'company_addr'];

    protected $fillable = [
        'quote_no',
        'quote_date',
        'quote_user',
        'contact_name',
        'contact_phone',
        'contact_email',
        'company_name',
        'company_phone',
        'company_fax',
        'company_addr',
        'total_price',
        'remark',
        'warranty_remark',
    ];

    public function Details()
    {
        return $this->hasMany(QuotationD::class, 'm_id', 'id');
    }

    public function User()
    {
        return $this->belongsTo(User::class, 'quote_user', 'id');
    }

    protected static function boot()
    {
        parent::boot();

        //一併刪除編輯的資料
        static::deleting(function ($main) {
            $main->Details()->delete();
        });
    }
}
