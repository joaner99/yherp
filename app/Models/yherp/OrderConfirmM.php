<?php

namespace App\Models\yherp;

use App\User;
use App\Models\yherp\OrderConfirmD;
use Illuminate\Database\Eloquent\Model;

class OrderConfirmM extends Model
{
    //order_type 編碼
    public const order_type_list = [
        1 => '調撥申請',
        2 => '調撥處理',
        3 => '新轉銷單(輕物)',
        4 => '收料作業',
        5 => '新轉銷單(重物)',
        6 => '新轉銷單(全部)',
        7 => '新轉銷單(特殊)', //新轉銷單(親送、補寄貨來回件等等)
        8 => '銷貨加單',
        9 => '商品安庫',
        10 => '蝦皮宅配出貨已匯出',
        11 => '新竹偏遠地區',
        12 => '上車檢驗覆核',
        13 => '協商換貨', //需採購註記與客人協商換貨中
    ];

    protected $connection = 'mysql';
    protected $table = 'order_confirm_m';

    protected $fillable = [
        'user_id',
        'order_type',
        'remark',
    ];

    public function Details()
    {
        return $this->hasMany(OrderConfirmD::class, 'm_id', 'id');
    }

    public function User()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
