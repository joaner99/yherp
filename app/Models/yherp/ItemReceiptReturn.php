<?php

namespace App\Models\yherp;

use App\Models\TMS\Posein;
use App\Models\TMS\Poseou;
use Illuminate\Database\Eloquent\Model;
use App\Models\yherp\ItemReceiptReturnD;
use Illuminate\Database\Eloquent\SoftDeletes;

class ItemReceiptReturn extends Model
{
    use SoftDeletes;
    //平台來源
    public const package_status = [1 => '未取', 2 => '退貨', 3 => '高額訂單', 99 => '未定義'];

    protected $connection = 'mysql';
    protected $table = 'item_receipt_return';
    protected $appends = ['package_status_name'];
    protected $fillable = [
        'sale_no',
        'return_no',
        'package_status',
        'is_flaw',
        'remark',
        'completed_at'
    ];

    public function getPackageStatusNameAttribute()
    {
        return self::package_status[$this->package_status] ?? '';
    }

    public function Details()
    {
        return $this->hasMany(ItemReceiptReturnD::class, 'm_id', 'id');
    }

    public function Posein()
    {
        return $this->belongsTo(Posein::class, 'sale_no', 'PCOD1');
    }

    //退貨
    public function Poseou()
    {
        return $this->hasManyThrough(
            Poseou::class,
            Posein::class,
            'PCOD1',
            'PCOD2',
            'sale_no',
            'PCOD2',
        );
    }
}
