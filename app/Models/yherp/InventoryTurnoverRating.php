<?php

namespace App\Models\yherp;

use Illuminate\Database\Eloquent\Model;

class InventoryTurnoverRating extends Model
{
    protected $connection = 'mysql';
    protected $table = 'inventory_turnover_rating';
}
