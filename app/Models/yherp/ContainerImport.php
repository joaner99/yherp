<?php

namespace App\Models\yherp;

use Carbon\Carbon;
use App\Services\BaseService;
use Illuminate\Database\Eloquent\Model;
use App\Models\yherp\ContainerImportFiles;
use Illuminate\Database\Eloquent\SoftDeletes;

class ContainerImport extends Model
{
    use SoftDeletes;

    protected $connection = 'mysql';
    protected $table = 'container_import';

    protected $fillable = [
        'order_no',
        'ship_date',
        'remittance_type',
        'remittance_date',
        'remittance_date2',
        'remittance_account',
        'remittance_currency',
        'remittance_amount',
        'remark',
        'completed_at',
    ];

    public function getRemittanceTypeNameAttribute()
    {
        switch ($this->remittance_type) {
            case 1:
                return '匯款';
            case 2:
                return 'OA';
            default:
                return '';
        }
    }

    public function getCurrencyNameAttribute()
    {
        $currency = BaseService::currency;
        $val = $this->remittance_currency;
        if (empty($val) || !array_key_exists($val, $currency)) {
            return '';
        } else {
            return $currency[$val];
        }
    }

    public function getShipdateListAttribute()
    {
        if (empty($this->ship_date)) {
            return [];
        } else {
            return json_decode($this->ship_date);
        }
    }

    //是否可以結案
    public function getCanFinishAttribute()
    {
        $today = Carbon::today();
        //無付款日期 或 未超過付款日期 不得結案
        if (empty($this->remittance_date) || $today <= new Carbon($this->remittance_date)) {
            return false;
        } else {
            return true;;
        }
    }

    public function Files()
    {
        return $this->hasMany(ContainerImportFiles::class, 'm_id', 'id');
    }
}
