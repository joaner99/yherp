<?php

namespace App\Models\yherp;

use App\Models\yherp\County;
use Illuminate\Database\Eloquent\Model;

class CountyArea extends Model
{
    protected $connection = 'mysql';
    protected $table = 'county_area';

    protected $fillable = [
        '_code', 'name'
    ];

    public function County()
    {
        return $this->belongsTo(County::class, 'county_id', 'id');
    }
}
