<?php

namespace App\Models\yherp;

use App\Traits\Encrypt;
use App\Models\TMS\Order;
use App\Models\TMS\Posein;
use App\Services\BaseService;
use App\Models\yherp\OtherTransport;
use App\Services\OrderSampleService;
use App\Models\yherp\PersonalTransport;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderSampleM extends Model
{
    use SoftDeletes;
    use Encrypt;

    protected $connection = 'mysql';
    protected $table = 'order_sample_m';
    protected $encrypt = ['original_data', 'receiver_name', 'receiver_tel', 'receiver_address'];

    protected $appends = ['cust', 'order_type_name', 'order_kind_name'];

    public function Order()
    {
        return $this->hasMany(Order::class, 'OCOD4', 'order_no');
    }

    public function Posein()
    {
        return $this->hasMany(Posein::class, 'PJONO', 'order_no');
    }
    //親送資料
    public function PersonalTransport()
    {
        return $this->hasOne(PersonalTransport::class, 'original_no', 'order_no');
    }
    //人工資料
    public function OtherTransport()
    {
        return $this->hasOne(OtherTransport::class, 'original_no', 'order_no');
    }

    protected $fillable = [
        'src',
        'order_date',
        'order_no',
        'order_kind',
        'order_type',
        'trans',
        'remark',
        'estimated_date',
        'header',
        'original_data',
        'receiver_name',
        'receiver_tel',
        'receiver_address',
        'exported_at',
    ];

    public function getHeaderDataAttribute()
    {
        if (empty($this->header)) {
            return null;
        } else {
            return json_decode($this->header);
        }
    }

    //取得客戶代碼
    public function getCustAttribute()
    {
        switch ($this->src) {
            case '1': //shopline
                return 'S002';
            case '2': //蝦皮1店
                return 'S001';
            case '3': //蝦皮2店
                return 'S003';
            case '4': //蝦皮3店
                return 'S004';
            case '5': //BV SHOP
                return 'S006';
            default:
                return '';
        }
    }

    //來源名稱
    public function getSrcNameAttribute()
    {
        return OrderSampleService::SrcType[$this->src] ?? '';
    }

    //訂單類型
    public function getOrderKindNameAttribute()
    {
        return OrderSampleService::OrderKind[$this->order_kind] ?? '';
    }

    //預排物流名稱
    public function getTransNameAttribute()
    {
        return OrderSampleService::TransportType[$this->trans] ?? '';
    }

    //訂單狀態
    public function getOrderTypeNameAttribute()
    {
        if ($this->confirmed == 1) {
            $order_type_name = '已確認';
        } else {
            switch ($this->order_type) {
                case 0:
                    $order_type_name = '免確認';
                    break;
                case 1:
                default:
                    $order_type_name = '未確認';
                    break;
            }
        }
        return $order_type_name;
    }

    public function GetType(array $floor_items)
    {
        $header = json_decode($this->header);
        $columns = (new OrderSampleService())->GetOrderSampleColumns($header, $this->src);
        $original_data = json_decode($this->original_data);
        foreach ($original_data as $d) {
            $item_no = $d[$columns['item_no']];
            if (in_array($item_no, $floor_items)) { //地板
                return 1;
            }
        }
        $transport = $original_data[0][$columns['transport']];
        if (in_array($transport, BaseService::original_home_trans[$this->src])) { //宅配
            return 3;
        } else { //超商
            return 2;
        }
    }
}
