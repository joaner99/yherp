<?php

namespace App\Models\yherp;

use App\Models\TMS\Item;
use App\Models\TMS\Order;
use App\Models\TMS\STOC;
use App\Models\TMS\ITEM1;
use App\Models\yherp\ItemConfig;
use App\Models\yherp\LogStockD;
use Illuminate\Database\Eloquent\Model;

class LogOrderRequire extends Model
{
    protected $connection = 'mysql';
    protected $table = 'log_order_require';

    protected $fillable = [
        'order_no',
        'item_no',
        'item_name',
        'qty',
    ];

    public function STOC()
    {
        return $this->hasMany(STOC::class, 'SITEM', 'item_no');
    }

    public function Config()
    {
        return $this->hasOne(ItemConfig::class, 'id', 'item_no');
    }

    public function LogStock()
    {
        return $this->hasMany(LogStockD::class, 'item_no', 'item_no');
    }

    public function Item()
    {
        return $this->hasOne(Item::class, 'ICODE', 'item_no');
    }

    public function Item1()
    {
        return $this->hasOne(ITEM1::class, 'ICODE1', 'item_no');
    }

    public function Order()
    {
        return $this->belongsTo(Order::class, 'order_no', 'OCOD1');
    }
}
