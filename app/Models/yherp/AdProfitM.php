<?php

namespace App\Models\yherp;

use App\Services\AdService;
use App\Models\yherp\AdProfitD;
use Illuminate\Database\Eloquent\Model;

class AdProfitM extends Model
{
    protected $connection = 'mysql';
    protected $table = 'ad_profit_m';

    protected $fillable = [
        'src_date',
        'shop_id',
        'updated_at'
    ];

    public function getShopNameAttribute()
    {
        $shop_name = AdService::shopee_shop[$this->shop_id] ?? '';
        return $shop_name;
    }

    public function Details()
    {
        return $this->hasMany(AdProfitD::class, 'm_id', 'id');
    }
}
