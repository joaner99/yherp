<?php

namespace App\Models\yherp;

use Carbon\Carbon;
use App\Models\yherp\ProductErpM;
use App\Models\yherp\ProductSampleD;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductSampleM extends Model
{
    use SoftDeletes;

    private const file_type = ['1' => '網址', '2' => '檔案'];

    protected $connection = 'mysql';
    protected $table = 'product_sample_m';

    protected $fillable = [
        'status',
        'product_name',
        'estimated_date',
        'finished_date',
    ];

    protected static function boot()
    {
        parent::boot();

        static::deleting(function ($invoice) {
            foreach ($invoice->Details as $details) {
                $details->Files()->delete();
            }
            $invoice->Details()->delete();
        });
    }

    public function Details()
    {
        return $this->hasMany(ProductSampleD::class, 'm_id', 'id');
    }

    public function Erp()
    {
        return $this->hasOne(ProductErpM::class, 'sample_id', 'id');
    }

    public function getCreatedAtAttribute($value)
    {
        return (new Carbon($value))->format('Y-m-d');
    }

    public function getEstimatedDateAttribute($value)
    {
        return (new Carbon($value))->format('Y-m-d');
    }

    public function getFinishedDateAttribute($value)
    {
        if (empty($value)) {
            return '';
        } else {
            return (new Carbon($value))->format('Y-m-d');
        }
    }

    public function getStatusNameAttribute($value)
    {
        switch ($this->status) {
            case 101:
                return '採購建檔';
            case 201:
                return '廠務建檔';
            case 202:
                return '財務建檔';
            case 301:
                return '行銷';
            default:
                return '未定義';
        }
    }

    public function getStatusColorAttribute($value)
    {
        $s = floor($this->status / 100);
        switch ($s) {
            case 1: //採購
                return '#0069d9';
            case 2: //廠務/財務
                return '#955616b8';
            case 3: //行銷
                return '#28a745';
            default:
                return '#6c757d';
        }
    }
}
