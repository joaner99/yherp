<?php

namespace App\Models\yherp;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Calendar extends Model
{
    use SoftDeletes;

    protected $connection = 'mysql';
    protected $table = 'calendar';

    protected $fillable = [
        'kind',
        's_date',
        's_time',
        'e_date',
        'e_time',
        'user',
        'content'
    ];


    public function User()
    {
        return $this->belongsTo(User::class, 'user', 'id');
    }
}
