<?php

namespace App\Models\yherp;

use App\Models\yherp\AdChannel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AdCost extends Model
{
    use SoftDeletes;

    protected $connection = 'mysql';
    protected $table = 'ad_cost';

    protected $fillable = [
        'ad_channel_id',
        'src_date',
        'ad',
        'handling',
        'event',
        'freight',
        'freight_shopee',
    ];

    //渠道
    public function Channel()
    {
        return $this->belongsTo(AdChannel::class, 'ad_channel_id', 'id');
    }
}
