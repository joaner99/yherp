<?php

namespace App\Models\yherp;

use App\User;
use App\Models\TMS\Histin;
use App\Models\TMS\Posein;
use App\Models\yherp\LogShipment;
use App\Models\TMS\FreightCompany;
use Illuminate\Database\Eloquent\Model;

class LogPackage extends Model
{
    protected $connection = 'mysql';
    protected $table = 'log_package';

    protected $fillable =
    [
        'sale_no',
        'user_id',
        'machine_user_id',
        'package_date',
        'package_type',
        'seq',
        'tran_code',
        'tran_qty',
        'completed_at',
        'printed',
    ];

    public function User()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function Machine_User()
    {
        return $this->belongsTo(User::class, 'machine_user_id', 'id');
    }

    public function FreightCompany()
    {
        return $this->belongsTo(FreightCompany::class, 'tran_code', 'FCode');
    }

    public function Shipment()
    {
        return $this->hasOne(LogShipment::class, 'PCOD1', 'sale_no');
    }

    public function Posein()
    {
        return $this->belongsTo(Posein::class, 'sale_no', 'PCOD1');
    }

    public function Histin()
    {
        return $this->hasMany(Histin::class, 'HCOD1', 'sale_no');
    }
}
