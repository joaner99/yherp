<?php

namespace App\Models\yherp;

use App\Models\yherp\ProductErpM;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductErpD extends Model
{
    use SoftDeletes;

    protected $connection = 'mysql';
    protected $table = 'product_erp_d';

    protected $fillable = [
        'ITCOD', 'ITCO2', 'ITCO3', 'IPOIN', 'ICONS', 'INAME', 'IINAME', 'T11_11', 'SiseCost',
        'warranty_y', 'warranty_m', 'warranty_d',
    ];

    public function Main()
    {
        return $this->belongsTo(ProductErpM::class, 'm_id', 'id');
    }
}
