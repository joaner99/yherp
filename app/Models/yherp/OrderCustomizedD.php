<?php

namespace App\Models\yherp;

use App\Models\yherp\OrderCustomizedM;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderCustomizedD extends Model
{
    use SoftDeletes;

    protected $connection = 'mysql';
    protected $table = 'order_customized_d';

    protected $fillable = [
        'item_no', 'item_qty', 'item_price', 'item_remark'
    ];

    public function Main()
    {
        return $this->belongsTo(OrderCustomizedM::class, 'm_id', 'id');
    }
}
