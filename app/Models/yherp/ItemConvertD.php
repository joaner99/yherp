<?php

namespace App\Models\yherp;

use App\Models\TMS\Item;
use App\Models\yherp\ItemConvertM;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ItemConvertD extends Model
{
    use SoftDeletes;

    protected $connection = 'mysql';
    protected $table = 'item_convert_d';

    protected $fillable = [
        'src_item_no',
        'item_no',
        'e_date'
    ];

    public function Main()
    {
        return $this->belongsTo(ItemConvertM::class, 'm_id', 'id');
    }

    public function SrcItem()
    {
        return $this->belongsTo(Item::class, 'src_item_no', 'ICODE');
    }

    public function Item()
    {
        return $this->belongsTo(Item::class, 'item_no', 'ICODE');
    }
}
