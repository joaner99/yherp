<?php

namespace App\Models\yherp;

use App\Models\yherp\CreditNoteM;
use Illuminate\Database\Eloquent\Model;

class CreditNoteD extends Model
{
    protected $connection = 'mysql';
    protected $table = 'credit_note_d';

    protected $fillable = ['item_name', 'qty', 'unit', 'total', 'tax'];

    public function Main()
    {
        return $this->belongsTo(CreditNoteM::class, 'm_id', 'id');
    }
}
