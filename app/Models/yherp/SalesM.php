<?php

namespace App\Models\yherp;

use App\User;
use App\Models\yherp\SalesD;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SalesM extends Model
{
    use SoftDeletes;

    protected $connection = 'YoHouse2';
    protected $table = 'sales_m';
    public $incrementing = false;
    protected $keyType = 'string';

    protected $fillable = [
        'id',
        'user_id',
        'sale_date',
        'total_price_tax'
    ];

    public function Details()
    {
        return $this->hasMany(SalesD::class, 'm_id', 'id');
    }

    public function User()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
