<?php

namespace App\Models\yherp;

use App\Models\yherp\HelpQuestions;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class HelpQuestionItem extends Model
{
    use SoftDeletes;

    protected $connection = 'mysql';
    protected $table = 'help_question_item';

    protected $fillable = [
        'item_no',
    ];

    public function Questions()
    {
        return $this->belongsTo(HelpQuestions::class, 'question_id', 'id');
    }
}
