<?php

namespace App\Models\yherp;

use App\Models\yherp\InventoryD;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class InventoryM extends Model
{
    use SoftDeletes;

    protected $connection = 'mysql';
    protected $table = 'inventory_m';

    protected $fillable = [
        'appointment_date',
        'inverntory_time',
        'completed_at',
        'remark'
    ];

    public function Details()
    {
        return $this->hasMany(InventoryD::class, 'm_id', 'id');
    }
}
