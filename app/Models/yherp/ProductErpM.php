<?php

namespace App\Models\yherp;

use Carbon\Carbon;
use App\Models\yherp\ProductErpD;
use App\Models\yherp\ProductSampleM;
use Illuminate\Database\Eloquent\Model;

class ProductErpM extends Model
{
    protected $connection = 'mysql';
    protected $table = 'product_erp_m';

    protected $fillable = [
        'finished_date',
        'finished_date_2',
    ];

    public function Details()
    {
        return $this->hasMany(ProductErpD::class, 'm_id', 'id');
    }

    public function Sample()
    {
        return $this->belongsTo(ProductSampleM::class, 'sample_id', 'id');
    }

    public function getCreatedAtAttribute($value)
    {
        return (new Carbon($value))->format('Y-m-d');
    }
}
