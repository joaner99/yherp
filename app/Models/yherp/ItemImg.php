<?php

namespace App\Models\yherp;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ItemImg extends Model
{
    use SoftDeletes;

    protected $connection = 'mysql';
    protected $table = 'item_img';

    protected $fillable = [
        'icode',
        'path',
    ];
}
