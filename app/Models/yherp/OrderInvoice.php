<?php

namespace App\Models\yherp;

use App\Models\TMS\Posein;
use App\Models\TMS\Poseou;
use App\Models\yherp\CreditNoteM;
use Illuminate\Database\Eloquent\Model;

class OrderInvoice extends Model
{
    public const type = [0 => '發票開立', 1 => '發票作廢'];

    protected $connection = 'mysql';
    protected $table = 'order_invoice';
    protected $appends = ['type_name'];

    protected $fillable = [
        '_type',
        'invoice_no',
        'order_no'
    ];

    public function getTypeNameAttribute()
    {
        return self::type[$this->_type] ?? '未定義';
    }

    //折讓
    public function CreditNoteM()
    {
        return $this->hasMany(CreditNoteM::class, 'invoice_no', 'invoice_no');
    }

    //銷貨
    public function Posein()
    {
        return $this->hasMany(Posein::class, 'PJONO', 'order_no');
    }

    //退貨
    public function Poseou()
    {
        return $this->hasMany(Poseou::class, 'PJONO', 'order_no');
    }
}
