<?php

namespace App\Models\yherp;

use Illuminate\Database\Eloquent\Model;

class LogShipment extends Model
{
	protected $connection = 'mysql';
	protected $table = 'log_shipment';

	protected $fillable =
	[
		'PCOD1',
		'PDATE',
		'status'
	];
}
