<?php

namespace App\Models\yherp;

use App\Models\yherp\ItemMarketingD;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ItemMarketingM extends Model
{
    use SoftDeletes;

    protected $connection = 'mysql';
    protected $table = 'item_marketing_m';

    protected $fillable = [
        'marketing',
        'remark',
        's_dt',
        'e_dt',
    ];

    public function Details()
    {
        return $this->hasMany(ItemMarketingD::class, 'm_id', 'id');
    }
}
