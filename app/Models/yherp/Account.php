<?php

namespace App\Models\yherp;

use App\Traits\Encrypt;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Account extends Model
{
    use SoftDeletes;
    use Encrypt;

    protected $connection = 'mysql';
    protected $table = 'account';
    protected $encrypt = ['password'];

    protected $fillable = [
        'name',
        'account',
        'password',
        'remark',
    ];
}
