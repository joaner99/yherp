<?php

namespace App\Models\yherp;

use App\Services\AdService;
use Illuminate\Database\Eloquent\Model;

class AdItem extends Model
{
    protected $connection = 'mysql';
    protected $table = 'ad_item';

    protected $fillable = [
        'ShopId',
        'item_id',
        'ICODE'
    ];

    public function getShopNameAttribute()
    {
        $shop_name = AdService::shopee_shop[$this->ShopId] ?? '';
        return $shop_name;
    }
}
