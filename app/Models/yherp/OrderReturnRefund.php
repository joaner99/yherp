<?php

namespace App\Models\yherp;

use App\Models\TMS\CUST;
use Illuminate\Database\Eloquent\Model;

class OrderReturnRefund extends Model
{
    protected $connection = 'mysql';
    protected $table = 'order_return_refund';
    protected $appends = ['group', 'group_name'];

    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;

    protected $fillable = [
        'id',
        'cust',
        'order_no',
        'order_time',
        'return_time',
        'return_status',
        'dispute_status'
    ];

    public function getGroupAttribute()
    {
        if ($this->dispute_status == '爭議待申請') {
            return 2;
        } else {
            switch ($this->return_status) {
                case '申請等待審核中':
                case '等待退貨中':
                case '退貨進行中':
                    return 2;
                case '蝦皮審核中':
                    return 3;
                case '已同意申請':
                case '申請已取消':
                    return 4;
                default:
                    return 0;
            }
        }
    }

    public function getGroupNameAttribute()
    {
        switch ($this->group) {
            case 0:
                return '尚未定義';
            case 1:
                return '蝦皮審核中';
            case 2:
                return '待回覆';
            case 3:
                return '已回覆';
            case 4:
                return '已完成';
        }
    }

    public function Cust()
    {
        return $this->belongsTo(CUST::class, 'cust', 'CCODE');
    }
}
