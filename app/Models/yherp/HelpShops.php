<?php

namespace App\Models\yherp;

use App\Models\yherp\HelpQuestions;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class HelpShops extends Model
{
    use SoftDeletes;

    protected $connection = 'mysql';
    protected $table = 'help_shops';

    protected $fillable = [
        'name',
    ];

    public function Questions()
    {
        return $this->hasMany(HelpQuestions::class, 'shop_id', 'id');
    }
}
