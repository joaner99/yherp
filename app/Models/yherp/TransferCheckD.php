<?php

namespace App\Models\yherp;

use App\Models\yherp\LogStockD;
use App\Models\yherp\TransferCheckM;
use Illuminate\Database\Eloquent\Model;

class TransferCheckD extends Model
{
    protected $connection = 'mysql';
    protected $table = 'transfer_check_d';

    protected $fillable = [
        'log_stock_id'
    ];

    public function Main()
    {
        return $this->belongsTo(TransferCheckM::class, 'm_id', 'id');
    }

    public function LogStock()
    {
        return $this->belongsTo(LogStockD::class, 'log_stock_id', 'id');
    }
}
