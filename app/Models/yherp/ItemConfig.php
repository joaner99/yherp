<?php

namespace App\Models\yherp;

use App\Models\TMS\STOC;
use App\Models\yherp\ItemPlaceD;
use App\Models\yherp\LogOrderRequire;
use Illuminate\Database\Eloquent\Model;
use App\Models\yherp\LogSafetyStockExport;
use App\Models\yherp\InventoryTurnoverRating;
use Illuminate\Database\Eloquent\SoftDeletes;

class ItemConfig extends Model
{
    use SoftDeletes;

    protected $connection = 'mysql';
    protected $table = 'item_config';
    public $incrementing = false;
    protected $keyType = 'string';

    protected $fillable = [
        'id',
        'name',
        'main_ss',
        'sale_ss',
        'disable_ss',
        'disable_gpm',
        'box_qty',
        'purchase_qty',
        'cost',
        'rating_id',
    ];

    public function Rating()
    {
        return $this->belongsTo(InventoryTurnoverRating::class, 'rating_id', 'id');
    }

    public function STOC()
    {
        return $this->hasMany(STOC::class, 'SITEM', 'id');
    }

    public function ItemPlaceD()
    {
        return $this->hasMany(ItemPlaceD::class, 'item_no', 'id');
    }

    public function LogOrderRequire()
    {
        return $this->hasMany(LogOrderRequire::class, 'item_no', 'id');
    }

    public function LogSafetyStockExport()
    {
        return $this->hasMany(LogSafetyStockExport::class, 'item_no', 'id');
    }
}
