<?php

namespace App\Models\yherp;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Attendance extends Model
{
    protected $connection = 'mysql';
    protected $table = 'attendance';

    protected $fillable = [
        'user_id',
        'card_time',
        'uploaded_at',
    ];

    public function User()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
