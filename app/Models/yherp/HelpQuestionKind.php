<?php

namespace App\Models\yherp;

use App\Models\yherp\HelpQuestions;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class HelpQuestionKind extends Model
{
    use SoftDeletes;

    protected $connection = 'mysql';
    protected $table = 'help_question_kind';

    protected $fillable = [
        'kind',
        'kind2',
        'kind3',
    ];

    public function Questions()
    {
        return $this->belongsTo(HelpQuestions::class, 'question_id', 'id');
    }
}
