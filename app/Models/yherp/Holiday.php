<?php

namespace App\Models\yherp;

use Illuminate\Database\Eloquent\Model;

class Holiday extends Model
{
    protected $connection = 'mysql';
    protected $table = 'holiday';

    protected $fillable = [
        'date',
        'year',
        'name',
        'isholiday',
        'holidaycategory',
        'description',
    ];
}
