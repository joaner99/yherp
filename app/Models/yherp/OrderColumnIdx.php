<?php

namespace App\Models\yherp;

use Illuminate\Database\Eloquent\Model;

class OrderColumnIdx extends Model
{
    protected $connection = 'mysql';
    protected $table = 'order_column_idx';

    protected $fillable = [
        'key',
        'remark',
        'shopee_idx',
        'shopline_idx',
        'bvshop_idx',
    ];
}
