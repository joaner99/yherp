<?php

namespace App\Models\yherp;

use App\Models\yherp\OrderConfirmM;
use Illuminate\Database\Eloquent\Model;

class OrderConfirmD extends Model
{
    protected $connection = 'mysql';
    protected $table = 'order_confirm_d';

    protected $fillable = [
        'order_no',
        'order_date',
    ];

    public function Main()
    {
        return $this->belongsTo(OrderConfirmM::class, 'm_id', 'id');
    }
}
