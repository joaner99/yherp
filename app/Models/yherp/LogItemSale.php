<?php

namespace App\Models\yherp;

use App\Models\TMS\STOC;
use App\Models\yherp\ItemPlaceD;
use App\Models\yherp\SafetyStock;
use App\Models\yherp\LogOrderRequire;
use Illuminate\Database\Eloquent\Model;

class LogItemSale extends Model
{
    protected $connection = 'mysql';
    protected $table = 'log_item_sale';
    public $incrementing = false;
    protected $keyType = 'string';

    protected $fillable = [
        'id',
        'sales',
        'ratio',
    ];

    public function SafetyStock()
    {
        return $this->hasOne(SafetyStock::class, 'item_no', 'id');
    }

    public function STOC()
    {
        return $this->hasMany(STOC::class, 'SITEM', 'id');
    }

    public function ItemPlaceD()
    {
        return $this->hasMany(ItemPlaceD::class, 'item_no', 'id');
    }

    public function LogOrderRequire()
    {
        return $this->hasMany(LogOrderRequire::class, 'item_no', 'id');
    }
}
