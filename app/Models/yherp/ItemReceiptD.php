<?php

namespace App\Models\yherp;

use App\Models\TMS\Item;
use App\Models\yherp\ItemReceiptM;
use Illuminate\Database\Eloquent\Model;

class ItemReceiptD extends Model
{
    protected $connection = 'mysql';
    protected $table = 'item_receipt_d';

    protected $fillable = [
        'item_no',
        'item_name',
        'qty',
    ];

    public function Main()
    {
        return $this->belongsTo(ItemReceiptM::class, 'm_id', 'id');
    }

    public function Item()
    {
        return $this->belongsTo(Item::class, 'item_no', 'ICODE');
    }
}
