<?php

namespace App\Models\yherp;

use Illuminate\Database\Eloquent\Model;

class CinForest extends Model
{
    protected $connection = 'mysql';
    protected $table = 'cinforest';

    protected $primaryKey = 'PCOD1';
    protected $keyType = 'string';
    public $incrementing = false;

    protected $fillable = [
        'PCOD1',
        'ConsignTran'
    ];
}
