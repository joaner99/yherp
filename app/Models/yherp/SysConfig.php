<?php

namespace App\Models\yherp;

use Illuminate\Database\Eloquent\Model;

class SysConfig extends Model
{
    protected $connection = 'mysql';
    protected $table = 'sys_config';

    protected $fillable = [
        'kind', 'config', 'note', 'enabled'
    ];
}
