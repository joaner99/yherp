<?php

namespace App\Models\yherp;

use App\User;
use App\Services\StockService;
use App\Models\yherp\LogStockD;
use App\Models\yherp\ItemPlaceM;
use Illuminate\Database\Eloquent\Model;

class LogStockM extends Model
{
    protected $connection = 'mysql';
    protected $table = 'log_stock_m';

    protected $fillable = [
        'user_id',
        'p_id',
        'log_type',
        'log_kind'
    ];

    public function Place()
    {
        return $this->belongsTo(ItemPlaceM::class, 'p_id', 'id');
    }

    public function User()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function Details()
    {
        return $this->hasMany(LogStockD::class, 'm_id', 'id');
    }

    public function getLogTypeNameAttribute()
    {
        return StockService::log_type[$this->log_type] ?? '未知';
    }

    public function getLogKindNameAttribute()
    {
        $log_kind_name = '';
        switch ($this->log_type) {
            case 1: //入庫
                if ($this->log_kind == 1) {
                    $log_kind_name = '移轉作業';
                }
                break;
            case 2: //出庫
                if ($this->log_kind == 1) {
                    $log_kind_name = '移轉作業';
                } elseif ($this->log_kind == 2) {
                    $log_kind_name = '調撥作業';
                }
                break;
            default:
                break;
        }
        return $log_kind_name;
    }
}
