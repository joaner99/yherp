<?php

namespace App\Models\yherp;

use Illuminate\Database\Eloquent\Model;

class ScheduleLog extends Model
{
    protected $connection = 'mysql';
    protected $table = 'schedule_log';
    public $timestamps = false;
}
