<?php

namespace App\Models\yherp;

use App\Models\yherp\SalesM;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SalesD extends Model
{
    use SoftDeletes;

    protected $connection = 'YoHouse2';
    protected $table = 'sales_d';

    protected $fillable = [
        'm_id',
        'item_no',
        'item_name',
        'item_qty',
        'unit_price_tax',
        'total_price_tax',
    ];

    public function Main()
    {
        return $this->belongsTo(SalesM::class, 'm_id', 'id');
    }
}
