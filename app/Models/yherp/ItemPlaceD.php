<?php

namespace App\Models\yherp;

use App\Models\TMS\Item;
use App\Models\TMS\STOC;
use App\Models\TMS\ITEM1;
use App\Models\TMS\ITEM3;
use App\Models\yherp\ItemPlaceM;
use Illuminate\Database\Eloquent\Model;

class ItemPlaceD extends Model
{
    protected $connection = 'mysql';
    protected $table = 'item_place_d';
    protected $keyType = 'string';

    protected $fillable = [
        'm_id',
        'item_no',
        'item_name',
        'qty',
    ];

    public function Main()
    {
        return $this->belongsTo(ItemPlaceM::class, 'm_id', 'id');
    }

    public function Item()
    {
        return $this->belongsTo(Item::class, 'item_no', 'ICODE');
    }

    public function Item1()
    {
        return $this->belongsTo(ITEM1::class, 'item_no', 'ICODE1');
    }

    public function Item3()
    {
        return $this->belongsTo(ITEM3::class, 'item_no', 'ICODE3');
    }

    public function STOC()
    {
        return $this->hasMany(STOC::class, 'SITEM', 'item_no');
    }
}
