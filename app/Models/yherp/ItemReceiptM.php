<?php

namespace App\Models\yherp;

use App\User;
use App\Models\yherp\ItemReceiptD;
use Illuminate\Database\Eloquent\Model;

class ItemReceiptM extends Model
{
    protected $connection = 'mysql';
    protected $table = 'item_receipt_m';

    protected $fillable = [
        'user_id',
        'created_at',
    ];

    public function User()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function Details()
    {
        return $this->hasMany(ItemReceiptD::class, 'm_id', 'id');
    }
}
