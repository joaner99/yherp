<?php

namespace App\Models\yherp;

use App\Models\yherp\OrderShipmentStatusD;
use Illuminate\Database\Eloquent\Model;

class OrderShipmentStatusM extends Model
{
    public const gpm_check_result_type = [0 => '', 1 => '正常', 2 => '採購', 3 => '行銷'];

    protected $connection = 'mysql';
    protected $table = 'order_shipment_status_m';
    protected $appends = ['gpm_check_result_name'];

    public function getGpmCheckResultNameAttribute()
    {
        return self::gpm_check_result_type[$this->gpm_check_result] ?? '';
    }

    protected $fillable = [
        'sales_no',
        'note_check',
        'gpm_check',
        'gpm_check_result',
        'shipment_check',
        'extimated_date',
    ];

    public function Details()
    {
        return $this->hasMany(OrderShipmentStatusD::class, 'm_id', 'id');
    }
}
