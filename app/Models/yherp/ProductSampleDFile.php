<?php

namespace App\Models\yherp;

use App\Models\yherp\ProductSampleD;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductSampleDFile extends Model
{
    use SoftDeletes;

    protected $connection = 'mysql';
    protected $table = 'product_sample_d_file';

    protected $fillable = [
        'type',
        'title',
        'content',
    ];

    public function Main()
    {
        return $this->belongsTo(ProductSampleD::class, 'm_id', 'id');
    }
}
