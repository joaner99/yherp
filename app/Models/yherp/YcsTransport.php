<?php

namespace App\Models\yherp;

use App\Models\TMS\Posein;
use App\Traits\Encrypt;
use App\Models\yherp\LogShipment;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class YcsTransport extends Model
{
    use Encrypt;
    use SoftDeletes;

    protected $connection = 'mysql';
    protected $table = 'ycs_transport';
    protected $encrypt = ['receiver', 'addr', 'tel', 'remark'];

    protected $fillable = [
        'settle_date',
        'sale_no',
        'qty',
        'size',
        'weight',
        'name',
        'receiver',
        'addr',
        'tel',
        'remark',
        'systemNumber',
    ];

    public function shipment()
    {
        return $this->hasMany(LogShipment::class, 'PCOD1', 'sale_no');
    }

    public function Posein()
    {
        return $this->hasOne(Posein::class, 'PCOD1', 'sale_no');
    }
}
