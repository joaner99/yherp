<?php

namespace App\Models\yherp;

use App\Models\yherp\HelpShops;
use App\Models\yherp\HelpGroups;
use App\Models\yherp\HelpActions;
use App\Models\yherp\HelpQuestionImg;
use App\Models\yherp\HelpQuestionItem;
use App\Models\yherp\HelpQuestionKind;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class HelpQuestions extends Model
{
    use SoftDeletes;

    protected $connection = 'mysql';
    protected $table = 'help_questions';

    protected $fillable = [
        'group_id',
        'content',
        'answer',
        'remark',
    ];

    public function Kinds()
    {
        return $this->hasMany(HelpQuestionKind::class, 'question_id', 'id');
    }

    public function Items()
    {
        return $this->hasMany(HelpQuestionItem::class, 'question_id', 'id');
    }

    public function Imgs()
    {
        return $this->hasMany(HelpQuestionImg::class, 'question_id', 'id');
    }

    public function Group()
    {
        return $this->belongsTo(HelpGroups::class, 'group_id', 'id');
    }

    public function Shops()
    {
        return $this->belongsToMany(HelpShops::class, 'help_question_shop', 'question_id', 'shop_id');
    }

    public function Actions()
    {
        return $this->belongsToMany(HelpActions::class, 'help_question_action', 'question_id', 'action_id');
    }
}
