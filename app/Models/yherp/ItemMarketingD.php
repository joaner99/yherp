<?php

namespace App\Models\yherp;

use App\Models\yherp\ItemMarketingM;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ItemMarketingD extends Model
{
    use SoftDeletes;

    protected $connection = 'mysql';
    protected $table = 'item_marketing_d';

    protected $fillable = [
        'item_no',
        'item_name',
    ];

    public function Main()
    {
        return $this->belongsTo(ItemMarketingM::class, 'm_id', 'id');
    }
}
