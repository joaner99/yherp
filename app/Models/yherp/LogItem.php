<?php

namespace App\Models\yherp;

use App\Models\TMS\Item;
use Illuminate\Database\Eloquent\Model;

class LogItem extends Model
{
    protected $connection = 'mysql';
    protected $table = 'log_item';

    protected $fillable = [
        'ICODE',
        'INUMB',
        'ISOUR',
        'item_no',
    ];

    public function ITEM()
    {
        return $this->belongsTo(Item::class, 'ICODE', 'ICODE');
    }
}
