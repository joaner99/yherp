<?php

namespace App\Models\yherp;

use Illuminate\Database\Eloquent\Model;

class AdChannel extends Model
{
    protected $connection = 'mysql';
    protected $table = 'ad_channel';

    protected $fillable = [
        'name'
    ];
}
