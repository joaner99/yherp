<?php

namespace App\Models\yherp;

use App\Models\yherp\SysConfigD;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SysConfigM extends Model
{
    use SoftDeletes;

    protected $connection = 'mysql';
    protected $table = 'sys_config_m';

    protected $fillable = [
        '_key', 'remark', 'value1_description', 'value2_description', 'value3_description'
    ];

    public function Details()
    {
        return $this->hasMany(SysConfigD::class, 'm_id', 'id');
    }
}
