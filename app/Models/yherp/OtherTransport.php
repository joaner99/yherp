<?php

namespace App\Models\yherp;

use App\Models\yherp\OrderSampleM;
use Illuminate\Database\Eloquent\Model;

class OtherTransport extends Model
{
    protected $connection = 'mysql';
    protected $table = 'other_transport';

    protected $fillable = [
        'original_no',
        'ship_date',
        'remark',
    ];

    public function OrderSample()
    {
        return $this->belongsTo(OrderSampleM::class, 'original_no', 'order_no');
    }

    public function Order()
    {
        return $this->belongsTo(OrderSampleM::class, 'original_no', 'order_no');
    }
}
