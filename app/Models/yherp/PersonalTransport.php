<?php

namespace App\Models\yherp;

use App\User;
use App\Models\TMS\Order;
use App\Models\yherp\Cars;
use App\Models\yherp\OrderSampleM;
use Illuminate\Database\Eloquent\Model;

class PersonalTransport extends Model
{
    protected $connection = 'mysql';
    protected $table = 'personal_transport';

    protected $fillable = [
        'original_no',
        'ship_date',
        'driver',
        'car',
        'remark',
        'additional_addr',
        'cust_confirmed',
    ];

    public function OrderSample()
    {
        return $this->belongsTo(OrderSampleM::class, 'original_no', 'order_no');
    }

    public function Order()
    {
        return $this->belongsTo(Order::class, 'original_no', 'OCOD4');
    }

    public function User()
    {
        return $this->belongsTo(User::class, 'driver', 'id');
    }

    public function Car()
    {
        return $this->belongsTo(Cars::class, 'car', 'id');
    }
}
