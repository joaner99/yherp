<?php

namespace App\Models\yherp;

use App\Models\yherp\SysConfigM;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SysConfigD extends Model
{
    use SoftDeletes;

    protected $connection = 'mysql';
    protected $table = 'sys_config_d';

    protected $fillable = [
        'value1', 'value2', 'value3',
    ];

    public function Main()
    {
        return $this->belongsTo(SysConfigM::class, 'm_id', 'id');
    }
}
