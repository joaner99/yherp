<?php

namespace App\Models\yherp;

use Illuminate\Database\Eloquent\Model;

class LogPickingPrint extends Model
{
    protected $connection = 'mysql';
    protected $table = 'log_picking_print';

    protected $fillable =
    [
        'PCOD1',
    ];
}
