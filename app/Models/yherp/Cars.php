<?php

namespace App\Models\yherp;

use Illuminate\Database\Eloquent\Model;

class Cars extends Model
{
    protected $connection = 'mysql';
    protected $table = 'cars';
    protected $appends = ['full_name'];

    protected $fillable = ['no', 'name'];

    public function getFullNameAttribute()
    {
        return "【{$this->no}】{$this->name}";
    }
}
