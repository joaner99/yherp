<?php

namespace App\Models\yherp;

use App\Traits\Encrypt;
use App\Models\TMS\Order;
use Illuminate\Database\Eloquent\Model;

class CustomerComplaint extends Model
{
    use Encrypt;

    protected $connection = 'mysql';
    protected $table = 'customer_complaint';
    protected $fillable = ['order_no', 'origin_no', 'remark', 'status', 'mark', 'urgent'];
    protected $encrypt = ['remark'];

    //訂單
    public function Order()
    {
        return $this->belongsTo(Order::class, 'order_no', 'OCOD1');
    }
}
