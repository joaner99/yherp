<?php

namespace App\Models\yherp;

use App\Models\yherp\County;
use App\Models\yherp\CountyArea;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LogisticsAddrMark extends Model
{
    use SoftDeletes;

    protected $connection = 'mysql';
    protected $table = 'logistics_addr_mark';

    protected $fillable = [
        'kind', 'county_id', 'area_id'
    ];

    public function County()
    {
        return $this->belongsTo(County::class, 'county_id', 'id');
    }

    public function Area()
    {
        return $this->belongsTo(CountyArea::class, 'area_id', 'id');
    }
}
