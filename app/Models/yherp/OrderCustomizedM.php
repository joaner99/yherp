<?php

namespace App\Models\yherp;

use App\Traits\Encrypt;
use App\Models\yherp\OrderCustomizedD;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderCustomizedM extends Model
{
    use SoftDeletes;
    use Encrypt;

    protected $connection = 'mysql';
    protected $table = 'order_customized_m';
    protected $encrypt = ['remark', 'remark2'];

    protected $fillable = [
        'order_date', 'customer', 'salesman', 'order_no', 'remark', 'remark2', 'completed', 'into_erp'
    ];

    public function Details()
    {
        return $this->hasMany(OrderCustomizedD::class, 'm_id', 'id');
    }
}
