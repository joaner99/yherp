<?php

namespace App\Models\yherp;

use App\Models\yherp\ContainerImport;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ContainerImportFiles extends Model
{
    use SoftDeletes;

    protected $connection = 'mysql';
    protected $table = 'container_import_files';

    protected $fillable = [
        'img_base64',
    ];

    public function Main()
    {
        return $this->belongsTo(ContainerImport::class, 'm_id', 'id');
    }
}
