<?php

namespace App\Models\yherp;

use App\Models\yherp\AdItem;
use App\Models\yherp\AdProfitM;
use App\Models\TMS\ApiShopeeProduct;
use Illuminate\Database\Eloquent\Model;

class AdProfitD extends Model
{
    protected $connection = 'mysql';
    protected $table = 'ad_profit_d';

    protected $fillable = [
        'seq',
        'item_name',
        '_status',
        'item_id',
        'ad_type',
        'keyword',
        's_time',
        'e_time',
        'view_count',
        'click_count',
        'click_rate',
        'convert1',
        'convert2',
        'convert1_rate',
        'convert2_rate',
        'convert1_cost',
        'convert2_cost',
        'sales1_qty',
        'sales2_qty',
        'sales1_amt',
        'sales2_amt',
        'cost',
        'roas1',
        'roas2',
        'acos1',
        'acos2',
        'item_view_count',
        'item_click_count',
        'item_click_rate',
    ];

    public function Main()
    {
        return $this->belongsTo(AdProfitM::class, 'm_id', 'id');
    }

    public function TMSItems()
    {
        return $this->hasMany(ApiShopeeProduct::class, 'item_id', 'item_id');
    }

    public function Shopee2TMSItems()
    {
        return $this->hasMany(AdItem::class, 'item_id', 'item_id');
    }
}
