<?php

namespace App\Models\yherp;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\yherp\ProductProposalNegativeReview;

class ProductProposal extends Model
{
    public const status_list = [
        0 => '未定義',
        1 => '行銷審核中',
        2 => '行銷審核未通過',
        3 => '採購審核中',
        4 => '採購審核未通過',
        5 => '樣品採買中',
        6 => '樣品未通過',
        7 => '正式上架',
    ];
    public const tiers = ['', 'A', 'B', 'C'];
    use SoftDeletes;

    protected $connection = 'mysql';
    protected $table = 'product_proposal';
    protected $appends = ['status_name', 'date'];

    protected $fillable = [
        'user',
        'status',
        'bonus',
        'name',
        'other_nr',
        'url',
        'kind',
        'kind_name',
        'img_base64',
        'tier'
    ];

    public function getStatusNameAttribute()
    {
        return self::status_list[$this->status] ?? '';
    }

    public function getDateAttribute()
    {
        return substr($this->created_at, 0, 10);
    }

    public function User()
    {
        return $this->belongsTo(User::class, 'user', 'id');
    }

    public function NR()
    {
        return $this->hasMany(ProductProposalNegativeReview::class, 'm_id', 'id');
    }
}
