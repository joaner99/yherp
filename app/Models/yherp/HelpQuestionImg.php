<?php

namespace App\Models\yherp;

use App\Models\yherp\HelpQuestions;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class HelpQuestionImg extends Model
{
    use SoftDeletes;

    protected $connection = 'mysql';
    protected $table = 'help_question_img';

    protected $fillable = [
        'path',
    ];

    public function Questions()
    {
        return $this->belongsTo(HelpQuestions::class, 'question_id', 'id');
    }
}
