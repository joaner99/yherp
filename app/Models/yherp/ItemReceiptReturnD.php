<?php

namespace App\Models\yherp;

use App\Models\yherp\ItemReceiptReturn;
use Illuminate\Database\Eloquent\Model;

class ItemReceiptReturnD extends Model
{
    protected $connection = 'mysql';
    protected $table = 'item_receipt_return_d';
    protected $fillable = ['item_no', 'item_name', 'qty', 'good_qty', 'bad_qty', 'lost_qty'];

    public function Main()
    {
        return $this->belongsTo(ItemReceiptReturn::class, 'm_id', 'id');
    }
}
