<?php

namespace App\Models\yherp;

use App\Models\TMS\PART;
use App\Models\yherp\ProductSampleM;
use App\Models\yherp\ProductSampleDFile;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductSampleD extends Model
{
    use SoftDeletes;

    protected $connection = 'mysql';
    protected $table = 'product_sample_d';

    protected $fillable = [
        'supplier',
        'product_name',
        'original_name',
        'batch_price',
        'package_price',
        'ch_freight',
        'tw_freight',
        'shopee_price',
        'package_w',
        'package_h',
        'package_d',
        'package_qty',
        'container_cbm',
        'export',
        'T11_11',
        'remark'
    ];

    protected static function boot()
    {
        parent::boot();

        static::deleting(function ($invoice) {
            $invoice->Files()->delete();
        });
    }

    public function Main()
    {
        return $this->belongsTo(ProductSampleM::class, 'm_id', 'id');
    }

    public function Files()
    {
        return $this->hasMany(ProductSampleDFile::class, 'm_id', 'id');
    }

    public function Supplier()
    {
        return $this->hasOne(PART::class, 'PCODE', 'supplier');
    }

    public function getTotalAttribute()
    {
        return (($this->batch_price + $this->ch_freight + $this->package_price) * 4.5 + $this->tw_freight) * 1.05;
    }

    public function getCBMAttribute()
    {
        $result = round($this->package_d * $this->package_w * $this->package_h / 1000000, 4);
        return $result;
    }
}
