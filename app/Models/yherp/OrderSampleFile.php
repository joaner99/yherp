<?php

namespace App\Models\yherp;

use App\Traits\Encrypt;
use App\Services\OrderSampleService;
use Illuminate\Database\Eloquent\Model;

class OrderSampleFile extends Model
{
    use Encrypt;

    protected $connection = 'mysql';
    protected $table = 'order_sample_file';
    public $timestamps = false;
    protected $encrypt = ['convert_file'];

    protected $fillable = [
        'start_date',
        'end_date',
        'src',
        'convert_type',
        'convert_file',
    ];

    public function getSrcNameAttribute()
    {
        return OrderSampleService::SrcType[$this->src] ?? '';
    }
}
