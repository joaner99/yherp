<?php

namespace App\Models\yherp;

use Illuminate\Database\Eloquent\Model;

class PersonalTransportSche extends Model
{
    protected $connection = 'mysql';
    protected $table = 'personal_transport_sche';

    protected $fillable = [
        'ship_date',
        'driver',
        'distance',
        'duration',
    ];
}
