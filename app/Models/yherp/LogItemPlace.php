<?php

namespace App\Models\yherp;

use Illuminate\Database\Eloquent\Model;

class LogItemPlace extends Model
{
    protected $connection = 'mysql';
    protected $table = 'log_item_place';

    protected $fillable = [
        'place',
        'shelf',
        'layer',
        'item_no',
    ];
}
