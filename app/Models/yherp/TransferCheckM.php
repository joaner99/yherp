<?php

namespace App\Models\yherp;

use App\User;
use App\Models\yherp\TransferCheckD;
use Illuminate\Database\Eloquent\Model;

class TransferCheckM extends Model
{
    protected $connection = 'mysql';
    protected $table = 'transfer_check_m';

    protected $fillable = [
        'user_id',
        'tms_order_no',
        'exported_at',
    ];

    public function Details()
    {
        return $this->hasMany(TransferCheckD::class, 'm_id', 'id');
    }

    public function User()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
