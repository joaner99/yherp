<?php

namespace App\Models\yherp;

use Illuminate\Database\Eloquent\Model;

class LogSafetyStockExport extends Model
{
    protected $connection = 'mysql';
    protected $table = 'log_safety_stock_export';

    protected $fillable =
    [
        'item_no'
    ];
}
