<?php

namespace App\Models\yherp;

use App\Models\TMS\Item;
use App\Models\yherp\QuotationM;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class QuotationD extends Model
{
    use SoftDeletes;

    protected $connection = 'mysql';
    protected $table = 'quotation_d';

    protected $fillable = [
        'item_seq',
        'item_no',
        'item_name',
        'item_qty',
        'item_price',
        'item_remark',
    ];

    public function Main()
    {
        return $this->belongsTo(QuotationM::class, 'm_id', 'id');
    }

    public function Item()
    {
        return $this->belongsTo(Item::class, 'item_no', 'ICODE');
    }
}
