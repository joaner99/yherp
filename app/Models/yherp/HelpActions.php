<?php

namespace App\Models\yherp;

use App\Models\yherp\HelpQuestions;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class HelpActions extends Model
{
    use SoftDeletes;

    protected $connection = 'mysql';
    protected $table = 'help_actions';

    protected $fillable = [
        'name',
    ];

    public function Questions()
    {
        return $this->hasMany(HelpQuestions::class, 'action_id', 'id');
    }
}
