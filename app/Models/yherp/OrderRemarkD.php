<?php

namespace App\Models\yherp;

use App\User;
use App\Models\yherp\OrderRemarkM;
use Illuminate\Database\Eloquent\Model;

class OrderRemarkD extends Model
{
    protected $connection = 'mysql';
    protected $table = 'order_remark_d';

    protected $fillable = [
        'user_id',
        'content',
    ];

    public function Main()
    {
        return $this->belongsTo(OrderRemarkM::class, 'm_id', 'id');
    }

    public function User()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
