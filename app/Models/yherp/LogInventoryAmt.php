<?php

namespace App\Models\yherp;

use Illuminate\Database\Eloquent\Model;

class LogInventoryAmt extends Model
{
    protected $connection = 'mysql';
    protected $table = 'log_inventory_amt';

    protected $fillable = [
        'kind_name',
        'amt',
    ];
}
