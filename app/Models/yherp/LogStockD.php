<?php

namespace App\Models\yherp;

use App\Models\yherp\LogStockM;
use App\Models\yherp\TransferCheckD;
use Illuminate\Database\Eloquent\Model;

class LogStockD extends Model
{
    protected $connection = 'mysql';
    protected $table = 'log_stock_d';

    protected $fillable = [
        'm_id',
        'item_no',
        'item_name',
        'qty',
    ];

    public function Main()
    {
        return $this->belongsTo(LogStockM::class, 'm_id', 'id');
    }

    public function TransferCheck()
    {
        return $this->hasOne(TransferCheckD::class, 'log_stock_id', 'id');
    }
}
