<?php

namespace App\Models\yherp;

use App\Models\yherp\OrderShipmentStatusM;
use Illuminate\Database\Eloquent\Model;

class OrderShipmentStatusD extends Model
{
    public const status_list = ['1' => '過大/過重', '2' => '延遲出貨', '3' => '二次檢驗', '4' => '包裝監督'];

    protected $connection = 'mysql';
    protected $table = 'order_shipment_status_d';

    protected $fillable = [
        '_status',
        'confirm',
    ];

    public function Main()
    {
        return $this->belongsTo(OrderShipmentStatusM::class, 'm_id', 'id');
    }

    public function getStatusNameAttribute()
    {
        return self::status_list[$this->_status] ?? '';;
    }
}
