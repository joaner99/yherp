<?php

namespace App\Models\warranty_web;

use Illuminate\Database\Eloquent\Model;
use App\Models\warranty_web\VideoConfigM;
use Illuminate\Database\Eloquent\SoftDeletes;

class VideoConfigD extends Model
{
    use SoftDeletes;

    protected $connection = 'aws';
    protected $table = 'video_config_d';

    protected $fillable = [
        'item_no',
        'item_kind',
        'item_kind2',
        'item_kind3',
    ];

    public function Main()
    {
        return $this->belongsTo(VideoConfigM::class, 'm_id', 'id');
    }
}
