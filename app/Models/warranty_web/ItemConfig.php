<?php

namespace App\Models\warranty_web;

use App\Models\TMS\Item;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ItemConfig extends Model
{
    use SoftDeletes;

    protected $connection = 'aws';
    protected $table = 'item_config';
    public $incrementing = false;
    protected $keyType = 'string';

    protected $fillable = [
        'id',
        'warranty_y',
        'warranty_m',
        'warranty_d',
        'warranty_url'
    ];

    public function Item()
    {
        return $this->hasOne(Item::class, 'ICODE', 'id');
    }
}
