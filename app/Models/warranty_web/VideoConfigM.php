<?php

namespace App\Models\warranty_web;

use Illuminate\Database\Eloquent\Model;
use App\Models\warranty_web\VideoConfigD;
use Illuminate\Database\Eloquent\SoftDeletes;

class VideoConfigM extends Model
{
    use SoftDeletes;

    protected $connection = 'aws';
    protected $table = 'video_config_m';
    public $incrementing = false;
    protected $keyType = 'string';

    protected $fillable = [
        'id',
        'title',
    ];

    public function Details()
    {
        return $this->hasMany(VideoConfigD::class, 'm_id', 'id');
    }
}
