<?php

namespace App\Models\TMS;

use Illuminate\Database\Eloquent\Model;

class FreightCompany extends Model
{
    protected $connection = 'sqlsrv';
    protected $table = 'FreightCompany';
}
