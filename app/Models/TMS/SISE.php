<?php

namespace App\Models\TMS;

use Illuminate\Database\Eloquent\Model;

class SISE extends Model
{
    protected $connection = 'sqlsrv';
    protected $table = 'SISE';
    protected $appends = ['item_no', 'item_name'];

    public function getItemNoAttribute()
    {
        return $this->SICOD;
    }

    public function getItemNameAttribute()
    {
        return $this->SINAM;
    }
}
