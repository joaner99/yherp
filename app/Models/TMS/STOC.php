<?php

namespace App\Models\TMS;

use App\Models\TMS\Item;
use Illuminate\Database\Eloquent\Model;

class STOC extends Model
{
    protected $connection = 'sqlsrv';
    protected $table = 'STOC';
    protected $appends = ['stock', 'item_no', 'qty', 'stock_name'];

    public function getStockAttribute()
    {
        return $this->STCOD;
    }

    public function getItemNoAttribute()
    {
        return $this->SITEM;
    }

    public function getQtyAttribute()
    {
        return $this->SNUMB;
    }

    public function getStockNameAttribute()
    {
        return $this->STNAM;
    }

    public function Item()
    {
        return $this->belongsTo(Item::class, 'SITEM', 'ICODE');
    }

    public function ITEM1()
    {
        return $this->belongsTo(ITEM1::class, 'SITEM', 'ICODE1');
    }

    public function ITEM3()
    {
        return $this->belongsTo(ITEM3::class, 'SITEM', 'ICODE3');
    }
}
