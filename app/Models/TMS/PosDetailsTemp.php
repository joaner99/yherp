<?php

namespace App\Models\TMS;

use App\Models\TMS\PosMainTemp;
use Illuminate\Database\Eloquent\Model;

class PosDetailsTemp extends Model
{
    protected $connection = 'sqlsrv';
    protected $table = 'PosDetailsTemp';

    public function Main()
    {
        return $this->belongsTo(PosMainTemp::class, 'MainID', 'MainID');
    }
}
