<?php

namespace App\Models\TMS;

use Illuminate\Database\Eloquent\Model;

class Visein extends Model
{
    protected $connection = 'sqlsrv';
    protected $table = 'VISEIN';
    protected $appends = ['order_no', 'supply_name', 'date', 'qty', 'unit', 'total'];

    public function getOrderNoAttribute()
    {
        return $this->VCOD1;
    }

    public function getSupplyNameAttribute()
    {
        return $this->VPNAM;
    }

    public function getDateAttribute()
    {
        return $this->VDAT1;
    }

    public function getQtyAttribute()
    {
        return $this->VINUM;
    }

    public function getUnitAttribute()
    {
        return $this->VUNIT;
    }

    public function getTotalAttribute()
    {
        return $this->VTOTA;
    }
}
