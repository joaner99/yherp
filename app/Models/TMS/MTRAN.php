<?php

namespace App\Models\TMS;

use Illuminate\Database\Eloquent\Model;

/**
 * 調撥處理主檔
 */
class MTRAN extends Model
{
    protected $connection = 'sqlsrv';
    protected $table = 'MTRAN';
}
