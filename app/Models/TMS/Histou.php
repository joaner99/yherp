<?php

namespace App\Models\TMS;

use App\Models\TMS\Poseou;
use Illuminate\Database\Eloquent\Model;

class Histou extends Model
{
    protected $connection = 'sqlsrv';
    protected $table = 'HISTOU';
    protected $appends = ['sale_no', 'unit', 'qty', 'sales_total', 'cust_name', 'date'];

    public function getSaleNoAttribute()
    {
        return $this->HCOD1;
    }
    public function getUnitAttribute()
    {
        return $this->HUNIT;
    }
    public function getQtyAttribute()
    {
        return $this->HINUM;
    }
    public function getSalesTotalAttribute()
    {
        return $this->HTOTA;
    }
    public function getCustNameAttribute()
    {
        return $this->HPNAM;
    }
    public function getDateAttribute()
    {
        return $this->HDAT1;
    }

    public function Poseou()
    {
        return $this->belongsTo(Poseou::class, 'HCOD1', 'PCOD1');
    }
}
