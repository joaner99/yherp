<?php

namespace App\Models\TMS;

use App\Models\TMS\ITEM1;
use App\Models\TMS\Posein;
use App\Models\yherp\ItemPlaceD;
use App\Models\TMS\PoseinCheckDataLog;
use Illuminate\Database\Eloquent\Model;

class Histin extends Model
{
    protected $connection = 'sqlsrv';
    protected $table = 'HISTIN';
    protected $appends = ['sale_no', 'item_no', 'item_name', 'unit', 'qty', 'sales_total', 'cust_name', 'date'];

    public function getSaleNoAttribute()
    {
        return $this->HCOD1;
    }

    public function getItemNoAttribute()
    {
        return $this->HICOD;
    }
    public function getItemNameAttribute()
    {
        return $this->HINAM;
    }
    public function getUnitAttribute()
    {
        return $this->HUNIT;
    }
    public function getQtyAttribute()
    {
        return $this->HINUM;
    }
    public function getSalesTotalAttribute()
    {
        return $this->HTOTA;
    }
    public function getCustNameAttribute()
    {
        return $this->HPNAM;
    }
    public function getDateAttribute()
    {
        return $this->HDAT1;
    }

    public function Posein()
    {
        return $this->belongsTo(Posein::class, 'HCOD1', 'PCOD1');
    }

    public function ITEM1()
    {
        return $this->belongsTo(ITEM1::class, 'HICOD', 'ICODE1');
    }

    public function PoseinCheckDataLog()
    {
        return $this->hasMany(PoseinCheckDataLog::class, 'PCOD1', 'HCOD1');
    }

    public function ItemPlaceD()
    {
        return $this->hasMany(ItemPlaceD::class, 'item_no', 'HICOD');
    }
}
