<?php

namespace App\Models\TMS;

use Illuminate\Database\Eloquent\Model;

class CUST extends Model
{
    protected $connection = 'sqlsrv';
    protected $table = 'CUST';
    protected $appends = ['cust', 'cust_name_2'];

    public function getCustAttribute()
    {
        return $this->CCODE;
    }

    public function getCustName2Attribute()
    {
        return $this->CNAM2;
    }
}
