<?php

namespace App\Models\TMS;

use Illuminate\Database\Eloquent\Model;

/**
 * 
 */
class ITYP extends Model
{
    protected $connection = 'sqlsrv';
    protected $table = 'ITYP';
    protected $appends = ['stock_id', 'stock_name'];

    public function getStockIdAttribute()
    {
        return $this->ITCOD;
    }

    public function getStockNameAttribute()
    {
        return $this->ITNAM;
    }
}
