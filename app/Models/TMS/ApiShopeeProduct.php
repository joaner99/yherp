<?php

namespace App\Models\TMS;

use App\Services\AdService;
use Illuminate\Database\Eloquent\Model;

class ApiShopeeProduct extends Model
{
    protected $connection = 'sqlsrv';
    protected $table = 'ApiShopeeProduct';

    public function getShopNameAttribute()
    {
        $shop_name = AdService::shopee_shop[$this->ShopId] ?? '';
        return $shop_name;
    }
}
