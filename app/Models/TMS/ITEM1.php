<?php

namespace App\Models\TMS;

use Illuminate\Database\Eloquent\Model;

/**
 * 
 */
class ITEM1 extends Model
{
    protected $connection = 'sqlsrv';
    protected $table = 'ITEM1';
    protected $appends = ['kind2', 'kind2_name', 'kind3', 'kind3_name'];

    public function getKind2Attribute()
    {
        return $this->ITCO2;
    }

    public function getKind2NameAttribute()
    {
        return $this->ITNA2;
    }

    public function getKind3Attribute()
    {
        return $this->ITCO3;
    }

    public function getKind3NameAttribute()
    {
        return $this->ITNA3;
    }
}
