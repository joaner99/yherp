<?php

namespace App\Models\TMS;

use Illuminate\Database\Eloquent\Model;

class SALE extends Model
{
    protected $connection = 'sqlsrv';
    protected $table = 'SALE';
}
