<?php

namespace App\Models\TMS;

use App\Models\TMS\STOC;
use App\Models\TMS\ITEM1;
use App\Models\TMS\ITEM3;
use App\Models\TMS\ITEM4;
use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $connection = 'sqlsrv';
    protected $table = 'ITEM';
    protected $appends = ['item_no', 'item_name', 'supplier', 'supplier_name', 'cost', 'kind', 'kind_name', 'kind2', 'kind2_name', 'kind3', 'kind3_name', 'qty'];

    public function getItemNoAttribute()
    {
        return $this->ICODE;
    }

    public function getItemNameAttribute()
    {
        return $this->INAME;
    }

    public function getSupplierAttribute()
    {
        return $this->IPCOD;
    }

    public function getSupplierNameAttribute()
    {
        return $this->IPNAM;
    }

    public function getCostAttribute()
    {
        return $this->ISOUR;
    }

    public function getKindAttribute()
    {
        return $this->ITCOD;
    }

    public function getKindNameAttribute()
    {
        return $this->ITNAM;
    }

    public function getKind2Attribute()
    {
        return $this->ITEM1->ITCO2 ?? '';
    }

    public function getKind2NameAttribute()
    {
        return $this->ITEM1->ITNA2 ?? '';
    }

    public function getKind3Attribute()
    {
        return $this->ITEM1->ITCO3 ?? '';
    }

    public function getKind3NameAttribute()
    {
        return $this->ITEM1->ITNA3 ?? '';
    }

    public function getQtyAttribute()
    {
        return $this->INUMB;
    }

    public function ITEM1()
    {
        return $this->hasOne(ITEM1::class, 'ICODE1', 'ICODE');
    }

    public function ITEM3()
    {
        return $this->hasOne(ITEM3::class, 'ICODE3', 'ICODE');
    }

    public function ITEM4()
    {
        return $this->hasOne(ITEM4::class, 'ICODE4', 'ICODE');
    }

    public function STOC()
    {
        return $this->hasMany(STOC::class, 'SITEM', 'ICODE');
    }
}
