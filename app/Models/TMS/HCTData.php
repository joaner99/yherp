<?php

namespace App\Models\TMS;

use App\Models\TMS\Posein;
use Illuminate\Database\Eloquent\Model;

class HCTData extends Model
{
    protected $connection = 'sqlsrv';
    protected $table = 'HCT_Data';

    public function Posein()
    {
        return $this->belongsTo(Posein::class, 'PCOD1', 'PCOD1');
    }
}
