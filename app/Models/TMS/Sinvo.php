<?php

namespace App\Models\TMS;

use Illuminate\Database\Eloquent\Model;

class Sinvo extends Model
{
    protected $connection = 'sqlsrv';
    protected $table = 'Sinvo';
    protected $appends = ['company_no'];

    //統一編號
    public function getCompanyNoAttribute()
    {
        return $this->Ctcod;
    }
}
