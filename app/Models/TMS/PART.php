<?php

namespace App\Models\TMS;

use Illuminate\Database\Eloquent\Model;

class PART extends Model
{
    protected $connection = 'sqlsrv';
    protected $table = 'PART';
}
