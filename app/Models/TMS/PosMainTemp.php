<?php

namespace App\Models\TMS;

use App\Models\TMS\PosDetailsTemp;
use Illuminate\Database\Eloquent\Model;

class PosMainTemp extends Model
{
    protected $connection = 'sqlsrv';
    protected $table = 'PosMainTemp';
    protected $appends = ['order_no'];

    public function getOrderNoAttribute()
    {
        return $this->RCOD1;
    }

    public function Details()
    {
        return $this->hasMany(PosDetailsTemp::class, 'MainID', 'MainID');
    }
}
