<?php

namespace App\Models\TMS;

use App\Models\TMS\Sinvo;
use App\Models\TMS\Posein;
use App\Services\BaseService;
use App\Models\TMS\OrderDetail;
use App\Models\yherp\OrderRemarkM;
use App\Models\yherp\OrderSampleM;
use App\Models\yherp\OrderConfirmD;
use App\Models\yherp\OtherTransport;
use App\Models\yherp\PersonalTransport;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $connection = 'sqlsrv';
    protected $table = 'ORDET';
    protected $appends = ['order_no', 'original_no', 'date', 'cust', 'cust_name', 'gross_margin', 'receive_no', 'sales_total', 'sales_total_tax', 'bak1', 'bak2', 'bak3', 'status'];

    public function getOrderNoAttribute()
    {
        return $this->OCOD1;
    }

    public function getOriginalNoAttribute()
    {
        return $this->OCOD4;
    }

    public function getDateAttribute()
    {
        return $this->ODATE;
    }

    public function getCustAttribute()
    {
        return $this->OCCOD;
    }

    public function getCustNameAttribute()
    {
        return $this->OCNAM;
    }

    //毛利率
    public function getGrossMarginAttribute()
    {
        return $this->OPERS;
    }

    //銷售總金額(含稅)
    public function getSalesTotalAttribute()
    {
        return $this->OTOT1;
    }

    //銷售總金額(含稅)
    public function getSalesTotalTaxAttribute()
    {
        return $this->OTOT2;
    }

    public function getBak1Attribute()
    {
        return $this->OBAK1;
    }

    public function getBak2Attribute()
    {
        return $this->OBAK2;
    }

    public function getBak3Attribute()
    {
        return $this->OBAK3;
    }

    public function getStatusAttribute()
    {
        return $this->OCANC;
    }

    public function getReceiveNoAttribute()
    {
        return $this->RecieveCTCOD;
    }

    public function getTransAttribute()
    {
        return BaseService::ConvertTransCode($this->TransportCode);
    }

    public function getTransNameAttribute()
    {
        return $this->TransportName;
    }

    public function OrderDetail()
    {
        return $this->hasMany(OrderDetail::class, 'OCOD1', 'OCOD1');
    }

    public function Posein()
    {
        return $this->hasMany(Posein::class, 'PCOD2', 'OCOD1');
    }

    public function OrderRemarkM()
    {
        return $this->hasOne(OrderRemarkM::class, 'OCOD1', 'OCOD1');
    }

    public function OrderConfirm()
    {
        return $this->hasMany(OrderConfirmD::class, 'order_no', 'OCOD4');
    }

    public function ShopeeTrans()
    {
        return $this->OrderConfirm()->whereHas('Main', function ($q) {
            $q->where('order_type', 10);
        });
    }

    public function OrderSample()
    {
        return $this->hasOne(OrderSampleM::class, 'order_no', 'OCOD4');
    }

    public function PersonalTransport()
    {
        return $this->hasOne(PersonalTransport::class, 'original_no', 'OCOD4');
    }

    public function OtherTransport()
    {
        return $this->hasOne(OtherTransport::class, 'original_no', 'OCOD4');
    }

    //發票
    public function Sinvo()
    {
        return $this->hasManyThrough(
            Sinvo::class,
            Posein::class,
            'PCOD2',
            'Invono',
            'OCOD1',
            'PTXCO'
        );
    }
}
