<?php

namespace App\Models\TMS;

use App\Models\TMS\Histou;
use App\Models\TMS\Posein;
use App\Models\yherp\OrderInvoice;
use Illuminate\Database\Eloquent\Model;

class Poseou extends Model
{
    protected $connection = 'sqlsrv';
    protected $table = 'POSEOU';
    protected $appends = ['order_no'];

    public function getOrderNoAttribute()
    {
        return $this->PCOD1;
    }

    public function Posein()
    {
        return $this->belongsTo(Posein::class, 'PCOD2', 'PCOD2');
    }

    public function OrderInvoice()
    {
        return $this->belongsTo(OrderInvoice::class, 'PJONO', 'order_no');
    }

    //退貨明細
    public function Histou()
    {
        return $this->hasMany(Histou::class, 'HCOD1', 'PCOD1');
    }
}
