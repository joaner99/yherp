<?php

namespace App\Models\TMS;

use App\Models\TMS\Order;
use App\Models\TMS\Histin;
use App\Models\TMS\Poseou;
use App\Services\BaseService;
use App\Models\yherp\CinForest;
use App\Models\yherp\LogPackage;
use App\Models\yherp\LogShipment;
use App\Models\yherp\OrderInvoice;
use App\Models\yherp\OrderSampleM;
use App\Models\yherp\LogPickingPrint;
use App\Models\TMS\PoseinCheckDataLog;
use App\Models\yherp\PersonalTransport;
use Illuminate\Database\Eloquent\Model;
use App\Models\yherp\OrderShipmentStatusM;

class Posein extends Model
{
    protected $connection = 'sqlsrv';
    protected $table = 'POSEIN';

    protected $appends = [
        'sale_no',
        'order_no',
        'original_no',
        'sale_date',
        'tax_type',
        'tax',
        'receive_no',
        'cust',
        'cust_name',
        'trans_no',
        'trans',
        'trans_name',
        'sales_total',
        'sales_total_tax',
        'bak1',
        'bak2',
        'bak3',
        'print_count',
        'print_time'
    ];

    public function getSaleNoAttribute()
    {
        return $this->PCOD1;
    }

    public function getOrderNoAttribute()
    {
        return $this->PCOD2;
    }

    public function getOriginalNoAttribute()
    {
        return $this->PJONO;
    }

    public function getSaleDateAttribute()
    {
        return $this->PDAT1;
    }

    public function getTaxTypeAttribute()
    {
        return $this->POFFE;
    }

    public function getTaxAttribute()
    {
        return $this->PTXCO;
    }

    public function getReceiveNoAttribute()
    {
        return $this->RecieveCTCOD;
    }

    public function getCustAttribute()
    {
        return $this->PPCOD;
    }

    public function getCustNameAttribute()
    {
        return $this->PPNAM;
    }

    public function getTransNoAttribute()
    {
        return $this->ConsignTran;
    }

    public function getTransAttribute()
    {
        return BaseService::ConvertTransCode($this->TransportCode);
    }

    public function getTransNameAttribute()
    {
        return $this->TransportName;
    }

    //銷售總金額(含稅)
    public function getSalesTotalAttribute()
    {
        return $this->PTOT1;
    }

    //銷售總金額(含稅)
    public function getSalesTotalTaxAttribute()
    {
        return $this->PTOTA;
    }

    public function getBak1Attribute()
    {
        return $this->PBAK1;
    }

    public function getBak2Attribute()
    {
        return $this->PBAK2;
    }

    public function getBak3Attribute()
    {
        return $this->PBAK3;
    }

    public function getPrintCountAttribute()
    {
        return $this->PPRIN;
    }

    public function getPrintTimeAttribute()
    {
        return $this->PPTIM;
    }

    //銷貨明細
    public function Histin()
    {
        return $this->hasMany(Histin::class, 'HCOD1', 'PCOD1');
    }

    //驗貨紀錄
    public function PoseinCheckDataLog()
    {
        return $this->hasMany(PoseinCheckDataLog::class, 'PCOD1', 'PCOD1');
    }

    //退貨
    public function Poseou()
    {
        return $this->hasOne(Poseou::class, 'PCOD2', 'PCOD2');
    }

    //訂單
    public function OriginalOrder()
    {
        return $this->belongsTo(OrderSampleM::class, 'PJONO', 'order_no');
    }

    //訂單
    public function Order()
    {
        return $this->belongsTo(Order::class, 'PCOD2', 'OCOD1');
    }

    //真桂森林訂單
    public function Cinforest()
    {
        return $this->hasOne(CinForest::class, 'PCOD1', 'PCOD1');
    }

    //自建系統的撿貨單列印紀錄
    public function LogPickingPrint()
    {
        return $this->hasMany(LogPickingPrint::class, 'PCOD1', 'PCOD1');
    }

    //自建系統的包裝紀錄
    public function LogPackage()
    {
        return $this->hasMany(LogPackage::class, 'sale_no', 'PCOD1');
    }

    //自建系統的上車紀錄
    public function LogShipment()
    {
        return $this->hasMany(LogShipment::class, 'PCOD1', 'PCOD1');
    }

    //託運狀態
    public function OrderShipmentStatusM()
    {
        return $this->hasOne(OrderShipmentStatusM::class, 'sales_no', 'PCOD1');
    }

    //代開發票
    public function OrderInvoice()
    {
        return $this->hasOne(OrderInvoice::class, 'order_no', 'PJONO');
    }

    //親送資料
    public function PersonalTransport()
    {
        return $this->hasOne(PersonalTransport::class, 'original_no', 'PJONO');
    }

    //產品詳細資料
    public function Item1()
    {
        return $this->hasManyThrough(
            ITEM1::class,
            Histin::class,
            'HCOD1',
            'ICODE1',
            'PCOD1',
            'HICOD'
        );
    }
}
