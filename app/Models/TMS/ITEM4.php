<?php

namespace App\Models\TMS;

use App\Models\TMS\Item;
use App\Models\TMS\STOC;
use App\Models\TMS\ITEM1;
use App\Models\TMS\ITEM3;
use Illuminate\Database\Eloquent\Model;

/**
 * 
 */
class ITEM4 extends Model
{
    protected $connection = 'sqlsrv';
    protected $table = 'ITEM4';
    protected $appends = ['item_no'];

    public function getItemNoAttribute()
    {
        return $this->ICODE4;
    }

    public function Item()
    {
        return $this->belongsTo(Item::class, 'ICODE4', 'ICODE');
    }

    public function Stoc()
    {
        return $this->hasMany(STOC::class, 'SITEM', 'ICODE4');
    }

    public function ITEM1()
    {
        return $this->hasOne(ITEM1::class, 'ICODE1', 'ICODE4');
    }

    public function ITEM3()
    {
        return $this->hasOne(ITEM3::class, 'ICODE3', 'ICODE4');
    }
}
