<?php

namespace App\Models\TMS;

use App\Models\TMS\Item;
use App\Models\TMS\STOC;
use App\Models\TMS\ITEM1;
use App\Models\TMS\Order;
use App\Models\TMS\Posein;
use App\Models\TMS\Poseou;
use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
	protected $connection = 'sqlsrv';
	protected $table = 'ORSUB';
	protected $appends = ['order_no', 'item_no', 'item_name', 'unit', 'qty', 'sales_total', 'ship', 'cust_name', 'date'];

	public function getOrderNoAttribute()
	{
		return $this->OCOD1;
	}

	public function getItemNoAttribute()
	{
		return (string)$this->OICOD;
	}

	public function getItemNameAttribute()
	{
		return $this->OINAM;
	}

	public function getUnitAttribute()
	{
		return $this->OUNIT;
	}

	public function getQtyAttribute()
	{
		return $this->OINUM;
	}

	public function getSalesTotalAttribute()
	{
		return $this->OTOTA;
	}

	public function getShipAttribute()
	{
		return $this->OINYN;
	}

	public function getCustNameAttribute()
	{
		return $this->OCNAM;
	}

	public function getDateAttribute()
	{
		return $this->ODATE;
	}

	public function Main()
	{
		return $this->belongsTo(Order::class, 'OCOD1', 'OCOD1');
	}

	public function Posein()
	{
		return $this->hasMany(Posein::class, 'PCOD2', 'OCOD1');
	}

	public function Poseou()
	{
		return $this->hasMany(Poseou::class, 'PCOD2', 'OCOD1');
	}

	public function Stoc()
	{
		return $this->hasMany(STOC::class, 'SITEM', 'OICOD');
	}

	public function Item()
	{
		return $this->belongsTo(Item::class, 'OICOD', 'ICODE');
	}

	public function Item1()
	{
		return $this->belongsTo(ITEM1::class, 'OICOD', 'ICODE1');
	}
}
