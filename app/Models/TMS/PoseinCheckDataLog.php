<?php

namespace App\Models\TMS;

use App\Models\TMS\Posein;
use Illuminate\Database\Eloquent\Model;

class PoseinCheckDataLog extends Model
{
    protected $connection = 'sqlsrv';
    protected $table = 'PoseinCheckDataLog';

    public function Posein()
    {
        return $this->belongsTo(Posein::class, 'PCOD1', 'PCOD1');
    }
}
