<?php

namespace App\Models\TMS;

use App\Models\TMS\Item;
use App\Models\TMS\STOC;
use App\Models\TMS\ITEM1;
use App\Models\TMS\ITEM3;
use Illuminate\Database\Eloquent\Model;

/**
 * 
 */
class ITEMP extends Model
{
    protected $connection = 'sqlsrv';
    protected $table = 'ITEMP';
    protected $appends = ['place', 'item_no'];

    public function getPlaceAttribute()
    {
        return $this->IICOD;
    }

    public function getItemNoAttribute()
    {
        return $this->ICODE;
    }

    public function Item()
    {
        return $this->belongsTo(Item::class, 'ICODE', 'ICODE');
    }

    public function Stoc()
    {
        return $this->hasMany(STOC::class, 'SITEM', 'ICODE');
    }

    public function ITEM1()
    {
        return $this->hasOne(ITEM1::class, 'ICODE1', 'ICODE');
    }

    public function ITEM3()
    {
        return $this->hasOne(ITEM3::class, 'ICODE3', 'ICODE');
    }
}
