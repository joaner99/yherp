<?php

namespace App\Models\ERP;

use Illuminate\Database\Eloquent\Model;

class PurchaseOrder extends Model
{
    protected $connection = 'sqlsrv';
    protected $table = 'SBIN';

    public function getOrderNoAttribute()
    {
        return $this->SCOD1;
    }
}
