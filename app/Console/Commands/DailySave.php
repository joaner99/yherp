<?php

namespace App\Console\Commands;

use Exception;
use Carbon\Carbon;
use App\Models\TMS\Item;
use App\Models\TMS\Posein;
use Illuminate\Console\Command;
use App\Models\yherp\LogItemSale;
use App\Models\yherp\SafetyStock;
use App\Services\ScheduleService;
use Illuminate\Support\Facades\DB;
use App\Repositories\BaseRepository;
use App\Models\yherp\CustomerComplaint;

class DailySave extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'daily:save';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '每日資料紀錄';

    protected $scheduleService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->scheduleService = new ScheduleService();
        $this->SaveItemSales();
        $this->SaveProductInventory();
        $this->SaveSafetyStock();
        $this->SaveMFTransport();
    }

    /**
     * 總庫存紀錄
     *
     * @return void
     */
    private function SaveProductInventory()
    {
        try {
            //取得目前產品庫存
            $dbItem = Item::select('ICODE', 'INUMB', 'ISOUR')
                ->whereNotIn('ITCOD', BaseRepository::excludedSalesType)
                ->distinct()
                ->get();
            //轉寫入陣列
            $datas = array();
            foreach ($dbItem as $value) {
                $icode = (string)$value->item_no;
                $inumb = $value->qty;
                $isour = $value->cost ?? 0;

                $datas[] = [
                    'ICODE' => $icode,
                    'INUMB' => $inumb,
                    'ISOUR' => $isour,
                ];
            }
            if (!empty($datas)) {
                //寫入資料庫
                DB::connection('mysql')
                    ->table('log_item')
                    ->insert($datas);
            }

            $this->scheduleService->CreateOnSuccessLog($this->signature, '總庫存紀錄');
        } catch (Exception $ex) {
            $this->scheduleService->CreateOnFailureLog($this->signature, '總庫存紀錄', $ex->getMessage());
        }
    }

    /**
     * 安全庫存紀錄
     *
     * @return void
     */
    private function SaveSafetyStock()
    {
        try {
            //清除舊的資料
            SafetyStock::where('created_at', '<=',  Carbon::now())->delete();
            $db = $this->scheduleService->GetSafetyStock();
            if (!empty($db)) {
                $data = array();
                foreach ($db as $item) {
                    $data[] = [
                        'supplier' => $item->IPNAM, //供應商名稱
                        'item_no' => $item->ICODE, //產品料號
                        'item_name' => $item->INAME, //產品名稱
                        'item_remark' => $item->T11_11, //產品備註
                        'stock' => $item->SNUMB, //庫存量
                        'daily_sales' => $item->DailySales, //每日銷售數
                        'remaining_day' => $item->RemainingDay, //庫存剩餘天數
                        'suggest' => $item->SuggestBuy, //建議採購量
                        'purchase_no' => $item->SCOD1, //最近採購單號
                        'inventory_date' => $item->UpdateDate, //最近盤點日期
                        'inventory_num' => $item->Quantity, //最近盤點數量
                        'elapsed_day' => $item->ElapsedDay, //最後銷售日期距今天數
                    ];
                }
                //有資料才儲存
                if (count($data) > 0) {
                    SafetyStock::insert($data);
                }
            }
            $this->scheduleService->CreateOnSuccessLog($this->signature, '安全庫存紀錄');
        } catch (Exception $ex) {
            $this->scheduleService->CreateOnFailureLog($this->signature, '安全庫存紀錄', $ex->getMessage());
        }
    }

    /**
     * 紀錄過去一年各商品銷售佔比
     *
     * @return void
     */
    private function SaveItemSales()
    {
        try {
            $data = $this->scheduleService->GetAllItemsSale();
            if (!empty($data) && count($data) > 0) {
                LogItemSale::truncate();
                $total = $data->sum('sales');
                $tmp = [];
                foreach ($data as $d) {
                    $tmp[] = [
                        'id' => $d->item_no,
                        'sales' => $d->sales,
                        'ratio' => round($d->sales / $total, 7),
                    ];
                }
                LogItemSale::insert($tmp);
            }
            $this->scheduleService->CreateOnSuccessLog($this->signature, '商品銷售佔比紀錄');
        } catch (Exception $ex) {
            $this->scheduleService->CreateOnFailureLog($this->signature, '商品銷售佔比紀錄', $ex->getMessage());
        }
    }

    //廠商直寄訂單轉客訴
    private function SaveMFTransport()
    {
        try {
            $e_date = $this->scheduleService->GetTWDateStr(Carbon::today());
            $s_date = $this->scheduleService->GetTWDateStr(Carbon::today()->addDays(-7));
            //廠商直寄
            $db = Posein::select('PCOD2', 'PJONO')
                ->whereNotIn('PPCOD', ['S001', 'S003', 'S004'])
                ->where(function ($q) {
                    $q
                        ->where('TransportCode', 'MF') //物流為廠商直寄
                        ->orWhereHas('Histin', function ($q2) {
                            $q2
                                ->select('PCOD1')
                                ->whereIn('HICOD', ['10001008021', '10001008025']);
                        })
                        ->orWhere('PBAK1', 'LIKE', '%廠商直%')
                        ->orWhere('PBAK2', 'LIKE', '%廠商直%')
                        ->orWhere('PBAK3', 'LIKE', '%廠商直%')
                        ->orWhere('InsideNote', 'LIKE', '%廠商直%');
                })
                ->whereBetween('PDAT1', [$s_date, $e_date])
                ->get();
            foreach ($db as $d) {
                $order = CustomerComplaint::where('status', '!=', '99')
                    ->where(function ($q) use ($d) {
                        $q->where('order_no', $d->order_no)->orWhere('origin_no', $d->original_no);
                    })->first();
                if (!empty($order)) {
                    if (mb_strpos($order->remark, '廠商直寄') === false) {
                        $order->remark .= '系統自動補充廠商直寄訂單';
                        $order->save();
                    } else {
                        continue;
                    }
                } else { //作廢或者未建立客訴
                    CustomerComplaint::create([
                        'order_no' => $d->order_no,
                        'origin_no' => $d->original_no,
                        'remark' => '系統自動建立廠商直寄訂單',
                        'status' => 8,
                        'urgent' => 1
                    ]);
                }
            }
            $this->scheduleService->CreateOnSuccessLog($this->signature, '廠商直寄轉客訴單');
        } catch (Exception $ex) {
            $this->scheduleService->CreateOnFailureLog($this->signature, '廠商直寄轉客訴單', $ex->getMessage());
        }
    }
}
