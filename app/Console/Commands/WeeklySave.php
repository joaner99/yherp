<?php

namespace App\Console\Commands;

use Exception;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Services\SaleService;
use Illuminate\Console\Command;
use App\Services\ScheduleService;
use App\Models\yherp\LogInventoryTurnover;

class WeeklySave extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'weekly:save';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '每週資料紀錄';

    protected $scheduleService;
    protected $saleService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->scheduleService = new ScheduleService();
        $this->saleService = new SaleService();
        $this->SaveInventoryTurnover();
    }

    /**
     * 庫存週轉率評等
     *
     * @return void
     */
    private function SaveInventoryTurnover()
    {
        try {
            //建立指定料號的請求
            $request = new Request();
            $request->replace(
                [
                    'start_date' => Carbon::today()->modify('-31 day')->format('Y-m-d'),
                    'end_date' => Carbon::today()->modify('-1 day')->format('Y-m-d'),
                    'marketing_item' => true,
                ]
            );
            $db = $this->saleService->GetSalesPerformance($request);
            if (!empty($db['data']) && count($db['data']) > 0) {
                //有行銷評等
                foreach (collect($db['data'])->where('rating', '!=', '') as $item_no => $d) {
                    LogInventoryTurnover::create([
                        'rating' => $d['rating_code'],
                        'sys_rating' => $d['sys_rating_code'],
                        'item_no' => $item_no,
                        'item_name' => $d['item_name'],
                    ]);
                }
            }

            $this->scheduleService->CreateOnSuccessLog($this->signature, '庫存週轉率評等紀錄');
        } catch (Exception $ex) {
            $this->scheduleService->CreateOnFailureLog($this->signature, '庫存週轉率評等紀錄', $ex->getMessage());
        }
    }
}
