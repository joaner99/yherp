<?php

namespace App\Console\Commands;

use Exception;
use Illuminate\Console\Command;
use App\Services\ScheduleService;
use App\Models\yherp\LogInventoryAmt;
use Carbon\Carbon;

class MonthlySave extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'monthly:save';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '每月資料紀錄';

    protected $scheduleService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->scheduleService = new ScheduleService();
        $this->SaveInventoryAmt();
    }

    private function  SaveInventoryAmt()
    {
        try {
            $date = Carbon::today()->format('Y-m');
            $c = LogInventoryAmt::where('created_at', 'like', $date . '-%')->count();
            if ($c > 0) { //當月份已記錄
                return;
            }
            $data = $this->scheduleService->GetStockAll()['inventory_amt'];
            if (!empty($data) && count($data) > 0) {
                $tmp = [];
                foreach ($data as $kind_name => $amt) {
                    $tmp[] = [
                        'kind_name' => $kind_name,
                        'amt' => $amt
                    ];
                }
                LogInventoryAmt::insert($tmp);
            }
            $this->scheduleService->CreateOnSuccessLog($this->signature, '庫存金額紀錄');
        } catch (Exception $ex) {
            $this->scheduleService->CreateOnFailureLog($this->signature, '庫存金額紀錄', $ex->getMessage());
        }
    }
}
