<?php

namespace App\Console\Commands;

use Exception;
use Carbon\Carbon;
use App\Mail\TableMail;
use App\Models\TMS\Posein;
use App\Mail\ChinaSalesMail;
use App\Services\ItemService;
use App\Services\SaleService;
use App\Services\StockService;
use Illuminate\Console\Command;
use App\Models\yherp\ItemPlaceD;
use App\Models\ERP\PurchaseOrder;
use App\Services\ScheduleService;
use App\Models\yherp\OrderSampleM;
use Illuminate\Support\Facades\DB;
use App\Services\OrderSampleService;
use Illuminate\Support\Facades\Mail;
use App\Models\yherp\ContainerImport;

class DailyCheck extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'daily:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '每日檢查';

    protected $scheduleService;
    protected $itemService;
    protected $saleService;
    protected $stockService;
    private $usersEmail;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->scheduleService = new ScheduleService();
        $this->itemService = new ItemService();
        $this->saleService = new SaleService();
        $this->stockService = new StockService();
        $this->usersEmail = $this->scheduleService->GetUsersEmail();

        $this->StockUnusualCheck();
        $this->ChinaItemCheck();
        //$this->SpOrderClear();
        $this->SalesCheck();
        $this->PurchaseCheck();
        $this->PurchaseTaxTypeCheck();
        $this->ItemPlaceEmptyCheck();
        $this->ItemInfoCheck();
        //$this->ContainerImportCheck();
        //$this->ContainerImportCheck2();
    }

    /**
     * 庫存異常檢查
     *
     * @return void
     */
    private function StockUnusualCheck()
    {
        try {
            $db = $this->scheduleService->GetStockUnusual();
            $data = array();
            if (!empty($db)) {
                $index = 0;
                foreach ($db as $value) {
                    $data[] = [
                        ++$index,
                        $value->item_no,
                        $value->Item->item_name,
                        $value->stock_name,
                        number_format($value->qty)
                    ];
                }
            }

            if (!empty($data)) {
                $mailData =
                    [
                        'subject' => '庫存異常',
                        'caption' => '庫存異常清單',
                        'thead' => ['序', '料號', '品名', '倉別', '庫存數'],
                        'tbody' => $data
                    ];
                $mails = $this->usersEmail->whereIn('name', ['max', 'ginny', 'yen'])->pluck('email')->toArray();
                if (!empty($mails)) {
                    Mail::to($mails)->send(new TableMail($mailData));
                }
            }
            $this->scheduleService->CreateOnSuccessLog($this->signature, '庫存異常檢查');
        } catch (Exception $ex) {
            $this->scheduleService->CreateOnFailureLog($this->signature, '庫存異常檢查', $ex->getMessage());
        }
    }

    /**
     * 中國商品銷售對比
     *
     * @return void
     */
    private function ChinaItemCheck()
    {
        try {
            $db = $this->scheduleService->GetChinaSales();
            if (!empty($db) && count($db) > 0) {
                $mails = $this->usersEmail->whereIn('name', ['max', 'ginny', 'yahsunk', '林盈沁'])->pluck('email')->toArray();
                if (!empty($mails)) {
                    Mail::to($mails)->send(new ChinaSalesMail($db));
                }
            }

            $this->scheduleService->CreateOnSuccessLog($this->signature, '中國商品銷售對比');
        } catch (Exception $ex) {
            $this->scheduleService->CreateOnFailureLog($this->signature, '中國商品銷售對比', $ex->getMessage());
        }
    }

    /**
     * 已完成特殊訂單清除
     *
     * @return void
     */
    private function SpOrderClear()
    {
        try {
            $date = Carbon::today()->addMonth(-2)->format('Y-m-d 00:00:00');
            OrderSampleM::withTrashed()->where('completed', 1)->where('order_date', '<', $date)->forcedelete();
            $this->scheduleService->CreateOnSuccessLog($this->signature, '清除已完成特殊訂單');
        } catch (Exception $ex) {
            $this->scheduleService->CreateOnFailureLog($this->signature, '清除已完成特殊訂單', $ex->getMessage());
        }
    }

    /**
     * 補寄貨、補寄貨來回件、退換貨銷售額檢查
     *
     * @return void
     */
    private function SalesCheck()
    {
        try {
            $db = Posein::select('PCOD1', 'PTOTA')
                ->whereIn('PPCOD', ['C002', 'C005', 'C006'])
                ->where('PTOTA', '!=', 0)
                ->where('PDAT1', '>=', '111.07.01')
                ->whereNotIn('PCOD1', ['11110280036'])
                ->orderBy('PCOD1')
                ->get();

            if (!empty($db) && count($db) > 0) {
                $mailData =
                    [
                        'subject' => '補寄貨、補寄貨來回件、退換貨銷售檢查',
                        'caption' => '銷貨單清單',
                        'thead' => ['流水號', '銷貨單號', '銷售總計'],
                    ];
                $data = [];
                foreach ($db as $key => $d) {
                    $data[] = [
                        $key,
                        $d->PCOD1,
                        number_format($d->PTOTA),
                    ];
                }
                $mailData['tbody'] = $data;
                $mails = $this->usersEmail->whereIn('name', ['fin'])->pluck('email')->toArray();
                if (!empty($mails)) {
                    Mail::to($mails)->send(new TableMail($mailData));
                }
            }

            $this->scheduleService->CreateOnSuccessLog($this->signature, '補寄貨銷售額檢查');
        } catch (Exception $ex) {
            $this->scheduleService->CreateOnFailureLog($this->signature, '補寄貨銷售額檢查', $ex->getMessage());
        }
    }

    //採購未進貨檢查
    private function PurchaseCheck()
    {
        try {
            $db = PurchaseOrder::select('SCOD1')
                ->where('SINYN', 'N')
                ->where('SDAT1', '>=', '111.07.01')
                ->get();
            if (!empty($db) && count($db) > 0) {
                $mailData =
                    [
                        'subject' => '採購未進貨檢查',
                        'caption' => "自111年7月1號以來，採購單未進貨完成清單",
                        'thead' => ['序', '採購單號'],
                    ];
                $data = [];
                $index = 1;
                foreach ($db as $d) {
                    $data[] = [
                        $index++,
                        $d->order_no,
                    ];
                }
                $mailData['tbody'] = $data;
                $mails = $this->usersEmail->whereIn('name', ['ginny'])->pluck('email')->toArray();
                if (!empty($mails)) {
                    Mail::to($mails)->send(new TableMail($mailData));
                }
            }

            $this->scheduleService->CreateOnSuccessLog($this->signature, '採購未進貨檢查');
        } catch (Exception $ex) {
            $this->scheduleService->CreateOnFailureLog($this->signature, '採購未進貨檢查', $ex->getMessage());
        }
    }

    //採購稅別檢查
    private function PurchaseTaxTypeCheck()
    {
        try {
            $db = PurchaseOrder::select('SCOD1')
                ->where('SINYN', 'N')
                ->whereIn('SPCOD', ['A0003', 'A0020', 'A0004', 'A0005', 'A0009', 'A0010', 'A0011', 'A0017', 'A0018', 'A0028', 'A0032', 'A0033', 'A0036', 'B001', 'Z0001'])
                ->where('SCTAX', '!=', '1')
                ->get();
            if (!empty($db) && count($db) > 0) {
                $mailData =
                    [
                        'subject' => '採購未進貨稅別檢查',
                        'caption' => "採購單未進貨稅別錯誤清單",
                        'thead' => ['序', '採購單號'],
                    ];
                $data = [];
                $index = 1;
                foreach ($db as $d) {
                    $data[] = [
                        $index++,
                        $d->order_no,
                    ];
                }
                $mailData['tbody'] = $data;
                $mails = $this->usersEmail->whereIn('name', ['ginny'])->pluck('email')->toArray();
                if (!empty($mails)) {
                    Mail::to($mails)->send(new TableMail($mailData));
                }
            }

            $this->scheduleService->CreateOnSuccessLog($this->signature, '採購稅別檢查');
        } catch (Exception $ex) {
            $this->scheduleService->CreateOnFailureLog($this->signature, '採購稅別檢查', $ex->getMessage());
        }
    }

    //清空主倉儲無數量商品的儲位
    private function ItemPlaceEmptyCheck()
    {
        try {
            ItemPlaceD::where('qty', 0)->whereHas('Main', function ($q) {
                $q->where('s_id', 'B001');
            })->delete();
            $this->scheduleService->CreateOnSuccessLog($this->signature, '主倉空儲位檢查');
        } catch (Exception $ex) {
            $this->scheduleService->CreateOnFailureLog($this->signature, '主倉空儲位檢查', $ex->getMessage());
        }
    }

    //產品成本、銷售檢查
    private function ItemInfoCheck()
    {
        try {
            $s_date = Carbon::today()->addYears(-1);
            $tw_s_date = $this->scheduleService->GetTWDateStr($s_date);
            $exclude = ['22010044027', '10024007001'];
            $db = DB::table('ITEM AS I')
                ->select('I.IPNAM', 'I.ICODE', 'I.INAME', 'I.IUNIT', 'I1.SiseCost', 'I.AddDate')
                ->addSelect(DB::raw("(SELECT SUM(HTOTA) FROM HISTIN WHERE HICOD=I.ICODE AND HDAT1 >= '" . $tw_s_date . "') AS sale"))
                ->join('ITEM1 AS I1', 'I1.ICODE1', 'I.ICODE')
                ->join('ITEM3 AS I3', 'I3.ICODE3', 'I.ICODE')
                ->where('I1.ISTOP', 0) //暫停出貨
                ->where('I3.IPSTOP', 0) //排除廠商停產
                ->where('I.ICONS', '!=', 'Y') //排除母件
                ->whereNotIn('I.ITCOD', ['97', '98', '99'])
                ->whereNotIn('I.ICODE', $exclude) //排除特定料號
                ->where(function ($q) {
                    $q->where('I1.SiseCost', 0)
                        ->orWhere('I.IUNIT', 0);
                })
                ->get();
            if (!empty($db) && count($db) > 0) {
                $mailData =
                    [
                        'subject' => '產品售價成本檢查',
                        'caption' => "過去一年內有銷售紀錄，包含一年內新品，售價及成本未確實填寫",
                        'thead' => ['序', '供應商', '產品代號', '產品名稱', '建立日期', '銷售單價1', '採購成本'],
                    ];
                $data = [];
                $index = 1;
                foreach ($db as $d) {
                    if (empty($d->sale) && $d->AddDate < $s_date) {
                        continue;
                    }
                    $data[] = [
                        $index++,
                        $d->IPNAM,
                        $d->ICODE,
                        $d->INAME,
                        $d->AddDate,
                        number_format($d->IUNIT),
                        number_format($d->SiseCost)
                    ];
                }
                $mailData['tbody'] = $data;
                $mails = $this->usersEmail->whereIn('name', ['fin', 'ginny', 'yen', 'max'])->pluck('email')->toArray();
                if (!empty($mails)) {
                    Mail::to($mails)->send(new TableMail($mailData));
                }
            }
            $this->scheduleService->CreateOnSuccessLog($this->signature, '產品售價成本檢查');
        } catch (Exception $ex) {
            $this->scheduleService->CreateOnFailureLog($this->signature, '產品售價成本檢查', $ex->getMessage());
        }
    }

    //貨櫃到廠超時未處裡檢查
    private function ContainerImportCheck()
    {
        try {
            $days = 20;
            $s_date = Carbon::today()->addDays(-$days)->format('Y-m-d');
            $db = ContainerImport::where(function ($q) {
                $q->orWhereNull('remittance_date')->orWhereNull('remittance_account')->orWhere('remittance_amount', 0);
            })
                ->where('date', '<=', $s_date)
                ->get();
            if (!empty($db) && count($db) > 0) {
                $mailData =
                    [
                        'subject' => '貨櫃到廠清單',
                        'caption' => "貨櫃到廠超過20天，未確實處理",
                        'thead' => ['流水號', '到貨日期',  '匯款日期', '匯款帳號', '匯款金額'],
                    ];
                $data = [];
                foreach ($db as $d) {
                    $data[] = [
                        $d->id,
                        join(',', $d->shipdate_list),
                        $d->remittance_date,
                        $d->remittance_account,
                        $d->remittance_amount,
                    ];
                }
                $mailData['tbody'] = $data;
                $mails = $this->usersEmail->whereIn('name', ['fin'])->pluck('email')->toArray();
                if (!empty($mails)) {
                    Mail::to($mails)->send(new TableMail($mailData));
                }
            }

            $this->scheduleService->CreateOnSuccessLog($this->signature, '貨櫃到廠超時未處裡檢查');
        } catch (Exception $ex) {
            $this->scheduleService->CreateOnFailureLog($this->signature, '貨櫃到廠超時未處裡檢查', $ex->getMessage());
        }
    }

    //貨櫃到廠超時未處二次檢查
    private function ContainerImportCheck2()
    {
        try {
            $days = 30;
            $s_date = Carbon::today()->addDays(-$days)->format('Y-m-d');
            $db = ContainerImport::where(function ($q) {
                $q->orWhereNull('remittance_date')->orWhereNull('remittance_account')->orWhere('remittance_amount', 0);
            })
                ->where('date', '<=', $s_date)
                ->get();
            if (!empty($db) && count($db) > 0) {
                $mailData =
                    [
                        'subject' => '貨櫃到廠清單',
                        'caption' => "貨櫃到廠超過30天，未確實處理",
                        'thead' => ['流水號', '到貨日期',  '匯款日期', '匯款帳號', '匯款金額'],
                    ];
                $data = [];
                foreach ($db as $d) {
                    $data[] = [
                        $d->id,
                        join(',', $d->shipdate_list),
                        $d->remittance_date,
                        $d->remittance_account,
                        $d->remittance_amount,
                    ];
                }
                $mailData['tbody'] = $data;
                $mails = $this->usersEmail->whereIn('name', ['fin', 'max'])->pluck('email')->toArray();
                if (!empty($mails)) {
                    Mail::to($mails)->send(new TableMail($mailData));
                }
            }

            $this->scheduleService->CreateOnSuccessLog($this->signature, '貨櫃到廠超時未處裡二次檢查');
        } catch (Exception $ex) {
            $this->scheduleService->CreateOnFailureLog($this->signature, '貨櫃到廠超時未處裡二次檢查', $ex->getMessage());
        }
    }
}
