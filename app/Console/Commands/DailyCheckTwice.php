<?php

namespace App\Console\Commands;

use Exception;
use Carbon\Carbon;
use App\Mail\TableMail;
use App\Models\TMS\Order;
use Illuminate\Console\Command;
use App\Services\ScheduleService;
use App\Services\OrderSampleService;
use Illuminate\Support\Facades\Mail;

class DailyCheckTwice extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'daily:twice-check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '每日檢查';

    protected $scheduleService;
    protected $orderSampleService;
    private $usersEmail;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->scheduleService = new ScheduleService();
        $this->orderSampleService = new OrderSampleService();
        $this->usersEmail = $this->scheduleService->GetUsersEmail();

        $this->OrderExpiredCheck();
        $this->SpOrderCheck();
    }

    //訂單過期通知
    private function OrderExpiredCheck()
    {
        try {
            $day = 3; //蝦皮3日到期
            $yy = substr(Carbon::today()->addDays(-$day)->format('Y'), 0, 2);
            $date = substr(Carbon::today()->addDays(-$day)->format('Ymd'), 2, 6);
            $db = Order::select('OCOD1', 'OCOD4', 'OCCOD', 'OCNAM')
                ->where('OCANC', '!=', '訂單取消')
                ->where('OCOD4', 'LIKE', '%' . $date . '%')
                ->whereDoesntHave('Posein', function ($q) { //無銷貨單號和物流單號
                    $q
                        ->select('PCOD1')
                        ->where(function ($q2) {
                            $q2->where('ConsignTran', '!=', '')
                                ->orWhere('TransportCode', 'MF'); //廠商直寄沒有物流單號
                        });
                })
                ->get();
            if (!empty($db) && count($db) > 0) {
                $mailData =
                    [
                        'subject' => '訂單過期通知',
                        'caption' => "明日將過期訂單",
                        'thead' => ['序', '訂單編號', '原始訂單編號', '平台簡稱', '平台訂單成立日期'],
                    ];
                $data = [];
                $index = 1;
                foreach ($db as $d) {
                    $order_date = '';
                    if ($d->cust == 'S002') { //官網格式#yyyymmdd
                        $order_date = substr($d->original_no, 1, 8);
                    } else { //#yymmdd
                        $order_date = $yy . substr($d->original_no, 0, 6);
                    }
                    if ($order_date != ($yy . $date)) { //日期不同
                        continue;
                    }
                    $order_date = substr_replace($order_date, '/', 4, 0);
                    $order_date = substr_replace($order_date, '/', 7, 0);
                    $data[] = [
                        $index++,
                        $d->order_no,
                        $d->original_no,
                        $d->cust_name,
                        $order_date
                    ];
                }
                $mailData['tbody'] = $data;
                $mails = $this->usersEmail->whereIn('name', ['ginny', 'yen'])->pluck('email')->toArray();
                if (!empty($mails)) {
                    Mail::to($mails)->send(new TableMail($mailData));
                }
            }

            $this->scheduleService->CreateOnSuccessLog($this->signature, '訂單過期通知');
        } catch (Exception $ex) {
            $this->scheduleService->CreateOnFailureLog($this->signature, '訂單過期通知', $ex->getMessage());
        }
    }

    /**
     * 特殊訂單未匯入檢查
     *
     * @return void
     */
    private function SpOrderCheck()
    {
        try {
            $db = $this->orderSampleService->GetSpOrderCheck();

            if (!empty($db) && count($db) > 0) {
                $mailData =
                    [
                        'subject' => '訂單超過48小時未處理通知',
                        'caption' => '排除以下情況1.有銷貨紀錄且有開立發票。2.有退貨紀錄。3.訂單取消',
                        'thead' => ['流水號', '訂單成立時間', '原始訂單編號', '客戶簡稱', '經過天數'],
                    ];
                $data = [];
                foreach ($db->sortBy('order_date') as $d) {
                    $data[] = [
                        $d->id,
                        $d->order_date,
                        $d->order_no,
                        $d->src_name,
                        $d->elapsedDays,
                    ];
                }
                $mailData['tbody'] = $data;
                $mails = $this->usersEmail->whereIn('name', ['ginny', 'yen'])->pluck('email')->toArray();
                if (!empty($mails)) {
                    Mail::to($mails)->send(new TableMail($mailData));
                }
            }

            $this->scheduleService->CreateOnSuccessLog($this->signature, '特殊訂單超時未處理檢查');
        } catch (Exception $ex) {
            $this->scheduleService->CreateOnFailureLog($this->signature, '特殊訂單超時未處理檢查', $ex->getMessage());
        }
    }
}
