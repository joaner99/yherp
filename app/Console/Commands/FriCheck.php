<?php

namespace App\Console\Commands;

use Exception;
use Carbon\Carbon;
use App\Mail\TableMail;
use App\Models\yherp\AdProfitM;
use App\Services\AdService;
use Illuminate\Console\Command;
use App\Services\ScheduleService;
use Illuminate\Support\Facades\Mail;

class FriCheck extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fri:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '每週五檢查';

    protected $scheduleService;
    private $usersEmail;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->scheduleService = new ScheduleService();
        $this->usersEmail = $this->scheduleService->GetUsersEmail();

        $this->AdConfigCheck();
    }

    private function AdConfigCheck()
    {
        try {
            $s_date = '2023-03-03';
            $e_date = Carbon::today()->modify('-1 day')->format('Y-m-d');
            $days = $this->scheduleService->GetEachDay($s_date, $e_date);
            $db = AdProfitM::whereBetween('src_date', [$s_date, $e_date])->get();
            $data = array();
            foreach ($days as $day) {
                foreach (AdService::shopee_shop as $key => $shop) {
                    if ($db->where('src_date', $day)->where('shop_id', $key)->count() == 0) {
                        $data[] = [
                            $day,
                            $shop
                        ];
                    }
                }
            }
            if (!empty($data)) {
                $mailData =
                    [
                        'subject' => '廣告參數檢查',
                        'caption' => '未匯入清單',
                        'thead' => ['日期', '平台'],
                        'tbody' => $data
                    ];
                $mails = $this->usersEmail->whereIn('name', ['yahsunk', '林盈沁'])->pluck('email')->toArray();
                if (!empty($mails)) {
                    Mail::to($mails)->send(new TableMail($mailData));
                }
            }
            $this->scheduleService->CreateOnSuccessLog($this->signature, '廣告參數檢查');
        } catch (Exception $ex) {
            $this->scheduleService->CreateOnFailureLog($this->signature, '廣告參數檢查', $ex->getMessage());
        }
    }
}
