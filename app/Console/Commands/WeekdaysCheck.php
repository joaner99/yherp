<?php

namespace App\Console\Commands;

use Exception;
use Carbon\Carbon;
use App\Mail\TableMail;
use Illuminate\Console\Command;
use App\Services\ScheduleService;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class WeekdaysCheck extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'weekdays:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '每工作日檢查';

    protected $scheduleService;
    private $usersEmail;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->scheduleService = new ScheduleService();
        $this->usersEmail = $this->scheduleService->GetUsersEmail();
        $this->GetGrossProfit();
        $this->CheckCanSale();
    }

    //檢查可銷售量表
    private function CheckCanSale()
    {
        try {
            $items = [
                '11010020002',
                '11010020003',
                '11010020004',
                '11010020005',
                '11010020008',
                '11010020011',
                '11010020013',
                '11010020014',
                '11010020015',
                '11010020016',
                '11010020017',
                '11010020018',
                '11010020019',
                '11010014072',
                '11010014073',
                '11010014074',
                '11010014075',
                '11010014076',
                '11010014077',
                '11010014078',
                '11010014079',
                '11010014080',
                '11010014088',
                '11010014089',
                '11010014090',
                '11010014094',
                '11010014064',
                '11010014093',
                '11010014081',
                '11010014082',
                '11010014083',
                '11010014084',
                '11010014085',
                '11010014086',
                '11010046001',
                '11010046002',
                '11010046003',
                '11010046004',
                '11010046005',
                '11010046006',
                '11010046007',
                '11010046008',
                '11010046009',
                '11010046010',
                '11010046011',
                '11010046012',
                '11010046013'
            ];
            $db = $this->scheduleService->AnalyzeCanSale($items);
            if (!empty($db) && count($db) > 0) {
                $mailData =
                    [
                        'subject' => '地板可銷售量通知',
                        'caption' => "SPC、LVT、收編條",
                        'thead' => ['料號', '品名', '(主倉+出貨)庫存', 'TMS訂單需求數', '籃子需求數', '(庫存-需求)可銷售數'],
                    ];
                $data = [];
                foreach ($db as $d) {
                    $data[] = [
                        $d->ICODE,
                        $d->INAME,
                        number_format($d->Stock),
                        number_format($d->OrderNeed),
                        number_format($d->BasketNeed),
                        number_format($d->CanSale),
                    ];
                }
                $mailData['tbody'] = $data;
                $mails = $this->usersEmail->whereIn('name', ['max', 'ginny', 'yen', 'yahsunk', '林盈沁'])->pluck('email')->toArray();
                if (!empty($mails)) {
                    Mail::to($mails)->send(new TableMail($mailData));
                }
            }
            $this->scheduleService->CreateOnSuccessLog($this->signature, '地板可銷售量通知');
        } catch (Exception $ex) {
            $this->scheduleService->CreateOnFailureLog($this->signature, '地板可銷售量通知', $ex->getMessage());
        }
    }

    /**
     * 取得毛利日報表
     *
     * @return void
     */
    private function GetGrossProfit()
    {
        try {
            //取設定檔參數
            $config = $this->scheduleService->configService->GetSysConfig('sche_gross_profit');
            $standard_min = $config->firstWhere('value1', 'standard_min')->value2 ?? 20; //毛利率下限

            $today = Carbon::today();
            if ($today->isWeekend()) { //非工作日排除
                return;
            } elseif ($today->dayOfWeek == 1) { //星期一，需取禮拜五資料
                $today->modify('-3 day');
            } else {
                $today->modify('-1 day');
            }

            $twDate = $this->scheduleService->GetTWDateStr($today);
            $db = DB::table('ORDET AS M')
                ->select('M.OCOD1', 'M.OCCOD', 'M.OCNAM', 'M.OPERS', 'M.OSOUR AS MOSOUR', 'M.OEARN')
                ->addSelect('D.OICOD', 'D.OINAM', 'D.OINUM', 'D.OUNIT', 'D.OTOTA', 'D.OSOUR')
                ->addSelect('I.ISOUR')
                ->join('ORSUB AS D', 'D.OCOD1', 'M.OCOD1')
                ->join('ITEM AS I', 'I.ICODE', 'D.OICOD')
                ->where('M.ODATE', $twDate)
                ->where('M.OCANC', '!=', '訂單取消')
                ->where('M.OPERS', '!=', '#.##')
                ->whereNotIn('M.OCCOD', ['x001', 'x002']) //排除包材、半成品領料
                ->orderBy('M.OCOD1')
                ->get();
            if (!empty($db) && count($db) > 0) {
                $mailData =
                    [
                        'subject' => '毛利率日報表',
                        'caption' => "前一天工作日訂單，亞銘：低於-5%或高於5%，其他：低於{$standard_min}%，排除包材、半成品客代",
                        'thead' => ['客戶簡稱', '訂單單號', '訂單毛利率', '成本總價', '利潤', '料號', '名稱', '數量', '單價', '小計', '成本', '成本小計'],
                    ];
                $data = [];
                foreach ($db as $d) {
                    $gmp = $d->OPERS;
                    if (!is_numeric($gmp)) {
                        continue;
                    }
                    if ($d->OCCOD == 'D001') { //亞銘燈飾
                        if (-5 <= $gmp && $gmp <= 5) {
                            continue;
                        }
                    } else {
                        if ($gmp >= $standard_min) {
                            continue;
                        }
                    }
                    $data[] = [
                        $d->OCNAM,
                        $d->OCOD1,
                        number_format($gmp, 2) . '%',
                        number_format($d->MOSOUR),
                        number_format($d->OEARN),
                        $d->OICOD,
                        $d->OINAM,
                        number_format($d->OINUM),
                        number_format($d->OUNIT),
                        number_format($d->OTOTA),
                        number_format($d->ISOUR),
                        number_format($d->OSOUR),
                    ];
                }
                $mailData['tbody'] = $data;
                $mails = $this->usersEmail->whereIn('name', ['max', 'fin'])->pluck('email')->toArray();
                if (!empty($mails)) {
                    Mail::to($mails)->send(new TableMail($mailData));
                }
            }
            $this->scheduleService->CreateOnSuccessLog($this->signature, '毛利率日報表');
        } catch (Exception $ex) {
            $this->scheduleService->CreateOnFailureLog($this->signature, '毛利率日報表', $ex->getMessage());
        }
    }
}
