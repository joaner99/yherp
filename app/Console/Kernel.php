<?php

namespace App\Console;

use Illuminate\Support\Facades\Log;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        //每日紀錄
        $schedule
            ->command('daily:save')
            ->dailyAt('09:00')
            ->withoutOverlapping()
            ->before(function () {
                Log::info("daily:save-開始執行");
            })
            ->onSuccess(function () {
                Log::info("daily:save-執行成功");
            })
            ->onFailure(function () {
                Log::info("daily:save-執行失敗");
            });

        //每周紀錄
        $schedule
            ->command('weekly:save')
            ->weeklyOn(7, '09:00')
            ->withoutOverlapping()
            ->before(function () {
                Log::info("weekly:save-開始執行");
            })
            ->onSuccess(function () {
                Log::info("weekly:save-執行成功");
            })
            ->onFailure(function () {
                Log::info("weekly:save-執行失敗");
            });

        //每月紀錄
        $schedule
            ->command('monthly:save')
            ->monthlyOn(1)
            ->withoutOverlapping()
            ->before(function () {
                Log::info("monthly:save-開始執行");
            })
            ->onSuccess(function () {
                Log::info("monthly:save-執行成功");
            })
            ->onFailure(function () {
                Log::info("monthly:save-執行失敗");
            });

        //每日檢查
        $schedule
            ->command('daily:check')
            ->dailyAt('10:00')
            ->withoutOverlapping()
            ->before(function () {
                Log::info("daily:check-開始執行");
            })
            ->onSuccess(function () {
                Log::info("daily:check-執行成功");
            })
            ->onFailure(function () {
                Log::info("daily:check-執行失敗");
            });

        //每日檢查2
        $schedule
            ->command('daily:twice-check')
            ->dailyAt('10:00')
            ->withoutOverlapping()
            ->before(function () {
                Log::info("daily:twice-check-開始執行");
            })
            ->onSuccess(function () {
                Log::info("daily:twice-check-執行成功");
            })
            ->onFailure(function () {
                Log::info("daily:twice-check-執行失敗");
            });

        //每日檢查2
        $schedule
            ->command('daily:twice-check')
            ->dailyAt('16:00')
            ->withoutOverlapping()
            ->before(function () {
                Log::info("daily:twice-check-開始執行");
            })
            ->onSuccess(function () {
                Log::info("daily:twice-check-執行成功");
            })
            ->onFailure(function () {
                Log::info("daily:twice-check-執行失敗");
            });
        //工作日檢查
        $schedule
            ->command('weekdays:check')
            ->weekdays()
            ->at('10:00')
            ->withoutOverlapping()
            ->before(function () {
                Log::info("weekdays:check-開始執行");
            })
            ->onSuccess(function () {
                Log::info("weekdays:check-執行成功");
            })
            ->onFailure(function () {
                Log::info("weekdays:check-執行失敗");
            });
        //每週五
        $schedule
            ->command('fri:check')
            ->weeklyOn(5, '18:00')
            ->withoutOverlapping()
            ->before(function () {
                Log::info("fri:check-開始執行");
            })
            ->onSuccess(function () {
                Log::info("fri:check-執行成功");
            })
            ->onFailure(function () {
                Log::info("fri:check-執行失敗");
            });

        // $schedule->command('inspire')
        //          ->hourly();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }

    protected function scheduleTimezone()
    {
        return 'Asia/Taipei';
    }
}
