<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithTitle;
use PhpOffice\PhpSpreadsheet\Shared\Font;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;

class BaseExport implements FromArray, ShouldAutoSize, WithStrictNullComparison, WithTitle
{
    protected $data = null;
    protected $title = null;

    //构造函数传值
    public function __construct($data, $title = null)
    {
        $this->data = $data;
        $this->title = $title;
        Font::setTrueTypeFontPath("C:/Windows/Fonts/");
        Font::setAutoSizeMethod(Font::AUTOSIZE_METHOD_EXACT);
    }

    //数组转集合
    public function array(): array
    {
        return $this->data;
    }

    public function title(): string
    {
        if (empty($this->title)) {
            return '未分類';
        } else {
            return $this->title;
        }
    }
}
