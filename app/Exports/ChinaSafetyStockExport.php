<?php

namespace App\Exports;

use App\Services\StockService;
use PhpOffice\PhpSpreadsheet\Shared\Font;
use App\Models\yherp\LogSafetyStockExport;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;

class ChinaSafetyStockExport implements FromCollection, ShouldAutoSize, WithHeadings, WithStrictNullComparison
{
    protected $items = null;

    //构造函数传值
    public function __construct($items)
    {
        $this->items = $items;
        Font::setTrueTypeFontPath("C:/Windows/Fonts/");
        Font::setAutoSizeMethod(Font::AUTOSIZE_METHOD_EXACT);
    }

    //数组转集合
    public function collection()
    {
        $result = [];
        $data = (new StockService())->GetUnsafetyStock($this->items);
        $idx = 1;
        $log = [];
        foreach ($data as $d) {
            $log[] = ['item_no' => $d['item_no']];
            $result[] = [
                $idx++,
                $d['item_no'],
                $d['item_name'],
                $d['order_require'],
                $d['m_stock'],
                $d['s_stock'],
                $d['stock'],
                $d['safety'],
                $d['status']
            ];
        }
        LogSafetyStockExport::insert($log);

        return collect($result);
    }

    public function headings(): array
    {
        return [
            "序",
            "料號",
            "品名",
            "訂單需求",
            "主倉庫存",
            "出貨庫存",
            "總庫存(主+出)",
            "安全庫存",
            "安庫狀態",
        ];
    }
}
