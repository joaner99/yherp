<?php

namespace App\Exports;

use App\Models\TMS\Item;
use App\Models\yherp\ProductErpM;
use PhpOffice\PhpSpreadsheet\Shared\Font;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class ErpPrintExport implements FromCollection, ShouldAutoSize, WithHeadings
{
    protected $id = null;

    //构造函数传值
    public function __construct($id)
    {
        $this->id = $id;
        Font::setTrueTypeFontPath("C:/Windows/Fonts/");
        Font::setAutoSizeMethod(Font::AUTOSIZE_METHOD_EXACT);
    }

    //数组转集合
    public function collection()
    {
        $result = [];
        $db = ProductErpM::with('Details')->find($this->id);
        foreach ($db->Details as $d) {
            $item_no = Item::where('INAME', $d->INAME)->first()->ICODE ?? '';
            if (empty($item_no)) {
                continue;
            }
            $result[] = [
                $item_no,
                $d->INAME,
            ];
        }

        return collect($result);
    }

    public function headings(): array
    {
        return [
            "產品代號",
            "產品名稱",
        ];
    }
}
