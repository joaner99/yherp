<?php

namespace App\Exports;

use App\Exports\BaseExport;
use PhpOffice\PhpSpreadsheet\Shared\Font;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class ChinaSalesExport implements WithMultipleSheets
{
    protected $data = null;

    //构造函数传值
    public function __construct($data)
    {
        $this->data = $data;
        Font::setTrueTypeFontPath("C:/Windows/Fonts/");
        Font::setAutoSizeMethod(Font::AUTOSIZE_METHOD_EXACT);
    }

    /**
     * @return array
     */
    public function sheets(): array
    {
        $sheets = [];
        foreach ($this->data as $kind3 => $g) {
            $cols = ['大類', '小類', '料號', '品名', '庫存', '採購未進貨'];
            for ($i = 1; $i <= 12; $i++) {
                $cols[] = $i . '月銷售';
            }
            for ($i = 1; $i <= 12; $i++) {
                $cols[] = '去年' . $i . '月銷售';
            }
            $cols[] = '去年平均銷售';
            array_unshift($g, $cols);
            $sheets[] = new BaseExport($g, $kind3);
        }

        return $sheets;
    }
}
