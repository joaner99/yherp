<?php

namespace App\Exports;

use App\Models\TMS\Item;
use App\Models\yherp\ProductErpM;
use Illuminate\Support\Facades\DB;
use App\Models\warranty_web\ItemConfig;
use PhpOffice\PhpSpreadsheet\Shared\Font;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class TMSItemExport implements FromCollection, ShouldAutoSize, WithHeadings
{
    protected $ids = null;

    //构造函数传值
    public function __construct($ids)
    {
        $this->ids = $ids;
        Font::setTrueTypeFontPath("C:/Windows/Fonts/");
        Font::setAutoSizeMethod(Font::AUTOSIZE_METHOD_EXACT);
    }

    //数组转集合
    public function collection()
    {
        $result = [];
        $db = ProductErpM::with('Details')->whereIn('id', $this->ids)->get();
        foreach ($db as $m) {
            foreach ($m->Details->groupBy(function ($item, $key) {
                return $item->ITCOD . $item->ITCO2 . $item->ITCO3;
            }) as $key => $group) {
                if (empty($key)) {
                    continue;
                }
                $now =  Item::select(DB::raw('SUBSTRING(ICODE,9,3) AS ICODE'))->whereBetween('ICODE', [$key . '000', $key . '999'])->orderByDesc('ICODE')->first()->ICODE ?? 0;
                $seq = $now + 1;
                foreach ($group as $d) {
                    if (!$this->valid($d)) {
                        continue;
                    }
                    $item_no = "{$d->ITCOD}{$d->ITCO2}{$d->ITCO3}" . str_pad($seq, 3, "0", STR_PAD_LEFT);
                    $result[] = [
                        $item_no,
                        $d->INAME,
                        $d->IINAME,
                        $d->ICONS,
                        $d->ITCOD,
                        $d->ITCO2,
                        $d->ITCO3,
                        $d->SiseCost,
                        $d->IPOIN,
                        $d->T11_11
                    ];
                    //保固新增
                    ItemConfig::updateOrCreate(
                        [
                            'id' => $item_no,
                        ],
                        [
                            'warranty_y' => $d->warranty_y ?? 0,
                            'warranty_m' => $d->warranty_m ?? 0,
                            'warranty_d' => $d->warranty_d ?? 0,
                        ]
                    );
                    $seq++;
                }
            }
        }

        return collect($result);
    }

    public function headings(): array
    {
        return [
            "產品代號",
            "產品名稱",
            "次品名",
            "產品型態",
            "大類類別代號",
            "中類類別代號",
            "小類類別代號",
            "採購成本",
            "單位",
            "產地國別",
        ];
    }

    //驗證
    private function valid($item): bool
    {
        if (
            empty($item->ITCOD) || empty($item->ITCO2) || empty($item->ITCO3) || //大中小類
            empty($item->IPOIN) || empty($item->ICONS) || empty($item->INAME) || empty($item->IINAME) || //單位/產品型態/產品名稱/次品名
            empty($item->T11_11) || !isset($item->SiseCost) //產地國別/採購成本
        ) {
            return false;
        } else {
            return true;
        }
    }
}
