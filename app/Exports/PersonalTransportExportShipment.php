<?php

namespace App\Exports;

use Exception;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Worksheet\PageSetup;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class PersonalTransportExportShipment implements WithEvents, WithStyles
{
    protected $data = null;

    //构造函数传值
    public function __construct($data)
    {
        $this->data = $data;
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                try {
                    $sheet = $event->sheet->getDelegate();
                    $sheet->getPageSetup()->setPaperSize(PageSetup::PAPERSIZE_A4);
                    $sheet->getPageSetup()->setFitToWidth(1);
                    foreach (['A', 'B', 'C', 'D', 'E', 'F'] as $c) {
                        $sheet->getColumnDimension($c)->setAutoSize(true);
                    }
                    $data = $this->data;
                    $header = $data[0];
                    $data = collect($data);
                    $columns = $this->GetColumns(collect($header));
                    //出車日期&出車人員
                    $sheet->setCellValueByColumnAndRow(1, 1, '出車日期');
                    $sheet->setCellValueByColumnAndRow(2, 1, '出車人員');
                    $sheet->setCellValueByColumnAndRow(3, 1, '出車車輛');
                    $date_idx = array_search('出車日期', $header);
                    $driver_idx = array_search('出車人員', $header);
                    $car_idx = array_search('出車車輛', $header);
                    $date_list = array_unique($data->slice(1)->pluck($date_idx)->toArray());
                    $driver_list = array_unique($data->slice(1)->pluck($driver_idx)->toArray());
                    $car_list = array_unique($data->slice(1)->pluck($car_idx)->toArray());
                    $sheet->setCellValueByColumnAndRow(1, 2, join(',', $date_list));
                    $sheet->setCellValueByColumnAndRow(2, 2, join(',', $driver_list));
                    $sheet->setCellValueByColumnAndRow(3, 2, join(',', $car_list));
                    //資料
                    $sheet->setCellValueByColumnAndRow(1, 3, '原始訂單編號');
                    $sheet->setCellValueByColumnAndRow(2, 3, '親送備註');
                    $sheet->setCellValueByColumnAndRow(3, 3, '收件資訊');
                    $sheet->setCellValueByColumnAndRow(4, 3, '類型');
                    $sheet->setCellValueByColumnAndRow(5, 3, '品名');
                    $sheet->setCellValueByColumnAndRow(6, 3, '數量');
                    $row_offset = 0;
                    $data_count = 0;
                    foreach ($data->slice(1)->groupBy($columns['original_no']) as $original_no => $group) {
                        $rows = $group->first();
                        $sheet->setCellValueByColumnAndRow(1, 4 + $row_offset, $rows[$columns['original_no']]);
                        $sheet->setCellValueByColumnAndRow(2, 4 + $row_offset, $rows[$columns['remark']]);
                        $sheet->setCellValueByColumnAndRow(3, 4 + $row_offset, $rows[$columns['receiver_name']]);
                        $sheet->setCellValueByColumnAndRow(3, 5 + $row_offset, $rows[$columns['receiver_phone']]);
                        $sheet->setCellValueByColumnAndRow(3, 6 + $row_offset, $rows[$columns['receiver_addr']]);
                        foreach ($group as $g) {
                            $sheet->setCellValueByColumnAndRow(4, 4 + $row_offset, $g[$columns['item_kind3_name']]);
                            $sheet->setCellValueByColumnAndRow(5, 4 + $row_offset, $g[$columns['item_name']]);
                            $sheet->setCellValueByColumnAndRow(6, 4 + $row_offset, $g[$columns['qty']]);
                            $row_offset++;
                        }
                        if (count($group) < 3) { //明細數量小魚聯絡人欄位數量
                            $row_offset += (3 - count($group));
                        }
                    }
                    $data_count = $row_offset;
                    //類別標題
                    $item_name_list = array_unique($data->slice(1)->pluck($columns['item_name'])->toArray());
                    $item_kind3_name_list = array_unique($data->slice(1)->pluck($columns['item_kind3_name'])->toArray());
                    sort($item_kind3_name_list);
                    $col_offset = 0;
                    foreach ($item_kind3_name_list as $item_kind3_name) {
                        $sheet->setCellValueByColumnAndRow(2 + $col_offset, 5 + $row_offset, $item_kind3_name);
                        $col_offset++;
                    }
                    //品名
                    foreach ($data->slice(1)->sortBy($columns['item_name'])->groupBy($columns['item_name']) as $item_name => $group) {
                        $sheet->setCellValueByColumnAndRow(1, 6 + $row_offset, $item_name);
                        $col_offset = 0;
                        foreach ($item_kind3_name_list as $item_kind3_name) {
                            $qty = $group->where($columns['item_kind3_name'], $item_kind3_name)->sum($columns['qty']);
                            $sheet->setCellValueByColumnAndRow(2 + $col_offset, 6 + $row_offset, $qty);
                            $col_offset++;
                        }
                        $col_offset++;
                        $row_offset++;
                    }
                    //數量統計
                    $sheet->setCellValueByColumnAndRow(1, 6 + $row_offset, '數量統計');
                    $col_offset = 0;
                    foreach ($item_kind3_name_list as $item_kind3_name) {
                        $qty = $data->where($columns['item_kind3_name'], $item_kind3_name)->sum($columns['qty']);
                        $sheet->setCellValueByColumnAndRow(2 + $col_offset, 6 + $row_offset, $qty);
                        $col_offset++;
                    }
                    //合併欄位
                    $merge_s_idx = 4;
                    $merge_cells = [];
                    foreach ($data->slice(1)->groupBy($columns['original_no']) as $group) {
                        $c = count($group);
                        $c = ($c < 3 ? 3 : $c);
                        $merge_e_idx = $merge_s_idx + $c - 1;
                        if ($merge_e_idx > $merge_s_idx) {
                            $merge_cells[] = "A{$merge_s_idx}:A{$merge_e_idx}";
                            $merge_cells[] = "B{$merge_s_idx}:B{$merge_e_idx}";
                        }
                        $merge_s_idx += $c;
                    }
                    $sheet->setMergeCells($merge_cells);
                    //欄位框線
                    $styles = [
                        'A1:C2',
                        'A3:F' . (3 + $data_count),
                        'A' . (3 + $data_count + 2) . ':' . $this->IntToChr(count($item_kind3_name_list)) . (3 + $data_count + 2 + count($item_name_list) + 1),
                    ];
                    foreach ($styles as $style) {
                        $sheet->getStyle($style)->applyFromArray([
                            'borders' => [
                                'allBorders' => [
                                    'borderStyle' => Border::BORDER_THIN,
                                    'color' => ['argb' => '000000'],
                                ],
                            ],
                        ]);
                    }
                } catch (Exception $ex) {
                }
            }
        ];
    }
    //取得欄位索引
    public function GetColumns($header)
    {
        $config = [
            'original_no' => '原始訂單編號',
            'remark' => '親送備註',
            'receiver_name' => '收件者姓名',
            'receiver_addr' => '收件者地址',
            'receiver_phone' => '收件者電話',
            'item_kind3_name' => '類型',
            'item_name' => '品名',
            'qty' => '數量'
        ];
        $result = [];
        foreach ($config as $key => $idx) {
            $result[$key] = false;
            foreach ($header as $header_idx => $h) {
                if (preg_replace('/\s(?=)/', '', $h) == preg_replace('/\s(?=)/', '', $idx)) { //去空白比較
                    $result[$key] = $header_idx;
                    $header->forget($header_idx);
                    continue 2;
                }
            }
        }
        return $result;
    }

    public function styles(Worksheet $sheet)
    {
        $result = [
            'A:K' => [
                'font' => [
                    'name' => 'Verdana',
                    'size' => 12,
                ],
                'alignment' => [
                    'horizontal' => Alignment::HORIZONTAL_LEFT,
                    'vertical' => Alignment::VERTICAL_TOP
                ]
            ],
            'B:B' => [
                'alignment' => [
                    'wrapText' => true,
                ]
            ],
            'C:C' => [
                'alignment' => [
                    'wrapText' => true,
                ]
            ]
        ];
        return $result;
    }
    private function IntToChr($index, $start = 65)
    {
        $str = '';
        if (floor($index / 26) > 0) {
            $str .= $this->IntToChr(floor($index / 26) - 1);
        }
        return $str . chr($index % 26 + $start);
    }
}
