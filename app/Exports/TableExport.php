<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use PhpOffice\PhpSpreadsheet\Shared\Font;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class TableExport implements FromView, ShouldAutoSize
{
    protected $caption; //表格標題
    protected $thead; //表格標頭
    protected $tbody; //表格內容

    function __construct($caption, $thead, $tbody)
    {
        $this->caption = $caption;
        $this->thead = $thead;
        $this->tbody = $tbody;
        Font::setTrueTypeFontPath("C:/Windows/Fonts/");
        Font::setAutoSizeMethod(Font::AUTOSIZE_METHOD_EXACT);
    }

    /**
     * @return View
     */
    public function view(): View
    {
        $tableData = [
            'caption' => $this->caption,
            'thead' =>  $this->thead,
            'tbody' => $this->tbody
        ];
        return view('layouts.table', $tableData);
    }
}
