<?php

namespace App\Exports;

use App\Services\BaseService;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithEvents;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use Maatwebsite\Excel\Concerns\FromCollection;

class TransportExport implements FromCollection, WithEvents
{
    protected $data = null;
    private $driver = null;
    private $s_date = null;
    private $e_date = null;

    //构造函数传值
    public function __construct($driver, $s_date, $e_date, $data)
    {
        $this->driver = $driver;
        $this->s_date = $s_date;
        $this->e_date = $e_date;
        $this->data = $data;
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $sheet = $event->sheet->getDelegate();
                $count = count($sheet->toArray());
                $sheet->insertNewRowBefore(1, 7);

                $sheet->setCellValueByColumnAndRow(1, 1, '蝦皮賣場電話');
                $sheet->setCellValueByColumnAndRow(1, 2, '1店');
                $sheet->setCellValueByColumnAndRow(2, 2, '0918-235-123');
                $sheet->setCellValueByColumnAndRow(1, 3, '2店');
                $sheet->setCellValueByColumnAndRow(2, 3, '0967-104-701');
                $sheet->setCellValueByColumnAndRow(1, 4, '3店');
                $sheet->setCellValueByColumnAndRow(2, 4, '0909-631-908');

                $sheet->setCellValueByColumnAndRow(1, 6, '日期');
                $sheet->setCellValueByColumnAndRow(2, 6, $this->s_date);
                if ($this->e_date != $this->s_date) {
                    $sheet->setCellValueByColumnAndRow(3, 6, $this->e_date);
                }
                $sheet->setCellValueByColumnAndRow(1, 7, '超商');
                $sheet->setCellValueByColumnAndRow(2, 7, '數量');
                $sheet->setCellValueByColumnAndRow(3, 7, '已送達');

                //欄位合併
                $sheet->setMergeCells([
                    'A1:C1',
                    'A5:C5',
                    'A' . ($count + 8) . ':C' . ($count + 8)
                ]);
                for ($i = 1; $i < 4; $i++) {
                    $sheet->getColumnDimensionByColumn($i)->setWidth(15);
                }
                $sheet->setCellValueByColumnAndRow(1, $count + 9, '司機');
                $sheet->setCellValueByColumnAndRow(2, $count + 9, $this->driver);
                //欄位對齊、框線
                $sheet->getStyle('A1:C' . ($count + 9))->applyFromArray([
                    'alignment' => [
                        'horizontal' => Alignment::HORIZONTAL_LEFT,
                        'vertical' => Alignment::VERTICAL_CENTER,
                    ],
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => Border::BORDER_THIN,
                            'color' => ['argb' => '000000'],
                        ],
                    ],
                ]);
            }
        ];
    }

    public function collection()
    {
        $result = [];
        $total = 0;
        $sorted =  collect($this->data)->sortBy(function ($item, $key) {
            $sort = 0;
            switch ($key) {
                case 'Shop-7':
                    $sort = 1;
                    break;
                case 'Shop-L':
                    $sort = 2;
                    break;
                case 'Shop-S':
                    $sort = 3;
                    break;
                case 'Shop-OK':
                    $sort = 4;
                    break;
                case 'Shop-F':
                    $sort = 5;
                    break;
            }
            return $sort;
        });
        foreach ($sorted as $key => $val) {
            if (!in_array($key, BaseService::shop_trans)) { //蝦皮店到店、萊爾富、7-11、OK、全家
                continue;
            }
            $total += $val['Done'];
            $result[] = collect([
                'name' => $val['Data'][0]->TransportName,
                'qty' => number_format($val['Done']),
            ]);
        }
        $result[] = collect([
            'name' => '合計',
            'qty' => number_format($total),
        ]);
        return collect($result);
    }
}
