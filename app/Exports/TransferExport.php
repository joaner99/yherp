<?php

namespace App\Exports;

use DateTime;
use Carbon\Carbon;
use App\Models\TMS\MTRAN;
use App\Services\BaseService;
use App\Models\yherp\TransferCheckM;
use PhpOffice\PhpSpreadsheet\Shared\Font;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class TransferExport implements FromCollection, ShouldAutoSize, WithHeadings
{
    protected $id = null;

    //构造函数传值
    public function __construct($id)
    {
        $this->id = $id;
        Font::setTrueTypeFontPath("C:/Windows/Fonts/");
        Font::setAutoSizeMethod(Font::AUTOSIZE_METHOD_EXACT);
    }

    //数组转集合
    public function collection()
    {
        $result = [];
        $m = TransferCheckM::with('Details')->where('id', $this->id)->first();
        $tms_data = MTRAN::select('MBACK3')
            ->where('MBACK3', '!=', '')
            ->where('MBACK3', (string)$this->id)
            ->pluck('MBACK3')
            ->toArray();
        $baseService = new BaseService();
        $tw_date = $baseService->GetTWDateStr(Carbon::today());
        if (in_array($m->id, $tms_data)) { //TMS已建檔
            return collect();
        }
        foreach ($m->Details->groupBy('LogStock.item_no') as $item_no => $d) {
            $result[] = [
                $tw_date,
                'B001',
                'A001',
                $item_no,
                $d->sum('LogStock.qty'),
                '',
                '',
                '',
                $m->id
            ];
        }
        $m->update(['exported_at' => new DateTime()]);

        return collect($result);
    }

    public function headings(): array
    {
        return [
            '調撥日期',
            '來源倉庫',
            '目的倉庫',
            '產品代號',
            '數量',
            '產品備註',
            '單據備註1',
            '單據備註2',
            '單據備註3'
        ];
    }
}
