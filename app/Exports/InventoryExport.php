<?php

namespace App\Exports;

use Exception;
use App\Models\yherp\InventoryM;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\FromArray;
use PhpOffice\PhpSpreadsheet\Shared\Font;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Style\Border;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use PhpOffice\PhpSpreadsheet\Worksheet\PageSetup;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;

class InventoryExport implements FromArray, ShouldAutoSize, WithStyles, WithStrictNullComparison, WithEvents
{
    protected $id = null;
    protected $item_count = 0;

    //构造函数传值
    public function __construct($id)
    {
        $this->id = $id;
        Font::setTrueTypeFontPath("C:/Windows/Fonts/");
        Font::setAutoSizeMethod(Font::AUTOSIZE_METHOD_EXACT);
    }

    public function array(): array
    {
        $result = [];
        $id = $this->id;
        $m = InventoryM::with('Details')->where('id', $id)->first();
        $result[] = ['流水號', '預定盤點日期', '盤點時間'];
        $result[] = [$id, $m->appointment_date, $m->inverntory_time];
        $result[] = ['料號', '品名', '盤點主倉庫存', '盤點出貨倉庫存', 'TMS主倉庫存', 'TMS出貨倉庫存', '主倉差異', '出貨倉差異'];
        foreach ($m->Details as $d) {
            $result[] = [
                $d->item_no,
                $d->item_name,
                $d->main_stock ?? 0,
                $d->sale_stock,
                $d->tms_main_stock,
                $d->tms_sale_stock,
                $d->main_diff,
                $d->sale_diff,
            ];
            $this->item_count++;
        }
        return $result;
    }

    public function styles(Worksheet $sheet)
    {
        $result = [
            'A:H' => [
                'font' => [
                    'size' => 10,
                ],
            ],
            'A1:C2' => [
                'borders' => [
                    'allBorders' => [
                        'borderStyle' => Border::BORDER_THIN,
                        'color' => ['argb' => '000000'],
                    ],
                ],
            ],
            ('A3:H' . (3 + $this->item_count)) => [
                'borders' => [
                    'allBorders' => [
                        'borderStyle' => Border::BORDER_THIN,
                        'color' => ['argb' => '000000'],
                    ],
                ],
            ],
        ];
        return $result;
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                try {
                    $sheet = $event->sheet->getDelegate();
                    $sheet->getPageSetup()->setFitToWidth(1);
                    $sheet->getPageSetup()->setPaperSize(PageSetup::PAPERSIZE_A4);
                    $sheet->getPageSetup()->setOrientation(PageSetup::ORIENTATION_LANDSCAPE);
                    $sheet->getPageMargins()->setTop(0.75);
                    $sheet->getPageMargins()->setBottom(0.75);
                    $sheet->getPageMargins()->setLeft(0.25);
                    $sheet->getPageMargins()->setRight(0.25);
                } catch (Exception $ex) {
                }
            }
        ];
    }
}
