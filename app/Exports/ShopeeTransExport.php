<?php

namespace App\Exports;

use App\Models\yherp\OrderConfirmD;
use App\Models\yherp\OrderConfirmM;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\FromArray;
use PhpOffice\PhpSpreadsheet\Shared\Font;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class ShopeeTransExport implements FromArray, ShouldAutoSize
{
    protected $data = null;

    //构造函数传值
    public function __construct($data)
    {
        $this->data = $data;
        Font::setTrueTypeFontPath("C:/Windows/Fonts/");
        Font::setAutoSizeMethod(Font::AUTOSIZE_METHOD_EXACT);
    }

    //数组转集合
    public function array(): array
    {
        $order_no_list = collect($this->data)->slice(1)->pluck(1)->toArray();
        $exists_order = OrderConfirmD::whereIn('order_no', $order_no_list)
            ->whereHas('Main', function ($q) {
                $q->where('order_type', 10);
            })
            ->pluck('order_no')
            ->toArray();
        $create_order_list = array_diff($order_no_list, $exists_order);
        if (count($create_order_list) > 0) {
            $m = OrderConfirmM::create([
                'user_id' => Auth::user()->id,
                'order_type' => 10,
            ]);
            foreach ($create_order_list  as $order_no) {
                $m->Details()->create([
                    'order_no' => $order_no
                ]);
            }
        }
        return $this->data;
    }
}
