<?php

namespace App\Exports;

use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use PhpOffice\PhpSpreadsheet\Worksheet\PageSetup;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;

class QuotationExport implements WithEvents, WithStyles, WithColumnWidths, WithColumnFormatting
{
    protected $data = null;
    protected $item_count = 0;
    private const remark_rowspan = 5;

    public function __construct($data)
    {
        $this->data = $data;
        $this->item_count = $data->Details()->count();
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $data = $this->data;
                $offset = 0;
                //合併欄位
                $merge_cells = [
                    'A1:K1', 'A3:K3', 'A5:K5',
                    'B6:D6', 'F6:H6', 'J6:K6',
                    'B7:D7', 'F7:H7', 'J7:K7',
                    'B8:D8', 'F8:H8', 'J8:K8',
                    'B9:D9', 'F9:K9',
                    'B10:D10', 'E10:H10',
                ];

                $sheet = $event->sheet->getDelegate();
                $sheet->setCellValueByColumnAndRow(1, 1, '優居科技股份有限公司');
                $sheet->setCellValueByColumnAndRow(1, 3, '【報價單】');
                $sheet->setCellValueByColumnAndRow(1, 5, '基本資料');
                $sheet->getPageSetup()->setPaperSize(PageSetup::PAPERSIZE_A4);
                $sheet->getPageSetup()->setFitToWidth(1);
                $sheet->setCellValueByColumnAndRow(1, 6, '報價單號');
                $sheet->setCellValueByColumnAndRow(5, 6, '報價日期');
                $sheet->setCellValueByColumnAndRow(9, 6, '報價人員');
                $sheet->setCellValueByColumnAndRow(1, 7, '客戶編號');
                $sheet->setCellValueByColumnAndRow(5, 7, '公司名稱');
                $sheet->setCellValueByColumnAndRow(9, 7, '聯絡人');
                $sheet->setCellValueByColumnAndRow(1, 8, '公司電話');
                $sheet->setCellValueByColumnAndRow(5, 8, '公司傳真');
                $sheet->setCellValueByColumnAndRow(9, 8, '聯絡人電話');
                $sheet->setCellValueByColumnAndRow(1, 9, '聯絡人郵件');
                $sheet->setCellValueByColumnAndRow(5, 9, '公司地址');
                $sheet->setCellValueByColumnAndRow(1, 10, '項次');
                $sheet->setCellValueByColumnAndRow(2, 10, '名稱 / 品名 / 規格');
                $sheet->setCellValueByColumnAndRow(5, 10, '內容描述');
                $sheet->setCellValueByColumnAndRow(9, 10, '數量');
                $sheet->setCellValueByColumnAndRow(10, 10, '單價');
                $sheet->setCellValueByColumnAndRow(11, 10, '小計');
                $sheet->setCellValueExplicitByColumnAndRow(2, 6, $data->quote_no, DataType::TYPE_STRING);
                $sheet->setCellValueByColumnAndRow(6, 6, $data->quote_date); //報價日期
                $sheet->setCellValueByColumnAndRow(10, 6, $data->User->name); //報價人員
                $sheet->setCellValueByColumnAndRow(2, 7, ''); //客戶編號
                $sheet->setCellValueByColumnAndRow(6, 7, $data->company_name); //公司名稱
                $sheet->setCellValueByColumnAndRow(10, 7, $data->contact_name); //聯絡人員
                $sheet->setCellValueByColumnAndRow(2, 8, $data->company_phone); //公司電話
                $sheet->setCellValueByColumnAndRow(6, 8, $data->company_fax); //公司傳真
                $sheet->setCellValueByColumnAndRow(10, 8, $data->contact_phone); //聯絡人電話
                $sheet->setCellValueByColumnAndRow(2, 9, $data->contact_email); //聯絡人郵件
                $sheet->setCellValueByColumnAndRow(6, 9, $data->company_addr); //公司地址
                $item_offset = 0;
                foreach ($data->Details as $item) {
                    $row = 11 + $item_offset;
                    $merge_cells[] = 'B' . $row . ':D' . $row;
                    $merge_cells[] = 'E' . $row . ':H' . $row;
                    $sheet->setCellValueByColumnAndRow(1, $row, $item->item_seq); //商品序
                    $sheet->setCellValueByColumnAndRow(2, $row, $item->item_name); //商品名稱
                    $sheet->setCellValueByColumnAndRow(5, $row, $item->item_remark); //商品備註
                    $sheet->setCellValueByColumnAndRow(9, $row, number_format($item->item_qty)); //商品數量
                    $sheet->setCellValueByColumnAndRow(10, $row, number_format($item->item_price)); //商品單價
                    $sheet->setCellValueByColumnAndRow(11, $row, number_format($item->item_qty * $item->item_price)); //商品小計
                    $item_offset++;
                }
                $offset = $item_offset;
                $sheet->setCellValueByColumnAndRow(1, 12 + $offset, '費用');
                $sheet->setCellValueByColumnAndRow(1, 13 + $offset, '專案價');
                $sheet->setCellValueByColumnAndRow(1, 15 + $offset, '總計(含稅)');
                $sheet->setCellValueByColumnAndRow(1, 17 + $offset, '報價說明');
                $sheet->setCellValueByColumnAndRow(1, 18 + $offset, '1. 本報價單之有效期限為7天。');
                $sheet->setCellValueByColumnAndRow(5, 17 + $offset, '保固說明');
                $sheet->setCellValueByColumnAndRow(5, 18 + $offset, $data->warranty_remark);
                $sheet->setCellValueByColumnAndRow(9, 17 + $offset, '備註');
                $sheet->setCellValueByColumnAndRow(9, 18 + $offset, $data->remark);
                $merge_cells = array_merge($merge_cells, [
                    'A' . (11 + $item_offset) . ':K' . (11 + $item_offset),
                    'A' . (12 + $offset) . ':K' . (12 + $offset),
                    'A' . (13 + $offset) . ':B' . (13 + $offset),
                    'C' . (13 + $offset) . ':K' . (13 + $offset),
                    'A' . (14 + $offset) . ':B' . (14 + $offset),
                    'C' . (14 + $offset) . ':K' . (14 + $offset),
                    'A' . (15 + $offset) . ':B' . (15 + $offset),
                    'C' . (15 + $offset) . ':K' . (15 + $offset),
                    'A' . (16 + $offset) . ':K' . (16 + $offset),
                    'A' . (17 + $offset) . ':D' . (17 + $offset),
                    'E' . (17 + $offset) . ':H' . (17 + $offset),
                    'I' . (17 + $offset) . ':K' . (17 + $offset),
                ]);
                $sheet->setCellValueByColumnAndRow(3, 13 + $this->item_count, number_format($data->total_price)); //總價
                $sheet->setCellValueByColumnAndRow(3, 15 + $this->item_count, number_format($data->total_price)); //總價
                $merge_cells = array_merge($merge_cells, [
                    'A' . (18 + $offset) . ':D' . (18 + $offset + self::remark_rowspan - 1),
                    'E' . (18 + $offset) . ':H' . (18 + $offset + self::remark_rowspan - 1),
                    'I' . (18 + $offset) . ':K' . (18 + $offset + self::remark_rowspan - 1),
                ]);
                $offset += self::remark_rowspan - 1;
                $sheet->setCellValueByColumnAndRow(1, 20 + $offset, '付款方式');
                $sheet->setCellValueByColumnAndRow(6, 20 + $offset, '付款條件');
                $sheet->setCellValueByColumnAndRow(1, 21 + $offset, '銀行匯款');
                $sheet->setCellValueByColumnAndRow(1, 24 + $offset, 'ATM轉帳');
                $sheet->setCellValueByColumnAndRow(2, 24 + $offset, '銀行代碼：011');
                $sheet->setCellValueByColumnAndRow(2, 25 + $offset, '帳號：16102000034762');
                $sheet->setCellValueByColumnAndRow(2, 21 + $offset, '銀行：上海銀行 員林分行');
                $sheet->setCellValueByColumnAndRow(2, 22 + $offset, '帳號：16102000034762');
                $sheet->setCellValueByColumnAndRow(2, 23 + $offset, '戶名：優居科技股份有限公司');
                $sheet->setCellValueByColumnAndRow(6, 21 + $offset, '付款條件');
                $sheet->setCellValueByColumnAndRow(6, 25 + $offset, '付款期限');
                $sheet->setCellValueByColumnAndRow(7, 25 + $offset, '取貨前付清');
                $sheet->setCellValueByColumnAndRow(7, 21 + $offset, '項目');
                $sheet->setCellValueByColumnAndRow(9, 21 + $offset, '總計(含稅)');
                $sheet->setCellValueByColumnAndRow(10, 21 + $offset, '備註');
                $sheet->setCellValueByColumnAndRow(7, 22 + $offset, '一次付清：100％');
                $sheet->setCellValueByColumnAndRow(9, 22 + $offset, number_format($data->total_price));
                $sheet->setCellValueByColumnAndRow(1, 27 + $offset, '專案需求之特別說明');
                $merge_cells = array_merge($merge_cells, [
                    'A' . (19 + $offset) . ':K' . (19 + $offset),
                    'A' . (20 + $offset) . ':E' . (20 + $offset),
                    'F' . (20 + $offset) . ':K' . (20 + $offset),
                    'A' . (21 + $offset) . ':A' . (23 + $offset),
                    'A' . (24 + $offset) . ':A' . (25 + $offset),
                    'F' . (21 + $offset) . ':F' . (24 + $offset),
                    'G' . (21 + $offset) . ':H' . (21 + $offset),
                    'J' . (21 + $offset) . ':K' . (21 + $offset),
                    'G' . (22 + $offset) . ':H' . (24 + $offset),
                    'I' . (22 + $offset) . ':I' . (24 + $offset),
                    'J' . (22 + $offset) . ':K' . (24 + $offset),
                    'B' . (21 + $offset) . ':E' . (21 + $offset),
                    'B' . (22 + $offset) . ':E' . (22 + $offset),
                    'B' . (23 + $offset) . ':E' . (23 + $offset),
                    'B' . (24 + $offset) . ':E' . (24 + $offset),
                    'B' . (25 + $offset) . ':E' . (25 + $offset),
                    'G' . (25 + $offset) . ':K' . (25 + $offset),
                    'A' . (26 + $offset) . ':K' . (26 + $offset),
                    'A' . (27 + $offset) . ':K' . (27 + $offset),
                    'A' . (28 + $offset) . ':K' . (32 + $offset),
                ]);

                $sheet->setMergeCells($merge_cells);
            }
        ];
    }
    //
    public function styles(Worksheet $sheet)
    {
        $item_count = $this->item_count;
        $result = [
            'A:K' => [
                'font' => [
                    'name' => '微軟正黑體',
                    'size' => 12,
                ],
                'alignment' => [
                    'horizontal' => Alignment::HORIZONTAL_CENTER,
                    'vertical' => Alignment::VERTICAL_CENTER
                ]
            ],
            'A5:K' . (32 + $item_count + self::remark_rowspan - 1) => [
                'borders' => [
                    'allBorders' => [
                        'borderStyle' => Border::BORDER_THIN,
                        'color' => ['argb' => '000000']
                    ]
                ]
            ],
        ];
        //字型
        $result['A1'] = ['font' => ['size' => 16, 'bold' => true]];
        $result['A3'] = ['font' => ['size' => 20]];
        //填滿色彩
        $fill = [
            'A5', 'A10:K10', 'A' . (12 + $item_count),
            'A' . (17 + $item_count) . ':K' . (17 + $item_count),
            'A' . (20 + $item_count + self::remark_rowspan - 1) . ':K' . (20 + $item_count + self::remark_rowspan - 1),
            'A' . (27 + $item_count + self::remark_rowspan - 1) . ':K' . (27 + $item_count + self::remark_rowspan - 1),
        ];
        foreach ($fill as $cell) {
            $result[$cell] = ['fill' => ['fillType' => Fill::FILL_SOLID, 'startColor' => ['rgb' => 'D9D9D9']]];
        }

        return $result;
    }
    //欄寬
    public function columnWidths(): array
    {
        $fix = 1;
        return [
            'A' => 12.25 / $fix,
            'B' => 21.25 / $fix,
            'C' => 8.38 / $fix,
            'D' => 18.25 / $fix,
            'E' => 10.5 / $fix,
            'F' => 8.38 / $fix,
            'G' => 8.38 / $fix,
            'H' => 10.25 / $fix,
            'I' => 14.88 / $fix,
            'J' => 12.88 / $fix,
            'K' => 13.25 / $fix,
        ];
    }
    //格式
    public function columnFormats(): array
    {
        return [
            'B6' => DataType::TYPE_STRING,
        ];
    }
}
