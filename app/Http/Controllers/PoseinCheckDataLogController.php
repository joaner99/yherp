<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Services\BaseService;
use App\Services\StockService;
use App\Models\yherp\InventoryM;
use Illuminate\Support\Facades\Auth;

class PoseinCheckDataLogController extends Controller
{
    private $baseService = null;

    public function __construct(BaseService $baseService)
    {
        $this->baseService = $baseService;
    }

    //現場狀況
    public function index(Request $request)
    {
        $stock_transfer = collect((new StockService())->GetStockTransfer());
        $order_schedule = $this->baseService->GetOrderSchedule();
        $order_schedule['sales'] = $order_schedule['sales']->where('status', '!=', 0);
        $order = $this->baseService->GetOrderAll(); //待優化
        $inventory_count = InventoryM::where('appointment_date', Carbon::today()->format('Y-m-d'))->whereNull('inverntory_time')->count();
        if (Auth::user()->can('check_table_management')) {
            $transfer_count = $stock_transfer->filter(function ($item) {
                return $item['sale_need'] > 0 && $item['main_stock'] > 0;
            })->count() ?? 0;
            $purchase_count = $stock_transfer->filter(function ($item) {
                return $item['main_need'] > 0;
            })->count() ?? 0;
            $purchase_undone_count = $stock_transfer->filter(function ($item) {
                return $item['purchase_undone'] > 0;
            })->count() ?? 0;
            $orderWeight = $this->baseService->GetOverweightOrder();
            $order_tax_id = $this->baseService->GetOrderTaxID($request);
            $hct_remote_db = $this->baseService->GetHctRemote($request);

            $order_expired = empty($order['expired']) ? 0 : count($order['expired']); //過期單
            $orderWeight = empty($orderWeight) ? 0 : count($orderWeight['data']); //過大過重
            $order_tax_id = empty($order_tax_id) ? 0 : count($order_tax_id); //需統編
            $hct_remote = empty($hct_remote_db['data']) ? 0 : count($hct_remote_db['data']->where('isRemote', true)->where('confirm', false));
        } else {
            $order_expired = null;
            $transfer_count = null;
            $purchase_count = null;
            $purchase_undone_count = null;
            $orderWeight = null;
            $order_tax_id = null;
            $hct_remote = null;
        }

        return view('check_table', [
            'order_status' => BaseService::order_status,
            'order_schedule' => $order_schedule,
            'order_expired' => $order_expired, //過期單
            'order_no_print' => empty($order['unprinted']) ? 0 : count($order['unprinted']), //未轉單
            'order_can_print' => empty($order['canPrint']) ? 0 : $order['canPrint'], //未轉單當中的可轉單
            'order_no_check' => empty($order['unchecked']) ? 0 : count($order['unchecked']), //未覆核
            'orderWeight' => $orderWeight, //過大過重
            'transfer_count' => $transfer_count, //可調撥
            'purchase_count' => $purchase_count, //需採購
            'purchase_undone_count' => $purchase_undone_count, //未進貨
            'order_tax_id' => $order_tax_id, //需統編
            'hct_remote' => $hct_remote, //新竹偏遠
            'inventory_count' => $inventory_count, //需盤點
        ]);
    }

    //訂單狀況。未覆核、未轉單、已過期
    public function order($table, Request $request)
    {
        $data = $this->baseService->GetOrderAll($request);
        $schedule = null;
        switch ($table) {
            case 'unchecked':
                $name = '未覆核';
                break;
            case 'unprinted':
                $name = '未轉單';
                $schedule = $this->baseService->GetOrderSchedule();
                break;
            case 'expired':
                $name = '已過期';
                break;
        }
        if (array_key_exists($table, $data)) {
            $data = $data[$table];
        } else {
            $data = array();
        }

        return view('order', ['data' => $data, 'name' => $name, 'schedule' => $schedule]);
    }

    //訂單備註查詢
    public function order_remark(Request $request)
    {
        $request->flash();
        $data = $this->baseService->GetOrderRemark($request);
        return view('order_remark', $data);
    }

    //庫存總表
    public function stockAll(Request $request)
    {
        $request->flash();
        $data = $this->baseService->GetStockAll($request);
        return view('stock_all', $data);
    }

    //商品可銷售量表
    public function stock_can_sale(Request $request)
    {
        $request->flash();
        $data = $this->baseService->GetStockCanSale($request);
        return view('stock_can_sale', $data);
    }

    //中國商品庫存
    public function stock_country()
    {
        $data = $this->baseService->GetStockCountry();
        return view('stock_country', compact('data'));
    }

    //儲位總表
    public function itemp($stock = null)
    {
        $data = $this->baseService->GetStorageAll($stock);
        $data['stock'] = $stock;
        return view('itemp', $data);
    }

    //訂單明細
    public function order_detail(Request $request)
    {
        $data = $this->baseService->GetOrderDetail($request);
        return view('order_detail', ['data' => $data]);
    }

    //籃子明細
    public function basket_detail(Request $request)
    {
        $data = $this->baseService->GetBasketDetail($request);
        return view('basket_detail', ['data' => $data]);
    }

    //銷售明細
    public function detail(Request $request)
    {
        $data = $this->baseService->GetSalesDetail($request);
        return view('check_detail', ['data' => $data]);
    }

    //查詢指定銷貨單號的資料
    public function check_search(Request $request)
    {
        $request->flash();
        $data = $this->baseService->GetSalesCheck($request);
        return view('check_search', ['data' => $data]);
    }

    //過大過重訂單
    public function overweight()
    {
        $data = $this->baseService->GetOverweightOrder();
        return view('order_weight', $data);
    }

    //需統編
    public function order_tax_id(Request $request)
    {
        $request->flash();
        $data = $this->baseService->GetOrderTaxID($request);
        return view('order_tax_id', ['data' => $data]);
    }

    //現場狀況的銷貨清單
    public function sales_list(Request $request)
    {
        $sales = $this->baseService->GetOrderSchedule()['sales'];
        if ($request->has('status')) {
            $status = $request->get('status');
            $sales = $sales->where('status', $status);
            switch ($status) {
                case 0:
                    $title = '壓單';
                    break;
                case 1:
                    $title = '未列印';
                    break;
                case 2:
                    $title = '未驗貨';
                    break;
                case 3:
                    $title = '未包裝';
                    break;
                case 4:
                    $title = '未上車';
                    break;
                case 5:
                    $title = '已出貨';
                    break;
                default:
                    $title = '未分類';
                    break;
            }
        } else {
            $title = '已轉單';
        }
        if ($request->has('type')) {
            $type = $request->get('type');
            switch ($type) {
                case 1:
                    $title .= '-地板';
                    break;
                case 2:
                    $title .= '-超商';
                    break;
                case 3:
                    $title .= '-宅配';
                    break;
            }
            $sales = $sales->where('type', $type);
        }
        $data = $this->baseService->GetSalesList($sales->pluck('PCOD1')->toArray());
        return view('sales_list', ['title' => $title, 'data' => $data]);
    }

    //庫存異常
    public function stock_unusual()
    {
        $data = $this->baseService->GetStockUnusual();
        $diff_data = $this->baseService->GetStockDiff();
        return view('stock_unusual', ['data' => $data, 'diff_data' => $diff_data]);
    }

    //商品成長比較表
    public function sales_comparison(Request $request)
    {
        $request->flash();
        $data = $this->baseService->GetSalesComparison($request);
        return view('sales_comparison', $data);
    }

    //上車檢驗
    public function check_shipment(Request $request)
    {
        $data = $this->baseService->GetShipmentCheck($request);
        return view('check_shipment', $data);
    }

    //今日地板數量查詢
    public function floor_count()
    {
        $data = $this->baseService->GetFloorCount();
        return view('floor_count', $data);
    }

    //訂單規劃
    public function order_plan(Request $request)
    {
        $request->flash();
        $data = $this->baseService->GetOrderPlan($request);
        return view('order_plan', $data);
    }

    //所有訂單狀態
    public function order_all(Request $request)
    {
        $request->flash();
        $data = $this->baseService->GetOrderAllStatus($request);
        return view('order_all', $data);
    }
    //群撿查詢
    public function group_picking()
    {
        $data = $this->baseService->GetGroupPicking();
        return view('group_picking', $data);
    }
}
