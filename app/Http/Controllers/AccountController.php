<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\yherp\Account;
use App\Imports\AccountImport;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;

class AccountController extends Controller
{
    function __construct()
    {
        $this->middleware('role:admin');
    }

    public function index()
    {
        $data = Account::all();
        return view('account.index', ['data' => $data]);
    }

    public function create()
    {
        return view('account.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'password' => 'required|same:confirm-password',
        ]);

        $input = $request->all();
        Account::create($input);

        return redirect()->route('account.index')->with('status', '使用者新增成功');
    }

    //刪除
    function destroy($id)
    {
        $m = Account::find($id);
        $m->delete();
        return redirect()->route('account.index')
            ->with('status', '刪除成功');
    }

    //匯入
    function import(Request $request)
    {
        $msg = '';
        if ($request->hasFile('file')) {
            $file = $request->file('file');
            if ($file->isValid()) {
                $import = new AccountImport();
                Excel::import($import, $file);
                $msg = $import->msg;
            } else {
                $msg = '檔案【' . $file->getClientOriginalName() . '】未通過驗證';
            }
        } else {
            $msg = '未選擇任何檔案';
        }

        if (empty($msg)) {
            return redirect()->back()->with('status', '匯入成功');
        } else {
            return redirect()->back()->with('status', $msg);
        }
    }
}
