<?php

namespace App\Http\Controllers;

use App\Models\TMS\CUST;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\yherp\OrderReturnRefund;
use App\Imports\OrderReturnRefundImport;

class OrderReturnRefundController extends Controller
{
    public function __construct()
    {
    }

    public function index()
    {
        $cust = CUST::select('CCODE', 'CNAM2')->whereIn('CCODE', ['S001', 'S003', 'S004'])->get();
        $data = OrderReturnRefund::with(['Cust' => function ($q) {
            $q->select('CCODE', 'CNAM2');
        }])
            ->get()
            ->where('group', '!=', 4)
            ->sortBy('group');
        return view('order_return_refund', ['data' => $data, 'cust' => $cust]);
    }

    public function import(Request $request)
    {
        $msg = '';
        if ($request->hasFile('file')) {
            $files = $request->file('file');
            $cust = $request->get('cust');
            foreach ($files as $file) {
                if ($file->isValid()) {
                    $import = new OrderReturnRefundImport($cust);
                    Excel::import($import, $file, null, \Maatwebsite\Excel\Excel::XLSX);
                    $msg .= $import->msg;
                } else {
                    $msg .= '檔案【' . $file->getClientOriginalName() . '】未通過驗證';
                }
            }
        } else {
            $msg = '未選擇任何檔案';
        }

        if (empty($msg)) {
            return redirect()->back()->with('status', '匯入成功');
        } else {
            return redirect()->back()->with('status', $msg);
        }
    }
}
