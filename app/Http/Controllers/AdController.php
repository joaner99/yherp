<?php

namespace App\Http\Controllers;

use DateTime;
use Carbon\Carbon;
use App\Models\yherp\AdCost;
use App\Models\yherp\AdProfitM;
use App\Services\AdService;
use Illuminate\Http\Request;
use App\Imports\AdItemImport;
use App\Imports\AdConfigImport;
use App\Models\yherp\AdChannel;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;

class AdController extends Controller
{
    private $adService = null;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct(AdService $adService)
    {
        $this->middleware('permission:ad');
        $this->adService = $adService;
    }

    //廣告每日花費
    public function cost(Request $request)
    {
        $request->flash();
        $data =  $this->adService->GetAdCost($request);
        return view('ad.cost', ['data' => $data]);
    }

    //廣告效益
    public function profit(Request $request)
    {
        $request->flash();
        $data =  $this->adService->GetProfit($request);
        return view('ad.profit', $data);
    }

    //廣告參數
    public function config(Request $request)
    {
        $request->flash();
        $s_date = $request->get('start_date', Carbon::today()->modify('-1 month')->format('Y-m-d'));
        $e_date = $request->get('end_date', Carbon::today()->format('Y-m-d'));
        $data = [];
        foreach ($this->adService->GetEachDay($s_date, $e_date) as $day) {
            foreach (AdService::shopee_shop as $s) {
                $data[$day][$s] = null;
            }
        }
        $db = AdProfitM::whereBetween('src_date', [$s_date, $e_date])->get();
        foreach ($db as $d) {
            $data[$d->src_date][$d->shop_name] = $d;
        }
        $items = $this->adService->baseRepository->GetShopeeProduct();
        return view('ad.config', ['data' => $data, 'items' => $items]);
    }

    //廣告花費設定
    public function config_cost(Request $request)
    {
        $request->flash();
        $data =  $this->adService->GetAdConfigCost($request);
        return view('ad.config_cost', ['data' => $data]);
    }

    //廣告花費新增畫面
    public function config_cost_create()
    {
        $ad_channel = AdChannel::all();
        return view('ad.config_cost_create', ['ad_channel' => $ad_channel]);
    }

    //新增廣告花費
    public function config_cost_store(Request $request)
    {
        try {
            $this->validate($request, [
                'ad_channel_id' => ['required', 'exists:mysql.ad_channel,id'],
                'ad' => 'integer',
                'handling' => 'integer',
                'event' => 'integer',
                'freight_shopee' => 'integer',
            ]);
            $input = $request->all();
            $input['src_date'] = $request->src_date . '-' . (new DateTime($request->src_date))->format('t');
            AdCost::updateOrCreate(
                [
                    'ad_channel_id' => $input['ad_channel_id'],
                    'src_date' => $input['src_date']
                ],
                $input
            );
            return redirect()->route('ad.config_cost')->with('status', '新增成功');
        } catch (\Exception  $ex) {
            return redirect()->back()->with('status', "新增失敗，原因：{$ex->getMessage()}");
        }
    }

    //新增物流運費
    public function config_cost_store_freight(Request $request)
    {
        try {
            $this->validate($request, [
                'freight' => 'integer',
            ]);
            $data = $this->adService->GetChannelSales($request);
            $input = $request->all();
            $input['src_date'] = $request->src_date . '-' . (new DateTime($request->src_date))->format('t');
            foreach ($data as $channel_id => $freight) {
                AdCost::updateOrCreate(
                    [
                        'ad_channel_id' => $channel_id,
                        'src_date' => $input['src_date']
                    ],
                    ['freight' => $freight]
                );
            }
            return redirect()->route('ad.config_cost')->with('status', '新增成功');
        } catch (\Exception  $ex) {
            return redirect()->back()->with('status', "新增失敗，原因：{$ex->getMessage()}");
        }
    }

    //廣告花費編輯畫面
    public function config_cost_edit($id)
    {
        $ad_channel = AdChannel::all();
        $data = AdCost::with('Channel')->find($id);
        if (empty($data)) {
            return redirect()->route('ad.config_cost')->with('status', '編輯失敗，找不到該資料');
        } else {
            return view('ad.config_cost_edit', ['data' => $data, 'ad_channel' => $ad_channel]);
        }
    }

    //更新廣告花費
    public function config_cost_update(Request $request, $id)
    {
        try {
            $this->validate($request, [
                'ad' => 'integer',
                'handling' => 'integer',
                'event' => 'integer',
                'freight' => 'integer',
                'freight_shopee' => 'integer',
            ]);
            $data = AdCost::find($id);
            if (empty($data)) {
                return redirect()->route('ad.config_cost')->with('status', '更新失敗，找不到該資料');
            } else {
                $data->ad = $request->get('ad');
                $data->handling = $request->get('handling');
                $data->event = $request->get('event');
                $data->freight = $request->get('freight');
                $data->freight_shopee = $request->get('freight_shopee');
                $data->save();
                return redirect()->route('ad.config_cost')->with('status', '更新成功');
            }
        } catch (\Exception  $ex) {
            return redirect()->back()->with('status', "更新失敗，原因：{$ex->getMessage()}");
        }
    }

    //刪除廣告花費設定
    public function config_cost_destroy($id)
    {
        $m = AdCost::find($id);
        $m->delete();
        return redirect()->route('ad.config_cost')
            ->with('status', '刪除成功');
    }

    //廣告模擬
    public function simulation(Request $request)
    {
        $request->flash();
        $data = $this->adService->GetSimulation($request);
        $types = $this->adService->GetItemType();
        return view('ad.simulation', ['data' => $data, 'types' => $types]);
    }

    //匯入廣告報表
    public function import_profit(Request $request)
    {
        $msg = '';
        if ($request->hasFile('file')) {
            $files = $request->file('file');
            foreach ($files as $file) {
                if ($file->isValid()) {
                    $import = new AdConfigImport();
                    if ($file->getClientOriginalExtension() == 'csv') {
                        $data = $import->import_csv($file->getPathname());
                        if (empty($import->get_error_msg())) {
                            $import->collection($data);
                            $msg = $import->msg;
                        } else {
                            $msg = $import->get_error_msg();
                        }
                    } else {
                        Excel::import($import, $file);
                        $msg = $import->msg;
                    }
                } else {
                    if (!empty($msg)) {
                        $msg .= ',';
                    }
                    $msg .= $file->getClientOriginalName() . '上傳失敗';
                }
            }
        } else {
            $msg = '上傳失敗';
        }

        if (empty($msg)) {
            return redirect()->back()->with('status', '上傳成功');
        } else {
            return redirect()->back()->with('status', $msg);
        }
    }

    //匯入商品資訊
    public function import_item(Request $request)
    {
        $msg = '';
        if ($request->hasFile('file')) {
            $files = $request->file('file');
            foreach ($files as $file) {
                if ($file->isValid()) {
                    $import = new AdItemImport(pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME));
                    if ($file->getClientOriginalExtension() == 'csv') {
                        $data = $import->import_csv($file->getPathname());
                        if (empty($import->get_error_msg())) {
                            $import->collection($data);
                            $msg = $import->msg;
                        } else {
                            $msg = $import->get_error_msg();
                        }
                    } else {
                        Excel::import($import, $file);
                        $msg = $import->msg;
                    }
                } else {
                    if (!empty($msg)) {
                        $msg .= ',';
                    }
                    $msg .= $file->getClientOriginalName() . '上傳失敗';
                }
            }
        } else {
            $msg = '上傳失敗';
        }

        if (empty($msg)) {
            return redirect()->back()->with('status', '上傳成功');
        } else {
            return redirect()->back()->with('status', $msg);
        }
    }

    //取得指定廣告的花費與銷售額
    public function ajax_get_ad_sales(Request $request)
    {
        $data = $this->adService->GetAdSales($request);
        return response()->json($data);
    }
}
