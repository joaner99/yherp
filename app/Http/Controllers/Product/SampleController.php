<?php

namespace App\Http\Controllers\Product;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\yherp\ProductSampleD;
use App\Models\yherp\ProductSampleM;
use App\Services\ProductService;
use App\Http\Controllers\Controller;

class SampleController extends Controller
{
    private $productService;

    public function __construct(ProductService $productService)
    {
        $this->productService = $productService;
    }

    public function index()
    {
        $data = ProductSampleM::all();
        return view('product.sample.index', ['data' => $data]);
    }

    public function create()
    {
        return view('product.sample.create');
    }

    public function store(Request $request)
    {
        //驗證
        $this->validate($request, [
            'product_name' => ['required', 'max:20'],
            'estimated_date' => ['required', 'date'],
        ]);
        $input = $request->all();
        $input['status'] = 101;
        $m = ProductSampleM::create($input);
        if (empty($request->get('continue'))) {
            return redirect()->route('product.sample.index')
                ->with(['status' => '建立成功']);
        } else {
            return redirect()->route('product.sample.detail_create', $m->id)
                ->with(['status' => '建立成功']);
        }
    }

    public function edit($id)
    {
        $data = ProductSampleM::find($id);
        return view('product.sample.edit', ['data' => $data]);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'product_name' => ['required', 'max:20'],
            'estimated_date' => ['required', 'date'],
        ]);
        ProductSampleM::find($id)->update($request->all());

        return redirect()->route('product.sample.index')
            ->with(['status' => '編輯成功']);
    }

    public function destroy($id)
    {
        ProductSampleM::find($id)->delete();
        return redirect()->route('product.sample.index')
            ->with(['status' => '作廢成功']);
    }

    public function confirm($id)
    {
        $m = ProductSampleM::find($id);
        if (empty($m->finished_date)) { //未有交付日期
            $m->status = 201;
            $m->finished_date = Carbon::now();
            $m->save();
            //ERP資料建立
            $erp_m = $m->erp()->create();
            foreach ($m->Details as $d) {
                $erp_m->Details()->create([
                    'IPOIN' => 'PCS',
                    'ICONS' => 'N',
                    'INAME' => $d->product_name,
                    'IINAME' => $d->product_name,
                    'T11_11' => $d->T11_11,
                    'SiseCost' => $d->total ?? 0,
                ]);
            }
            return redirect()->route('product.sample.index')
                ->with(['status' => '交付成功']);
        } else {
            return redirect()->route('product.sample.index')
                ->with(['status' => '交付失敗，已經交付過了']);
        }
    }

    public function detail($id)
    {
        $data = ProductSampleM::with('Details.Files')->find($id);
        return view('product.sample.detail', ['data' => $data, 'id' => $id]);
    }

    public function detail_create($id)
    {
        $supplier_list = $this->productService->GetItemSupply();
        $export_list = ProductSampleD::select('export')->distinct()->pluck('export')->toArray();
        return view('product.sample.detail_create', ['id' => $id, 'supplier_list' => $supplier_list, 'export_list' => $export_list]);
    }

    public function detail_store($id, Request $request)
    {
        //驗證
        $this->validate($request, [
            'supplier' => ['required', 'max:20'],
            'product_name' => ['required', 'max:20'],
            'original_name' => ['required', 'max:20'],
            'batch_price' => ['required', 'numeric'],
            'package_price' => ['required', 'numeric'],
            'ch_freight' => ['required', 'numeric'],
            'tw_freight' => ['required', 'numeric'],
            'shopee_price' => ['required', 'numeric'],
            'package_w' => ['required', 'numeric'],
            'package_h' => ['required', 'numeric'],
            'package_d' => ['required', 'numeric'],
            'package_qty' => ['required', 'numeric'],
            'container_cbm' => ['required', 'numeric'],
            'export' => ['required', 'max:10'],
            'T11_11' => ['required', 'max:5'],
            'remark' => ['max:65535'],
        ]);
        $m = ProductSampleM::find($id);
        $d = $m->Details()->create($request->all());
        //附件-URL
        $url_title = $request->get('url_title');
        $url_content = $request->get('url_content');
        if (!empty($url_title) && !empty($url_content)) {
            for ($i = 0; $i < count($url_title); $i++) {
                $title = $url_title[$i] ?? '';
                $content = $url_content[$i] ?? '';
                if (empty($title) || empty($content)) {
                    continue;
                }
                $d->Files()->create([
                    'type' => 1,
                    'title' => $title,
                    'content' => $content,
                ]);
            }
        }
        //附件-檔案
        $msg = '';
        if ($request->hasFile('file')) {
            $files_folder = 'product/sample';
            $m_folder = str_pad($id, 4, '0', STR_PAD_LEFT);
            $d_folder = str_pad($d->id, 6, '0', STR_PAD_LEFT);
            $path = "{$files_folder}/{$m_folder}/{$d_folder}";
            $files = $request->file('file');
            foreach ($files as $file) {
                $file_name = $file->getClientOriginalName();
                if ($file->isValid()) {
                    $file->storeAs("public/{$path}", $file_name);
                    $d->Files()->create([
                        'type' => 2,
                        'title' => $file_name,
                        'content' => "{$path}/{$file_name}",
                    ]);
                } else {
                    if (!empty($msg)) {
                        $msg .= ',';
                    }
                    $msg .= $file_name . '上傳失敗';
                }
            }
            //備份NAS
            // File::copy(public_path("public/{$path}/{$m_folder}"), '//192.168.1.2/採購/ERP/product/sample');
        }
        if (empty($msg)) {
            return redirect()->route('product.sample.detail', $id)
                ->with(['status' => '建立成功']);
        } else {
            return redirect()->back()->with('status', $msg);
        }
    }

    public function detail_edit($id)
    {
        $data = ProductSampleD::with('supplier')->find($id);
        $supplier_list = $this->productService->GetItemSupply();
        $export_list = ProductSampleD::select('export')->distinct()->pluck('export')->toArray();
        return view('product.sample.detail_edit', ['data' => $data, 'supplier_list' => $supplier_list, 'export_list' => $export_list]);
    }

    public function detail_update(Request $request, $id)
    {
        $this->validate($request, [
            'supplier' => ['required', 'max:20'],
            'product_name' => ['required', 'max:20'],
            'original_name' => ['required', 'max:20'],
            'batch_price' => ['required', 'numeric'],
            'package_price' => ['required', 'numeric'],
            'ch_freight' => ['required', 'numeric'],
            'tw_freight' => ['required', 'numeric'],
            'shopee_price' => ['required', 'numeric'],
            'package_w' => ['required', 'numeric'],
            'package_h' => ['required', 'numeric'],
            'package_d' => ['required', 'numeric'],
            'package_qty' => ['required', 'numeric'],
            'container_cbm' => ['required', 'numeric'],
            'export' => ['required', 'max:10'],
            'T11_11' => ['required', 'max:5'],
            'remark' => ['max:65535'],
        ]);
        $d = ProductSampleD::find($id);
        $d->update($request->all());
        //附件-URL
        $url_title = $request->get('url_title');
        $url_content = $request->get('url_content');
        $d->Files()->where('type', 1)->delete();
        if (!empty($url_title) && !empty($url_content)) {
            for ($i = 0; $i < count($url_title); $i++) {
                $title = $url_title[$i] ?? '';
                $content = $url_content[$i] ?? '';
                if (empty($title) || empty($content)) {
                    continue;
                }
                $d->Files()->create([
                    'type' => 1,
                    'title' => $title,
                    'content' => $content,
                ]);
            }
        } //附件-舊檔案
        $old_file = $request->get('old_file');
        if (!empty($old_file)) {
            $d->Files()->where('type', 2)->whereNotIn('id', $old_file)->delete();
        } else {
            $d->Files()->where('type', 2)->delete();
        }
        //附件-檔案
        $msg = '';
        if ($request->hasFile('file')) {
            $files_folder = 'product/sample';
            $m_folder = str_pad($id, 4, '0', STR_PAD_LEFT);
            $d_folder = str_pad($d->id, 6, '0', STR_PAD_LEFT);
            $path = "{$files_folder}/{$m_folder}/{$d_folder}";
            $files = $request->file('file');
            foreach ($files as $file) {
                $file_name = $file->getClientOriginalName();
                $d->Files()->where('type', 2)->where('title', 'like', ('%' . $file_name . '%'));
                if ($file->isValid()) {
                    $file->storeAs("public/{$path}", $file_name);
                    $d->Files()->firstOrCreate([
                        'type' => 2,
                        'title' => $file_name,
                        'content' => "{$path}/{$file_name}",
                    ]);
                } else {
                    if (!empty($msg)) {
                        $msg .= ',';
                    }
                    $msg .= $file_name . '上傳失敗';
                }
            }
        }
        return redirect()->route('product.sample.detail', $d->Main->id)
            ->with(['status' => '編輯成功']);
    }

    public function detail_destroy($id)
    {
        $d = ProductSampleD::find($id);
        $d->delete();
        return redirect()->route('product.sample.detail', $d->Main->id)
            ->with(['status' => '作廢成功']);
    }
}
