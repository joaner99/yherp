<?php

namespace App\Http\Controllers\Product;

use Exception;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Services\ProductService;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\yherp\ProductProposal;
use App\Services\BaseService;
use Illuminate\Support\Facades\Session;

class ProposalController extends Controller
{
    private $productService;

    public function __construct(ProductService $productService)
    {
        $this->middleware('role:admin')->only(['return']);
        $this->productService = $productService;
    }

    public function index(Request $request)
    {
        $request->flash();
        $s_date = $request->get('s_date', Carbon::today()->format('Y-m'));
        $e_date = $request->get('e_date', Carbon::today()->format('Y-m'));
        $week_interval = BaseService::GetWeekInterval($s_date, $e_date);
        Session::put(['proposal_index_s_date' => $s_date, 'proposal_index_e_date' => $e_date]);
        $s_date .= '-01 00:00:00';
        $e_date = (new Carbon($e_date . '-01'))->endOfMonth()->format('Y-m-d 23:59:59');
        $data = ProductProposal::with('NR', 'User')->whereBetween('created_at', [$s_date, $e_date]);
        if (Auth::user()->hasRole('採購主管')) { //採購審核中、採購審核未通過、樣品採買中、樣品未通過、正式上架
            $data->whereIn('status', [3, 4, 5, 6, 7]);
        }
        $data = $data->get()->sortBy('status');
        //每週統計
        $week_data = [];
        $week_s_date = $week_interval[0][0];
        $week_e_date = end($week_interval)[1];
        foreach (ProductProposal::with('User')->select('user', 'created_at')->whereBetween('created_at', [$week_s_date, $week_e_date])->get()->groupBy('user') as $user => $group) {
            $user_name = $group->first()->User->name;
            foreach ($week_interval as $wi) {
                $week_data[$user_name][] = $group->whereBetween('created_at', $wi)->count();
            }
        }
        return view('product.proposal.index', ['data' => $data, 'week_data' => $week_data, 'week_interval' => $week_interval, 'status_list' => ProductProposal::status_list]);
    }

    public function create()
    {
        $types = $this->productService->GetItemType();
        unset($types[1][99]);
        return view('product.proposal.create', ['types' => $types[1], 'negative_review' => ProductService::negative_review]);
    }

    public function store(Request $request)
    {
        //驗證
        $this->validate($request, [
            'name' => ['required', 'max:50'],
            'url' => ['required'],
            'kind' => ['required'],
        ]);
        try {
            $input = $request->all();
            $types = $this->productService->GetItemType();
            $kind_name = $types[1][$input['kind']] ?? '其他';
            $image = $input['img'];
            $m = ProductProposal::create([
                'user' => Auth::user()->id,
                'status' => 1,
                'name' => $input['name'],
                'other_nr' => $input['other_nr'],
                'url' => $input['url'],
                'kind' => $input['kind'],
                'kind_name' => $kind_name,
                'img_base64' => $image,
            ]);
            if (!empty($input['nr'])) {
                foreach ($input['nr'] as $nr) {
                    $m->NR()->create([
                        'val' => $nr,
                    ]);
                }
            }
            return redirect()->action(
                'Product\ProposalController@index',
                ['s_date' => Session::get('proposal_index_s_date'), 'e_date' => Session::get('proposal_index_e_date')]
            )->with(['status' => '建立成功']);
        } catch (Exception $ex) {
            return redirect()->back()->with('status', "建立失敗，原因：{$ex->getMessage()}");
        }
    }

    public function edit($id)
    {
        $data = ProductProposal::with('NR')->where('id', $id)->first();
        if (empty($data)) {
            return redirect()->back()->with('status', '找不到該筆資料，無法進行編輯');
        }
        $types = $this->productService->GetItemType();
        unset($types[1][99]);
        $status_list = [];
        $user = Auth::user();
        if ($user->hasRole('admin')) { //admin開放全權限
            $status_list = ProductProposal::status_list;
        } else if ($user->hasRole('採購主管')) {
            //狀態=採購審核中、採購審核未通過、樣品採買中。採購可編輯
            if (in_array($data->status, [3, 4, 5])) {
                //可更改=採購審核中、採購審核未通過、樣品採買中、正式上架
                $status_list = array_intersect_key(
                    ProductProposal::status_list,
                    array_flip([3, 4, 5])
                );
            }
        }
        return view('product.proposal.edit', [
            'data' => $data,
            'types' => $types[1],
            'negative_review' => ProductService::negative_review,
            'status_list' => $status_list,
            'tiers' => ProductProposal::tiers,
        ]);
    }

    public function update($id, Request $request)
    {
        //驗證
        $this->validate($request, [
            'name' => ['required', 'max:50'],
            'url' => ['required'],
            'kind' => ['required'],
        ]);
        try {
            $m = ProductProposal::find($id);
            if (empty($m)) {
                return redirect()->back()->with('status', '找不到該筆資料，無法更新');
            }
            $input = $request->all();
            $types = $this->productService->GetItemType();
            $kind_name = $types[1][$input['kind']] ?? '其他';
            //縮圖轉base64
            $image = $input['img'];
            //主檔
            $m->update([
                'name' => $input['name'],
                'other_nr' => $input['other_nr'],
                'url' => $input['url'],
                'kind' => $input['kind'],
                'kind_name' => $kind_name,
            ]);
            //更新附圖
            if (!empty($image)) {
                $m->update([
                    'img_base64' => $image,
                ]);
            }
            if (Auth::user()->hasRole('admin')) {
                $m->update([
                    'status' => $input['status'],
                    'bonus' => $input['bonus'],
                    'tier' => $input['tier'],
                ]);
            }
            //負評
            $m->NR()->delete();
            if (!empty($input['nr'])) {
                foreach ($input['nr'] as $nr) {
                    $m->NR()->create([
                        'val' => $nr,
                    ]);
                }
            }
            if ($input['redirect']) {
                return redirect()->route('product.sample.create')->with(['status' => '更新成功']);
            } else {
                return redirect()->action(
                    'Product\ProposalController@index',
                    ['s_date' => Session::get('proposal_index_s_date'), 'e_date' => Session::get('proposal_index_e_date')]
                )->with(['status' => '更新成功']);
            }
        } catch (Exception $ex) {
            return redirect()->back()->with('status', "更新失敗，原因：{$ex->getMessage()}");
        }
    }

    function destroy($id)
    {
        $m = ProductProposal::find($id);
        if ($m->user != Auth::user()->id) {
            return redirect()->route('product.proposal.index')->with('status', '刪除失敗，無法刪除他人的資料');
        } else if ($m->status != 1) { //行銷審核中
            return redirect()->route('product.proposal.index')->with('status', '刪除失敗，已進入審核程序');
        } else {
            $m->NR()->delete();
            $m->delete();
            return redirect()->route('product.proposal.index')
                ->with('status', '刪除成功');
        }
    }

    //退件
    function return($id)
    {
        $m = ProductProposal::find($id);
        $m->update(['status' => 2]);
        return redirect()->route('product.proposal.index')
            ->with('status', '退件成功');
    }

    //AJAX檢查名稱重複
    function ajax_check_repeat(Request $request)
    {
        try {
            $name = $request->get('name');
            $db = ProductProposal::select('id', 'name', 'kind_name', 'url', 'img_base64', 'status')->where('name', 'LIKE', "%$name%")->get();
            $result = [];
            foreach ($db as $d) {
                $result[] = [
                    'name' => $d->name,
                    'kind_name' => $d->kind_name,
                    'url' => $d->url,
                    'img_base64' => $d->img_base64,
                    'status' => $d->status,
                    'status_name' => $d->status_name,
                ];
            }
            return response()->json($result);
        } catch (Exception $ex) {
            return response()->json(['msg' => "重複檢查失敗，原因：{$ex->getMessage()}"], 404);
        }
    }
}
