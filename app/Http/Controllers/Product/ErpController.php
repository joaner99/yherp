<?php

namespace App\Http\Controllers\Product;

use Carbon\Carbon;
use App\Models\TMS\Item;
use App\Models\yherp\ProductErpD;
use App\Models\yherp\ProductErpM;
use Illuminate\Http\Request;
use App\Services\ItemService;
use App\Exports\TMSItemExport;
use App\Exports\ErpPrintExport;
use App\Services\ProductService;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;

class ErpController extends Controller
{
    private $productService;
    private $itemService = null;

    public function __construct(ProductService $productService, ItemService $itemService)
    {
        $this->productService = $productService;
        $this->itemService = $itemService;
    }

    public function index()
    {
        $data = ProductErpM::with('Sample');
        if (Auth::user()->can('product_erp_edit1')) {
            $data->orWhereHas('sample', function ($q) {
                $q->where('status', 201);
            });
        }
        if (Auth::user()->can('product_erp_edit2')) {
            $data->orWhereHas('sample', function ($q) {
                $q->where('status', 202);
            });
        }
        $data = $data->get();
        foreach ($data as &$d) {
            foreach ($d->Details()->pluck('INAME')->toArray() as $name) {
                if (empty(Item::where('INAME', $name)->first())) {
                    continue 2;
                }
            }
            $d->CanPrint = 1;
        }
        return view('product.erp.index', ['data' => $data]);
    }

    public function detail($id)
    {
        $data = ProductErpM::with('Details')->find($id);
        $item_type = $this->itemService->GetItemType();
        if (!empty($data->Details) && count($data->Details) > 0) {
            foreach ($data->Details as &$d) {
                if (!empty($d->ITCOD)) {
                    $d->ITCOD = '【' . $d->ITCOD . '】' . ($item_type[1][$d->ITCOD] ?? '');
                }
                if (!empty($d->ITCO2)) {
                    $d->ITCO2 = '【' . $d->ITCO2 . '】' . ($item_type[2][$d->ITCO2] ?? '');
                }
                if (!empty($d->ITCO3)) {
                    $d->ITCO3 = '【' . $d->ITCO3 . '】' . ($item_type[3][$d->ITCO3] ?? '');
                }
            }
        }

        return view('product.erp.detail', ['data' => $data]);
    }

    public function detail_edit($id)
    {
        $data = ProductErpD::find($id);
        $item_type = $this->itemService->GetItemType();
        $item_supplier = $this->itemService->GetItemSupply();
        return view('product.erp.detail_edit', ['data' => $data, 'item_type' => $item_type, 'item_supplier' => $item_supplier]);
    }

    public function detail_update($id, Request $request)
    {
        $input = $request->all();
        if (Auth::user()->can('product_erp_edit1')) {
            //驗證
            $this->validate($request, [
                'INAME' => ['required', 'max:40', 'unique:mysql.product_erp_d,INAME,' . $id],
                'IPOIN' => ['required', 'max:3'],
                'ICONS' => ['required', 'max:1'],
                'warranty_y' => ['required', 'integer'],
                'warranty_m' => ['required', 'integer'],
                'warranty_d' => ['required', 'integer'],
            ]);
            $input['IINAME'] = $input['INAME']; //次品名=主品名
        }
        if (Auth::user()->can('product_erp_edit2')) {
            //驗證
            $this->validate($request, [
                'ITCOD' => ['required', 'max:2'],
                'ITCO2' => ['required', 'max:3'],
                'ITCO3' => ['required', 'max:3'],
            ]);
        }
        $d = ProductErpD::find($id);
        $d->update($input);
        return redirect()->route('product.erp.detail', $d->Main->id)
            ->with('status', '編輯成功');
    }

    public function confirm($id)
    {
        $erp = ProductErpM::with('Details')->find($id);
        $sample = $erp->Sample;
        if ($sample->status == 201 && Auth::user()->can('product_erp_edit1')) { //廠務交付給財物
            if ($this->productService->ValidERPConfirm($erp)) {
                $sample->status = 202;
                $sample->save();
                $erp->finished_date = Carbon::now();
                $erp->save();
            } else {
                return redirect()->route('product.erp.index')
                    ->with(['status' => '交付失敗。原因：' . $this->productService->msg]);
            }
        } elseif ($sample->status  == 202 && Auth::user()->can('product_erp_edit2')) { //財務交付給行銷
            if ($this->productService->ValidERPConfirm2($erp)) {
                $sample->status = 301;
                $sample->save();
                $erp->finished_date_2 = Carbon::now();
                $erp->save();
            } else {
                return redirect()->route('product.erp.index')
                    ->with(['status' => '交付失敗。原因：' . $this->productService->msg]);
            }
        }
        return redirect()->route('product.erp.index')
            ->with(['status' => '交付成功']);
    }

    public function export(Request $request)
    {
        $id = json_decode($request->get('id'));
        if (empty($id) || count($id) == 0) {
            return redirect()->route('product.erp.index')->with('status', '匯出失敗，找不到匯出的資料');
        } else {
            return Excel::download(new TMSItemExport($id), 'TMS新建料號.' . \Maatwebsite\Excel\Excel::XLSX);
        }
    }

    public function print(Request $request)
    {
        $id = $request->get('id');
        if (empty($id)) {
            return redirect()->route('product.erp.index')->with('status', '列印失敗，找不到匯出的資料');
        } else {
            $product_name = ProductErpM::find($id)->Sample->product_name;
            return Excel::download(new ErpPrintExport($id), $product_name . '.' . \Maatwebsite\Excel\Excel::XLSX);
        }
    }
}
