<?php

namespace App\Http\Controllers;

use DateTime;
use Exception;
use App\Models\TMS\Order;
use Illuminate\Http\Request;
use App\Services\BaseService;
use App\Exports\QuotationExport;
use App\Models\yherp\QuotationM;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Validator;

class QuotationController extends Controller
{
    private $baseService = null;

    public function __construct(BaseService $baseService)
    {
        $this->baseService = $baseService;
    }

    //報價單
    public function index()
    {
        $data = QuotationM::with(['Details', 'User'])->get();
        $order_no_list = $data->pluck('quote_no')->toArray();
        $order_no_list = Order::select('OCOD4')
            ->where('OCANC', '!=', '訂單取消')
            ->whereIn('OCOD4', $order_no_list)
            ->get()
            ->pluck('OCOD4')->toArray();
        if (count($order_no_list) > 0) { //排除已匯入TMS
            $data = $data->filter(function ($item) use ($order_no_list) {
                return !in_array($item->quote_no, $order_no_list);
            });
        }
        return view('quotation.index', ['data' => $data]);
    }

    public function create()
    {
        $items = $this->baseService->baseRepository->GetItemAll();
        return view('quotation.create', ['items' => $items]);
    }

    public function store(Request $request)
    {
        //主檔驗證
        $this->validate($request, [
            'quote_date' => ['required', 'date', 'max:11'],
            'contact_name' => 'max:50',
            'company_name' => 'max:50',
            'remark' => 'max:100',
            'warranty_remark' => 'max:100',
        ]);
        //明細檔驗證
        $items = json_decode($request->get('details'), true);
        $rules = [
            '*.item_no' => ['required', 'max:16'],
            '*.item_name' => ['required', 'max:40'],
            '*.item_qty' => ['required', 'numeric', 'integer', 'digits_between:0,65535'],
            '*.item_price' => ['required', 'numeric'],
            '*.item_remark' => 'max:100',
        ];
        $validator = Validator::make($items, $rules);
        if ($validator->fails()) {
            return redirect()->route('quotation.create')
                ->withErrors($validator)
                ->withInput();
        }
        $quote_date = $request->get('quote_date');
        //儲存
        DB::connection('mysql')->beginTransaction();
        try {
            $db = QuotationM::lockForUpdate()
                ->select('quote_no')
                ->where('quote_date', $quote_date)
                ->withTrashed()
                ->orderBy('quote_no', 'desc');
            $seq = $db->first()->quote_no ?? null;
            if (isset($seq)) {
                $seq++;
            } else {
                $today = new DateTime($quote_date);
                $seq = $today->format('Ymd0001');
            }

            $quote_user = Auth::user()->id;
            $total_price = 0;
            $m = QuotationM::create([
                'quote_no' => 'Q' . $seq,
                'quote_date' => $quote_date,
                'quote_user' => $quote_user,
                'contact_name' => $request->get('contact_name'),
                'contact_phone' => $request->get('contact_phone'),
                'contact_email' => $request->get('contact_email'),
                'company_name' => $request->get('company_name'),
                'company_phone' => $request->get('company_phone'),
                'company_fax' => $request->get('company_fax'),
                'company_addr' => $request->get('company_addr'),
                'total_price' => 0,
                'remark' => $request->get('remark'),
                'warranty_remark' =>  $request->get('warranty_remark'),
            ]);
            foreach ($items as $seq => $item) {
                $item['item_seq'] = $seq + 1;
                $m->Details()->create($item);
                $total_price += $item['item_price'] * $item['item_qty'];
            }
            $m->update(['total_price' => $total_price]);
        } catch (\Exception $e) {
            DB::connection('mysql')->rollback();
            return redirect()->back()->with('status', '報價單建立時，發生錯誤。原因：' . $e->getMessage());
        }
        DB::connection('mysql')->commit();
        return redirect()->route('quotation.index')->with('status', '報價單新增成功');
    }

    //編輯
    public function edit($id)
    {
        $data = QuotationM::with(['Details', 'Details.Item:ICODE,ISOUR'])->where('id', $id)->first();
        $items = $this->baseService->baseRepository->GetItemAll();
        return view('quotation.edit', ['data' => $data, 'items' => $items]);
    }

    //更新
    public function update($id, Request $request)
    {
        //主檔驗證
        $this->validate($request, [
            'quote_date' => ['required', 'date', 'max:11'],
            'contact_name' => 'max:50',
            'company_name' => 'max:50',
            'remark' => 'max:100',
            'warranty_remark' => 'max:100',
        ]);
        //明細檔驗證
        $items = json_decode($request->get('details'), true);
        $rules = [
            '*.item_no' => ['required', 'max:16'],
            '*.item_name' => ['required', 'max:40'],
            '*.item_qty' => ['required', 'numeric', 'integer', 'digits_between:0,65535'],
            '*.item_price' => ['required', 'numeric'],
            '*.item_remark' => 'max:100',
        ];
        $validator = Validator::make($items, $rules);
        if ($validator->fails()) {
            return redirect()->route('quotation.create')
                ->withErrors($validator)
                ->withInput();
        }
        $total_price = 0;
        $m = QuotationM::find($id);
        $m->update([
            'contact_name' => $request->get('contact_name'),
            'contact_phone' => $request->get('contact_phone'),
            'contact_email' => $request->get('contact_email'),
            'company_name' => $request->get('company_name'),
            'company_phone' => $request->get('company_phone'),
            'company_fax' => $request->get('company_fax'),
            'company_addr' => $request->get('company_addr'),
            'total_price' => 0,
            'remark' => $request->get('remark'),
            'warranty_remark' =>  $request->get('warranty_remark'),
        ]);
        //明細
        $m->Details()->delete();
        foreach ($items as $seq => $item) {
            $item['item_seq'] = $seq + 1;
            $m->Details()->create($item);
            $total_price += $item['item_price'] * $item['item_qty'];
        }
        $m->update(['total_price' => $total_price]);
        return redirect()->route('quotation.index')->with('status', '編輯成功');
    }

    //刪除
    public function destroy($id)
    {
        $m = QuotationM::find($id);
        $m->delete();
        return redirect()->route('quotation.index')->with('status', '刪除成功');
    }

    //匯出
    public function export($id)
    {
        try {
            $data = QuotationM::with(['User', 'Details'])->where('id', $id)->first();
            return Excel::download(new QuotationExport($data), $data->quote_no . '報價單.' . \Maatwebsite\Excel\Excel::XLSX);
        } catch (Exception $ex) {
            return response()->json(['msg' => '匯出時，發生無法預期的錯誤。原因：' . $ex->getMessage()], 404);
        }
    }
}
