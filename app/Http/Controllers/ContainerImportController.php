<?php

namespace App\Http\Controllers;

use DateTime;
use Illuminate\Http\Request;
use App\Services\BaseService;
use App\Models\yherp\ContainerImport;

class ContainerImportController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:container_import');
    }

    public function index(Request $request)
    {
        $data = ContainerImport::with('Files');
        if (empty($request->get('search_all'))) {
            $data->whereNull('completed_at');
        }
        $data = $data->get();
        return view('container_import.index', ['data' => $data]);
    }

    public function create()
    {
        return view('container_import.create');
    }

    public function store(Request $request)
    {
        //驗證
        $this->validate($request, [
            'order_no' => ['required'],
        ]);
        $ship_date = $request->get('ship_date');
        $order_no = $request->get('order_no');
        $m = ContainerImport::create([
            'order_no' => $order_no,
            'ship_date' => $ship_date,
        ]);
        if ($request->hasFile('file')) {
            $files = $request->file('file');
            foreach ($files as $file) {
                if ($file->isValid()) {
                    $image = "data:image/png;base64," . base64_encode(file_get_contents($file));
                    $m->Files()->create(['img_base64' => $image]);
                }
            }
        }

        return redirect()->route('container_import.index')->with('status', '新增成功');
    }

    public function edit($id)
    {
        $data = ContainerImport::find($id);
        return view('container_import.edit', ['data' => $data, 'currency' => BaseService::currency]);
    }

    public function update(Request $request, $id)
    {
        $m = ContainerImport::find($id);
        $m->update($request->input());
        return redirect()->route('container_import.index')
            ->with('status', '更新成功');
    }

    //刪除
    function destroy($id)
    {
        $m = ContainerImport::find($id);
        $m->Files()->delete();
        $m->delete();
        return redirect()->route('container_import.index')
            ->with('status', '刪除成功');
    }

    //結案
    function finish($id)
    {
        $m = ContainerImport::find($id);
        $m->update(['completed_at' => new DateTime()]);
        return redirect()->route('container_import.index')
            ->with('status', '結案成功');
    }
}
