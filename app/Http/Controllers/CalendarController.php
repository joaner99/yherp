<?php

namespace App\Http\Controllers;

use App\User;
use DateTime;
use Exception;
use Illuminate\Http\Request;
use App\Models\yherp\Calendar;
use App\Services\CalendarService;

class CalendarController extends Controller
{
    private $calendarService = null;

    function __construct(CalendarService $calendarService)
    {
        $this->calendarService = $calendarService;
    }

    //出勤行事曆
    public function index(Request $request)
    {
        $request->flash();
        $data = $this->calendarService->GetCalendar($request);
        return view('calendar.index', $data);
    }

    public function create()
    {
        $users = User::select('id', 'name2')->whereNotNull('emp_no')->get()->sortBy('name2');
        return view('calendar.create', ['users' => $users, 'kinds' => CalendarService::calendar_kind]);
    }

    public function store(Request $request)
    {
        //驗證
        $this->validate($request, [
            's_date' => ['required'],
            'e_date' => ['required'],
            'kind' => ['required']
        ]);
        try {
            $all_day = !empty($request->get('all_day'));
            $kind = $request->get('kind');
            $s_date = $request->get('s_date');
            $e_date = $request->get('e_date');
            $tmp = [
                'kind' => $kind,
                's_date' => $s_date,
                'e_date' => $e_date,
            ];
            if ($all_day) {
                $tmp['s_time'] = null;
                $tmp['e_time'] = null;
            } else {
                $tmp['s_time'] = $request->get('s_time');
                $tmp['e_time'] = $request->get('e_time');
            }
            switch ($kind) {
                case '1': //請假
                    $tmp['user'] = $request->get('user');
                    Calendar::create($tmp);
                    break;
                case '2': //假勤
                    $tmp['user'] = $request->get('user2');
                    Calendar::create($tmp);
                    break;
                case '99': //其他
                    $tmp['content'] = $request->get('content');
                    Calendar::create($tmp);
                    break;
            }
            return redirect()->route('calendar.index')->with(['status' => '建立成功']);
        } catch (Exception $ex) {
            return redirect()->back()->with('status', "建立失敗，原因：{$ex->getMessage()}");
        }
    }

    public function edit($id)
    {
        $data = Calendar::find($id);
        $users = User::select('id', 'name2')->whereNotNull('emp_no')->get()->sortBy('name2');
        return view('calendar.edit', ['users' => $users, 'kinds' => CalendarService::calendar_kind, 'data' => $data]);
    }

    public function update(Request $request, $id)
    {
        $data = Calendar::find($id);
        if (empty($data)) {
            return redirect()->back()->with('status', "更新失敗，原因：找不到該資料");
        }
        //驗證
        $this->validate($request, [
            's_date' => ['required'],
            'e_date' => ['required'],
            'kind' => ['required']
        ]);
        try {
            $all_day = !empty($request->get('all_day'));
            $kind = $request->get('kind');
            $s_date = $request->get('s_date');
            $e_date = $request->get('e_date');
            $tmp = [
                'kind' => $kind,
                's_date' => $s_date,
                'e_date' => $e_date,
            ];
            if ($all_day) {
                $tmp['s_time'] = null;
                $tmp['e_time'] = null;
            } else {
                $tmp['s_time'] = $request->get('s_time');
                $tmp['e_time'] = $request->get('e_time');
            }
            switch ($kind) {
                case '1': //請假
                    $tmp['user'] = $request->get('user');
                    $data->update($tmp);
                    break;
                case '2': //出差
                    $tmp['user'] = $request->get('user2');
                    $data->update($tmp);
                    break;
                case '99': //其他
                    $tmp['content'] = $request->get('content');
                    $data->update($tmp);
                    break;
            }
            return redirect()->route('calendar.index')->with(['status' => '更新成功']);
        } catch (Exception $ex) {
            return redirect()->back()->with('status', "更新失敗，原因：{$ex->getMessage()}");
        }
    }

    //
    function destroy($id)
    {
        $m = Calendar::find($id);
        $m->delete();
        return redirect()->route('calendar.index')
            ->with('status', '刪除成功');
    }
}
