<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use App\Services\TransportService;
use App\Http\Controllers\Controller;
use App\Models\yherp\OtherTransport;

class OtherTransportController extends Controller
{
    private $transportService = null;

    public function __construct(TransportService $transportService)
    {
        $this->transportService = $transportService;
    }

    //人工資料
    public function index(Request $request)
    {
        $request->flash();
        $data  = $this->transportService->GetOtherTransport($request);
        return  view('other_transport.index', $data);
    }

    //AJAX更新資料
    public function ajax_update(Request $request)
    {
        $data = $request->input();
        $msg = '';
        try {
            $original_no = $data['original_no'];
            unset($data['original_no']);
            OtherTransport::updateOrCreate(['original_no' => $original_no], $data);
        } catch (Exception $ex) {
            $msg = $ex->getMessage();
        }
        if (empty($msg)) {
            return response()->json();
        } else {
            return response()->json(['msg' => '儲存失敗。原因：' . $msg], 404);
        }
    }
}
