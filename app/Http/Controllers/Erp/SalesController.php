<?php

namespace App\Http\Controllers\Erp;

use DateTime;
use stdClass;
use Exception;
use App\Models\yherp\SalesD;
use App\Models\yherp\SalesM;
use App\Services\ErpService;
use Illuminate\Http\Request;
use App\Services\StockService;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class SalesController extends Controller
{
    private $erpService = null;
    private $stockService = null;

    public function __construct(ErpService $erpService, StockService $stockService)
    {
        $this->erpService = $erpService;
        $this->stockService = $stockService;
    }

    public function index(Request $request)
    {
        $request->flash();
        $data = $this->erpService->GetSales($request);
        return view('erp.sales.index', $data);
    }

    public function create(Request $request)
    {
        $company = $request->get('company');
        $items = $this->erpService->GetFloorItem($company);
        return view('erp.sales.create', ['items' => $items, 'company' => $company]);
    }

    public function store(Request $request)
    {
        //驗證
        $this->validate($request, [
            'sale_date' => ['required'],
            'discount' => ['required'],
            'details' => ['required'],
        ]);
        $company = $request->get('company');
        $connection = $this->erpService->GetConnection($company);
        $database = $this->erpService->GetDatabase($company);
        DB::connection($connection)->beginTransaction();
        try {
            $input = $request->all();
            $discount = $input['discount'];
            $sale_date = $input['sale_date'];
            $details = json_decode($input['details'], '[]');
            $db = SalesM::on($connection)->lockForUpdate()
                ->select('id')
                ->withTrashed()
                ->where('sale_date', $sale_date)
                ->orderBy('id', 'desc');
            $seq = $db->first()->id ?? null;
            if (isset($seq)) {
                $seq++;
            } else {
                $today = new DateTime($sale_date);
                $seq = $today->format('Ymd0001');
            }
            //主檔
            SalesM::on($connection)->create([
                'id' => $seq,
                'user_id' => Auth::user()->id,
                'sale_date' => $input['sale_date'],
                'total_price_tax' => collect($details)->sum('total_price_tax') - $discount,
            ]);
            //明細檔
            $sales_require = [];
            foreach ($details as $d) {
                $item_no = $d['item_no'];
                $item_qty = $d['item_qty'];
                SalesD::on($connection)->create([
                    'm_id' => $seq,
                    'item_no' => $item_no,
                    'item_name' => trim($d['item_name']),
                    'item_qty' => $item_qty,
                    'unit_price_tax' => $d['unit_price_tax'],
                    'total_price_tax' => $d['total_price_tax'],
                ]);
                //統整商品需要的數量
                if (key_exists($item_no, $sales_require)) {
                    $sales_require[$item_no] += $item_qty;
                } else {
                    $sales_require[$item_no] = $item_qty;
                }
            }
            //折扣
            if ($discount > 0) {
                SalesD::on($connection)->create([
                    'm_id' => $seq,
                    'item_no' => 'XX126',
                    'item_name' => '折扣',
                    'item_qty' => 1,
                    'unit_price_tax' => -$discount,
                    'total_price_tax' => -$discount,
                ]);
            }
            //根據銷貨自動進行出庫紀錄
            // switch ($database) {
            //     case 'yherp2':
            //         $s_id = 'Y001';
            //         break;
            //     default:
            //         $s_id = '';
            //         break;
            // }
            // if (!empty($s_id)) {
            //     $places = $this->stockService->GetPlacesByItems(array_keys($sales_require), $database, $s_id);
            //     foreach ($places->groupBy('m_id') as $p_id => $group) {
            //         $items = [];
            //         foreach ($group as $item) {
            //             $item_no = $item->item_no;
            //             $item_name = $item->item_name;
            //             $item_stock = $item->qty;
            //             $require = $sales_require[$item_no] ?? 0;
            //             if (empty($require)) {
            //                 continue;
            //             }
            //             $qty = $require >= $item_stock ? $item_stock : $require;
            //             $tmp = new stdClass();
            //             $tmp->no = $item_no;
            //             $tmp->name = $item_name;
            //             $tmp->qty = $qty;
            //             $items[] = $tmp;
            //             $sales_require[$item_no] -= $qty;
            //         }
            //         $msg = $this->stockService->StockModify($connection, $database, $s_id, $p_id, $items, 2);
            //         if (!empty($msg)) {
            //             DB::connection($connection)->rollBack();
            //             return redirect()->back()->with('status', '建立失敗，扣庫原因：' . $msg);
            //         }
            //     }
            // }
            DB::connection($connection)->commit();
            return redirect()->action(
                'Erp\SalesController@index',
                [
                    's_date' => Session::get('erp_sales_index_s_date'),
                    'e_date' => Session::get('erp_sales_index_e_date'),
                    'company' => $company,
                ]
            )->with(['status' => '建立成功']);
        } catch (Exception $ex) {
            DB::connection($connection)->rollBack();
            return redirect()->back()->with('status', "建立失敗，原因：{$ex->getMessage()}");
        }
    }

    public function edit($id, Request $request)
    {
        $company = $request->get('company');
        $database = $this->erpService->GetDatabase($company);
        $items = $this->erpService->GetFloorItem($company);
        $data = SalesM::with('Details')->with([
            'Details' => function ($q) use ($database) {
                $q->from($database . '.sales_d');
            }
        ])->where('id', $id)->first();
        $discount = abs($data->Details()->where('item_no', 'XX126')->first()->total_price_tax ?? 0);
        //計算目前庫存
        foreach ($data->Details as $d) {
            $item_stock = 0;
            $item = $items->where('HICOD', $d->item_no)->first();
            if (!empty($item)) {
                $item_stock = $item->ItemPlaceD->sum('qty') ?? 0;
            }
            $d->item_stock = $item_stock;
        }
        return view('erp.sales.edit', ['data' => $data, 'discount' => $discount, 'items' => $items, 'company' => $company]);
    }

    public function update($id, Request $request)
    {
        //驗證
        $this->validate($request, [
            'discount' => ['required'],
            'details' => ['required'],
        ]);
        $company = $request->get('company');
        $connection = $this->erpService->GetConnection($company);
        DB::connection($connection)->beginTransaction();
        try {
            $input = $request->all();
            $discount = $input['discount'];
            $details = json_decode($input['details'], '[]');
            SalesD::on($connection)->where('m_id', $id)->delete();
            //明細檔
            foreach ($details as $d) {
                SalesD::on($connection)->create([
                    'm_id' => $id,
                    'item_no' => $d['item_no'],
                    'item_name' => trim($d['item_name']),
                    'item_qty' => $d['item_qty'],
                    'unit_price_tax' => $d['unit_price_tax'],
                    'total_price_tax' => $d['total_price_tax'],
                ]);
            }
            //折扣
            if ($discount > 0) {
                SalesD::on($connection)->create([
                    'm_id' => $id,
                    'item_no' => 'XX126',
                    'item_name' => '折扣',
                    'item_qty' => 1,
                    'unit_price_tax' => -$discount,
                    'total_price_tax' => -$discount,
                ]);
            }
            DB::connection($connection)->commit();
            return redirect()->action(
                'Erp\SalesController@index',
                [
                    's_date' => Session::get('erp_sales_index_s_date'),
                    'e_date' => Session::get('erp_sales_index_e_date'),
                    'company' => $company,
                ]
            )->with(['status' => '更新成功']);
        } catch (Exception $ex) {
            DB::connection($connection)->rollBack();
            return redirect()->back()->with('status', "更新失敗，原因：{$ex->getMessage()}");
        }
    }

    //刪除
    function destroy($id, Request $request)
    {
        $company = $request->get('company');
        $connection = $this->erpService->GetConnection($company);
        SalesM::on($connection)->where('id', $id)->delete();
        SalesD::on($connection)->where('m_id', $id)->delete();
        return redirect()->action(
            'Erp\SalesController@index',
            [
                's_date' => Session::get('erp_sales_index_s_date'),
                'e_date' => Session::get('erp_sales_index_e_date'),
                'company' => $company,
            ]
        )->with(['status' => '刪除成功']);
    }
}
