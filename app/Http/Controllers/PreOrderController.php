<?php

namespace App\Http\Controllers;

use App\Services\BaseService;
use App\Http\Controllers\Controller;

class PreOrderController extends Controller
{
    private $baseService = null;

    public function __construct(BaseService $baseService)
    {
        $this->baseService = $baseService;
    }

    //預購總表
    public function index()
    {
        $data = $this->baseService->GetPreOrder();
        return view('pre_order', $data);
    }
}
