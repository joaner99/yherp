<?php

namespace App\Http\Controllers;

use App\User;
use DateTime;
use Exception;
use Carbon\Carbon;
use App\Models\TMS\Posein;
use Illuminate\Http\Request;
use App\Imports\FreightImport;
use App\Exports\TransportExport;
use App\Models\yherp\LogShipment;
use App\Services\TransportService;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\yherp\OrderShipmentStatusM;

class TransportController extends Controller
{
    private $transportService = null;

    public function __construct(TransportService $transportService)
    {
        $this->middleware('permission:bh_ship')->only(['bh_unshipped', 'bh_ship']);
        $this->middleware('permission:transport_index_edit')->only(['index_update', 'index_batch_update', 'index_batch_confirm', 'index_batch_status_confirm']);
        $this->middleware('permission:transport_index_edit_full')->only(['index_check']);
        $this->transportService = $transportService;
    }

    //託運總表
    public function index(Request $request)
    {
        $request->flash();
        $data = $this->transportService->GetTransport($request);
        return view('transport.index', $data);
    }

    //託運總表狀態編輯
    public function index_update(Request $request)
    {
        $sales_no = $request->get('sales_no');
        $status = $request->get('status');
        if (!empty($sales_no)) {
            $m = OrderShipmentStatusM::firstOrCreate(['sales_no' => $sales_no]);
            $m->estimated_date = $request->get('estimated_date');
            $m->shipment_check = $request->get('shipment_check', 0);
            $m->save();
            //更新狀態
            $m->Details()->delete();
            if (!empty($status) && count($status) > 0) {
                foreach ($status as $s) {
                    $m->Details()->create([
                        '_status' => $s,
                    ]);
                }
            }
        }
        return redirect()->route('transport.index', ['start_date' => $request->get('start_date'), 'end_date' => $request->get('end_date')])
            ->with('status', '覆核成功');
    }

    //託運總表批次狀態編輯
    public function index_batch_update(Request $request)
    {
        $data = json_decode($request->get('data', '[]'));
        foreach ($data as $d) {
            $sale_no = $d->sales_no;
            $estimated_date = empty($d->estimated_date) ? NULL : $d->estimated_date;
            $status = $d->status;
            $m = OrderShipmentStatusM::firstOrCreate(['sales_no' => $sale_no]);
            $m->estimated_date = $estimated_date;
            $m->save();
            //更新狀態
            $m->Details()->delete();
            if (!empty($status) && count($status) > 0) {
                foreach ($status as $s) {
                    $m->Details()->create([
                        '_status' => $s,
                    ]);
                }
            }
        }
        return redirect()->back()->with('status', '批次狀態編輯成功');
    }

    //託運總表批次出貨覆核
    public function index_batch_confirm(Request $request)
    {
        $sales = $request->get('sales');
        OrderShipmentStatusM::whereIn('sales_no', $sales)->update(['shipment_check' => 1]);

        return redirect()->route('transport.index', ['start_date' => $request->get('start_date'), 'end_date' => $request->get('end_date')])
            ->with('status', '批次出貨覆核成功');
    }

    //二次檢驗覆核成功
    public function index_batch_status_confirm(Request $request)
    {
        $sales = $request->get('sales');
        foreach ($sales as $sale_no) {
            $m = OrderShipmentStatusM::firstOrCreate(['sales_no' => $sale_no]);
            $m->Details()->where('_status', 3)->update(['confirm' => 1]);
        }
        return redirect()->route('transport.index', ['start_date' => $request->get('start_date'), 'end_date' => $request->get('end_date')])
            ->with('status', '批次二次檢驗覆核成功');
    }

    //託運總表毛利審核
    public function index_check(Request $request)
    {
        $data = collect(json_decode($request->get('gpm_check_list', '[]')));
        //批次取消覆核
        foreach ($data->where('chk', false) as $d) {
            OrderShipmentStatusM::updateOrCreate(
                ['sales_no' => $d->sales_no],
                ['gpm_check' => '0', 'gpm_check_result' => '0']
            );
        }
        //批次覆核
        foreach ($data->where('chk', true) as $d) {
            OrderShipmentStatusM::updateOrCreate(
                ['sales_no' => $d->sales_no],
                ['gpm_check' => '1', 'gpm_check_result' => $d->gpm_check_result]
            );
        }
        return redirect()->route('transport.index', ['start_date' => $request->get('start_date'), 'end_date' => $request->get('end_date')])
            ->with('status', '毛利批次覆核成功');
    }

    //託運總表備註覆核
    public function index_note_check(Request $request)
    {
        $data = collect(json_decode($request->get('note_check_list', '[]')));
        //批次取消覆核
        foreach ($data->where('chk', false) as $d) {
            OrderShipmentStatusM::updateOrCreate(['sales_no' => $d->sales_no], ['note_check' => '0']);
        }
        //批次覆核
        foreach ($data->where('chk', true) as $d) {
            OrderShipmentStatusM::updateOrCreate(['sales_no' => $d->sales_no], ['note_check' => '1']);
        }
        return redirect()->route('transport.index', ['start_date' => $request->get('start_date'), 'end_date' => $request->get('end_date')])
            ->with('status', '備註批次覆核成功');
    }

    //託運總表狀態編輯
    public function index_edit(Request $request)
    {
        $request->flash();
        $data = $this->transportService->GetTransport($request);
        //排除親送
        if (key_exists('COM', $data['data'])) {
            unset($data['data']['COM']);
        }
        return view('transport.index_edit', $data);
    }

    //物流建議查詢
    public function suggest(Request $request)
    {
        $request->flash();
        $data = $this->transportService->GetTransportSuggest($request);
        return view('transport.suggest', compact('data'));
    }

    //上傳API檔案
    public function import_api(Request $request)
    {
        $settle_date = $request->get('settle_date', Carbon::today()->format('Y-m-d'));
        $selected_order = json_decode($request->get('selected_order'));
        $msg = $this->transportService->ConvertYcs($settle_date, $selected_order);
        if (empty($msg)) {
            return redirect()->route('ycs_transport.index')->with('status', '轉檔成功');
        } else {
            return redirect()->back()->with('status', '轉檔失敗。原因：' . $msg);
        }
    }

    //匯出託運總表，超取已上車
    public function export(Request $request)
    {
        $s_date = $request->get('start_date') ?? (new DateTime())->format('Y-m-d');
        $e_date = $request->get('end_date') ?? (new DateTime())->format('Y-m-d');
        $user = User::where('barcode', $request->get('user'))->first();
        $driver = $user->name2 ?? '';
        if (empty($driver)) {
            $driver = $user->name ?? '';
        }
        if (empty($driver)) {
            return redirect()->back()->with('status', '找不到員工');
        } else {
            $data = $this->transportService->GetTransport($request);
            return Excel::download(new TransportExport($driver, $s_date, $e_date, $data['data']), '超取已上車清單.' . \Maatwebsite\Excel\Excel::XLSX);
        }
    }

    //補上車
    public function ship(Request $request)
    {
        $sales = $request->get('sales', []);
        if (count($sales) == 0) {
            return redirect()->route('transport.index')->with('status', '補上車失敗，請選擇銷貨單');
        } else {
            $sales = Posein::select('PCOD1', 'PDAT1')->whereIn('PCOD1', $sales)->distinct()->get();
            foreach ($sales as $sale) {
                LogShipment::create([
                    'PCOD1' => $sale->PCOD1,
                    'PDATE' => $sale->PDAT1,
                    'status' => 1,
                ]);
            }
            return redirect()->route('transport.index')->with('status', '補上車成功');
        }
    }

    //取得毛利覆核所需資料
    public function ajax_get_order(Request $request)
    {
        $data =  $this->transportService->GetOrder($request);
        return response()->json($data);
    }

    //取得未上車
    public function ajax_get_unshipped()
    {
        $data  = $this->transportService->GetUnshipped();
        return response()->json($data);
    }

    //物流運費
    public function freight_index(Request $request)
    {
        $request->flash();
        $data = $this->transportService->GetFreight($request);
        return view('transport.freight_index',  $data);
    }

    //運費匯入
    public function import_freight(Request $request)
    {
        $msg = '';
        if ($request->hasFile('file')) {
            $type = $request->get('type');
            $file = $request->file('file');
            if ($file->isValid() && !empty($type)) {
                $import = new FreightImport($type);
                Excel::import($import, $file);
                $msg = $import->msg;
            } else {
                $msg = '檔案【' . $file->getClientOriginalName() . '】未通過驗證';
            }
        } else {
            $msg = '未選擇任何檔案';
        }

        if (empty($msg)) {
            return redirect()->back()->with('status', '匯入成功');
        } else {
            return redirect()->back()->with('status', $msg);
        }
    }
}
