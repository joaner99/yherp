<?php

namespace App\Http\Controllers;

use App\Models\yherp\CountyArea;
use App\Models\yherp\County;
use Illuminate\Http\Request;
use App\Models\yherp\LogisticsAddrMark;
use App\Http\Controllers\Controller;

class LogisticsAddrMarkController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:logistics_addr_mark');
    }

    //首頁
    function index()
    {
        $data = LogisticsAddrMark::with('county', 'area')->get()->sortBy('kind')->sortBy('county_id')->sortBy('area_id');
        return view('logistics_addr_mark.index', compact('data'));
    }

    public function create()
    {
        $countys = County::all();
        $areas = CountyArea::all();
        return view('logistics_addr_mark.create', compact('countys', 'areas'));
    }

    public function store(Request $request)
    {
        //驗證
        $this->validate($request, [
            'kind' => 'required',
            'county' => ['required', 'exists:mysql.county,id'],
            'area' => ['required', 'exists:mysql.county_area,id'],
        ]);
        LogisticsAddrMark::create([
            'kind' => $request->get('kind'),
            'county_id' => $request->get('county'),
            'area_id' => $request->get('area'),
        ]);

        return redirect()->route('logistics_addr_mark.index')
            ->with('status', '新增成功');
    }

    //刪除
    function destroy($id)
    {
        $m = LogisticsAddrMark::find($id);
        $m->delete();
        return redirect()->route('logistics_addr_mark.index')
            ->with('status', '刪除成功');
    }
}
