<?php

namespace App\Http\Controllers;


use DateTime;
use Illuminate\Http\Request;
use App\Imports\AllInvoiceImport;
use App\Models\yherp\CreditNoteM;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class CreditNoteController extends Controller
{

    function __construct()
    {
        $this->middleware('permission:credit_note_confirm')->only('confirm');
    }

    //
    public function index()
    {
        $data = CreditNoteM::with('Details')
            ->with(['Invoice.Posein' => function ($q) {
                $q->select('PCOD1', 'PJONO');
            }])
            ->with(['Poseou' => function ($q) {
                $q->select('PCOD1', 'PJONO');
            }])
            ->get()
            ->sortByDesc('total');
        //未建立TMS退單
        $undone = [];
        foreach ($data->where('tms_return_no', NULL)->where('return_no', '!=', NULL)->groupBy('return_no') as $return_no => $group) {
            $tms_return_no = $group->first()->Poseou->order_no ?? '';
            if (!empty($tms_return_no)) {
                foreach ($group as $cn_data) {
                    $cn_data->update(['tms_return_no' => $tms_return_no]);
                    $cn_data->save();
                }
                continue;
            }
            $undone[$return_no] = ['items' => [], 'sum_tax' => $group->sum('tax'), 'sum_total' => $group->sum('total')];
            foreach ($group as $cn_data) {
                $item_name = '';
                foreach ($cn_data->Details as $item) {
                    $item_name = $item->item_name;
                    $qty = $item->qty;
                    $unit = strval(round($item->unit, 2));
                    if (key_exists($item_name, $undone[$return_no]['items']) && key_exists($unit, $undone[$return_no]['items'][$item_name])) {
                        $undone[$return_no]['items'][$item_name][$unit] += $qty;
                    } else {
                        $undone[$return_no]['items'][$item_name][$unit] = $qty;
                    }
                }
                ksort($undone[$return_no]['items'][$item_name]); //排序單價
            }
            ksort($undone[$return_no]['items']); //排序品項
        }
        ksort($undone); //排序外掛退貨單號
        return view('credit_note', ['data' => $data, 'undone' => $undone]);
    }

    //上傳
    public function import(Request $request)
    {
        $msg = '';
        if ($request->hasFile('file')) {
            $file = $request->file('file');
            if ($file->isValid()) {
                $import = new AllInvoiceImport();
                Excel::import($import, $file);
                $msg .= $import->GetMsg();
            } else {
                $msg .= '檔案【' . $file->getClientOriginalName() . '】未通過驗證';
            }
        } else {
            $msg = '未選擇任何檔案';
        }

        if (empty($msg)) {
            return redirect()->back()->with('status', '匯入成功');
        } else {
            return redirect()->back()->with('status', $msg);
        }
    }

    //覆核折讓單
    public function confirm(Request $request)
    {
        $ids = $request->get('ids');
        if (empty($ids) || count($ids) < 1) {
            return redirect()->back()->with('status', '覆核折讓單失敗，找不到折讓單');
        } else {
            try {
                $data = CreditNoteM::whereIn('id', $ids)->where('status', 0);
                $data->update(['status' => 1]);
            } catch (\Exception $e) {
                return redirect()->back()->with('status', '覆核折讓單時，發生錯誤。原因：' . $e->getMessage());
            }
            return redirect()->back()->with('status', '覆核折讓單成功');
        }
    }

    //建立退貨單
    public function create_return(Request $request)
    {
        $ids = $request->get('ids');
        if (empty($ids) || count($ids) < 1) {
            return redirect()->back()->with('status', '外掛退貨單建立失敗，找不到折讓單');
        } else {
            $first_seq = (new DateTime())->format('Ymd0000');
            DB::connection('mysql')->beginTransaction();
            try {
                $db = CreditNoteM::lockForUpdate()
                    ->select('return_no')
                    ->where('return_no', '>', $first_seq)
                    ->where('status', 1)
                    ->orderBy('return_no', 'desc');
                $return_no = ($db->first()->return_no ?? $first_seq) + 1;
                $data = CreditNoteM::whereIn('id', $ids)->where('status', 1);
                $data->update(['return_no' => $return_no]);
            } catch (\Exception $e) {
                DB::connection('mysql')->rollback();
                return redirect()->back()->with('status', '外掛退貨單建立時，發生錯誤。原因：' . $e->getMessage());
            }
            DB::connection('mysql')->commit();
            return redirect()->back()->with('status', '外掛退貨單建立成功');
        }
    }
}
