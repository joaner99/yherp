<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use App\Models\yherp\OrderSampleM;
use App\Services\TransportService;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\yherp\PersonalTransport;
use App\Models\yherp\PersonalTransportSche;
use App\Exports\PersonalTransportExportShipment;

class PersonalTransportController extends Controller
{
    private $transportService = null;

    public function __construct(TransportService $transportService)
    {
        $this->transportService = $transportService;
    }

    //親送概況
    public function overview(Request $request)
    {
        $request->flash();
        $data = $this->transportService->GetPersonalTransportOverview($request);
        return view('personal_transport.overview', $data);
    }

    //親送總表
    public function index(Request $request)
    {
        $request->flash();
        $data = $this->transportService->GetPersonalTransport($request);
        return view('personal_transport.index', $data);
    }

    //轉人工包裝
    public function update_trans(Request $request)
    {
        $original_no = $request->get('original_no');
        $trans = $request->get('trans');
        if (empty($original_no) || empty($trans)) {
            return redirect()->route('personal_transport.index')->with('status', '轉人工包裝失敗');
        }
        OrderSampleM::where('order_no', $original_no)->update(['trans' => $trans]); //更新籃子
        PersonalTransport::where('original_no', $original_no)->delete(); //刪除親送總表資料
        return redirect()->back()->with('status', '轉人工包裝成功');
    }

    //匯出派車單
    public function export_shipment(Request $request)
    {
        $exportData = json_decode($request->export_data, true);
        $exportName = $request->export_name;
        if (empty($exportData) || empty($exportName)) {
            return redirect()->back();
        } else {
            return Excel::download(new PersonalTransportExportShipment($exportData), str_replace(['/', '$', ' '], '', $exportName) . '.' . \Maatwebsite\Excel\Excel::XLSX);
        }
    }

    //AJAX更新資料
    public function ajax_update(Request $request)
    {
        $data = $request->input();
        $msg = '';
        try {
            $original_no = $data['original_no'];
            unset($data['original_no']);
            PersonalTransport::updateOrCreate(['original_no' => $original_no], $data);
        } catch (Exception $ex) {
            $msg = $ex->getMessage();
        }
        if (empty($msg)) {
            return response()->json();
        } else {
            return response()->json(['msg' => '儲存失敗。原因：' . $msg], 404);
        }
    }

    //AJAX更新當日出車時間
    public function ajax_update_sche(Request $request)
    {
        $msg = '';
        try {
            $ship_date = $request->get('ship_date');
            $driver = $request->get('driver');
            $distance = $request->get('distance');
            $duration = $request->get('duration');
            if (empty($ship_date) || empty($driver) || empty($distance) || empty($duration)) {
                return response()->json(['msg' => '儲存失敗。原因：出車日期/人員/距離/時間為空'], 404);
            }
            PersonalTransportSche::updateOrCreate(
                ['ship_date' => $ship_date],
                ['driver' => $driver, 'distance' => $distance, 'duration' => $duration]
            );
        } catch (Exception $ex) {
            $msg = $ex->getMessage();
        }
        if (empty($msg)) {
            return response()->json();
        } else {
            return response()->json(['msg' => '儲存失敗。原因：' . $msg], 404);
        }
    }

    //親送天數
    public function ship_day(Request $request)
    {
        $request->flash();
        $data = $this->transportService->GetPTShipDay($request);
        return view('personal_transport.ship_day', $data);
    }
}
