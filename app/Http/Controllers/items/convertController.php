<?php

namespace App\Http\Controllers\items;

use Illuminate\Http\Request;
use App\Services\BaseService;
use App\Services\ItemService;
use App\Models\TMS\OrderDetail;
use App\Models\yherp\ItemConvertM;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class convertController extends Controller
{
    private $baseService = null;

    function __construct(BaseService $baseService)
    {
        $this->baseService = $baseService;
    }

    public function index()
    {
        $data = ItemConvertM::with('Details')->get();
        return view('items.convert.index', ['data' => $data]);
    }

    public function create()
    {
        $items = $this->baseService->baseRepository->GetItemAll();
        return view('items.convert.create', ['items' => $items]);
    }

    public function store(Request $request)
    {
        //主檔驗證
        $this->validate($request, [
            's_date' => ['required', 'date'],
            'e_date' => ['required', 'date'],
            'enabled' => ['required'],
            'remark' => ['required', 'max:50']
        ]);
        $s_date = $request->get('s_date');
        $e_date =  $request->get('e_date');
        if ($s_date > $e_date) {
            $tmp = $s_date;
            $s_date = $e_date;
            $e_date = $tmp;
        }
        $m = ItemConvertM::create([
            's_date' => $s_date,
            'e_date' => $e_date,
            'enabled' => $request->get('enabled'),
            'remark' => $request->get('remark'),
        ]);
        //明細檔驗證
        $items = json_decode($request->get('details'), true);
        $rules = [
            '*.item_no' => ['required', 'max:11'],
            '*.src_item_no' => ['required', 'max:30'],
        ];
        $validator = Validator::make($items, $rules);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        } else {
            foreach ($items as $item) {
                $m->Details()->create([
                    'src_item_no' => $item['src_item_no'],
                    'item_no' => $item['item_no'],
                ]);
            }
            return redirect()->route('items.convert.index')->with('status', '建立成功');
        }
    }

    public function edit($id)
    {
        $data = ItemConvertM::with('Details', 'Details.SrcItem:ICODE,INAME', 'Details.Item:ICODE,INAME')->find($id);
        $items = $this->baseService->baseRepository->GetItemAll();
        return view('items.convert.edit', ['items' => $items, 'data' => $data]);
    }

    public function update($id, Request $request)
    {
        //主檔驗證
        $this->validate($request, [
            's_date' => ['required', 'date'],
            'e_date' => ['required', 'date'],
            'enabled' => ['required'],
            'remark' => ['required', 'max:50']
        ]);
        $s_date = $request->get('s_date');
        $e_date =  $request->get('e_date');
        if ($s_date > $e_date) {
            $tmp = $s_date;
            $s_date = $e_date;
            $e_date = $tmp;
        }
        $m = ItemConvertM::find($id);
        $m->update([
            's_date' => $s_date,
            'e_date' => $e_date,
            'enabled' => $request->get('enabled'),
            'remark' => $request->get('remark'),
        ]);
        //明細檔驗證
        $items = json_decode($request->get('details'), true);
        $rules = [
            '*.item_no' => ['required', 'max:11'],
            '*.src_item_no' => ['required', 'max:30'],
        ];
        $validator = Validator::make($items, $rules);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        } else {
            $m->Details()->delete();
            foreach ($items as $item) {
                $m->Details()->create([
                    'src_item_no' => $item['src_item_no'],
                    'item_no' => $item['item_no'],
                ]);
            }
            return redirect()->route('items.convert.index')->with('status', '編輯成功');
        }
    }

    public function destroy($id)
    {
        $m = ItemConvertM::find($id);
        $m->Details()->delete();
        $m->delete();

        return redirect()->route('items.convert.index')
            ->with('status', '刪除成功');
    }

    //需轉換料號清單
    public function list()
    {
        $item_all_name = $this->baseService->GetItemAllList()->pluck('INAME', 'ICODE')->toArray();
        $convert_items = (new ItemService())->GetItemConvert();
        $items = array_unique(array_map('strval', array_keys($convert_items)));
        $data = OrderDetail::select('OCOD1', 'OICOD', 'OINAM')
            ->whereIn('OICOD', $items)
            ->get()
            ->map(function ($item) use ($convert_items, $item_all_name) {
                $convert_item_no = $convert_items[$item->item_no] ?? '';
                $item->convert_item_no = $convert_item_no;
                $item->convert_item_name = $item_all_name[$convert_item_no] ?? '';
                return $item;
            });
        return view('items.convert.list', ['data' => $data]);
    }
}
