<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\BaseService;
use App\Http\Controllers\Controller;

class HctTransportController extends Controller
{
    private $baseService = null;

    public function __construct(BaseService $baseService)
    {
        $this->baseService = $baseService;
    }

    //新竹偏遠地區查詢
    public function remote_index(Request $request)
    {
        $request->flash();
        $data =  $this->baseService->GetHctRemote($request);
        return view('hct_transport.remote_index', $data);
    }
}
