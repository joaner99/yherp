<?php

namespace App\Http\Controllers;

use DateTime;
use Exception;
use Illuminate\Http\Request;
use App\Services\BaseService;
use App\Models\yherp\OrderRemarkM;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class SalesLostCheckController extends Controller
{
    private $baseService = null;

    public function __construct(BaseService $baseService)
    {
        $this->baseService = $baseService;
    }

    //總表
    public function index()
    {
        $data = $this->baseService->GetSalesLostCheck();
        return view('sales_lost_check', $data);
    }

    //未完成訂單更新備註
    public function ajax_update(Request $request)
    {
        $order_no = $request->get('order_no');
        $track = $request->get('track', 0);
        $content = $request->get('content');
        if (empty($order_no) || empty($content)) {
            return response()->json(['msg' => '備註失敗，訂單號或備註為空'], 404);
        } else {
            try {
                $m = OrderRemarkM::updateOrCreate(
                    ['OCOD1' => $order_no],
                    ['track' => $track,]
                );
                $d = $m->Details()->create([
                    'user_id' => Auth::user()->id,
                    'content' => $content
                ]);
                return response()->json([
                    'user' => Auth::user()->name,
                    'time' => (new DateTime($d->created_at))->format('Y-m-d H:i:s'),
                    'track' => $m->track,
                    'content' => $d->content
                ], 200);
            } catch (Exception $ex) {
                return response()->json(['msg' => '備註失敗，原因：' . $ex->getMessage()], 404);
            }
        }
    }

    //取得備註資料
    public function ajax_get_remark(Request $request)
    {
        $order_no = $request->get('order_no');
        if (empty($order_no)) {
            return response()->json(['msg' => '備註取得發生錯誤，訂單號為空'], 404);
        } else {
            try {
                $data = [
                    'order_no' => $order_no,
                    'track' => 0,
                    'content' => '',
                    'details' => [],
                ];
                $m = OrderRemarkM::with('Details', 'Details.User')
                    ->where('OCOD1', $order_no)
                    ->first();
                if (!empty($m)) {
                    $data['track'] = $m->track;
                    $flag = 0;
                    $details = [];
                    foreach ($m->Details->sortByDesc('created_at') as $d) {
                        if ($flag == 0) {
                            $data['content'] = $d->content;
                            $flag++;
                        }
                        $details[] = [
                            'user' => $d->User->name,
                            'content' => $d->content,
                            'time' => (new DateTime($d->created_at))->format('Y-m-d H:i:s'),
                        ];
                    }
                    $data['details'] = $details;
                }
                return response()->json($data, 200);
            } catch (Exception $ex) {
                return response()->json(['msg' => '備註取得發生錯誤，原因：' . $ex->getMessage()], 404);
            }
        }
    }

    //未完成訂單結案
    public function complete(Request $request)
    {
        $order_no = $request->get('order_no');
        if (empty($order_no)) {
            return redirect()->back()->with(['status' => '結案失敗，訂單號為空']);
        } else {
            OrderRemarkM::where('OCOD1', $order_no)->update([
                'completed_at' => new DateTime(),
            ]);
            return redirect()->back()->with(['status' => '結案成功']);
        }
    }
}
