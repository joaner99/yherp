<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use App\Services\HelpService;
use App\Models\yherp\HelpShops;
use App\Models\yherp\HelpGroups;
use App\Models\yherp\HelpActions;
use App\Models\yherp\HelpQuestions;
use App\Http\Controllers\Controller;
use App\Models\yherp\HelpQuestionImg;
use App\Models\yherp\HelpQuestionItem;
use App\Models\yherp\HelpQuestionKind;

class HelpController extends Controller
{
    private const ImgsPath = 'img/questions';
    private $helpService = null;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct(HelpService $helpService)
    {
        $this->middleware('permission:help_modify')->except(['index', 'search', 'ajax_get_ans']);
        $this->helpService = $helpService;
    }

    //首頁
    function index(Request $request)
    {
        $data = $this->helpService->GetIndexViewData($request);
        return view('help.index', $data);
    }

    //查詢
    function search(Request $request)
    {
        $data = $this->helpService->GetSearchResult($request);
        return view('help.search', $data);
    }

    //新增問題
    function create()
    {
        $groups = HelpGroups::select('id', 'name')->get();
        $shops = HelpShops::select('id', 'name')->get();
        $actions = HelpActions::select('id', 'name')->get();
        $types = $this->helpService->GetItemType();
        $items = $this->helpService->GetItemAllList();

        return view('help.create', compact('groups', 'shops', 'actions', 'types', 'items'));
    }

    //儲存問題
    function store(Request $request)
    {
        //驗證
        $this->validate($request, [
            'question' => ['required', 'max:20'],
            'qroup' => ['exists:mysql.help_question_qroup,id'],
            'answer' => ['required', 'max:65535'],
            'remark' => ['max:65535'],
            'shops' => ['required', 'exists:mysql.help_shops,id'],
            'actions' => ['required', 'exists:mysql.help_actions,id'],
            'type' => ['required', 'integer']
        ]);
        //問題
        $q = HelpQuestions::create([
            'group_id' => $request->get('group'),
            'content' => $request->get('question'),
            'answer' => $request->get('answer'),
            'remark' => $request->get('remark', '')
        ]);
        //銷售平台
        $shops = HelpShops::find($request->get('shops'));
        $q->Shops()->attach($shops);
        //行為
        $actions = HelpShops::find($request->get('actions'));
        $q->Actions()->attach($actions);
        //關聯
        $type = $request->get('type');
        $kinds = json_decode($request->get('kinds'));
        $items = json_decode($request->get('items'));
        if ($type > 0) { //非通用類
            if (!empty($items) && count($items) > 0) { //指定料號
                foreach ($items as $d) {
                    $q->Items()->create([
                        'item_no' => $d->item_no,
                    ]);
                }
            } else if (!empty($kinds) && count($kinds) > 0) { //指定大中小類
                foreach ($kinds as $d) {
                    $q->Kinds()->create([
                        'kind' => $d->kind,
                        'kind2' => $d->kind2,
                        'kind3' => $d->kind3,
                    ]);
                }
            }
        }
        //圖片
        if ($request->hasFile('add_imgs')) {
            $files = $request->file('add_imgs');
            $m_id = $q->id;
            foreach ($files as $file) {
                if (!$file->isValid()) {
                    continue;
                }
                $extension = $file->extension();
                $fileName = uniqid() . '.' . $extension;
                $imgPath = self::ImgsPath . '/' . $m_id;
                $file->storeAs('public/' . $imgPath, $fileName);
                $path = $imgPath . '/' . $fileName;
                $q->Imgs()->create(['path' => $path]);
            }
        }

        return redirect()->route('help.edit_index')
            ->with('status', '新增成功');
    }

    //編輯題庫清單
    function edit_index(Request $request)
    {
        $request->flash();
        $data = HelpQuestions::with('Kinds')->with('Items')->with('Group')->with('Shops')->with('Actions')->orderByDesc('updated_at');
        if (!empty($request->get('with_trashed'))) {
            $data->onlyTrashed();
        }
        $data = $data->get();
        //設定類別名稱、料號名稱
        $items = $this->helpService->GetItemAllList();
        $types = $this->helpService->GetItemType();
        foreach ($data as $d) {
            if (!empty($d->Kinds) && count($d->Kinds) > 0) {
                foreach ($d->Kinds as $item) {
                    $kind = $item->kind;
                    $kind2 = $item->kind2;
                    $kind3 = $item->kind3;
                    if ($kind != '') {
                        $item->kind_name = $types[1][$kind] ?? '';
                    } else {
                        $item->kind_name = '';
                    }
                    if ($kind2 != '') {
                        $item->kind2_name = $types[2][$kind2] ?? '';
                    } else {
                        $item->kind2_name = '';
                    }
                    if ($kind3 != '') {
                        $item->kind3_name = $types[3][$kind3] ?? '';
                    } else {
                        $item->kind3_name = '';
                    }
                }
            } elseif (!empty($d->Items) && count($d->Items) > 0) {
                foreach ($d->Items as $item) {
                    $item_no = $item->item_no;
                    $item->item_name = $items->firstWhere('ICODE', $item_no)->INAME ?? '';
                }
            }
        }
        $isEmptyGroup = $data->where('group_id', '!=', null)->count() == 0;
        return view('help.edit_index', compact('data', 'isEmptyGroup'));
    }

    //編輯
    function edit($id)
    {
        $shops = HelpShops::select('id', 'name')->get();
        $actions = HelpActions::select('id', 'name')->get();
        $groups = HelpGroups::select('id', 'name')->get();
        $items = $this->helpService->GetItemAllList();
        $types = $this->helpService->GetItemType();
        $data = HelpQuestions::with('Kinds')->with('Items')->with('Shops')->with('Actions')->find($id);
        if (empty($data)) {
            return redirect()->route('help.edit_index')
                ->with('status', '編輯失敗，找不到該問題');
        } else {
            return view('help.edit', compact('data', 'shops', 'actions', 'groups', 'items', 'types'));
        }
    }

    //更新
    public function update(Request $request, $id)
    {
        //驗證
        $this->validate($request, [
            'question' => ['required', 'max:20'],
            'qroup' => ['exists:mysql.help_question_qroup,id'],
            'answer' => ['required', 'max:65535'],
            'remark' => ['max:65535'],
            'shops' => ['required', 'exists:mysql.help_shops,id'],
            'actions' => ['required', 'exists:mysql.help_actions,id'],
            'type' => ['required', 'integer']
        ]);
        //主檔
        $main = HelpQuestions::find($id);
        $main->update([
            'group_id' => $request->get('group'),
            'content' => $request->get('question'),
            'answer' => $request->get('answer'),
            'remark' => $request->get('remark', '')
        ]);
        //銷售平台
        $shops = HelpShops::find($request->get('shops'));
        $main->Shops()->sync($shops);
        //行為
        $actions = HelpActions::find($request->get('actions'));
        $main->Actions()->sync($actions);
        //大中小類
        $kinds = json_decode($request->get('kinds'));
        $ids = collect($kinds)->where('id', '!=', '')->pluck('id')->toArray(); //原資料ID
        if (empty($ids)) { //編輯後，無舊資料，刪除全部
            $main->Kinds()->delete();
        } else { //編輯後，有舊資料，刪除不在編輯過後的舅資料
            $main->Kinds()->whereNotIn('id', $ids)->delete();
        }
        if (!empty($kinds)) {
            foreach ($kinds as $detail) {
                $id = $detail->id;
                if (empty($id)) { //無id，新增
                    $main->Kinds()->create([
                        'kind' => $detail->kind,
                        'kind2' => $detail->kind2,
                        'kind3' => $detail->kind3,
                    ]);
                } else { //有id，更新
                    $main->Kinds()->find($id)->update([
                        'kind' => $detail->kind,
                        'kind2' => $detail->kind2,
                        'kind3' => $detail->kind3,
                    ]);
                }
            }
        }
        //料號
        $items = json_decode($request->get('items'));
        $ids = collect($items)->where('id', '!=', '')->pluck('id')->toArray(); //原資料ID
        if (empty($ids)) { //編輯後，無舊資料，刪除全部
            $main->Items()->delete();
        } else { //編輯後，有舊資料，刪除不在編輯過後的舅資料
            $main->Items()->whereNotIn('id', $ids)->delete();
        }
        if (!empty($items)) {
            foreach ($items as $detail) {
                $id = $detail->id;
                if (empty($id)) { //無id，新增
                    $main->Items()->create([
                        'item_no' => $detail->item_no,
                    ]);
                } else { //有id，更新
                    $main->Items()->find($id)->update([
                        'item_no' => $detail->item_no,
                    ]);
                }
            }
        }
        //刪除圖片
        $delete_imgs = json_decode($request->get('delete_imgs'));
        if (!empty($delete_imgs)) {
            foreach ($delete_imgs as $img_id) {
                HelpQuestionImg::find($img_id)->delete();
            }
        }
        //圖片
        if ($request->hasFile('add_imgs')) {
            $files = $request->file('add_imgs');
            $m_id = $main->id;
            foreach ($files as $file) {
                if (!$file->isValid()) {
                    continue;
                }
                $extension = $file->extension();
                $fileName = uniqid() . '.' . $extension;
                $imgPath = self::ImgsPath . '/' . $m_id;
                $file->storeAs('public/' . $imgPath, $fileName);
                $path = $imgPath . '/' . $fileName;
                $main->Imgs()->create(['path' => $path]);
            }
        }

        return redirect()->route('help.edit_index')
            ->with('status', '題目編輯成功');
    }

    //刪除題庫
    function destroy($id)
    {
        $m = HelpQuestions::find($id);
        $m->Items()->delete();
        $m->Kinds()->delete();
        $m->Imgs()->delete();
        $m->delete();
        return redirect()->route('help.edit_index')
            ->with('status', '題目刪除成功');
    }

    //永久刪除題庫
    function force_destroy($id)
    {
        $m = HelpQuestions::withTrashed()->find($id);
        $m->Items()->forceDelete();
        $m->Kinds()->forceDelete();
        $m->Imgs()->forceDelete();
        $m->forceDelete();
        return redirect()->route('help.edit_index')
            ->with('status', '題目永久刪除成功');
    }

    //還原題庫
    function restore($id)
    {
        $m = HelpQuestions::onlyTrashed()->find($id);
        $m->Items()->restore();
        $m->Kinds()->restore();
        $m->restore();
        return redirect()->route('help.edit_index')
            ->with('status', '題目還原成功');
    }

    //AJAX取得答案
    function ajax_get_ans(Request $request)
    {
        try {
            $id = $request->get('id');
            if (empty($id)) {
                return response();
            } else {
                $result = HelpQuestions::with('Imgs')->find($id);
                return response()->json($result);
            }
        } catch (Exception $ex) {
            return response()->json(['msg' => '取得答案時，發生無法預期的錯誤。原因：' . $ex->getMessage()], 404);
        }
    }

    //AJAX取得問題關聯
    function ajax_get_details(Request $request)
    {
        try {
            $id = $request->get('id');
            if (empty($id)) {
                return response();
            } else {
                $result = [];
                $items = HelpQuestionItem::where('question_id', $id)->where('item_no', '!=', '')->get();
                $kinds = HelpQuestionKind::where('question_id', $id)->get();
                if (count($items) > 0) { //指定料號
                    $itemsDetail = collect();
                    $item_no_list = $items->pluck('item_no')->toArray();
                    if (count($item_no_list) > 0) {
                        $itemsDetail = $this->helpService->GetItemAllDetail($item_no_list);
                    }
                    $details = [];
                    foreach ($itemsDetail as $item) {
                        $details[] = [
                            'item_no' => $item->ICODE,
                            'item_name' => $item->INAME,
                        ];
                    }
                    $result['type'] = 1;
                    $result['details'] = $details;
                } else if (count($kinds) > 0) { //指定大中小
                    $types = $this->helpService->GetItemType();
                    $details = [];
                    foreach ($kinds as $item) {
                        $kind = $item->kind;
                        $kind2 = $item->kind2;
                        $kind3 = $item->kind3;
                        if ($kind != '') {
                            $kind_name = $types[1][$kind] ?? '';
                        } else {
                            $kind_name = '';
                        }
                        if ($kind2 != '') {
                            $kind2_name = $types[2][$kind2] ?? '';
                        } else {
                            $kind2_name = '';
                        }
                        if ($kind3 != '') {
                            $kind3_name = $types[3][$kind3] ?? '';
                        } else {
                            $kind3_name = '';
                        }
                        $details[] = [
                            'kind' => $kind,
                            'kind_name' => $kind_name,
                            'kind2' => $kind2,
                            'kind2_name' => $kind2_name,
                            'kind3' => $kind3,
                            'kind3_name' => $kind3_name,
                        ];
                    }
                    $result['type'] = 2;
                    $result['details'] = $details;
                }
                return response()->json($result);
            }
        } catch (Exception $ex) {
            return response()->json(['msg' => '取得關聯時，發生無法預期的錯誤。原因：' . $ex->getMessage()], 404);
        }
    }
}
