<?php

namespace App\Http\Controllers;

use App\Services\ToolService;
use Illuminate\Http\Request;

class ToolController extends Controller
{
    private $toolService = null;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct(ToolService $toolService)
    {
        $this->toolService = $toolService;
    }

    public function index()
    {
        $data = $this->toolService->get_floor_color_area();
        return view('tool.index', $data);
    }

    //地板銷售
    public function floor_sales(Request $request)
    {
        $request->flash();
        $data = $this->toolService->get_floor_sales($request);
        return view('tool.floor_sales', $data);
    }

    //親送司機個地區品項加總
    public function personal_transport_count()
    {
        $data = $this->toolService->get_personal_transport_count();
        return view('tool.personal_transport_count', $data);
    }
}
