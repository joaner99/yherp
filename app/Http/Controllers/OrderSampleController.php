<?php

namespace App\Http\Controllers;

use Exception;
use Carbon\Carbon;
use App\Exports\BaseExport;
use Illuminate\Http\Request;
use App\Exports\ShopeeTransExport;
use App\Imports\OrderSampleImport;
use App\Models\yherp\OrderSampleM;
use App\Http\Controllers\Controller;
use App\Services\OrderSampleService;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\TransportConvertImport;

class OrderSampleController extends Controller
{
    private $orderSampleService = null;

    public function __construct(OrderSampleService $orderSampleService)
    {
        $this->orderSampleService = $orderSampleService;
    }

    //特殊訂單轉檔下載頁面
    public function download_page()
    {
        $data = $this->orderSampleService->GetDownloadView();
        return view('order_sample.download_page', $data);
    }

    //特殊訂單轉檔
    public function convert(Request $request)
    {
        $request->flash();
        $data = $this->orderSampleService->GetOrderSampleConvertView($request);
        return view('order_sample.convert', $data);
    }

    //確認訂單
    public function confirm(Request $request)
    {
        try {
            $idList = json_decode($request->get('id_list'));
            if (!empty($idList) && count($idList) > 0) {
                $orderSample = OrderSampleM::whereIn('id', $idList)->where('confirmed', 0)->get();
                foreach ($orderSample as $item) {
                    $item->confirmed = 1;
                    $item->save();
                }
                return redirect()->back()->with('status', '確認成功');
            } else {
                return redirect()->back()->with('status', '確認失敗，原因：找不到勾選的訂單');
            }
        } catch (\Exception  $ex) {
            return redirect()->back()->with('status', "確認失敗，原因：{$ex->getMessage()}");
        }
    }

    //取消確認訂單
    public function cancel_confirm(Request $request)
    {
        try {
            $idList = json_decode($request->get('id_list'));
            if (!empty($idList) && count($idList) > 0) {
                $orderSample = OrderSampleM::whereIn('id', $idList)->where('confirmed', 1)->get();
                foreach ($orderSample as $item) {
                    $item->confirmed = 0;
                    $item->save();
                }
                return redirect()->back()->with('status', '取消確認成功');
            } else {
                return redirect()->back()->with('status', '取消確認失敗，原因：找不到勾選的訂單');
            }
        } catch (\Exception  $ex) {
            return redirect()->back()->with('status', "取消確認失敗，原因：{$ex->getMessage()}");
        }
    }

    //重做訂單
    public function redo(Request $request)
    {
        try {
            $order_no = $request->get('order_no');
            $m = OrderSampleM::where('completed', 1)->where('order_no', $order_no)->first();
            if (empty($m)) {
                return redirect()->back()->with('status', '重做失敗，找不到該訂單');
            } else {
                $m->completed = 0;
                $m->save();
                return redirect()->back()->with('status', '重做成功');
            }
        } catch (\Exception  $ex) {
            return redirect()->back()->with('status', "重做失敗，原因：{$ex->getMessage()}");
        }
    }

    //編輯訂單
    public function update(Request $request)
    {
        try {
            $id = $request->get('id');
            $orderSampleM = OrderSampleM::find($id);
            if (empty($orderSampleM)) {
                return redirect()->back()->with('status', "編輯失敗，原因：找不到該筆資料");
            } else {
                $orderSampleM->update([
                    'remark' => $request->get('remark'),
                ]);
                return redirect()->back()->with('status', '編輯成功');
            }
        } catch (\Exception  $ex) {
            return redirect()->back()->with('status', "編輯失敗，原因：{$ex->getMessage()}");
        }
    }

    //刪除訂單
    public function delete(Request $request)
    {
        try {
            $id = $request->get('id');
            if (!empty($id)) {
                OrderSampleM::destroy($id);
                return redirect()->back()->with('status', '刪除成功');
            } else {
                return redirect()->back()->with('status', '刪除失敗，原因：找不到指定流水號');
            }
        } catch (\Exception  $ex) {
            return redirect()->back()->with('status', "刪除失敗，原因：{$ex->getMessage()}");
        }
    }

    //特殊訂單匯入
    public function import(Request $request)
    {
        set_time_limit(300); //5分鐘
        $msg = '';
        $type = $request->type;
        if ($request->hasFile('file')) {
            $files = $request->file('file');
            foreach ($files as $file) {
                if ($file->isValid()) {
                    $import = new OrderSampleImport($type);
                    Excel::import($import, $file);
                    $msg .= $import->msg;
                } else {
                    $msg .= '檔案【' . $file->getClientOriginalName() . '】未通過驗證';
                }
            }
        } else {
            $msg = '未選擇任何檔案';
        }

        if (empty($msg)) {
            return redirect()->back()->with('status', '匯入成功');
        } else {
            return redirect()->back()->with('status', $msg);
        }
    }

    //特殊訂單下載
    public function order_export(Request $request)
    {
        try {
            $defaultDate = Carbon::today()->format('Y-m-d');
            $start_date = $request->get('start_date', $defaultDate);
            $end_date = $request->get('end_date', $defaultDate);
            $src = $request->get('src', '0');
            $type = $request->get('type', '1');
            $exportName =  $start_date . '_' .  $end_date; //日期
            //平台來源
            if (key_exists($src, OrderSampleService::SrcType)) {
                $exportName .= OrderSampleService::SrcType[$src];
            } else {
                return redirect()->back();
            }
            //匯出類型
            if (key_exists($type, OrderSampleService::ExportType)) {
                $exportName .= OrderSampleService::ExportType[$type];
            } else {
                return redirect()->back();
            }
            $exportData = $this->orderSampleService->GetExport($start_date . ' 00:00:00', $end_date . ' 23:59:59', $src, $type);
            $this->orderSampleService->CreateOrderSampleExportLog($start_date, $end_date, $src, $type, $exportData['data']);
            return Excel::download(new BaseExport($exportData['data']), $exportName . '.' . \Maatwebsite\Excel\Excel::XLSX);
        } catch (Exception $ex) {
            return response()->json(['msg' => '匯出時，發生無法預期的錯誤。原因：' . $ex->getMessage()], 404);
        }
    }

    //蝦皮宅配出貨轉檔
    public function shopee_trans(Request $request)
    {
        $data = $request->session()->get('shopee_trans');
        return view('order_sample.shopee_trans', ['data' => $data]);
    }

    //蝦皮宅配出貨上傳
    public function shopee_trans_convert(Request $request)
    {
        $msg = '';
        $import = new TransportConvertImport();
        if ($request->hasFile('file') && $request->file('file')->isValid()) {
            $file = $request->file('file');
            Excel::import($import, $file);
            $msg = $import->msg;
        } else {
            $msg = '找不到上傳檔案';
        }
        if (empty($msg)) {
            $data = $import->data;
            $request->session()->flash('shopee_trans', $data);
            return redirect()->action('OrderSampleController@shopee_trans');
        } else {
            return redirect()->back()->with('status', '蝦皮宅配物流轉檔失敗。原因：' . $msg);
        }
    }

    //蝦皮宅配出貨匯出
    public function shopee_trans_export(Request $request)
    {
        $export_data = json_decode($request->get('export_data', '[]'));
        $export_name = '蝦皮宅配出貨' . Carbon::today()->format('Y-m-d');
        return Excel::download(new ShopeeTransExport($export_data), $export_name . '.' . \Maatwebsite\Excel\Excel::XLSX);
    }

    //AJAX更新訂單
    public function ajax_update_order(Request $request)
    {
        try {
            $id = $request->get('id');
            if (empty($id)) {
                $original_no = $request->get('original_no');
                $m = OrderSampleM::where('order_no', $original_no)->first();
            } else {
                $m = OrderSampleM::find($id);
            }
            if (empty($m)) {
                return response()->json(['msg' => '找不到該訂單'], 404);
            } else {
                if ($request->has('order_kind')) { //訂單類型
                    $m->order_kind = $request->get('order_kind');
                }
                if ($request->has('trans')) { //預排物流
                    $m->trans = $request->get('trans');
                    //親送總表建立資料
                    if ($m->trans == 'COM' && empty($m->PersonalTransport)) {
                        $m->PersonalTransport()->create([
                            'original_no' => $m->order_no,
                            'remark' => $m->remark,
                        ]);
                    }
                }
                if ($request->has('remark')) { //撿貨單備註
                    $m->remark = $request->get('remark');
                }
                if ($request->has('estimated_date')) { //預計出貨日
                    $m->estimated_date = $request->get('estimated_date');
                }
                $m->save();
                return response()->json();
            }
        } catch (Exception $ex) {
            return response()->json(['msg' => '更新訂單時，發生無法預期的錯誤。原因：' . $ex->getMessage()], 404);
        }
    }
}
