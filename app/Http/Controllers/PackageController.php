<?php


namespace App\Http\Controllers;


use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Services\PackageService;
use App\Http\Controllers\Controller;
use App\Models\yherp\OrderShipmentStatusM;

class PackageController extends Controller
{
    private $package_Service = null;

    public function __construct(PackageService $package_Service)
    {
        $this->package_Service = $package_Service;
    }

    //首頁
    public function index(Request $request)
    {
        $msg = $request->get('msg');
        $user_id = $request->get('user_id');
        if (empty($user_id)) {
            return view('package.index', ['msg' => $msg]);
        } else {
            $pass = $request->get('pass');
            $user = User::find($user_id);
            if (empty($user)) {
                return view('package.index', ['msg' => $msg]);
            } else {
                return view('package.index', ['msg' => $msg, 'user_id' => $user->id, 'user_name' => $user->name, 'pass' => $pass]);
            }
        }
    }

    //包裝驗貨開始
    public function check_start(Request $request)
    {
        $data = $this->package_Service->start_checking($request);
        if (empty($data['msg'])) {
            if (isset($data['skip'])) { //免包裝，無須包裝驗貨，直接跳轉包裝開始
                return redirect()->action('PackageController@start', $data);
            } else {
                return view('package.check_start', $data);
            }
        } else {
            return redirect()->action('PackageController@index', ['msg' => $data['msg'], 'user_id' =>  $data['user_id']]);
        }
    }

    //包裝驗貨儲存狀態
    public function check_store($sale_no, Request $request)
    {
        $m = OrderShipmentStatusM::firstOrCreate(['sales_no' => $sale_no]);
        $m->estimated_date = Carbon::today()->format('Y-m-d');
        //更新狀態
        $m->Details()->firstOrCreate(['_status' => $request->get('status', 3)]);
        $m->save();
        return redirect()->action('PackageController@index', ['msg' =>  "銷貨單號：{$sale_no}。因驗貨錯誤超過3次鎖定包裹。需廠務覆核", 'user_id' => $request->get('user_id')]);
    }

    //包裝開始
    public function start(Request $request)
    {
        $data = $this->package_Service->start_packing($request);
        if (empty($data['msg'])) {
            if ($data['type'] == 0) { //免包裝，導向首頁
                return view('package.index', array_merge($data['data'], ['no_package' => 1]));
            } else {
                return view('package.start', $data);
            }
        } else {
            return view('package.index', $data);
        }
    }

    //包裝結束
    public function end(Request $request)
    {
        $data = $this->package_Service->end_packing($request);
        if (empty($data['msg'])) {
            return redirect()->action('PackageController@index', ['user_id' =>  $data['user_id'], 'pass' => $data['pass']]);
        } else {
            $request->session()->flash('msg', $data['msg']);
            return redirect()->back();
        }
    }

    //包裝清單
    public function list(Request $request)
    {
        $request->flash();
        $data = $this->package_Service->GetPackageList($request);
        return view('package.list', $data);
    }

    /**
     * 訂單包裝人員查詢
     *
     * @return \Illuminate\Http\Response
     */
    public function order_packager(Request $request)
    {
        $request->flash();
        $data = $this->package_Service->GetPackager($request);
        $viewData = $this->package_Service->GetOrderPackagerView($request);

        return view('package.order_packager', compact('data', 'viewData'));
    }
}
