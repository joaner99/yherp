<?php

namespace App\Http\Controllers;

use DateTime;
use App\Models\TMS\Posein;
use Illuminate\Http\Request;
use App\Services\ReceiptService;
use App\Http\Controllers\Controller;
use App\Models\yherp\CustomerComplaint;
use App\Models\yherp\ItemReceiptReturn;

class ReceiptReturnController extends Controller
{
    public function index(Request $request)
    {
        $request->flash();
        $scrapped_items = [];
        $data = ItemReceiptReturn::with([
            'Details',
            'Posein:Posein.PCOD1,Posein.PJONO,Posein.PPNAM',
            'Poseou:Poseou.PCOD1,Poseou.PCOD2,Poseou.PTIME',
            'Poseou.Histou:HCOD1,HICOD,HINAM,HTCOD,HINUM'
        ]);
        if (!$request->get('include_done')) {
            $data->where('completed_at', NULL); //未結案
        }
        $data = $data->get();
        foreach ($data as $key => $d) {
            $tms_done = false;
            if (empty($d->Posein)) { //排除銷貨單已刪除
                $tms_done = true;
            }
            $return_data = $d->Poseou->where('PTIME', '>=', $d->created_at)->sortBy('created_at')->first();
            if (!empty($return_data)) {
                $d->return_no = $return_data->PCOD1; //取最靠近退貨檢驗日期的退貨單
                $d->save();
                $tms_done = true;
                //判斷明細是否報廢
                foreach ($return_data->Histou->where('HTCOD', 'I001')->groupBy('HICOD') as $item_no => $group) {
                    $first = $group->first();
                    $scrapped_items[$d->id][] = [
                        'item_no' => $item_no,
                        'item_name' => $first->HINAM,
                        'qty' => $group->sum('HINUM'),
                    ];
                }
            }
            $d->tms_done = $tms_done;
        };
        return view('receipt_return.index', ['data' => $data->sortBy('created_at'), 'scrapped_items' => $scrapped_items]);
    }

    public function create(Request $request)
    {
        $request->flash();
        $keyword = $request->get('keyword');
        $data = null;
        $msg = '';
        $skip = false;
        $need_check = false;
        if (!empty($keyword)) {
            //解密銷貨單號，16進位長度比11小
            if (strlen($keyword) < 11 && ctype_xdigit($keyword)) {
                $sale_no = hexdec($keyword);
            } else {
                $sale_no = '';
            }
            $data = Posein::select('PCOD1', 'PCOD2', 'PJONO', 'PPNAM', 'ConsignTran', 'PTOT1', 'PTOTA')
                ->withWhereHas('Histin', function ($q) {
                    $q
                        ->select('HCOD1', 'HICOD', 'HINAM', 'HUNIT', 'HINUM', 'HTOTA')
                        ->where('HINUM', '>', 0)
                        ->where(function ($q) {
                            $q->whereNotIn('HYCOD', ['98'])
                                ->orWhereIn('HICOD', ['99010014001', '99010014002', '99010014003', '99010014004']);
                        });
                })
                ->where(function ($q) use ($keyword, $sale_no) {
                    $q
                        ->where('PCOD1', $keyword)
                        ->orWhere('PJONO', $keyword)
                        ->orWhere('ConsignTran', $keyword);
                    if (!empty($sale_no)) {
                        $q->orWhere('PCOD1', $sale_no);
                    }
                })
                ->first();
            if (empty($data)) {
                $msg = '找不到銷貨資料';
            } else if (count(ItemReceiptReturn::where('sale_no', $data->PCOD1)->get()) > 0) {
                $data = null;
                $msg = '已填寫過退貨單';
            } else {
                $order_no = $data->order_no;
                $origin_no = $data->original_no;
                $need_check = CustomerComplaint::whereNotIn('status', [0, 99])
                    ->where('mark', 'LIKE', '%7%')
                    ->where(function ($q) use ($order_no, $origin_no) {
                        $q->where('order_no', $order_no)->orWhere('origin_no', $origin_no);
                    })
                    ->get()->count() > 0;
            }
        } else {
            $msg = '請輸入關鍵字查詢';
        }
        return view('receipt_return.create', [
            'data' => $data,
            'package_status' => ItemReceiptReturn::package_status,
            'msg' => $msg,
            'need_check' => $need_check
        ]);
    }

    //儲存
    public function store(Request $request)
    {
        $sale_no = $request->get('sale_no');
        if (empty($sale_no)) {
            return redirect()->back()->with('status', '新增失敗。原因：輸入資料有誤');
        } else {
            $items = json_decode($request->get('items', '[]'));
            $is_flaw = collect($items)->where('bad_qty', '>', 0)->count() > 0;
            $m = ItemReceiptReturn::create([
                'sale_no' => $sale_no,
                'package_status' => $request->get('package_status'),
                'is_flaw' => $is_flaw,
                'remark' => $request->get('remark'),
            ]);
            foreach ($items as $item) {
                if (empty($item->good_qty) && empty($item->bad_qty)) {
                    continue;
                }
                $m->details()->create([
                    'item_no' => $item->item_no,
                    'item_name' => $item->item_name,
                    'qty' => $item->qty,
                    'good_qty' => $item->good_qty,
                    'bad_qty' => $item->bad_qty,
                    'lost_qty' => $item->lost_qty,
                ]);
            }
            return redirect()->route('receipt_return.create')->with('status', '新增成功');
        }
    }

    //結案
    public function update(Request $request)
    {
        $ids =  json_decode($request->get('ids'), '[]');
        $now = new DateTime();
        ItemReceiptReturn::whereIn('id', $ids)->update(['completed_at' => $now]);
        return redirect()->route('receipt_return.index')
            ->with('status', '結案成功');
    }

    //刪除
    public function destroy(Request $request)
    {
        $ids =  json_decode($request->get('ids'), '[]');
        ItemReceiptReturn::whereIn('id', $ids)->delete();
        return redirect()->route('receipt_return.index')
            ->with('status', '刪除成功');
    }
}
