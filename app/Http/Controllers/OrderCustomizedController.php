<?php

namespace App\Http\Controllers;

use App\Models\TMS\Order;
use App\Exports\BaseExport;
use Illuminate\Http\Request;
use App\Models\yherp\QuotationM;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Repositories\BaseRepository;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\yherp\OrderCustomizedM;
use App\Models\yherp\CustomerComplaint;
use Illuminate\Support\Facades\Validator;

class OrderCustomizedController extends Controller
{
    private const OrderKeyword = '客製化訂單#';
    private $baseRepository = null;

    public function __construct(BaseRepository $baseRepository)
    {
        $this->baseRepository = $baseRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = OrderCustomizedM::with('Details')->where('completed', 0)->get();
        $ids = [];
        foreach ($data->pluck('id')->toArray() as $id) {
            $ids[] = self::OrderKeyword . $id;
        }
        $completed_order = Order::select('OBAK3')->where('OCANC', '!=', '訂單取消')->whereIn('OBAK3', $ids)->pluck('OBAK3')->toArray();
        $customers = $this->baseRepository->GetCustomer();
        $salesmans = $this->baseRepository->GetSalesman();
        foreach ($data as $key => $d) {
            if (in_array(self::OrderKeyword . $d->id, $completed_order)) {
                $d->update(['completed' => 1]);
                $data->forget($key);
            } else {
                $d->customer_text = $customers->firstWhere('CCODE', $d->customer)->CNAM2 ?? '';
                $d->salesman_text = $salesmans->firstWhere('SCODE', $d->salesman)->SNAME ?? '';
            }
        }
        return view('order_customized.index', ['data' => $data]);
    }

    //建立
    public function create()
    {
        $items = $this->baseRepository->GetItemAll();
        $customers = $this->baseRepository->GetCustomer();
        $salesmans = $this->baseRepository->GetSalesman();

        return view('order_customized.create', [
            'customers' => $customers,
            'salesmans' => $salesmans,
            'items' => $items,
        ]);
    }

    //從客訴單建立
    public function create_by_cc($id)
    {
        $items = $this->baseRepository->GetItemAll();
        $customers = $this->baseRepository->GetCustomer();
        $salesmans = $this->baseRepository->GetSalesman();
        $origin_no = '';
        $remark = '';
        $cc_remark = '';
        $details = null;
        if (!empty($id)) {
            $data = CustomerComplaint::with(['Order:OCOD1', 'Order.OrderDetail:OCOD1,OICOD,OINAM,OINUM,OUNIT,OSOUR', 'Order.OrderDetail.Item:ICODE,ITCOD,ICONS'])
                ->where('id', $id)
                ->first();
            $origin_no = $data->origin_no;
            $remark = mb_substr($data->remark, 0, 200, 'utf-8'); //擷取200字
            $cc_remark = $data->remark;
            foreach ($data->Order->OrderDetail as $item) {
                $kind1 = $item->ITEM->ITCOD;
                $qty = intval($item->qty);
                $unit = round($item->unit, 2);
                if ($kind1 == '98' || $unit < 0 || $qty <= 0) {
                    continue;
                }
                $cost = round($item->OSOUR / $qty, 2);
                $details[] = [
                    'item_no' => $item->item_no,
                    'item_name' => $item->item_name,
                    'qty' => $qty,
                    'unit' => $unit,
                    'cost' => $cost,
                ];
            }
        }

        return view('order_customized.create', [
            'customers' => $customers,
            'salesmans' => $salesmans,
            'items' => $items,
            'origin_no' => $origin_no,
            'remark' => $remark,
            'cc_remark' => $cc_remark,
            'details' => $details,
        ]);
    }

    //從報價單建立
    public function create_by_quotation($id)
    {
        $items = $this->baseRepository->GetItemAll();
        $customers = $this->baseRepository->GetCustomer();
        $salesmans = $this->baseRepository->GetSalesman();
        $origin_no = '';
        $remark = '';
        $reamrk1 = '';
        $details = null;
        if (!empty($id)) {
            $m = QuotationM::with(['Details', 'Details.Item:ICODE,ISOUR'])->where('id', $id)->first();
            $origin_no = $m->quote_no;
            $remark = $m->remark;
            if (!empty($m->contact_name)) { //聯絡人
                $reamrk1 .= "聯絡人:{$m->contact_name}/";
            }
            if (!empty($m->contact_phone)) { //聯絡人電話
                $reamrk1 .= "客戶電話:{$m->contact_phone}/";
            }
            foreach ($m->Details as $item) {
                $cost = number_format($item->Item->cost, 2);
                $details[] = [
                    'item_no' => $item->item_no,
                    'item_name' => $item->item_name,
                    'qty' => $item->item_qty,
                    'unit' => $item->item_price,
                    'cost' => $cost,
                    'remark' => $item->item_remark,
                ];
            }
        }

        return view('order_customized.create', [
            'customers' => $customers,
            'salesmans' => $salesmans,
            'items' => $items,
            'origin_no' => $origin_no,
            'remark' => $remark,
            'reamrk1' => $reamrk1,
            'details' => $details,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //主檔驗證
        $this->validate($request, [
            'order_date' => ['required', 'date', 'max:11'],
            'customer' => ['required', 'max:16'],
            'salesman' => ['required', 'max:4'],
            'order_no' => 'max:40',
            'remark' => 'max:200',
            'remark2' => 'max:200',
            'details' => 'required',
        ]);
        //明細檔驗證
        $items = json_decode($request->get('details'), true);
        $rules = [
            '*.item_no' => ['required', 'max:16'],
            '*.item_qty' => ['required', 'numeric', 'integer', 'digits_between:0,65535'],
            '*.item_price' => ['required', 'numeric'],
            '*.item_remark' => 'max:100',
        ];
        $validator = Validator::make($items, $rules);
        if ($validator->fails()) {
            return redirect()->route('order_customized.create')
                ->withErrors($validator)
                ->withInput();
        }
        //儲存
        $m = OrderCustomizedM::create($request->all());
        foreach ($items as $item) {
            $m->Details()->create($item);
        }

        return redirect()->route('order_customized.index')
            ->with('status', '訂單新增成功');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $items = $this->baseRepository->GetItemAll();
        $customers = $this->baseRepository->GetCustomer();
        $salesmans = $this->baseRepository->GetSalesman();
        $data = OrderCustomizedM::with('Details')->find($id);
        foreach ($data->Details as $item) {
            $item_db = $items->firstWhere('ICODE', $item->item_no);
            $item->item_name = $item_db->INAME ?? '';
            $item->item_cost = round($item_db->ISOUR, 2) ?? '';
        }
        return view('order_customized.edit', compact('data', 'items', 'customers', 'salesmans'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //主檔驗證
        $this->validate($request, [
            'order_date' => ['required', 'date', 'max:11'],
            'customer' => ['required', 'max:16'],
            'salesman' => ['required', 'max:4'],
            'order_no' => 'max:40',
            'remark' => 'max:200',
            'remark2' => 'max:200',
            'details' => 'required',
        ]);
        //明細檔驗證
        $items = json_decode($request->get('details'), true);
        $rules = [
            '*.item_no' => ['required', 'max:16'],
            '*.item_qty' => ['required', 'numeric', 'integer', 'digits_between:0,65535'],
            '*.item_price' => ['required', 'numeric'],
            '*.item_remark' => 'max:100',
        ];
        $validator = Validator::make($items, $rules);
        if ($validator->fails()) {
            return redirect()->route('order_customized.edit')
                ->withErrors($validator)
                ->withInput();
        }
        //主檔更新
        $m = OrderCustomizedM::find($id);
        $m->update($request->all());
        //明細檔
        $d = json_decode($request->get('details'), true);
        $ids = collect($d)->where('id', '!=', '')->pluck('id')->toArray(); //原資料ID
        if (empty($ids)) { //編輯後，無舊資料，刪除全部
            $m->Details()->delete();
        } else { //編輯後，有舊資料，刪除不在編輯過後的舅資料
            $m->Details()->whereNotIn('id', $ids)->delete();
        }
        foreach ($d as $item) {
            $id = $item['id'];
            if (empty($id)) { //無id，新增
                $m->Details()->create($item);
            } else { //有id，更新
                $m->Details()->find($id)->update($item);
            }
        }

        return redirect()->route('order_customized.index')
            ->with('status', '訂單編輯成功');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $m = OrderCustomizedM::find($id);
        $m->Details()->delete();
        $m->delete();

        return redirect()->route('order_customized.index')
            ->with('status', '訂單刪除成功');
    }

    /**
     * 訂單匯出
     *
     * @param [type] $id 流水號
     * @return void
     */
    public function Export($id)
    {
        $exportData[] = [
            '訂單日期', '客戶代號', '業務員代號', '客戶訂單單號', '送貨方式', '商品代號', '產品數量', '倉庫代號', '產品售價', '產品備註', '備註1', '備註2', '備註3'
        ];
        $data = OrderCustomizedM::with('Details')->find($id);
        if (!empty($data)) {
            $exportName = '客製化訂單_' . $data->id;
            $isRepost = false; //是否為補寄貨
            foreach ($data->Details as $item) {
                if (in_array($data->customer, ['C005', 'C006', 'C010'])) { //補寄貨、補寄貨來回件、保固
                    $isRepost = true;
                }
                $exportData[] = [
                    $data->order_date,
                    $data->customer,
                    $data->salesman,
                    $data->order_no,
                    '',
                    $item->item_no,
                    $item->item_qty,
                    '',
                    round($item->item_price / 1.05), //因TMS會自動轉成含稅(X1.05)，匯入價格需改成未稅
                    $item->item_remark,
                    $data->remark,
                    $data->remark2,
                    self::OrderKeyword . $data->id,
                ];
            }
            //補寄貨新增折扣項目
            if (count($exportData) > 0 && $isRepost) {
                $copy = end($exportData);
                $copy[5] = 'XX126'; //商品代號
                $copy[6] = '1'; //產品數量
                $copy[7] = ''; //倉庫代號
                $copy[8] = '0'; //產品售價
                $exportData[] = $copy;
            }
            return Excel::download(new BaseExport($exportData), $exportName . '.' . \Maatwebsite\Excel\Excel::XLS);
        } else {
            return redirect()->route('order_customized.index')->with('status', '訂單匯出失敗');
        }
    }

    /**
     * 訂單完成
     *
     * @param [type] $id PK
     * @return \Illuminate\Http\Response
     */
    public function complete($id)
    {
        OrderCustomizedM::find($id)->update(['completed' => '1']);
        return redirect()->route('order_customized.index');
    }
}
