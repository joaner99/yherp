<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\BaseService;
use App\Models\yherp\ItemMarketingM;
use App\Services\ConfigService;
use App\Http\Controllers\Controller;

class ItemMarketingController extends Controller
{
    private $baseService = null;
    private $configService = null;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct(BaseService $baseService, ConfigService $configService)
    {
        $this->baseService = $baseService;
        $this->configService = $configService;
    }

    public function index()
    {
        $data = ItemMarketingM::with('Details')->get();
        $config = $this->configService->GetSysConfig('marketing');
        foreach ($data as $d) {
            $d->marketing_name = $config->firstWhere('value1', $d->marketing)->value2 ?? '';
        }
        return view('item_marketing.index', compact('data'));
    }

    public function create()
    {
        $items = $this->baseService->GetItemAllList();
        $config = $this->configService->GetSysConfig('marketing');
        return view('item_marketing.create', compact('items', 'config'));
    }

    public function store(Request $request)
    {
        //驗證
        $this->validate($request, [
            'marketing' => 'required',
            'remark' => 'max:500',
            's_dt' => ['required', 'date'],
            'e_dt' => ['required', 'date'],
            'items' => 'required'
        ]);
        $main = ItemMarketingM::create($request->all());
        $items = json_decode($request->get('items'));
        foreach ($items as $item) {
            $main->Details()->create(
                [
                    'item_no' => $item->item_no,
                    'item_name' => $item->item_name
                ]
            );
        }

        return redirect()->route('item_marketing.index')
            ->with('status', '新增成功');
    }

    //編輯
    function edit($id)
    {
        $items = $this->baseService->GetItemAllList();
        $config = $this->configService->GetSysConfig('marketing');
        $data = ItemMarketingM::with('Details')->find($id);
        if (empty($data)) {
            return redirect()->route('item_marketing.index')
                ->with('status', '編輯失敗，找不到資料');
        } else {
            return view('item_marketing.edit', compact('items', 'config', 'data'));
        }
    }

    //更新
    public function update(Request $request, $id)
    {
        //驗證
        $this->validate($request, [
            'marketing' => 'required',
            'remark' => 'max:500',
            's_dt' => ['required', 'date'],
            'e_dt' => ['required', 'date'],
            'items' => 'required'
        ]);
        //主檔
        $main = ItemMarketingM::find($id);
        $main->update($request->all());

        //料號
        $items = json_decode($request->get('items'));
        $ids = collect($items)->where('id', '!=', '')->pluck('id')->toArray(); //原資料ID
        if (empty($ids)) { //編輯後，無舊資料，刪除全部
            $main->Details()->delete();
        } else { //編輯後，有舊資料，刪除不在編輯過後的舅資料
            $main->Details()->whereNotIn('id', $ids)->delete();
        }
        if (!empty($items)) {
            foreach ($items as $detail) {
                $id = $detail->id;
                if (empty($id)) { //無id，新增
                    $main->Details()->create([
                        'item_no' => $detail->item_no,
                        'item_name' => $detail->item_name,
                    ]);
                } else { //有id，更新
                    $main->Details()->find($id)->update([
                        'item_no' => $detail->item_no,
                        'item_name' => $detail->item_name,
                    ]);
                }
            }
        }
        return redirect()->route('item_marketing.index')
            ->with('status', '編輯成功');
    }

    //刪除
    function destroy($id)
    {
        $m = ItemMarketingM::find($id);
        $m->Details()->delete();
        $m->delete();
        return redirect()->route('item_marketing.index')
            ->with('status', '刪除成功');
    }
}
