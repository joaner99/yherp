<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\yherp\OrderConfirmM;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Services\OrderConfirmService;

class OrderConfirmController extends Controller
{
    private $orderConfirmService = null;

    public function __construct(OrderConfirmService $orderConfirmService)
    {
        $this->orderConfirmService = $orderConfirmService;
    }

    public function index()
    {
        $data = $this->orderConfirmService->GetIndexVD();
        return view('order_confirm.index', $data);
    }

    public function store(Request $request)
    {
        $order_type = $request->get('order_type', 0);
        $order_no =  $request->get('order_no');
        if (empty($order_type)) {
            return redirect()->back()->with('status', '確認失敗');
        }
        //應採購未採購，可以不須明細
        if ($order_type != 12 && (empty($order_no) || count($order_no) == 0)) {
            return redirect()->back()->with('status', '確認失敗，無任何細項');
        }
        $m = OrderConfirmM::create(['user_id' => Auth::user()->id, 'order_type' => $order_type]);
        if (!empty($order_no) && count($order_no) > 0) {
            foreach ($order_no as $o) {
                $m->Details()->create(['order_no' => $o]);
            }
        }
        return redirect()->back()->with('status', '確認成功');
    }
}
