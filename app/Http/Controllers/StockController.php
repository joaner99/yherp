<?php

namespace App\Http\Controllers;

use DateTime;
use Carbon\Carbon;
use App\Exports\BaseExport;
use Illuminate\Http\Request;
use App\Services\StockService;
use App\Services\ConfigService;
use App\Models\yherp\ItemPlaceD;
use App\Models\yherp\ItemPlaceM;
use Illuminate\Support\Facades\DB;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\ChinaSafetyStockExport;

class StockController extends Controller
{
    private $stockService;

    public function __construct(StockService $stockService)
    {
        $this->stockService = $stockService;
    }
    //外掛倉儲
    public function index(Request $request)
    {
        $request->flash();
        $data = $this->stockService->GetStockPlace($request);
        return view('stock.index', $data);
    }
    //商品安庫表
    public function safety_stock()
    {
        $data = $this->stockService->GetUnsafetyStock();
        return view('stock.safety_stock', [
            'china_data' => $data->where('country', 1),
            'unchina_data' => $data->where('country', '!=', 1)
        ]);
    }
    //倉庫異動紀錄
    public function log(Request $request)
    {
        $request->flash();
        $data = $this->stockService->GetStockLog($request);
        return view('stock.log', $data);
    }
    //手機畫面-層架選擇
    public function create_index($s_id, $shelf)
    {
        $place = ItemPlaceM::with('Stock')->where('s_id', $s_id)->where('shelf', $shelf)->get();
        return view('stock.create_index', ['place' => $place]);
    }
    //倉庫異動畫面
    public function create($s_id, $p_id, Request $request)
    {
        $company = $request->get('company');
        $database = $this->stockService->GetDatabase($company);
        $item_all = $this->stockService->GetItemAllList();
        $all_place = ItemPlaceM::from($database . '.item_place_m')->select('id', 'name')->where('id', '!=', $p_id)->where('s_id', $s_id)->get();
        $place = ItemPlaceM::from($database . '.item_place_m')
            ->with([
                'Details' => function ($q) use ($database) {
                    $q->from($database . '.item_place_d');
                },
                'Stock' => function ($q) use ($database) {
                    $q->from($database . '.stock');
                }
            ])->where('id', $p_id)->where('s_id', $s_id)->get()->first();
        $exists_items = $place->Details->pluck('item_no')->toArray();
        $item_all = $item_all->filter(function ($item) use ($exists_items) {
            return !in_array($item->ICODE, $exists_items);
        });
        $transfer_ips = (new ConfigService(new BaseRepository()))->GetSysConfig('transfer_ip')->pluck('value1')->toArray();
        $is_transfer = in_array($request->ip(), $transfer_ips);
        //篩選可作業類型
        $log_type = StockService::log_type;
        foreach ($log_type as $key => $val) {
            switch ($key) {
                case 1: //入庫，當儲位內無任何商品時，禁止
                    if (count($place->Details) == 0) {
                        unset($log_type[$key]);
                    }
                    break;
                case 2: //出庫，當儲位內無任何商品時或非總公司，禁止
                    if (count($place->Details) == 0 /*|| !Auth::user()->hasRole('YoHouse')*/) {
                        unset($log_type[$key]);
                    }
                    break;
                case 3: //首次入庫
                    break;
            }
        }

        return view('stock.create', [
            's_id' => $s_id,
            'p_id' => $p_id,
            'place' => $place,
            'all_place' => $all_place,
            'log_type' => $log_type,
            'item_all' => $item_all,
            'is_transfer' => $is_transfer,
            'company' => $company
        ]);
    }
    //倉庫異動儲存
    public function store($s_id, $p_id, Request $request)
    {
        $company = $request->get('company');
        $database = $this->stockService->GetDatabase($company);
        $connection = $this->stockService->GetConnection($company);
        $type = $request->get('type');
        $items = json_decode($request->get('items'));
        if (empty($type) || (!in_array($type, [98, 99]) && !array_key_exists($type, StockService::log_type)) || empty($items) || count($items) == 0) {
            return redirect()->back()->with('status', '倉庫異動失敗');
        } else {
            $msg = '';
            DB::connection($connection)->beginTransaction();
            if ($type == 99) { //移轉作業
                $log_kind = 1;
                $msg = $this->stockService->StockModify($connection, $database, $s_id, $p_id, $items, 2, $log_kind);
                if (!empty($msg)) {
                    DB::connection($connection)->rollBack();
                    return redirect()->route('stock.index')->with('status', '1.倉庫出庫異動失敗，原因：' . $msg);
                }
                $in_p_id =  $request->get('place');
                $exists_items_list = ItemPlaceD::from($database . '.item_place_d')->select('item_no')->where('m_id', $in_p_id)->pluck('item_no')->toArray();
                //區分首次入庫的項目
                $first_items = [];
                $exists_items = [];
                foreach ($items as $item) {
                    if (in_array($item->no, $exists_items_list)) { //入庫
                        $exists_items[] = $item;
                    } else { //首次入庫
                        $first_items[] = $item;
                    }
                }
                $msg = $this->stockService->StockModify($connection, $database, $s_id, $in_p_id, $exists_items, 1, $log_kind);
                if (!empty($msg)) {
                    DB::connection($connection)->rollBack();
                    return redirect()->route('stock.index')->with('status', '2.倉庫入庫異動失敗，原因：' . $msg);
                }
                $msg = $this->stockService->StockModify($connection, $database, $s_id, $in_p_id, $first_items, 3, $log_kind);
                if (!empty($msg)) {
                    DB::connection($connection)->rollBack();
                    return redirect()->route('stock.index')->with('status', '倉庫首次入庫異動失敗，原因：' . $msg);
                }
            } else if ($type == 98) { //調撥作業
                $msg = $this->stockService->StockModify($connection, $database, $s_id, $p_id, $items, 2, 2);
                if (!empty($msg)) {
                    DB::connection($connection)->rollBack();
                    return redirect()->route('stock.index')->with('status', '調撥作業倉庫出庫異動失敗，原因：' . $msg);
                }
            } else { //入庫、出庫、首次入庫
                $msg = $this->stockService->StockModify($connection, $database, $s_id, $p_id, $items, $type);
                if (!empty($msg)) {
                    DB::connection($connection)->rollBack();
                    return redirect()->route('stock.index')->with('status', '倉庫異動失敗，原因：' . $msg);
                }
            }
            DB::connection($connection)->commit();
            return redirect()->route('stock.index')->with('status', '倉庫異動成功');
        }
    }
    //調撥總表
    public function transfer()
    {
        $data = $this->stockService->GetStockTransfer();
        $unchinaPurchaseExport = $this->stockService->GetStockPurchaseExport($data, false);
        $chinaPurchaseExport = $this->stockService->GetStockPurchaseExport($data, true);
        return view('stock.transfer', compact('data', 'unchinaPurchaseExport', 'chinaPurchaseExport'));
    }

    //匯出
    public function export($s_id)
    {
        $db = ItemPlaceD::select('item_no', 'item_name', 'qty', 'm_id')
            ->whereHas('Main', function ($q) use ($s_id) {
                $q->where('s_id', $s_id);
            })
            ->with(['STOC' => function ($q) use ($s_id) {
                $q->select('SITEM', 'SNUMB')->where('STCOD', $s_id);
            }])
            ->orderBy('item_no')
            ->get();
        $result[] = [
            '產品代號',
            '產品名稱',
            '倉庫代號',
            'TMS庫存',
            '外掛庫存',
            '儲位'
        ];
        foreach ($db as $d) {
            $result[] = [
                'item_no' => $d->item_no,
                'item_name' => $d->item_name,
                'stock' => $s_id,
                'tms_qty' => $d->STOC->sum('SNUMB'),
                'qty' => $d->qty,
                'place' => $d->m_id,
            ];
        }
        return Excel::download(new BaseExport($result), Carbon::now()->format('Ymd_His') . '盤點' . '.' . \Maatwebsite\Excel\Excel::XLS);
    }

    //康品安庫匯出
    public function china_safety_stock_export(Request $request)
    {
        $items = json_decode($request->get('items', '[]'));
        $exportName = (new DateTime())->format('Y-m-d') . '中國商品安庫表';
        if (empty($items) || count($items) == 0) {
            return redirect()->back();
        } else {
            return Excel::download(new ChinaSafetyStockExport($items), str_replace(['/', '$', ' '], '', $exportName) . '.' . \Maatwebsite\Excel\Excel::XLSX);
        }
    }
}
