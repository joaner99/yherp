<?php

namespace App\Http\Controllers;

use App\Exports\BaseExport;
use Illuminate\Http\Request;
use App\Services\ConfigService;
use App\Models\yherp\YcsTransport;
use App\Services\TransportService;
use App\Imports\YcsTransportImport;
use App\Http\Controllers\Controller;
use App\Repositories\BaseRepository;
use Maatwebsite\Excel\Facades\Excel;

class YcsTransportController extends Controller
{
    private $transportService = null;

    public function __construct(TransportService $transportService)
    {
        $this->transportService = $transportService;
    }

    //音速託運總表
    public function index(Request $request)
    {
        $data = $this->transportService->GetTransportYcs($request);
        return view('ycs_transport.index', $data);
    }

    //編輯
    public function edit($id, Request $request)
    {
        $data = YcsTransport::with(['Posein' => function ($q) {
            $q->select('PCOD1', 'PTOTA', 'PBAK1', 'PBAK2', 'PBAK3', 'InsideNote');
        }])->find($id);
        $date = $request->get('date');
        $item_names = (new ConfigService(new BaseRepository()))->GetSysConfig('ycs_transport_item_name')->pluck('value1');
        return view('ycs_transport.edit', compact('data', 'date', 'item_names'));
    }

    //更新
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'settle_date' => ['required'],
            'qty' => ['required', 'numeric', 'integer', 'digits_between:0,255'],
            'size' => ['required', 'numeric', 'integer', 'digits_between:0,255'],
            'weight' => ['required', 'numeric', 'integer', 'digits_between:0,255'],
            'receiver' => 'required',
            'addr' => 'required',
            'tel' => 'required',
            'remark' => 'max:100'
        ]);
        YcsTransport::find($id)->update($request->all());

        return redirect()->route('ycs_transport.index')
            ->with(['status' => '編輯成功', 'date' => $request->get('date')]);
    }

    //刪除
    public function destroy(Request $request, $id)
    {
        YcsTransport::find($id)->delete();
        return redirect()->route('ycs_transport.index')
            ->with(['status' => '刪除成功', 'date' => $request->get('date')]);
    }

    //批次刪除
    public function batch_delete(Request $request)
    {
        $ids = json_decode($request->get('ids', '[]'));
        YcsTransport::whereIn('id', $ids)->delete();
        return redirect()->route('ycs_transport.index')
            ->with(['status' => '刪除成功', 'date' => $request->get('date')]);
    }

    //音速物流匯入
    public function import(Request $request)
    {
        $msg = '';
        $settle_date = $request->get('settle_date');
        if (!empty($settle_date) && $request->hasFile('file') && $request->file('file')->isValid()) {
            $file = $request->file('file');
            $import = new YcsTransportImport($settle_date);
            Excel::import($import, $file);
            $msg = $import->msg;
        } else {
            $msg = '上傳失敗';
        }
        if (empty($msg)) {
            return redirect()->back()->with('status', '匯入成功');
        } else {
            return redirect()->back()->with('status', $msg);
        }
    }

    //匯出TMS格式
    public function export_tms(Request $request)
    {
        $exportData = $this->transportService->GetYcsTmsExport($request);
        if (empty($exportData)) {
            return redirect()->back()->with('status', '匯出失敗，找不到匯出資料');
        } else {
            return Excel::download(new BaseExport($exportData),  str_replace(['/', '$', ' '], '',  'TMS託運單號匯入') . '.' . \Maatwebsite\Excel\Excel::XLS);
        }
    }

    //api上傳
    public function api(Request $request)
    {
        $msg = $this->transportService->YCSApi($request);
        return redirect()->back()->with(['date' => $request->get('date'), 'status' => empty($msg) ? '上傳成功' : $msg]);
    }
}
