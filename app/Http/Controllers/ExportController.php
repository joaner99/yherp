<?php

namespace App\Http\Controllers;

use App\Exports\BaseExport;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ExportController extends Controller
{
    //匯出Excel
    public function table(Request $request)
    {
        $exportData = json_decode($request->export_data, true);
        $exportName = $request->export_name;
        if (empty($exportData) || empty($exportName)) {
            return redirect()->back();
        } else {
            return Excel::download(new BaseExport($exportData), str_replace(['/', '$', ' '], '', $exportName) . '.' . \Maatwebsite\Excel\Excel::XLSX);
        }
    }
}
