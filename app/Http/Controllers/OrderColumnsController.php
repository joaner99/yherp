<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\yherp\OrderSampleM;
use App\Imports\OrderColumnsImport;
use App\Models\yherp\OrderColumnIdx;
use App\Services\OrderSampleService;
use Maatwebsite\Excel\Facades\Excel;

class OrderColumnsController extends Controller
{
    //取得排程紀錄
    public function index()
    {
        $latest_shopline_header = json_decode(OrderSampleM::where('src', 1)->orderByDesc('updated_at')->first()->header ?? '');
        $latest_shopee_header = json_decode(OrderSampleM::whereIn('src', [2, 3, 4])->orderByDesc('updated_at')->first()->header ?? '');
        $latest_bvshop_header = json_decode(OrderSampleM::where('src', 5)->orderByDesc('updated_at')->first()->header ?? '');
        $shopline_columns = (new OrderSampleService())->GetOrderSampleColumns($latest_shopline_header, 1, true);
        $shopee_columns = (new OrderSampleService())->GetOrderSampleColumns($latest_shopee_header, 2, true);
        $bvshop_columns = (new OrderSampleService())->GetOrderSampleColumns($latest_bvshop_header, 5, true);
        $db = OrderColumnIdx::all()->map(function ($item, $idx) use ($shopline_columns, $shopee_columns, $bvshop_columns) {
            if (!empty($shopee_columns) && key_exists($item->key, $shopee_columns)) {
                $item->shopee_status = $shopee_columns[$item->key];
            }
            if (!empty($shopline_columns) && key_exists($item->key, $shopline_columns)) {
                $item->shopline_status = $shopline_columns[$item->key];
            }
            if (!empty($bvshop_columns) && key_exists($item->key, $bvshop_columns)) {
                $item->bvshop_status = $bvshop_columns[$item->key];
            }
            return $item;
        });
        return view('order_columns.index', ['data' => $db]);
    }

    public function update(Request $request)
    {
        $msg = '';
        $type = $request->get('type');
        $file = $request->file('file');
        if ($file->isValid()) {
            $import = new OrderColumnsImport($type);
            Excel::import($import, $file);
            $msg .= $import->msg;
        } else {
            $msg .= '檔案【' . $file->getClientOriginalName() . '】未通過驗證';
        }
        if (empty($msg)) {
            return redirect()->back()->with('status', '更新成功');
        } else {
            return redirect()->back()->with('status', $msg);
        }
    }

    public function update_idx(Request $request)
    {
        $data = $request->get('data');
        foreach ($data as $id => $d) {
            $obj = OrderColumnIdx::find($id);
            $obj->update($d);
            $obj->save();
        }
        return redirect()->back()->with('status', '更新成功');
    }
}
