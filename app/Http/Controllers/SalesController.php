<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Services\SaleService;
use App\Imports\ShopeeOrderImport;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;

class SalesController extends Controller
{
    private $saleService = null;

    public function __construct(SaleService $saleService)
    {
        $this->saleService = $saleService;
    }

    //銷貨日報表
    public function report(Request $request)
    {
        if (!Auth::user()->can('sales_report')) {
            return view('index');
        }
        $request->flash();
        $data = $this->saleService->GetReport($request);
        $last_start_date = $request->get('last_start_date', Carbon::today()->addMonth(-1)->format('Y-m-d'));
        $last_end_date = $request->get('last_end_date', Carbon::today()->addMonth(-1)->format('Y-m-d'));
        $fake_request = new Request();
        $fake_request->replace(
            [
                'start_date' => $last_start_date,
                'end_date' => $last_end_date,
            ]
        );
        $last_data = $this->saleService->GetReport($fake_request);
        $data['last_total'] = $last_data['total_include_unsale'];
        $data['last_start_date'] = $last_start_date;
        $data['last_end_date'] = $last_end_date;
        if ($last_data['total_include_unsale'] > 0) {
            $data['grow'] = ($data['total_include_unsale'] / $last_data['total_include_unsale'] - 1) * 100;
        }

        return view('sales.report', $data);
    }

    //庫存周轉率
    public function performance(Request $request)
    {
        if (!Auth::user()->can('sales_performance')) {
            return view('index');
        }
        $request->flash();
        $data = $this->saleService->GetSalesPerformance($request);

        return view('sales.performance', $data);
    }

    //平台銷售
    public function cust_item(Request $request)
    {
        $request->flash();
        $data = $this->saleService->GetCustItem($request);
        return view('sales.cust_item', $data);
    }

    //商品大類年度銷售
    public function item_kind_sales(Request $request)
    {
        $request->flash();
        $data = $this->saleService->GetItemKindSales($request);
        return view('sales.item_kind_sales', $data);
    }

    //商品大類年度銷售
    public function item_kind3_sales(Request $request)
    {
        $request->flash();
        $data = $this->saleService->GetItemKind3Sales($request);
        return view('sales.item_kind3_sales', $data);
    }

    //商品類別銷售
    public function item_sales(Request $request)
    {
        $request->flash();
        $data = $this->saleService->GetItemSales($request);
        return view('sales.item_sales', $data);
    }

    //人工地板銷貨統計
    public function floor(Request $request)
    {
        $request->flash();
        $data = $this->saleService->GetFloor($request);
        return view('sales.floor', $data);
    }

    //蝦皮出貨分析
    public function shopee_trans(Request $request)
    {
        $data = $request->session()->get('sales.shopee_data', []);
        $avg_order_to_ship = round(collect($data)->avg('order_to_ship'));
        $avg_pay_to_ship = round(collect($data)->avg('pay_to_ship'));
        return view('sales.shopee_trans', ['data' => $data, 'avg_order_to_ship' => $avg_order_to_ship, 'avg_pay_to_ship' => $avg_pay_to_ship]);
    }

    //蝦皮訂單匯入
    public function import(Request $request)
    {
        $msg = '';
        if ($request->hasFile('file')) {
            $files = $request->file('file');
            $data = [];
            foreach ($files as $file) {
                if ($file->isValid()) {
                    $import = new ShopeeOrderImport();
                    Excel::import($import, $file);
                    $msg .= $import->msg;
                    $data = array_merge($data, $import->data);
                } else {
                    $msg .= '檔案【' . $file->getClientOriginalName() . '】未通過驗證';
                }
            }
            $request->session()->put('sales.shopee_data', $data);
        } else {
            $msg = '未選擇任何檔案';
        }

        if (empty($msg)) {
            return redirect()->back()->with('status', '匯入成功');
        } else {
            return redirect()->back()->with('status', $msg);
        }
    }

    //蝦幣折抵查詢
    public function shopee_discount(Request $request)
    {
        $request->flash();
        $data = $this->saleService->GetShopeeDiscount($request);
        return view('sales.shopee_discount', $data);
    }
}
