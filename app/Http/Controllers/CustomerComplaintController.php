<?php

namespace App\Http\Controllers;

use Exception;
use App\Models\TMS\Order;
use App\Models\TMS\Posein;
use App\Models\TMS\OrderDetail;
use Illuminate\Http\Request;
use App\Services\BaseService;
use App\Models\yherp\CustomerComplaint;

class CustomerComplaintController extends Controller
{
    private $baseService = null;

    public function __construct(BaseService $baseService)
    {
        $this->baseService = $baseService;
    }

    //取得客訴單資料
    public function index(Request $request)
    {
        $request->flash();
        $data = $this->baseService->GetCustomerComplaint($request);

        return view('customer_complaint', $data);
    }

    //AJAX 取得訂單資訊
    public function ajax_get_order(Request $request)
    {
        try {
            $order_no = $request->get('order_no', '');
            $type = $request->get('type', '1');
            if (empty($order_no)) {
                return response()->json(['msg' => 'ERP/原始訂單編號為空'], 404);
            }
            $isExists = CustomerComplaint::where('status', '!=', '99')
                ->where(function ($query) use ($order_no) {
                    $query
                        ->where('order_no', $order_no)
                        ->orWhere('origin_no', $order_no);
                })
                ->get()->count() > 0;
            if ($isExists) {
                return response()->json(['msg' => '訂單已經建立過客訴單'], 404);
            }
            $order = Order::select('OCNAM as source', 'OBAK1 as order_remark1', 'OBAK2 as order_remark2', 'OBAK3 as order_remark3', 'ORDET.InsideNote as order_remarkInside', 'OCOD1', 'OCOD4 as origin_no', 'POSEIN.PCOD1 as sale_no', 'OCCOD', 'ODATE')
                ->leftJoin('POSEIN', 'POSEIN.PCOD2', 'ORDET.OCOD1');
            switch ($type) {
                default:
                case '1': //原始訂單編號查詢
                    $order->where('OCOD4', $order_no);
                    break;
                case '2': //ERP訂單編號查詢
                    $order->where('OCOD1', $order_no);
                    break;
                case '3': //ERP銷貨編號查詢
                    $order->where('OCOD1', Posein::where('PCOD1', $order_no)->value('PCOD2'));
                    break;
            }
            $order = $order->first();
            if (!empty($order)) {
                $order->OrderDetail = OrderDetail::select('OICOD as no', 'OINAM as name', 'OINUM as amount', 'OTOTA as total')
                    ->where('OCOD1', $order->order_no)
                    ->orderBy('OICOD')
                    ->get();
            }

            return response()->json($order);
        } catch (Exception $ex) {
            return response()->json(['msg' => '取得訂單資料時，發生無法預期的錯誤。原因：' . $ex->getMessage()], 404);
        }
    }

    //新增客訴單
    public function create(Request $request)
    {
        try {
            $origin_no = $request->get('origin_no', '');
            $remark = $request->get('remark', '');

            $customer_complaint = CustomerComplaint::firstOrCreate([
                'order_no' => $request->get('order_no', ''),
                'origin_no' => $origin_no,
                'status' => $request->get('status', ''),
                'remark' => $remark,
                'mark' => join(',', $request->get('marks', [])),
                'urgent' => $request->get('urgent', '0')
            ]);
            if (!empty($customer_complaint)) {
                if ($request->get('create_customer', false)) {
                    return redirect()->action('OrderCustomizedController@create', ['cc_id' => $customer_complaint->id]);
                } else {
                    return redirect()->back()->with('status', '新增成功');
                }
            } else {
                return redirect()->back()->with('status', '新增失敗');
            }
        } catch (\Exception  $ex) {
            return redirect()->back()->with('status', "新增失敗，原因：{$ex->getMessage()}");
        }
    }

    //編輯客訴單
    public function update(Request $request)
    {
        try {
            $customer_complaint = CustomerComplaint::find($request->get('id', ''));
            if (empty($customer_complaint)) {
                return redirect()->back()->with('status', '編輯失敗');
            } else {
                $customer_complaint->status = $request->get('status', '');
                $customer_complaint->remark = $request->get('remark', '');
                $customer_complaint->mark = join(',', $request->get('marks', []));
                $customer_complaint->urgent = $request->get('urgent', '0');
                if ($customer_complaint->save()) {
                    return redirect()->back()->with('status', '編輯成功');
                } else {
                    return redirect()->back()->with('status', '編輯失敗');
                }
            }
        } catch (\Exception  $ex) {
            return redirect()->back()->with('status', "編輯失敗，原因：{$ex->getMessage()}");
        }
    }

    //批次修改狀態
    public function update_status(Request $request)
    {
        try {
            $ids = json_decode($request->get('ids', []));
            $status = $request->get('status');
            if (empty($ids) || count($ids) == 0) {
                return redirect()->back()->with('status', '批次修改狀態失敗，未選取訂單');
            } elseif (!isset($status)) {
                return redirect()->back()->with('status', '批次修改狀態失敗，未選取狀態');
            } else {
                $r = CustomerComplaint::whereIn('id', $ids)->update(['status' => $status]);
                return redirect()->back()->with('status', "批次修改狀態成功，已更新{$r}筆");
            }
        } catch (\Exception  $ex) {
            return redirect()->back()->with('status', "批次修改狀態失敗，原因：{$ex->getMessage()}");
        }
    }
}
