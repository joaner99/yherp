<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\yherp\CinForest;
use App\Services\CinForestService;

class CinForestController extends Controller
{
    private $cinforestService = null;

    function __construct(CinForestService $cinforestService)
    {
        $this->cinforestService = $cinforestService;
    }

    public function index()
    {
        $data = $this->cinforestService->GetOrder();
        return view('cinforest.index', $data);
    }

    public function update(Request $request)
    {
        CinForest::updateOrCreate(
            ['PCOD1' => $request->get('sale_no')],
            ['ConsignTran' => $request->get('tran_no')]
        );
        return redirect()->route('cinforest.index')->with('status', '更新成功');
    }
}
