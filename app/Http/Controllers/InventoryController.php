<?php

namespace App\Http\Controllers;

use DateTime;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Services\BaseService;
use App\Exports\InventoryExport;
use App\Models\yherp\InventoryD;
use App\Models\yherp\InventoryM;
use Maatwebsite\Excel\Facades\Excel;

class InventoryController extends Controller
{
    private $baseService = null;

    function __construct(BaseService $baseService)
    {
        $this->baseService = $baseService;
    }

    public function index(Request $request)
    {
        $request->flash();
        $start_date = $request->get('start_date', Carbon::today()->format('Y-m-d'));
        $end_date = $request->get('end_date', Carbon::today()->format('Y-m-d'));
        $include_complete = $request->get('include_complete');
        $data = InventoryM::with('Details')
            ->whereBetween('appointment_date', [$start_date, $end_date]);
        if (!$include_complete) {
            $data->whereNull('completed_at');
        }
        $data = $data->get();
        return view('inventory.index', ['data' => $data]);
    }

    public function create()
    {
        $items = $this->baseService->GetItemAllList();
        return view('inventory.create', ['items' => $items]);
    }

    public function store(Request $request)
    {
        //驗證
        $this->validate($request, [
            'appointment_date' => ['required', 'date'],
            'items' => 'required',
            'remark' => 'max:50'
        ]);
        $items = json_decode($request->get('items', '[]'));
        $m = InventoryM::create([
            'appointment_date' => $request->get('appointment_date'),
            'remark' => $request->get('remark')
        ]);
        foreach ($items as $item) {
            $m->Details()->create([
                'item_no' => $item->item_no,
                'item_name' => $item->item_name,
            ]);
        }
        return redirect()->route('inventory.index')
            ->with('status', '新增成功');
    }

    public function edit($id)
    {
        $data = InventoryM::with('Details')->where('id', $id)->first();
        $item_place = $this->baseService->baseRepository->GetItemp($data->Details->pluck('item_no')->toArray());
        foreach ($data->Details as $d) {
            $d->item_place = $item_place[$d->item_no] ?? [];
        }
        return view('inventory.edit', ['data' => $data]);
    }

    public function update($id, Request $request)
    {
        //驗證
        $this->validate($request, [
            'remark' => 'max:50'
        ]);
        InventoryM::find($id)->update(['remark' => $request->get('remark')]);
        $items = $request->get('items');
        foreach ($items as $d_id => $item) {
            InventoryD::where('id', $d_id)->update([
                'main_stock' => $item['main_stock'],
                'sale_stock' => $item['sale_stock'],
            ]);
        }
        return redirect()->route('inventory.index')
            ->with('status', '編輯成功');
    }

    public function inventory_done($id)
    {
        $m = InventoryM::with(['Details', 'Details.STOC:SITEM,STCOD,SNUMB'])->where('id', $id)->first();
        $m->update(['inverntory_time' => new DateTime()]);
        foreach ($m->Details as $d) {
            $tms_main_stock = $d->Stoc->where('STCOD', 'B001')->sum('SNUMB');
            $tms_sale_stock = $d->Stoc->where('STCOD', 'A001')->sum('SNUMB');
            $d->update([
                'tms_main_stock' => $tms_main_stock,
                'tms_sale_stock' => $tms_sale_stock,
            ]);
        }
        return redirect()->route('inventory.index')
            ->with('status', '盤點完成成功');
    }

    public function complete($id)
    {
        InventoryM::find($id)->update(['completed_at' => new DateTime()]);
        return redirect()->route('inventory.index')
            ->with('status', '結案成功');
    }

    public function destroy($id)
    {
        $m = InventoryM::find($id);
        $m->Details()->delete();
        $m->delete();
        return redirect()->route('inventory.index')
            ->with('status', '刪除成功');
    }

    public function export($id)
    {
        if (empty($id)) {
            return redirect()->back();
        } else {
            return Excel::download(new InventoryExport($id), $id . '盤點匯出.' . \Maatwebsite\Excel\Excel::XLSX);
        }
    }
}
