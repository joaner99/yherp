<?php

namespace App\Http\Controllers;

use App\Models\yherp\ItemReceiptM;
use Illuminate\Http\Request;
use App\Services\ReceiptService;

class ReceiptController extends Controller
{
    private $receiptService = null;

    public function __construct(ReceiptService $receiptService)
    {
        $this->receiptService = $receiptService;
    }

    public function index()
    {
        $items = $this->receiptService->GetPurchaseNotInItems();
        return view('receipt.index', ['items' => $items]);
    }

    public function store(Request $request)
    {
        $user_id = $request->get('user_id');
        $time = $request->get('time');
        $items = $request->get('items');
        if (empty($time) || empty($items)) {
            return redirect()->route('receipt.index')->with('status', '資料錯誤無法儲存');
        } else {
            $items = json_decode($items);
            $m = ItemReceiptM::create(['user_id' => $user_id, 'created_at' => $time]);
            foreach ($items as $item) {
                $m->Details()->create([
                    'item_no' => $item->no,
                    'item_name' => $item->name,
                    'qty' => $item->qty
                ]);
            }
            return redirect()->route('receipt.index');
        }
    }

    public function ajax_get_item(Request $request)
    {
        $id = $request->get('id');
        if (empty($id)) {
            return response()->json(['msg' => '請掃描商品條碼'], 404);
        }
        $item = $this->receiptService->GetItem($id);
        if (empty($item)) {
            return response()->json(['msg' => '找不到該商品'], 404);
        } else {
            return response()->json($item);
        }
    }
}
