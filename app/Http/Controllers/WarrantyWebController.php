<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\BaseService;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\warranty_web\ItemConfig;
use App\Models\warranty_web\VideoConfigM;
use App\Imports\warranty_web\ItemConfigImport;

class WarrantyWebController extends Controller
{
    private $baseService = null;

    public function __construct(BaseService $baseService)
    {
        $this->baseService = $baseService;
    }

    //保固查詢
    public function search(Request $request)
    {
        $request->flash();
        $data = $this->baseService->GetOrderWarranty($request);
        return view('warranty_web.search', $data);
    }

    //影片設定
    public function video_config()
    {
        $data = VideoConfigM::all();
        return view('warranty_web.video_config', ['data' => $data]);
    }

    //新增影片畫面
    public function video_config_create()
    {
        return view('warranty_web.video_config_create');
    }

    //編輯影片畫面
    public function video_config_edit($id)
    {
        $data = VideoConfigM::with('Details')->find($id);
        $items = $this->baseService->GetItemAllList();
        $type = $this->baseService->GetItemType();
        return view('warranty_web.video_config_edit', compact('data', 'type', 'items'));
    }

    //更新影片
    public function video_config_update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'max:50'
        ]);
        //主檔
        $main = VideoConfigM::find($id);
        $main->update($request->all());
        //明細檔
        $details = json_decode($request->get('details'));
        $ids = collect($details)->where('id', '!=', '')->pluck('id')->toArray(); //原資料ID
        if (empty($ids)) { //編輯後，無舊資料，刪除全部
            $main->Details()->delete();
        } else { //編輯後，有舊資料，刪除不在編輯過後的舅資料
            $main->Details()->whereNotIn('id', $ids)->delete();
        }
        foreach ($details as $detail) {
            $id = $detail->id;
            if (empty($id)) { //無id，新增
                $main->Details()->create([
                    'item_no' => $detail->item_no,
                    'item_kind' => $detail->item_kind,
                    'item_kind2' => $detail->item_kind2,
                    'item_kind3' => $detail->item_kind3,
                ]);
            } else { //有id，更新
                $main->Details()->find($id)->update([
                    'item_no' => $detail->item_no,
                    'item_kind' => $detail->item_kind,
                    'item_kind2' => $detail->item_kind2,
                    'item_kind3' => $detail->item_kind3,
                ]);
            }
        }
        return redirect()->route('warranty_web.video_config')
            ->with('status', '影片編輯成功');
    }

    //新增影片
    public function video_config_store(Request $request)
    {
        $this->validate($request, [
            'id' => ['required', 'max:11', 'unique:aws.video_config_m'],
            'title' => 'max:50'
        ]);

        VideoConfigM::create($request->all());

        return redirect()->route('warranty_web.video_config')
            ->with('status', '影片新增成功');
    }

    //刪除影片
    public function video_config_destroy($id)
    {
        $main = VideoConfigM::find($id);
        $main->Details()->delete();
        $main->delete();
        return redirect()->route('warranty_web.video_config')
            ->with('status', '影片刪除成功');
    }

    //影片設定
    public function video_config_detail($id)
    {
        $data = VideoConfigM::with('Details')->get();
        return view('warranty_web.video_config', ['data' => $data]);
    }

    //保固設定
    public function item_config()
    {
        $names = $this->baseService->GetItemsName();
        $data = ItemConfig::get()->map(function ($item) use ($names) {
            $item->name = $names[$item->id] ?? '';
            return $item;
        });

        return view('warranty_web.item_config', ['data' => $data]);
    }

    //保固設定匯入
    public function item_import(Request $request)
    {
        $msg = '';
        if ($request->hasFile('file')) {
            $file = $request->file('file');
            if ($file->isValid()) {
                $import = new ItemConfigImport();
                Excel::import($import, $file);
                $msg = $import->msg;
            } else {
                $msg = $file->getClientOriginalName() . '匯入失敗';
            }
        } else {
            $msg = '匯入失敗，無選擇的檔案';
        }
        if (empty($msg)) {
            return redirect()->back()->with('status', '匯入成功');
        } else {
            return redirect()->back()->with('status', $msg);
        }
    }
}
