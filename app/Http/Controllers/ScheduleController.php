<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\ScheduleService;

class ScheduleController extends Controller
{
    private $scheduleService = null;

    public function __construct(ScheduleService $scheduleService)
    {
        $this->scheduleService = $scheduleService;
    }

    //取得排程紀錄
    public function schedule_log(Request $request)
    {
        $request->flash();
        $data = $this->scheduleService->GetLog($request);
        return view('schedule_log', $data);
    }
}
