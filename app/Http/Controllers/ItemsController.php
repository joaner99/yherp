<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use App\Services\ItemService;
use App\Models\yherp\ItemConfig;
use App\Imports\ItemConfigImport;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\yherp\InventoryTurnoverRating;

class ItemsController extends Controller
{
    private $itemService = null;

    public function __construct(ItemService $itemService)
    {
        $this->middleware('permission:item_config')->only(['config', 'config_update', 'import']);
        $this->middleware('permission:item_image')->only(['image_management', 'image_update', 'ajax_upload_image']);
        $this->itemService = $itemService;
    }

    //商品總表
    public function summary(Request $request)
    {
        $data = $this->itemService->GetItemSummary($request);
        return view('items.summary', $data);
    }

    //商品銷售量查詢
    public function sales(Request $request)
    {
        $request->flash();
        $data = $this->itemService->GetSales($request);
        return view('items.sales', $data);
    }

    //商品履歷
    public function log(Request $request)
    {
        $request->flash();
        $data = $this->itemService->GetLog($request);
        return view('items.log', $data);
    }

    //商品庫存紀錄
    public function stock_history(Request $request)
    {
        $request->flash();
        $data = $this->itemService->GetStockHistory($request);
        return view('items.stock_history', ['data' => $data]);
    }

    //商品詳細資訊
    public function detail(Request $request)
    {
        $data = $this->itemService->GetDetail($request);
        $item_all = $this->itemService->GetItemAllList();
        return view('items.detail', compact('data', 'item_all'));
    }

    //參數設定
    public function config()
    {
        $data = ItemConfig::with('Rating')->get();
        $rating_list = InventoryTurnoverRating::select('id', 'rating')->orderByDesc('maximum')->get();
        return view('items.config', ['data' => $data, 'rating_list' => $rating_list]);
    }

    //參數同步
    public function config_sync()
    {
        $this->itemService->ConfigSync();
        return redirect()->route('items.config');
    }

    //參數更新
    public function config_update(Request $request)
    {
        try {
            $data = json_decode($request->get('data'));
            foreach ($data as $d) {
                $update_data = [];
                if (isset($d->main_ss)) {
                    $update_data['main_ss'] = empty($d->main_ss) ? 0 : $d->main_ss;
                }
                if (isset($d->disable_ss)) {
                    $update_data['disable_ss'] = $d->disable_ss;
                }
                if (isset($d->disable_gpm)) {
                    $update_data['disable_gpm'] = $d->disable_gpm;
                }
                if (isset($d->box_qty)) {
                    $update_data['box_qty'] = empty($d->box_qty) ? 0 : $d->box_qty;
                }
                if (isset($d->purchase_qty)) {
                    $update_data['purchase_qty'] = empty($d->purchase_qty) ? 0 : $d->purchase_qty;
                }
                if (isset($d->rating_id)) {
                    $update_data['rating_id'] = empty($d->rating_id) ? null : $d->rating_id;
                }
                ItemConfig::find($d->id)->update($update_data);
            }
            return redirect()->back()->with('status', '更新成功，共' . count($data) . '筆');
        } catch (Exception $ex) {
            return redirect()->back()->with('status', '更新失敗，原因:' . $ex->getMessage());
        }
    }

    //參數設定匯入
    public function import(Request $request)
    {
        $msg = '';
        if ($request->hasFile('file')) {
            $file = $request->file('file');
            if ($file->isValid()) {
                $import = new ItemConfigImport();
                Excel::import($import, $file);
                $msg = $import->msg;
            } else {
                $msg = $file->getClientOriginalName() . '匯入失敗';
            }
        } else {
            $msg = '匯入失敗，無選擇的檔案';
        }
        if (empty($msg)) {
            return redirect()->back()->with('status', '匯入成功');
        } else {
            return redirect()->back()->with('status', $msg);
        }
    }

    //商品照片管理
    public function image_management(Request $request)
    {
        $request->flash();
        $data = $this->itemService->GetDetail($request);
        $item_all = $this->itemService->GetItemAllList();
        return view('items.image_management', compact('data', 'item_all'));
    }

    //更新商品照片
    public function image_update(Request $request)
    {
        $msg = $this->itemService->UpdateItemImage($request);
        if (empty($msg)) {
            return redirect()->back()->with('status', '變更成功');
        } else {
            return redirect()->back()->with('status', '變更失敗，原因：' . $msg);
        }
    }

    //商品儲位建議
    public function place(Request $request)
    {
        $data = $this->itemService->GetPlace($request);
        return view('items.place', $data);
    }

    //上傳圖片
    public function ajax_upload_image(Request $request)
    {
        $msg = '';
        if ($request->has('id') && $request->hasFile('file') && $request->file('file')->isValid()) {
            $id = $request->get('id');
            $file = $request->file('file');
            $extension = $file->extension();
            $fileIdx = $this->itemService->GetImageFileIndex($id);
            $fileName = "{$id}-{$fileIdx}.{$extension}";
            $itemPath = ItemService::ItemsPath . '/' . $id;
            $file->storeAs('public/' . $itemPath, $fileName);
            $path = $itemPath . '/' . $fileName;
            $msg = $this->itemService->CreateItemImage($id, [$path]);
        } else {
            $msg = '找不到上傳檔案';
        }
        if (empty($msg)) {
            return response()->json(['path' => $path]);
        } else {
            return response()->json(['msg' => '上傳失敗。原因：' . $msg], 404);
        }
    }

    //更新箱數
    public function update_config(Request $request)
    {
        try {
            $id = $request->get('id');
            $item_name = $request->get('item_name');
            $box_qty = $request->get('box_qty');
            ItemConfig::updateOrCreate(
                ['id' => $id],
                ['name' => $item_name, 'box_qty' => $box_qty]
            );
            return redirect()->back()->with('status', '更新成功');
        } catch (Exception $ex) {
            return redirect()->back()->with('status', '更新失敗，原因：' . $ex->getMessage());
        }
    }
}
