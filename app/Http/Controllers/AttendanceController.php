<?php

namespace App\Http\Controllers;

use App\User;
use DateTime;
use Exception;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\yherp\Attendance;

class AttendanceController extends Controller
{
    function __construct() {}

    public function index(Request $request)
    {
        $ip = $request->ip();
        $users = User::role('應出勤')->get();
        $today = Carbon::today()->format('Y-m-d');
        $db = Attendance::with('User')
            ->whereBetween('card_time', [$today . ' 00:00:00', $today . ' 23:59:59'])
            ->orderBy('card_time')
            ->get();
        $data = [];
        foreach ($users as $user) {
            $user_id = $user->id;
            $user_name = $user->name2 ?? $user->name;
            $data[$user_name] = [];
            foreach ($db->where('user_id', $user_id) as $d) {
                $data[$user_name][] = [
                    'card_time' => substr($d->card_time, 11, 5),
                    'uploaded_at' => $d->uploaded_at,
                ];
            }
        }

        return view('attendance.index', ['data' => $data]);
    }

    //打卡
    public function ajax_check_in(Request $request)
    {
        try {
            $ip = $request->ip();
            $barcode = $request->get('barcode');
            $cardTime = (new DateTime())->format('Y-m-d H:i:s');
            $result = ['time' => $cardTime, 'user' => '', 'emp_no' => '', 'msg' => ''];
            if (empty($barcode)) {
                $result['msg'] = '打卡失敗，請掃描員工條碼';
                return response()->json($result, 200);
            } else {
                $user = User::where('barcode', $barcode)->first();
                if (empty($user)) {
                    $result['msg'] = '打卡失敗，找不到該員工';
                    return response()->json($result, 200);
                } else {
                    $empNo = $user->emp_no;
                    $user_name = $user->name2 ?? $user->name;
                    $result['user'] = $user_name;
                    $result['emp_no'] = $empNo;
                    $result['msg'] = '打卡成功';
                    if (empty($empNo) || empty($cardTime)) {
                        $result['msg'] = '打卡失敗，空的員工編號/空的打卡時間';
                        return response()->json($result, 200);
                    } else {
                        $last_time = Attendance::where('user_id', $user->id)->orderByDesc('card_time')->first()->card_time ?? null;
                        if (empty($last_time)) {
                            Attendance::create([
                                'user_id' => $user->id,
                                'card_time' => $cardTime,
                            ]);
                        } else {
                            $diff = (new DateTime($cardTime))->getTimestamp() - (new DateTime($last_time))->getTimestamp();
                            $limit_s = 5;
                            if ($diff > $limit_s) { //打卡最小間隔秒數
                                Attendance::create([
                                    'user_id' => $user->id,
                                    'card_time' => $cardTime,
                                ]);
                            } else {
                                $result['msg'] = "打卡失敗，禁止{$limit_s}秒內重複打卡";
                                return response()->json($result, 200);
                            }
                        }
                    }
                    return  response()->json($result, 200);
                }
            }
        } catch (Exception $ex) {
            return response()->json(['msg' => '發生無法預期的錯誤，原因：' . $ex->getMessage()], 404);
        }
    }
}
