<?php

namespace App\Http\Controllers;

use DateTime;
use Illuminate\Http\Request;
use App\Models\yherp\Holiday;
use App\Http\Controllers\Controller;

class HolidayController extends Controller
{
    //首頁
    function index()
    {
        $data = Holiday::get();
        return view('holiday.index', ['data' => $data]);
    }

    public function update()
    {
        try {
            $curl = curl_init();
            $url = 'https://data.ntpc.gov.tw/api/datasets/308dcd75-6434-45bc-a95f-584da4fed251/json?size=10000';
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
            ));
            $response = curl_exec($curl);
            curl_close($curl);
            $result = collect(json_decode($response));
            foreach ($result->where('year', '>=', 2024)->whereNotIn('holidaycategory', ['星期六、星期日', '特定節日']) as $d) {
                $date = new DateTime($d->date);
                $year = $d->year;
                $name = $d->name;
                $isholiday = $d->isholiday == '是' ? 1 : 0;
                $holidaycategory = $d->holidaycategory;
                $description = $d->description;
                Holiday::updateOrCreate(
                    ['date' => $date],
                    [
                        'year' => $year,
                        'isholiday' => $isholiday,
                        'name' => $name,
                        'holidaycategory' => $holidaycategory,
                        'description' => $description,
                    ]
                );
            }
            return redirect()->route('holiday.index')->with('status', '更新成功');
        } catch (\Exception  $ex) {
            return redirect()->back()->with('status', "更新失敗，原因：{$ex->getMessage()}");
        }
    }
}
