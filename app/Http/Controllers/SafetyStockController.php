<?php

namespace App\Http\Controllers;

use App\User;
use App\Models\yherp\SafetyStock;
use Illuminate\Http\Request;
use App\Services\BaseService;

class SafetyStockController extends Controller
{
    private $baseService = null;

    public function __construct(BaseService $baseService)
    {
        $this->baseService = $baseService;
    }

    //商品安庫表(銷售)
    public function index(Request $request)
    {
        $request->flash();
        $data = $this->baseService->GetSafetyStockHistory($request);
        $users = User::all();
        return view('safety_stock', ['data' => $data, 'users' => $users]);
    }

    //編輯
    public function update(Request $request)
    {
        try {
            $id = $request->get('id', '');
            if (empty($id)) {
                return;
            }
            $safetyStock = SafetyStock::find($id);
            if (empty($safetyStock)) {
                return;
            }
            $safetyStock->remark = $request->get('remark', '');
            $safetyStock->save();

            return redirect()->back()->with('status', '編輯成功');
        } catch (\Exception $ex) {
            return redirect()->back()->with('status', "編輯失敗，原因：{$ex->getMessage()}");
        }
    }

    //覆核
    public function check(Request $request)
    {
        try {
            $id = $request->get('id', '');
            if (empty($id)) {
                return;
            }
            $safetyStock = SafetyStock::find($id);
            if (empty($safetyStock)) {
                return;
            }
            $safetyStock->signinger = $request->get('signinger', '');
            $safetyStock->save();

            return redirect()->back()->with('status', '覆核成功');
        } catch (\Exception $ex) {
            return redirect()->back()->with('status', "覆核失敗，原因：{$ex->getMessage()}");
        }
    }
}
