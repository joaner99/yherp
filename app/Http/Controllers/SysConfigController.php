<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Models\yherp\SysConfigM;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class SysConfigController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:sys_config')->except(['index', 'edit', 'update']);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = SysConfigM::with('Details')->paginate(10);
        return view('sys_config.index', compact('data'))
            ->with('i', ($request->input('page', 1) - 1) * 10);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('sys_config.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            '_key' => ['required', Rule::unique('mysql.sys_config_m', '_key')->whereNull('deleted_at')],
            'value1_description' => 'required'
        ]);

        SysConfigM::create($request->all());

        return redirect()->route('sys_config.index')
            ->with('status', '參數主檔新增成功');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $redirect = $request->get('redirect');
        if (!empty($redirect)) {
            $request->session()->flash('redirect', $redirect);
        }
        $data = SysConfigM::with('Details')->find($id);
        if (Auth::user()->can('sys_config') || Auth::user()->can('sys_config_' . $data->_key)) {
            return view('sys_config.edit', compact('data'));
        } else {
            return redirect()->back()->with('status', "您沒有編輯『{$data->remark}』的權限");
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'value1_description' => 'required'
        ]);
        //主檔
        $main = SysConfigM::find($id);
        if (!Auth::user()->can('sys_config') && !Auth::user()->can('sys_config_' . $main->_key)) {
            if ($request->session()->has('redirect')) {
                return redirect()->route($request->session()->get('redirect'))
                    ->with('status', "您沒有更新『{$main->remark}』的權限");
            } else {
                return redirect()->back()->with('status', "您沒有更新『{$main->remark}』的權限");
            }
        }
        $main->update($request->all());
        //明細檔
        $details = json_decode($request->get('details'));
        $ids = collect($details)->where('id', '!=', '')->pluck('id')->toArray(); //原資料ID
        if (empty($ids)) { //編輯後，無舊資料，刪除全部
            $main->Details()->delete();
        } else { //編輯後，有舊資料，刪除不在編輯過後的舅資料
            $main->Details()->whereNotIn('id', $ids)->delete();
        }
        foreach ($details as $detail) {
            $id = $detail->id;
            if (empty($id)) { //無id，新增
                $main->Details()->create([
                    'value1' => $detail->value1,
                    'value2' => $detail->value2,
                    'value3' => $detail->value3,
                ]);
            } else { //有id，更新
                $main->Details()->find($id)->update([
                    'value1' => $detail->value1,
                    'value2' => $detail->value2,
                    'value3' => $detail->value3,
                ]);
            }
        }
        if ($request->session()->has('redirect')) {
            return redirect()->route($request->session()->get('redirect'))
                ->with('status', '參數編輯成功');
        } else {
            return redirect()->route('sys_config.index')
                ->with('status', '參數編輯成功');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        SysConfigM::find($id)->delete();
        return redirect()->route('sys_config.index')
            ->with('status', '參數刪除成功');
    }
}
