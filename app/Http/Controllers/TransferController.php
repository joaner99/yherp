<?php

namespace App\Http\Controllers;

use App\Models\TMS\MTRAN;
use Illuminate\Http\Request;
use App\Exports\TransferExport;
use App\Services\TransferService;
use App\Models\yherp\TransferCheckM;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;

class TransferController extends Controller
{
    private $transferService;
    public function __construct(TransferService $transferService)
    {
        $this->transferService = $transferService;
    }

    //調撥作業
    public function index(Request $request)
    {
        $request->flash();
        $data = $this->transferService->GetTransfer($request);
        return view('transfer.index', $data);
    }

    //調撥清點作業
    public function check_index()
    {
        $data = $this->transferService->GetTransferCheck();
        return view('transfer.check_index', ['data' => $data]);
    }

    //調撥清點儲存
    public function check(Request $request)
    {
        $user_id = $request->get('user_id', Auth::user()->id);
        $items = json_decode($request->get('items'));
        if (empty($items) || count($items) == 0) {
            return redirect()->route('transfer.check_index')
                ->with('status', '清點失敗，為勾選任何項目');
        } else {
            $m = TransferCheckM::create([
                'user_id' => $user_id
            ]);
            foreach ($items as $item) {
                $m->Details()->create(['log_stock_id' => $item]);
            }
            return redirect()->route('transfer.check_index')
                ->with('status', '清點成功');
        }
    }

    //匯出TMS調撥處裡單
    public function export(Request $request)
    {
        $id = $request->get('id');
        return Excel::download(new TransferExport($id), 'TMS調撥處理單' . '.' . \Maatwebsite\Excel\Excel::XLSX);
    }

    //調撥匯出作業
    public function export_index()
    {
        $data = TransferCheckM::with(['User', 'Details', 'Details.LogStock'])
            ->whereNull('tms_order_no')
            ->get();
        $ids = [];
        foreach ($data->pluck('id')->toArray() as $id) {
            $ids[] = (string)$id;
        }
        $tms_data = MTRAN::select('MCODE', 'MDATE', 'MBACK3')
            ->where('MBACK3', '!=', '')
            ->whereIn('MBACK3', $ids)
            ->get();
        foreach ($data as $key => $d) {
            $tms = $tms_data->where('MBACK3', $d->id)->first();
            if (!empty($tms)) { //TMS有調撥處理單
                $d->update(['tms_order_no' => $tms->MCODE]);
                $data->forget($key);
            }
        }
        return view('transfer.export_index', ['data' => $data]);
    }
}
