<?php

namespace App\Http\Middleware;

use Closure;

class DataCheckMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (false) {
            $data = ['header' => '標題', 'content' => '內容'];
            return response()->view('message', ['data' => $data]);
        } else {
            return $next($request);
        }
    }
}
