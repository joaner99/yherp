<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\yherp\OrderCustomizedM;
use Illuminate\Support\Facades\Session;

class GetBadgesMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        Session::flash('badges_order_customized', $this->GetOrderCustomized());
        return $next($request);
    }

    private function GetOrderCustomized()
    {
        try {
            return OrderCustomizedM::where('completed', 0)->count();
        } catch (\Exception  $ex) {
            return 0;
        }
    }
}
