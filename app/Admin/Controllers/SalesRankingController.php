<?php

namespace App\Admin\Controllers;

use App\Models\TMS\Histin;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use DateTime;

class SalesRankingController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Example controller';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {

        //erp訂單日期格式
        $date = new DateTime("now");
        $date->modify("-1911 year");
        $nowDate = ltrim($date->format("Y.m.d"), "0");
        //預設當天日期
        if (empty(request()->HDAT1)) {
            request()->offsetSet('HDAT1', ['start' => $nowDate, 'end' => $nowDate]);
        }
        $grid = new Grid(new Histin);
        $grid->model()->selectRaw(
            '
            HICOD,HINAM,SUM("HINUM") AS amount,
            FLOOR(SUM("HTOTA")) AS total ,
            (SUM("HTOTA")-FLOOR(SUM("HSOUR")))/(select SUM(HTOTA) from Histin where  HDAT1 between ' . "'" . request()->HDAT1['start'] . "'" . '  and  ' . "'" . request()->HDAT1['end'] . "'" . ') as ContributionMargin
            '
        )->whereNotIn('HICOD', ['xxx123', 'XX126', 'XX123', '99010022004'])
            ->groupBy('HICOD')->groupBy('HINAM');
        $grid->column('HICOD', __('料號'));
        $grid->column('HINAM', __('品名'))->sortable();
        $grid->column('amount', __('數量'))->display(function ($amount) {
            return number_format($amount);
        })->sortable();
        $grid->column('total', __('總金額'))->display(function ($total) {
            return number_format($total);
        })->sortable();
        $grid->column('ContributionMargin', __('邊際貢獻'))->display(function ($ContributionMargin) {
            return $ContributionMargin * 100 . "%";
        })->sortable();
        $grid->disableRowSelector();
        $grid->disableActions();
        $grid->disableCreateButton();
        $grid->paginate(1000);
        $grid->filter(function ($filter) {
            $filter->disableIdFilter();
            $filter->between('HDAT1', '銷貨單日期');
        });
        $grid->quickSearch(function ($model, $query) {
            $model->where('HINAM', 'like', "%{$query}%");
        });



        $grid->selector(function (Grid\Tools\Selector $selector) {
            $selector->select('HPCOD', '客戶', [
                'S001' => '蝦皮一店',
                'S003' => '蝦皮二店',
                'S002' => '官網',
                'Y001' => 'YAHOO',
                'C001' => '面交',
                'L001' => '生活',
                'P001' => '松果',
                'P002' => 'pchome',
                'R001' => '露天',
                'D001' => '亞銘',
                'C002' => '退換貨',
                'C003' => '貨到付款',
                'C004' => '客人換貨',
                'C005' => '補寄貨',
                'C006' => '來回件',
                'C007' => '已付款後寄貨',
                'C008' => '邱大哥',
                'Z001' => '其他',
            ]);
        });
        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed   $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(ExampleModel::findOrFail($id));

        $show->field('id', __('ID'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new ExampleModel);

        $form->display('id', __('ID'));
        $form->display('created_at', __('Created At'));
        $form->display('updated_at', __('Updated At'));

        return $form;
    }
}
