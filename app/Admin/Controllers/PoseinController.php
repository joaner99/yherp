<?php

namespace App\Admin\Controllers;

use App\Models\TMS\Posein;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use DateTime;

class PoseinController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Example controller';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {

        //erp訂單日期格式
        $date = new DateTime("now");
        $date->modify("-1911 year");
        $nowDate = ltrim($date->format("Y.m.d"), "0");
        //預設當天日期
        if (empty(request()->PDAT1)) {
            request()->offsetSet('PDAT1', ['start' => $nowDate, 'end' => $nowDate]);
        }
        $grid = new Grid(new Posein);
        $grid->column('PDAT1', __('銷貨日'));
        $grid->column('PCOD1', __('訂單號碼'));
        $grid->column('PCOD2', __('銷貨單號碼'));
        $grid->column('PJONO', __('原始訂單號碼'));
        $grid->column('PTXCO', __('發票號碼'));
        $grid->column('ConsignTran', __('物流單號'));
        $grid->column('PPNAM', __('客戶'));
        $grid->column('PoseinCheckDataLog.CheckCount', __('驗貨次數'));
        $grid->column('Poseou.PCOD1', __('退貨單'));
        //修改顯示輸出
        $grid->column('PTOTA', __('小計'))->display(function ($text) {
            return number_format($text);
        })->totalRow(function ($text) {
            return number_format($text);
        });

        $grid->disableRowSelector();
        $grid->disableActions();
        $grid->disableCreateButton();


        $grid->filter(function ($filter) {
            $filter->disableIdFilter();
            $filter->between('PDAT1', '銷貨單日期');
        });

        $grid->selector(function (Grid\Tools\Selector $selector) {
            $selector->select('PPCOD', '客戶', [
                'S001' => '蝦皮一店',
                'S003' => '蝦皮二店',
                'S002' => '官網',
                'Y001' => 'YAHOO',
                'C001' => '面交',
                'L001' => '生活',
                'P001' => '松果',
                'P002' => 'pchome',
                'R001' => '露天',
                'D001' => '亞銘',
                'C002' => '退換貨',
                'C003' => '貨到付款',
                'C004' => '客人換貨',
                'C005' => '補寄貨',
                'C006' => '來回件',
                'C007' => '已付款後寄貨',
                'C008' => '邱大哥',
                'Z001' => '其他',
            ]);
        });

        $grid->quickSearch(function ($model, $query) {
            $model->where('PCOD1', $query)->orWhere('PCOD2', 'like', "%{$query}%")->orWhere('PJONO', 'like', "%{$query}%")->orWhere('PTXCO', 'like', "%{$query}%");
        });
        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed   $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(ExampleModel::findOrFail($id));

        $show->field('id', __('ID'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new ExampleModel);

        $form->display('id', __('ID'));
        $form->display('created_at', __('Created At'));
        $form->display('updated_at', __('Updated At'));

        return $form;
    }
}
