
//JS Date add days
Date.prototype.addDays = function (days) {
    var date = new Date(this.valueOf());
    date.setDate(date.getDate() + days);
    return date;
}

//string insert
String.prototype.splice = function (idx, rem, str) {
    return this.slice(0, idx) + str + this.slice(idx + Math.abs(rem));
};

//
function IsNullOrEmpty(obj) {
    return obj == null || obj === '';
}

//convert to thousands
function toThousands(num) {
    var parts = num.toString().split('.');
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    return parts.join('.');
}

//convert to number from thousands
function CustomerToNumber(str) {
    str = str.trim();
    //thousands
    const thousandsRule = /^(-?\d{1,3})(\,\d{3})*(((\,\d{3})|(\.\d+))%?)$/;
    if (thousandsRule.test(str)) {
        str = str.replaceAll(",", "");
    }
    //percent
    const percentRule = /(%{1})$/;
    if (percentRule.test(str)) {
        str = str.replaceAll("%", "");
    }
    return str;
}

//
function DateToString(date) {
    if (IsNullOrEmpty(date)) {
        return date;
    } else {
        var dd = date.getDate();
        var mm = date.getMonth() + 1; //January is 0!
        var yyyy = date.getFullYear();
        if (dd < 10) {
            dd = '0' + dd
        }
        if (mm < 10) {
            mm = '0' + mm
        }
        return yyyy + '-' + mm + '-' + dd;
    }
}

//roc to ad
function toADDate(date) {
    if (IsNullOrEmpty(date)) {
        return date;
    } else {
        var sp = date.split('.');
        var yyyy = parseInt(sp[0]) + 1911;
        var mm = parseInt(sp[1]) - 1;
        var dd = sp[2];
        return DateToString(new Date(yyyy, mm, dd));
    }
}