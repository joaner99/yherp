@extends('layouts.base')
@section('title')
    排程管理
@stop
@section('css')
    <style type="text/css">

    </style>
@endsection
@section('content')
    <!-- 主畫面 -->
    <div class="container-fluid">
        <form action="{{ route('schedule_log') }}" method="GET" class="mb-2">
            <div class="form-row">
                <div class="form-group col-auto">
                    <label for="input_start_date">起日</label>
                    <input id="input_start_date" name="start_date" class="form-control" type="date"
                        value="{{ old('start_date') }}" required>
                </div>
                <div class="form-group col-auto">
                    <label for="input_end_date">訖日</label>
                    <input id="input_end_date" name="end_date" class="form-control" type="date"
                        value="{{ old('end_date') }}" required>
                </div>
            </div>
            <button class="btn btn-primary" type="submit">
                <i class="bi bi-search"></i>查詢
            </button>
        </form>
        <div class="form-row mt-2">
            <div class="col">
                @if (empty($data))
                    <div class="alert alert-danger">
                        找不到資料
                    </div>
                @else
                    <div class="FrozenTable" style="max-height: 75vh; font-size: 0.95rem;">
                        <table class="table table-bordered table-hover sortable table-filter">
                            <thead>
                                <tr>
                                    <th style="width: 5%;">流水號</th>
                                    <th style="width: 10%;">指令</th>
                                    <th style="width: 15%;">名稱</th>
                                    <th style="width: 8%;" class="filter-col">狀態</th>
                                    <th>訊息</th>
                                    <th style="width: 8%;">時間</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($data as $item)
                                    <tr>
                                        <td>{{ $item->id }}</td>
                                        <td>{{ $item->cmd }}</td>
                                        <td>{{ $item->name }}</td>
                                        @switch($item->status)
                                            @case('T')
                                                <td class="bg-success text-white">成功</td>
                                            @break

                                            @case('F')
                                                <td class="bg-danger text-white">失敗</td>
                                            @break

                                            @default
                                                <td class="bg-secondary text-white">未知</td>
                                        @endswitch
                                        <td>{{ $item->msg }}</td>
                                        <td>{{ $item->created_at }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                @endif
            </div>
        </div>
    </div>
@stop
@section('script')
    <script type="text/javascript">
        $(document).ready(InitialDate());

        function InitialDate() {
            var today = new Date();

            if ($("#input_start_date").val() == '') {
                $("#input_start_date").val(DateToString(today));
            }
            if ($("#input_end_date").val() == '') {
                $("#input_end_date").val(DateToString(today));
            }

            $("#input_start_date,#input_end_date").attr("max", DateToString(today));
        }
    </script>
@stop
