@include('includes.login')
@extends('layouts.master')
@section('master_title')
    收料作業
@stop
@section('master_css')
    @yield('login_css')
    <style type="text/css">
        .title {
            font-size: 4rem;
            font-weight: bolder;
            color: white;
            text-align: center;
            margin: 0 0.5rem;
            border-radius: 20px;
        }

        #btn_start {
            font-size: 10rem;
            font-weight: bolder;
            color: white;
            padding: 1rem 5rem;
            margin-top: 25vh;
            border-radius: 30px;
        }

        #btn_done,
        #btn_cancel,
        #div_msg {
            font-size: 3rem;
            border-radius: 30px;
        }

        #div_items table,
        #div_items input {
            font-size: 1.5rem;
            font-weight: bolder;
        }

        #div_default_msg>i {
            font-size: 10rem;
        }

        #div_default_msg>div {
            font-size: 3rem;
        }
    </style>
@stop
@section('master_body')
    @yield('login_body')
    {{-- Modal 訊息 --}}
    <div id="modal_msg" class="modal">
        <div class="modal-dialog modal-dialog-centered modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title">訊息</h1>
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="div_msg"></div>
                </div>
                <div class="modal-footer">
                    <button id="btn_msg_close" type="button" class="btn btn-secondary" data-dismiss="modal">關閉</button>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid pt-2">
        <div class="form-row">
            <div class="col-auto p-0">
                <form action="{{ route('receipt.store') }}"method="POST" enctype="multipart/form-data" class="h-100">
                    @csrf
                    <input id="input_user_id" name="user_id" type="hidden">
                    <input name="time" type="hidden">
                    <input name="items" type="hidden">
                    <button id="btn_done" class="btn btn-success btn-lg h-100" style="display: none;" type="submit">
                        <i class="bi bi-check-circle"></i>完成
                    </button>
                </form>
            </div>
            <div class="col title bg-info">
                收料作業
            </div>
            <div class="col-auto p-0">
                <button id="btn_cancel" class="btn btn-danger btn-lg h-100" style="display: none;">
                    <i class="bi bi-x-circle"></i>取消
                </button>
            </div>
        </div>
        <div class="form-row">
            <div class="col"></div>
            <div class="col-auto">
                <button id="btn_start" class="btn btn-success btn-lg">開始</button>
            </div>
            <div class="col"></div>
        </div>
        <div id="div_receiving" class="form-row mt-2" style="display:none;">
            <div class="col">
                <div class="form-row">
                    <div class="col">
                        <div class="input-group input-group-lg h-100">
                            <div class="input-group-prepend">
                                <span class="input-group-text">收料人員</span>
                            </div>
                            <span class="input-group-text bg-white" id="span_user_name"></span>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="input-group input-group-lg h-100">
                            <div class="input-group-prepend">
                                <span class="input-group-text">商品條碼</span>
                            </div>
                            <input class="form-control" style="height: auto;" id="input_item_no" name="item_no"
                                list="item_list" type="text" autocomplete="off" placeholder="請使用手持式掃描機，掃描商品條碼" autofocus>
                            <div class="input-group-append">
                                <button id="btn_add" class="btn btn-primary" type="submit">確認</button>
                            </div>
                        </div>
                    </div>
                    <div class="col"></div>
                </div>
                <div id="div_default_msg" class="form-row flex-column align-items-center">
                    <i class="bi bi-upc-scan"></i>
                    <div class="text-center font-weight-bolder">請使用手持式掃描機<br>掃描商品條碼</div>
                </div>
                <div id="div_items" class="form-row mt-2" style="display: none;">
                    <div class="col">
                        <div class="FrozenTable" style="max-height: 73vh;">
                            <table class="table table-bordered sortable">
                                <caption>已點清單</caption>
                                <thead>
                                    <tr>
                                        <th style="width: 7%;">序</th>
                                        <th style="width: 14%;">料號</th>
                                        <th>品名</th>
                                        <th style="width: 10%;">數量</th>
                                        <th style="width: 7%;">編輯</th>
                                    </tr>
                                </thead>
                                <tbody id="tbody_items">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <datalist id="item_list">
        @if (!empty($items))
            @foreach ($items as $item)
                <option value="{{ $item->item_no }}">{{ $item->item_name }}</option>
            @endforeach
        @endif
    </datalist>
@stop
@section('master_js')
    @yield('login_js')
    <script type="text/javascript">
        var lock = false; //ajax lock
        var item_idx = 1; //清單流水號
        //未登入防止關閉
        $('#modal_login').on('hide.bs.modal', function(e) {
            if (isLogin) {
                $('#input_item_no').focus();
                $('#input_user_id').val(user_id);
                $('#span_user_name').text(user_name);
            } else {
                return false;
            }
        })
        //開始
        $('#btn_start').click(function() {
            $('#modal_login').modal('show');
            $(this).parent('div').hide();
            $('#div_receiving,#btn_done,#btn_cancel').show();
            $('input[name="time"]').val("{{ date('Y-m-d H:i:s') }}");
        });
        //完成
        $('#btn_done').click(function() {
            var tmp = new Array();
            $('input[data-no]').each(function(i, e) {
                tmp.push({
                    no: $(e).data().no,
                    name: $(e).data().name,
                    qty: $(e).val(),
                });
            });
            if (tmp.length == 0) {
                show_msg('無任何商品，請勿完成');
                return false;
            }
            $("input[name='items']").val(JSON.stringify(tmp));
        });
        //取消
        $('#btn_cancel').click(function() {
            location.reload();
        });
        //確認商品
        $('#btn_add').click(get_item);
        //確認數量
        $('#input_item_no').keyup(function(event) {
            if (event.keyCode === 13) {
                event.preventDefault();
                get_item();
            }
        });
        //顯示訊息
        function show_msg(text) {
            $('#div_msg').text(text);
            $('#modal_msg').modal('show');
            $('#btn_msg_close').focus();
        }
        //取得料號資訊
        function get_item() {
            if (lock) {
                return;
            }
            var id = $('#input_item_no').val();
            if (IsNullOrEmpty(id)) {
                $('#input_item_no').val('');
                $('#input_item_no').focus();
                return;
            } else {
                var item_name = $('#item_list>option[value="' + id + '"]').text();
                if (IsNullOrEmpty(item_name)) {
                    $('#input_item_no').attr('disabled', true);
                    lock = true;
                } else {
                    add_item({
                        ICODE: id,
                        INAME: item_name
                    });
                    $('#input_item_no').val('');
                    return;
                }
            }
            $.ajax({
                url: '{{ route('receipt.ajax_get_item') }}',
                type: "get",
                dataType: 'json',
                data: {
                    id: id,
                },
                contentType: "application/json",
                success: function(data) {
                    if (!$.isEmptyObject(data)) {
                        add_item(data);
                    }
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    var msg = JSON.parse(xhr.responseText).msg;
                    show_msg(msg);
                },
                complete: function() {
                    lock = false;
                    $('#input_item_no').val('');
                    $('#input_item_no').attr('disabled', false);
                    $('#input_item_no').focus();
                }
            });
        }
        //將商品加入清單
        function add_item(obj) {
            $('#div_items').show();
            $('#div_default_msg').hide();
            var no = obj.ICODE;
            var name = obj.INAME;
            var qty = 0;
            var input = $('input[data-no="' + no + '"]');
            var exists = input.length != 0;
            if (exists) {
                qty = parseInt($(input).val(), 10) + 1;
            } else {
                qty = 1;
            }
            if (exists) {
                $('input[data-no="' + no + '"]').val(qty);
            } else {
                var dom = '';
                dom += '<tr>';
                dom += '<td>' + item_idx + '</td>';
                dom += '<td>' + no + '</td>';
                dom += '<td class="text-left">' + name + '</td>';
                dom += '<td><input data-no="' + no + '" data-name="' + name +
                    '" class="form-control text-center" type="number" value="' + qty +
                    '" min="0"></td>';
                dom +=
                    '<td><button class="btn btn-danger" type="button" onclick="ItemDelete(this)"><i class="bi bi-trash"></i>刪除</button></td>';
                dom += '</tr>';
                $('#div_items tbody').append(dom);
                item_idx++;
            }
        }
        //刪除商品
        function ItemDelete(obj) {
            if ($("#tbody_items>tr").length > 1) {
                $(obj).closest('tr').remove();
            }
        }
    </script>
@stop
