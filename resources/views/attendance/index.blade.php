@extends('layouts.base')
@section('title')
    出勤系統
@stop
@section('css')
    <style type="text/css">
        .clock {
            position: relative;
            bottom: 0;
            right: 0;
            width: 700px;
            height: 150px;
            font-size: 6rem;
            align-content: center;
        }

        #txta_log {
            width: 700px;
            height: 300px;
            resize: none;
        }

        #input_user {
            height: 100px;
            font-size: 4rem;
            color: transparent;
            height: auto;
        }
    </style>
@endsection
@section('content')
    <div class="container-fluid">
        <div class="form-row">
            <div class="col">
                <input class="form-control" id="input_user" name="barcode" type="text" autocomplete="off"
                    placeholder="請使用手持式掃描機，掃描員工條碼，進行打卡">
            </div>
        </div>
        <div class="form-row mt-2" style="position: relative; z-index: 99;">
            <div class="col">
                @if (!empty($data) && count($data) > 0)
                    <div class="FrozenTable" style="max-height: 86vh;">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($data as $name => $times)
                                    <tr>
                                        <td>{{ $name }}</td>
                                        @foreach ($times as $t)
                                            <td style="width: 11%;">
                                                {{ $t['card_time'] }}
                                                @if (empty($t['uploaded_at']))
                                                    <br>
                                                    未上傳
                                                @endif
                                            </td>
                                        @endforeach
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                @endif
            </div>
            <div class="col-auto">
                <div class="form-row">
                    <div class="col">
                        <div class="clock"></div>
                    </div>
                </div>
                <div class="form-row mt-2">
                    <div class="col">
                        <div class="card">
                            <div class="card-header">
                                Log
                            </div>
                            <div class="card-body p-0">
                                <textarea id="txta_log" class="form-control" readonly></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('script')
    <script type="text/javascript">
        HideSidebar();
        $(document).ready(function() {
            //10M自動刷新
            setInterval(function() {
                location.reload();
            }, 10 * 60 * 1000);
            clockUpdate();
            setInterval(function() {
                clockUpdate();
                $('#input_user').focus();
            }, 1000);
        });
        //打卡
        $('#input_user').keyup(function(event) {
            if (event.keyCode === 13) { //Enter
                event.preventDefault();
                lockScreen();
                data = {
                    barcode: $('#input_user').val()
                };
                $('#input_user').val('');
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: "{{ route('attendance.ajax_check_in') }}",
                    type: "post",
                    data: data,
                    success: function(data) {
                        showMsg(data);
                    },
                    error: function(xhr, ajaxOptions, thrownError) {
                        var msg = JSON.parse(xhr.responseText).msg;
                        alert(msg);
                    },
                    complete: function() {
                        unlockScreen();
                        $('#input_user').focus();
                    }
                });
            }
        });
        //顯示Log
        function showMsg(data) {
            var time = data.time;
            var user = data.user;
            var emp_no = data.emp_no;
            var msg = data.msg;
            var result = '';
            if (!IsNullOrEmpty(time)) {
                result += '【' + time + '】';
            }
            if (!IsNullOrEmpty(user)) {
                result += ' ' + user;
            }
            if (!IsNullOrEmpty(emp_no)) {
                result += ' ' + emp_no;
            }
            if (!IsNullOrEmpty(msg)) {
                result += ' ' + msg;
            }
            $('#txta_log').append(result + '\n');
            var textarea = document.getElementById('txta_log');
            textarea.scrollTop = textarea.scrollHeight;
        }
    </script>
@stop
