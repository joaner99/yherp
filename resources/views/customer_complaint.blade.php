@extends('layouts.base')
@section('title')
    客訴單
@stop
@section('css')
    <style type="text/css">
        #ul_status>li,
        #ul_mark>li,
        #div_status,
        #div_mark {
            padding: 0.25rem 0.75rem;
            cursor: pointer;
        }

        #btn_search_order {
            border-top-right-radius: 0.25rem;
            border-bottom-right-radius: 0.25rem;
        }

        /*特殊註記*/
        .spMark {
            border-style: solid;
            border-width: 1px;
            border-radius: 0.25rem;
            border-color: darkgray;
            padding: 0.25rem;
            margin: 0.1rem 0.25rem;
        }

        /*急件*/
        .urgent {
            color: white;
            background-color: red;
            font-weight: bolder;
            font-size: 3rem;
            font-family: cursive;
            border-radius: 30px;
        }

        li[data-status="0"],
        tr[data-status="0"]>td.status {
            background-color: lightgreen;
            color: #343a40;
        }

        li[data-status="1"],
        tr[data-status="1"]>td.status {
            background-color: #007bff;
            color: #fff;
        }


        li[data-status="2"],
        tr[data-status="2"]>td.status,
        li[data-status="7"],
        tr[data-status="7"]>td.status {
            background-color: darkorchid;
            color: #fff;
        }

        li[data-status="8"],
        tr[data-status="8"]>td.status {
            background-color: darkblue;
            color: #fff;
        }

        li[data-status="3"],
        tr[data-status="3"]>td.status,
        li[data-status="6"],
        tr[data-status="6"]>td.status {
            background-color: #ffc107;
            color: #343a40;
        }

        li[data-status="4"],
        tr[data-status="4"]>td.status {
            background-color: #e83e8c;
            color: #fff;
        }

        li[data-status="5"],
        tr[data-status="5"]>td.status {
            background-color: aquamarine;
            color: #343a40;
        }

        li[data-status="99"],
        tr[data-status="99"]>td.status {
            background-color: whitesmoke;
            color: #343a40;
        }
    </style>
@endsection
@section('content')
    <!-- Modal 新增客訴單 -->
    <div id="modal_create" class="modal fade">
        <div class="modal-dialog modal-xl modal-dialog-centered">
            <div class="modal-content">
                <form method="POST" action="{{ route('customer_complaint.create') }}" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-header">
                        <div class="modal-title">
                            <h1>新增客訴單</h1>
                        </div>
                        <button type="button" class="close" data-dismiss="modal">
                            <span>&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="container-fluid">
                            <div class="form-row">
                                <div class="col-6">
                                    <div class="input-group">
                                        <select id="select_type" class="custom-select bg-light">
                                            <option value="1" selected>原始訂單編號</option>
                                            <option value="2">ERP訂單編號</option>
                                        </select>
                                        <input id="input_order_no" class="form-control" type="text">
                                        <div class="input-group-append">
                                            <button id="btn_search_order" class="btn btn-primary"
                                                type="button">帶入資料</button>
                                            <button id="btn_search_order_loading" class="btn btn-primary"
                                                style="display: none;" type="button" disabled>
                                                <span class="spinner-border spinner-border-sm" role="status"></span>
                                                處理中...
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="div_order_data" class="form-row mt-2">
                                <div class="col">
                                    <div class="FrozenTable" style="max-height: 100%;">
                                        <table class="table table-bordered">
                                            <caption>訂單資料</caption>
                                            <tbody>
                                                <tr>
                                                    <th style="width: 13%;">ERP訂單編號</th>
                                                    <td style="width: 20%;" id="td_order_no"></td>
                                                    <th style="width: 13%;">ERP銷貨編號</th>
                                                    <td style="width: 20%;" id="td_sale_no"></td>
                                                    <th style="width: 13%;">原始訂單編號</th>
                                                    <td id="td_origin_no"></td>
                                                </tr>
                                                <tr>
                                                    <th style="width: 10%;">訂單日期</th>
                                                    <td id="td_date"></td>
                                                    <th style="width: 10%;">平台來源</th>
                                                    <td id="td_source"></td>
                                                    <th style="width: 10%;">客戶代碼</th>
                                                    <td id="td_occod"></td>
                                                </tr>
                                                <tr>
                                                    <th>備註1</th>
                                                    <td id="td_order_remark1" colspan="6"></td>
                                                </tr>
                                                <tr>
                                                    <th>備註2</th>
                                                    <td id="td_order_remark2" colspan="6"></td>
                                                </tr>
                                                <tr>
                                                    <th>備註3</th>
                                                    <td id="td_order_remark3" colspan="6"></td>
                                                </tr>
                                                <tr>
                                                    <th>內部備註</th>
                                                    <td id="td_order_remarkInside" colspan="6"></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <table class="table table-bordered sortable">
                                            <caption>訂單明細</caption>
                                            <thead>
                                                <tr>
                                                    <th style="width: 15%;">產品料號</th>
                                                    <th>產品名稱</th>
                                                    <th style="width: 10%;">數量</th>
                                                    <th style="width: 10%;">總價</th>
                                                </tr>
                                            </thead>
                                            <tbody id="tbody_order_detail">
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div id="div_create_form" style="display: none;">
                                <hr class="my-4">
                                <div class="form-row mt-2">
                                    <div class="form-group col">
                                        <label class="form-check-label text-danger" for="input_remark">*事情簡述</label>
                                        <textarea class="form-control" id="input_remark" name="remark" style="height: 200px;" required></textarea>
                                    </div>
                                </div>
                                <div class="form-row mt-2">
                                    <div class="form-group col-auto">
                                        <label class="text-danger" for="create_select_status">*狀態</label>
                                        <select id="create_select_status" name="status" class="custom-select" required>
                                            <option value="" selected>請選擇...</option>
                                            @if (!empty($config))
                                                @foreach ($config as $cfg)
                                                    @if ($cfg->value1 == '0' || $cfg->value1 == '99')
                                                        @continue
                                                    @endif
                                                    <option value="{{ $cfg->value1 }}">{{ $cfg->value2 }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                    <div class="form-group col-auto">
                                        <label>其他選項</label>
                                        <div class="form-check-list form-check-list-horizontal">
                                            <div class="form-check">
                                                <input class="form-check-input" id="input_urgent" name="urgent"
                                                    type="checkbox" value="1">
                                                <label class="form-check-label" for="input_urgent">
                                                    急件
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-auto">
                                        <label class="">特殊註記</label>
                                        <div class="form-check-list form-check-list-horizontal">
                                            @if (!empty($markConfig))
                                                @foreach ($markConfig as $item)
                                                    <div class="form-check">
                                                        <input class="form-check-input"
                                                            id="input_role_{{ $item->value1 }}" name="marks[]"
                                                            type="checkbox" value="{{ $item->value1 }}">
                                                        <label class="form-check-label"
                                                            for="input_role_{{ $item->value1 }}">
                                                            {{ $item->value2 }}
                                                        </label>
                                                    </div>
                                                @endforeach
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="div_order_msg" class="alert alert-danger mt-2" style="display: none;"></div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div id="div_create_customer" class="form-check" style="display: none;">
                            <input class="form-check-input" id="input_create_customer" name="create_customer"
                                type="checkbox" value="1">
                            <label class="form-check-label" for="input_create_customer">同時建立客製化訂單</label>
                        </div>
                        <button id="btn_create" class="btn btn-primary" style="display: none;"
                            type="submit">確認</button>
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">取消</button>
                    </div>
                    <input id="hdf_order_no" name="order_no" type="hidden">
                    <input id="hdf_origin_no" name="origin_no" type="hidden">
                </form>
            </div>
        </div>
    </div>
    <!-- Modal 編輯客訴單 -->
    <div id="modal_edit" class="modal fade">
        <div class="modal-dialog modal-xl modal-dialog-centered">
            <div class="modal-content">
                <form method="POST" action="{{ route('customer_complaint.update') }}" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-header bg-warning">
                        <div class="modal-title">
                            <h1>編輯客訴單</h1>
                        </div>
                        <button type="button" class="close" data-dismiss="modal">
                            <span>&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="container-fluid">
                            <div class="form-row">
                                <div class="form-group col-auto">
                                    <label>ERP訂單編號</label>
                                    <input name="order_no" class="form-control-plaintext" type="text" readonly>
                                </div>
                                <div class="form-group col-auto">
                                    <label>原始訂單編號</label>
                                    <input name="origin_no" class="form-control-plaintext" type="text" readonly>
                                </div>
                                <div class="form-group col-auto">
                                    <label for="edit_select_status">狀態</label>
                                    <select id="edit_select_status" name="status" class="custom-select" required>
                                        @if (!empty($config))
                                            @foreach ($config as $cfg)
                                                <option value="{{ $cfg->value1 }}">{{ $cfg->value2 }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                                <div class="form-group col-auto">
                                    <label>其他選項</label>
                                    <div class="form-check-list form-check-list-horizontal">
                                        <div class="form-check">
                                            <input class="form-check-input" id="edit_input_urgent" name="urgent"
                                                type="checkbox" value="1">
                                            <label class="form-check-label" for="edit_input_urgent">
                                                急件
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-auto">
                                    <label>特殊註記</label>
                                    <div class="form-check-list form-check-list-horizontal">
                                        @if (!empty($markConfig))
                                            @foreach ($markConfig as $item)
                                                <div class="form-check">
                                                    <input class="form-check-input"
                                                        id="edit_input_role_{{ $item->value1 }}" name="marks[]"
                                                        type="checkbox" value="{{ $item->value1 }}">
                                                    <label class="form-check-label"
                                                        for="edit_input_role_{{ $item->value1 }}">
                                                        {{ $item->value2 }}
                                                    </label>
                                                </div>
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="form-row mt-2">
                                <div class="form-group col">
                                    <label class="form-check-label" for="input_edit_remark">事情簡述</label>
                                    <textarea id="input_edit_remark" name="remark" class="form-control" style="height: 450px;" required></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary" type="submit">確認</button>
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">取消</button>
                    </div>
                    <input name="id" type="hidden">
                </form>
            </div>
        </div>
    </div>
    <!-- 主畫面 -->
    <div class="container-fluid" style="font-size: 0.9rem;">
        <div class="form-row">
            <div class="col-auto">
                <form action="{{ route('customer_complaint.index') }}" method="GET">
                    <div class="form-row">
                        <div class="form-group col-auto mb-2">
                            <label for="input_start_date">更新起日</label>
                            <input id="input_start_date" name="start_date" class="form-control" type="date"
                                value="{{ old('start_date') }}" required>
                        </div>
                        <div class="form-group col-auto mb-2">
                            <label for="input_end_date">更新訖日</label>
                            <input id="input_end_date" name="end_date" class="form-control" type="date"
                                value="{{ old('end_date') }}" required>
                        </div>
                        <div class="form-group col-auto mb-2">
                            <label for="input_order_no">ERP/原始訂單編號</label>
                            <input id="input_order_no" name="order_no" class="form-control" type="text"
                                placeholder="空白則搜尋全部" value="{{ old('order_no', '') }}">
                        </div>
                    </div>
                    <div class="form-row mb-2">
                        <div class="col-auto ml-1">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" id="chk_include_done"
                                    name="include_done" value='1'
                                    {{ request()->has('include_done') ? 'checked' : '' }}>
                                <label class="form-check-label" for="chk_include_done">
                                    含結案訂單
                                </label>
                            </div>
                        </div>
                    </div>
                    <button class="btn btn-primary btn-sm">查詢</button>
                    <a class="btn btn-info btn-sm" href="{{ route('customer_complaint.index') . '?only_undone=1' }}">
                        查詢未結案
                    </a>
                </form>
            </div>
            <div class="col"></div>
            <div class="col-auto">
                <div class="form-row">
                    <div class="col"></div>
                    <div class="col-auto">
                        <button type="button" class="btn btn-info btn-sm" data-toggle="modal"
                            data-target="#modal_create">
                            <i class="bi bi-plus-lg"></i>新增客訴單
                        </button>
                    </div>
                    <div class="col-auto">
                        <form action="{{ route('export_table') }}" method="post">
                            @csrf
                            <input name="export_name" type="hidden" value="客訴單">
                            <input name="export_data" type="hidden">
                            <button id="btn_export" class="btn btn-success btn-sm" type="submit"
                                export-target="#table_data">
                                <i class="bi bi-file-earmark-excel"></i>匯出Excel
                            </button>
                        </form>
                    </div>
                </div>
                @if (!empty($config))
                    <form method="POST" action="{{ route('customer_complaint.update_status') }}"
                        enctype="multipart/form-data">
                        @csrf
                        <div class="form-row mt-2">
                            <div class="col"></div>
                            <div class="col-auto">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <label class="input-group-text" for="select_batch_status">批次修改狀態</label>
                                    </div>
                                    <select class="custom-select" id="select_batch_status" name="status">
                                        @foreach ($config as $cfg)
                                            <option value="{{ $cfg->value1 }}">{{ $cfg->value2 }}</option>
                                        @endforeach
                                    </select>
                                    <div class="input-group-append">
                                        <button id="btn_update_status" class="btn btn-primary" type="submit">修改</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <input id="input_status_ids" name="ids" type="hidden">
                    </form>
                @endif
            </div>
        </div>
        @if (!empty($data) && count($data) > 0)
            <div class="form-row mt-2">
                <div class="col-auto">
                    <div class="card">
                        <div id="div_status" class="card-header bg-secondary text-white py-2">
                            狀態
                            <span class="float-right">總計：{{ count($data) }}單</span>
                        </div>
                        <div class="card-body p-2">
                            <ul id="ul_status" class="list-group list-group-horizontal">
                                @foreach ($data->sortBy('status_name')->groupBy('status') as $status => $group)
                                    <li class="list-group-item" data-status="{{ $status }}">
                                        <span>{{ $group->first()->status_name }}：{{ $group->count() }}
                                            單</span>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-auto">
                    <div class="card">
                        <div id="div_mark" class="card-header bg-light py-2">
                            特殊註記
                        </div>
                        <div class="card-body p-2">
                            <ul id="ul_mark" class="list-group list-group-horizontal">
                                @if (!empty($markConfig) && count($markConfig) > 0)
                                    @foreach ($markConfig as $config)
                                        <li class="list-group-item" data-mark="{{ $config->value1 }}"
                                            @if (!empty($config->value3)) style="background-color: {{ $config->value3 }}" @endif>
                                            <span>{{ $config->value2 }}：{{ $config->count ?? 0 }}單</span>
                                        </li>
                                    @endforeach
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        <div class="form-row mt-2">
            <div class="col">
                <div class="FrozenTable" style="max-height: 67vh; font-size:0.8rem;">
                    <table id="table_data" class="table table-bordered table-hover sortable">
                        <thead>
                            <tr>
                                <th style="width: 3%;">選取</th>
                                <th style="width: 3%;">序</th>
                                <th style="width: 7%;">ERP訂單單號</th>
                                <th style="width: 7%;">ERP銷貨單號</th>
                                <th style="width: 9%;">原始訂單編號</th>
                                <th style="width: 4%;">客戶<br>代碼</th>
                                <th style="width: 4%;">急件</th>
                                <th style="width: 4%;">狀態</th>
                                <th style="width: 6%;">特殊註記</th>
                                <th>事情簡述</th>
                                <th style="width: 6%;">建立時間</th>
                                <th style="width: 6%;">更新時間</th>
                                <th style="width: 4%;">經過<br>天數</th>
                                <th class="export-none" style="width: 6%;">操作</th>
                            </tr>
                        </thead>
                        <tbody id="tbody_data">
                            @foreach ($data as $value)
                                <tr class="{{ $value->is_timeout ? 'table-danger' : '' }}" data-id="{{ $value->id }}"
                                    data-order_no="{{ $value->order_no }}" data-origin_no="{{ $value->origin_no }}"
                                    data-status="{{ $value->status }}" data-remark="{{ $value->remark }}"
                                    data-mark="{{ $value->mark }}" data-urgent="{{ $value->urgent }}">
                                    <td><input class="confirm" type="checkbox"></td>
                                    <td>{{ $value->id }}</td>
                                    <td>
                                        <a href="{{ route('order_detail', $value->order_no) }}">
                                            {{ $value->order_no }}
                                        </a>
                                    </td>
                                    <td>
                                        @foreach ($value->Order->POSEIN as $sale)
                                            <div>
                                                <a href="{{ route('check_log_detail', $sale->sale_no) }}">
                                                    {{ $sale->sale_no }}
                                                </a>
                                            </div>
                                        @endforeach
                                    </td>
                                    <td class="original_no_font">{{ $value->origin_no }}</td>
                                    <td>{{ $value->Order->cust }}<br>{{ $value->Order->cust_name }}</td>
                                    <td class="p-0">
                                        @if (!empty($value->urgent))
                                            <span class="urgent">！</span>
                                        @endif
                                    </td>
                                    <td class="status">{{ $value->status_name }}</td>
                                    <td class="export-cell-multiple p-0">
                                        <div class="d-flex flex-column">
                                            @foreach ($value->mark_names as $mark_name)
                                                <span class="spMark"
                                                    @if (!empty($mark_name['color'])) style="background-color: {{ $mark_name['color'] }}" @endif>{{ $mark_name['name'] }}</span>
                                            @endforeach
                                        </div>
                                    </td>
                                    <td class="px-2 py-1">
                                        <textarea class="form-control-plaintext p-0" readonly>{{ $value->remark }}</textarea>
                                    </td>
                                    <td>{{ $value->created_at }}</td>
                                    <td>{{ $value->updated_at }}</td>
                                    <td>{{ $value->elapsed_days }}</td>
                                    <td class="p-0">
                                        <a class="btn btn-secondary btn-sm my-1"
                                            href="{{ route('order_customized.create_by_cc', $value->id) }}">
                                            <i class="bi bi-arrow-repeat"></i>轉單
                                        </a>
                                        <button class="btn btn-warning btn-sm my-1" type="button" data-toggle="modal"
                                            data-target="#modal_edit">
                                            <i class="bi bi-pencil-fill"></i>編輯
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop
@section('script')
    <script type="text/javascript">
        HideSidebar();
        $(document).ready(InitialDate());

        //textarea show all text
        $(function() {
            $('#tbody_data textarea').each(function() {
                $(this).height($(this).prop('scrollHeight'));
            });
        });

        function InitialDate() {
            var today = new Date();
            var halfMonthAgo = today.addDays(-15);

            if ($("#input_start_date").val() == '') {
                $("#input_start_date").val(DateToString(halfMonthAgo));
            }
            if ($("#input_end_date").val() == '') {
                $("#input_end_date").val(DateToString(today));
            }
        }
        //匯出檔名修正
        $("#btn_export").click(function() {
            var name = '';
            var sDate = $("#input_start_date").val().replaceAll('-', '');
            var eDate = $("#input_end_date").val().replaceAll('-', '');
            name = sDate + '_' + eDate + '客訴單';

            $("input[name='export_name']").val(name);
        });

        //新增客訴單畫面開啟
        $('#modal_create').on('show.bs.modal', function(e) {
            $('#input_order_no').val('');
            $('#div_order_data').hide();
            $('#div_order_msg').hide();
            $('#div_create_form').hide();
            $('#btn_create').hide();
            $('#div_create_customer').hide();
        });
        //訂單資料帶入
        $('#btn_search_order').on('click', function() {
            $('#btn_search_order').hide();
            $('#btn_search_order_loading').show();
            order_no = $('#input_order_no').val();
            var type = $('#select_type').val();
            url = "{{ route('customer_complaint.ajax_get_order') }}";
            $.ajax({
                url: url,
                type: "get",
                dataType: 'json',
                data: {
                    order_no: order_no,
                    type: type
                },
                contentType: "application/json",
                success: function(data) {
                    if (!$.isEmptyObject(data)) {
                        $('#td_order_no').text(data.order_no);
                        $('#td_sale_no').text(data.sale_no);
                        $('#td_origin_no').text(data.origin_no);
                        $('#td_source').text(data.source);
                        $('#td_order_remark1').text(data.order_remark1);
                        $('#td_order_remark2').text(data.order_remark2);
                        $('#td_order_remark3').text(data.order_remark3);
                        $('#td_order_remarkInside').text(data.order_remarkInside);
                        $('#td_occod').text(data.OCCOD);
                        $('#td_date').text(data.ODATE);
                        var orderDetailDOM = '';
                        $.each(data.OrderDetail, function(index, element) {
                            orderDetailDOM += '<tr>';
                            orderDetailDOM += '<td>' + element.no + '</td>';
                            orderDetailDOM += '<td>' + element.name + '</td>';
                            orderDetailDOM += '<td>' + Math.round(element.amount) + '</td>';
                            orderDetailDOM += '<td>' + Math.round(element.total) + '</td>';
                            orderDetailDOM += '</tr>';
                        });
                        $("#hdf_order_no").val(data.order_no);
                        $("#hdf_origin_no").val(data.origin_no);
                        $('#tbody_order_detail').html(orderDetailDOM);
                        $('#div_order_data').show();
                        $('#div_order_msg').hide();
                        $('#div_create_form').show();
                        $('#btn_create').show();
                        $('#div_create_customer').show();
                    } else {
                        $('#div_order_data').hide();
                        $('#div_order_msg').text("找不到訂單資料");
                        $('#div_order_msg').show();
                        $('#div_create_form').hide();
                        $('#btn_create').hide();
                        $('#div_create_customer').hide();
                    }
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    var msg = JSON.parse(xhr.responseText).msg;
                    $('#div_order_data').hide();
                    $('#div_order_msg').text(msg);
                    $('#div_order_msg').show();
                    $('#div_create_form').hide();
                    $('#btn_create').hide();
                    $('#div_create_customer').hide();
                },
                complete: function() {
                    $('#btn_search_order').show();
                    $('#btn_search_order_loading').hide();
                }
            });
        });
        //編輯客訴單
        $('#modal_edit').on('show.bs.modal', function(event) {
            var button = $(event.relatedTarget);
            var data = button.closest("tr").data();
            var modal_content = $(this).find(".modal-content");

            modal_content.find("input[name='id']").val(data.id);
            modal_content.find("input[name='order_no']").val(data.order_no);
            modal_content.find("input[name='origin_no']").val(data.origin_no);
            modal_content.find("select[name='status']").val(data.status);
            modal_content.find("textarea[name='remark']").val(data.remark);
            modal_content.find("input[name='urgent']").prop('checked', data.urgent == 1);
            //特殊註記帶入
            const marks = String(data.mark).split(',');
            $(modal_content.find("input[name='marks[]']")).each(function(index, element) {
                var val = $(element).val();
                $(element).prop('checked', marks.indexOf(val) != -1);
            });
        });
        //狀態不篩選
        $('#div_status,#div_mark').on('click', function() {
            $('#tbody_data > tr').show();
        });
        //狀態篩選
        $('#ul_status>li[data-status]').on('click', function() {
            var status = $(this).data('status');
            if (status == "undefined") {
                $('#tbody_data > tr').show();
            } else {
                $('#tbody_data > tr').each(function(index, element) {
                    if (status == $(element).data('status')) {
                        $(element).show();
                    } else {
                        $(element).hide();
                    }
                });
            }
        });
        //特殊註記篩選
        $('#ul_mark>li[data-mark]').on('click', function() {
            var mark = $(this).data('mark');
            if (mark == "undefined") {
                $('#tbody_data > tr').show();
            } else {
                $('#tbody_data > tr').each(function(index, element) {
                    if (String($(element).data('mark')).indexOf(mark) != -1) {
                        $(element).show();
                    } else {
                        $(element).hide();
                    }
                });
            }
        });
        $('#btn_update_status').click(function() {
            var selected_order = $('#table_data>tbody>tr .confirm:checked');
            if (selected_order.length == 0) {
                alert('請勾選訂單');
                return false;
            } else {
                var ids = new Array();
                $(selected_order).each(function(i, e) {
                    ids.push($(e).closest('tr').data().id);
                });
                $('#input_status_ids').val(JSON.stringify(ids));
            }
        });
    </script>
@stop
