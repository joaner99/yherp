@extends('layouts.base')
@section('title')
    {{ $title ?? '' }}
@stop
@section('content')
    <div class="container-fluid">
        <div class="FrozenTable" style="max-height: 90vh; font-size:0.85rem;">
            <table class="table table-bordered table-hover table-filter">
                <thead>
                    <tr>
                        <th scope="col">序</th>
                        <th scope="col">銷貨單號</th>
                        <th scope="col">訂單單號</th>
                        <th scope="col">平台單號</th>
                        <th scope="col" class="filter-col">平台名稱</th>
                        <th scope="col">物流單號</th>
                        <th scope="col">聯絡人</th>
                        <th scope="col">電話</th>
                        <th scope="col">備註1</th>
                        <th scope="col">備註2</th>
                        <th scope="col">備註3</th>
                        <th scope="col">內部備註</th>
                        <th scope="col" style="width: 4%;">明細</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i = 0; ?>
                    @if (!empty($data))
                        @foreach ($data as $value)
                            <tr>
                                <th scope="row">{{ $i = $i + 1 }}</td>
                                <td>{{ $value->PCOD1 }}</td>
                                <td>{{ $value->PCOD2 }}</td>
                                <td>{{ $value->PJONO }}</td>
                                <td>{{ $value->PPNAM }}</td>
                                <td>{{ $value->ConsignTran }}</td>
                                <td>{{ $value->PCMAN }}</td>
                                <td>{{ $value->PTELE }}</td>
                                <td>{{ $value->PBAK1 }}</td>
                                <td>{{ $value->PBAK2 }}</td>
                                <td>{{ $value->PBAK3 }}</td>
                                <td>{{ $value->InsideNote }}</td>
                                <td>
                                    <a href="{{ route('check_log_detail', $value->PCOD1) }}">明細</a>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@stop
