@extends('layouts.base')
@section('title')
    折讓清單
@stop
@section('css')
    <style type="text/css">
        table.inner-table>thead>tr>th:nth-child(n+2),
        table.inner-table>tbody>tr>td:nth-child(n+2) {
            width: 90px;
        }

        #table_main>tbody>tr {
            cursor: pointer;
        }

        /*外掛退貨單明細*/
        #table_detail .inner-table>thead>tr>th:nth-child(n+2),
        #table_detail .inner-table>tbody>tr>td:nth-child(n+2) {
            width: 100px;
        }

        .return_active {
            background-color: #007bff;
            color: #fff;
        }
    </style>
@endsection
@section('content')
    {{-- Modal 上傳 --}}
    <div id="modal_upload" class="modal">
        <div class="modal-dialog modal-dialog-centered modal-xl">
            <div class="modal-content">
                <form action="{{ route('credit_note.import') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title">上傳</h5>
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">光貿發票報表</span>
                            </div>
                            <div class="custom-file">
                                <input class="custom-file-input" id="input_file" name="file" type="file"
                                    accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel"
                                    required>
                                <label class="custom-file-label" for="input_file">請選擇檔案</label>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button id="btn_import" type="submit" class="btn btn-primary">確認</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">關閉</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="modal_order" class="modal">
        <div class="modal-dialog modal-dialog-centered"style="min-width: 99vw;">
            <div class="modal-content">
                <div class="modal-header bg-warning text-dark">
                    <h5 class="modal-title">未建立TMS退貨單</h5>
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-row">
                        <div class="col text-danger">※請務必在TMS的【客戶訂單】上填入退貨單號，才能匹配※</div>
                    </div>
                    <div class="form-row">
                        <div class="col-3">
                            <div class="FrozenTable" style="max-height: 70vh;">
                                <table id="table_main" class="table table-bordered">
                                    <caption>主檔</caption>
                                    <thead>
                                        <tr>
                                            <th style="width: 30%;">外掛退貨單號</th>
                                            <th style="width: 25%;">總營業稅額</th>
                                            <th>總未稅總計</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($undone as $return_no => $items)
                                            <tr class="return_main" data-return_no="{{ $return_no }}">
                                                <td>{{ $return_no }}</td>
                                                <td>{{ $items['sum_tax'] }}</td>
                                                <td>{{ $items['sum_total'] }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col">
                            <div class="FrozenTable" style="max-height: 70vh;">
                                <table id="table_detail" class="table table-bordered">
                                    <caption>外掛退貨單號【<span class="font-weight-bolder"></span>】</caption>
                                    <thead>
                                        <tr>
                                            <th>品名</th>
                                            <th class="p-0" style="width: 225px;">
                                                <table class="inner-table">
                                                    <thead>
                                                        <tr>
                                                            <th>未稅單價</th>
                                                            <th>數量</th>
                                                        </tr>
                                                    </thead>
                                                </table>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($undone as $return_no => $items)
                                            @foreach ($items['items'] as $item_name => $item)
                                                <tr data-return_no="{{ $return_no }}" style="display: none;">
                                                    <td class="text-left">{{ $item_name }}</td>
                                                    <td class="p-0">
                                                        <table class="inner-table">
                                                            <tbody>
                                                                @foreach ($item as $unit => $qty)
                                                                    <tr class="{{ $unit < 0 ? 'table-danger' : '' }}">
                                                                        <td class="text-right">{{ $unit }}</td>
                                                                        <td class="text-right">{{ $qty }}</td>
                                                                    </tr>
                                                                @endforeach
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">關閉</button>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <form action="" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-row">
                <div class="col-auto">
                    <button class="btn btn-info" type="button" data-toggle="modal" data-target="#modal_upload">
                        <i class="bi bi-cloud-upload"></i>上傳
                    </button>
                </div>
                @can('credit_note_confirm')
                    <div class="col-auto">
                        <button id="btn_confirm" class="btn btn-primary" type="submit">
                            <i class="bi bi-check2-square"></i>覆核折讓單
                        </button>
                    </div>
                @endcan
                <div class="col-auto">
                    <button id="btn_create_return" class="btn btn-success" type="submit">
                        <i class="bi bi-plus-lg"></i>建立外掛退貨單
                    </button>
                </div>
                <div class="col-auto">
                    <button class="btn btn-warning" type="button" data-toggle="modal" data-target="#modal_order">
                        <i class="bi bi-exclamation-triangle-fill"></i>未建立TMS退貨單【{{ count($undone) }}】
                    </button>
                </div>
            </div>
            <div class="form-row mt-2">
                <div class="col">
                    <div class="FrozenTable" style="max-height: 86vh; font-size: 0.85rem;">
                        <table class="table table-bordered sortable table-filter" style="min-width: 120vw;">
                            <thead>
                                <tr>
                                    <th class="sort-none"><input id="chk_all" type="checkbox"></th>
                                    <th class="filter-col" style="width: 10%;">狀態</th>
                                    <th class="filter-col" style="width: 5%;">TMS退貨單號</th>
                                    <th class="filter-col" style="width: 5%;">外掛退貨單號</th>
                                    <th style="width: 12%;">折讓單號碼</th>
                                    <th class="filter-col" style="width: 5%;">折讓日期</th>
                                    <th class="filter-col" style="width: 5%;">原發票號碼</th>
                                    <th class="filter-col" style="width: 5%;">原發票日期</th>
                                    <th style="width: 5%;">營業稅額</th>
                                    <th style="width: 5%;">未稅總計</th>
                                    <th class="p-0">
                                        <table class="inner-table">
                                            <thead>
                                                <tr>
                                                    <th>品名</th>
                                                    <th>數量</th>
                                                    <th>未稅單價</th>
                                                    <th>未稅小計</th>
                                                    <th>營業稅額</th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (!empty($data))
                                    @foreach ($data as $cn_item)
                                        <tr class="{{ $cn_item->total > 3000 ? 'table-danger' : '' }}">
                                            <td>
                                                @if (empty($cn_item->return_no))
                                                    <input name="ids[]" class="confirm" type="checkbox"
                                                        value="{{ $cn_item->id }}">
                                                @endif
                                            </td>
                                            <td>{{ $cn_item->status_name }}</td>
                                            <td>
                                                @if (!empty($cn_item->return_no))
                                                    {{ $cn_item->Poseou->order_no ?? '' }}
                                                @endif
                                            </td>
                                            <td>{{ $cn_item->return_no }}</td>
                                            <td>{{ $cn_item->order_no }}</td>
                                            <td>{{ $cn_item->order_date }}</td>
                                            <td>{{ $cn_item->invoice_no }}</td>
                                            <td>{{ $cn_item->invoice_date }}</td>
                                            <td class="text-right">{{ number_format($cn_item->tax, 2) }}</td>
                                            <td class="text-right">{{ number_format($cn_item->total, 2) }}</td>
                                            <td class="p-0">
                                                @if (!empty($cn_item->Details))
                                                    <table class="inner-table">
                                                        <tbody>
                                                            @foreach ($cn_item->Details as $d)
                                                                <tr>
                                                                    <td class="text-left">{{ $d->item_name }}</td>
                                                                    <td class="text-right">{{ $d->qty }}</td>
                                                                    <td class="text-right">
                                                                        {{ number_format($d->unit, 2) }}
                                                                    </td>
                                                                    <td class="text-right">
                                                                        {{ number_format($d->total, 2) }}
                                                                    </td>
                                                                    <td class="text-right">{{ number_format($d->tax, 2) }}
                                                                    </td>
                                                                </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </form>
    </div>
@stop
@section('script')
    <script type="text/javascript">
        //上傳檔案後顯示文件名稱
        $('#input_file').change(function(e) {
            var fileName = e.target.files[0].name;
            $(this).next('.custom-file-label').html(fileName);
        });
        //上傳
        $('#btn_import').click(function() {
            lockScreen();
        })
        //全選功能
        $('#chk_all').change(check_all);
        $('.return_main').click(function() {
            var return_no = $(this).data().return_no;
            $('#table_detail>caption>span').text(return_no);
            $('#table_main>tbody>tr.return_active').removeClass('return_active');
            $(this).addClass('return_active');
            $('#table_detail>tbody>tr').hide();
            $('#table_detail>tbody>tr[data-return_no="' + return_no + '"]').show();
        });
        //覆核折讓單
        $('#btn_confirm').click(function() {
            $(this).closest('form').attr('action', "{{ route('credit_note.confirm') }}");
        });
        //建立外掛退貨單
        $('#btn_create_return').click(function() {
            $(this).closest('form').attr('action', "{{ route('credit_note.create_return') }}");
        });
    </script>
@stop
