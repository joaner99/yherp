@extends('layouts.base')
@section('title')
    庫存異常清單
@stop
@section('content')
    <div class="container-fluid">
        <div class="form-row">
            <div class="col-auto">
                <ul class="pagination m-0">
                    <li class="page-item active" data-target="#div_stock_error">
                        <button class="page-link" type="button">庫存負數</button>
                    </li>
                    <li class="page-item" data-target="#div_stock_diff">
                        <button class="page-link" type="button">庫存不同</button>
                    </li>
                </ul>
            </div>
        </div>
        <div id="div_stock_error" class="form-row mt-2">
            <div class="col">
                @if (empty($data))
                    <div class="alert alert-danger">
                        找不到庫存異常資料
                    </div>
                @else
                    <div class="FrozenTable" style="max-height: 86vh;">
                        <?php $tableIndex = 0; ?>
                        <table class="table table-bordered table-hover sortable">
                            <caption>庫存為負數異常</caption>
                            <thead>
                                <tr>
                                    <th style="width: 5%;">序</th>
                                    <th style="width: 10%;">料號</th>
                                    <th>品名</th>
                                    <th style="width: 10%;">倉別</th>
                                    <th style="width: 10%;">庫存數</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($data as $value)
                                    <tr>
                                        <td>{{ ++$tableIndex }}</td>
                                        <td>
                                            <a
                                                href="{{ route('items.log', ['id' => $value->item_no]) }}">{{ $value->item_no }}</a>
                                        </td>
                                        <td class="text-left">{{ $value->Item->item_name }}</td>
                                        <td>{{ $value->stock_name }}</td>
                                        <td>{{ number_format($value->qty) }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                @endif
            </div>
        </div>
        <div id="div_stock_diff" class="form-row mt-2" style="display: none;">
            <div class="col">
                @if (empty($diff_data))
                    <div class="alert alert-danger">
                        找不到庫存不同步資料
                    </div>
                @else
                    <div class="FrozenTable" style="max-height: 86vh;">
                        <?php $tableIndex = 0; ?>
                        <table class="table table-bordered table-hover sortable table-filter">
                            <caption>外掛庫存與TMS庫存不同步</caption>
                            <thead>
                                <tr>
                                    <th style="width: 5%;">序</th>
                                    <th class="filter-col" style="width: 10%;">料號</th>
                                    <th>品名</th>
                                    <th class="filter-col">倉別</th>
                                    <th style="width: 10%;">外掛庫存</th>
                                    <th style="width: 10%;">TMS庫存</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($diff_data as $item)
                                    <tr>
                                        <td>{{ ++$tableIndex }}</td>
                                        <td>{{ $item['item_no'] }}</td>
                                        <td class="text-left">{{ $item['item_name'] }}</td>
                                        <td>{{ $item['stock_name'] }}</td>
                                        <td>{{ number_format($item['stock']) }}</td>
                                        <td>
                                            <a
                                                href="{{ route('items.log', ['id' => $item['item_no']]) }}">{{ number_format($item['tms_stock']) }}</a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                @endif
            </div>
        </div>
    </div>
@stop
