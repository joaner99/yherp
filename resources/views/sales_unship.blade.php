@extends('layouts.base')
@section('title')
    未出貨
@stop
@section('content')
    <div class="container">
        <div class="FrozenTable" style="max-height: 92vh;">
            <table class="table table-bordered table-hover sortable">
                <thead>
                    <tr>
                        <th>序</th>
                        <th>銷貨單號</th>
                        <th>原始訂單編號</th>
                        <th>客戶代碼</th>
                        <th>銷貨日期</th>
                        <th>重物</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i = 1; ?>
                    @if (!empty($data))
                        @foreach ($data as $item)
                            <tr>
                                <td>{{ $i++ }}</td>
                                <td>{{ $item->PCOD1 }}</td>
                                <td>{{ $item->PJONO }}</td>
                                <td>{{ $item->PPCOD }}</td>
                                <td>{{ $item->PDAT1 }}</td>
                                <td>{{ $item->HEAVY == '1' ? '✔' : '' }}</td>
                            </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@stop
