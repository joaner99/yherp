@extends('layouts.base')
@section('title')
    親送司機各地區品項統計
@stop
@section('css')
    <style type="text/css">
    </style>
@endsection
@section('content')
    <div class="container-fluid">
        <div class="form-row">
            <div class="col-auto">
                <form action="{{ route('export_table') }}" method="post">
                    @csrf
                    <input name="export_name" type="hidden" value="123456789">
                    <input name="export_data" type="hidden">
                    <button id="btn_export" class="btn btn-success btn-sm" type="submit" export-target="#table_data">
                        <i class="bi bi-file-earmark-excel"></i>匯出Excel
                    </button>
                </form>
            </div>
        </div>
        <div class="form-row">
            <div class="col">
                <div class="FrozenTable" style="max-height: 100%;">
                    <table id="table_data" class="table table-bordered">
                        <thead>
                            <tr>
                                <th></th>
                                @foreach (['北', '中', '南', '其他'] as $text)
                                    <th colspan="4">{{ $text }}</th>
                                @endforeach
                            </tr>
                            <tr>
                                <th>司機</th>
                                @foreach (['北', '中', '南', '其他'] as $item)
                                    @foreach (['SPC', 'LVT', '塑木', '貓砂'] as $text)
                                        <th>{{ $text }}</th>
                                    @endforeach
                                @endforeach
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $driver => $d)
                                <tr>
                                    <td>{{ $driver }}</td>
                                    @foreach (['北', '中', '南', '其他'] as $area)
                                        @foreach (['SPC', 'LVT', '塑木', '貓砂'] as $kind)
                                            <td>{{ $d[$area][$kind] ?? 0 }}</td>
                                        @endforeach
                                    @endforeach
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop
@section('script')
    <script type="text/javascript"></script>
@stop
