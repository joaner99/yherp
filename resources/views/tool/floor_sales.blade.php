@extends('layouts.base')
@section('title')
    地板銷售計算器
@stop
@section('css')
    <style type="text/css">
        .name {
            font-size: 0.85rem;
            text-align: left !important;
        }
    </style>
@endsection
@section('content')
    <div class="container-fluid">
        <div class="form-row">
            <div class="col-auto">
                <form action="{{ route('tool.floor_sales') }}" method="get">
                    <div class="form-row">
                        <div class="form-group col-auto">
                            <label for="input_start_date">起日</label>
                            <input id="input_start_date" name="start_date" class="form-control" type="date"
                                value="{{ old('start_date') }}" required>
                        </div>
                        <div class="form-group col-auto">
                            <label for="input_end_date">訖日</label>
                            <input id="input_end_date" name="end_date" class="form-control" type="date"
                                value="{{ old('end_date') }}" required>
                        </div>
                    </div>
                    <button class="btn btn-primary" type="submit">查詢</button>
                </form>
            </div>
        </div>
        <div class="form-row mt-2">
            <div class="col">
                @if (empty($data) || count($data) == 0)
                    <div class="alert alert-danger">
                        找不到資料
                    </div>
                @else
                    <div class="FrozenTable">
                        <table class="table table-bordered sortable">
                            <thead>
                                <tr>
                                    <th style="width: 5%;">地板</th>
                                    <th>品項</th>
                                    <th style="width: 15%;">(網路+POS)銷售數量</th>
                                    <th style="width: 15%;">變數</th>
                                    <th style="width: 15%;">結果</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($data as $type => $qty)
                                    <tr>
                                        @switch($type)
                                            @case('014')
                                                <td>SPC</td>
                                            @break

                                            @case('046')
                                                <td>LVT</td>
                                            @break
                                        @endswitch
                                        <td class="name">
                                            @foreach ($floors->where('ITCO3', $type) as $floor)
                                                {{ "【{$floor->ICODE}】{$floor->INAME}" }}<br>
                                            @endforeach
                                        </td>
                                        <td class="qty">{{ number_format($qty) }}</td>
                                        <td><input class="form-control variable" type="number"></td>
                                        <td class="ans"></td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                @endif
            </div>
        </div>
    </div>
@stop
@section('script')
    <script type="text/javascript">
        $(document).ready(function() {
            InitialDate();
        });

        function InitialDate() {
            var today = new Date();
            var oneMonthAgo = today.addDays(-30);

            if ($("#input_start_date").val() == '') {
                $("#input_start_date").val(DateToString(oneMonthAgo));
            }
            if ($("#input_end_date").val() == '') {
                $("#input_end_date").val(DateToString(today));
            }
        }
        //計算結果
        $('.variable').on('change', function() {
            var val = $(this).val();
            var tr = $(this).closest('tr');
            var qty = CustomerToNumber($(tr).find('.qty').text());
            $(tr).find('.ans').text(toThousands(qty * val));
        });
    </script>
@stop
