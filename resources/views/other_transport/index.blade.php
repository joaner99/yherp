@extends('layouts.base')
@section('title')
    人工總表
@stop
@section('css')
    <style type="text/css">
        .list-group-item {
            padding: 0.375rem 0.75rem;
        }

        table.inner-table>thead>tr>th:nth-child(odd),
        table.inner-table>tbody>tr>td:nth-child(odd) {
            width: 90px;
        }

        .order_font {
            font-family: monospace;
        }
    </style>
@endsection
@section('content')
    <!-- Modal 統計 -->
    <div id="modal_total" class="modal fade">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="modal-title">
                        <h1>統計</h1>
                    </div>
                    <button type="button" class="close" data-dismiss="modal">
                        <span>&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-row">
                        <div class="col">
                            <div class="FrozenTable" style="max-height: 71vh;">
                                <table class="table table-bordered sortable table-filter  table-hover">
                                    <caption>數量統計</caption>
                                    <thead>
                                        <tr>
                                            <th>品名\小類</th>
                                            @foreach ($statistics_columns as $col)
                                                <th>{{ $col }}</th>
                                            @endforeach
                                        </tr>
                                    </thead>
                                    <tbody class="statistics">
                                        @foreach ($statistics as $item_name => $item)
                                            <tr>
                                                <td class="text-right">{{ $item_name }}</td>
                                                @foreach ($statistics_columns as $col)
                                                    <td>{{ $item[$col] ?? '' }}</td>
                                                @endforeach
                                            </tr>
                                        @endforeach
                                    </tbody>
                                    <tfoot>
                                        @foreach ($statistics_total as $item_name => $item)
                                            <tr>
                                                <td class="text-right">{{ $item_name }}</td>
                                                @foreach ($statistics_columns as $col)
                                                    <td>{{ $item[$col] ?? '' }}</td>
                                                @endforeach
                                            </tr>
                                        @endforeach
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">關閉</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal 編輯 -->
    <div id="modal_edit" class="modal fade">
        <div class="modal-dialog modal-xl modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="modal-title">
                        <h1>編輯</h1>
                    </div>
                    <button type="button" class="close" data-dismiss="modal">
                        <span>&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-row">
                        <div class="col-auto">
                            <div class="form-group">
                                <label for="input_original_no">原始訂單編號</label>
                                <input class="form-control" id="input_original_no" name="original_no" type="text"
                                    value="" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-3">
                            <label>預排日期</label>
                            <input class="form-control" name="ship_date" type="date">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col">
                            <label>人工備註</label>
                            <label class="text-success ml-2">※200字以內</label>
                            <textarea class="form-control" name="remark" style="height: 100px;"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="form-row w-100">
                        <div class="col"></div>
                        <div class="col-auto">
                            <button id="btn_edit_confirm" class="btn btn-primary" type="button">儲存</button>
                        </div>
                        <div class="col-auto">
                            <button class="btn btn-secondary" type="button" data-dismiss="modal">取消</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="form-row">
            <div class="col-auto">
                <form action="{{ route('other_transport.index') }}" method="GET">
                    <input name="unsched" type="hidden" value="1">
                    <button class="btn btn-secondary" type="submit">查詢未安排</button>
                </form>
            </div>
            <div class="col-auto">
                <form action="{{ route('other_transport.index') }}" method="GET">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">指定日期</span>
                        </div>
                        <input id="input_date" name="date" class="form-control" type="date"
                            value="{{ old('date') }}">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <input id="input_include_ship" type="checkbox" name="include_ship" value="1"
                                    {{ old('include_ship') ? 'checked' : '' }}>
                                <label class="ml-2 m-0" for="input_include_ship">包含已上車</label>
                            </div>
                        </div>
                        <div class="input-group-append">
                            <button class="btn btn-info" type="submit">查詢</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col"></div>
            <div class="col-auto">
                <ul class="list-group list-group-horizontal">
                    <li class="list-group-item bg-info text-white">單數：{{ count($data) }}</li>
                </ul>
            </div>
            <div class="col-auto">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal_total">
                    <i class="bi bi-bar-chart"></i>統計
                </button>
            </div>
            <div class="col"></div>
            @can('other_transport')
                <div class="col-auto">
                    <form action="{{ route('order_sample.confirm') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <button id="btn_confirm" class="btn btn-primary" type="submit">
                            <i class="bi bi-check2-square"></i>業務確認訂單
                        </button>
                        <input name="id_list" type="hidden">
                    </form>
                </div>
            @endcan
        </div>
        <div class="form-row mt-2">
            <div class="col">
                <div class="FrozenTable" style="max-height: 87vh; font-size: 0.85rem;">
                    <table id="table_data" class="table table-bordered sortable table-filter">
                        <caption>{{ $caption }}</caption>
                        <thead>
                            <tr>
                                @can('other_transport')
                                    <th style="width: 1%;" class="export-none sort-none">
                                        <input id="chk_all" type="checkbox">
                                    </th>
                                    <th class="export-none" style="width: 6%;">操作</th>
                                @endcan
                                <th class="filter-col" style="width: 6%;">預排日期</th>
                                <th class="filter-col" style="width: 4%;">棧板</th>
                                <th class="filter-col" style="width: 10%;">原始訂單編號</th>
                                <th style="width: 7%;">訂單單號</th>
                                <th style="width: 7%;">銷貨單號</th>
                                <th>人工備註</th>
                                <th class="p-0" style="width: 25%;">
                                    <table class="inner-table">
                                        <thead>
                                            <tr>
                                                <th>類型</th>
                                                <th>品名</th>
                                                <th>數量</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach (collect($data)->groupBy('original_no') as $original_no => $group)
                                @foreach ($group as $value)
                                    <tr class="{{ $value['is_pallet'] ? 'table-warning' : '' }}"
                                        data-original_no="{{ $value['original_no'] }}"
                                        data-ship_date="{{ $value['ship_date'] }}" data-remark="{{ $value['remark'] }}"
                                        data-unconfirmed_id="{{ $value['unconfirmed_id'] ?? '' }}">
                                        @can('other_transport')
                                            <td>
                                                @if (!empty($value['unconfirmed_id']))
                                                    <input class="confirm" type="checkbox">
                                                @endif
                                            </td>
                                            <td>
                                                <button class="btn btn-warning btn-sm" data-toggle="modal"
                                                    data-target="#modal_edit">
                                                    <i class="bi bi-pencil-fill"></i>編輯
                                                </button>
                                            </td>
                                        @endcan
                                        <td name="ship_date">{{ $value['ship_date'] }}</td>
                                        <td class="font-weight-bolder" style="font-size: 1rem;">
                                            {{ $value['is_pallet'] ? '✔' : '' }}</td>
                                        <td>
                                            <a class="order_font"
                                                href="{{ route('basket_detail') . '?order_no=' . urlencode($value['original_no']) }}">
                                                {{ $value['original_no'] }}
                                            </a>
                                        </td>
                                        <td>
                                            @foreach ($value['order_no'] as $order_no)
                                                <div>
                                                    <a href="{{ route('order_detail', $order_no) }}">
                                                        {{ $order_no }}
                                                    </a>
                                                </div>
                                            @endforeach
                                        </td>
                                        <td>
                                            @foreach ($value['sale_no'] as $sale_no)
                                                <div>
                                                    <a href="{{ route('check_log_detail', $sale_no) }}">
                                                        {{ $sale_no }}
                                                    </a>
                                                </div>
                                            @endforeach
                                        </td>
                                        <td class="text-left" name="remark">{{ $value['remark'] }}</td>
                                        <td class="p-0">
                                            <table class="inner-table">
                                                <tbody>
                                                    @foreach ($value['detail'] as $item)
                                                        <tr>
                                                            <td>{{ $item['item_kind3_name'] }}</td>
                                                            <td class="text-left">{{ $item['item_name'] }}</td>
                                                            <td>{{ number_format($item['qty']) }}</td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                @endforeach
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop
@section('script')
    <script type="text/javascript">
        HideSidebar();
        $('#chk_all').change(check_all);
        //編輯頁面帶入資料
        $('button[data-target="#modal_edit"]').click(function() {
            var data = $(this).closest('tr').data();
            $.each(data, function(key, value) {
                $('#modal_edit').find('[name="' + key + '"]').val(value);
            });
        });
        //編輯儲存
        $('#btn_edit_confirm').click(function() {
            var data = {};
            $('#modal_edit [name]').each(function(i, e) {
                var key = $(e).attr('name');
                var val = $(e).val();
                if (key == 'cust_confirmed' && $(e).prop('checked') == false) {
                    return;
                }
                data[key] = val;
            });
            lockScreen();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{ route('other_transport.ajax_update') }}",
                type: "post",
                data: data,
                success: function(data) {},
                error: function(xhr, ajaxOptions, thrownError) {
                    var msg = JSON.parse(xhr.responseText).msg;
                    alert(msg);
                },
                complete: function() {
                    var original_no = data.original_no;
                    var tr = $('tr[data-original_no="' + original_no + '"]');
                    $.each(data, function(key, value) {
                        //更新data-
                        $(tr).data(key, value);
                        $(tr).find('[name="' + key + '"]').text(value);
                    });
                    $('#modal_edit').modal('hide');
                    unlockScreen();
                }
            });
        });
        //業務確認訂單
        $("#btn_confirm").click(function() {
            var idList = new Array();
            $("#table_data>tbody>tr .confirm:checked").each(function(index, element) {
                idList.push($(element).closest('tr').data().unconfirmed_id);
            });
            if (idList.length > 0) {
                $('input[name="id_list"]').val(JSON.stringify(idList));
            } else {
                alert('請勾選訂單');
                return false;
            }
        });
    </script>
@stop
