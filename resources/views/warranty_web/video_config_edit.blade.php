@extends('layouts.base')
@section('title')
    編輯影片
@stop
@section('content')
    <div class="container-fluid">
        @if ($errors->any())
            <div class="form-row mb-2">
                <div class="col-auto">
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        @endif
        <div class="form-row">
            <div class="col-auto">
                <a class="btn btn-secondary" href="{{ route('warranty_web.video_config') }}">回上頁</a>
            </div>
        </div>
        <form method="POST" action="{{ route('warranty_web.video_config_update', $data->id) }}">
            @method('PATCH')
            @csrf
            <div class="card mt-2">
                <h1 class="card-header text-info font-weight-bold">影片設定</h1>
                <div class="card-body">
                    <div class="form-row mt-2">
                        <div class="col-lg-2">
                            <div class="form-group">
                                <label>ID</label>
                                <input class="form-control"type="text" value="{{ $data->id }}" maxlength="11"
                                    readonly>
                            </div>
                        </div>
                        <div class="col-lg-10">
                            <div class="form-group">
                                <label for="input_title">影片標題<label class="text-muted m-0">(50字)</label></label>
                                <input class="form-control" id="input_title" name="title" type="text"
                                    value="{{ $data->title }}" maxlength="50">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card mt-2">
                <h1 class="card-header text-info font-weight-bold">關聯設定</h1>
                <div class="card-body">
                    <button class="btn btn-info" type="button" id="btn_create">
                        <i class="bi bi-plus-lg"></i>新增關聯
                    </button>
                    <div class="FrozenTable mt-2" style="max-height: 40vh;">
                        <table class="table table-bordered sortable">
                            <thead>
                                <tr>
                                    <th style="width: 20%;">料號</th>
                                    <th style="width: 20%;">大類</th>
                                    <th style="width: 20%;">中類</th>
                                    <th style="width: 20%;">小類</th>
                                    <th style="width: 10%;">操作</th>
                                </tr>
                            </thead>
                            <tbody id="tbody_items">
                                @if (!empty($data->Details))
                                    @foreach ($data->Details as $detail)
                                        <tr data-id="{{ $detail->id }}">
                                            <td>
                                                <input class="form-control item_no" type="text" list="itemList"
                                                    value="{{ $detail->item_no }}">
                                            </td>
                                            <td>
                                                <input class="form-control item_kind" type="text" list="kindList"
                                                    value="{{ $detail->item_kind }}">
                                            </td>
                                            <td>
                                                <input class="form-control item_kind2" type="text" list="kind2List"
                                                    value="{{ $detail->item_kind2 }}">
                                            </td>
                                            <td>
                                                <input class="form-control item_kind3" type="text" list="kind3List"
                                                    value="{{ $detail->item_kind3 }}">
                                            </td>
                                            <td>
                                                <button class="btn btn-danger btn-sm" type="button"
                                                    onclick="ItemDelete(this)">
                                                    <i class="bi bi-trash"></i>刪除
                                                </button>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <input id="input_details" name="details" type="hidden">
            <button class="btn btn-primary mt-2" id="btn_confirm" type="submit">確認</button>
        </form>
    </div>
    @if (!empty($items) && count($items) > 0)
        <datalist id="itemList">
            @foreach ($items as $item)
                <option value="{{ $item->ICODE }}">{{ $item->INAME }}</option>
            @endforeach
        </datalist>
    @endif
    @if (!empty($type) && count($type) > 0)
        <datalist id="kindList">
            @foreach ($type[1] as $key => $name)
                <option value="{{ $key }}">{{ $name }}</option>
            @endforeach
        </datalist>
        <datalist id="kind2List">
            @foreach ($type[2] as $key => $name)
                <option value="{{ $key }}">{{ $name }}</option>
            @endforeach
        </datalist>
        <datalist id="kind3List">
            @foreach ($type[3] as $key => $name)
                <option value="{{ $key }}">{{ $name }}</option>
            @endforeach
        </datalist>
    @endif
@stop
@section('script')
    <script type="text/javascript">
        //去空白
        $(function() {
            $('input[type="text"]').change(function() {
                this.value = $.trim(this.value);
            });
        });
        //確認
        $("#btn_confirm").click(function() {
            var configList = new Array();
            $("#tbody_items>tr").each(function(index, element) {
                var id = $(element).data().id;
                var item_no = $(element).find(".item_no").val();
                var item_kind = $(element).find(".item_kind").val();
                var item_kind2 = $(element).find(".item_kind2").val();
                var item_kind3 = $(element).find(".item_kind3").val();
                var config = {
                    id: id,
                    item_no: item_no,
                    item_kind: item_kind,
                    item_kind2: item_kind2,
                    item_kind3: item_kind3,
                };
                configList.push(config);
            });
            $("#input_details").val(JSON.stringify(configList));
        });
        //建立參數
        $("#btn_create").click(function() {
            var dom = '';
            dom += '<tr data-id="">';
            dom +=
                '<td><input class="form-control item_no" type="text" list="itemList"></td>';
            dom +=
                '<td><input class="form-control item_kind" type="text" list="kindList"></td>';
            dom +=
                '<td><input class="form-control item_kind2" type="text" list="kind2List"></td>';
            dom +=
                '<td><input class="form-control item_kind3" type="text" list="kind3List"></td>';
            dom +=
                '<td><button class="btn btn-danger btn-sm" type="button"onclick="ItemDelete(this)"><i class="bi bi-trash"></i>刪除</button></td>';
            dom += '</tr>';

            $("#tbody_items").append(dom);
            $("#tbody_items>tr:last-child>td:eq(0)>input").focus();
        });
        //明細刪除按鈕
        function ItemDelete(obj) {
            $(obj).closest('tr').remove();
        }
    </script>
@stop
