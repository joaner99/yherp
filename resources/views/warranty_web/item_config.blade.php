@extends('layouts.base')
@section('title')
    保固設定
@stop
@section('content')
    {{-- Modal 上傳 --}}
    <div id="modal_upload" class="modal">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <form action="{{ route('warranty_web.item_import') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title">匯入</h5>
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">商品保固設定表</span>
                            </div>
                            <div class="custom-file">
                                <input class="custom-file-input" id="input_file" name="file" type="file"
                                    accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel"
                                    required>
                                <label class="custom-file-label" for="input_file">請選擇檔案</label>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button id="btn_import" type="submit" class="btn btn-primary">確認</button>
                        <button id="btn_loading" type="button" class="btn btn-primary" style="display: none;" disabled>
                            <div class="spinner-border spinner-border-sm">
                                <span class="sr-only"></span>
                            </div>
                            <span>匯入中...</span>
                        </button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">關閉</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="form-row mb-2">
            <div class="col-auto">
                <form action="{{ route('export_table') }}" method="post">
                    @csrf
                    <input name="export_name" type="hidden" value="商品保固設定表">
                    <input name="export_data" type="hidden">
                    <button class="btn btn-success" type="submit" export-target="#table_data">
                        <i class="bi bi-box-arrow-up"></i>匯出
                    </button>
                </form>
            </div>
            <div class="col-auto">
                <button class="btn btn-info" type="button" data-toggle="modal" data-target="#modal_upload">
                    <i class="bi bi-box-arrow-in-down"></i>匯入
                </button>
            </div>
        </div>
        <div class="form-row mb-2">
            <div class="col">
                <div class="FrozenTable" style="max-height: 85vh;">
                    <table id="table_data" class="table table-bordered table-hover sortable">
                        <thead>
                            <tr>
                                <th class="export-none">序</th>
                                <th>料號</th>
                                <th>品名</th>
                                <th>保固年數</th>
                                <th>保固月數</th>
                                <th>保固天數</th>
                                <th>原廠保固網址</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 1; ?>
                            @if (!empty($data))
                                @foreach ($data as $item)
                                    <tr>
                                        <td>{{ $i++ }}</td>
                                        <td>{{ $item->id }}</td>
                                        <td class="text-left">{{ $item->name }}</td>
                                        <td>{{ $item->warranty_y }}</td>
                                        <td>{{ $item->warranty_m }}</td>
                                        <td>{{ $item->warranty_d }}</td>
                                        <td>
                                            @if (empty($item->warranty_url))
                                                {{ $item->warranty_url }}
                                            @else
                                                <a href="{{ $item->warranty_url }}" target="_blank">
                                                    {{ $item->warranty_url }}
                                                </a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop
@section('script')
    <script type="text/javascript">
        //上傳檔案後顯示文件名稱
        $('#input_file').change(function(e) {
            var fileName = '';
            $(e.target.files).each(function(index, element) {
                if (fileName != '') {
                    fileName += ',';
                }
                fileName += element.name;
            });
            $(this).next('.custom-file-label').html(fileName);
        });
        //匯入
        $("#btn_import").click(function() {
            if (!IsNullOrEmpty($('#input_file').val())) {
                $("#btn_import").hide();
                $("#btn_loading").show();
            }
        });
    </script>
@stop
