@extends('layouts.base')
@section('title')
    新增影片
@stop
@section('content')
    <div class="container-fluid">
        @if ($errors->any())
            <div class="form-row mb-2">
                <div class="col-auto">
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        @endif
        <div class="form-row">
            <div class="col-auto">
                <a class="btn btn-secondary" href="{{ route('warranty_web.video_config') }}">回上頁</a>
            </div>
        </div>
        <form method="POST" action="{{ route('warranty_web.video_config_store') }}">
            @csrf
            <div class="form-row mt-2">
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="input_url">影片網址</label>
                        <div class="input-group">
                            <input class="form-control" id="input_url" type="text">
                            <div class="input-group-append">
                                <button class="btn btn-info" type="button" id="btn_get_id">分析</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="form-group">
                        <label class="text-danger" for="input_id">*ID</label>
                        <input class="form-control" id="input_id" name="id" type="text" maxlength="11" readonly
                            required>
                    </div>
                </div>
            </div>
            <div class="form-row mt-2">
                <div class="col-lg-8">
                    <div class="form-group">
                        <label for="input_title">影片標題<label class="text-muted">(50字)</label></label>
                        <input class="form-control" id="input_title" name="title" type="text" maxlength="50">
                    </div>
                </div>
            </div>
            <hr>
            <button class="btn btn-primary" type="submit">確認</button>
        </form>
    </div>
@stop
@section('script')
    <script type="text/javascript">
        $('#btn_get_id').click(function() {
            var url = $('#input_url').val();
            var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#&?]*).*/;
            var match = url.match(regExp);
            var id = (match && match[7].length == 11) ? match[7] : false;
            if (id !== false) {
                $('#input_id').val(id);
            } else {
                $('#input_id').val('');
                alert('網址無法分析');
            }
        })
    </script>
@stop
