@extends('layouts.base')
@section('title')
    保固查詢
@stop
@section('content')
    <div class="container-fluid">
        <form action="{{ route('warranty_web.search') }}" method="GET">
            <div class="form-row">
                <div class="form-group col-2 mb-2">
                    <label for="input_sale_no">銷貨單號</label>
                    <input id="input_sale_no" name="sale_no" class="form-control" type="text" value="{{ old('sale_no') }}">
                </div>
                <div class="form-group col-2 mb-2">
                    <label for="input_original_no">原始訂單編號</label>
                    <input id="input_original_no" name="original_no" class="form-control" type="text"
                        value="{{ old('original_no') }}">
                </div>
                <div class="form-group col-2 mb-2">
                    <label for="input_user_id">買家ID</label>
                    <input id="input_user_id" name="user_id" class="form-control" type="text"
                        value="{{ old('user_id') }}">
                </div>
                <div class="form-group col-2 mb-2">
                    <label for="input_start_date">銷售起日</label>
                    <input id="input_start_date" name="start_date" class="form-control" type="date"
                        value="{{ old('start_date') }}">
                </div>
                <div class="form-group col-2 mb-2">
                    <label for="input_end_date">銷售訖日</label>
                    <input id="input_end_date" name="end_date" class="form-control" type="date"
                        value="{{ old('end_date') }}">
                </div>
                <div class="form-group col-2 mb-2">
                    <label for="select_customer">客戶簡稱</label>
                    <select id="select_customer" name="customer" class="custom-select">
                        <option value="" selected></option>
                        @if (!empty($customers))
                            @foreach ($customers as $customer)
                                <option value="{{ $customer->CCODE }}"
                                    {{ $customer->CCODE == old('customer') ? 'selected' : '' }}>
                                    {{ $customer->CNAM2 }}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-2 mb-2">
                    <label for="select_tms_sys">TMS系統</label>
                    <select id="select_tms_sys" name="tms_sys" class="form-control">
                        <option value="" {{ empty(old('tms_sys')) ? 'selected="selected"' : '' }}>驗貨
                        </option>
                        <option value="pos" {{ old('tms_sys') == 'pos' ? 'selected="selected"' : '' }}>POS</option>
                    </select>
                </div>
            </div>
            <button class="btn btn-primary btn-sm">查詢</button>
        </form>
        <hr>
        @if (!empty($orders) && count($orders) > 0)
            <div class="form-row">
                <div class="col">
                    <div class="FrozenTable" style="max-height: 68vh;">
                        <table class="table table-bordered table-hover sortable">
                            <thead>
                                <tr>
                                    <th style="width: 10%;">銷貨單號</th>
                                    <th>買家ID</th>
                                    <th style="width: 15%;">原始訂單編號</th>
                                    <th style="width: 10%;">客戶簡稱</th>
                                    <th style="width: 10%;">銷貨日期</th>
                                    <th style="width: 12%;">操作</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($orders as $item)
                                    <tr>
                                        <td>{{ $item->id }}</td>
                                        <td>{{ $item->user_id ?? '' }}</td>
                                        <td>{{ $item->original_no ?? '' }}</td>
                                        <td>{{ $item->sale_src ?? '' }}</td>
                                        <td>{{ $item->sale_date }}</td>
                                        <td>
                                            <div class="d-flex justify-content-center">
                                                <a class="btn btn-primary btn-sm mx-1" href="{{ $csURL . $item->guid }}">
                                                    <i class="bi bi-globe"></i>網站
                                                </a>
                                                <button class="btn btn-success btn-sm mx-1" name="btn_copy" type="button"
                                                    data-csurl="{{ $csURL . $item->guid }}">
                                                    <i class="bi bi-clipboard"></i>複製
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        @endif
    </div>
@stop
@section('script')
    <script type="text/javascript">
        $(document).ready(InitialDate());
        //初始化日期
        function InitialDate() {
            var today = new Date();
            $("#input_start_date,#input_end_date").attr("max", DateToString(today));
        }
        //複製剪貼簿
        $("button[name='btn_copy']").click(function() {
            var value = $(this).data().csurl;

            const el = document.createElement('textarea');
            el.value = value;
            document.body.appendChild(el);
            el.select();
            document.execCommand('copy');
            document.body.removeChild(el);
            alert('已複製到剪貼簿');
        });
    </script>
@stop
