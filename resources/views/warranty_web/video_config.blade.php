@extends('layouts.base')
@section('title')
    影片設定
@stop
@section('content')
    <div class="container-fluid">
        <div class="form-row mb-2">
            <div class="col-auto">
                <a class="btn btn-info" href="{{ route('warranty_web.video_config_create') }}">
                    <i class="bi bi-plus-lg"></i>新增影片
                </a>
            </div>
        </div>
        <div class="form-row">
            <div class="col">
                <div class="FrozenTable" style="max-height: 85vh;">
                    <table class="table table-bordered table-hover sortable">
                        <thead>
                            <tr>
                                <th style="width: 5%;">序</th>
                                <th style="width: 11%;">影片ID</th>
                                <th>影片標題</th>
                                <th style="width: 11%;">操作</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 1; ?>
                            @if (!empty($data))
                                @foreach ($data as $item)
                                    <tr>
                                        <td>{{ $i++ }}</td>
                                        <td>{{ $item->id }}</td>
                                        <td>{{ $item->title }}</td>
                                        <td>
                                            <div class="d-flex justify-content-around">
                                                <a class="btn btn-warning btn-sm my-1"
                                                    href="{{ route('warranty_web.video_config_edit', $item->id) }}">
                                                    <i class="bi bi-pencil-fill"></i>編輯
                                                </a>
                                                <form method="POST"
                                                    action="{{ route('warranty_web.video_config_destroy', $item->id) }}">
                                                    @csrf
                                                    <input name="_method" type="hidden" value="DELETE">
                                                    <button class="btn btn-danger btn-sm my-1">
                                                        <i class="bi bi-trash"></i>刪除
                                                    </button>
                                                </form>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop
@section('script')
    <script type="text/javascript"></script>
@stop
