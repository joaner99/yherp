@extends('layouts.base')
@section('title')
    託運總表
@stop
@section('css')
    <style type="text/css">
        .group {
            border: 1px solid lightgray;
            border-radius: 0.25rem;
            flex-shrink: 0;
            flex-basis: 400px;
        }

        .form-control {
            text-align: center;
        }

        .Done {
            text-decoration: line-through;
        }

        .Undone>td>a {
            color: red;
        }

        .transport-header {
            background-color: aliceblue;
            border-top-left-radius: 0.25rem;
            border-top-right-radius: 0.25rem;
            border-bottom: 1px solid lightgray;
        }

        .transport-header>.transport-total>.list-group>.list-group-item {
            padding: 0.25rem 1rem;
        }

        .transport-title>.transport-name {
            font-size: 1.75rem;
            font-weight: bold;
            color: darkgoldenrod;
            position: relative;
        }

        .transport-title>.schedule {
            position: absolute;
            top: 0;
            right: 5px;
            font-size: 1.75rem;
            font-weight: 1;
            font-family: Arial;
        }

        /*統計狀態*/
        .transport-total>div {
            border: 1px solid lightgray;
            border-radius: 0.25rem;
            flex-basis: 120px;
            margin: 0 0.15rem 0.15rem 0.15rem;
            padding: 0 0.5rem;
            display: flex;
            justify-content: space-between;
            align-items: center;
        }

        /*統計狀態數量*/
        .transport-total>div>span {
            font-size: 1.25rem;
            font-weight: bolder;
            font-family: Arial;
        }

        .transport-body>table>thead>tr>th {
            padding: 0.25rem 0.5rem;
        }

        .transport-body>table>tbody>tr>td {
            padding: 0.2em;
        }

        .transport-footer {
            font-size: 5rem;
            color: #28a745;
        }

        /*狀態*/
        .status {
            border-style: solid;
            border-width: 1px;
            border-radius: 0.25rem;
            border-color: darkgray;
            padding: 0.1rem 0.2rem;
            margin: 0.1rem;
        }

        /*毛利異常、買家備註*/
        .gpm_error,
        .buyer_notes {
            color: #fff;
            background-color: #dc3545;
            border-color: #dc3545;
        }

        /*新竹件數重量異常、新竹偏遠*/
        .hct_tran_error,
        .hct_remote {
            color: #fff;
            background-color: brown;
            border-color: brown;
        }

        /*毛利覆核、出貨覆核*/
        .gpm_check,
        .note_check,
        .shipment_check {
            color: #fff;
            background-color: #28a745;
            border-color: #28a745;
        }

        /*過大/過重*/
        .status1 {
            background-color: #ffc107;
            border-color: #ffc107;
        }

        /*延遲出貨*/
        .status2 {
            color: #fff;
            background-color: cornflowerblue;
            border-color: cornflowerblue;
        }

        /*二次檢驗*/
        .status3 {
            color: #fff;
            background-color: darkmagenta;
            border-color: darkmagenta;
        }

        /*銷貨單號、客戶簡稱*/
        .sales_no,
        .cust {
            min-width: 120px;
        }

        /*編輯按鈕*/
        .edit {
            min-width: 60px;
        }

        /*毛利批次覆核的銷單*/
        #tbody_gpm_edit .edit_sales_no,
        #tbody_gpm_edit .edit_check>input,
        #tbody_note_edit .edit_sales_no,
        #tbody_note_edit .edit_check>input {
            cursor: pointer;
        }

        #tbody_gpm_edit .edit_check,
        #tbody_note_edit .edit_check {
            padding: 1rem;
        }

        #tbody_gpm_edit .edit_check>input,
        #tbody_note_edit .edit_check>input {
            transform: scale(3);
        }

        #input_user {
            color: transparent;
            height: auto;
        }
    </style>
@endsection
@section('content')
    {{-- Modal 匯出超取已上車 --}}
    <div id="modal_shop_ship" class="modal">
        <div class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content">
                <form
                    action="{{ route('transport.export') . '?start_date=' . old('start_date') . '&' . 'end_date=' . old('end_date') }}"
                    method="POST">
                    @csrf
                    <div class="modal-header">
                        <h1 class="modal-title">匯出超取已上車</h1>
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-row">
                            <div class="col">
                                <div class="input-group input-group-lg h-100">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">員工條碼</span>
                                    </div>
                                    <input class="form-control" id="input_user" name="user" type="text"
                                        autocomplete="off" placeholder="請使用手持式掃描機，掃描員工條碼">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button id="btn_shop_ship" class="btn btn-primary" type="submit">確認</button>
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">關閉</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{-- Modal 銷貨單狀態編輯 --}}
    <div id="modal_status_edit" class="modal">
        <div class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content">
                <form action="{{ route('transport.index_update') }}" method="POST" enctype="multipart/form-data">
                    @method('PATCH')
                    @csrf
                    <div class="modal-header">
                        <h1 class="modal-title">銷貨單狀態編輯</h1>
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-row">
                            <div class="form-group col-3">
                                <label>銷貨單號</label>
                                <input name="sales_no" class="form-control" type="text" readonly>
                            </div>
                            <div class="form-group col-3">
                                <label for="input_estimated_date">預計出貨日期</label>
                                <input id="input_estimated_date" name="estimated_date" class="form-control" type="date">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-auto">
                                <label>狀態</label>
                                <div class="form-check-list form-check-list-horizontal">
                                    @if (!empty($status_list))
                                        @foreach ($status_list as $key => $item)
                                            <div class="form-check">
                                                <input class="form-check-input" id="edit_input_status_{{ $key }}"
                                                    name="status[]" type="checkbox" value="{{ $key }}"
                                                    onchange="ShipmentCheckEnable()">
                                                <label class="form-check-label" for="edit_input_status_{{ $key }}">
                                                    {{ $item }}
                                                </label>
                                            </div>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div id="div_shipment_check" class="form-check">
                            <input class="form-check-input" id="input_shipment_check" name="shipment_check" type="checkbox"
                                value="1">
                            <label class="form-check-label" for="input_shipment_check">出貨覆核</label>
                        </div>
                        <button id="btn_status_edit_confirm" class="btn btn-primary" type="submit">確認</button>
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">關閉</button>
                    </div>
                    <input name="start_date" type="hidden" value="{{ old('start_date') }}">
                    <input name="end_date" type="hidden" value="{{ old('end_date') }}">
                    <input name="order_expired" type="hidden">
                </form>
            </div>
        </div>
    </div>
    {{-- Modal 批次覆核買家備註 --}}
    <div id="modal_note_edit" class="modal">
        <div class="modal-dialog modal-dialog-centered" style="min-width: 90%">
            <div class="modal-content">
                <form action="{{ route('transport.index_note_check') }}" method="POST" enctype="multipart/form-data">
                    @method('PATCH')
                    @csrf
                    <div class="modal-header">
                        <h1 class="modal-title">備註批次覆核</h1>
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-row">
                            <div class="col">
                                <div class="FrozenTable" style="max-height: 70vh;">
                                    <table class="table table-bordered table-hover sortable">
                                        <thead>
                                            <tr>
                                                <th style="width: 8%;">銷貨單號</th>
                                                <th style="width: 7%;">客戶簡稱</th>
                                                <th style="width: 7%;">統編</th>
                                                <th>備註1</th>
                                                <th style="width: 40%;">買家備註</th>
                                                <th style="width: 5%;">覆核</th>
                                            </tr>
                                        </thead>
                                        <tbody id="tbody_note_edit">
                                            @foreach ($data as $value)
                                                @foreach ($value['Data'] as $value2)
                                                    @if (!empty($value2->buyer_notes))
                                                        <tr data-gpm_check="{{ $value2->gpm_check }}">
                                                            <td class="edit_sales_no">
                                                                <a
                                                                    href="{{ route('check_log_detail', $value2->PCOD1) }}"target="_blank">
                                                                    {{ $value2->PCOD1 }}
                                                                </a>
                                                            </td>
                                                            <td>{{ $value2->PPNAM }}</td>
                                                            <td>{{ $value2->RecieveCTCOD }}</td>
                                                            <td>{{ $value2->PBAK1 }}</td>
                                                            <td>{{ $value2->buyer_notes }}</td>
                                                            <td class="edit_check">
                                                                <input type="checkbox" value="1"
                                                                    {{ $value2->note_check ? 'checked' : '' }}>
                                                            </td>
                                                        </tr>
                                                    @endif
                                                @endforeach
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        @if (Auth::user()->can('transport_index_edit_full'))
                            <button id="btn_note_edit_confirm" class="btn btn-primary" type="submit">確認</button>
                        @endif
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">取消</button>
                    </div>
                    <input name="note_check_list" type="hidden">
                    <input name="start_date" type="hidden" value="{{ old('start_date') }}">
                    <input name="end_date" type="hidden" value="{{ old('end_date') }}">
                </form>
            </div>
        </div>
    </div>
    {{-- Modal 批次覆核毛利異常 --}}
    <div id="modal_gpm_edit" class="modal">
        <div class="modal-dialog modal-dialog-centered" style="min-width: 99%">
            <div class="modal-content">
                <form action="{{ route('transport.index_check') }}" method="POST" enctype="multipart/form-data">
                    @method('PATCH')
                    @csrf
                    <div class="modal-header">
                        <h1 class="modal-title">毛利批次覆核</h1>
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-row">
                            <div class="col text-success">
                                ※以下資料為訂單毛利率小於{{ $min }} 或 大於{{ $max }} 或 已完全出貨之銷貨金額不等於訂單金額
                            </div>
                        </div>
                        <div class="form-row mt-2">
                            <div class="col-3">
                                <div class="FrozenTable" style="max-height: 70vh;">
                                    <table class="table table-bordered table-hover sortable">
                                        <thead>
                                            <tr>
                                                <th style="width: 40%;">銷貨單號</th>
                                                <th style="width: 20%;">覆核</th>
                                                <th>原因</th>
                                            </tr>
                                        </thead>
                                        <tbody id="tbody_gpm_edit">
                                            @foreach ($data as $value)
                                                @foreach ($value['Data'] as $value2)
                                                    @if ($value2->gpm_error)
                                                        <tr data-gpm_check="{{ $value2->gpm_check }}">
                                                            <td class="edit_sales_no">{{ $value2->PCOD1 }}</td>
                                                            <td class="edit_check">
                                                                <input type="checkbox" value="1"
                                                                    {{ $value2->gpm_check ? 'checked' : '' }}>
                                                            </td>
                                                            <td>
                                                                <select class="custom-select gpm_check_result"
                                                                    style="{{ $value2->gpm_check ? '' : 'display: none;' }}">
                                                                    <option value="1"
                                                                        {{ $value2->gpm_check_result == '1' ? 'selected' : '' }}>
                                                                        正常</option>
                                                                    <option value="2"
                                                                        {{ $value2->gpm_check_result == '2' ? 'selected' : '' }}>
                                                                        採購</option>
                                                                    <option value="3"
                                                                        {{ $value2->gpm_check_result == '3' ? 'selected' : '' }}>
                                                                        行銷</option>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                    @endif
                                                @endforeach
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="col order_detail" style="display: none;">
                                <div class="card">
                                    <h5 class="card-header bg-warning">銷單明細</h5>
                                    <div class="card-body">
                                        <div class="form-row">
                                            <div class="col">
                                                <ul class="list-group list-group-horizontal-xl">
                                                    <li class="list-group-item list-group-item-secondary sales_no">
                                                        銷貨單號：<br><span></span>
                                                    </li>
                                                    <li class="list-group-item list-group-item-primary order_amount">
                                                        訂單總額：<br><span></span>
                                                    </li>
                                                    <li class="list-group-item list-group-item-success sales_amount">
                                                        銷單總額：<br><span></span>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="form-row mt-2">
                                            <div class="col">
                                                <ul class="list-group list-group-horizontal-xl">
                                                    <li class="list-group-item bak1" style="width: 25%;">
                                                        備註1：<br><span></span>
                                                    </li>
                                                    <li class="list-group-item bak2" style="width: 25%;">
                                                        備註2：<br><span></span>
                                                    </li>
                                                    <li class="list-group-item bak3" style="width: 25%;">
                                                        備註3：<br><span></span>
                                                    </li>
                                                    <li class="list-group-item InsideNote" style="width: 25%;">
                                                        內部備註：<br><span></span>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="form-row mt-2">
                                            <div class="col">
                                                <div class="FrozenTable" style="max-height: 51vh;">
                                                    <table class="table table-bordered table-hover sortable">
                                                        <thead>
                                                            <tr>
                                                                <th style="width: 10%;">料號</th>
                                                                <th>品名</th>
                                                                <th style="width: 10%;">單個銷售額</th>
                                                                <th style="width: 10%;">單個成本</th>
                                                                <th style="width: 10%;">單個毛利率</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody id="tbody_gpm_order">
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button id="btn_gpm_edit_confirm" class="btn btn-primary" type="submit">確認</button>
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">取消</button>
                    </div>
                    <input name="gpm_check_list" type="hidden">
                    <input name="start_date" type="hidden" value="{{ old('start_date') }}">
                    <input name="end_date" type="hidden" value="{{ old('end_date') }}">
                </form>
            </div>
        </div>
    </div>
    {{-- Modal 批次出貨覆核 --}}
    <div id="modal_batch_confirm" class="modal">
        <div class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content">
                <form action="{{ route('transport.index_batch_confirm') }}" method="POST"
                    enctype="multipart/form-data">
                    @method('PATCH')
                    @csrf
                    <div class="modal-header">
                        <h1 class="modal-title">批次出貨覆核</h1>
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-row">
                            <div class="col">
                                <div class="FrozenTable" style="max-height: 55vh;">
                                    <table class="table table-bordered table-hover sortable">
                                        <thead>
                                            <tr>
                                                <th style="width: 20%;">銷貨單號</th>
                                                <th style="width: 20%;">出貨日期</th>
                                                <th>原本狀態</th>
                                                <th class="sort-none" style="width: 6%;">
                                                    <input id="chk_confirm_all" type="checkbox">
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($data as $value)
                                                @foreach (collect($value['Data'])->where('isDone', false)->where('shipment_check', false) as $value2)
                                                    @if (count($value2->status) == 0)
                                                        @continue
                                                    @endif
                                                    <tr data-sales_date="{{ $value2->PDAT1 }}">
                                                        <td>{{ $value2->PCOD1 }}</td>
                                                        <td>{{ $value2->PDAT1 }}</td>
                                                        <td>
                                                            @if (count($value2->status) > 0)
                                                                @foreach ($value2->status as $key => $s)
                                                                    <span class="status status{{ $key }}">
                                                                        {{ $s['name'] }}
                                                                        @if ($key == '2')
                                                                            @php
                                                                                if (
                                                                                    substr(
                                                                                        $value2->estimated_date,
                                                                                        0,
                                                                                        4,
                                                                                    ) == (new DateTime())->format('Y')
                                                                                ) {
                                                                                    echo (new DateTime(
                                                                                        $value2->estimated_date,
                                                                                    ))->format('m-d');
                                                                                } else {
                                                                                    echo $value2->estimated_date;
                                                                                }
                                                                            @endphp
                                                                        @endif
                                                                    </span>
                                                                @endforeach
                                                            @endif
                                                        </td>
                                                        <td>
                                                            <input name="sales[]" type="checkbox"
                                                                value="{{ $value2->PCOD1 }}">
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary" type="submit">確認</button>
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">取消</button>
                    </div>
                    <input name="start_date" type="hidden" value="{{ old('start_date') }}">
                    <input name="end_date" type="hidden" value="{{ old('end_date') }}">
                </form>
            </div>
        </div>
    </div>
    {{-- Modal 批次二次檢驗覆核 --}}
    {{--
    <div id="modal_batch_status_confirm" class="modal">
        <div class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content">
                <form action="{{ route('transport.index_batch_status_confirm') }}" method="POST"
                    enctype="multipart/form-data">
                    @method('PATCH')
                    @csrf
                    <div class="modal-header">
                        <h1 class="modal-title">批次二次檢驗覆核</h1>
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-row">
                            <div class="col">
                                <div class="FrozenTable" style="max-height: 55vh;">
                                    <table class="table table-bordered table-hover sortable">
                                        <thead>
                                            <tr>
                                                <th style="width: 20%;">銷貨單號</th>
                                                <th style="width: 20%;">出貨日期</th>
                                                <th class="sort-none" style="width: 6%;">
                                                    <input id="chk_status_confirm_all" type="checkbox">
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($data as $value)
                                                @foreach (collect($value['Data'])->where('isDone', false)->where('shipment_check', false) as $value2)
                                                    @if (count($value2->status) == 0 || !array_key_exists(3, $value2->status))
                                                        @continue
                                                    @endif
                                                    <tr data-sales_date="{{ $value2->PDAT1 }}">
                                                        <td>{{ $value2->PCOD1 }}</td>
                                                        <td>{{ $value2->PDAT1 }}</td>
                                                        <td>
                                                            <input name="sales[]" type="checkbox"
                                                                value="{{ $value2->PCOD1 }}">
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary" type="submit">確認</button>
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">取消</button>
                    </div>
                    <input name="start_date" type="hidden" value="{{ old('start_date') }}">
                    <input name="end_date" type="hidden" value="{{ old('end_date') }}">
                </form>
            </div>
        </div>
    </div>
    --}}
    {{-- Modal 已包裝補上車 --}}
    <div id="modal_ship" class="modal">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <form action="{{ route('transport.ship') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-header">
                        <h1 class="modal-title">已包裝補上車</h1>
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="FrozenTable">
                            <table class="table table-bordered table-hover sortable">
                                <thead>
                                    <tr>
                                        <th>銷貨單號</th>
                                        <th>覆核</th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div id="div_batch_shipment_check" class="form-check">
                            <button class="btn btn-primary" type="submit">確認</button>
                            <button class="btn btn-secondary" type="button" data-dismiss="modal">取消</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="form-row mb-2">
            <div class="col-auto">
                <form action="{{ route('transport.index') }}" method="GET">
                    <div class="form-row">
                        <div class="form-group col-auto">
                            <label for="input_start_date">起日</label>
                            <input id="input_start_date" name="start_date" class="form-control" type="date"
                                value="{{ old('start_date') }}" required>
                        </div>
                        <div class="form-group col-auto">
                            <label for="input_end_date">訖日</label>
                            <input id="input_end_date" name="end_date" class="form-control" type="date"
                                value="{{ old('end_date') }}" required>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-auto">
                            <div class="form-check">
                                <input type="checkbox" class="form-check-input" id="input_onlyTax" name="only_tax"
                                    {{ old('only_tax') ? 'checked' : '' }}>
                                <label class="form-check-label" for="input_onlyTax">只查詢有開立發票</label>
                            </div>
                        </div>
                    </div>
                    <button class="btn btn-primary btn-sm">查詢區間</button>
                    <button id="btn_search_today" class="btn btn-warning btn-sm ml-2">查詢今日</button>
                    <span class="text-success align-bottom ml-2">※排除客代為【包材】【半成品領料】【外包設計師】</span>
                </form>
            </div>
            <div class="col"></div>
            <div class="col">
                <div class="form-row justify-content-end">
                    <div class="col-auto my-1">
                        <button class="btn btn-success btn-sm" data-toggle="modal" data-target="#modal_note_edit">
                            <i class="bi bi-check2-square"></i>備註批次覆核
                        </button>
                    </div>
                    @if (Auth::user()->can('transport_index_edit_full'))
                        <div class="col-auto my-1">
                            <button class="btn btn-success btn-sm" data-toggle="modal" data-target="#modal_gpm_edit">
                                <i class="bi bi-check2-square"></i>毛利批次覆核
                            </button>
                        </div>
                    @endif
                    @if (Auth::user()->can('transport_index_edit'))
                        {{--
                        <div class="col-auto">
                            <button class="btn btn-sm status3" data-toggle="modal"
                                data-target="#modal_batch_status_confirm">
                                <i class="bi bi-check2-square"></i>批次二次檢驗覆核
                            </button>
                        </div>
                        --}}
                        <div class="col-auto my-1">
                            <button class="btn btn-success btn-sm" data-toggle="modal"
                                data-target="#modal_batch_confirm">
                                <i class="bi bi-check2-square"></i>批次出貨覆核
                            </button>
                        </div>
                        <div class="col-auto my-1">
                            <a class="btn btn-warning btn-sm" href="{{ route('transport.index_edit') }}">
                                <i class="bi bi-pencil"></i>批次編輯
                            </a>
                        </div>
                        @role('admin')
                            <div class="col-auto my-1">
                                <button class="btn btn-info btn-sm" data-toggle="modal" data-target="#modal_ship">
                                    <i class="bi bi-check2-square"></i>已包裝補上車
                                </button>
                            </div>
                        @endrole
                    @endif
                </div>
                <div class="form-row justify-content-end mt-2">
                    <div class="col-auto">
                        <button class="btn btn-success btn-sm" data-toggle="modal" data-target="#modal_shop_ship">
                            <i class="bi bi-check2-square"></i>匯出超取已上車
                        </button>
                    </div>
                </div>
                <div class="form-row justify-content-end mt-2">
                    <div class="col-auto">
                        <div class="form-group form-check">
                            <input type="checkbox" class="form-check-input" id="input_onlyUndone" checked="checked"
                                onclick="input_onlyUndone_onclick()">
                            <label class="form-check-label" for="input_onlyUndone">只顯示銷貨單未上車</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-row align-items-start">
            @if (!empty($data))
                @foreach ($data as $trans => $value)
                    <div class="group col p-0 m-1 text-center">
                        <div class="transport-header">
                            <div class="transport-title">
                                <span class="transport-name">{{ $value['Data'][0]->TransportName }}</span>
                                <span
                                    class="schedule {{ $value['Done'] == $value['All'] ? 'text-success' : 'text-danger' }}">
                                    {{ $value['Done'] . '/' . $value['All'] }}
                                </span>
                            </div>
                            <div class="transport-total d-flex justify-content-center flex-wrap">
                                @if (!empty($value['buyer_notes']))
                                    <div class="buyer_notes">
                                        買家備註<span>{{ $value['buyer_notes'] }}</span>
                                    </div>
                                @endif
                                @if (!empty($value['gpm_error']))
                                    <div class="gpm_error">
                                        毛利異常<span>{{ $value['gpm_error'] }}</span>
                                    </div>
                                @endif
                                @if (!empty($value['hct_tran_error']))
                                    <div class="hct_tran_error">
                                        新竹件數重量異常<span>{{ $value['hct_tran_error'] }}</span>
                                    </div>
                                @endif
                                @if (!empty($value['hct_remote']))
                                    <div class="hct_remote">
                                        新竹偏遠<span>{{ $value['hct_remote'] }}</span>
                                    </div>
                                @endif
                                @if (!empty($value['status']))
                                    @foreach ($value['status'] as $key => $status)
                                        <div class="status{{ $key }}">
                                            {{ $status['status_data']['name'] }}
                                            <span>{{ $status['count'] }}</span>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                        <div class="transport-body FrozenTable" style="max-height: 30vh; font-size:0.95rem;">
                            <table class="table table-bordered table-hover sortable">
                                <thead>
                                    <tr>
                                        <th style="width: 120px">銷貨單號</th>
                                        <th style="width: 120px">客戶簡稱</th>
                                        <th>狀態</th>
                                        <th style="width: 60px;">編輯</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($value['Data'] as $value2)
                                        <tr class="{{ $value2->isDone ? 'Done' : 'Undone' }}"
                                            style="{{ $value2->isDone ? 'display: none;' : '' }}"
                                            data-sales_no="{{ $value2->PCOD1 }}" data-sales_date="{{ $value2->PDAT1 }}"
                                            data-gpm_error="{{ $value2->gpm_error }}"
                                            data-estimated_date="{{ $value2->estimated_date }}"
                                            data-shipment_check="{{ $value2->shipment_check }}"
                                            data-gpm_check="{{ $value2->gpm_check }}"
                                            data-status='{{ json_encode($value2->status) }}'
                                            data-order_expired="{{ $value2->order_expired }}">
                                            <td class="sales_no">
                                                <a href="{{ route('check_log_detail', $value2->PCOD1) }}">
                                                    {{ $value2->PCOD1 }}
                                                </a>
                                            </td>
                                            <td class="cust">{{ $value2->PPNAM }}</td>
                                            <td>
                                                <div class="text-nowrap">
                                                    @if ($value2->note_check)
                                                        <span class="status note_check">備註覆核</span>
                                                    @elseif (!empty($value2->buyer_notes))
                                                        <span class="status buyer_notes">買家備註</span>
                                                    @endif
                                                    @if ($value2->gpm_check)
                                                        <span class="status gpm_check">毛利覆核</span>
                                                    @elseif($value2->gpm_error)
                                                        <span class="status gpm_error">毛利異常</span>
                                                    @endif
                                                    @if ($value2->shipment_check)
                                                        <span class="status shipment_check">出貨覆核</span>
                                                    @elseif (count($value2->status) > 0)
                                                        @foreach ($value2->status as $key => $s)
                                                            <span class="status status{{ $key }}">
                                                                {{ $s['name'] }}
                                                                @if ($key == '2')
                                                                    @php
                                                                        if (
                                                                            substr($value2->estimated_date, 0, 4) ==
                                                                            (new DateTime())->format('Y')
                                                                        ) {
                                                                            echo (new DateTime(
                                                                                $value2->estimated_date,
                                                                            ))->format('m-d');
                                                                        } else {
                                                                            echo $value2->estimated_date;
                                                                        }
                                                                    @endphp
                                                                @endif
                                                            </span>
                                                        @endforeach
                                                    @endif
                                                    @if ($value2->hct_tran_error)
                                                        <span class="status hct_tran_error">新竹件數重量異常</span>
                                                    @endif
                                                    @if ($value2->hct_remote)
                                                        <span class="status hct_remote">新竹偏遠</span>
                                                    @endif
                                                </div>
                                            </td>
                                            <td class="edit">
                                                @if (Auth::user()->can('transport_index_edit') && $trans != 'COM')
                                                    <button class="btn btn-secondary btn-sm btn_status_edit"
                                                        type="button">
                                                        <i class="bi bi-pencil-fill m-0"></i>
                                                    </button>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="transport-footer" style="display: none;">全上車</div>
                    </div>
                @endforeach
            @else
                <div class="alert alert-danger">
                    找不到資料
                </div>
            @endif
        </div>
    </div>
@stop
@section('script')
    <script type="text/javascript">
        $(document).ready(function() {
            InitialDate();
            input_onlyUndone_onclick();
        });
        //全選功能
        $("#chk_all").change(check_all);
        //全選功能
        $("#chk_confirm_all").change(check_all);
        //全選功能
        $("#chk_status_confirm_all").change(check_all);

        function InitialDate() {
            var today = new Date();

            if ($("#input_start_date").val() == '') {
                $("#input_start_date").val(DateToString(today));
            }
            if ($("#input_end_date").val() == '') {
                $("#input_end_date").val(DateToString(today));
            }
        }
        //查詢今日
        $('#btn_search_today').click(function() {
            $("#input_start_date,#input_end_date").val(DateToString(new Date()));
        });
        //匯出超取已上車
        $('#btn_export').click(function() {
            var data = [
                ['超商', '數量', '已送達']
            ];
            var total = 0;
            $('.transport-title').each(function(i, e) {
                var name = $(e).find('.transport-name').text().trim();
                if (name.includes('超取')) {
                    var done = $(e).find('.schedule').text().split('/')[0].trim();
                    data.push([name, done, '']);
                    total += parseInt(done);
                }
            });
            data.push(['合計', total, '']);
            $('input[name="export_data"]').val(JSON.stringify(data));
        });
        //只顯示銷貨單未上車
        function input_onlyUndone_onclick() {
            var only_undone = $("#input_onlyUndone").prop("checked");
            if (only_undone) {
                $(".Done").hide();
                $('.transport-body').each(function(i, e) {
                    if ($(e).find('>table>tbody>.Undone').length == 0) {
                        $(e).hide();
                        $(e).find('+.transport-footer').show();
                    } else {
                        $(e).show();
                        $(e).find('+.transport-footer').hide();
                    }
                });
            } else {
                $(".Done,.transport-body").show();
                $('.transport-footer').hide();
            }
        }
        //狀態編輯
        $('.btn_status_edit').click(function() {
            var data = $(this).closest('tr').data();
            var sales_no = data.sales_no;
            var sales_date = data.sales_date;
            var estimated_date = data.estimated_date;
            var shipment_check = data.shipment_check;
            var status = data.status;
            var order_expired = data.order_expired;
            $('#modal_status_edit').find("input[name='status[]']").each(function(index, element) {
                var val = $(element).val();
                $(element).prop('checked', status[val] !== undefined);
            });
            $('input[name="sales_no"]').val(sales_no);
            $('input[name="order_expired"]').val(order_expired);
            //預計出貨日期
            $('#input_estimated_date').val(estimated_date);
            var min_date = DateToString((new Date(toADDate(sales_date))));
            $("#input_estimated_date").attr("min", min_date);
            $('#input_shipment_check').prop('checked', shipment_check);
            ShipmentCheckEnable();
            $('#modal_status_edit').modal('show');
        });
        //編輯出貨覆核顯示
        function ShipmentCheckEnable() {
            if ($('#modal_status_edit input[name="status[]"]:checked').length > 0) {
                $('#div_shipment_check').show();
            } else {
                $('#div_shipment_check').hide();
            }
        }
        //銷單明細
        $('#modal_gpm_edit .edit_sales_no').click(function() {
            $(this).addClass('bg-secondary text-white');
            var sales_no = $(this).text();
            var url = "{{ route('transport.ajax_get_order') }}";
            lockScreen();
            $.ajax({
                url: url,
                type: "get",
                dataType: 'json',
                data: {
                    sales_no: sales_no,
                },
                contentType: "application/json",
                success: function(d) {
                    if (!$.isEmptyObject(d)) {
                        if (!IsNullOrEmpty(d.msg)) {
                            alert(d.msg);
                        } else {
                            SetOrderData(d.data);
                        }
                    }
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    var msg = JSON.parse(xhr.responseText).msg;
                    alert(msg);
                },
                complete: function() {
                    unlockScreen();
                    $('#modal_gpm_edit').modal('show');
                }
            });
        });
        //狀態編輯確認
        $('#btn_status_edit_confirm').click(function() {
            if ($('input[name="status[]"]:checked').length > 0 && IsNullOrEmpty($('#input_estimated_date')
                    .val())) {
                alert('當有勾選『狀態』時，『預計出貨日期』為必填');
                $('#input_estimated_date').focus();
                return false;
            }
            //非出貨覆核 且 訂單快過期 且 (延遲出貨 或 預計出貨日期)
            if (!$('#input_shipment_check').prop('checked') && $('input[name="order_expired"]').val() == '訂單過期' &&
                ($('#edit_input_status_2').prop('checked') || !IsNullOrEmpty($('#input_estimated_date').val()))
            ) {
                alert('該銷單快『訂單過期』，禁止延遲出貨');
                return false;
            }
        });
        //毛利批次覆核確認
        $('#btn_gpm_edit_confirm').click(function() {
            var data = [];
            $('#tbody_gpm_edit>tr').each(function(i, e) {
                var sales_no = $(e).find('.edit_sales_no').text().trim();
                var chk = $(e).find('.edit_check>input').is(':checked');
                var gpm_check_result = $(e).find('.gpm_check_result').val();
                data.push({
                    sales_no: sales_no,
                    chk: chk,
                    gpm_check_result: gpm_check_result,
                });
            });
            $('input[name="gpm_check_list"]').val(JSON.stringify(data));
        });
        //備註批次覆核確認
        $('#btn_note_edit_confirm').click(function() {
            var data = [];
            $('#tbody_note_edit>tr').each(function(i, e) {
                var sales_no = $(e).find('.edit_sales_no').text().trim();
                var chk = $(e).find('.edit_check>input').is(':checked');
                data.push({
                    sales_no: sales_no,
                    chk: chk
                });
            });
            $('input[name="note_check_list"]').val(JSON.stringify(data));
        });
        //毛利批次覆核關閉，還原畫面
        $('#modal_gpm_edit').on('hidden.bs.modal', function(e) {
            $('#tbody_gpm_edit .edit_check>input').prop('checked', false);
            $('#tbody_gpm_edit>tr[data-gpm_check="1"] .edit_check>input').prop('checked', true);
            $('.edit_sales_no').removeClass('bg-secondary text-white');
            $('.order_detail').hide();
        })
        $('#modal_ship').on('shown.bs.modal', function(e) {
            var url = "{{ route('transport.ajax_get_unshipped') }}";
            lockScreen();
            $.ajax({
                url: url,
                type: "get",
                dataType: 'json',
                contentType: "application/json",
                success: function(d) {
                    if (!$.isEmptyObject(d)) {
                        if (!IsNullOrEmpty(d.msg)) {
                            alert(d.msg);
                        } else {
                            SetUnshippedData(d.data);
                        }
                    }
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    var msg = JSON.parse(xhr.responseText).msg;
                    alert(msg);
                },
                complete: function() {
                    unlockScreen();
                }
            });
        });
        //設定銷單的毛利資訊
        function SetOrderData(data) {
            var dom = '';
            var sales_no = data.sales_no;
            var order_amount = data.order_amount;
            var sales_amount = data.sales_amount;
            var items = data.items;
            $(items).each(function(i, e) {
                dom += '<tr>';
                dom += '<td>' + e.no + '</td>';
                dom += '<td class="text-left">' + e.name + '</td>';
                dom += '<td>' + e.unit + '</td>';
                dom += '<td>' + e.cost + '</td>';
                dom += '<td>' + e.gpm + '</td>';
                dom += '</tr>';
            });
            $('#tbody_gpm_order').html(dom);
            $('input[name="sales_no"]').val(sales_no);

            $('.sales_no>span').text(sales_no);
            $('.order_amount>span').text(order_amount);
            $('.sales_amount>span').text(sales_amount);
            $('.bak1>span').text(data.bak1);
            $('.bak2>span').text(data.bak2);
            $('.bak3>span').text(data.bak3);
            $('.InsideNote>span').text(data.InsideNote);
            $('.order_detail').show();
        }
        //
        function SetUnshippedData(data) {
            var dom = '';
            $(data).each(function(i, e) {
                dom += '<tr>';
                dom += '<td>' + e + '</td>';
                dom += '<td>' + '<input name="sales[]" type="checkbox" value="' + e + '">' + '</td>';
                dom += '</tr>';
            });
            $('#modal_ship tbody').html(dom);
        }
        //毛利覆核開關原因
        $('.edit_check>input').on('change', function() {
            var tr = $(this).closest('tr');
            $(tr).find('.gpm_check_result').toggle($(this).checked);
        });
    </script>
@stop
