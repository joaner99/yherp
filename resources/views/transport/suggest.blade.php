@extends('layouts.base')
@section('title')
    物流建議查詢
@stop
@section('css')
    <style type="text/css">
        /*選取樣式*/
        #table_data>tbody>tr:has(.confirm:checked) {
            color: #fff;
            background-color: #007bff;
        }
    </style>
@endsection
@section('content')
    {{-- Modal 上傳 --}}
    <div id="modal_convert_ycs" class="modal">
        <div class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content">
                <form action="{{ route('transport.import_api') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title">轉音速物流</h5>
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group mb-4">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">上車日期</span>
                                </div>
                                <input id="input_settle_date" name="settle_date" class="form-control col-3" type="date"
                                    required>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button id="btn_convert_ycs" class="btn btn-primary" type="submit">轉檔</button>
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">關閉</button>
                    </div>
                    <input name="selected_order" type="hidden" value="">
                </form>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="form-row mb-2">
            <div class="col-auto">
                <form action="{{ route('transport.suggest') }}" method="GET">
                    <div class="form-row">
                        <div class="form-group col-auto">
                            <label for="input_start_date">起日</label>
                            <input id="input_start_date" name="start_date" class="form-control" type="date"
                                value="{{ old('start_date') }}">
                        </div>
                        <div class="form-group col-auto">
                            <label for="input_end_date">訖日</label>
                            <input id="input_end_date" name="end_date" class="form-control" type="date"
                                value="{{ old('end_date') }}">
                        </div>
                    </div>
                    <button class="btn btn-primary btn-sm" type="submit">查詢</button>
                </form>
            </div>
        </div>
        <div class="form-row align-items-end">
            <div class="col-auto text-muted">
                ※銷貨單，蝦皮1/2/3店、官網、露天、iOPEN Mall、MOMO、MO店+、Yahoo、已付款後寄貨、貨到付款；排除原物流為五大超商超取、本公司親送、廠商直寄、自取；備註棧板；送貨地址有填寫
            </div>
            <div class="col"></div>
            @if (!empty($data))
                <div class="col-auto">
                    <button id="btn_modal_convert_ycs" type="button" class="btn btn-secondary btn-sm">
                        <i class="bi bi-arrow-repeat"></i>轉音速物流
                    </button>
                </div>
            @endif
        </div>
        <div class="form-row mt-2">
            @if (empty($data))
                <div class="alert alert-danger">
                    無資料
                </div>
            @else
                <div class="col">
                    <div class="FrozenTable" style="max-height: 74vh; font-size: 0.95rem;">
                        <table id="table_data" class="table table-bordered sortable table-filter">
                            <thead>
                                <tr>
                                    <th class="sort-none">
                                        <input id="chk_all" type="checkbox">
                                    </th>
                                    <th>序</th>
                                    <th>銷貨單號</th>
                                    <th>原始訂單號</th>
                                    <th class="filter-col">客戶簡稱</th>
                                    <th>籃子地址</th>
                                    <th class="filter-col">原物流</th>
                                    <th>建議物流</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $index = 1; ?>
                                @foreach ($data as $item)
                                    <tr data-suggest="{{ $item->suggest }}"data-order_no="{{ $item->sale_no }}">
                                        <td>
                                            <input class="confirm" type="checkbox">
                                        </td>
                                        <td>{{ $index++ }}</td>
                                        <td>{{ $item->sale_no }}</td>
                                        <td class="original_no_font">{{ $item->original_no }}</td>
                                        <td>{{ $item->cust_name }}</td>
                                        <td class="text-left">{{ $item->addr }}</td>
                                        <td>{{ $item->TransportName }}</td>
                                        <td>{{ $item->suggest }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            @endif
        </div>
    </div>
@stop
@section('script')
    <script type="text/javascript">
        $(document).ready(InitialDate());

        function InitialDate() {
            var today = new Date();
            if ($("#input_start_date").val() == '') {
                $("#input_start_date").val(DateToString(today));
            }
            if ($("#input_end_date").val() == '') {
                $("#input_end_date").val(DateToString(today));
            }
            if ($("#input_settle_date").val() == '') {
                $("#input_settle_date").val(DateToString(today));
            }
        }
        //全選功能
        $('#chk_all').change(check_all);
        //開啟轉音速物流
        $('#btn_modal_convert_ycs').click(function() {
            if ($("#table_data>tbody>tr .confirm:checked").length > 0) {
                $('#modal_convert_ycs').modal('show')
            } else {
                alert('請選取訂單');
            }
        });
        //轉檔
        $('#btn_convert_ycs').click(function() {
            var data = new Array();
            $("#table_data>tbody>tr .confirm:checked").each(function(i, e) {
                data.push($(e).closest('tr').data().order_no);
            });
            $('input[name="selected_order"]').val(JSON.stringify(data));
        });
    </script>
@stop
