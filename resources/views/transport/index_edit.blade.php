@extends('layouts.base')
@section('title')
    批次狀態編輯
@stop
@section('css')
    <style type="text/css">
        /*checkbox size*/
        input[type="checkbox"] {
            transform: scale(2.5);
        }

        .changed {
            background-color: #ffc107;
        }
    </style>
@endsection
@section('content')
    <div class="container-fluid">
        <div class="form-row">
            <div class="col">
                <form action="{{ route('transport.index_edit') }}" method="GET">
                    <div class="form-row">
                        <div class="form-group col-auto">
                            <label for="input_start_date">銷貨起日</label>
                            <input id="input_start_date" name="start_date" class="form-control" type="date"
                                value="{{ old('start_date') }}" required>
                        </div>
                        <div class="form-group col-auto">
                            <label for="input_end_date">銷貨訖日</label>
                            <input id="input_end_date" name="end_date" class="form-control" type="date"
                                value="{{ old('end_date') }}" required>
                        </div>
                    </div>
                    <button class="btn btn-primary" type="submit">查詢</button>
                </form>
            </div>
            <div class="col"></div>
            <div class="col-auto align-content-end">
                <form action="{{ route('transport.index_batch_update') }}" method="POST">
                    @csrf
                    <button id="btn_confirm" class="btn btn-info" type="submit">確認</button>
                    <input name="data" type="hidden">
                </form>
            </div>
        </div>
        <div class="form-row mt-2">
            <div class="col">
                <div class="FrozenTable" style="max-height: 78vh;">
                    <table class="table table-bordered sortable table-filter">
                        <thead>
                            <tr>
                                <th class="filter-col" style="width: 8%">訂單快過期</th>
                                <th class="filter-col" style="width: 10%;">客戶簡稱</th>
                                <th class="filter-col">物流</th>
                                <th style="width: 10%;">銷貨單號</th>
                                <th style="width: 10%;">銷貨日期</th>
                                <th style="width: 10%;" class="sort-none">
                                    <input id="estimated_date_all" class="form-control" type="date">
                                    <div>預計出貨日</div>
                                </th>
                                @if (!empty($status_list))
                                    @foreach ($status_list as $key => $item)
                                        <th style="width: 7%;" class="sort-none">
                                            <input id="chk_all_{{ $key }}" type="checkbox">
                                            <div class="mt-2">{{ $item }}</div>
                                        </th>
                                    @endforeach
                                @endif
                            </tr>
                        </thead>
                        <tbody id="tbody_data">
                            @foreach ($data as $value)
                                @foreach (collect($value['Data'])->where('isDone', false)->whereNotIn('TransportCode', ['COM']) as $value2)
                                    <tr data-sales_no="{{ $value2->PCOD1 }}">
                                        <td>{{ $value2->order_expired }}</td>
                                        <td>{{ $value2->PPNAM }}</td>
                                        <td>{{ $value2->TransportName }}</td>
                                        <td>
                                            <a href="{{ route('check_log_detail', $value2->PCOD1) }}">
                                                {{ $value2->PCOD1 }}
                                            </a>
                                        </td>
                                        <td>{{ $value2->PDAT1 }}</td>
                                        <td>
                                            @if (Auth::user()->can('transport_expired_delay') || $value2->order_expired != '訂單過期')
                                                <input name="estimated_date" class="form-control" type="date"
                                                    value="{{ $value2->estimated_date }}"
                                                    data-original="{{ $value2->estimated_date }}">
                                            @endif
                                        </td>
                                        @if (!empty($status_list))
                                            @foreach ($status_list as $key => $item)
                                                <td>
                                                    @if (!Auth::user()->can('transport_expired_delay') && $value2->order_expired == '訂單過期' && $key == 2)
                                                        @continue
                                                    @endif
                                                    <input name="status" type="checkbox" value="{{ $key }}"
                                                        {{ array_key_exists($key, $value2->status) ? 'checked' : '' }}
                                                        data-original=" {{ array_key_exists($key, $value2->status) ? '1' : '0' }}">
                                                </td>
                                            @endforeach
                                        @endif
                                    </tr>
                                @endforeach
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop
@section('script')
    <script type="text/javascript">
        //全選功能
        $('[id*="chk_all_"').change(check_all);
        $(document).ready(InitialDate());

        function InitialDate() {
            var today = new Date();
            if ($("#input_start_date").val() == '') {
                $("#input_start_date").val(DateToString(today));
            }
            if ($("#input_end_date").val() == '') {
                $("#input_end_date").val(DateToString(today));
            }
        }
        //全選功能
        $('#chk_all').change(check_all);
        $('#btn_confirm').click(function() {
            var data = [];
            $('#tbody_data>.changed').each(function(i, e) {
                var sales_no = $(e).data().sales_no;
                var estimated_date = $(e).find('input[name="estimated_date"]').val();
                var status = $(e).find("input[name='status']:checked").map(function() {
                    return $(this).val();
                }).get();
                var item = {
                    sales_no: sales_no,
                    estimated_date: estimated_date,
                    status: status
                };
                data.push(item);
            });
            $('input[name="data"]').val(JSON.stringify(data));
        });
        //值異動
        $('#tbody_data>tr>td>input').change(function() {
            var tr = $(this).closest('tr');
            var changed = false;
            $(tr).find('[data-original]').each(function(i2, e2) {
                var val;
                if ($(e2).prop('type') == 'checkbox') {
                    val = $(e2).is(":checked") ? 1 : 0;
                } else {
                    val = $(e2).val();
                }
                if (val != $(e2).data().original) {
                    changed = true;
                    return false;
                }
            });
            if (changed) {
                if (!$(tr).hasClass('changed')) {
                    $(tr).addClass('changed');
                }
            } else {
                $(tr).removeClass('changed');
            }
        });
        //預計出貨日全設定
        $('#estimated_date_all').change(function() {
            $('input[name="estimated_date"]').val($(this).val()).change();
        });
    </script>
@stop
