@extends('layouts.base')
@section('title')
    物流運費
@stop
@section('css')
    <style type="text/css">
        /*明細*/
        table.inner-table>thead>tr>th:nth-child(1),
        table.inner-table>tbody>tr>td:nth-child(1) {
            width: 120px;
        }

        table.inner-table>thead>tr>th:nth-child(3),
        table.inner-table>tbody>tr>td:nth-child(3) {
            width: 70px;
        }

        .card-header {
            color: var(--white);
            background-color: var(--secondary);
            caption-side: top;
            text-align: left;
            padding: 0.5rem 1rem;
            font-size: 2rem;
            font-weight: bold;
            height: 64px;
        }
    </style>
@endsection
@section('content')
    {{-- Modal 匯入 --}}
    <div id="modal_upload" class="modal">
        <div class="modal-dialog modal-xl modal-dialog-centered">
            <div class="modal-content">
                <form action="{{ route('transport.import_freight') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title">匯入</h5>
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">物流</span>
                            </div>
                            <select class="custom-select col-2" name="type" required>
                                <option value="HCT">新竹</option>
                                <option value="YCS">音速</option>
                            </select>
                            <div class="custom-file">
                                <input class="custom-file-input" id="input_file" name="file" type="file"
                                    accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,.xls"required>
                                <label class="custom-file-label" for="input_file">請選擇檔案</label>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button id="btn_import" type="submit" class="btn btn-primary">確認</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">關閉</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @role('admin')
        {{-- Modal 分析 --}}
        <div id="modal_analyze" class="modal">
            <div class="modal-dialog modal-dialog-centered" style="max-width: 98vw;">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">分析{{ old('start_date') . '～' . old('end_date') }}</h5>
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-row">
                            <div class="col-3">
                                <div class="card">
                                    <div class="card-header">
                                        產品大類-總運費比例
                                    </div>
                                    <div class="card-body">
                                        <div id="chart_freight">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="card">
                                    <div class="card-header">
                                        產品大類-件數比例
                                    </div>
                                    <div class="card-body">
                                        <div id="chart_qty">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="card">
                                    <div class="card-header">
                                        產品大類-單數比例
                                    </div>
                                    <div class="card-body">
                                        <div id="chart_order">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-row mt-2">
                            <div class="col-3">
                                <div class="FrozenTable" style="max-height: 78vh;">
                                    <table class="table table-bordered sortable table-hover">
                                        <caption>產品大類-分析</caption>
                                        <thead>
                                            <tr>
                                                <th>產品大類</th>
                                                <th>運費</th>
                                                <th>件數</th>
                                                <th>單數</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($kind1_analyze as $kind1 => $item)
                                                <tr>
                                                    <td>{{ $kind1 }}</td>
                                                    <td>{{ number_format($item['freight']) }}</td>
                                                    <td>{{ number_format($item['qty']) }}</td>
                                                    <td>{{ number_format($item['order']) }}</td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <span class="text-success">※因訂單包含不同種類，故運費會有重疊計算※</span>
                            </div>
                            <div class="col-3">
                                <div class="FrozenTable" style="max-height: 78vh;">
                                    <table class="table table-bordered table-hover">
                                        <caption>統計</caption>
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th>新竹</th>
                                                <th>音速</th>
                                                <th>加總</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <th>運費</th>
                                                <td>{{ number_format($data->where('trans', 'HCT')->sum('freight')) }}</td>
                                                <td>{{ number_format($data->where('trans', 'YCS')->sum('freight')) }}</td>
                                                <td>{{ number_format($data->sum('freight')) }}</td>
                                            </tr>
                                            <tr>
                                                <th>聯運費</th>
                                                <td>{{ number_format($data->where('trans', 'HCT')->sum('combined_freight')) }}
                                                </td>
                                                <td>{{ number_format($data->where('trans', 'YCS')->sum('combined_freight')) }}
                                                </td>
                                                <td>{{ number_format($data->sum('combined_freight')) }}</td>
                                            </tr>
                                            <tr>
                                                <th>其他運費</th>
                                                <td>{{ number_format($data->where('trans', 'HCT')->sum('other_freight')) }}
                                                </td>
                                                <td>{{ number_format($data->where('trans', 'YCS')->sum('other_freight')) }}
                                                </td>
                                                <td>{{ number_format($data->sum('other_freight')) }}</td>
                                            </tr>
                                            <tr>
                                                <th>總運費</th>
                                                <td>{{ number_format($data->where('trans', 'HCT')->sum('total_freight')) }}
                                                </td>
                                                <td>{{ number_format($data->where('trans', 'YCS')->sum('total_freight')) }}
                                                </td>
                                                <td>{{ number_format($data->sum('total_freight')) }}</td>
                                            </tr>
                                            <tr>
                                                <th colspan="4"></th>
                                            </tr>
                                            <tr>
                                                <th>件數</th>
                                                <td>{{ number_format($data->where('trans', 'HCT')->sum('qty')) }}
                                                </td>
                                                <td>{{ number_format($data->where('trans', 'YCS')->sum('qty')) }}
                                                </td>
                                                <td>{{ number_format($data->sum('qty')) }}</td>
                                            </tr>
                                            <tr>
                                                <th>單數</th>
                                                <td>{{ number_format($data->where('trans', 'HCT')->count()) }}
                                                </td>
                                                <td>{{ number_format($data->where('trans', 'YCS')->count()) }}
                                                </td>
                                                <td>{{ number_format($data->count()) }}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">關閉</button>
                    </div>
                </div>
            </div>
        </div>
    @endrole
    <div class="container-fluid">
        <div class="form-row">
            <div class="col">
                <div class="form-row">
                    <div class="col">
                        <form action="{{ route('transport.freight_index') }}" method="GET">
                            <div class="form-row">
                                <div class="form-group col-auto">
                                    <label for="input_start_date">配送起日</label>
                                    <input id="input_start_date" name="start_date" class="form-control" type="date"
                                        value="{{ old('start_date') }}" required>
                                </div>
                                <div class="form-group col-auto">
                                    <label for="input_end_date">配送訖日</label>
                                    <input id="input_end_date" name="end_date" class="form-control" type="date"
                                        value="{{ old('end_date') }}" required>
                                </div>
                                <div class="form-group col-auto">
                                    <label for="input_type">物流</label>
                                    <select class="custom-select" id="input_type" name="type">
                                        <option value="" {{ empty(old('type')) ? 'selected' : '' }}>全部</option>
                                        <option value="HCT" {{ old('type') == 'HCT' ? 'selected' : '' }}>新竹</option>
                                        <option value="YCS" {{ old('type') == 'YCS' ? 'selected' : '' }}>音速</option>
                                    </select>
                                </div>
                            </div>
                            <button id="btn_search" class="btn btn-primary btn-sm">查詢</button>
                        </form>
                    </div>
                </div>
            </div>
            @role('admin')
                <div class="col-auto">
                    <button class="btn btn-info btn-sm" type="button" data-toggle="modal" data-target="#modal_analyze">
                        <i class="bi bi-bar-chart"></i>分析
                    </button>
                </div>
            @endrole
            <div class="col-auto">
                <button class="btn btn-success btn-sm" type="button" data-toggle="modal" data-target="#modal_upload">
                    <i class="bi bi-file-earmark-excel"></i>匯入
                </button>
            </div>
        </div>
        <div class="form-row mt-2">
            <div class="col">
                <div class="FrozenTable" style="max-height: 78vh; font-size: 0.8rem;">
                    <table class="table table-bordered sortable table-filter table-hover">
                        <thead>
                            <tr>
                                <th style="width: 4%">序</th>
                                <th style="width: 9%" class="filter-col">銷貨單號</th>
                                <th style="width: 7%" class="filter-col">物流單號</th>
                                <th style="width: 5%" class="filter-col">物流</th>
                                <th style="width: 6%" class="filter-col">配送日期</th>
                                <th style="width: 5%" class="filter-col">件數</th>
                                <th style="width: 5%" class="filter-col">重量(KG)</th>
                                @can('freight_money')
                                    <th style="width: 5%" class="filter-col">單件運費</th>
                                    <th style="width: 5%" class="filter-col">運費小計</th>
                                    <th style="width: 5%" class="filter-col">聯運費</th>
                                    <th style="width: 5%" class="filter-col">其他運費</th>
                                    <th style="width: 5%" class="filter-col">總運費</th>
                                @endcan
                                <th class="p-0">
                                    <table class="inner-table">
                                        <thead>
                                            <tr>
                                                <th>料號</th>
                                                <th>品名</th>
                                                <th>數量</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $idx = 1; ?>
                            @foreach ($data as $item)
                                <?php $class = '';
                                if (empty($item->detail)) {
                                    $class = 'table-warning';
                                }
                                if ($item->combined_freight != 0 || $item->other_freight != 0) {
                                    $class = 'table-danger';
                                }
                                ?>
                                <tr class="{{ $class }}">
                                    <td>{{ $idx++ }}</td>
                                    <td>
                                        @if (!empty($item->sale_no))
                                            <a href="{{ route('check_log_detail', $item->sale_no) }}">
                                                {{ $item->sale_no }}
                                            </a>
                                        @endif
                                    </td>
                                    <td>{{ $item->trans_no }}</td>
                                    <td>{{ $item->trans_name }}</td>
                                    <td>{{ $item->ship_date }}</td>
                                    <td>{{ number_format($item->qty) }}</td>
                                    <td>{{ number_format($item->weight) }}</td>
                                    @can('freight_money')
                                        <td>{{ number_format($item->avg_freight) }}</td>
                                        <td>{{ number_format($item->freight) }}</td>
                                        <td>{{ number_format($item->combined_freight) }}</td>
                                        <td>{{ number_format($item->other_freight) }}</td>
                                        <td>{{ number_format($item->total_freight) }}</td>
                                    @endcan
                                    <td class="p-0">
                                        @if (!empty($item->detail))
                                            <table class="inner-table">
                                                <tbody>
                                                    @foreach ($item->detail as $item_no => $d)
                                                        <tr>
                                                            <td>{{ $item_no }}</td>
                                                            <td class="text-left">{{ $d['item_name'] }}</td>
                                                            <td>{{ number_format($d['qty']) }}</td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop
@section('script')
    <script type="text/javascript">
        HideSidebar();
        $(document).ready(InitialDate());

        function InitialDate() {
            var today = new Date();
            var year = today.getFullYear();
            var month = today.getMonth();
            if ($("#input_start_date").val() == '') {
                $("#input_start_date").val(DateToString(new Date(year, month, 1)));
            }
            if ($("#input_end_date").val() == '') {
                $("#input_end_date").val(DateToString(new Date(year, month + 1, 0)));
            }
        }
        //上傳檔案後顯示文件名稱
        $('#input_file').change(function(e) {
            var fileName = e.target.files[0].name;
            $(this).next('.custom-file-label').html(fileName);
        });
        //上傳
        $('#btn_import,#btn_search').click(function() {
            lockScreen();
        })
        $('#modal_analyze').on('shown.bs.modal', function(e) {
            var chart_freight = c3.generate({
                bindto: "#chart_freight",
                title: {
                    text: ''
                },
                data: {
                    columns: [
                        @foreach ($kind1_analyze as $kind1 => $d)
                            ['{{ $kind1 }}', {{ $d['freight'] }}],
                        @endforeach
                    ],
                    type: 'pie',
                },
                legend: {
                    position: 'right'
                }
            });
            var chart_qty = c3.generate({
                bindto: "#chart_qty",
                title: {
                    text: ''
                },
                data: {
                    columns: [
                        @foreach ($kind1_analyze as $kind1 => $d)
                            ['{{ $kind1 }}', {{ $d['qty'] }}],
                        @endforeach
                    ],
                    type: 'pie',
                },
                legend: {
                    position: 'right'
                }
            });
            var chart_order = c3.generate({
                bindto: "#chart_order",
                title: {
                    text: ''
                },
                data: {
                    columns: [
                        @foreach ($kind1_analyze as $kind1 => $d)
                            ['{{ $kind1 }}', {{ $d['order'] }}],
                        @endforeach
                    ],
                    type: 'pie',
                },
                legend: {
                    position: 'right'
                }
            });
        })
    </script>
@stop
