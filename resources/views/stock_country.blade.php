@extends('layouts.base')
@section('css')
    <style type="text/css">

    </style>
@endsection
@section('title')
    中國商品庫存
@stop
@section('content')
    <div class="container-fluid">
        <div class="FrozenTable" style="max-height: 88vh;">
            <table class="table table-bordered table-hover sortable table-filter">
                <thead>
                    <tr>
                        <th style="width: 5%;">序</th>
                        <th style="width: 10%;">料號</th>
                        <th>品名</th>
                        <th class="filter-col" style="width: 8%;">大類</th>
                        <th style="width: 8%;">主倉</th>
                        <th style="width: 8%;">出貨倉</th>
                        <th style="width: 8%;">合計</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i = 1; ?>
                    @if (!empty($data))
                        @foreach ($data as $item_no => $item)
                            <tr>
                                <td>{{ $i++ }}</td>
                                <td>{{ $item_no }}</td>
                                <td>{{ $item['name'] }}</td>
                                <td>{{ $item['kind'] }}</td>
                                <td>{{ number_format($item['main_stock']) }}</td>
                                <td>{{ number_format($item['sale_stock']) }}</td>
                                <td>{{ number_format($item['main_stock'] + $item['sale_stock']) }}</td>
                            </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@stop
@section('script')
    <script type="text/javascript"></script>
@stop
