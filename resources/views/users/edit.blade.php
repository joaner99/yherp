@extends('layouts.base')
@section('title')
    編輯使用者
@stop
@section('content')
    <div class="container-fluid">
        @if ($errors->any())
            <div class="form-row mb-2">
                <div class="col-auto">
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        @endif
        <div class="form-row">
            <div class="col-auto">
                <a class="btn btn-secondary" href="{{ route('users.index') }}">回上頁</a>
            </div>
        </div>
        <form method="POST" action="{{ route('users.update', $user->id) }}">
            @method('PATCH')
            @csrf
            <div class="form-row mt-2">
                <div class="col-lg-3">
                    <div class="form-group">
                        <label class="text-danger" for="input_name">*名稱</label>
                        <input class="form-control" id="input_name" name="name" type="text"
                            value="{{ $user->name }}" required>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label class="text-danger" for="input_email">*電子郵件</label>
                        <input class="form-control" id="input_email" name="email" type="text"
                            value="{{ $user->email }}" required>
                    </div>
                </div>
            </div>
            <div class="form-row mt-2">
                <div class="col-lg-3">
                    <div class="form-group">
                        <label for="input_password">密碼</label>
                        <input class="form-control" id="input_password" name="password" type="password">
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label for="input_confirm-password">確認密碼</label>
                        <input class="form-control" id="input_confirm-password" name="confirm-password" type="password">
                    </div>
                </div>
            </div>
            <div class="form-row mt-2">
                <div class="col-lg-6">
                    <div class="form-group">
                        <label class="text-danger" for="select_roles">*角色清單</label>
                        <div class="form-check-list">
                            @foreach ($roles as $item)
                                <div class="form-check">
                                    <input class="form-check-input" id="input_role_{{ $item->id }}" name="roles[]"
                                        type="checkbox" value="{{ $item->id }}"
                                        {{ in_array($item->name, $userRole) ? 'checked' : '' }}>
                                    <label class="form-check-label" for="input_role_{{ $item->id }}">
                                        {{ $item->name }}
                                    </label>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            <button class="btn btn-primary" type="submit">確認</button>
        </form>
    </div>
@stop
