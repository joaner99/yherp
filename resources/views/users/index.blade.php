@extends('layouts.base')
@section('title')
    使用者管理
@stop
@section('css')
    <style type="text/css">
        .role {
            margin: 0;
            font-size: 1rem;
        }
    </style>
@endsection
@section('content')
    <div class="container-fluid">
        <div class="form-row">
            <div class="col-auto">
                <a class="btn btn-info" href="{{ route('users.create') }}">
                    <i class="bi bi-plus-lg"></i>新增使用者
                </a>
            </div>
        </div>
        <div class="form-row mt-2">
            <div class="col">
                <div class="FrozenTable" style="max-height: 85vh;">
                    <table class="table table-bordered sortable">
                        <thead>
                            <tr>
                                <th style="width: 7%;">流水號</th>
                                <th style="width: 20%;">名稱</th>
                                <th style="width: 20%;">電子郵件</th>
                                <th>權限</th>
                                <th style="width: 15%;">操作</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $key => $user)
                                <tr>
                                    <td>{{ $user->id }}</td>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>
                                        @if (!empty($user->getRoleNames()))
                                            @foreach ($user->getRoleNames() as $v)
                                                <label class="role badge badge-secondary">{{ $v }}</label>
                                            @endforeach
                                        @endif
                                    </td>
                                    <td class="d-flex justify-content-center">
                                        <a class="btn btn-warning mx-1" href="{{ route('users.edit', $user->id) }}">
                                            <i class="bi bi-pencil-fill"></i>編輯
                                        </a>
                                        <form method="POST" action="{{ route('users.destroy', $user->id) }}">
                                            @csrf
                                            <input name="_method" type="hidden" value="DELETE">
                                            <button class="btn btn-danger mx-1">
                                                <i class="bi bi-trash"></i>刪除
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="form-row mt-2">
            <div class="col">
                {!! $data->render() !!}
            </div>
        </div>
    </div>
@stop
