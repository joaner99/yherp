@extends('layouts.base')
@section('title')
    角色管理詳情
@stop
@section('content')
    <div class="container-fluid">
        <div class="form-row">
            <div class="col-auto">
                <a class="btn btn-secondary" href="{{ route('roles.index') }}">回上頁</a>
            </div>
        </div>
        <div class="form-row mt-2">
            <div class="col">
                <div class="FrozenTable" style="max-height: 87vh;">
                    <table class="table table-bordered">
                        <tbody>
                            <tr>
                                <th>名稱</th>
                                <td>{{ $role->name }}</td>
                            </tr>
                            <tr>
                                <th>權限</th>
                                <td>
                                    <div class="FrozenTable" style="max-height: 79vh;">
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>代碼</th>
                                                    <th>備註</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @if (!empty($rolePermissions))
                                                    @foreach ($rolePermissions as $v)
                                                        <tr>
                                                            <td>{{ $v->name }}</td>
                                                            <td>{{ $v->remark }}</td>
                                                        </tr>
                                                    @endforeach
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop
