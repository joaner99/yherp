@extends('layouts.base')
@section('title')
    新增角色
@stop
@section('content')
    <div class="container-fluid">
        @if ($errors->any())
            <div class="form-row mb-2">
                <div class="col-auto">
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        @endif
        <div class="form-row">
            <div class="col-auto">
                <a class="btn btn-secondary" href="{{ route('roles.index') }}">回上頁</a>
            </div>
        </div>
        <form method="POST" action="{{ route('roles.store') }}">
            @csrf
            <div class="form-row mt-2">
                <div class="col-auto">
                    <div class="form-group">
                        <label class="text-danger" for="input_name">*名稱</label>
                        <input class="form-control" id="input_name" name="name" type="text" required>
                    </div>
                </div>
            </div>
            <div class="form-row mt-2">
                <div class="col-auto">
                    <div class="form-group">
                        <label class="text-danger">*權限</label>
                        <div class="FrozenTable">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th><input id="chk_all" type="checkbox"></th>
                                        <th>代碼</th>
                                        <th>備註</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($permission as $value)
                                        <tr>
                                            <td><input name="permission[]" type="checkbox" value="{{ $value->id }}"></td>
                                            <td>{{ $value->name }}</td>
                                            <td>{{ $value->remark }}</td>
                                        </tr>
                                    @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <button class="btn btn-primary m" type="submit">確認</button>
        </form>
    </div>
@stop
@section('script')
    <script type="text/javascript">
        //全選功能
        $("#chk_all").change(check_all);
    </script>
@stop
