@extends('layouts.base')
@section('title')
    角色管理
@stop
@section('content')
    <div class="container-fluid">
        <div class="form-row">
            <div class="col-auto">
                <a class="btn btn-info" href="{{ route('roles.create') }}">
                    <i class="bi bi-plus-lg"></i>新增角色
                </a>
            </div>
        </div>
        <div class="form-row mt-2">
            <div class="col-4">
                <div class="FrozenTable" style="max-height: 80vh;">
                    <table class="table table-bordered sortable">
                        <thead>
                            <tr>
                                <th>流水號</th>
                                <th>名稱</th>
                                <th>操作</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($roles as $key => $role)
                                <tr>
                                    <td>{{ $role->id }}</td>
                                    <td>{{ $role->name }}</td>
                                    <td class="d-flex justify-content-center">
                                        <a class="btn btn-info mx-1" href="{{ route('roles.show', $role->id) }}">
                                            <i class="bi bi-info-lg"></i>詳情
                                        </a>
                                        <a class="btn btn-warning mx-1" href="{{ route('roles.edit', $role->id) }}">
                                            <i class="bi bi-pencil-fill"></i>編輯
                                        </a>
                                        <form method="POST" action="{{ route('roles.destroy', $role->id) }}">
                                            @csrf
                                            <input name="_method" type="hidden" value="DELETE">
                                            <button class="btn btn-danger mx-1">
                                                <i class="bi bi-trash"></i>刪除
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="form-row mt-2">
            <div class="col">
                {!! $roles->render() !!}
            </div>
        </div>
    </div>
@stop
