@extends('layouts.base')
@section('title')
    人工地板銷貨統計
@stop
@section('css')
    <style type="text/css">
        table.inner-table>thead>tr>th:nth-child(2),
        table.inner-table>tbody>tr>td:nth-child(2) {
            width: 100px;
        }
    </style>
@endsection
@section('content')
    <div class="container-fluid">
        <div class="form-row">
            <div class="col-auto">
                <form action="{{ route('sales.floor') }}" method="GET">
                    <div class="form-group">
                        <label for="input_date">銷貨日期</label>
                        <input id="input_date" name="date" class="form-control" type="date" value="{{ old('date') }}">
                    </div>
                    <button class="btn btn-primary" type="submit">查詢</button>
                    <span class="text-success">※排除親送、面交</span>
                </form>
            </div>
            <div class="col"></div>
            <div class="col-auto">
                <ul class="list-group">
                    <li class="list-group-item">單數：{{ $order_count }}</li>
                </ul>
            </div>
            <div class="col-4">
                <div class="FrozenTable">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th class="bg-info">包數統計</th>
                                <th style="width: 20%;">SPC</th>
                                <th style="width: 20%;">SPC(V3)</th>
                                <th style="width: 20%;">SPC(厚寬)</th>
                                <th style="width: 20%;">LVT</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($statistics as $name => $ss)
                                <tr class="{{ $name == '總計' ? 'table-secondary' : '' }}">
                                    <th class="text-right">{{ $name }}</th>
                                    <td class="text-right">
                                        <span class="text-danger font-weight-bolder">
                                            {{ $ss['SPC'] }}
                                        </span>
                                    </td>
                                    <td class="text-right">
                                        <span class="text-danger font-weight-bolder">
                                            {{ $ss['SPC(V3)'] }}
                                        </span>
                                    </td>
                                    <td class="text-right">
                                        <span class="text-danger font-weight-bolder">
                                            {{ $ss['SPC(厚寬)'] }}
                                        </span>
                                    </td>
                                    <td class="text-right">
                                        <span class="text-danger font-weight-bolder">
                                            {{ $ss['LVT'] }}
                                        </span>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="form-row mt-2">
            <div class="col">
                <div class="FrozenTable" style="max-height: 77vh;">
                    <table class="table table-bordered table-hover sortable">
                        <thead>
                            <tr>
                                <th style="width: 10%;">料號</th>
                                <th>品名</th>
                                <th style="width: 10%;">人工總數</th>
                                <th style="width: 20%;" class="p-0">
                                    <table class="inner-table">
                                        <thead>
                                            <tr>
                                                <th>銷貨單號</th>
                                                <th>數量</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (!empty($data))
                                @foreach ($data as $item)
                                    <tr>
                                        <td>{{ $item['item_no'] }}</td>
                                        <td class="text-left">{{ $item['item_name'] }}</td>
                                        <td>{{ number_format($item['total']) }}</td>
                                        <td class="p-0">
                                            <table class="inner-table">
                                                <tbody>
                                                    @foreach ($item['orders'] as $order_no => $qty)
                                                        <tr>
                                                            <td>
                                                                <a href="{{ route('check_log_detail', $order_no) }}">
                                                                    {{ $order_no }}
                                                                </a>
                                                            </td>
                                                            <td>{{ number_format($qty) }}</td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop
@section('script')
    <script type="text/javascript">
        $(document).ready(InitialDate());

        function InitialDate() {
            var today = new Date();

            if ($("#input_date").val() == '') {
                $("#input_date").val(DateToString(today));
            }
        }
    </script>
@stop
