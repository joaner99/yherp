@extends('layouts.base')
@section('css')
    <style type="text/css">
        .c3-axis-y-label {
            baseline-shift: sub;
            transform: translate(-60px, 150px);
        }

        #div_items select {
            min-height: 400px;
        }
    </style>
@endsection
@section('title')
    商品小類年度銷售
@stop
@section('content')
    <!-- Modal大類選取 -->
    <div id="modal_type_selectlist" class="modal fade">
        <div class="modal-dialog modal-xl modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="modal-title">
                        <h1>小類選擇</h1>
                    </div>
                    <button type="button" class="close" data-dismiss="modal">
                        <span>&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="form-row">
                            <div id="div_items" class="col">
                                <div class="card mt-2">
                                    <div class="card-body">
                                        <div class="form-row">
                                            <div class="col">
                                                <div class="form-group">
                                                    <label for="select_unselected_items">未選取</label>
                                                    <select class="form-control" id="select_unselected_items"
                                                        style="background-color: lightgoldenrodyellow;" multiple>
                                                        @if (!empty($typeList[3]))
                                                            @foreach ($typeList[3] as $value => $text)
                                                                <?php
                                                                $selected1 = in_array($value, $selected_types); ?>
                                                                <option value="{{ $value }}"
                                                                    style="{{ !$selected1 ? '' : 'display: none;' }}">
                                                                    {{ '【' . $value . '】' . $text }}
                                                                </option>
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-1">
                                                <div class="form-row flex-column text-center mt-5">
                                                    <div class="col mb-2">
                                                        <button id="btn_add" class="btn btn-info w-75" type="button">
                                                            <i class="bi bi-chevron-right"></i>
                                                        </button>
                                                    </div>
                                                    <div class="col mb-2">
                                                        <button id="btn_remove" class="btn btn-info w-75" type="button">
                                                            <i class="bi bi-chevron-left"></i>
                                                        </button>
                                                    </div>
                                                    <div class="col mb-2">
                                                        <button id="btn_remove_all" class="btn btn-info w-75"
                                                            type="button">
                                                            <i class="bi bi-chevron-double-left"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col">
                                                <div class="form-group">
                                                    <label for="select_selected_items">已選取</label>
                                                    <select class="form-control" id="select_selected_items"
                                                        style="background-color: aliceblue" multiple>
                                                        @if (!empty($typeList[3]))
                                                            @foreach ($typeList[3] as $value => $text)
                                                                <?php
                                                                $selected2 = in_array($value, $selected_types); ?>
                                                                <option value="{{ $value }}"
                                                                    style="{{ $selected2 ? '' : 'display: none;' }}">
                                                                    {{ '【' . $value . '】' . $text }}
                                                                </option>
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">關閉</button>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <form action="{{ route('sales.item_kind3_sales') }}" method="get" class="p-0">
            <div class="form-row">
                <div class="col-auto">
                    <div class="form-group">
                        <label for="input_s_year">開始年份</label>
                        <input id="input_s_year" name="s_year" class="form-control" type="number"
                            value="{{ old('s_year') }}" min="1" max="2999" required>
                    </div>
                </div>
                <div class="col-auto">
                    <div class="form-group">
                        <label for="input_e_year">結束年份</label>
                        <input id="input_e_year" name="e_year" class="form-control" type="number"
                            value="{{ old('e_year') }}" min="1" max="2999" required>
                    </div>
                </div>
                <div class="col-auto">
                    <button type="button" class="btn btn-info" data-toggle="modal" data-target="#modal_type_selectlist">
                        商品小類選擇
                    </button>
                </div>
            </div>
            <button class="btn btn-primary btn-sm" type="submit">查詢</button>
            <input name="types" type="hidden" value="{{ old('types') }}">
        </form>
        <div class="form-row my-4">
            <div class="col-6">
                <div id="chart"></div>
            </div>
        </div>
    </div>
@stop
@section('script')
    <script type="text/javascript">
        $(document).ready(InitialDate());

        function InitialDate() {
            var today = new Date();

            if ($("#input_s_year").val() == '') {
                $("#input_s_year").val(today.getFullYear());
            }
            if ($("#input_e_year").val() == '') {
                $("#input_e_year").val(today.getFullYear());
            }
        }
        //加入選取的商品
        $('#btn_add').click(function() {
            $('#select_unselected_items>option:selected').each(function(index, element) {
                var val = $(element).val();
                $('#select_selected_items>option[value="' + val + '"]').show();
                $(element).hide();
            });
        });
        //移除選取的商品
        $('#btn_remove').click(function() {
            $('#select_selected_items>option:selected').each(function(index, element) {
                var val = $(element).val();
                $('#select_unselected_items>option[value="' + val + '"]').show();
                $(element).hide();
            });
        });
        //移除全部的商品
        $('#btn_remove_all').click(function() {
            $('#select_selected_items>option').hide();
            $('#select_unselected_items>option').show();
        });
        //選取完成後
        $('#modal_type_selectlist').on('hide.bs.modal', function(e) {
            var items = new Array();
            $('#select_selected_items>option:visible').each(function(i, e) {
                items.push($(e).val());
            });
            $('input[name="types"]').val(JSON.stringify(items));
        })
        var chart = c3.generate({
            bindto: "#chart",
            padding: {
                top: 10,
            },
            title: {
                text: '每年總營業額'
            },
            data: {
                x: 'x',
                columns: [
                    [
                        'x',
                        @foreach ($chart['x'] as $value)
                            '{{ $value }}',
                        @endforeach
                    ],
                    [
                        '{{ $chart['data_name'] }}',
                        @foreach ($chart['y'] as $value)
                            '{{ $value }}',
                        @endforeach
                    ]
                ],
                type: 'bar',
                labels: {
                    show: true,
                    format: d3.format(',')
                }
            },
            axis: {
                x: {
                    label: {
                        text: '年度',
                        position: 'outer-right'
                    },
                    padding: {
                        left: 1,
                        right: 1
                    }
                },
                y: {
                    min: 0,
                    label: {
                        text: '營業額(未稅)',
                        position: 'outer-top'
                    },
                    tick: {
                        format: d3.format(',')
                    },
                    padding: {
                        top: 50,
                        bottom: 0
                    }
                }
            },
        });
    </script>
@stop
