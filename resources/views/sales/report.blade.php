@extends('layouts.base')
@section('css')
    <style type="text/css">
        .list-group-item {
            padding: 0.75rem 1rem;
        }
    </style>
@endsection
@section('title')
    銷貨日報表
@stop
@section('content')
    <div class="container-fluid">
        <form id="form_search" action="{{ route('sales.report') }}" method="get" class="p-1">
            <div class="form-row">
                <div class=" form-group col-auto">
                    <label class="text-info" for="input_start_date">起日</label>
                    <input id="input_start_date" name="start_date" class="form-control" type="date"
                        value="{{ old('start_date') }}" required>
                </div>
                <div class="form-group col-auto">
                    <label class="text-info" for="input_end_date">訖日</label>
                    <input id="input_end_date" name="end_date" class="form-control" type="date"
                        value="{{ old('end_date') }}" required>
                </div>
                <div class=" form-group col-auto">
                    <label class="text-warning" for="input_last_start_date">上期起日</label>
                    <input id="input_last_start_date" name="last_start_date" class="form-control" type="date"
                        value="{{ old('last_start_date') }}" required>
                </div>
                <div class="form-group col-auto">
                    <label class="text-warning" for="input_last_end_date">上期訖日</label>
                    <input id="input_last_end_date" name="last_end_date" class="form-control" type="date"
                        value="{{ old('last_end_date') }}" required>
                </div>
            </div>
            <button class="btn btn-primary" type="submit">查詢</button>
        </form>
        <div class="form-row mt-2">
            <div class="col-auto">
                日期：{{ $start_date }}～{{ $end_date }}
            </div>
            <div class="col-1"></div>
            <div class="col-auto">
                上期日期：{{ $last_start_date }}～{{ $last_end_date }}
            </div>
            <div class="col-1"></div>
            <div class="col-auto text-success">
                ※不包含以下客戶【x001】包材、【x002】半成品領料 ※以下金額都是不含稅
            </div>
        </div>
        <div class="form-row mt-2">
            <div class="col-auto">
                <ul class="list-group list-group-horizontal-lg">
                    <li class="list-group-item list-group-item-info">
                        【A】<br>未匯入營業額小計<br>{{ number_format($notImportSum) }}
                    </li>
                    <li class="list-group-item list-group-item-info">
                        【B】<br>未出貨營業額小計<br>{{ number_format($unsalesSum) }}
                    </li>
                    <li class="list-group-item list-group-item-info">
                        【C】<br>出貨營業額小計【含退回】<br>{{ number_format($salesSum) }}
                    </li>
                    <li class="list-group-item list-group-item-info">
                        【D】<br>POS機營業額(面交)小計【含退回】<br>{{ number_format($posSales) }}
                    </li>
                    <li class="list-group-item list-group-item-info">
                        【E】<br>銷貨退回小計<br>{{ number_format($returnSum) }}
                    </li>
                    <li class="list-group-item list-group-item-info">
                        【F】<br>POS機退回(面交)小計<br>{{ number_format($posReturnSum) }}
                    </li>
                </ul>
            </div>
        </div>
        <div class="form-row mt-2">
            <div class="col-auto">
                <ul class="list-group list-group-horizontal-lg">
                    <li class="list-group-item list-group-item-success">
                        【C+D-E-F】<br>營業額總計<br>{{ number_format($total) }}
                    </li>
                    <li class="list-group-item list-group-item-secondary">
                        <div>【A+B+C+D-E-F】</div>
                        <div>營業額總計(含未出)</div>
                        <?php
                        $class = '';
                        if (isset($grow)) {
                            if ($grow > 0) {
                                $class = 'text-danger';
                            } elseif ($grow < 0) {
                                $class = 'text-success';
                            }
                        }
                        ?>
                        <div class="text-right {{ $class }}">
                            @if (isset($grow))
                                【{{ ($grow > 0 ? '+' : '') . number_format($grow, 2) }}%】
                            @endif
                            {{ number_format($total_include_unsale) }}
                        </div>
                        <div class="text-right">【上期】{{ number_format($last_total) }}</div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="form-row my-4">
            <div class="col-xl-4 col-lg-6">
                <div id="chart"></div>
            </div>
            <div class="col-xl-4 col-lg-6">
                <div id="chart_web"></div>
            </div>
            <div class="col-xl-4 col-lg-6">
                <div id="chart_detail"></div>
            </div>
            <div class="col-xl-4 col-lg-6">
                <div id="chart_pos_detail"></div>
            </div>
            <div class="col-xl-4 col-lg-6">
                <div id="chart_total_detail"></div>
            </div>
            <div class="col-xl-8 col-lg-12">
                <div id="chart_sales_line"></div>
            </div>
        </div>
    </div>
@stop
@section('script')
    <script type="text/javascript">
        $(document).ready(InitialDate());

        function InitialDate() {
            var today = new Date();

            if ($("#input_start_date").val() == '') {
                $("#input_start_date").val(DateToString(today));
            }
            if ($("#input_end_date").val() == '') {
                $("#input_end_date").val(DateToString(today));
            }
            if ($("#input_last_start_date").val() == '') {
                $("#input_last_start_date").val(DateToString(today.addDays(-30)));
            }
            if ($("#input_last_end_date").val() == '') {
                $("#input_last_end_date").val(DateToString(today.addDays(-30)));
            }
        }
        var chart = c3.generate({
            bindto: "#chart",
            title: {
                text: '渠道銷售比例'
            },
            data: {
                // iris data from R
                columns: [
                    ['線上', {{ $salesSum - $returnSum }}],
                    ['線下', {{ $posSales }}],
                ],
                type: 'pie',
                onclick: function(d, i) {
                    console.log("onclick", d, i);
                },
                onmouseover: function(d, i) {
                    console.log("onmouseover", d, i);
                },
                onmouseout: function(d, i) {
                    console.log("onmouseout", d, i);
                }
            }
        });
        var chart_web = c3.generate({
            bindto: "#chart_web",
            title: {
                text: '線上平台銷售比例'
            },
            data: {
                // iris data from R
                columns: [
                    @foreach ($salesWebSum as $key => $value)
                        ['{{ $key }}', {{ $value }}],
                    @endforeach
                ],
                type: 'pie',
                onclick: function(d, i) {
                    console.log("onclick", d, i);
                },
                onmouseover: function(d, i) {
                    console.log("onmouseover", d, i);
                },
                onmouseout: function(d, i) {
                    console.log("onmouseout", d, i);
                }
            }
        });
        var chart_detail = c3.generate({
            bindto: "#chart_detail",
            title: {
                text: '線上產品銷售比例'
            },
            data: {
                // iris data from R
                columns: [
                    @foreach ($salesDetails as $key => $value)
                        ['{{ $key }}', {{ $value }}],
                    @endforeach

                ],
                type: 'pie',
                onclick: function(d, i) {
                    console.log("onclick", d, i);
                },
                onmouseover: function(d, i) {
                    console.log("onmouseover", d, i);
                },
                onmouseout: function(d, i) {
                    console.log("onmouseout", d, i);
                }
            }
        });
        var chart_pos_detail = c3.generate({
            bindto: "#chart_pos_detail",
            title: {
                text: '線下產品銷售比例'
            },
            data: {
                // iris data from R
                columns: [
                    @foreach ($posDetails as $key => $value)
                        ['{{ $key }}', {{ $value }}],
                    @endforeach

                ],
                type: 'pie',
                onclick: function(d, i) {
                    console.log("onclick", d, i);
                },
                onmouseover: function(d, i) {
                    console.log("onmouseover", d, i);
                },
                onmouseout: function(d, i) {
                    console.log("onmouseout", d, i);
                }
            }
        });
        var chart_total_detail = c3.generate({
            bindto: "#chart_total_detail",
            title: {
                text: '總產品銷售比例'
            },
            data: {
                // iris data from R
                columns: [
                    @foreach ($totalDetails as $key => $value)
                        ['{{ $key }}', {{ $value }}],
                    @endforeach
                ],
                type: 'pie',
            }
        });
        var chart_sales_line = c3.generate({
            bindto: "#chart_sales_line",
            padding: {
                top: 10,
            },
            title: {
                text: '每日營業額(總計)'
            },
            data: {
                x: 'x',
                columns: [
                    [
                        'x',
                        @foreach ($salesEachDay as $key => $value)
                            '{{ $key }}',
                        @endforeach
                    ],
                    [
                        '營業額',
                        @foreach ($salesEachDay as $value)
                            '{{ $value }}',
                        @endforeach
                    ]
                ],
                type: 'line',
                onclick: function(d, i) {
                    console.log("onclick", d, i);
                },
                onmouseover: function(d, i) {
                    console.log("onmouseover", d, i);
                },
                onmouseout: function(d, i) {
                    console.log("onmouseout", d, i);
                }
            },
            axis: {
                x: {
                    label: {
                        text: '日期',
                        position: 'outer-right'
                    },
                    tick: {
                        format: '%m-%d',
                        culling: true,
                        centered: true
                    },
                    type: 'timeseries',
                    padding: {
                        left: 1000 * 60 * 60 * 12,
                        right: 1000 * 60 * 60 * 12
                    }
                },
                y: {
                    min: 0,
                    label: {
                        text: '營業額',
                        position: 'outer-top'
                    },
                    tick: {
                        format: d3.format(',')
                    },
                    padding: 0
                }
            },
        });
    </script>
@stop
