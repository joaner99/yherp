@extends('layouts.base')
@section('css')
    <style type="text/css">
        .c3-axis-y-label {
            baseline-shift: sub;
            transform: translate(-65px, 150px);
        }

        .select-list .form-group {
            margin-bottom: 0;
        }

        #modal_type_selectlist select {
            min-height: 200px;
        }
    </style>
@endsection
@section('title')
    商品類別銷售
@stop
@section('content')
    <!-- Modal 商品類別選取 -->
    <div id="modal_type_selectlist" class="modal fade">
        <div class="modal-dialog modal-xl modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="modal-title">
                        <h1>商品類別選取</h1>
                    </div>
                    <button type="button" class="close" data-dismiss="modal">
                        <span>&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="form-row">
                            <div class="col-auto">
                                <div class="form-check-list form-check-list-horizontal">
                                    <div class="form-check">
                                        <input class="form-check-input" id="input_item_kind1" name="item_kind"
                                            type="radio" value="1"
                                            {{ empty(old('y_type')) || old('y_type') == 1 ? 'checked' : '' }}>
                                        <label class="form-check-label" for="input_item_kind1">商品大類</label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" id="input_item_kind3" name="item_kind"
                                            type="radio" value="3" {{ old('y_type') == 3 ? 'checked' : '' }}>
                                        <label class="form-check-label" for="input_item_kind3">商品小類</label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" id="input_item_kind4" name="item_kind"
                                            type="radio" value="4" {{ old('y_type') == 4 ? 'checked' : '' }}>
                                        <label class="form-check-label" for="input_item_kind4">商品料號</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="div_item_kind1" class="form-row"
                            style="{{ !empty(old('y_type')) && old('y_type') != 1 ? 'display: none;' : '' }}">
                            <div class="col">
                                <div class="card mt-2">
                                    <div class="card-header">
                                        商品大類
                                    </div>
                                    <div class="card-body">
                                        <div class="form-row select-list">
                                            <div class="col">
                                                <div class="form-group">
                                                    <label>未選取</label>
                                                    <select class="form-control select_unselected_items"
                                                        style="background-color: lightgoldenrodyellow;" multiple>
                                                        @if (!empty($typeList[1]))
                                                            @foreach ($typeList[1] as $value => $text)
                                                                <?php
                                                                $selected1 = old('y_type') == 1 && in_array($value, $selected_types); ?>
                                                                <option value="{{ $value }}"
                                                                    style="{{ !$selected1 ? '' : 'display: none;' }}">
                                                                    {{ '【' . $value . '】' . $text }}
                                                                </option>
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-1">
                                                <div class="form-row flex-column text-center mt-5">
                                                    <div class="col mb-2">
                                                        <button class="btn btn-info btn_add w-75" type="button">
                                                            <i class="bi bi-chevron-right"></i>
                                                        </button>
                                                    </div>
                                                    <div class="col mb-2">
                                                        <button class="btn btn-info btn_remove w-75" type="button">
                                                            <i class="bi bi-chevron-left"></i>
                                                        </button>
                                                    </div>
                                                    <div class="col mb-2">
                                                        <button class="btn btn-info btn_remove_all w-75" type="button">
                                                            <i class="bi bi-chevron-double-left"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col">
                                                <div class="form-group">
                                                    <label>已選取</label>
                                                    <select
                                                        class="form-control select_selected_items"style="background-color: aliceblue"
                                                        multiple>
                                                        @if (!empty($typeList[1]))
                                                            @foreach ($typeList[1] as $value => $text)
                                                                <?php
                                                                $selected2 = old('y_type') == 1 && in_array($value, $selected_types); ?>
                                                                <option value="{{ $value }}"
                                                                    style="{{ $selected2 ? '' : 'display: none;' }}">
                                                                    {{ '【' . $value . '】' . $text }}
                                                                </option>
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="div_item_kind3" class="form-row" style="{{ old('y_type') != 3 ? 'display: none;' : '' }}">
                            <div class="col">
                                <div class="card mt-2">
                                    <div class="card-header">
                                        商品小類
                                    </div>
                                    <div class="card-body">
                                        <div class="form-row select-list">
                                            <div class="col">
                                                <div class="form-group">
                                                    <label>未選取</label>
                                                    <select class="form-control select_unselected_items"
                                                        style="background-color: lightgoldenrodyellow;" multiple>
                                                        @if (!empty($typeList[3]))
                                                            @foreach ($typeList[3] as $value => $text)
                                                                <?php
                                                                $selected1 = old('y_type') == 3 && in_array($value, $selected_types); ?>
                                                                <option value="{{ $value }}"
                                                                    style="{{ !$selected1 ? '' : 'display: none;' }}">
                                                                    {{ '【' . $value . '】' . $text }}
                                                                </option>
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-1">
                                                <div class="form-row flex-column text-center mt-5">
                                                    <div class="col mb-2">
                                                        <button class="btn btn-info btn_add w-75" type="button">
                                                            <i class="bi bi-chevron-right"></i>
                                                        </button>
                                                    </div>
                                                    <div class="col mb-2">
                                                        <button class="btn btn-info btn_remove w-75" type="button">
                                                            <i class="bi bi-chevron-left"></i>
                                                        </button>
                                                    </div>
                                                    <div class="col mb-2">
                                                        <button class="btn btn-info btn_remove_all w-75" type="button">
                                                            <i class="bi bi-chevron-double-left"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col">
                                                <div class="form-group">
                                                    <label>已選取</label>
                                                    <select class="form-control select_selected_items"
                                                        style="background-color: aliceblue" multiple>
                                                        @if (!empty($typeList[3]))
                                                            @foreach ($typeList[3] as $value => $text)
                                                                <?php
                                                                $selected2 = old('y_type') == 3 && in_array($value, $selected_types); ?>
                                                                <option value="{{ $value }}"
                                                                    style="{{ $selected2 ? '' : 'display: none;' }}">
                                                                    {{ '【' . $value . '】' . $text }}
                                                                </option>
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="div_item_kind4" class="form-row"
                            style="{{ old('y_type') != 4 ? 'display: none;' : '' }}">
                            <div class="col">
                                <div class="card mt-2">
                                    <div class="card-header">
                                        商品料號
                                    </div>
                                    <div class="card-body">
                                        <div class="form-row select-list">
                                            <div class="col">
                                                <div class="form-group">
                                                    <label>未選取</label>
                                                    <select class="form-control select_unselected_items"
                                                        style="background-color: lightgoldenrodyellow;" multiple>
                                                        @if (!empty($items))
                                                            @foreach ($items as $item_no => $item_name)
                                                                <?php
                                                                $selected1 = old('y_type') == 4 && in_array($item_no, $selected_types); ?>
                                                                <option value="{{ $item_no }}"
                                                                    style="{{ !$selected1 ? '' : 'display: none;' }}">
                                                                    {{ '【' . $item_no . '】' . $item_name }}
                                                                </option>
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-1">
                                                <div class="form-row flex-column text-center mt-5">
                                                    <div class="col mb-2">
                                                        <button class="btn btn-info btn_add w-75" type="button">
                                                            <i class="bi bi-chevron-right"></i>
                                                        </button>
                                                    </div>
                                                    <div class="col mb-2">
                                                        <button class="btn btn-info btn_remove w-75" type="button">
                                                            <i class="bi bi-chevron-left"></i>
                                                        </button>
                                                    </div>
                                                    <div class="col mb-2">
                                                        <button class="btn btn-info btn_remove_all w-75" type="button">
                                                            <i class="bi bi-chevron-double-left"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col">
                                                <div class="form-group">
                                                    <label>已選取</label>
                                                    <select class="form-control select_selected_items"
                                                        style="background-color: aliceblue" multiple>
                                                        @if (!empty($items))
                                                            @foreach ($items as $item_no => $item_name)
                                                                <?php
                                                                $selected2 = old('y_type') == 4 && in_array($item_no, $selected_types); ?>
                                                                <option value="{{ $item_no }}"
                                                                    style="{{ $selected2 ? '' : 'display: none;' }}">
                                                                    {{ '【' . $item_no . '】' . $item_name }}
                                                                </option>
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="div_unit" class="form-row mt-2">
                            <div class="col">
                                <div class="FrozenTable" style="max-height: 250px;">
                                    <table class="table table-bordered">
                                        <caption>小類數量換算</caption>
                                        <thead>
                                            <tr>
                                                <th>商品</th>
                                                <th style="width: 15%;">數量換算</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($typeList[3] as $value => $text)
                                                <?php
                                                $selected2 = old('y_type') == 3 && in_array($value, $selected_types); ?>
                                                <tr data-id="{{ $value }}"
                                                    style="{{ $selected2 ? '' : 'display: none;' }}">
                                                    <td class="text-left">{{ '【' . $value . '】' . $text }}</td>
                                                    <td>
                                                        <div class="input-group input-group-sm">
                                                            <input class="form-control text-right" type="number"
                                                                value="{{ $units[$value] ?? 1 }}">
                                                            <div class="input-group-append">
                                                                <span class="input-group-text">：1</span>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">關閉</button>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <form action="{{ route('sales.item_sales') }}" method="get" class="p-0">
            <div class="form-row">
                <div class="col-auto">
                    <div class="form-group">
                        <label for="input_s_date">銷貨起日</label>
                        <input id="input_s_date" name="s_date" class="form-control" type="date"
                            value="{{ old('s_date') }}" required>
                    </div>
                </div>
                <div class="col-auto">
                    <div class="form-group">
                        <label for="input_e_date">銷貨迄日</label>
                        <input id="input_e_date" name="e_date" class="form-control" type="date"
                            value="{{ old('e_date') }}" required>
                    </div>
                </div>
                <div class="form-group col-auto">
                    <label>X軸</label>
                    <div class="form-check-list-horizontal">
                        <div class="form-check">
                            <input class="form-check-input" id="input_x_type_1" name="x_type" type="radio"
                                value="1" {{ empty(old('x_type')) || old('x_type') == 1 ? 'checked' : '' }}>
                            <label class="form-check-label" for="input_x_type_1">年份</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" id="input_x_type_2" name="x_type" type="radio"
                                value="2" {{ old('x_type') == 2 ? 'checked' : '' }}>
                            <label class="form-check-label" for="input_x_type_2">月份</label>
                        </div>
                    </div>
                </div>
                <div class="form-group col-auto">
                    <label>Y軸</label>
                    <div class="form-check-list-horizontal">
                        <div class="form-check">
                            <input class="form-check-input" id="input_y_unit_1" name="y_unit" type="radio"
                                value="1" {{ empty(old('y_unit')) || old('y_unit') == 1 ? 'checked' : '' }}>
                            <label class="form-check-label" for="input_y_unit_1">營業額(未稅)</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" id="input_y_unit_2" name="y_unit" type="radio"
                                value="2" {{ old('y_unit') == 2 ? 'checked' : '' }}>
                            <label class="form-check-label" for="input_y_unit_2">銷售數</label>
                        </div>
                    </div>
                </div>
                <div class="form-group col-auto">
                    <label>類別計算</label>
                    <div class="form-check-list-horizontal">
                        <div class="form-check">
                            <input class="form-check-input" id="input_item_merge_1" name="item_merge" type="radio"
                                value="1" {{ old('item_merge') == 1 ? 'checked' : '' }}>
                            <label class="form-check-label" for="input_item_merge_1">合併</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" id="input_item_merge_0" name="item_merge" type="radio"
                                value="0" {{ empty(old('item_merge')) ? 'checked' : '' }}>
                            <label class="form-check-label" for="input_item_merge_0">分開</label>
                        </div>
                    </div>
                </div>
                <div class="col-auto">
                    <label>商品類別</label>
                    <div>
                        <button type="button" class="btn btn-info" data-toggle="modal"
                            data-target="#modal_type_selectlist">
                            選擇
                        </button>
                    </div>
                </div>
            </div>
            <button class="btn btn-primary btn-sm" type="submit">查詢</button>
            <input name="types" type="hidden" value="{{ old('types') }}">
            <input name="y_type" type="hidden" value="{{ old('y_type') }}">
            <input name="units" type="hidden" value="{{ old('units') }}">
        </form>
        <div class="form-row my-4">
            <div class="col">
                <div id="chart"></div>
            </div>
        </div>
        <div class="form-row mt-2">
            <div class="col">
                <div class="FrozenTable" style="max-height: 39vh;">
                    <table id="table_data" class="table table-bordered">
                        <caption>
                            <form action="{{ route('export_table') }}" method="post">
                                @csrf
                                <input name="export_name" type="hidden" value="商品類別銷售">
                                <input name="export_data" type="hidden">
                                <button class="btn btn-success" type="submit" export-target="#table_data">
                                    <i class="bi bi-file-earmark-excel"></i>匯出Excel
                                </button>
                            </form>
                        </caption>
                        <thead>
                            <tr>
                                <th></th>
                                @foreach ($chart['x'] as $value)
                                    <th>{{ $value }}</th>
                                @endforeach
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($chart['y'] as $y)
                                <tr>
                                    @foreach ($y as $idx => $value)
                                        <td>{{ $idx == 0 ? $value : number_format($value) }}</td>
                                    @endforeach
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop
@section('script')
    <script type="text/javascript">
        $(document).ready(InitialDate());

        function InitialDate() {
            var today = new Date();

            if ($("#input_s_date").val() == '') {
                $("#input_s_date").val(DateToString(today));
            }
            if ($("#input_e_date").val() == '') {
                $("#input_e_date").val(DateToString(today));
            }
        }
        //商品類別選取開啟
        $('button[data-target="#modal_type_selectlist"]').click(function() {
            var y_unit = $('input[name="y_unit"]:checked').val();
        });
        //通用類與指定大中小類切換
        $('[name="item_kind"]').on('change', function() {
            if ($('#input_item_kind1').prop('checked')) { //大類
                $('#div_item_kind1').show();
                $('#div_item_kind3,#div_unit,#div_item_kind4').hide();
            } else if ($('#input_item_kind3').prop('checked')) { //小類
                $('#div_item_kind3,#div_unit').show();
                $('#div_item_kind1,#div_item_kind4').hide();
            } else if ($('#input_item_kind4').prop('checked')) { //料號
                $('#div_item_kind4').show();
                $('#div_item_kind1,#div_unit,#div_item_kind3').hide();
            }
        });
        //雙擊觸發加入
        $('.select-list .select_unselected_items>option').on("dblclick", function() {
            var obj = $(this).closest('.select-list');
            $(obj).find('.btn_add').trigger('click');
        });
        //雙擊觸發移除
        $('.select-list .select_selected_items>option').on("dblclick", function() {
            var obj = $(this).closest('.select-list');
            $(obj).find('.btn_remove').trigger('click');
        });
        //加入選取的商品
        $('.select-list .btn_add').click(function() {
            var obj = $(this).closest('.select-list');
            var show_unit = $('input[name="item_kind"]:checked').val() == 3;
            $(obj).find('.select_unselected_items>option:selected').each(function(index, element) {
                var val = $(element).val();
                $(obj).find('.select_selected_items>option[value="' + val + '"]').show();
                $(element).hide();
                if (show_unit) {
                    $('tr[data-id="' + val + '"]').show();
                }
            });
        });
        //移除選取的商品
        $('.select-list .btn_remove').click(function() {
            var obj = $(this).closest('.select-list');
            var show_unit = $('input[name="item_kind"]:checked').val() == 3;
            $(obj).find('.select_selected_items>option:selected').each(function(index, element) {
                var val = $(element).val();
                $(obj).find('.select_unselected_items>option[value="' + val + '"]').show();
                $(element).hide();
                if (show_unit) {
                    $('tr[data-id="' + val + '"]').hide();
                }
            });
        });
        //移除全部的商品
        $('.select-list .btn_remove_all').click(function() {
            var obj = $(this).closest('.select-list');
            var show_unit = $('input[name="item_kind"]:checked').val() == 3;
            $(obj).find('.select_selected_items>option').hide();
            $(obj).find('.select_unselected_items>option').show();
            if (show_unit) {
                $('tr[data-id]').hide();
            }
        });
        //選取完成後
        $('#modal_type_selectlist').on('hide.bs.modal', function(e) {
            var items = new Array();
            $('.select_selected_items:visible>option:visible').each(function(i, e) {
                items.push($(e).val());
            });
            $('input[name="types"]').val(JSON.stringify(items));
            $('input[name="y_type"]').val($('input[name="item_kind"]:checked').val());
            //小類數量換算藏值
            var units = [];
            $('tr[data-id]:visible').each(function(i, e) {
                var val = $(e).find('input').val();
                if (val == 1 || val == 0) {
                    return;
                }
                units.push({
                    kind: $(e).data().id,
                    val: val,
                });
            });
            $('input[name="units"]').val(JSON.stringify(units));
        })
        var chart = c3.generate({
            bindto: "#chart",
            padding: {
                top: 10,
                left: 100,
            },
            title: {
                text: "{{ $chart['title'] ?? '' }}"
            },
            data: {
                x: 'x',
                columns: [
                    [
                        'x',
                        @foreach ($chart['x'] as $value)
                            '{{ $value }}',
                        @endforeach
                    ],
                    @foreach ($chart['y'] as $y)
                        [
                            @foreach ($y as $value)
                                '{{ $value }}',
                            @endforeach
                        ],
                    @endforeach
                ],
                type: 'bar',
                labels: {
                    show: true,
                    format: d3.format(',')
                }
            },
            zoom: {
                enabled: true
            },
            axis: {
                x: {
                    label: {
                        text: "{{ $chart['axis_x'] ?? '' }}",
                        position: 'outer-right'
                    },
                    padding: {
                        left: 1,
                        right: 1
                    },
                    type: 'category',
                },
                y: {
                    min: 0,
                    label: {
                        text: "{{ $chart['axis_y'] ?? '' }}",
                        position: 'outer-top'
                    },
                    tick: {
                        format: d3.format(',')
                    },
                    padding: {
                        top: 50,
                        bottom: 0
                    }
                }
            },
        });
    </script>
@stop
