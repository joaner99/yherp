@extends('layouts.base')
@section('css')
    <style type="text/css">
        #div_items select {
            min-height: 400px;
        }

        .c3-axis-y-label {
            transform: translate(-70px, 120px);
        }
    </style>
@endsection
@section('title')
    平台銷售
@stop
@section('content')
    <!-- Modal料號選取 -->
    <div id="modal_item_selectlist" class="modal fade">
        <div class="modal-dialog modal-xl modal-dialog-centered" style="max-width: 90vw;">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="modal-title">
                        <h1>商品選擇</h1>
                    </div>
                    <button type="button" class="close" data-dismiss="modal">
                        <span>&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="form-row">
                            <div id="div_items" class="col">
                                <div class="card mt-2">
                                    <div class="card-body">
                                        <div class="form-row">
                                            <div class="col">
                                                <div class="form-group">
                                                    <label for="select_unselected_items">未選取</label>
                                                    <select class="form-control" id="select_unselected_items"
                                                        style="background-color: lightgoldenrodyellow;" multiple>
                                                        @if (!empty($items))
                                                            @foreach ($items as $item)
                                                                <?php
                                                                $selected1 = in_array($item->item_no, $selected_items); ?>
                                                                <option value="{{ $item->item_no }}"
                                                                    style="{{ !$selected1 ? '' : 'display: none;' }}">
                                                                    {{ '【' . $item->item_no . '】' . $item->item_name }}
                                                                </option>
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-1">
                                                <div class="form-row flex-column text-center mt-5">
                                                    <div class="col mb-2">
                                                        <button id="btn_add" class="btn btn-info w-75" type="button">
                                                            <i class="bi bi-chevron-right"></i>
                                                        </button>
                                                    </div>
                                                    <div class="col mb-2">
                                                        <button id="btn_remove" class="btn btn-info w-75" type="button">
                                                            <i class="bi bi-chevron-left"></i>
                                                        </button>
                                                    </div>
                                                    <div class="col mb-2">
                                                        <button id="btn_remove_all" class="btn btn-info w-75"
                                                            type="button">
                                                            <i class="bi bi-chevron-double-left"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col">
                                                <div class="form-group">
                                                    <label for="select_selected_items">已選取</label>
                                                    <select class="form-control" id="select_selected_items"
                                                        style="background-color: aliceblue" multiple>
                                                        @if (!empty($items))
                                                            @foreach ($items as $item)
                                                                <?php
                                                                $selected2 = in_array($item->item_no, $selected_items); ?>
                                                                <option value="{{ $item->item_no }}"
                                                                    style="{{ $selected2 ? '' : 'display: none;' }}">
                                                                    {{ '【' . $item->item_no . '】' . $item->item_name }}
                                                                </option>
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">關閉</button>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <form action="{{ route('sales.cust_item') }}" method="get" class="p-0">
            <div class="form-row">
                <div class="form-group col-auto">
                    <label for="input_start_date">起日</label>
                    <input id="input_start_date" name="start_date" class="form-control" type="date"
                        value="{{ old('start_date') }}" required>
                </div>
                <div class="form-group col-auto">
                    <label for="input_end_date">訖日</label>
                    <input id="input_end_date" name="end_date" class="form-control" type="date"
                        value="{{ old('end_date') }}" required>
                </div>
                <div class="col-auto">
                    <button type="button" class="btn btn-info" data-toggle="modal" data-target="#modal_item_selectlist">
                        商品選擇
                    </button>
                </div>
            </div>
            <button class="btn btn-primary btn-sm" type="submit">查詢</button>
            <input name="items" type="hidden" value="{{ old('items') }}">
        </form>
        <div class="form-row mt-2">
            <div class="col">
                @if (!empty($cust_sale))
                    <div class="FrozenTable" style="max-height: 78vh;">
                        <table class="table table-bordered sortable">
                            <thead>
                                <tr>
                                    <th style="width: 8%;">序</th>
                                    <th>客代簡稱</th>
                                    <th style="width: 14%;">銷售數</th>
                                    <th style="width: 18%;">銷售額</th>
                                    <th style="width: 18%;">去年銷售額</th>
                                    <th style="width: 16%;">圖表</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $index = 1; ?>
                                @foreach ($cust_sale as $item)
                                    <tr>
                                        <td>{{ $index++ }}</td>
                                        <td>{{ $item['cust_name'] }}</td>
                                        <td>{{ number_format($item['qty']) }}</td>
                                        <td>{{ number_format($item['sales_total']) }}</td>
                                        <td>{{ number_format($item['last_sales_total']) }}</td>
                                        <td>
                                            <button class="btn btn-info btn-sm" type="button"
                                                onclick="chart1('{{ $item['cust_name'] }}')">
                                                對比走勢
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                @else
                    <div class="alert alert-danger" style="text-align: center;">
                        找不到資料
                    </div>
                @endif
            </div>
            <div class="col-7" style="display: none;">
                <div id="chart"></div>
            </div>
        </div>
    </div>
    <datalist id="item_list">
        @if (!empty($items))
            @foreach ($items as $item)
                <option value="{{ $item->ICODE }}">{{ $item->INAME }}</option>
            @endforeach
        @endif
    </datalist>
@stop
@section('script')
    <script type="text/javascript">
        $(document).ready(function() {
            InitialDate();
        });

        var y_data = @json($chart['y']);
        var chart = c3.generate({
            bindto: "#chart",
            padding: {
                top: 10,
                left: 100
            },
            title: {
                text: '對比去年銷售額'
            },
            data: {
                columns: [],
                type: 'line',
            },
            axis: {
                x: {
                    type: 'category',
                    categories: [
                        @foreach ($chart['x'] as $value)
                            '{{ $value }}',
                        @endforeach
                    ],
                    label: {
                        text: '月份',
                        position: 'outer-right'
                    },
                },
                y: {
                    min: 0,
                    label: {
                        text: '營業額',
                        position: 'outer-top'
                    },
                    tick: {
                        format: d3.format(',')
                    },
                }
            },
        });

        function InitialDate() {
            var today = new Date();
            var oneMonthAgo = today.addDays(-30);

            if ($("#input_start_date").val() == '') {
                $("#input_start_date").val(DateToString(oneMonthAgo));
            }
            if ($("#input_end_date").val() == '') {
                $("#input_end_date").val(DateToString(today));
            }
        }

        //顯示圖表
        function chart1(cust) {
            $('#chart').parent('div').show();
            $('#chart .c3-title').text('【' + cust + '】對比去年銷售額');
            var data = y_data[cust];
            chart.load({
                unload: true,
                columns: [
                    data['當年'],
                    data['去年']
                ],
            });
        }

        //加入選取的商品
        $('#btn_add').click(function() {
            $('#select_unselected_items>option:selected').each(function(index, element) {
                var val = $(element).val();
                $('#select_selected_items>option[value="' + val + '"]').show();
                $(element).hide();
            });
        });
        //移除選取的商品
        $('#btn_remove').click(function() {
            $('#select_selected_items>option:selected').each(function(index, element) {
                var val = $(element).val();
                $('#select_unselected_items>option[value="' + val + '"]').show();
                $(element).hide();
            });
        });
        //移除全部的商品
        $('#btn_remove_all').click(function() {
            $('#select_selected_items>option').hide();
            $('#select_unselected_items>option').show();
        });
        //選取完成後
        $('#modal_item_selectlist').on('hide.bs.modal', function(e) {
            var items = new Array();
            $('#select_selected_items>option:visible').each(function(i, e) {
                items.push($(e).val());
            });
            $('input[name="items"]').val(JSON.stringify(items));
        })
    </script>
@stop
