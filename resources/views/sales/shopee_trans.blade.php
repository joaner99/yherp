@extends('layouts.base')
@section('css')
    <style type="text/css">
    </style>
@endsection
@section('title')
    蝦皮出貨分析
@stop
@section('content')
    {{-- Modal 上傳 --}}
    <div id="modal_upload" class="modal">
        <div class="modal-dialog modal-xl modal-dialog-centered">
            <div class="modal-content">
                <form action="{{ route('sales.import') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title">匯入訂單</h5>
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">蝦皮</span>
                            </div>
                            <div class="custom-file">
                                <input class="custom-file-input" id="input_file" name="file[]" type="file"
                                    accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,.xls" multiple
                                    required>
                                <label class="custom-file-label" for="input_file">請選擇檔案</label>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">確認</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">關閉</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="form-row">
            <div class="col-auto">
                <button class="btn btn-success" type="button" data-toggle="modal" data-target="#modal_upload">
                    匯入訂單
                </button>
            </div>
        </div>
        <div class="form-row mt-2">
            <div class="col-auto">
                <div class="card">
                    <div class="card-header bg-secondary text-white">
                        單數：<span name="order_count">{{ count($data) }}</span>
                    </div>
                    <div class="card-body p-2">
                        <ul class="list-group list-group-horizontal">
                            <li class="list-group-item bg-info text-white p-2">
                                成立到出貨耗時平均：<span name="avg_order_to_ship">{{ $avg_order_to_ship }}</span>時
                            </li>
                            <li class="list-group-item bg-success text-white p-2">
                                付款到出貨耗時平均：<span name="avg_pay_to_ship">{{ $avg_pay_to_ship }}</span>時
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-auto d-flex align-items-end">
                <span class="text-success">※排除訂單不成立、尚未出貨</span>
            </div>
        </div>
        <div class="form-row mt-2">
            <div class="col">
                <div class="FrozenTable" style="max-height: 74vh;">
                    <table class="table table-bordered table-hover sortable table-filter">
                        <thead>
                            <tr>
                                <th>序</th>
                                <th>訂單編號</th>
                                <th class="filter-col">訂單成立日期</th>
                                <th class="filter-col">寄送方式</th>
                                <th>訂單成立時間</th>
                                <th>買家付款時間</th>
                                <th>實際出貨時間</th>
                                <th>成立到出貨耗時(時)</th>
                                <th>付款到出貨耗時(時)</th>
                            </tr>
                        </thead>
                        <tbody id="tbody_data">
                            @if (!empty($data))
                                <?php $idx = 1; ?>
                                @foreach ($data as $item)
                                    <tr data-order_to_ship="{{ $item['order_to_ship'] }}"
                                        data-pay_to_ship="{{ $item['pay_to_ship'] }}">
                                        <td>{{ $idx++ }}</td>
                                        <td>{{ $item['order_no'] ?? '' }}</td>
                                        <td>{{ $item['order_date'] ?? '' }}</td>
                                        <td>{{ $item['trans'] }}</td>
                                        <td>{{ $item['order_time'] ?? '' }}</td>
                                        <td>{{ $item['pay_time'] ?? '' }}</td>
                                        <td>{{ $item['s_ship_time'] ?? '' }}</td>
                                        <td>{{ $item['order_to_ship'] ?? '' }}</td>
                                        <td>{{ $item['pay_to_ship'] ?? '' }}</td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop
@section('script')
    <script type="text/javascript">
        //上傳檔案後顯示文件名稱
        $('#input_file').change(function(e) {
            var fileName = '';
            $(e.target.files).each(function(index, element) {
                if (fileName != '') {
                    fileName += ',';
                }
                fileName += element.name;
            });
            $(this).next('.custom-file-label').html(fileName);
        });
        $('.tr-filter>th>select').on('change', function() {
            var count = 0;
            var order_to_ship = 0;
            var pay_to_ship = 0;
            $('#tbody_data>tr:visible').each(function(i, e) {
                count++;
                var data = $(e).data();
                order_to_ship += data.order_to_ship;
                pay_to_ship += data.pay_to_ship;
            });
            var avg_order_to_ship = 0;
            var avg_pay_to_ship = 0;
            if (count != 0) {
                avg_order_to_ship = Math.round(order_to_ship / count);
                avg_pay_to_ship = Math.round(pay_to_ship / count);
            } else {
                avg_order_to_ship = '';
                avg_pay_to_ship = '';
            }
            $('span[name="avg_order_to_ship"]').text(avg_order_to_ship);
            $('span[name="avg_pay_to_ship"]').text(avg_pay_to_ship);
            $('span[name="order_count"]').text(count);
        });
    </script>
@stop
