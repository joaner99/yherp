@extends('layouts.base')
@section('css')
    <style type="text/css">
    </style>
@endsection
@section('title')
    蝦幣折抵查詢
@stop
@section('content')
    <div class="container-fluid">
        <div class="form-row">
            <div class="col-auto">
                <form action="{{ route('sales.shopee_discount') }}" method="GET">
                    <div class="form-row">
                        <div class="form-group col-auto">
                            <label for="input_start_date">訂單起日</label>
                            <input id="input_start_date" name="start_date" class="form-control" type="date"
                                value="{{ old('start_date') }}" required>
                        </div>
                        <div class="form-group col-auto">
                            <label for="input_end_date">訂單訖日</label>
                            <input id="input_end_date" name="end_date" class="form-control" type="date"
                                value="{{ old('end_date') }}" required>
                        </div>
                    </div>
                    <button class="btn btn-primary btn-sm">查詢區間</button>
                </form>
            </div>
            <div class="col"></div>
            <div class="col-auto">
                <form action="{{ route('export_table') }}" method="post">
                    @csrf
                    <input name="export_name" type="hidden" value="蝦幣折抵">
                    <input name="export_data" type="hidden">
                    <button class="btn btn-success" type="submit" export-target="#table_data">
                        <i class="bi bi-file-earmark-excel"></i>匯出
                    </button>
                </form>
            </div>
        </div>
        <div class="form-row mt-2">
            <div class="col">
                <div class="FrozenTable" style="max-height: 78vh;">
                    <table id="table_data" class="table table-bordered sortable table-filter">
                        <thead>
                            <tr>
                                <th>序</th>
                                <th class="filter-col">客戶簡稱</th>
                                <th>原始訂單編號</th>
                                <th>訂單單號</th>
                                <th>金額(含稅)</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $idx = 1; ?>
                            @foreach ($data as $item)
                                <tr>
                                    <td>{{ $idx++ }}</td>
                                    <td>{{ $item->Main->cust_name }}</td>
                                    <td>{{ $item->Main->original_no }}</td>
                                    <td>
                                        <a href="{{ route('order_detail', $item->order_no) }}">
                                            {{ $item->order_no }}
                                        </a>
                                    </td>
                                    <td>{{ round($item->sales_total) }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            @foreach ($data->sortBy('Main.OCNAM')->groupBy('Main.OCNAM') as $cust_name => $group)
                                <tr>
                                    <td colspan="4">{{ $cust_name }}</td>
                                    <td>{{ number_format($group->sum('sales_total')) }}</td>
                                </tr>
                            @endforeach
                            <tr>
                                <td colspan="4">合計金額</td>
                                <td>{{ number_format($data->sum('sales_total')) }}</td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop
@section('script')
    <script type="text/javascript">
        $(document).ready(InitialDate());

        function InitialDate() {
            var today = new Date();
            if ($("#input_start_date").val() == '') {
                $("#input_start_date").val(DateToString(today));
            }
            if ($("#input_end_date").val() == '') {
                $("#input_end_date").val(DateToString(today));
            }
        }
    </script>
@stop
