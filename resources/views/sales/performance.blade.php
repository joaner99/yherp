@extends('layouts.master')
@section('master_title')
    庫存周轉率
@stop
@section('master_css')
    <style type="text/css">
        #table_data {
            width: 170%;
        }

        [class*="marketing-"] {
            width: 100%;
            font-size: 1rem;
            margin: 0.25rem 0;
        }

        .marketing-1 {
            background-color: lightgreen;
            color: #343a40;
        }

        .marketing-2 {
            background-color: #007bff;
            color: #fff;
        }

        .marketing-3 {
            background-color: darkorchid;
            color: #fff;
        }

        .form-check>.form-check-input[type=checkbox] {
            margin-top: 0.365rem;
        }

        /*checkbox size*/
        input[type="checkbox"] {
            transform: scale(1.4);
        }

        input.form-control {
            /*shadow-sm*/
            box-shadow: 0 .125rem .25rem rgba(0, 0, 0, .075) !important;
        }

        /*checkbox list*/
        .form-check-list {
            border: 1px solid #ced4da;
            border-radius: 0.25rem;
            padding: 0.25rem 1rem;
            overflow: auto;
        }

        .form-check-list-horizontal {
            display: flex;
            padding: 0.325rem 0.5rem;
            border: 1px solid #dee2e6 !important;
            border-radius: 0.25rem;
            box-shadow: 0 0.125rem 0.25rem rgb(0 0 0 / 8%);
        }

        .form-check-list>.form-check {
            margin: 6px 0;
        }

        .form-check-list-horizontal>.form-check {
            margin: 2px 6px;
        }
    </style>
@endsection
@section('master_body')
    <div class="container-fluid">
        <div class="form-row">
            <div class="col-auto border-right">
                <form action="{{ route('sales.performance') }}" method="get" class="p-0">
                    <div class="form-row">
                        <div class="form-group col-auto">
                            <label for="input_start_date">銷售起日</label>
                            <input id="input_start_date" name="start_date" class="form-control" type="date"
                                value="{{ old('start_date') }}" required>
                        </div>
                        <div class="form-group col-auto">
                            <label for="input_end_date">銷售訖日</label>
                            <input id="input_end_date" name="end_date" class="form-control" type="date"
                                value="{{ old('end_date') }}" required>
                        </div>
                    </div>
                    <button class="btn btn-primary btn-sm" type="submit">查詢</button>
                </form>
            </div>
            <div class="col"></div>
            <div class="col-auto d-flex align-items-end">
                <div id="div_marketing" class="form-group mb-0">
                    <label>行銷篩選</label>
                    <div class="form-check-list-horizontal">
                        <div class="form-check">
                            <input class="form-check-input" id="input_marketing_" type="checkbox" value=""
                                onchange="filter()" checked>
                            <label class="form-check-label" for="input_marketing_">　</label>
                        </div>
                        @foreach ($marketing as $m)
                            <div class="form-check">
                                <input class="form-check-input" id="input_marketing_{{ $m->id }}" type="checkbox"
                                    value="{{ $m->value2 }}" onchange="filter()" checked>
                                <label class="form-check-label" for="input_marketing_{{ $m->id }}">
                                    {{ $m->value2 }}
                                </label>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div id="div_rating" class="form-group mb-0 ml-2">
                    <label>評等篩選</label>
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="rbtn_rating_sys" name="filter_rating" class="custom-control-input"
                            onchange="filter()" value="sys" checked>
                        <label class="custom-control-label" for="rbtn_rating_sys">系統</label>
                    </div>
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="rbtn_rating_marketing" name="filter_rating" class="custom-control-input"
                            onchange="filter()" value="">
                        <label class="custom-control-label" for="rbtn_rating_marketing">行銷</label>
                    </div>
                    <div class="form-check-list-horizontal">
                        <div class="form-check">
                            <input class="form-check-input" id="input_rating_" type="checkbox" value=""
                                onchange="filter()" checked>
                            <label class="form-check-label" for="input_rating_">　</label>
                        </div>
                        @foreach ($rating as $r)
                            <div class="form-check">
                                <input class="form-check-input" id="input_rating_{{ $r->id }}" type="checkbox"
                                    value="{{ $r->rating }}" onchange="filter()" checked>
                                <label class="form-check-label" for="input_rating_{{ $r->id }}">
                                    {{ $r->rating }}
                                </label>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="col-3">
                <div class="form-row flex-column h-100">
                    <div class="col-auto text-right">
                        <span class="text-muted">※排除98、99大類</span>
                    </div>
                    <div class="col"></div>
                    <div class="col-auto text-right">
                        <form action="{{ route('export_table') }}" method="post">
                            @csrf
                            <input name="export_name" type="hidden" value="庫存周轉率">
                            <input name="export_data" type="hidden">
                            <button class="btn btn-success btn-sm" type="submit" export-target="#table_data">
                                <i class="bi bi-file-earmark-excel"></i>匯出Excel
                            </button>
                        </form>
                    </div>
                    <div class="col-auto mt-2">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">區間庫存金額篩選</span>
                            </div>
                            <input id="input_total_min" type="number" class="form-control text-center" value="5000"
                                onkeyup="filter()" onchange="filter()">
                            <span class="input-group-text">~</span>
                            <input id="input_total_max" type="number" class="form-control text-center" value="999999"
                                onkeyup="filter()" onchange="filter()">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-row mt-2" style="font-size: 0.9rem;">
            <div class="col-auto">
                <div class="card">
                    <div class="card-header bg-warning">
                        各系統評等『區間庫存金額』總計
                    </div>
                    <div class="card-body p-1">
                        <ul class="list-group list-group-horizontal">
                            @foreach ($rating_total as $sys_rating => $rt)
                                <li class="list-group-item">
                                    <span>{{ $sys_rating }}級【{{ $rt['proportion'] }}%】<br>{{ number_format($rt['sum']) }}</span>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-auto">
                <div class="card">
                    <div class="card-header text-white bg-success">
                        各系統評等『目前庫存金額』總計
                    </div>
                    <div class="card-body p-1">
                        <ul class="list-group list-group-horizontal">
                            @foreach ($rating_now_total as $sys_rating => $rt)
                                <li class="list-group-item">
                                    <span>{{ $sys_rating }}級【{{ $rt['proportion'] }}%】<br>{{ number_format($rt['sum']) }}</span>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-row mt-2">
            <div class="col">
                @if (!empty($data))
                    <div class="FrozenTable" style="max-height: 63vh; font-size: 0.85rem;">
                        <table id="table_data" class="table table-bordered sortable">
                            <colgroup>
                                <col span="10">
                                <col span="4" style="background-color:honeydew;">
                                <col span="3" style="background-color:antiquewhite;">
                                <col span="3" style="background-color:beige;">
                                <col span="3" style="background-color:aliceblue;">
                            </colgroup>
                            <thead>
                                <tr>
                                    <th style="width: 2%;">序</th>
                                    <th style="width: 2%;">採購</th>
                                    <th style="width: 3%;">行銷</th>
                                    <th style="width: 2%;">系統<br>評等</th>
                                    <th style="width: 2%;">行銷<br>評等</th>
                                    <th style="width: 5%;">料號</th>
                                    <th>品名</th>
                                    <th style="width: 4%;">成本</th>
                                    <th style="width: 4%;">目前<br>庫存</th>
                                    <th style="width: 4%;">目前<br>庫存金額</th>
                                    <th style="width: 4%;">區間<br>平均庫存</th>
                                    <th class="d-none" style="width: 4%;">近7日<br>平均庫存</th>
                                    <th class="d-none" style="width: 4%;">近14日<br>平均庫存</th>
                                    <th style="width: 4%;">平均庫存<br>占比</th>
                                    <th style="width: 4%;">區間<br>銷售量</th>
                                    <th class="d-none" style="width: 4%;">近7日<br>銷售量</th>
                                    <th class="d-none" style="width: 4%;">近14日<br>銷售量</th>
                                    <th style="width: 4%;">區間<br>庫存金額</th>
                                    <th class="d-none" style="width: 4%;">近7日<br>庫存金額</th>
                                    <th class="d-none" style="width: 4%;">近14日<br>庫存金額</th>
                                    <th style="width: 4%;">區間<br>庫存周轉率</th>
                                    <th class="d-none" style="width: 4%;">近7日<br>庫存周轉率</th>
                                    <th class="d-none" style="width: 4%;">近14日<br>庫存周轉率</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $index = 1; ?>
                                @foreach ($data as $item_no => $item)
                                    <tr>
                                        <td>{{ $index++ }}</td>
                                        <td>{{ empty($item['buy']) ? '' : '✔' }}</td>
                                        <td class="marketing p-1">
                                            @foreach ($item['marketing'] as $val => $text)
                                                <span
                                                    class="badge marketing-{{ $val }}">{{ $text }}</span>
                                            @endforeach
                                        </td>
                                        <td class="sys_rating">{{ $item['sys_rating'] }}</td>
                                        <td class="rating">{{ $item['rating'] }}</td>
                                        <td>{{ $item_no }}</td>
                                        <td class="text-left">{{ $item['item_name'] }}</td>
                                        <td>{{ number_format($item['cost']) }}</td>
                                        <td>{{ number_format($item['now_stock']) }}</td>
                                        <td>{{ number_format($item['now_cost_total']) }}</td>
                                        <td>{{ number_format($item['stock'], 2) }}</td>
                                        <td class="d-none">{{ number_format($item['stock2'], 2) }}</td>
                                        <td class="d-none">{{ number_format($item['stock3'], 2) }}</td>
                                        <td>{{ number_format(($item['stock'] / collect($data)->sum('stock')) * 100, 2) }}%
                                        </td>
                                        <td>{{ number_format($item['sales']) }}</td>
                                        <td class="d-none">{{ number_format($item['sales2']) }}</td>
                                        <td class="d-none">{{ number_format($item['sales3']) }}</td>
                                        <td class="cost_total">{{ number_format($item['cost_total']) }}</td>
                                        <td class="d-none">{{ number_format($item['cost_total2']) }}</td>
                                        <td class="d-none">{{ number_format($item['cost_total3']) }}</td>
                                        <td>{{ $item['performance'] }}</td>
                                        <td class="d-none">{{ $item['performance2'] }}</td>
                                        <td class="d-none">{{ $item['performance3'] }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                @else
                    <div class="alert alert-danger" style="text-align: center;">
                        找不到資料
                    </div>
                @endif
            </div>
        </div>
    </div>
@stop
@section('master_js')
    <script type="text/javascript">
        $(document).ready(function() {
            InitialDate();
            filter();
        });

        function InitialDate() {
            var today = new Date();
            var oneMonthAgo = today.addDays(-30);

            if ($("#input_start_date").val() == '') {
                $("#input_start_date").val(DateToString(oneMonthAgo));
            }
            if ($("#input_end_date").val() == '') {
                $("#input_end_date").val(DateToString(today));
            }
            $("#input_start_date").attr("max", DateToString(today.addDays(-14)));
            $("#input_end_date").attr("max", DateToString(today));
        }
        //篩選
        function filter() {
            var rating_type = $('input[name="filter_rating"]:checked').val();
            var rating_list = new Array();
            $('#div_rating input[type="checkbox"]:checked').each(function(index, element) {
                rating_list.push($(element).val());
            });
            var marketing_list = new Array();
            $('#div_marketing input:checked').each(function(index, element) {
                marketing_list.push($(element).val());
            });
            var min = Number($('#input_total_min').val());
            var max = Number($('#input_total_max').val());
            $('#table_data>tbody>tr').each(function(index, element) {
                var marketing = new Array();
                $(element).find('.marketing>span').each(function(index, element) {
                    marketing.push($(element).text());
                });
                var sys_rating = '';
                if (rating_type == 'sys') {
                    sys_rating = $(element).find('.sys_rating').text();
                } else {
                    sys_rating = $(element).find('.rating').text();
                }
                var total = $(element).find('.cost_total').text();
                total = Number(total.replaceAll(',', ''));
                if (min <= total && total <= max && rating_list.indexOf(sys_rating) != -1) {
                    if (marketing.length == 0) { //商品無行銷
                        if (marketing_list.indexOf('') != -1) { //篩選 選擇空白
                            $(element).show();
                        } else {
                            $(element).hide();
                        }
                    } else {
                        var include = false;
                        $(marketing).each(function(index, element) {
                            if (marketing_list.indexOf(element) != -1) {
                                include = true;
                                return false;
                            }
                        });
                        if (include) {
                            $(element).show();
                        } else {
                            $(element).hide();
                        }
                    }
                } else {
                    $(element).hide();
                }
            });
        }
    </script>
@stop
