@extends('layouts.base')
@section('css')
    <style type="text/css">
        .done {
            text-decoration: line-through;
        }
    </style>
@endsection
@section('title')
    群撿查詢
@stop
@section('content')
    <div class="container-fluid">
        <div class="form-row">
            <div class="col-auto">
                <div class="dropdown">
                    <button class="btn btn-secondary dropdown-toggle" type="button" data-toggle="dropdown">
                        <i class="bi bi-gear-fill"></i>設定
                    </button>
                    <div class="dropdown-menu">
                        <form action="{{ $config_url }}" method="GET">
                            <input name="redirect" value="{{ Route::currentRouteName() }}" type="hidden">
                            <button class="dropdown-item" type="submit">
                                群撿設定
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-row mt-2">
            <div class="col">
                <div class="FrozenTable" style="max-height: 87vh;">
                    <table class="table table-bordered sortable table-filter">
                        <thead>
                            <tr>
                                <th class="filter-col">群組名稱</th>
                                <th class="filter-col" style="width: 9%;">料號</th>
                                <th class="filter-col">品名</th>
                                <th class="filter-col" style="width: 8%;">物流類型</th>
                                <th style="width: 6%;">總數量</th>
                                <th style="width: 6%;">一箱數量</th>
                                <th style="width: 8%;">總箱數</th>
                                <th style="width: 6%;">已驗數量</th>
                                <th style="width: 8%;">已驗箱數</th>
                                <th style="width: 13%;">銷貨單</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (!empty($data))
                                @foreach ($data as $item_no => $item)
                                    @foreach ($item as $trans_type => $item)
                                        <tr>
                                            <td>{{ $item['group_name'] }}</td>
                                            <td>{{ $item_no }}</td>
                                            <td class="text-left">{{ $item['item_name'] }}</td>
                                            <td>{{ $trans_type }}</td>
                                            <td>{{ number_format($item['qty']) }}</td>
                                            <td>{{ $item['box'] }}</td>
                                            <td>{{ $item['box_qty'] }}</td>
                                            <td>{{ number_format($item['check_qty']) }}</td>
                                            <td>{{ $item['check_box_qty'] }}</td>
                                            <td>
                                                @foreach (collect($item['sales'])->sortBy('is_check') as $sale_no => $sale)
                                                    <div
                                                        class="d-flex justify-content-between {{ $sale['is_check'] ? 'done' : '' }}">
                                                        <a href="{{ route('check_log_detail', $sale_no) }}">
                                                            {{ $sale_no }}
                                                        </a>
                                                        <span>【{{ number_format($sale['qty']) }}】</span>
                                                    </div>
                                                @endforeach
                                            </td>
                                        </tr>
                                    @endforeach
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop
@section('script')
    <script type="text/javascript"></script>
@stop
