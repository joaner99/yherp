@extends('layouts.base')
@section('title')
    親送總表
@stop
@section('css')
    <style type="text/css">
        .list-group-item {
            padding: 0.375rem 0.75rem;
        }

        /*不配送的地區*/
        .hide_point {
            background-color: #d6d8db;
            border-color: #b3b7bb;
        }

        table.inner-table>thead>tr>th:nth-child(odd),
        table.inner-table>tbody>tr>td:nth-child(odd) {
            width: 90px;
        }

        td[name="cust_confirmed"] {
            padding: 0;
            color: var(--success);
            font-size: 1.75rem;
            font-weight: bolder;
        }

        .sche_legs i {
            margin: 0;
        }

        .sche_legs input[type="checkbox"] {
            transform: scale(1);
            box-shadow: none !important;
        }
    </style>
@endsection
@section('content')
    <!-- Modal 最佳路線 -->
    <div id="modal_map" class="modal fade">
        <div class="modal-dialog modal-xl modal-dialog-centered" style="max-width: 98vw;">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="modal-title">
                        <h1>最佳路線</h1>
                    </div>
                    <button type="button" class="close" data-dismiss="modal">
                        <span>&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-row">
                        <div class="col">
                            <div id="div_map" class="form-row">
                                <div class="col" style="height: 650px;">
                                    <div class="map w-100 h-100" style="float:left;"></div>
                                </div>
                                <div class="col-6">
                                    <div class="FrozenTable" style="max-height: 650px; font-size: 0.9rem;">
                                        <table class="table table-bordered">
                                            <caption>
                                                <span class="leg_count"></span>個點　<span class="distance_sum"></span>公里　<span
                                                    class="duration_sum"></span>　彰化出發含回程
                                            </caption>
                                            <thead>
                                                <tr>
                                                    <th>標記</th>
                                                    <th>地址</th>
                                                    <th>距離</th>
                                                    <th>時間</th>
                                                </tr>
                                            </thead>
                                            <tbody class="legs"></tbody>
                                            <tfoot class="destination"></tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">關閉</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal 自訂路線 -->
    <div id="modal_cust_map" class="modal fade">
        <div class="modal-dialog modal-xl modal-dialog-centered" style="max-width: 98vw;">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="modal-title">
                        <h1>自訂路線</h1>
                    </div>
                    <button type="button" class="close" data-dismiss="modal">
                        <span>&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="div_cust" class="form-row mt-2">
                        <div class="col">
                            <div id="div_cust_map" class="form-row">
                                <div class="col" style="height: 650px;">
                                    <div class="map w-100 h-100" style="float:left;"></div>
                                </div>
                                <div class="col-4">
                                    <div class="FrozenTable" style="max-height: 650px; font-size: 0.9rem;">
                                        <table class="table table-bordered">
                                            <caption>自訂路線</caption>
                                            <thead>
                                                <tr>
                                                    <th style="width: 100px;">順序</th>
                                                    <th>配送</th>
                                                    <th>地址</th>
                                                </tr>
                                            </thead>
                                            <tbody class="sche_legs"></tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="col-5">
                                    <div class="FrozenTable" style="max-height: 650px; font-size: 0.9rem;">
                                        <table class="table table-bordered">
                                            <caption>
                                                <span class="leg_count"></span>個點　<span class="distance_sum"></span>公里　<span
                                                    class="duration_sum"></span>　彰化出發含回程
                                            </caption>
                                            <thead>
                                                <tr>
                                                    <th>標記</th>
                                                    <th>地址</th>
                                                    <th>距離</th>
                                                    <th>時間</th>
                                                </tr>
                                            </thead>
                                            <tbody class="legs"></tbody>
                                            <tfoot class="destination"></tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button id="btn_calu" class="btn btn-primary" type="button">計算路線</button>
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">關閉</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal 統計 -->
    <div id="modal_total" class="modal fade">
        <div class="modal-dialog modal-xl modal-dialog-centered" style="max-width: 98vw;">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="modal-title">
                        <h1>統計</h1>
                        <ul class="list-group list-group-horizontal">
                            <li class="list-group-item list-group-item-info">車輛<br>限重</li>
                            @foreach ($cars as $car)
                                <li class="list-group-item list-group-item-secondary">
                                    {{ $car->name . '：' }}<br>{{ number_format($car->weight_limit) . '(KG)' }}
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    <button type="button" class="close" data-dismiss="modal">
                        <span>&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-row">
                        <div class="col">
                            <div class="FrozenTable" style="max-height: 71vh;">
                                <table class="table table-bordered sortable table-filter">
                                    <thead>
                                        <tr>
                                            <th>品名\小類</th>
                                            @foreach ($statistics_columns as $col)
                                                <th>{{ $col }}</th>
                                            @endforeach
                                        </tr>
                                    </thead>
                                    <tbody class="statistics">
                                        @foreach ($statistics as $item_name => $item)
                                            <tr>
                                                <td class="text-right">{{ $item_name }}</td>
                                                @foreach ($statistics_columns as $col)
                                                    <td>{{ isset($item[$col]) ? number_format($item[$col]) : '' }}</td>
                                                @endforeach
                                            </tr>
                                        @endforeach
                                    </tbody>
                                    <tfoot>
                                        @foreach ($statistics_total as $item_name => $item)
                                            <tr>
                                                <td class="text-right">{{ $item_name }}</td>
                                                @foreach ($statistics_columns as $col)
                                                    <td>{{ isset($item[$col]) ? number_format($item[$col]) : '' }}</td>
                                                @endforeach
                                            </tr>
                                        @endforeach
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                        <div class="col-3">
                            @if (!empty($config))
                                <div class="FrozenTable" style="max-height: 87vh; font-size: 0.85rem;">
                                    <table class="table table-bordered sortable table-filter">
                                        <thead>
                                            <tr>
                                                <th style="width: 40%;">小類</th>
                                                <th>重量(G)</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($config as $item)
                                                <tr>
                                                    <td>{{ $item->value1 }}</td>
                                                    <td>{{ number_format($item->value2) }}</td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">關閉</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal 編輯 -->
    <div id="modal_edit" class="modal fade">
        <div class="modal-dialog modal-xl modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="modal-title">
                        <h1>編輯</h1>
                    </div>
                    <button type="button" class="close" data-dismiss="modal">
                        <span>&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-row">
                        <div class="col-auto">
                            <div class="form-group">
                                <label for="input_original_no">原始訂單編號</label>
                                <input class="form-control" id="input_original_no" name="original_no" type="text"
                                    value="" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-3">
                            <label>出車日期</label>
                            <input class="form-control" name="ship_date" type="date">
                        </div>
                        <div class="form-group col-3">
                            <label>出車人員</label>
                            <select name="driver" class="custom-select">
                                <option value="" selected></option>
                                @if (!empty($drivers))
                                    @foreach ($drivers as $driver)
                                        <option value="{{ $driver->id }}">{{ $driver->name }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="form-group col-3">
                            <label>出車車輛</label>
                            <select name="car" class="custom-select">
                                <option value="" selected></option>
                                @if (!empty($cars))
                                    @foreach ($cars as $car)
                                        <option value="{{ $car->id }}">{{ $car->name . '【' . $car->no . '】' }}
                                        </option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="form-group col-3">
                            <label>是否與客戶電聯</label>
                            <div class="form-check-list form-check-list-horizontal">
                                <div class="form-check">
                                    <input class="form-check-input" id="input_cust_confirmed_1" name="cust_confirmed"
                                        type="radio" value="1">
                                    <label class="form-check-label" for="input_cust_confirmed_1">是</label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" id="input_cust_confirmed_0" name="cust_confirmed"
                                        type="radio" value="0">
                                    <label class="form-check-label" for="input_cust_confirmed_0">否</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="div_additional_addr" class="form-row">
                        <div class="col">
                            <div class="form-group">
                                <label for="input_additional_addr">額外收件者地址<small
                                        class="text-danger ml-2">※將覆蓋原先地址，編輯後請重新整理</small></label>
                                <input class="form-control" id="input_additional_addr" name="additional_addr"
                                    type="text">
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col">
                            <label>親送備註</label>
                            <label class="text-success ml-2">※200字以內</label>
                            <textarea class="form-control" name="remark" style="height: 100px;"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="form-row w-100">
                        <div id="div_src_1" class="col-auto">
                            <form action="{{ route('personal_transport.update_trans') }}" method="POST">
                                @csrf
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">改為</span>
                                    </div>
                                    <select class="custom-select" name="trans">
                                        <option value="HCT">新竹</option>
                                        <option value="OTHER2">音速</option>
                                    </select>
                                    <div class="input-group-append">
                                        <span class="input-group-text" id="basic-addon2">人工包裝</span>
                                    </div>
                                    <div class="input-group-append">
                                        <button class="btn btn-info" type="submit">確認</button>
                                    </div>
                                </div>
                                <input name="original_no" type="hidden">
                            </form>
                        </div>
                        <div id="div_src_2" class="col-auto">
                            <div class="alert alert-warning">
                                改人工，請直接進入TMS訂單修改物流
                            </div>
                        </div>
                        <div class="col"></div>
                        <div class="col-auto">
                            <button id="btn_edit_confirm" class="btn btn-primary" type="button">儲存</button>
                        </div>
                        <div class="col-auto">
                            <button class="btn btn-secondary" type="button" data-dismiss="modal">取消</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal 說明 -->
    <div id="modal_help" class="modal fade">
        <div class="modal-dialog modal-xl modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="modal-title">
                        <h1>說明</h1>
                    </div>
                    <button type="button" class="close" data-dismiss="modal">
                        <span>&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-row">
                        <div class="col">
                            <ul class="list-group">
                                <li class="list-group-item">非籃子訂單，請在TMS訂單物流改為親送，並且原始訂單編號為必填不得重複</li>
                                <li class="list-group-item">當訂單完成後，資料將不會留存</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">關閉</button>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="form-row">
            <div class="col-5">
                <form action="{{ route('personal_transport.index') }}" method="GET">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">查詢方式</span>
                        </div>
                        <select id="select_search_type" class="custom-select" name="search_type"
                            onchange="search_type_onchange(this)">
                            <option value="1">未安排</option>
                            <option value="2">依日期人員</option>
                            <option value="3">依原始訂單編號</option>
                        </select>
                        <div class="input-group-prepend" style="display: none;" data-search_type="2">
                            <span class="input-group-text">日期</span>
                        </div>
                        <input id="input_date" name="date" style="display: none;" class="form-control"
                            data-search_type="2" type="date" value="{{ old('date') }}">
                        <div class="input-group-prepend" style="display: none;" data-search_type="2">
                            <label class="input-group-text">人員</label>
                        </div>
                        <select class="custom-select" style="display: none;" name="driver" data-search_type="2">
                            <option value=""{{ empty(old('driver')) ? 'selected' : '' }}>全部</option>
                            @foreach ($drivers as $driver)
                                <option value="{{ $driver->id }}" {{ old('driver') == $driver->id ? 'selected' : '' }}>
                                    {{ $driver->name }}</option>
                            @endforeach
                        </select>
                        <div class="input-group-prepend" style="display: none;" data-search_type="3">
                            <span class="input-group-text">原始訂單編號</span>
                        </div>
                        <input id="input_date" name="original_no" style="display: none;" class="form-control"
                            data-search_type="3" type="text" value="{{ old('original_no') }}">
                        <div class="input-group-append">
                            <button class="btn btn-info" type="submit">查詢</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col"></div>
            <div class="col-auto">
                <ul class="list-group list-group-horizontal">
                    <li class="list-group-item bg-info text-white">總計：{{ array_sum($count) }}</li>
                    @foreach ($count as $area => $c)
                        <li class="list-group-item">{{ $area }}：{{ $c }}</li>
                    @endforeach
                </ul>
            </div>
            <div class="dropdown">
                <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">
                    <i class="bi bi-bar-chart"></i>功能
                </button>
                <div class="dropdown-menu">
                    <button type="button" class="dropdown-item text-info" data-toggle="modal"
                        data-target="#modal_total">
                        <i class="bi bi-bar-chart mr-2"></i>統計品項
                    </button>
                    <a class="dropdown-item text-info" href="{{ route('personal_transport.ship_day') }}">
                        <i class="bi bi-bar-chart mr-2"></i>出車天數
                    </a>
                    <button id="btn_map" type="button" class="dropdown-item text-success" data-toggle="modal"
                        data-target="#modal_map">
                        <i class="bi bi-map mr-2"></i>最佳路線
                    </button>
                    <button id="btn_cust_map" type="button" class="dropdown-item text-warning" data-toggle="modal"
                        data-target="#modal_cust_map">
                        <i class="bi bi-map mr-2"></i>自訂路線
                    </button>
                </div>
            </div>
            <div class="col"></div>
            <div class="col-auto">
                <button class="btn btn-success" type="button" data-toggle="modal" data-target="#modal_help">
                    <i class="bi bi-question-circle"></i>說明
                </button>
            </div>
            @can('personal_transport')
                <div class="col-auto">
                    <form action="{{ route('order_sample.confirm') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <button id="btn_confirm" class="btn btn-primary" type="submit">
                            <i class="bi bi-check2-square"></i>業務確認訂單
                        </button>
                        <input name="id_list" type="hidden">
                    </form>
                </div>
                <div class="dropdown">
                    <button class="btn btn-secondary dropdown-toggle" type="button" data-toggle="dropdown">
                        <i class="bi bi-gear-fill"></i>設定
                    </button>
                    <div class="dropdown-menu">
                        <form action="{{ $floor_short_name_config_url }}" method="GET">
                            <input name="redirect" value="{{ Route::currentRouteName() }}" type="hidden">
                            <button class="dropdown-item" type="submit">
                                地板縮寫設定
                            </button>
                        </form>
                        <form action="{{ $config_url }}" method="GET">
                            <input name="redirect" value="{{ Route::currentRouteName() }}" type="hidden"
                                class="dropdown-item">
                            <button class="dropdown-item" type="submit">
                                重量設定
                            </button>
                        </form>
                    </div>
                </div>
            @endcan
            <div class="col-auto">
                <form action="{{ route('personal_transport.export_shipment') }}" method="post">
                    @csrf
                    <input name="export_name" type="hidden" value="{{ $caption }}派車單">
                    <input name="export_data" type="hidden">
                    <button class="btn btn-success" type="submit" export-target="#table_data">
                        <i class="bi bi-truck"></i>匯出派車單
                    </button>
                </form>
            </div>
        </div>
        <div class="form-row mt-2">
            <div class="col">
                <div class="FrozenTable" style="max-height: 87vh; font-size: 0.85rem;">
                    <table id="table_data" class="table table-bordered sortable table-filter" style=" width: 140vw;">
                        <caption>{{ $caption }}</caption>
                        <thead>
                            <tr>
                                @can('personal_transport')
                                    <th style="width: 1%;" class="export-none sort-none">
                                        <input id="chk_all" type="checkbox">
                                    </th>
                                    <th class="export-none" style="width: 4%;">操作</th>
                                @endcan
                                <th class="filter-col export-none" style="width: 3%;">電聯</th>
                                <th class="filter-col" style="width: 4%;">出車日期</th>
                                <th class="filter-col" style="width: 3%;">出車人員</th>
                                <th class="filter-col" style="width: 5%;">出車車輛</th>
                                <th class="filter-col" style="width: 2%;">地區</th>
                                <th class="filter-col" style="width: 6%;">原始訂單編號</th>
                                <th class="export-none" style="width: 4%;">訂單單號</th>
                                <th class="export-none" style="width: 4%;">銷貨單號</th>
                                <th>親送備註</th>
                                <th class="filter-col" style="width: 5%;">收件者姓名</th>
                                <th style="width: 15%;">收件者地址</th>
                                <th style="width: 7%;">收件者電話</th>
                                <th class="p-0" style="width: 20%;">
                                    <table class="inner-table">
                                        <thead>
                                            <tr>
                                                <th>類型</th>
                                                <th>品名</th>
                                                <th>數量</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </th>
                                <th class="export-none" style="width: 4%;">總重(KG)</th>
                                <th class="export-none" style="width: 4%;">總金額(稅)</th>
                            </tr>
                        </thead>
                        <tbody id="tbody_data">
                            @foreach (collect($data)->groupBy('original_no') as $original_no => $group)
                                @foreach ($group as $value)
                                    <tr data-src="{{ $value['src'] }}" data-original_no="{{ $value['original_no'] }}"
                                        data-ship_date="{{ $value['ship_date'] }}" data-driver="{{ $value['driver'] }}"
                                        data-car="{{ $value['car'] }}" data-remark="{{ $value['remark'] }}"
                                        data-unconfirmed_id="{{ $value['unconfirmed_id'] ?? '' }}"
                                        data-cust_confirmed="{{ $value['cust_confirmed'] }}"
                                        data-receiver_address="{{ $value['receiver_address'] }}"
                                        data-additional_addr ="{{ $value['additional_addr'] }}">
                                        @can('personal_transport')
                                            <td>
                                                @if (!empty($value['unconfirmed_id']))
                                                    <input class="confirm" type="checkbox">
                                                @endif
                                            </td>
                                            <td>
                                                <button class="btn btn-warning btn-sm" data-toggle="modal"
                                                    data-target="#modal_edit">
                                                    <i class="bi bi-pencil-fill"></i>編輯
                                                </button>
                                            </td>
                                        @endcan
                                        <td name="cust_confirmed">{{ $value['cust_confirmed'] ? '✔' : '' }}</td>
                                        <td name="ship_date">{{ $value['ship_date'] }}</td>
                                        <td name="driver">{{ $value['driver_name'] }}</td>
                                        <td name="car">
                                            @empty(!$value['car_name'])
                                                {{ $value['car_name'] }}<br> {{ "【{$value['car_no']}】" }}
                                            @endempty
                                        </td>
                                        <td>{{ $value['area'] }}</td>
                                        <td>
                                            <a href="{{ route('basket_detail') . '?order_no=' . urlencode($value['original_no']) }}"
                                                class="original_no_font">
                                                {{ $value['original_no'] }}
                                            </a>
                                        </td>
                                        <td>
                                            @foreach ($value['order_no'] as $order_no)
                                                <div>
                                                    <a href="{{ route('order_detail', $order_no) }}">
                                                        {{ $order_no }}
                                                    </a>
                                                </div>
                                            @endforeach
                                        </td>
                                        <td>
                                            @foreach ($value['sale_no'] as $sale_no)
                                                <div>
                                                    <a href="{{ route('check_log_detail', $sale_no) }}">
                                                        {{ $sale_no }}
                                                    </a>
                                                </div>
                                            @endforeach
                                        </td>
                                        <td class="text-left" name="remark">{{ $value['remark'] }}</td>
                                        <td>{{ $value['receiver_name'] }}</td>
                                        <td class="text-left" name="additional_addr">
                                            @if (empty($value['additional_addr']))
                                                {{ $value['receiver_address'] }}
                                            @else
                                                {{ $value['additional_addr'] }}
                                            @endif
                                        </td>
                                        <td>{{ $value['receiver_tel'] }}</td>
                                        <td class="p-0">
                                            <table class="inner-table">
                                                <tbody>
                                                    @foreach ($value['detail'] as $item)
                                                        <tr>
                                                            <td>{{ $item['item_kind3_name'] }}</td>
                                                            <td class="text-left">{{ $item['item_name'] }}</td>
                                                            <td>{{ round($item['qty']) }}</td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </td>
                                        <td>{{ number_format($value['total_weight']) }}</td>
                                        <td>{{ number_format($value['total_tax']) }}</td>
                                    </tr>
                                @endforeach
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop
@section('script')
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA_WiMOa0u9MlGA9_1z9Ypq186p4C-tS3c"></script>
    <script type="text/javascript">
        HideSidebar();
        $(document).ready(function() {
            $('#select_search_type').val("{{ old('search_type') }}").change();
        });
        var mapInitialized = false;
        var directionsService;
        var directionsRenderer;
        var mapOptions;
        $('#chk_all').change(check_all);
        var drivers = @json($drivers);
        var cars = @json($cars);
        var select_driver = 0;
        const company = '彰化縣彰化市線東路一段459號';
        //編輯頁面帶入資料
        $('button[data-target="#modal_edit"]').click(function() {
            var data = $(this).closest('tr').data();
            $.each(data, function(key, value) {
                if (key == 'src') {
                    $('#div_src_1').toggle(value == 1);
                    $('#div_src_2').toggle(value == 2);
                } else if (key == 'cust_confirmed') {
                    $('[name="cust_confirmed"]').prop('checked', false);
                    $('[name="cust_confirmed"][value="' + value + '"]').prop('checked', true);
                } else {
                    $('#modal_edit').find('[name="' + key + '"]').val(value);
                }
            });
        });
        //編輯儲存
        $('#btn_edit_confirm').click(function() {
            var data = {};
            $('#modal_edit [name]').each(function(i, e) {
                var key = $(e).attr('name');
                var val = $(e).val();
                if (key == 'cust_confirmed' && $(e).prop('checked') == false) {
                    return;
                }
                data[key] = val;
            });
            if (!Verify(data)) {
                return;
            }
            lockScreen();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{ route('personal_transport.ajax_update') }}",
                type: "post",
                data: data,
                success: function(data) {},
                error: function(xhr, ajaxOptions, thrownError) {
                    var msg = JSON.parse(xhr.responseText).msg;
                    alert(msg);
                },
                complete: function() {
                    var original_no = data.original_no;
                    var tr = $('tr[data-original_no="' + original_no + '"]');
                    $.each(data, function(key, value) {
                        //更新data-
                        $(tr).data(key, value);
                        //更新表格
                        if (key == 'driver') {
                            $.each(drivers, function(i, e) {
                                if (e.id == value) {
                                    value = e.name;
                                    return false;
                                }
                            })
                        } else if (key == 'car') {
                            $.each(cars, function(i, e) {
                                if (e.id == value) {
                                    value = e.name;
                                    return false;
                                }
                            })
                        } else if (key == 'cust_confirmed') {
                            value = (value == '1' ? '✔' : '');
                        } else if (key == 'additional_addr' && IsNullOrEmpty(
                                value)) { //無額外地址帶入原本地址
                            value = $(tr).data().receiver_address;
                        }
                        $(tr).find('[name="' + key + '"]').text(value);
                    });
                    $('#modal_edit').modal('hide');
                    unlockScreen();
                }
            });
        });
        //業務確認訂單
        $("#btn_confirm").click(function() {
            var idList = new Array();
            $("#table_data>tbody>tr .confirm:checked").each(function(index, element) {
                idList.push($(element).closest('tr').data().unconfirmed_id);
            });
            if (idList.length > 0) {
                $('input[name="id_list"]').val(JSON.stringify(idList));
            } else {
                alert('請勾選訂單');
                return false;
            }
        });
        //驗證編輯項目
        function Verify(data) {
            if (data.cust_confirmed == 1 && data.ship_date == '') { //已電聯需壓出貨日期
                alert('若是已經和客戶電聯過，出車日期為必填');
                return false;
            }
            return true;
        }
        //最佳路線
        $('[data-target="#modal_map"]').click(function() {
            if (!checkDriver()) {
                return false;
            }
            initMap();
            calcRoute();
        });
        //自訂路線
        $('#btn_calu').click(function() {
            initMap();
            custCalcRoute();
        });
        //檢查司機是否不同
        function checkDriver() {
            var select_drivers = [];
            var obj = $('#tbody_data');
            $(obj).find('>tr:visible:not(.hide_point)').each(function(i, e) {
                var data = $(e).data();
                var driver = Number(data.driver);
                if (!select_drivers.includes(driver)) {
                    select_drivers.push(driver);
                }
            });
            if (select_drivers.length != 1) {
                alert('只能以單個司機去計算路線');
                return false;
            } else {
                select_driver = select_drivers[0];
                return true;
            }
        }
        //初始化
        function initMap() {
            if (mapInitialized) {
                return;
            }
            directionsService = new google.maps.DirectionsService();
            directionsRenderer = new google.maps.DirectionsRenderer();
            var taiwan = new google.maps.LatLng(23.697809, 120.960518);
            mapOptions = {
                zoom: 14,
                center: taiwan,
                streetViewControl: false, //街景
            }
            mapInitialized = true;
        }
        //計算路線
        function calcRoute() {
            var div_map = $('#div_map');
            var map = new google.maps.Map($(div_map).find('.map')[0], mapOptions);
            var waypoints = getWayPoints($('#tbody_data'));
            var request = {
                origin: company, //起點
                destination: company, //終點
                waypoints: waypoints, //停靠點
                optimizeWaypoints: true, //最佳化路線
                travelMode: 'DRIVING', //交通方式
                avoidFerries: true, //避開渡輪
            };
            directionsService.route(request, function(response, status) {
                if (status == 'OK') {
                    directionsRenderer.setMap(map);
                    directionsRenderer.setDirections(response);
                    // directionsRenderer.setOptions({
                    //     draggable: true
                    // });
                    display_map(div_map, response);
                } else {
                    alert("Directions request failed due to " + status);
                }
            }).catch((e) => window.alert("Directions request failed due to " + status));
        }
        //自訂路線
        function custCalcRoute() {
            var div_map = $('#div_cust_map');
            var map = new google.maps.Map($(div_map).find('.map')[0], mapOptions);
            var waypoints = getWayPoints($(div_map).find('.sche_legs'));
            var request = {
                origin: company, //起點
                destination: company, //終點
                waypoints: waypoints, //停靠點
                optimizeWaypoints: false, //最佳化路線
                travelMode: 'DRIVING', //交通方式
                avoidFerries: true, //避開渡輪
            };
            directionsService.route(request, function(response, status) {
                if (status == 'OK') {
                    directionsRenderer.setMap(map);
                    directionsRenderer.setDirections(response);
                    display_map(div_map, response, true);
                } else {
                    alert("Directions request failed due to " + status);
                }
            }).catch((e) => window.alert("Directions request failed due to " + status));
        }
        //顯示地圖結果
        function display_map(obj, response, cust = false) {
            //路線資訊
            const route = response.routes[0];
            const legs = $(obj).find('.legs')[0];
            var leg_count = route.legs.length - 1;
            var distance_sum = 0;
            var duration_sum = 0;
            var dom = "";
            for (let i = 0; i < route.legs.length; i++) {
                var is_last = i == (route.legs.length - 1);
                var mark = ((i + 2) + 9).toString(36).toUpperCase();
                var leg = route.legs[i];
                var distance = leg.distance; //距離
                var duration = leg.duration; //時間
                var end_address = leg.end_address; //路段終點
                distance_sum += distance.value;
                duration_sum += duration.value;
                var addr_dom = createAddrDOM(is_last, mark, end_address, distance, duration);
                if (is_last) {
                    $(obj).find('.destination').html(addr_dom);
                } else {
                    dom += addr_dom;
                }
            }
            if (!cust) {
                update_sche(distance_sum, duration_sum);
            }
            legs.innerHTML = dom; //路程明細
            $(obj).find('.leg_count').text(leg_count); //點數
            $(obj).find('.distance_sum').text(Math.floor(distance_sum / 1000)); //總距離
            var hh = Math.floor(duration_sum / 3600);
            duration_sum %= 3600;
            var mm = Math.floor(duration_sum / 60);
            $(obj).find('.duration_sum').text(hh + '小時' + mm + '分鐘'); //總時間
        }
        //取得停靠站
        function getWayPoints(obj) {
            var result = [];
            $(obj).find('>tr:visible:not(.hide_point)').each(function(i, e) {
                var data = $(e).data();
                var addr = data.receiver_address;
                var add_addr = data.additional_addr;
                if (IsNullOrEmpty(add_addr)) {
                    result.push({
                        location: addr,
                        stopover: true
                    });
                } else {
                    result.push({
                        location: add_addr,
                        stopover: true
                    });
                }
            });
            return result;
        }
        //建立地圖地址
        function createAddrDOM(is_last, mark, end_address, distance, duration) {
            var dom = "";
            dom += '<tr data-receiver_address="' + end_address + '">';
            dom += '<td>' + mark + '</td>';
            dom += '<td class="text-left">' + end_address + '</td>';
            dom += '<td class="text-right">' + distance.text + '</td>';
            dom += '<td class="text-right">' + duration.text + '</td>';
            dom += '</tr>';
            return dom;
        }
        //自訂路線畫面
        $('[data-target="#modal_cust_map"]').click(function() {
            if (!checkDriver()) {
                return false;
            }
            var dom = '';
            var waypoints = getWayPoints($('#tbody_data'));
            $(waypoints).each(function(i, e) {
                dom += '<tr data-receiver_address="' + e.location + '">';
                dom += '<td>';
                dom +=
                    '<button class="btn btn-success btn-sm up mr-1" type="button" onclick="move(this)"><i class="bi bi-arrow-up"></i></button>';
                dom +=
                    '<button class="btn btn-danger btn-sm down mr-1" type="button" onclick="move(this)"><i class="bi bi-arrow-down"></i></button>';
                dom += '</td>';
                dom +=
                    '<td><input class="form-control form-control-sm" type="checkbox" onchange="ignore(this)" checked></td>';
                dom += '<td class="text-left">' + e.location + '</td>';
                dom += '</tr>';
            });
            $('#div_cust_map .sche_legs').html(dom);
        });
        //調整地點
        function move(obj) {
            var row = $(obj).parents("tr:first");
            if ($(obj).is(".up")) {
                row.prev().hide().show('slow');
                row.insertBefore(row.prev()).hide().show('slow');
            } else {
                row.next().hide().show('slow');
                row.insertAfter(row.next()).hide().show('slow');
            }
        };

        function ignore(obj) {
            var tr = $(obj).closest('tr');
            if (tr.is('.hide_point')) {
                tr.removeClass('hide_point');
            } else {
                tr.addClass('hide_point');
            }
        }
        //更新該日期總路程時間
        function update_sche(distance, duration) {
            var ship_date = '{{ $ship_date }}';
            if (IsNullOrEmpty(ship_date) || select_driver == 0 || distance == 0 || duration == 0) {
                return;
            }
            lockScreen();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{ route('personal_transport.ajax_update_sche') }}",
                type: "post",
                data: {
                    ship_date: ship_date,
                    driver: select_driver,
                    distance: distance,
                    duration: duration,
                },
                success: function(data) {},
                error: function(xhr, ajaxOptions, thrownError) {
                    var msg = JSON.parse(xhr.responseText).msg;
                    alert(msg);
                },
                complete: function() {
                    unlockScreen();
                }
            });
        }

        function search_type_onchange(obj) {
            var search_type = $(obj).val();
            $('[data-search_type]').each(function(i, e) {
                $(e).toggle($(e).data().search_type == search_type);
            });
        }
    </script>
@stop
