@extends('layouts.base')
@section('title')
    親送概況
@stop
@section('css')
    <style type="text/css">
        .weight_level_0 {
            border-color: #8fd19e;
            background-color: #c3e6cb;
        }

        .weight_level_1 {
            border-color: #ed969e;
            background-color: #f5c6cb;
        }

        .weight_level_2 {
            border-color: #ffdf7e;
            background-color: #ffeeba;
        }

        .car {
            font-family: monospace !important;
        }
    </style>
@endsection
@section('content')
    <!-- Modal 說明 -->
    <div id="modal_help" class="modal fade">
        <div class="modal-dialog modal-xl modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="modal-title">
                        <h1>說明</h1>
                    </div>
                    <button type="button" class="close" data-dismiss="modal">
                        <span>&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="form-row">
                            <div class="col">
                                <ul class="list-group">
                                    <li class="list-group-item">下貨時間=單數 x 20分鐘</li>
                                    @foreach ($cars as $car)
                                        <li class="list-group-item car">
                                            {{ $car->full_name . ' 限重：' . number_format($car->weight_limit) . '公斤' }}
                                        </li>
                                    @endforeach
                                    <li class="list-group-item weight_level_1">紅底：重量＞限重</li>
                                    <li class="list-group-item weight_level_2">黃底：重量＞限重的90%</li>
                                    <li class="list-group-item weight_level_0">綠底：重量 ≤ 限重的90%</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">關閉</button>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <form action="{{ route('personal_transport.overview') }}" method="GET">
            <div class="form-row">
                <div class="col-auto">
                    <div class="form-group">
                        <label for="input_start_date">出車起日</label>
                        <input id="input_start_date" name="start_date" class="form-control" type="date"
                            value="{{ old('start_date') }}" required>
                    </div>
                </div>
                <div class="col-auto">
                    <div class="form-group">
                        <label for="input_end_date">出車訖日</label>
                        <input id="input_end_date" name="end_date" class="form-control" type="date"
                            value="{{ old('end_date') }}" required>
                    </div>
                </div>
                <div class="col"></div>
                <div class="col-auto">
                    <button class="btn btn-success" type="button" data-toggle="modal" data-target="#modal_help">
                        <i class="bi bi-question-circle"></i>說明
                    </button>
                </div>
            </div>
            <button class="btn btn-primary" type="submit">查詢</button>
        </form>
        <div class="form-row mt-2">
            <div class="col">
                <div class="FrozenTable" style="max-height: 77vh;">
                    <table class="table table-bordered sortable table-filter">
                        <caption>彰化出發含回程</caption>
                        <thead>
                            <tr>
                                <th class="filter-col">日期</th>
                                <th class="filter-col">人員</th>
                                <th class="filter-col">車輛</th>
                                <th class="filter-col">途經地區</th>
                                <th>單數</th>
                                <th>重量(公斤)</th>
                                <th>最短時間</th>
                                <th>下貨時間</th>
                                <th>總時間</th>
                                <th>距離(公里)</th>
                                <th>最後更新時間</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $ship_date => $drivers)
                                @foreach ($drivers as $item)
                                    <tr>
                                        <td>{{ $ship_date }}</td>
                                        <td>{{ $item['driver'] }}</td>
                                        <td class="car">{{ $item['car'] }}</td>
                                        <td>{{ $item['area'] }}</td>
                                        <td>{{ $item['order_count'] }}</td>
                                        <td class="weight_level_{{ $item['weight_level'] }}">{{ $item['weight'] }}</td>
                                        <td>{{ $item['duration'] }}</td>
                                        <td>{{ $item['upload_duration'] }}</td>
                                        <td>{{ $item['total_duration'] }}</td>
                                        <td>{{ $item['distance'] }}</td>
                                        <td>{{ $item['updated_at'] }}</td>
                                    </tr>
                                @endforeach
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop
@section('script')
    <script type="text/javascript">
        $(document).ready(InitialDate());

        function InitialDate() {
            var today = new Date();
            var end_day = today.addDays(7);

            if ($("#input_start_date").val() == '') {
                $("#input_start_date").val(DateToString(today));
            }
            if ($("#input_end_date").val() == '') {
                $("#input_end_date").val(DateToString(end_day));
            }
        }
    </script>
@stop
