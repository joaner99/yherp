@extends('layouts.base')
@section('title')
    親送總表
@stop
@section('css')
    <style type="text/css">
    </style>
@endsection
@section('content')
    <div class="container">
        <div class="form-row">
            <div class="col-6">
                <form action="{{ route('personal_transport.ship_day') }}" method="GET">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">月份</span>
                        </div>
                        <input id="input_month" name="month"class="form-control" type="month" value="{{ old('month') }}"
                            required>
                        <div class="input-group-prepend">
                            <label class="input-group-text">人員</label>
                        </div>
                        <select class="custom-select" name="driver">
                            <option value=""{{ empty(old('driver')) ? 'selected' : '' }}>全部</option>
                            @foreach ($drivers as $driver)
                                <option value="{{ $driver->id }}" {{ old('driver') == $driver->id ? 'selected' : '' }}>
                                    {{ $driver->name }}</option>
                            @endforeach
                        </select>
                        <div class="input-group-append">
                            <button class="btn btn-info" type="submit">查詢</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col"></div>
            <div class="col-auto">
                <form action="{{ route('export_table') }}" method="post">
                    @csrf
                    <input name="export_name" type="hidden" value="親送出車日期">
                    <input name="export_data" type="hidden">
                    <button class="btn btn-success" type="submit" export-target="#table_data">
                        <i class="bi bi-file-earmark-excel"></i>匯出
                    </button>
                </form>
            </div>
        </div>
        <div class="form-row mt-2">
            <div class="col">
                <div class="FrozenTable" style="max-height: 87vh;">
                    <table id="table_data" class="table table-bordered sortable table-filter">
                        <thead>
                            <tr>
                                <th>序</th>
                                <th class="filter-col">出車日期</th>
                                <th class="filter-col">出車人員</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (!empty($data) && count($data) > 0)
                                <?php $idx = 1; ?>
                                @foreach ($data as $ship_date => $ship_drivers)
                                    @foreach ($ship_drivers as $driver)
                                        <tr>
                                            <td>{{ $idx++ }}</td>
                                            <td>{{ $ship_date }}</td>
                                            <td>{{ $driver }}</td>
                                        </tr>
                                    @endforeach
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop
@section('script')
    <script type="text/javascript"></script>
@stop
