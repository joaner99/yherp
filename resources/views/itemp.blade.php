@extends('layouts.base')
@section('title')
    儲位總表
@stop
@section('css')
    <style type="text/css">
        div[id*="div_img"] {
            background-color: rgb(119, 119, 119);
        }

        .carousel-item img {
            width: auto;
            max-width: 150px;
            max-height: 150px;
            margin: auto;
            cursor: pointer;
        }

        #modal_img img {
            max-width: 100%;
            margin: auto;
        }
    </style>
@endsection
@section('content')
    {{-- Modal 詳細圖片 --}}
    <div id="modal_img" class="modal">
        <div class="modal-dialog modal-dialog-centered" style="max-width: 95%;">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">商品圖片</h5>
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div style="overflow-y: auto; max-height: 75vh;">
                        <img id="img_detail" class="d-block">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">關閉</button>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        @if (empty($data))
            <div class="alert alert-danger">
                找不到資料
            </div>
        @else
            <nav>
                <ul class="pagination">
                    <li class="page-item {{ empty($stock) ? 'active' : '' }}">
                        <a class="page-link" href="{{ route('itemp') }}">
                            全倉庫
                            @if (empty($stock))
                                <span class="sr-only">(current)</span>
                            @endif
                        </a>
                    </li>
                    @if (!empty($stockType))
                        @foreach ($stockType as $item)
                            <li class="page-item {{ $stock == $item->stock_id ? 'active' : '' }}">
                                <a class="page-link" href="{{ route('itemp', $item->stock_id) }}">{{ $item->stock_name }}
                                    @if ($stock == $item->stock_id)
                                        <span class="sr-only">(current)</span>
                                    @endif
                                </a>
                            </li>
                        @endforeach
                    @endif
                </ul>
            </nav>
            <div class="form-row">
                <div class="col">
                    <div class="FrozenTable" style="font-size:0.9rem; max-height: 85vh;">
                        <?php $idx = 1; ?>
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th style="width: 4%;">序</th>
                                    <th style="width: 6%;">儲位</th>
                                    <th>圖片<small>(點圖可放大)</small></th>
                                    <th style="width: 8%;">料號</th>
                                    <th>品名</th>
                                    <th style="width: 6%;">倉庫數量</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($data as $key => $value)
                                    @foreach ($value as $item)
                                        <tr>
                                            @if ($loop->first)
                                                <td rowspan="{{ count($value) }}">{{ $idx++ }}</td>
                                                <td rowspan="{{ count($value) }}">{{ $key }}</td>
                                            @endif
                                            <td>
                                                @if (!empty($imgs))
                                                    @if (array_key_exists($item['item_no'], $imgs))
                                                        <div id="div_img_{{ $item['item_no'] }}" class="carousel slide"
                                                            data-ride="carousel">
                                                            <div class="carousel-inner">
                                                                @foreach ($imgs[$item['item_no']] as $img)
                                                                    <div
                                                                        class="carousel-item {{ $loop->first ? 'active' : '' }}">
                                                                        <img src="{{ asset('storage/' . $img) }}"
                                                                            class="d-block" loading="lazy">
                                                                    </div>
                                                                @endforeach
                                                            </div>
                                                            @if (count($imgs[$item['item_no']]) > 1)
                                                                <a class="carousel-control-prev"
                                                                    href="#div_img_{{ $item['item_no'] }}" role="button"
                                                                    data-slide="prev">
                                                                    <span class="carousel-control-prev-icon"
                                                                        aria-hidden="true"></span>
                                                                    <span class="sr-only">Previous</span>
                                                                </a>
                                                                <a class="carousel-control-next"
                                                                    href="#div_img_{{ $item['item_no'] }}" role="button"
                                                                    data-slide="next">
                                                                    <span class="carousel-control-next-icon"
                                                                        aria-hidden="true"></span>
                                                                    <span class="sr-only">Next</span>
                                                                </a>
                                                            @endif
                                                        </div>
                                                    @else
                                                        無
                                                    @endif
                                                @endif
                                            </td>
                                            <td>{{ $item['item_no'] }}</td>
                                            <td>{{ $item['item_name'] }}</td>
                                            <td>{{ number_format($item['qty']) }}</td>
                                        </tr>
                                    @endforeach
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        @endif
    </div>
@stop
@section('script')
    <script type="text/javascript">
        //關閉輪播
        $('.carousel').carousel({
            interval: false,
        })
        //顯示大圖片
        $('.carousel-item').click(function() {
            var src = $(this).find('img').prop('src');
            if (!IsNullOrEmpty(src)) {
                $('#img_detail').prop('src', src);
                $('#modal_img').modal('show');
            }
        });
    </script>
@stop
