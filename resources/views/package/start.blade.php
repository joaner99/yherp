@extends('layouts.master')
@section('master_title')
    包裝作業-包裝開始
@stop
@section('master_css')
    <style type="text/css">
        #tbody_detail>tr>td.name {
            text-align: left;
        }

        #table_order>caption,
        #table_order_detail>caption {
            font-size: 2rem;
        }

        #table_order th,
        #table_order_detail th {
            font-size: 1.25rem;
        }

        #table_order td,
        #table_order_detail td {
            font-size: 2rem;
            font-weight: bolder;
        }

        .msg-icon {
            font-size: 20rem;
            text-align: center;
        }

        .msg {
            font-size: 2.5rem;
            font-weight: bolder;
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: center;
        }
    </style>
@stop
@section('master_body')
    <div class="clock"></div>
    <div class="container-fluid p-3">
        <div class="form-row">
            <div class="col-9">
                <form id="form_end" action="{{ route('package.end') }}" method="GET" class="h-100">
                    <div class="input-group h-100">
                        <div class="input-group-prepend">
                            <span class="input-group-text">包裝機台</span>
                            <span class="input-group-text bg-white">{{ Auth::user()->name ?? '' }}</span>
                            <span class="input-group-text">包裝人員</span>
                            <span class="input-group-text bg-white"
                                id="span_user_name">{{ $data['user_name'] ?? '' }}</span>
                            <span class="input-group-text">包裝條碼</span>
                        </div>
                        <input class="form-control" style="height: auto;" id="input_sale_no" name="sale_no" type="text"
                            autocomplete="off" placeholder="請使用手持式掃描機，掃描包裝條碼，結束包裝" autofocus>
                        <div class="input-group-append">
                            <button id="btn_end" class="btn btn-success" type="submit">結束包裝</button>
                        </div>
                    </div>
                    <input name="id" type="hidden" value="{{ $data['id'] }}">
                    <input name="user_id" type="hidden" value="{{ $data['user_id'] }}">
                </form>
            </div>
            <div class="col text-success" style="align-self: center;">
                ※連續按下Backspace鍵5次，將會返回包裝作業
            </div>
        </div>
        <div class="form-row">
            <div class="col-9">
                <div class="form-row mt-2">
                    <div class="col">
                        <div class="FrozenTable">
                            <table id="table_order" class="table table-bordered table-hover sortable">
                                <caption>訂單資訊</caption>
                                <tbody>
                                    <tr>
                                        <th>銷貨單號</th>
                                        <td>{{ $data['sale_no'] }}</td>
                                        <th>客戶簡稱</th>
                                        <td>{{ $data['cust_name'] }}</td>
                                    </tr>
                                    <tr>
                                        <th>物流名稱</th>
                                        <td>{{ $data['tran_name'] }}</td>
                                        <th>託運單號</th>
                                        <td>{{ $data['tran_no'] }}</td>
                                    </tr>
                                    <tr>
                                        <th>類別</th>
                                        <td>{{ $type_name }}</td>
                                        <th>件數</th>
                                        <td>{{ $data['tran_qty'] }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="form-row mt-2">
                    <div class="col">
                        <div class="FrozenTable" style="max-height: 68vh;">
                            <table id="table_order_detail" class="table table-bordered table-hover sortable">
                                <caption>訂單明細</caption>
                                <thead>
                                    <tr>
                                        <th style="width: 20%;">料號</th>
                                        <th>品名</th>
                                        <th style="width: 8%;">數量</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($data['detail'] as $item)
                                        <tr>
                                            <td>{{ $item['item_no'] }}</td>
                                            <td class="text-left">{{ $item['name'] }}</td>
                                            <td>{{ $item['qty'] }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col">
                @if ($msg = Session::get('msg'))
                    <div class="form-row">
                        <div class="col msg-icon">
                            <i class="bi bi-x-octagon-fill text-danger"></i>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col msg">
                            <div>{{ $msg }}</div>
                        </div>
                    </div>
                @else
                    <div class="form-row">
                        <div class="col msg-icon">
                            <i class="bi bi-box-seam text-success"></i>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col msg">
                            <div>包裝中</div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
@stop
@section('master_js')
    <script type="text/javascript">
        $(document).ready(function() {
            var pak_max = 0;
            var pak_type = {{ $type }};
            if (pak_type == 2) {
                var qty = "{{ $data['tran_qty'] }}";
                pak_max = (qty == 0 ? 1 : qty) * 3 * 60 * 1000;
            } else {
                pak_max = 15 * 60 * 1000;
            }
            //包裝時間超過15分鐘
            setInterval(function() {
                window.location.replace("{{ route('package.index') }}");
            }, pak_max);
            clockUpdate();
            setInterval(function() {
                clockUpdate();
            }, 1000);
        });
        //包奘結束
        var back_count = 0;
        $('#input_sale_no').keydown(function(event) {
            if (event.keyCode === 13) {
                event.preventDefault();
                $('#btn_end').click();
            } else if (event.keyCode === 8) {
                if (back_count == 4) { //backspace按壓5次，返回首頁
                    window.location.replace("{{ route('package.index') }}");
                } else {
                    back_count++;
                }
            }
        });
        //prevent double submit
        $('#form_end').on('submit', function() {
            $('#btn_end').attr('disabled', 'true');
        });
    </script>
@stop
