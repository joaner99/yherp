@include('includes.login')
@extends('layouts.master')
@section('master_title')
    包裝作業
@stop
@section('master_css')
    @yield('login_css')
    <style type="text/css">
        .msg-icon {
            font-size: 20rem;
            text-align: center;
        }

        .msg {
            font-size: 5rem;
            font-weight: bolder;
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: center;
        }
    </style>
@stop
@section('master_body')
    @yield('login_body')
    <div class="clock"></div>
    <div class="container-fluid p-3">
        <div class="form-row">
            <div id="div_start" class="col-7">
                <form id="form_start" action="{{ route('package.check_start') }}" method="GET" class="h-100">
                    <div class="input-group h-100">
                        <div class="input-group-prepend">
                            <span class="input-group-text">包裝機台</span>
                            <span class="input-group-text bg-white">{{ Auth::user()->name ?? '' }}</span>
                            <span class="input-group-text">包裝人員</span>
                            <span class="input-group-text bg-white" id="span_user_name">{{ $user_name ?? '' }}</span>
                            <span class="input-group-text">銷貨/發票條碼</span>
                        </div>
                        <input class="form-control" style="height: auto;" id="input_package_no" name="package_no"
                            type="text" autocomplete="off" placeholder="請使用手持式掃描機，掃描銷貨/發票條碼，開始包裝" autofocus>
                        <div class="input-group-append">
                            <button id="btn_start" class="btn btn-primary" type="submit">開始包裝</button>
                        </div>
                    </div>
                    <input id="input_user_id" name="user_id" type="hidden" value="{{ $user_id ?? '' }}">
                </form>
            </div>
            <div class="col text-success" style="align-self: center;">
                ※連續按下Backspace鍵5次，將會登出
            </div>
            <div class="col-auto">
                @if (!empty($pass))
                    <ul class="list-group list-group-horizontal">
                        <li class="list-group-item bg-success text-white">上一筆包裝時間</li>
                        <li class="list-group-item">{{ floor($pass / 60) . ' 分 ' . $pass % 60 . ' 秒' }}</li>
                    </ul>
                @elseif (!empty($no_package))
                    <div class="alert alert-success">免包裝完成</div>
                @endif
            </div>
        </div>
        <div class="form-row mt-2">
            <div class="col">
                @if (!empty($msg))
                    <div class="form-row">
                        <div class="col msg-icon">
                            <i class="bi bi-x-octagon-fill text-danger"></i>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col msg">
                            <div>{{ $msg }}</div>
                        </div>
                    </div>
                @else
                    <div class="form-row">
                        <div class="col msg-icon">
                            <i class="bi bi-upc-scan"></i>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col msg">
                            <div>請使用手持式掃描機<br>掃描銷貨/發票條碼開始包裝</div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
@stop
@section('master_js')
    @yield('login_js')
    <script type="text/javascript">
        var lock = false;
        var interval = 60 * 1000; //60S
        $(document).ready(function() {
            check_user();
            //10M自動刷新
            setInterval(function() {
                location.reload();
            }, 10 * 60 * 1000);
            clockUpdate();
            setInterval(function() {
                clockUpdate();
            }, 1000);
        });

        //檢查登入
        function check_user() {
            var user_id = $('#input_user_id').val();
            if (IsNullOrEmpty(user_id)) { //未登入
                $('#modal_login').modal('show');
            } else {
                setTimeout(logout, interval);
            }
        }
        //使用者登入
        $('#input_user').keyup(function(event) {
            if (event.keyCode === 13) {
                event.preventDefault();
                if (!lock) {
                    login();
                }
            }
        });
        //包裝開始
        $('#input_package_no').keyup(function(event) {
            if (event.keyCode === 13) {
                event.preventDefault();
                $('#btn_start').click();
            }
        });
        //按下backspace，跳轉上一個輸入框
        var back_count = 0;
        $('#input_package_no').keydown(function(event) {
            if (event.keyCode === 8) {
                var val = $(this).val();
                if (IsNullOrEmpty(val)) {
                    event.preventDefault();
                    if (back_count == 4) {
                        logout();
                    } else {
                        back_count++;
                    }
                }
            } else {
                back_count = 0;
            }
        });
        //prevent double submit
        $('#form_start').on('submit', function() {
            $('#btn_start').attr('disabled', 'true');
        });
        //員工登入後
        $('#modal_login').on('hide.bs.modal', function(e) {
            if (isLogin) {
                $('#input_user_id').val(user_id);
                $('#span_user_name').text(user_name);
                $('#input_package_no').focus();
                setTimeout(logout, interval);
            } else {
                return false;
            }
        })
        //登出
        function logout() {
            $('#div_login_status').text('');
            $('#input_user_id').val('');
            $('#span_user_name').text('');
            $('#modal_login').modal('show');
            back_count = 0;
        }
    </script>
@stop
