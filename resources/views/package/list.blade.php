@extends('layouts.base')
@section('title')
    包裝清單
@stop
@section('css')
    <style type="text/css">
        #div_statistics {
            flex-wrap: 1;
        }

        #div_statistics>div {
            flex-basis: 520px;
            flex-grow: 0;
        }
    </style>
@stop
@section('content')
    <div class="container-fluid">
        <div class="form-row mb-2">
            <div class="col-auto">
                <form action="{{ route('package.list') }}" method="GET">
                    <div class="form-row">
                        <div class="form-group col-auto">
                            <label for="input_s_date">包裝起日</label>
                            <input id="input_s_date" name="s_date" class="form-control" type="date"
                                value="{{ old('s_date') }}">
                        </div>
                        <div class="form-group col-auto">
                            <label for="input_e_date">包裝訖日</label>
                            <input id="input_e_date" name="e_date" class="form-control" type="date"
                                value="{{ old('e_date') }}">
                        </div>
                    </div>
                    <button id="btn_search" class="btn btn-primary btn-sm" type="submit">查詢</button>
                </form>
            </div>
        </div>
        <div id="div_statistics" class="form-row mb-2">
            @foreach ($statistics as $day => $day_data)
                <div class="col">
                    <div class="FrozenTable" style="font-size: 0.9rem;">
                        <table class="table table-bordered table-hover sortable">
                            <colgroup>
                                <col span="5">
                                <col span="1" class="table-danger">
                            </colgroup>
                            <caption>{{ $day }}小計</caption>
                            <thead>
                                <tr>
                                    <th>包裝人員</th>
                                    <th>超商<br>件數</th>
                                    <th>宅配<br>件數</th>
                                    <th>其他<br>件數</th>
                                    <th>總件數</th>
                                    <th>漏刷<br>件數</th>
                                    <th>超商<br>平均時間</th>
                                    <th>宅配<br>平均時間</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($day_data as $item)
                                    <tr>
                                        <td>{{ $item['user_name'] }}</td>
                                        <td>{{ $item['shop'] }}</td>
                                        <td>{{ $item['home'] }}</td>
                                        <td>{{ $item['other'] }}</td>
                                        <td>{{ $item['total'] }}</td>
                                        <td>{{ $item['miss'] }}</td>
                                        <td>{{ $item['shop_time'] }}</td>
                                        <td>{{ $item['home_time'] }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="form-row mb-2">
            <div class="col-6">
                <div class="FrozenTable">
                    <table class="table table-bordered table-hover sortable">
                        <colgroup>
                            <col span="5">
                            <col span="1" class="table-danger">
                        </colgroup>
                        <caption>統計</caption>
                        <thead>
                            <tr>
                                <th>包裝人員</th>
                                <th>超商<br>件數</th>
                                <th>宅配<br>件數</th>
                                <th>其他<br>件數</th>
                                <th>總件數</th>
                                <th>漏刷<br>件數</th>
                                <th>超商<br>平均時間</th>
                                <th>宅配<br>平均時間</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($total_all as $item)
                                <tr>
                                    <td>{{ $item['user_name'] }}</td>
                                    <td>{{ $item['shop'] }}</td>
                                    <td>{{ $item['home'] }}</td>
                                    <td>{{ $item['other'] }}</td>
                                    <td>{{ $item['total'] }}</td>
                                    <td>{{ $item['miss'] }}</td>
                                    <td>{{ $item['shop_time'] }}</td>
                                    <td>{{ $item['home_time'] }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-6">
                <div class="FrozenTable">
                    <table class="table table-bordered table-hover sortable">
                        <colgroup>
                            <col span="5">
                            <col span="1" class="table-danger">
                        </colgroup>
                        <caption>親送統計</caption>
                        <thead>
                            <tr>
                                <th>包裝人員</th>
                                @foreach ($com_item_type as $t)
                                    <th>{{ $t }}</th>
                                @endforeach
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($com_total as $user_name => $items)
                                <tr>
                                    <td>{{ $user_name }}</td>
                                    @foreach ($items as $qty)
                                        <td>{{ number_format($qty) }}</td>
                                    @endforeach
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="form-row">
            <div class="col">
                <div class="FrozenTable" style="max-height: 78vh; font-size: 0.9rem;">
                    <table class="table table-bordered table-hover sortable table-filter">
                        <thead>
                            <tr>
                                <th>序</th>
                                <th>銷貨單號</th>
                                <th>包裝機台</th>
                                <th class="filter-col">包裝人員</th>
                                <th class="filter-col">類別</th>
                                <th class="filter-col">物流</th>
                                <th>件數</th>
                                <th>流水號</th>
                                <th>包裝開始時間</th>
                                <th>包裝結束時間</th>
                                <th>包裝時間</th>
                                <th>標籤列印</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 1; ?>
                            @if (!empty($data))
                                @foreach ($data as $item)
                                    <tr>
                                        <td>{{ $i++ }}</td>
                                        <td>
                                            <a href="{{ route('check_log_detail', $item['sale_no']) }}">
                                                {{ $item['sale_no'] }}
                                            </a>
                                        </td>
                                        <td>{{ $item->Machine_User->name }}</td>
                                        <td>{{ $item->User->name }}</td>
                                        <td>{{ $item->type }}</td>
                                        <td>{{ $item->Posein->trans_name ?? '無物流' }}</td>
                                        <td>{{ $item['tran_qty'] }}</td>
                                        <td>{{ $item['seq'] }}</td>
                                        <td>{{ $item['created_at'] }}</td>
                                        <td>{{ $item['completed_at'] ?? '' }}</td>
                                        <td>{{ empty($item['interval']) ? '' : $item['interval']->format('%H:%I:%S') }}
                                        </td>
                                        <td>{{ $item['printed'] > 0 ? '✔' : '' }}</td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop
@section('script')
    <script type="text/javascript">
        $(document).ready(InitialDate());

        function InitialDate() {
            var today = new Date();

            if ($("#input_s_date").val() == '') {
                $("#input_s_date").val(DateToString(today));
            }
            if ($("#input_e_date").val() == '') {
                $("#input_e_date").val(DateToString(today));
            }
            $("#input_s_date,#input_e_date").attr("max", DateToString(today));
        }

        //上傳
        $('#btn_search').click(function() {
            lockScreen();
        })
    </script>
@stop
