@extends('layouts.master')
@section('master_title')
    包裝作業-驗貨開始
@stop
@section('master_css')
    <style type="text/css">
        #tbody_detail>tr>td.name {
            text-align: left;
        }

        #table_order_detail>caption,
        .gift {
            font-size: 1.75rem;
        }

        #table_order_detail th,
        .gift {
            font-size: 1.25rem;
        }

        #table_order_detail td,
        .gift {
            font-size: 1.5rem;
            font-weight: bolder;
        }

        .msg-icon {
            font-size: 20rem;
            text-align: center;
        }

        .msg {
            font-size: 2.5rem;
            font-weight: bolder;
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: center;
        }

        #div_login_status {
            font-size: 2rem;
            font-weight: bolder;
            color: #dc3545;
        }

        #input_user {
            color: transparent;
        }

        /*倉庫明細-倉別*/
        table.inner-table>thead>tr>th:nth-child(1),
        table.inner-table>tbody>tr>td:nth-child(1) {
            width: 250px;
        }
    </style>
@stop
@section('master_body')
    <!---覆核-->
    <div id="modal_check" class="modal">
        <div class="modal-dialog modal-dialog-centered modal-dialog-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">覆核</h5>
                </div>
                <div class="modal-body">
                    <div class="form-row">
                        <div class="col">
                            <div class="input-group h-100">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">員工條碼</span>
                                </div>
                                <input class="form-control" style="height: auto;" id="input_user" type="text"
                                    autocomplete="off" placeholder="請使用手持式掃描機，掃描員工條碼">
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div id="div_login_status" class="col"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid p-3">
        <div class="form-row">
            <div class="col-auto">
                <div class="input-group h-100">
                    <div class="input-group-prepend">
                        <span class="input-group-text">包裝機台</span>
                        <span class="input-group-text bg-white">{{ Auth::user()->name ?? '' }}</span>
                        <span class="input-group-text">包裝人員</span>
                        <span class="input-group-text bg-white">{{ $user_name ?? '' }}</span>
                        <span class="input-group-text">銷貨單號</span>
                        <span class="input-group-text bg-white">{{ $package_no ?? '' }}</span>
                    </div>
                </div>
            </div>
            <div class="col text-success" style="align-self: center;">
                ※連續按下Backspace鍵5次，將會返回包裝作業
            </div>
        </div>
        <div class="form-row">
            <div class="col-9">
                <div class="form-row mt-2">
                    <div class="col">
                        <div class="FrozenTable" style="max-height: 45vh;">
                            <table id="table_order_detail" class="table table-bordered table-hover sortable">
                                <caption>訂單明細</caption>
                                <thead>
                                    <tr>
                                        <th style="width: 250px;">料號</th>
                                        <th>品名</th>
                                        <th style="width: 8%;">數量</th>
                                        <th style="width: 8%;">結果</th>
                                    </tr>
                                </thead>
                                <tbody id="tbody_details">
                                    @foreach ($detail->groupBy('HICOD') as $item)
                                        <tr data-item_no="{{ $item->first()->item_no }}">
                                            <td>{{ $item->first()->item_no }}</td>
                                            <td class="text-left">{{ $item->first()->item_name }}</td>
                                            <td>
                                                <input class="form-control" data-qty="{{ $item->SUM('HINUM') }}"
                                                    type="number" min="1" step="1">
                                            </td>
                                            <td class="status"></td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                @if (!empty($gift))
                    <div class="form-row mt-2">
                        <div class="col">
                            <div class="FrozenTable" style="max-height: 45vh;">
                                <table class="table table-bordered table-hover sortable gift">
                                    <caption>贈品明細</caption>
                                    <thead>
                                        <tr>
                                            <th class="p-0">
                                                <table class="inner-table">
                                                    <thead>
                                                        <tr>
                                                            <th>料號</th>
                                                            <th>品名</th>
                                                        </tr>
                                                    </thead>
                                                </table>
                                            </th>
                                            <th style="width: 8%;">數量</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($gift as $g)
                                            <tr>
                                                <td class="p-0">
                                                    <table class="inner-table">
                                                        <tbody>
                                                            @foreach ($g['items'] as $item)
                                                                <tr>
                                                                    <td>{{ $item[0] }}</td>
                                                                    <td class="text-left">{{ $item[1] }}</td>
                                                                </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                </td>
                                                <td>{{ $g['qty'] }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
            <div class="col">
                @if ($msg = Session::get('msg'))
                    <div class="form-row">
                        <div class="col msg-icon">
                            <i class="bi bi-x-octagon-fill text-danger"></i>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col msg">
                            <div>{{ $msg }}</div>
                        </div>
                    </div>
                @else
                    <div class="form-row">
                        <div class="col msg-icon">
                            <i class="bi bi-box-seam text-success"></i>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col msg">
                            <div>驗貨中</div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
        <div class="form-row d-none">
            <form action="{{ route('package.start') }}" method="GET">
                <input name="user_id" type="hidden" value="{{ $user_id }}">
                <input name="package_no" type="hidden" value="{{ $package_no }}">
                <button id="btn_start" type="submit"></button>
            </form>
        </div>
        <div class="form-row d-none">
            <form action="{{ route('package.check_store', $package_no) }}" method="POST">
                @csrf
                <input name="user_id" type="hidden" value="{{ $user_id }}">
                <input name="status" type="hidden" value="3">
                <button id="btn_lock" type="submit"></button>
            </form>
        </div>
    </div>
@stop
@section('master_js')
    <script type="text/javascript">
        var error_count = 0;
        var permission = '';
        $(document).ready(function() {
            if ({{ $double_check }}) { //需要二次確認
                switch ({{ $status }}) {
                    case 3:
                        permission = 'package_check';
                        break;
                    case 4:
                        permission = 'package_fragile_check';
                        break;
                }
                $('#modal_check').modal('show');
                $('#input_user').focus();
            } else {
                $('#tbody_details>tr:first-child input').focus();
            }
            //檢驗時間超過3分鐘
            setInterval(function() {
                window.location.replace("{{ route('package.index') }}");
            }, 3 * 60 * 1000);
        });
        //使用者登入
        $('#input_user').keyup(function(event) {
            if (event.keyCode === 13) {
                event.preventDefault();
                login();
            } else {
                $('#div_login_status').text('');
            }
        });
        //登入
        function login() {
            var id = $('#input_user').val();
            if (IsNullOrEmpty(id)) {
                return;
            }
            $.ajax({
                url: '{{ route('user.ajax_detail') }}',
                type: "get",
                dataType: 'json',
                data: {
                    id: id,
                    permission: permission,
                },
                contentType: "application/json",
                success: function(data) {
                    if (!$.isEmptyObject(data)) {
                        $('#div_login_status').text('');
                        $('#modal_check').modal('hide');
                        $('#tbody_details>tr:first-child input').focus();
                    }
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    var msg = JSON.parse(xhr.responseText).msg;
                    $('#div_login_status').text(msg);
                    $('#input_user').val('');
                    $('#input_user').focus();
                }
            });
        }
        //按下Enter，跳轉下一個輸入框
        $('input[data-qty]').keyup(function(event) {
            if (event.keyCode === 13) { //Enter
                event.preventDefault();
                var val = $(this).val();
                if (IsNullOrEmpty(val) || val == 0) {
                    return;
                }
                var next_input = $(this).closest('tr').find('~tr>td>input[data-qty]:not([readonly])');
                if (next_input.length > 0) {
                    next_input.first().focus();
                } else {
                    CheckItem();
                }
            }
        });
        //按下backspace，跳轉上一個輸入框  
        var back_count = 0;
        $('input[data-qty],#input_user').keydown(function(event) {
            if (event.keyCode === 8) {
                var val = $(this).val();
                if (IsNullOrEmpty(val)) {
                    event.preventDefault();
                    var last_input = $(this).closest('tr').prevAll(':not(.bg-success)').first().find(
                        'input[data-qty]:not([readonly])');
                    if (last_input.length == 0) {
                        if (back_count == 4) {
                            window.location.replace("{{ route('package.index') }}");
                        } else {
                            back_count++;
                        }
                    }
                    last_input.focus();
                }
            } else {
                back_count = 0;
            }
        });
        //驗貨
        function CheckItem() {
            var confirm = true;
            $('#tbody_details input').each(function(i, e) {
                var tr = $(e).closest('tr');
                var qty = parseInt($(e).data().qty);
                var val = parseInt($(e).val());
                var status = '符合';
                if (isNaN(val) || IsNullOrEmpty(val)) {
                    confirm = false;
                    status = '缺件';
                    tr.attr('class', 'bg-danger');
                } else if (val < qty) {
                    confirm = false;
                    status = '缺件';
                    tr.attr('class', 'bg-danger');
                } else if (val > qty) {
                    confirm = false;
                    status = '多件';
                    tr.attr('class', 'bg-danger');
                } else {
                    $(e).attr('readonly', 'true');
                    tr.attr('class', 'bg-success');
                }
                tr.find('.status').text(status);
            });
            if (confirm) {
                window.location.replace("{{ route('package.start') }}");
                $('#btn_start').click();
            } else {
                error_count++;
                if (error_count > 3) {
                    $('#btn_lock').click();
                } else {
                    $('input[data-qty]:not([readonly])').first().focus();
                    $('.msg').text('已經錯誤' + error_count + '次');
                    $('.msg').addClass('text-danger');
                }
            }
        }
    </script>
@stop
