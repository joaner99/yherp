@extends('layouts.base')
@section('title')
    訂單包裝人員查詢
@stop
@section('css')
    <style type="text/css">
        #table_data>tbody>tr>td>textarea {
            font-size: 0.9rem;
        }

        table.inner-table>thead>tr>th,
        table.inner-table>tbody>tr>td {
            width: 100px;
        }
    </style>
@endsection
@section('content')
    <div class="container-fluid">
        <div class="form-row">
            <div class="col-auto">
                <form action="{{ route('package.order_packager') }}" method="GET">
                    <div class="form-row">
                        <div class="form-group col-auto">
                            <label for="input_start_date">起日</label>
                            <input id="input_start_date" name="start_date" class="form-control" type="date"
                                value="{{ old('start_date') }}" required>
                        </div>
                        <div class="form-group col-auto">
                            <label for="input_end_date">訖日</label>
                            <input id="input_end_date" name="end_date" class="form-control" type="date"
                                value="{{ old('end_date') }}" required>
                        </div>
                    </div>
                    <button class="btn btn-primary btn-sm">查詢</button>
                </form>
            </div>
            <div class="col"></div>
            <div class="col-auto">
                <div class="FrozenTable" style="font-size: 0.9rem;">
                    <table class="table table-bordered">
                        <tbody>
                            @if (!empty($viewData['order_count']))
                                <tr>
                                    <th class="bg-warning text-dark text-right">銷貨單數</th>
                                    <td>{{ number_format($viewData['order_count']) }}</td>
                                </tr>
                            @endif
                            <tr>
                                <th class="bg-warning text-dark text-right">工作天數</th>
                                <td>{{ number_format($viewData['work_day']) }}</td>
                            </tr>
                            <tr>
                                <th class="bg-warning text-dark text-right">當日驗貨未超過17:30天數</th>
                                <td>{{ number_format($viewData['in_work_time']) }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-auto">
                @if (!empty($viewData['machine_check']))
                    <div class="FrozenTable" style="font-size: 0.9rem;">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th class="bg-primary">機台</th>
                                    <th class="bg-primary">單數</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($viewData['machine_check'] as $item)
                                    <tr>
                                        <td>{{ $item['machine'] }}</td>
                                        <td>{{ number_format($item['count']) }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                @endif
            </div>
            <div class="col-auto">
                @if (!empty($data))
                    <form action="{{ route('export_table') }}" method="post">
                        @csrf
                        <input name="export_name" type="hidden" value="訂單包裝人清單">
                        <input name="export_data" type="hidden">
                        <button class="btn btn-success btn-sm" type="submit" export-target="#table_data">
                            <i class="bi bi-file-earmark-excel"></i>匯出Excel
                        </button>
                    </form>
                @endif
            </div>
        </div>
        <div class="form-row mt-2">
            <div class="col">
                <div class="FrozenTable" style="max-height: 65vh; font-size: 0.8rem;">
                    <table id="table_data" class="table table-bordered sortable">
                        <colgroup>
                            <col span="3" style="background-color: aliceblue;">
                        </colgroup>
                        <thead>
                            <tr>
                                <th style="width: 7%">訂單單號</th>
                                <th style="width: 10%">原始訂單編號</th>
                                <th>客訴簡述</th>
                                <th style="width: 600px;" class="p-0">
                                    <table class="inner-table">
                                        <thead>
                                            <tr>
                                                <th>銷貨單號</th>
                                                <th>驗貨機台</th>
                                                <th>驗貨時間</th>
                                                <th>包裝機台</th>
                                                <th>包裝時間</th>
                                                <th>包裝人員</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (!empty($data))
                                @foreach ($data as $item)
                                    <tr>
                                        <td>{{ $item['order_no'] }}</td>
                                        <td>{{ $item['origin_no'] }}</td>
                                        <td>
                                            <textarea class="form-control" readonly>{{ $item['remark'] }}</textarea>
                                        </td>
                                        <td class="p-0">
                                            <table class="inner-table">
                                                <tbody>
                                                    @foreach ($item['sales'] as $sale)
                                                        <tr>
                                                            <td>{{ $sale['sales_no'] }}</td>
                                                            <td>{{ $sale['check_machine'] }}</td>
                                                            <td>{{ $sale['check_time'] }}</td>
                                                            <td>{{ $sale['package_machine'] }}</td>
                                                            <td>{{ $sale['package_time'] }}</td>
                                                            <td>{{ $sale['packager'] }}</td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop
@section('script')
    <script type="text/javascript">
        $(document).ready(InitialDate());

        //textarea show all text
        $(function() {
            $('#table_data>tbody>tr>td>textarea').each(function() {
                $(this).height($(this).prop('scrollHeight'));
            });
        });

        function InitialDate() {
            var today = new Date();

            if ($("#input_start_date").val() == '') {
                $("#input_start_date").val(DateToString(today));
            }
            if ($("#input_end_date").val() == '') {
                $("#input_end_date").val(DateToString(today));
            }
        }
    </script>
@stop
