@extends('layouts.base')
@section('css')
    <style type="text/css">
        .div-img {
            max-width: 450px;
            flex-grow: 1;
            cursor: pointer;
        }

        .image-selected {
            border-width: 10px !important;
            border-color: red !important;
        }
    </style>
@endsection
@section('title')
    商品照片管理
@stop
@section('content')
    {{-- Modal 上傳 --}}
    <div id="modal_upload" class="modal">
        <div class="modal-dialog modal-lg modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">上傳圖片</h5>
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="input-group mb-3">
                        <div class="custom-file">
                            <input class="custom-file-input" id="input_file" name="file" type="file" accept="image/*">
                            <label class="custom-file-label" for="input_file">請選擇檔案</label>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button id="btn_upload" type="button" class="btn btn-primary">上傳</button>
                    <button id="btn_loading" type="button" class="btn btn-primary" style="display: none;" disabled>
                        <div class="spinner-border spinner-border-sm">
                            <span class="sr-only"></span>
                        </div>
                        <span>上傳中...</span>
                    </button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">關閉</button>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="form-row">
            <div class="col-auto">
                <form action="{{ route('items.image_management') }}" method="GET">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                                商品料號
                            </span>
                        </div>
                        <input autofocus type="text" name="id1" id="input_id" list="idList" class="form-control"
                            style="height: auto;" value="{{ old('id1') }}" required>
                        <datalist id="idList">
                            @if (!empty($item_all))
                                @foreach ($item_all as $item)
                                    <option value="{{ $item->ICODE }}">{{ $item->INAME }}</option>
                                @endforeach
                            @endif
                        </datalist>
                        <div class="input-group-append">
                            <button id="btn_search" class="btn btn-primary" type="submit">查詢</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        @if (!empty(old('id1')))
            @if (empty($data) || empty($data['name']))
                <div class="form-row">
                    <div class="col text-center" style="font-size: 15rem;">
                        <i class="bi bi-x-octagon-fill text-danger"></i>
                    </div>
                    <div class="col font-weight-bold d-flex flex-column align-items-start justify-content-center"
                        style="font-size: 2rem;">
                        <div>{{ old('id1') }}</div>
                        <div>找不到該料號商品資料，料號輸入錯誤，或已停產</div>
                    </div>
                </div>
            @else
                <div class="form-row">
                    <div></div>
                </div>
                <div class="form-row">
                    <div class="col">
                        <div class="FrozenTable" style="max-height: 85vh;">
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <th>料號</th>
                                        <td>{{ old('id1') }}</td>
                                        <th>名稱</th>
                                        <td>{{ $data['name'] }}</td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" class="p-0" style="overflow: auto;">
                                            <div id="div_img_list"
                                                class="d-flex flex-wrap justify-content-around align-items-stretch"
                                                style="height: 75vh;">
                                                @if (!empty($data['img']))
                                                    @foreach ($data['img'] as $img)
                                                        <div class="div-img d-flex justify-content-center align-items-center m-1 border rounded"
                                                            onclick="ImageClick(this)">
                                                            @if ($img === false)
                                                                <img src="{{ asset('img/null_image.png') }}"
                                                                    class="d-block">
                                                            @else
                                                                <img src="{{ asset('storage/' . $img) }}"
                                                                    class="img-fluid d-block"
                                                                    data-img="{{ $img }}">
                                                            @endif
                                                        </div>
                                                    @endforeach
                                                @endif
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="form-row mt-2">
                    <div class="col-auto">
                        <button class="btn btn-info" type="button" data-toggle="modal" data-target="#modal_upload">
                            <i class="bi bi-plus-lg"></i>新增圖片
                        </button>
                    </div>
                    <div class="col-auto">
                        <button id="btn_delete" class="btn btn-danger" type="button">
                            <i class="bi bi-trash"></i>刪除圖片
                        </button>
                    </div>
                    <div class="col"></div>
                    <div class="col-auto">
                        <form action="{{ route('items.image_update') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <input name="icode" type="hidden" value="{{ old('id1') }}">
                            <input name="img_path" type="hidden">
                            <button id="btn_confirm" class="btn btn-success" style="display: none;" type="submit">
                                <i class="bi bi-check-lg"></i>確認刪除
                            </button>
                        </form>
                    </div>
                </div>
            @endif
        @endif
    </div>
@stop
@section('script')
    <script type="text/javascript">
        //上傳檔案後顯示文件名稱
        $('#input_file').change(function(e) {
            var files = e.target.files;
            if (files.length == 0) {
                return;
            }
            var fileName = files[0].name;
            $(this).next('.custom-file-label').html(fileName);
        });

        //圖片選取
        function ImageClick(obj) {
            if ($(obj).hasClass('image-selected')) {
                $(obj).removeClass('image-selected');
            } else {
                $(obj).addClass('image-selected');
            }
        }

        //刪除選取圖片
        $('#btn_delete').click(function() {
            var selected = false;
            $('#div_img_list .image-selected').each(function(index, element) {
                selected = true;
                $(element).removeClass('d-flex');
                $(element).removeClass('image-selected');
                $(element).addClass('d-none');
            });
            if (selected) {
                $('#btn_confirm').show();
            }
        });

        //確認變更
        $('#btn_confirm').click(function() {
            var img_path = [];
            $('#div_img_list .d-flex').each(function(index, element) {
                var img = $(element).find('img').data().img;
                if (!IsNullOrEmpty(img)) {
                    img_path.push(img);
                }
            });
            $("input[name='img_path']").val(JSON.stringify(img_path));
        });

        //上傳圖片
        $('#btn_upload').click(function() {
            var files = $('#input_file')[0].files;
            if (files.length == 0) {
                alert('請選擇上傳檔案');
                return;
            }
            lockScreen();
            $('#div_loading').show();
            url = "{{ route('items.ajax_upload_image') }}";
            var form_data = new FormData();
            form_data.append('id', '{{ old('id1') }}');
            form_data.append('file', files[0]);
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: url,
                type: "post",
                dataType: 'json',
                data: form_data,
                contentType: false,
                processData: false,
                success: function(data) {
                    var path = data.path;
                    BuildImageDOM(path);
                    $('#input_file').val(null);
                    $('#input_file').next('.custom-file-label').html('請選擇檔案');
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    var msg = JSON.parse(xhr.responseText).msg;
                    alert(msg);
                },
                complete: function() {
                    unlockScreen();
                    $('#div_loading').hide();
                    $('#modal_upload').modal('hide')
                }
            });
        });

        //建立圖片DOM
        function BuildImageDOM(path) {
            var src = "{{ asset('storage/') }}" + '/' + path;
            dom =
                '<div class="div-img d-flex justify-content-center align-items-center m-1 border rounded" onclick="ImageClick(this)">';
            dom +=
                '<img src="' + src + '" class="img-fluid d-block" data-img="' + path + '">';
            dom += '</div>';
            $('#div_img_list').append(dom);
        }
    </script>
@stop
