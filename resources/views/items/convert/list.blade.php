@extends('layouts.base')
@section('title')
    需轉換料號清單
@stop
@section('css')
    <style type="text/css">
    </style>
@endsection
@section('content')
    <div class="container-fluid">
        <div class="form-row mt-2">
            <div class="col">
                <div class="FrozenTable" style="max-height: 730px;">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>訂單單號</th>
                                <th>原始訂單單號</th>
                                <th>原料號</th>
                                <th>原品名</th>
                                <th>新料號</th>
                                <th>新品名</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $item)
                                <tr>
                                    <td>{{ $item['order_no'] }}</td>
                                    <td></td>
                                    <td>{{ $item['item_no'] }}</td>
                                    <td class="text-left">{{ $item['item_name'] }}</td>
                                    <td>{{ $item['convert_item_no'] }}</td>
                                    <td class="text-left">{{ $item['convert_item_name'] }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop
@section('script')
    <script type="text/javascript"></script>
@stop
