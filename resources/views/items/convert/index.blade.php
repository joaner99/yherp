@extends('layouts.base')
@section('title')
    商品料號轉換
@stop
@section('css')
    <style type="text/css">
    </style>
@endsection
@section('content')
    <div class="container-fluid">
        <div class="form-row">
            <div class="col-auto">
                <a class="btn btn-info" href="{{ route('items.convert.create') }}">
                    <i class="bi bi-plus-lg"></i>新增
                </a>
            </div>
        </div>
        <div class="form-row mt-2">
            <div class="col">
                <div class="FrozenTable" style="max-height: 730px;">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th style="width: 10%;">操作</th>
                                <th style="width: 6%;">序</th>
                                <th style="width: 6%;">啟用</th>
                                <th style="width: 8%;">開始日期</th>
                                <th style="width: 8%;">結束日期</th>
                                <th>備註</th>
                                <th style="width: 22%;">原料號</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $item)
                                <tr>
                                    <td class="p-0">
                                        <div class="d-flex justify-content-center">
                                            <div>
                                                <form method="POST"
                                                    action="{{ route('items.convert.destroy', $item->id) }}"onsubmit="return confirm('確定要刪除?');">
                                                    @csrf
                                                    <input name="_method" type="hidden" value="DELETE">
                                                    <button class="btn btn-danger btn-sm mx-1">
                                                        <i class="bi bi-trash"></i>刪除
                                                    </button>
                                                </form>
                                            </div>
                                            <div>
                                                <a class="btn btn-warning btn-sm mx-1"
                                                    href="{{ route('items.convert.edit', $item->id) }}">
                                                    <i class="bi bi-pencil"></i>編輯
                                                </a>
                                            </div>
                                        </div>
                                    </td>
                                    <td>{{ $item->id }}</td>
                                    <td><span style="font-size: 1.5rem;">{{ $item->enabled == 1 ? '✔' : '' }}</span></td>
                                    <td>{{ $item->s_date }}</td>
                                    <td>{{ $item->e_date }}</td>
                                    <td>{{ $item->remark }}</td>
                                    <td>
                                        @foreach ($item->Details as $d)
                                            <div>{{ $d->src_item_no . ' ➔ ' . $d->item_no }}</div>
                                        @endforeach
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop
@section('script')
    <script type="text/javascript"></script>
@stop
