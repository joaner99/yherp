@extends('layouts.base')
@section('title')
    商品料號轉換-編輯
@stop
@section('css')
    <style type="text/css">
        .convert-icon {
            padding: 0 !important;
            font-size: 1.75rem;
            font-weight: bolder;
        }

        .item_name {
            font-size: 0.85rem;
        }
    </style>
@endsection
@section('content')
    <div class="container-fluid">
        @if ($errors->any())
            <div class="form-row mb-2">
                <div class="col-auto">
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        @endif
        <div class="form-row">
            <div class="col-auto">
                <a class="btn btn-secondary" href="{{ route('items.convert.index') }}">回上頁</a>
            </div>
        </div>
        <hr>
        <form method="POST" action="{{ route('items.convert.update', $data->id) }}">
            @csrf
            @method('PATCH')
            <div class="card mt-2">
                <h2 class="card-header text-info font-weight-bold">主檔</h2>
                <div class="card-body">
                    <div class="form-row">
                        <div class="col-lg-2">
                            <div class="form-group">
                                <label class="text-danger" for="input_s_date">*開始日期</label>
                                <input class="form-control" id="input_s_date" name="s_date" type="date"
                                    value="{{ $data->s_date }}" required>
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <div class="form-group">
                                <label class="text-danger" for="input_e_date">*結束日期</label>
                                <input class="form-control" id="input_e_date" name="e_date" type="date"
                                    value="{{ $data->e_date }}" required>
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <div class="form-group">
                                <label class="text-danger">*狀態</label>
                                <div class="form-check-list form-check-list-horizontal">
                                    <div class="form-check">
                                        <input class="form-check-input" id="input_enabled_1" name="enabled" type="radio"
                                            value="1" {{ $data->enabled ? 'checked' : '' }}>
                                        <label class="form-check-label" for="input_enabled_1">啟用</label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" id="input_enabled_0" name="enabled" type="radio"
                                            value="0" {{ !$data->enabled ? 'checked' : '' }}>
                                        <label class="form-check-label" for="input_enabled_0">停用</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg">
                            <div class="form-group">
                                <label class="text-danger" for="input_remark">*備註
                                    <small class="text-muted">(50字)</small></label>
                                <textarea class="form-control" id="input_remark" name="remark" maxlength="50" required>{{ $data->remark }}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card mt-2">
                <div class="card-header text-info">
                    <div class="form-row">
                        <div class="col">
                            <h2 class="font-weight-bold m-0">商品明細檔</h2>
                        </div>
                        <div class="col-auto">
                            <button class="btn btn-info" type="button" id="btn_create">
                                <i class="bi bi-plus-lg"></i>新增商品
                            </button>
                        </div>
                    </div>
                </div>
                <div class="card-body p-2">
                    <div class="FrozenTable" style="max-height: 47vh;">
                        <table class="table table-bordered">
                            <caption>說明：會將A料號轉換成B料號且A料號不得重複</caption>
                            <thead>
                                <tr>
                                    <th>操作</th>
                                    <th style="width: 12%;">A料號</th>
                                    <th style="width: 33%;">A品名</th>
                                    <th style="width: 50px;">轉</th>
                                    <th style="width: 12%;">B料號</th>
                                    <th style="width: 33%;">B品名</th>
                                </tr>
                            </thead>
                            <tbody id="tbody_items">
                                @if (!empty($data->details))
                                    @foreach ($data->details as $item)
                                        <tr>
                                            <td>
                                                <button class="btn btn-danger btn-sm"
                                                    type="button"onclick="ItemDelete(this)"><i
                                                        class="bi bi-trash"></i>刪除</button>
                                            </td>
                                            <td>
                                                <input class="form-control form-control-sm item_no" name="A"
                                                    type="text" list="itemList" onchange="SetItemDetail($(this))"
                                                    value="{{ $item->src_item_no }}" required>
                                            </td>
                                            <td class="text-left">
                                                <span class="item_name"
                                                    name="A">{{ $item->SrcItem->item_name ?? '' }}</span>
                                            </td>
                                            <td class="convert-icon">➜</td>
                                            <td>
                                                <input class="form-control form-control-sm item_no" name="B"
                                                    type="text" list="itemList" onchange="SetItemDetail($(this))"
                                                    value="{{ $item->item_no }}" required>
                                            </td>
                                            <td class="text-left">
                                                <span class="item_name"
                                                    name="B">{{ $item->Item->item_name }}</span>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <datalist id="itemList">
                @if (!empty($items) && count($items) > 0)
                    @foreach ($items as $key => $item)
                        <option value="{{ $item->ICODE }}" data-price="{{ $item->IUNIT }}"
                            data-cost="{{ $item->ISOUR }}">
                            {{ $item->INAME }}</option>
                    @endforeach
                @endif
            </datalist>
            <input id="input_details" name="details" type="hidden">
            <button id="btn_confirm" class="btn btn-primary btn-sm mt-2" type="submit">確認</button>
        </form>
    </div>
@stop
@section('script')
    <script type="text/javascript">
        $(document).ready(function() {
            InitialDate();
            if ($('#tbody_items>tr').length == 0) {
                CreateItemDOM();
            }
        });

        function InitialDate() {
            var today = new Date();
            if ($("#input_s_date").val() == '') {
                $("#input_s_date").val(DateToString(today));
            }
            if ($("#input_e_date").val() == '') {
                $("#input_e_date").val(DateToString(today));
            }
        }
        //去空白
        $(function() {
            $('input[type="text"]').change(function() {
                this.value = $.trim(this.value);
            });
        });
        //確認
        $("#btn_confirm").click(function() {
            var itemList = new Array();
            var src_item_list = [];
            var error = false;
            var msg = '';
            $("#tbody_items>tr").each(function(index, element) {
                var src_item_no = $(element).find('input.item_no[name="A"]').val();
                if (IsNullOrEmpty(src_item_no)) {
                    error = true;
                    msg = '請輸入A料號';
                    return false;
                } else if ($.inArray(src_item_no, src_item_list) !== -1) {
                    error = true;
                    msg = 'A料號不能重複';
                    return false;
                }
                src_item_list.push(src_item_no);
                var input_item_no = $(element).find('input.item_no[name="B"]');
                if (verifiDatalist(input_item_no)) {
                    var item_no = $(input_item_no).val();
                    var item = {
                        src_item_no: src_item_no,
                        item_no: item_no,
                    };
                    itemList.push(item);
                } else {
                    error = true;
                    msg = 'B料號不存在系統';
                    return false;
                }

            });
            if (error) {
                alert(msg);
                return false;
            }
            $("#input_details").val(JSON.stringify(itemList));
        });
        //建立商品明細
        $("#btn_create").click(CreateItemDOM);
        //建立商品明細DOM
        function CreateItemDOM() {
            var dom = '';
            dom += '<tr class="table-danger">';
            //操作
            dom +=
                '<td><button class="btn btn-danger btn-sm" type="button"onclick="ItemDelete(this)"><i class="bi bi-trash"></i>刪除</button></td>';
            //A料號
            dom +=
                '<td><input class="form-control form-control-sm item_no" name="A" type="text" list="itemList" onchange="SetItemDetail($(this))" required></td>';
            //A品名
            dom +=
                '<td class="text-left"><span class="item_name" name="A">請輸入料號</span></td>';
            dom += '<td class="convert-icon">➜</td>';
            //B料號
            dom +=
                '<td><input class="form-control form-control-sm item_no" name="B" type="text" list="itemList" onchange="SetItemDetail($(this))" required></td>';
            //B品名
            dom +=
                '<td class="text-left"><span class="item_name" name="B">請輸入料號</span></td>';
            dom += '</tr>';

            $("#tbody_items").append(dom);
            $("#tbody_items>tr:last-child>td:eq(0)>input").focus();
        }
        //帶入料號資料
        function SetItemDetail(obj) {
            var name = $(obj).attr('name');
            var status = 0;
            var item_no = obj.val();
            var item_name = '';
            if (IsNullOrEmpty(item_no)) {
                status = 0;
                item_name = '請輸入料號';
            } else {
                var item = $('#itemList>option[value="' + item_no + '"]');
                if (name == 'B' && item.length == 0) {
                    status = 1;
                    item_name = '找不到該料號';
                } else {
                    status = 2;
                    item_name = item.text();
                }
            }
            var tr = obj.closest('tr');
            $(tr).find('.item_name[name="' + name + '"]').text(item_name);
            switch (status) {
                case 0:
                case 1:
                    tr.addClass('table-danger');
                    break;
                case 2:
                    tr.removeClass('table-danger');
                    break;
            }
        };
        //明細刪除按鈕
        function ItemDelete(obj) {
            if ($("#tbody_items>tr").length == 1) {
                alert('最少輸入一筆商品明細');
            } else {
                $(obj).closest('tr').remove();
            }
        }
        //驗證輸入是否在選項內
        function verifiDatalist(obj) {
            const $input = obj,
                $options = $('#' + $input.attr('list') + ' option'),
                inputVal = $input.val();
            let verification = false;
            for (let i = 0; i < $options.length; i++) {
                const $option = $options.eq(i),
                    showWord = $option.val();
                if (showWord == inputVal) {
                    verification = true;
                }
            }
            return verification;
        }
    </script>
@stop
