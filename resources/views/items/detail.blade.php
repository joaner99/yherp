@extends('layouts.base')
@section('css')
    <style type="text/css">
        #div_img {
            background-color: rgb(119, 119, 119);
        }

        .FrozenTable>table table th {
            background-color: #6c757d;
        }

        .carousel-item img {
            width: auto;
            max-width: 100%;
            max-height: 75vh;
            margin: auto;
        }
    </style>
@endsection
@section('title')
    商品查詢
@stop
@section('content')
    <div class="container-fluid">
        <div class="form-row">
            <div class="col-8">
                <form action="{{ route('items.detail') }}" method="GET">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                                產品代號/EAN13碼/Code128/外部碼
                            </span>
                        </div>
                        <input name="id1" class="form-control" style="height: auto;" type="text" autocomplete="off"
                            autofocus>
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                                清單
                            </span>
                        </div>
                        <input type="text" name="id2" id="input_id" list="idList" class="form-control"
                            style="height: auto;" autocomplete="off">
                        <datalist id="idList">
                            @if (!empty($item_all))
                                @foreach ($item_all as $item)
                                    <option value="{{ $item->ICODE }}">{{ $item->INAME }}</option>
                                @endforeach
                            @endif
                        </datalist>
                        <div class="input-group-append">
                            <button id="btn_search" class="btn btn-primary" type="submit">查詢</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="form-row">
            <div class="col-xl-8 pb-2">
                <div class="d-flex">
                    @if (empty($data))
                        <div class="col text-center" style="font-size: 15rem;">
                            <i class="bi bi-upc-scan"></i>
                        </div>
                        <div class="col font-weight-bold d-flex flex-column align-items-start justify-content-center"
                            style="font-size: 2rem;">
                            請使用手持式掃描機掃碼
                        </div>
                    @elseif(empty($data['name']))
                        <div class="col text-center" style="font-size: 15rem;">
                            <i class="bi bi-x-octagon-fill text-danger"></i>
                        </div>
                        <div class="col font-weight-bold d-flex flex-column align-items-start justify-content-center"
                            style="font-size: 2rem;">
                            <div>料號：{{ $data['id'] }}</div>
                            <div>找不到該料號商品資料，料號輸入錯誤</div>
                        </div>
                    @elseif(!empty($data['img']))
                        <div class="container-fluid p-0">
                            <div class="card">
                                <div class="card-header text-white bg-success">
                                    實際照片
                                </div>
                                <div class="card-body p-2">
                                    <div id="div_img" class="carousel slide" data-ride="carousel">
                                        <ol class="carousel-indicators">
                                            @foreach ($data['img'] as $key => $img)
                                                <li data-target="#div_img" data-slide-to="{{ $key }}"
                                                    {{ $loop->first ? 'class="active"' : '' }}></li>
                                            @endforeach
                                        </ol>
                                        <div class="carousel-inner">
                                            @foreach ($data['img'] as $img)
                                                <div class="carousel-item {{ $loop->first ? 'active' : '' }}">
                                                    @if ($img === false)
                                                        <img src="{{ asset('img/null_image.png') }}" class="d-block">
                                                    @else
                                                        <img src="{{ asset('storage/' . $img) }}" class="d-block">
                                                    @endif
                                                </div>
                                            @endforeach
                                        </div>
                                        <a class="carousel-control-prev" href="#div_img" role="button" data-slide="prev">
                                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                            <span class="sr-only">Previous</span>
                                        </a>
                                        <a class="carousel-control-next" href="#div_img" role="button" data-slide="next">
                                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                            <span class="sr-only">Next</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @else
                        <div class="col p-0">
                            <div class="alert alert-info" style="font-size: 5rem;">
                                該商品尚未建立相關圖片
                            </div>
                        </div>
                    @endif
                </div>
            </div>
            <div class="col-xl-4 pb-2">
                @if (!empty($data) && !empty($data['name']))
                    @if ($data['pause'] == 1 || $data['stop'] == 1)
                        <div class="d-flex justify-content-around mb-2" style="font-size: 2.5rem;">
                            @if ($data['pause'] == 1)
                                <div class="alert alert-warning">
                                    <i class="bi bi-exclamation-triangle-fill">
                                        暫停出貨
                                    </i>
                                </div>
                            @endif
                            @if ($data['stop'] == 1)
                                <div class="alert alert-danger">
                                    <i class="bi bi-x-circle-fill">
                                        廠商停產
                                    </i>
                                </div>
                            @endif
                        </div>
                    @endif
                    <div class="card">
                        <div class="card-header bg-warning">
                            詳細資訊
                        </div>
                        <div class="card-body p-2">
                            <div class="FrozenTable" style="max-height: 78vh;">
                                <table class="table table-bordered">
                                    <tbody>
                                        <tr>
                                            <th style="width: 20%;">料號</th>
                                            <td>{{ $data['id'] }}</td>
                                            <th style="width: 20%;">ENA13碼</th>
                                            <td>{{ $data['ena13'] }}</td>
                                        </tr>
                                        <tr>
                                            <th>Code128</th>
                                            <td>{{ $data['co128'] }}</td>
                                            <th>外部碼</th>
                                            <td>{{ $data['id2'] }}</td>
                                        </tr>
                                        <tr>
                                            <th>大類類別</th>
                                            <td>{{ $data['kind'] }}</td>
                                            <th>中類類別</th>
                                            <td>{{ $data['kind2'] }}</td>
                                        </tr>
                                        <tr>
                                            <th>小類類別</th>
                                            <td>{{ $data['kind3'] }}</td>
                                        </tr>
                                        <tr>
                                            <th>名稱</th>
                                            <td colspan="3" style="font-size: 0.9rem;">{{ $data['name'] }}</td>
                                        </tr>
                                        <tr>
                                            <th>儲位</th>
                                            <td>{{ $data['storage'] }}</td>
                                            <th>產地國別</th>
                                            <td>{{ $data['origin'] }}</td>
                                        </tr>
                                        <tr>
                                            <th>庫存</th>
                                            <td colspan="3">
                                                <table class="w-100 sortable">
                                                    <thead>
                                                        <tr>
                                                            <th>倉別</th>
                                                            <th>庫存數</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach ($data['stock'] as $item)
                                                            <tr>
                                                                <td>{{ $item['name'] }}</td>
                                                                <td>{{ $item['amount'] }}</td>
                                                            </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>單品</th>
                                            <td colspan="3">
                                                <table class="w-100">
                                                    <tbody>
                                                        <tr>
                                                            <th style="width: 27%;">重量(公斤)</th>
                                                            <td>{{ number_format($data['weight'], 3) }}</td>
                                                        </tr>
                                                        <tr>
                                                            <th>體積(公分)</th>
                                                            <td>
                                                                {{ number_format($data['depth'], 1) }}
                                                                <small class="text-muted mr-3">長</small>
                                                                {{ number_format($data['width'], 1) }}
                                                                <small class="text-muted mr-3">寬</small>
                                                                {{ number_format($data['height'], 1) }}
                                                                <small class="text-muted">高</small>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>包裝</th>
                                            <td colspan="3">
                                                <table class="w-100">
                                                    <tbody>
                                                        <tr>
                                                            <th style="width: 27%;">重量(公斤)</th>
                                                            <td colspan="2">{{ number_format($data['box_weight'], 3) }}
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th>體積(公分)</th>
                                                            <td colspan="2">
                                                                {{ number_format($data['box_depth'], 1) }}
                                                                <small class="text-muted mr-3">長</small>
                                                                {{ number_format($data['box_width'], 1) }}
                                                                <small class="text-muted mr-3">寬</small>
                                                                {{ number_format($data['box_height'], 1) }}
                                                                <small class="text-muted">高</small>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th rowspan="2" class="align-middle">材積</th>
                                                            <td class="border-right-0">
                                                                {{ number_format($data['box_cuft'], 8) }}
                                                            </td>
                                                            <td class="border-left-0 text-left">
                                                                <small class="text-muted">Cuft(立方呎)</small>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="border-right-0">
                                                                {{ number_format($data['box_cbm'], 8) }}
                                                            </td>
                                                            <td class="border-left-0 text-left">
                                                                <small class="text-muted">CBM(立方公尺)</small>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
    </div>
@stop
@section('script')
    <script type="text/javascript"></script>
@stop
