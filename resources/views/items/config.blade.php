@extends('layouts.base')
@section('title')
    商品參數設定
@stop
@section('css')
    <style type="text/css">
        .changed {
            background-color: #ffc107;
        }

        input[type="checkbox"] {
            transform: scale(2);
        }
    </style>
@endsection
@section('content')
    {{-- Modal 上傳 --}}
    <div id="modal_upload" class="modal">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <form action="{{ route('items.import') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title">匯入</h5>
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">商品參數設定表</span>
                            </div>
                            <div class="custom-file">
                                <input class="custom-file-input" id="input_file" name="file" type="file"
                                    accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel"
                                    required>
                                <label class="custom-file-label" for="input_file">請選擇檔案</label>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button id="btn_import" type="submit" class="btn btn-primary">確認</button>
                        <button id="btn_loading" type="button" class="btn btn-primary" style="display: none;" disabled>
                            <div class="spinner-border spinner-border-sm">
                                <span class="sr-only"></span>
                            </div>
                            <span>匯入中...</span>
                        </button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">關閉</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="form-row mb-2">
            <div class="col-auto">
                <form action="{{ route('export_table') }}" method="post">
                    @csrf
                    <input name="export_name" type="hidden" value="商品參數設定表">
                    <input name="export_data" type="hidden">
                    <button class="btn btn-success" type="submit" export-target="#table_data">
                        <i class="bi bi-box-arrow-up"></i>匯出
                    </button>
                </form>
            </div>
            <div class="col-auto">
                <button class="btn btn-info" type="button" data-toggle="modal" data-target="#modal_upload">
                    <i class="bi bi-box-arrow-in-down"></i>匯入
                </button>
            </div>
            <div class="col"></div>
            <div class="col-auto">
                <form action="{{ route('items.config_sync') }}" method="GET">
                    <button id="btn_sync" class="btn btn-success" type="submit">
                        <i class="bi bi-arrow-repeat"></i>ERP同步
                    </button>
                </form>
            </div>
            <div class="col-auto">
                <form action="{{ route('items.config_update') }}" method="post">
                    @csrf
                    <input name="data" type="hidden" value="">
                    <button id="btn_update" class="btn btn-warning" type="submit">
                        <i class="bi bi-cloud-upload"></i>套用變更<span id="span_changed"></span>
                    </button>
                </form>
            </div>
        </div>
        <div class="form-row">
            <div class="col">
                <div class="FrozenTable" style="max-height: 87vh; font-size: 0.9rem;">
                    <table id="table_data" class="table table-bordered sortable">
                        <thead>
                            <tr>
                                <th>序</th>
                                <th style="width: 10%;">料號</th>
                                <th>名稱</th>
                                <th style="width: 6%;">停用安庫</th>
                                <th style="width: 8%;">停用毛利檢查</th>
                                <th style="width: 8%;">安庫量</th>
                                <th class="d-none" style="width: 8%;">出貨倉安庫量</th>
                                @role('倉管')
                                    <th style="width: 8%;">調撥單位</th>
                                @endrole
                                <th style="width: 8%;">採購單位</th>
                                <th style="width: 10%;">進價</th>
                                <th style="width: 7%;">庫存週轉率<br>評等</th>
                                <th class="export-none" style="width: 9%;">更新日期</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (!empty($data))
                                <?php $index = 1; ?>
                                @foreach ($data as $item)
                                    <tr data-id="{{ $item->id }}">
                                        <td>{{ $index++ }}</td>
                                        <td>{{ $item->id }}</td>
                                        <td class="text-left">{{ $item->name }}</td>
                                        <td>
                                            <input type="checkbox" data-col="disable_ss"
                                                data-original="{{ $item->disable_ss }}"
                                                {{ $item->disable_ss ? 'checked' : '' }}>
                                        </td>
                                        <td>
                                            <input type="checkbox" data-col="disable_gpm"
                                                data-original="{{ $item->disable_gpm }}"
                                                {{ $item->disable_gpm ? 'checked' : '' }}>
                                        </td>
                                        <td>
                                            <input class="form-control text-center" data-col="main_ss"
                                                data-original="{{ $item->main_ss }}" type="number"
                                                value="{{ $item->main_ss }}" min="0" step="1">
                                        </td>
                                        <td class="d-none">{{ number_format($item->sale_ss) }}</td>
                                        @role('倉管')
                                            <td>
                                                <input class="form-control text-center" data-col="box_qty"
                                                    data-original="{{ $item->box_qty }}" type="number"
                                                    value="{{ $item->box_qty }}" min="0" step="1">
                                            </td>
                                        @endrole
                                        <td>
                                            <input class="form-control text-center" data-col="purchase_qty"
                                                data-original="{{ $item->purchase_qty }}" type="number"
                                                value="{{ $item->purchase_qty }}" min="0" step="1">
                                        </td>
                                        <td>{{ number_format($item->cost, 2) }}</td>
                                        <td>
                                            <select class="custom-select" data-col="rating_id"
                                                data-original="{{ $item->Rating->id ?? '' }}">
                                                <option {{ empty($item->Rating->rating) ? 'selected' : '' }}></option>
                                                @foreach ($rating_list as $r)
                                                    <option value="{{ $r->id }}"
                                                        {{ !empty($item->Rating->rating) && $item->Rating->rating == $r->rating ? 'selected' : '' }}>
                                                        {{ $r->rating }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </td>
                                        <td>{{ $item->updated_at }}</td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop
@section('script')
    <script type="text/javascript">
        var changed_count = 0;
        //上傳檔案後顯示文件名稱
        $('#input_file').change(function(e) {
            var fileName = '';
            $(e.target.files).each(function(index, element) {
                if (fileName != '') {
                    fileName += ',';
                }
                fileName += element.name;
            });
            $(this).next('.custom-file-label').html(fileName);
        });
        //匯入
        $("#btn_import").click(function() {
            if (!IsNullOrEmpty($('#input_file').val())) {
                $("#btn_import").hide();
                $("#btn_loading").show();
            }
        });
        //異動內容轉json
        $('#btn_update').click(function() {
            var data = new Array();
            $('#table_data>tbody>.changed').each(function(i, e) {
                var id = $(e).data().id;
                data.push({
                    id: id,
                    main_ss: $(e).find('[data-col="main_ss"]').val(),
                    disable_ss: $(e).find('[data-col="disable_ss"]').is(":checked") ? 1 : 0,
                    disable_gpm: $(e).find('[data-col="disable_gpm"]').is(':checked') ? 1 : 0,
                    box_qty: $(e).find('[data-col="box_qty"]').val(),
                    purchase_qty: $(e).find('[data-col="purchase_qty"]').val(),
                    rating_id: $(e).find('[data-col="rating_id"]').val(),
                });
            });
            $('input[name="data"]').val(JSON.stringify(data));
            lockScreen();
        });
        $('#btn_sync').click(function() {
            lockScreen();
        });
        //值異動
        $('#table_data>tbody>tr>td>select, #table_data>tbody>tr>td>input').change(function() {
            var tr = $(this).closest('tr');
            var changed = false;
            $(tr).find('[data-col]').each(function(i2, e2) {
                var val;
                if ($(e2).prop('type') == 'checkbox') {
                    val = $(e2).is(":checked") ? 1 : 0;
                } else {
                    val = $(e2).val();
                }
                if (val != $(e2).data().original) {
                    changed = true;
                    return false;
                }
            });
            if (changed) {
                if (!$(tr).hasClass('changed')) {
                    $(tr).addClass('changed');
                    changed_count++;
                }
            } else {
                $(tr).removeClass('changed');
                changed_count--;
            }
            if (changed_count > 0) {
                $('#span_changed').text('【' + changed_count + '筆】');
            } else {
                $('#span_changed').text('');
            }
        });
    </script>
@stop
