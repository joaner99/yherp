@extends('layouts.base')
@section('css')
    <style type="text/css">
        /*進貨*/
        .Purchase {
            background-color: #007bff;
            color: #fff;
        }

        /*進貨退回*/
        .RPurchase {
            background-color: whitesmoke;
            color: #343a40;
        }

        /*銷貨*/
        .Sales {
            background-color: purple;
            color: #fff;
        }

        /*銷貨退回*/
        .RSales {
            background-color: #ffc107;
            color: #343a40;
        }

        /*POS銷貨*/
        .POSSales {
            background-color: mediumpurple;
            color: #fff;
        }

        /*POS銷貨退回*/
        .RPOSSales {
            background-color: lightyellow;
            color: #343a40;
        }

        /*調撥*/
        .Transfer {
            background-color: #1d565f;
            color: #fff;
        }

        /*盤盈*/
        .InventoryProfit {
            background-color: lime;
            color: #343a40;
        }

        /*盤虧*/
        .InventoryLoss {
            background-color: red;
            color: #fff;
        }

        /*庫存減少註記*/
        .Reduce {
            color: red;
            font-weight: bold;
        }
    </style>
@endsection
@section('title')
    商品履歷
@stop
@section('content')
    <div class="container-fluid">
        <form action="{{ route('items.log') }}" method="GET">
            <div class="form-row">
                <div class="form-group col-md-2">
                    <label for="input_start_date">起日</label>
                    <input id="input_start_date" name="start_date" class="form-control" type="date"
                        value="{{ old('start_date') }}">
                </div>
                <div class="form-group col-md-2">
                    <label for="input_end_date">訖日</label>
                    <input id="input_end_date" name="end_date" class="form-control" type="date"
                        value="{{ old('end_date') }}">
                </div>
                <div class="form-group col-md-3">
                    <label for="input_id">
                        商品料號
                    </label>
                    <input id="input_id" name="id" class="form-control" style="height: auto;" type="text"
                        value="{{ old('id') }}" list="idList" autocomplete="off" required>
                    <datalist id="idList">
                        @if (!empty($item_all))
                            @foreach ($item_all as $item)
                                <option value="{{ $item->ICODE }}">{{ $item->INAME }}</option>
                            @endforeach
                        @endif
                    </datalist>
                </div>
            </div>
            <button class="btn btn-primary" type="submit">查詢</button>
        </form>
        <div class="form-row mt-2">
            <div class="col-10">
                <div class="FrozenTable" style="max-height: 72vh;">
                    @if (!empty($datas))
                        <table id="table_data" class="table table-bordered table-hover sortable table-filter"
                            style="min-width: 720px;">
                            <caption>【{{ old('id') }}】{{ $item_name }}</caption>
                            <thead>
                                <tr>
                                    <th style="width: 9%;" class="filter-col">申請日期</th>
                                    <th style="width: 16%;">最後異動時間</th>
                                    <th class="filter-col" style="width: 10%;">類別</th>
                                    <th>單號</th>
                                    <th class="filter-col">倉別</th>
                                    <th class="filter-col">客戶簡稱</th>
                                    <th style="width: 7%;">數量</th>
                                    <th style="width: 10%;">編輯者</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($datas as $value)
                                    <tr>
                                        <td>{{ $value['Date'] }}</td>
                                        <td>{{ $value['Time'] }}</td>
                                        <td class="{{ $value['TypeClass'] }}">{{ $value['Type'] }}</td>
                                        <td>{{ $value['Id'] }}</td>
                                        <td class="{{ $value['StockClass'] }}">{{ $value['Stock'] }}</td>
                                        <td>{{ $value['Cust'] }}</td>
                                        <td class="{{ $value['NumClass'] }}">{{ number_format($value['Num']) }}</td>
                                        <td>{{ $value['UserName'] }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    @elseif(empty(old('id')))
                        <div class="alert alert-info">
                            請輸入日期、料號
                        </div>
                    @else
                        <div class="alert alert-danger">
                            找不到相關資料
                        </div>
                    @endif
                </div>
            </div>
            <div class="col-2">
                @if (!empty($stock) && count($stock) > 0)
                    <div class="FrozenTable" style="max-height: 72vh;">
                        <table class="table table-bordered sortable">
                            <thead>
                                <tr>
                                    <th>倉別</th>
                                    <th>數量</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($stock as $item)
                                    <tr>
                                        <td>{{ $item['name'] }}</td>
                                        <td>{{ number_format($item['qty']) }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                @endif
            </div>
        </div>
        <div class="form-row mt-2">
            <div class="form-group col-auto">
                @if (!empty($datas))
                    <form action="{{ route('export_table') }}" method="post">
                        @csrf
                        <input name="export_name" type="hidden" value="商品履歷">
                        <input name="export_data" type="hidden">
                        <button class="btn btn-success" type="submit" export-target="#table_data">
                            <i class="bi bi-file-earmark-excel"></i>匯出Excel
                        </button>
                    </form>
                @endif
            </div>
            <div class="form-group col-auto">
                <span>小計:</span>
                @if (!empty($subtotal))
                    <span id="span_suptotal" class="{{ $subtotal < 0 ? 'Reduce' : '' }}">{{ $subtotal }}</span>
                @endif
            </div>
        </div>
    </div>
@stop
@section('script')
    <script type="text/javascript">
        $(document).ready(InitialDate());

        function InitialDate() {
            var today = new Date();
            var oneMonthAgo = new Date();
            oneMonthAgo.setMonth(today.getMonth() - 1);

            if ($("#input_start_date").val() == '') {
                $("#input_start_date").val(DateToString(oneMonthAgo));
            }
            if ($("#input_end_date").val() == '') {
                $("#input_end_date").val(DateToString(today));
            }
            $("#input_start_date,#input_end_date").attr("max", DateToString(today));
        }
    </script>
@stop
