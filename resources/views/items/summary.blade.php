@extends('layouts.base')
@section('title')
    商品總表
@stop
@section('css')
    <style type="text/css">
        .hidden {
            background-color: gray;
            color: white;
        }
    </style>
@endsection
@section('content')
    {{-- Modal 編碼原則 --}}
    <div id="modal_kind" class="modal">
        <div class="modal-dialog modal-dialog-centered" style="max-width: 80vw;">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">編碼原則</h5>
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    @if (!empty($item_kind) && count($item_kind) > 0)
                        <div class="form-row">
                            @foreach ($item_kind->sortBy('value1')->groupBy('value1') as $kind => $group)
                                <?php
                                switch ($kind) {
                                    case '1':
                                        $kind_name = '大類';
                                        $kind_color = 'text-secondary';
                                        break;
                                    case '2':
                                        $kind_name = '中類';
                                        $kind_color = 'text-success';
                                        break;
                                    case '3':
                                        $kind_name = '小類';
                                        $kind_color = 'text-warning';
                                        break;
                                    case '4':
                                        $kind_name = '廠商';
                                        $kind_color = 'text-primary';
                                        break;
                                
                                    default:
                                        $kind_name = '';
                                        $kind_color = '';
                                        break;
                                }
                                ?>
                                <div class="col-3">
                                    <h2 class="{{ $kind_color }}">{{ $kind_name }}</h2>
                                    <ul class="list-group overflow-auto" style="max-height: 600px;">
                                        @foreach ($group->sortBy('value2') as $item)
                                            <li class="list-group-item">【{{ $item->value2 }}】{{ $item->value3 }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endforeach
                        </div>
                    @endif
                </div>
                <div class="modal-footer">
                    <a href="{{ route('sys_config.edit', $item_kind->first()->m_id) }}" class="btn btn-warning">修改編碼原則</a>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">關閉</button>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        @if (empty($data) || count($data) == 0)
            <div class="alert alert-danger">
                找不到相關資料
            </div>
        @else
            <div class="form-row align-items-center mb-2">
                <div class="col-auto">
                    <div class="form-check-list-horizontal">
                        <div class="form-check">
                            <form action="{{ route('items.summary') }}" method="GET">
                                <input class="form-check-input" type="checkbox" id="chk_show_hidden" name="show_hidden"
                                    value='1' {{ request()->has('show_hidden') ? 'checked' : '' }}>
                                <label class="form-check-label" for="chk_show_hidden">
                                    顯示隱藏商品(暫停進/出貨、淘汰品)
                                </label>
                                <button id="submit" class="d-none" type="submit"></button>
                            </form>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" id="chk_show_b" data-type="b">
                            <label class="form-check-label" for="chk_show_b" checked>顯示條碼資訊</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" id="chk_show_p" data-type="p">
                            <label class="form-check-label" for="chk_show_p" checked>顯示單品資訊</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" id="chk_show_box" data-type="box">
                            <label class="form-check-label" for="chk_show_box" checked>顯示包裝資訊</label>
                        </div>
                    </div>
                </div>
                <div class="col"></div>
                <div class="col-auto">
                    <button class="btn btn-info btn-sm" data-toggle="modal" data-target="#modal_kind"
                        type="button">編碼原則</button>
                </div>
                <div class="col-auto">
                    <form action="{{ route('export_table') }}" method="post">
                        @csrf
                        <input name="export_name" type="hidden" value="商品總表">
                        <input name="export_data" type="hidden">
                        <button class="btn btn-success btn-sm" type="submit" export-target="#table_data">
                            <i class="bi bi-file-earmark-excel"></i>匯出Excel
                        </button>
                    </form>
                </div>
            </div>
            <div class="form-row">
                <div class="col">
                    <div class="FrozenTable" style="font-size: 0.85rem; max-height: 85vh;">
                        <table id="table_data" class="table table-bordered sortable table-filter">
                            <thead>
                                <tr>
                                    <th style="width: 150px;">產品<br>代號</th>
                                    <th data-type="b">EAN13</th>
                                    <th data-type="b">CODE128</th>
                                    <th data-type="b">外部碼</th>
                                    <th style="width: 90px;">大類<br>代號</th>
                                    <th class="filter-col" style="width: 150px;">大類<br>名稱</th>
                                    <th style="width: 90px;">中類<br>代號</th>
                                    <th class="filter-col" style="width: 150px;">中類<br>名稱</th>
                                    <th style="width: 90px;">小類<br>代號</th>
                                    <th class="filter-col" style="width: 150px;">小類<br>名稱</th>
                                    <th style="width: 90px;">流水號</th>
                                    <th>產品<br>名稱</th>
                                    <th style="width: 90px;">供應廠商<br>代號</th>
                                    <th class="filter-col" style="width: 150px;">供應廠商<br>名稱</th>
                                    <th style="width: 100px;">進價</th>
                                    @can('item_summary_columns')
                                        <th style="width: 100px;">採購成本</th>
                                    @endcan
                                    <th data-type="p">單品重量<br>(公斤)</th>
                                    <th data-type="p">單品長度<br>(公分)</th>
                                    <th data-type="p">單品寬度<br>(公分)</th>
                                    <th data-type="p">單品高度<br>(公分)</th>
                                    <th data-type="box">包裝重量<br>(公斤)</th>
                                    <th data-type="box">包裝長度<br>(公分)</th>
                                    <th data-type="box">包裝寬度<br>(公分)</th>
                                    <th data-type="box">包裝高度<br>(公分)</th>
                                    <th data-type="box">包裝材積<br>(CUFT)</th>
                                    <th data-type="box">包裝材積<br>(CBM)</th>
                                    <th class="filter-col" style="width: 90px;">產地國別</th>
                                    <th class="filter-col" style="width: 90px;">暫停進貨</th>
                                    <th class="filter-col" style="width: 90px;">暫停出貨</th>
                                    <th class="filter-col" style="width: 90px;">淘汰品</th>
                                    <th class="filter-col" style="width: 90px;">廠商停產</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (!empty($data))
                                    @foreach ($data as $item)
                                        <tr
                                            class="{{ $item->INSTOP == 1 && $item->ISTOP == 1 && $item->STOPSALE == 1 ? 'hidden' : '' }}">
                                            <td>{{ $item->item_no }}</td>
                                            <td>{{ $item->ENA13 }}</td>
                                            <td>{{ $item->CO128 }}</td>
                                            <td>{{ $item->ICOD2 }}</td>
                                            <td>{{ $item->kind }}</td>
                                            <td>{{ $item->kind_name }}</td>
                                            <td>{{ $item->kind2 }}</td>
                                            <td>{{ $item->kind2_name }}</td>
                                            <td>{{ $item->kind3 }}</td>
                                            <td>{{ $item->kind3_name }}</td>
                                            <td>{{ substr($item->item_no, 8, 3) ?? '' }}</td>
                                            <td class="text-left">{{ $item->item_name }}</td>
                                            <td>{{ $item->supply }}</td>
                                            <td>{{ $item->supply_name }}</td>
                                            <td>{{ $item->cost }}</td>
                                            @can('item_summary_columns')
                                                <td>{{ number_format($item->erp_cost, 2) }}</td>
                                            @endcan
                                            <td>{{ $item->IWEIGHT }}</td>
                                            <td>{{ $item->PDepth }}</td>
                                            <td>{{ $item->PWidth }}</td>
                                            <td>{{ $item->PHeight }}</td>
                                            <td>{{ $item->Box_Weight }}</td>
                                            <td>{{ $item->Box_Depth }}</td>
                                            <td>{{ $item->Box_Width }}</td>
                                            <td>{{ $item->Box_Height }}</td>
                                            <td>{{ $item->Box_CUFT }}</td>
                                            <td>{{ $item->Box_CBM }}</td>
                                            <td>{{ $item->country }}</td>
                                            <td>{{ $item->INSTOP ? '✔' : '' }}</td>
                                            <td>{{ $item->ISTOP ? '✔' : '' }}</td>
                                            <td>{{ $item->STOPSALE ? '✔' : '' }}</td>
                                            <td>{{ $item->IPSTOP ? '✔' : '' }}</td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        @endif
    </div>
@stop
@section('script')
    <script type="text/javascript">
        $(document).ready(function() {
            $('input[data-type]').trigger("change");
        });
        //checkbox勾選觸發submit
        $("#chk_show_hidden").click(function() {
            $("#submit").click();
        });
        $('input[data-type]').change(function() {
            var type = $(this).data().type;
            var show = $(this).prop('checked');
            $('#table_data>thead>tr>th[data-type="' + type + '"]').each(function(index, element) {
                var idx = $(element).index() + 1;
                $('#table_data th:nth-child(' + idx + ')').toggle(show);
                $('#table_data td:nth-child(' + idx + ')').toggle(show);
            });
            var c = $('#table_data>thead>tr:first-child>th:visible').length;
            $('#table_data').css('min-width', (c * 7 + 'vw'));
        });
    </script>
@stop
