@extends('layouts.base')
@section('css')
    <style type="text/css">

    </style>
@endsection
@section('title')
    出貨儲位建議
@stop
@section('content')
    <div class="container-fluid">
        <div class="form-row">
            <div class="col text-success d-flex align-items-center">
                ※新品：首次進貨距今3個月內。層順序：2>3>4>1。層上限：2層=1項、3層=8項、4層=14項。相同大中小類不得放同一層。長條商品只能放第4層。
            </div>
            <div class="col-auto d-flex align-items-center">
                <div class="form-group form-check m-0">
                    <input type="checkbox" class="form-check-input" id="input_empty_stock" checked="checked"
                        onclick="input_empty_stock_onclick()">
                    <label class="form-check-label" for="input_empty_stock">顯示空庫存</label>
                </div>
            </div>
            @if (!empty($cfg_id))
                <div class="col-auto">
                    <a href="{{ route('sys_config.edit', $cfg_id) . '?redirect=items.place' }}"
                        class="btn btn-warning">修改參數</a>
                </div>
            @endif
            <div class="col-auto">
                <form action="{{ route('export_table') }}" method="post">
                    @csrf
                    <input name="export_name" type="hidden" value="儲位建議">
                    <input name="export_data" type="hidden">
                    <button id="btn_export" class="btn btn-success" type="submit" export-target="#table_data">
                        <i class="bi bi-file-earmark-excel"></i>匯出Excel
                    </button>
                </form>
            </div>
        </div>
        <div class="form-row mt-2">
            <div class="col">
                <div class="FrozenTable" style="font-size:0.9rem; max-height: 87vh;">
                    <table id="table_data" class="table table-bordered sortable table-filter">
                        <caption>商品總表</caption>
                        <thead>
                            <tr>
                                <th class="filter-col">儲位</th>
                                <th class="filter-col">架</th>
                                <th class="filter-col">層</th>
                                <th>評等</th>
                                <th>商品</th>
                                <th>大類</th>
                                <th>中類</th>
                                <th>小類</th>
                                <th>銷售數</th>
                                <th>庫存</th>
                            </tr>
                        </thead>
                        <tbody id="tbody_items">
                            @if (!empty($items))
                                @foreach ($items as $key => $item)
                                    <tr data-stock="{{ intval($item->stock) }}">
                                        <td>{{ $item->place ?? '' }}</td>
                                        <td>{{ $item->shelf ?? '' }}</td>
                                        <td>{{ $item->layer ?? '' }}</td>
                                        <td>{{ number_format($key + 1) }}</td>
                                        <td class="text-left">{{ "【{$item->ICODE}】{$item->INAME}" }}</td>
                                        <td class="text-left">{{ "【{$item->ITCOD}】{$item->ITNAM}" }}</td>
                                        <td class="text-left">{{ "【{$item->ITCO2}】{$item->ITNA2}" }}</td>
                                        <td class="text-left">{{ "【{$item->ITCO3}】{$item->ITNA3}" }}</td>
                                        <td>{{ isset($item->qty) ? number_format($item->qty) : '' }}</td>
                                        <td>{{ number_format($item->stock) }}</td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="form-row mt-2">
            @if (!empty($place))
                @foreach ($place->groupBy('shelf') as $shelf => $s)
                    <div class="col">
                        <div class="card">
                            <div class="card-header bg-primary text-white">
                                {{ $shelf }}架【{{ count($s) }}項】
                            </div>
                            <div class="card-body">
                                @foreach ($s as $l)
                                    <div class="card my-2">
                                        <div class="card-header bg-success text-white">
                                            {{ $l->layer }}層【{{ count($l->items) }}項】
                                        </div>
                                        <div class="card-body">
                                            <ul class="list-group">
                                                @foreach ($l->items as $item)
                                                    <li class="list-group-item">{{ "{$item->ICODE}" }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                @endforeach
            @endif
        </div>
    </div>
@stop
@section('script')
    <script type="text/javascript">
        function input_empty_stock_onclick() {
            var show = $("#input_empty_stock").prop("checked");
            if (show) {
                $('tr[data-stock]').show();
            } else {
                $('tr[data-stock="0"]').hide();
            }
        }
    </script>
@stop
