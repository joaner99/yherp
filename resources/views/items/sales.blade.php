@extends('layouts.base')
@section('title')
    商品進銷查詢
@stop
@section('css')
    <style type="text/css">
        h2 {
            margin: 0;
        }
    </style>
@endsection
@section('content')
    <div class="container-fluid">
        <div class="form-row">
            <div class="col-auto">
                <form action="{{ route('items.sales') }}" method="GET">
                    <div class="form-row">
                        <div class="form-group col-auto">
                            <label for="input_start_date">起日</label>
                            <input id="input_start_date" name="start_date" class="form-control" type="date"
                                value="{{ old('start_date') }}">
                        </div>
                        <div class="form-group col-auto">
                            <label for="input_end_date">訖日</label>
                            <input id="input_end_date" name="end_date" class="form-control" type="date"
                                value="{{ old('end_date') }}">
                        </div>
                        <div class="form-group col-auto">
                            <label for="input_id">料號</label>
                            <input id="input_id" name="id" class="form-control" style="height: auto;" type="text"
                                list="idList" autocomplete="off" value="{{ old('id') }}">
                        </div>
                    </div>
                    <button id="btn_search" class="btn btn-primary" type="submit">查詢</button>
                </form>
            </div>
        </div>
        <div class="form-row align-items-center mt-2">
            @if (!empty($item_all) && count($item_all) > 0)
                <div class="col-2">
                    料號：{{ old('id') }}
                </div>
                <div class="col-4">
                    品名：{{ $item_all->where('ICODE', old('id'))->first()->INAME ?? '' }}
                </div>
            @endif
        </div>
        <div class="form-row mt-2">
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        <div class="form-row align-items-center">
                            <h2 class="col-auto font-weight-bold text-info">銷貨單個毛利率</h2>
                            <div class="col">
                                總銷售數量：
                                @if (!empty($gpm) && count($gpm) > 0)
                                    {{ number_format($gpm->sum('qty')) }}
                                @endif
                            </div>
                            <div class="col"></div>
                            <div class="col-auto">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">顯示毛利率位於</span>
                                    </div>
                                    <input id="input_gpm_min" class="form-control" type="number">
                                    <div class="input-group-append">
                                        <span class="input-group-text">~</span>
                                    </div>
                                    <input id="input_gpm_max" class="form-control" type="number">
                                    <div class="input-group-append">
                                        <span class="input-group-text">%</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="FrozenTable" style="max-height: 30vh; font-size: 0.9rem;">
                            <table class="table table-bordered sortable">
                                <thead>
                                    <tr>
                                        <th>序</th>
                                        <th>來源</th>
                                        <th>訂單單號</th>
                                        <th>銷貨/結帳單號</th>
                                        <th>銷售量</th>
                                        <th>單個銷售額</th>
                                        <th>總銷售額</th>
                                        <th>單個成本</th>
                                        <th>單個毛利率</th>
                                    </tr>
                                </thead>
                                <tbody id="tbody_gpm">
                                    @if (!empty($gpm))
                                        @foreach ($gpm as $idx => $item)
                                            <tr data-gpm="{{ $item->gpm }}">
                                                <td>{{ $idx + 1 }}</td>
                                                <td>{{ $item->src }}</td>
                                                <td>{{ $item->order_no }}</td>
                                                <td>{{ $item->sales_no }}</td>
                                                <td>{{ number_format($item->qty) }}</td>
                                                <td>{{ number_format($item->sales, 2) }}</td>
                                                <td>{{ number_format($item->sales_total, 2) }}</td>
                                                <td>{{ number_format($item->cost, 2) }}</td>
                                                <td>{{ isset($item->gpm) ? number_format($item->gpm, 2) . '%' : '' }}</td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-row mt-2">
            <div class="col-6">
                <div class="card">
                    <div class="card-header bg-success">
                        <div class="form-row align-items-center">
                            <h2 class="col-auto font-weight-bold text-white">進貨</h2>
                        </div>
                    </div>
                    <div class="card-body p-2">
                        <div class="FrozenTable" style="max-height: 30vh; font-size: 0.9rem;">
                            <table class="table table-bordered sortable">
                                <thead>
                                    <tr>
                                        <th>序</th>
                                        <th>單號</th>
                                        <th>廠商簡稱</th>
                                        <th>日期</th>
                                        <th>數量</th>
                                        <th>單價</th>
                                        <th>小計</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if (!empty($purchase))
                                        @foreach ($purchase as $idx => $item)
                                            <tr>
                                                <td>{{ $idx + 1 }}</td>
                                                <td>{{ $item->order_no }}</td>
                                                <td>{{ $item->supply_name }}</td>
                                                <td>{{ $item->date }}</td>
                                                <td>{{ number_format($item->qty) }}</td>
                                                <td>{{ number_format($item->unit, 2) }}</td>
                                                <td>{{ number_format($item->total, 2) }}</td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-6">
                <div class="card">
                    <div class="card-header bg-success">
                        <div class="form-row align-items-center">
                            <h2 class="col-auto font-weight-bold text-white">進貨退回</h2>
                        </div>
                    </div>
                    <div class="card-body p-2">
                        <div class="FrozenTable" style="max-height: 30vh; font-size: 0.9rem;">
                            <table class="table table-bordered sortable">
                                <thead>
                                    <tr>
                                        <th>序</th>
                                        <th>單號</th>
                                        <th>廠商簡稱</th>
                                        <th>日期</th>
                                        <th>數量</th>
                                        <th>單價</th>
                                        <th>小計</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if (!empty($purchase_return))
                                        @foreach ($purchase_return as $idx => $item)
                                            <tr>
                                                <td>{{ $idx + 1 }}</td>
                                                <td>{{ $item->order_no }}</td>
                                                <td>{{ $item->supply_name }}</td>
                                                <td>{{ $item->date }}</td>
                                                <td>{{ number_format($item->unit) }}</td>
                                                <td>{{ number_format($item->qty, 2) }}</td>
                                                <td>{{ number_format($item->total, 2) }}</td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-row mt-2">
            <div class="col-12">
                <div class="card">
                    <div class="card-header bg-info">
                        <div class="form-row align-items-center">
                            <h2 class="col-auto font-weight-bold text-white">訂單</h2>
                        </div>
                    </div>
                    <div class="card-body p-2">
                        <div class="FrozenTable" style="max-height: 30vh; font-size: 0.9rem;">
                            <table class="table table-bordered sortable">
                                <thead>
                                    <tr>
                                        <th>序</th>
                                        <th>單號</th>
                                        <th>客戶簡稱</th>
                                        <th>日期</th>
                                        <th>數量</th>
                                        <th>單價</th>
                                        <th>小計</th>
                                        <th>狀態</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if (!empty($orders))
                                        @foreach ($orders as $idx => $item)
                                            <tr>
                                                <td>{{ $idx + 1 }}</td>
                                                <td>{{ $item->order_no }}</td>
                                                <td>{{ $item->cust_name }}</td>
                                                <td>{{ $item->date }}</td>
                                                <td>{{ number_format($item->qty) }}</td>
                                                <td>{{ number_format($item->unit, 2) }}</td>
                                                <td>{{ number_format($item->sales_total, 2) }}</td>
                                                <td>{{ $item->Main->status }}</td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-row mt-2">
            <div class="col-6">
                <div class="card">
                    <div class="card-header bg-warning">
                        <div class="form-row align-items-center">
                            <h2 class="col-auto font-weight-bold text-white">銷貨</h2>
                        </div>
                    </div>
                    <div class="card-body p-2">
                        <div class="FrozenTable" style="max-height: 30vh; font-size: 0.9rem;">
                            <table class="table table-bordered sortable">
                                <thead>
                                    <tr>
                                        <th>序</th>
                                        <th>單號</th>
                                        <th>客戶簡稱</th>
                                        <th>日期</th>
                                        <th>數量</th>
                                        <th>單價</th>
                                        <th>小計</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if (!empty($sales))
                                        @foreach ($sales as $idx => $item)
                                            <tr>
                                                <td>{{ $idx + 1 }}</td>
                                                <td>{{ $item->sale_no }}</td>
                                                <td>{{ $item->cust_name }}</td>
                                                <td>{{ $item->date }}</td>
                                                <td>{{ number_format($item->qty) }}</td>
                                                <td>{{ number_format($item->unit, 2) }}</td>
                                                <td>{{ number_format($item->sales_total, 2) }}</td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-6">
                <div class="card">
                    <div class="card-header bg-warning">
                        <div class="form-row align-items-center">
                            <h2 class="col-auto font-weight-bold text-white">銷貨退回</h2>
                        </div>
                    </div>
                    <div class="card-body p-2">
                        <div class="FrozenTable" style="max-height: 30vh; font-size: 0.9rem;">
                            <table class="table table-bordered sortable">
                                <thead>
                                    <tr>
                                        <th>序</th>
                                        <th>單號</th>
                                        <th>客戶簡稱</th>
                                        <th>日期</th>
                                        <th>數量</th>
                                        <th>單價</th>
                                        <th>小計</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if (!empty($sales_return))
                                        @foreach ($sales_return as $idx => $item)
                                            <tr>
                                                <td>{{ $idx + 1 }}</td>
                                                <td>{{ $item->sale_no }}</td>
                                                <td>{{ $item->cust_name }}</td>
                                                <td>{{ $item->date }}</td>
                                                <td>{{ number_format($item->qty) }}</td>
                                                <td>{{ number_format($item->unit, 2) }}</td>
                                                <td>{{ number_format($item->sales_total, 2) }}</td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-row my-2">
            <div class="col-6">
                <div class="card">
                    <div class="card-header bg-primary">
                        <div class="form-row align-items-center">
                            <h2 class="col-auto font-weight-bold text-white">POS銷貨</h2>
                        </div>
                    </div>
                    <div class="card-body p-2">
                        <div class="FrozenTable" style="max-height: 30vh; font-size: 0.9rem;">
                            <table class="table table-bordered sortable">
                                <thead>
                                    <tr>
                                        <th>序</th>
                                        <th>單號</th>
                                        <th>日期</th>
                                        <th>數量</th>
                                        <th>單價</th>
                                        <th>小計</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if (!empty($pos_sales))
                                        @foreach ($pos_sales as $idx => $item)
                                            <tr>
                                                <td>{{ $idx + 1 }}</td>
                                                <td>{{ $item->Main->order_no }}</td>
                                                <td>{{ substr($item->CREATE_Date, 0, 10) }}</td>
                                                <td>{{ number_format($item->Quantity) }}</td>
                                                <td>{{ number_format($item->SaleMoney, 2) }}</td>
                                                <td>{{ number_format($item->SaleMoneyTotal, 2) }}</td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-6">
                <div class="card">
                    <div class="card-header bg-primary">
                        <div class="form-row align-items-center">
                            <h2 class="col-auto font-weight-bold text-white">POS銷貨退回</h2>
                        </div>
                    </div>
                    <div class="card-body p-2">
                        <div class="FrozenTable" style="max-height: 30vh; font-size: 0.9rem;">
                            <table class="table table-bordered sortable">
                                <thead>
                                    <tr>
                                        <th>序</th>
                                        <th>單號</th>
                                        <th>日期</th>
                                        <th>數量</th>
                                        <th>單價</th>
                                        <th>小計</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if (!empty($pos_sales_return))
                                        @foreach ($pos_sales_return as $idx => $item)
                                            <tr>
                                                <td>{{ $idx + 1 }}</td>
                                                <td>{{ $item->RCOD3 }}</td>
                                                <td>{{ substr($item->CREATE_Date, 0, 10) }}</td>
                                                <td>{{ number_format($item->ReturnQuantity) }}</td>
                                                <td>{{ number_format($item->ReturnMoney, 2) }}</td>
                                                <td>{{ number_format($item->ReturnMoneyTotal, 2) }}</td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <datalist id="idList">
            @if (!empty($item_all))
                @foreach ($item_all as $item)
                    <option value="{{ $item->ICODE }}">{{ $item->INAME }}</option>
                @endforeach
            @endif
        </datalist>
    </div>
@stop
@section('script')
    <script type="text/javascript">
        $(document).ready(InitialDate());

        function InitialDate() {
            var today = new Date();
            var yearAgo = today.addDays(-365);
            if ($("#input_start_date").val() == '') {
                $("#input_start_date").val(DateToString(yearAgo));
            }
            if ($("#input_end_date").val() == '') {
                $("#input_end_date").val(DateToString(today));
            }
        }
        //毛利率篩選
        $('#input_gpm_min,#input_gpm_max').on('change', function() {
            var min = $('#input_gpm_min').val();
            var max = $('#input_gpm_max').val();
            $('#tbody_gpm>tr').each(function(i, e) {
                var gpm = $(e).data().gpm;
                if (min != '' && max != '') {
                    if (gpm >= min && gpm <= max) {
                        $(e).show();
                    } else {
                        $(e).hide();
                    }
                } else if (max != '') {
                    if (gpm <= max) {
                        $(e).show();
                    } else {
                        $(e).hide();
                    }
                } else if (min != '') {
                    if (gpm >= min) {
                        $(e).show();
                    } else {
                        $(e).hide();
                    }
                } else {
                    $(e).show();
                }
            });
        });
    </script>
@stop
