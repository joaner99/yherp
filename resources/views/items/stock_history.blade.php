@extends('layouts.base')
@section('title')
    商品庫存紀錄
@stop
@section('content')
    <div class="container-fluid">
        <form action="{{ route('items.stock_history') }}" method="GET" class="p-1">
            <div class="form-row">
                <div class="form-group col-lg-3 col-sm-6">
                    <label for="input_start_date">起日</label>
                    <input id="input_start_date" name="start_date" class="form-control" type="date"
                        value="{{ old('start_date') }}">
                </div>
                <div class="form-group col-lg-3 col-sm-6">
                    <label for="input_end_date">訖日</label>
                    <input id="input_end_date" name="end_date" class="form-control" type="date"
                        value="{{ old('end_date') }}">
                </div>
                <div class="form-group col-lg-3">
                    <label for="input_id">
                        商品料號
                    </label>
                    <input id="input_id" name="id" list="idList" class="form-control" style="height: auto;" type="text"
                        value="{{ old('id') }}" autocomplete="off" required>
                    <datalist id="idList">
                        @if (!empty($data['ItemList']))
                            @foreach ($data['ItemList'] as $item)
                                <option value="{{ $item->ICODE }}">{{ $item->INAME }}</option>
                            @endforeach
                        @endif
                    </datalist>
                </div>
            </div>
            <button class="btn btn-primary" type="submit">查詢</button>
        </form>
        <div class="mt-3 p-1">
            @if (empty(old('id')))
                <div class="alert alert-info">
                    請輸入日期、料號
                </div>
            @else
                <div id="chart"></div>
            @endif
        </div>
    </div>
@stop
@section('script')
    <script type="text/javascript">
        $(document).ready(InitialDate());

        function InitialDate() {
            var min = "2021-06-29"; //之前無資料
            var today = new Date();
            var oneMonthAgo = new Date();
            oneMonthAgo.setMonth(today.getMonth() - 1);

            $("#input_start_date,#input_end_date").attr("max", DateToString(today));
            $("#input_start_date,#input_end_date").attr("min", min);
            if ($("#input_start_date").val() == '') {
                var tmp = DateToString(oneMonthAgo);
                $("#input_start_date").val(tmp < min ? min : tmp);
            }
            if ($("#input_end_date").val() == '') {
                $("#input_end_date").val(DateToString(today));
            }
        }
        var chart = c3.generate({
            bindto: '#chart',
            padding: {
                left: 100,
                right: 100
            },
            zoom: {
                enabled: true
            },
            title: {
                text: '<?php echo $data['chart']['title']; ?>'
            },
            data: {
                x: 'x',
                columns: [
                    <?php
                    echo json_encode($data['chart']['x']);
                    echo ',';
                    echo json_encode($data['chart']['y']);
                    ?>
                ],
                labels: {
                    show: true,
                    format: d3.format(',')
                }
            },
            axis: {
                x: {
                    label: {
                        text: '日期',
                        position: 'outer-right'
                    },
                    tick: {
                        format: '%m-%d',
                        culling: true,
                        centered: true
                    },
                    type: 'timeseries',
                    padding: {
                        left: 1000 * 60 * 60 * 12,
                        right: 1000 * 60 * 60 * 12
                    }
                },
                y: {
                    label: {
                        text: '庫存量',
                        position: 'outer-top'
                    },
                    tick: {
                        format: d3.format(',')
                    }
                }
            },
            tooltip: {
                format: {
                    value: function(value, ratio, id) {
                        var format = d3.format(',');
                        return format(value);
                    }
                }
            }
        });
    </script>
@stop
