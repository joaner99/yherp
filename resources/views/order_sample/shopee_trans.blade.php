@extends('layouts.base')
@section('title')
    蝦皮宅配出貨轉檔
@stop
@section('css')
    <style type="text/css">
        #table_data>thead>tr>th:nth-child(2),
        #table_data>thead>tr>th:nth-child(3),
        #table_data>thead>tr>th:nth-child(4),
        #table_data>thead>tr>th:nth-child(5),
        #table_data>thead>tr>th:nth-child(6),
        #table_data>thead>tr>th:nth-child(8) {
            width: 10%;
        }

        input[type="checkbox"] {
            transform: scale(2);
        }
    </style>
@endsection
@section('content')
    <div class="container-fluid">
        @if (empty($data))
            <div class="form-row">
                <div class="col-6">
                    <form action="{{ route('order_sample.shopee_trans_convert') }}" method="post"
                        enctype="multipart/form-data">
                        @csrf
                        <div class="input-group mb-3">
                            <div class="custom-file">
                                <input class="custom-file-input" id="input_transport_file" name="file" type="file"
                                    accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,.xls"required>
                                <label class="custom-file-label" for="input_transport_file">請選擇蝦皮批次出貨檔案</label>
                            </div>
                            <div class="input-group-append">
                                <button type="submit" class="btn btn-primary">上傳</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        @else
            <div class="form-row">
                <div class="col-auto">
                    <form action="{{ route('order_sample.shopee_trans_export') }}" method="post">
                        @csrf
                        <input name="export_data" type="hidden">
                        <button id="btn_export" class="btn btn-success" type="submit">
                            <i class="bi bi-file-earmark-excel"></i>匯出Excel
                        </button>
                    </form>
                </div>
                <div class="col-auto">
                    <a href="{{ route('order_sample.shopee_trans') }}" class="btn btn-secondary">
                        <i class="bi bi-arrow-clockwise"></i>重新上傳
                    </a>
                </div>
                <div class="col d-flex align-items-center">
                    <span class="text-danger">※有訂單資料、銷貨資料、上車紀錄情況下，才會填入託運單號，排除未在籃子訂單</span>
                </div>
            </div>
            <div class="form-row mt-2">
                <div class="col">
                    <div class="FrozenTable" style="max-height: 87vh; font-size: 0.95rem;">
                        <table id="table_data" class="table table-bordered table-hover sortable table-filter">
                            <thead>
                                <tr>
                                    <th style="width: 4%;" class="sort-none export-none">
                                        <input id="chk_all" type="checkbox">
                                    </th>
                                    @foreach ($data[0] as $col)
                                        <th>{{ $col }}</th>
                                    @endforeach
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($data as $idx => $items)
                                    <?php $checked = !empty($items[2]); ?>
                                    @if ($idx == 0)
                                        @continue
                                    @endif
                                    <tr class="{{ $checked ? '' : 'table-danger' }}">
                                        <td class="export-none">
                                            <input class="confirm" type="checkbox" {{ $checked ? 'checked' : '' }}>
                                        </td>
                                        @foreach ($items as $item)
                                            <td>{{ $item }}</td>
                                        @endforeach
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        @endif
    </div>
    </div>
@stop
@section('script')
    <script type="text/javascript">
        //上傳檔案後顯示文件名稱
        $('#input_transport_file').change(function(e) {
            var fileName = '';
            $(e.target.files).each(function(index, element) {
                if (fileName != '') {
                    fileName += ',';
                }
                fileName += element.name;
            });
            $(this).next('.custom-file-label').html(fileName);
        });
        //全選功能
        $('#chk_all').change(check_all);
        $('#btn_export').click(function() {
            var has_empty = false;
            var data = new Array();
            var cols = new Array();
            $('#table_data>thead>tr>th').each(function(i, e) {
                if (!$(e).hasClass('export-none')) {
                    cols.push($(e).text().trim());
                }
            });
            data.push(cols);
            $('#table_data>tbody .confirm:checked').each(function(i, e) {
                var rows = new Array();
                var tr = $(e).closest('tr');
                $(tr).find('td').each(function(i, e) {
                    if (!$(e).hasClass('export-none')) {
                        var text = $(e).text().trim();
                        if (i == 3 && IsNullOrEmpty(text)) {
                            has_empty = true;
                        }
                        rows.push(text);
                    }
                });
                data.push(rows);
            });
            $('input[name="export_data"]').val(JSON.stringify(data));
            if (has_empty && !confirm("內含無物流訂單，確定匯出??")) {
                return false;
            }
        });
    </script>
@stop
