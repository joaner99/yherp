@extends('layouts.base')
@section('title')
    特殊訂單轉檔下載頁面
@stop
@section('css')
    <style type="text/css">
        .btn>i {
            margin: 0;
        }

        table>tbody>tr {
            height: 50px;
        }

        .details {
            margin: 0;
        }

        .details>tbody>tr>td:nth-child(1) {
            width: 10%;
        }

        .details>tbody>tr>td:nth-child(2) {
            text-align: left;
        }

        .details>tbody>tr>td:nth-child(3) {
            width: 5%;
        }

        /*新竹偏遠地去*/
        .hct_remote {
            font-size: 2.5rem;
            font-weight: bolder;
            color: red;
        }
    </style>
@endsection
@section('content')
    <div class="container-fluid">
        <form method="POST" action="{{ route('order_sample.order_export') }}" enctype="multipart/form-data">
            @csrf
            <div class="form-row">
                <div class="form-group col-auto">
                    <label for="input_start_date">起日</label>
                    <input id="input_start_date" name="start_date" class="form-control" type="date" required>
                </div>
                <div class="form-group col-auto">
                    <label for="input_end_date">訖日</label>
                    <input id="input_end_date" name="end_date" class="form-control" type="date" required>
                </div>
                <div class="form-group col-auto">
                    <label>平台來源</label>
                    <select id="select_src" name="src" class="form-control">
                        @if (!empty($srcTypeList) && count($srcTypeList) > 0)
                            @foreach ($srcTypeList as $value => $text)
                                @if ($value == '1')
                                    @continue
                                @endif
                                <option value="{{ $value }}" {{ $value == 5 ? 'selected' : '' }}>{{ $text }}
                                </option>
                            @endforeach
                        @endif
                    </select>
                </div>
                <div class="form-group col-auto">
                    <label>匯出類型</label>
                    <select id="select_type" name="type" class="form-control">
                        @if (!empty($exportTypeList) && count($exportTypeList) > 0)
                            @foreach ($exportTypeList as $value => $text)
                                <option value="{{ $value }}" {{ $value == 3 ? 'selected' : '' }}>{{ $text }}
                                </option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
            <button class="btn btn-success" type="submit">
                <i class="bi bi-file-earmark-excel"></i>匯出
            </button>
        </form>
        <div class="form-row mt-3">
            <div class="col">
                <div class="FrozenTable" style="max-height: 70vh;">
                    <table class="table table-bordered table-hover sortable">
                        <caption>最近10筆紀錄</caption>
                        <thead>
                            <tr>
                                <th>流水號</th>
                                <th>起日</th>
                                <th>訖日</th>
                                <th>平台來源</th>
                                <th>匯出類型</th>
                                <th>匯出資料</th>
                                <th>匯出時間</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (!empty($data))
                                @foreach ($data as $item)
                                    <tr class="{{ $item->color }}">
                                        <td>{{ $item->id }}</td>
                                        <td>{{ $item->start_date }}</td>
                                        <td>{{ $item->end_date }}</td>
                                        <td>{{ $item->src_name }}</td>
                                        <td>{{ $item->convert_type_name }}</td>
                                        <td>
                                            @if (empty($item->convert_file) || $item->convert_file == '[]')
                                                無資料
                                            @else
                                                <form action="{{ route('export_table') }}" method="post">
                                                    @csrf
                                                    <input name="export_name" type="hidden"
                                                        value="{{ $item->convert_file_name }}">
                                                    <input name="export_data" type="hidden"
                                                        value="{{ $item->convert_file }}">
                                                    <button class="btn btn-success btn-sm" type="submit">
                                                        <i class="bi bi-download"></i>
                                                    </button>
                                                </form>
                                            @endif
                                        </td>
                                        <td>{{ $item->created_at }}</td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop
@section('script')
    <script type="text/javascript">
        $(document).ready(InitialDate());

        function InitialDate() {
            var today = new Date();

            if ($("#input_start_date").val() == '') {
                $("#input_start_date").val(DateToString(today.addDays(-10)));
            }
            if ($("#input_end_date").val() == '') {
                $("#input_end_date").val(DateToString(today));
            }
        }
    </script>
@stop
