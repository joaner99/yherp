@extends('layouts.base')
@section('title')
    特殊訂單轉檔
@stop
@section('css')
    <style type="text/css">
        .item_total {
            font-size: 1.5rem;
            font-weight: bolder;
        }

        td[data-order_type_name="已確認"],
        li[data-order_type_name="已確認"] {
            color: #fff;
            background-color: #28a745;
        }

        td[data-order_type_name="未確認"],
        li[data-order_type_name="未確認"] {
            color: #fff;
            background-color: #17a2b8;
        }

        /*商品明細-料號*/
        table.inner-table>thead>tr>th:nth-child(1),
        table.inner-table>tbody>tr>td:nth-child(1) {
            width: 110px;
        }

        /*商品明細-數量*/
        table.inner-table>thead>tr>th:nth-child(3),
        table.inner-table>tbody>tr>td:nth-child(3) {
            width: 50px;
        }

        /*新竹偏遠地去*/
        .hct_remote {
            font-size: 2.5rem;
            font-weight: bolder;
            color: red;
        }

        .order_font {
            font-family: monospace !important;
        }

        /*超賣*/
        .out_of_stock {
            color: red;
            font-weight: bolder;
        }
    </style>
@endsection
@section('content')
    {{-- Modal 舊版本訂單 --}}
    <div id="modal_old" class="modal">
        <div class="modal-dialog modal-lg modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">舊版本訂單</h5>
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    @if (count($old_ver_order) > 0)
                        <div class="form-row">
                            <div class="col">
                                <div class="FrozenTable" style="max-height: 70vh;">
                                    <table class="table table-bordered table-hover sortable">
                                        <caption>版本不同，請重新上蝦皮匯入該訂單</caption>
                                        <thead>
                                            <tr>
                                                <th>原始訂單編號</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($old_ver_order as $order)
                                                <tr>
                                                    <td>{{ $order }}</td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">關閉</button>
                </div>
            </div>
        </div>
    </div>
    {{-- Modal 上傳 --}}
    <div id="modal_upload" class="modal">
        <div class="modal-dialog modal-xl modal-dialog-centered">
            <div class="modal-content">
                <form action="{{ route('order_sample.import') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title">匯入訂單</h5>
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">來源</span>
                            </div>
                            <select class="custom-select col-2" id="input_type" name="type">
                                @if (!empty($srcTypeList) && count($srcTypeList) > 0)
                                    @foreach ($srcTypeList as $value => $text)
                                        @if ($value == '1')
                                            @continue
                                        @endif
                                        <option value="{{ $value }}">{{ $text }}</option>
                                    @endforeach
                                @endif
                            </select>
                            <div class="custom-file">
                                <input class="custom-file-input" id="input_file" name="file[]" type="file"
                                    accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,.xls" multiple
                                    required>
                                <label class="custom-file-label" for="input_file">請選擇檔案</label>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button id="btn_import" type="submit" class="btn btn-primary">確認</button>
                        <button id="btn_loading" type="button" class="btn btn-primary" style="display: none;" disabled>
                            <div class="spinner-border spinner-border-sm">
                                <span class="sr-only"></span>
                            </div>
                            <span>匯入中...</span>
                        </button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">關閉</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{-- Modal 編輯訂單 --}}
    <div id="modal_edit" class="modal fade">
        <div class="modal-dialog modal-dialog-centered" style="max-width: 100%; width: 99%;">
            <div class="modal-content">
                <form action="{{ route('order_sample.update') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-header bg-warning">
                        <div class="modal-title">
                            <h1>編輯訂單</h1>
                        </div>
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" style="font-size: 0.95rem;">
                        <div class="container-fluid">
                            <div class="form-row">
                                <div class="form-group col-lg-2">
                                    <label>訂單成立日期</label>
                                    <input class="form-control" type="text" name="order_date" readonly>
                                </div>
                                <div class="form-group col-lg-2">
                                    <label>原始訂單編號</label>
                                    <input class="form-control" type="text" name="order_no" readonly>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-lg-2">
                                    <label>收件者姓名</label>
                                    <input class="form-control" type="text" name="receiver_name" readonly>
                                </div>
                                <div class="form-group col-lg-2">
                                    <label>收件者電話</label>
                                    <input class="form-control" type="text" name="receiver_tel" readonly>
                                </div>
                                <div class="form-group col-lg-2">
                                    <label>城市</label>
                                    <input class="form-control" type="text" name="receiver_city" readonly>
                                </div>
                                <div class="form-group col-lg-2">
                                    <label>行政區</label>
                                    <input class="form-control" type="text" name="receiver_district" readonly>
                                </div>
                                <div class="form-group col-lg-1">
                                    <label>郵遞區號</label>
                                    <input class="form-control" type="text" name="receiver_postal" readonly>
                                </div>
                                <div class="form-group col-lg">
                                    <label>收件地址</label>
                                    <input class="form-control" type="text" name="receiver_address" readonly>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-lg-3">
                                    <label>買家備註</label>
                                    <textarea class="form-control" name="customer_remark" style="height: 100px;" readonly></textarea>
                                </div>
                                <div class="form-group col-lg-3">
                                    <label>賣家備註</label>
                                    <textarea class="form-control" name="seller_remark" style="height: 100px;" readonly></textarea>
                                </div>
                                <div class="form-group col-lg-6">
                                    <label>撿貨單備註</label>
                                    <label class="text-success ml-2">※200字以內</label>
                                    <textarea class="form-control" name="remark" style="height: 100px;"></textarea>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col">
                                    <div class="FrozenTable" style="max-height: 340px;">
                                        <table class="table table-bordered sortable">
                                            <caption>商品明細</caption>
                                            <thead>
                                                <tr>
                                                    <th style="width: 13%;">料號</th>
                                                    <th>名稱</th>
                                                    <th style="width: 10%;">售價</th>
                                                    <th style="width: 7%;">數量</th>
                                                </tr>
                                            </thead>
                                            <tbody id="tbody_items"></tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row mt-2">
                                <div class="col"></div>
                                <div class="col-auto item_total">
                                    <div>
                                        <span>原訂單商品總價:</span>
                                        <span id="span_item_total" class="p-1"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button id="btn_edit" type="submit" class="btn btn-primary">確認</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">取消</button>
                    </div>
                    <input type="hidden" name="id">
                    <input type="hidden" name="genuine_items">
                    <datalist id="items">
                        @if (!empty($items))
                            @foreach ($items as $item)
                                <option value="{{ $item->ICODE }}" data-price="{{ number_format($item->IUNIT) }}">
                                    {{ $item->INAME }}
                                </option>
                            @endforeach
                        @endif
                    </datalist>
                </form>
            </div>
        </div>
    </div>
    {{-- Modal 已完成訂單重做 --}}
    <div id="modal_redo" class="modal">
        <div class="modal-dialog modal modal-dialog-centered">
            <div class="modal-content">
                <form action="{{ route('order_sample.redo') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title">已完成訂單重做</h5>
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-row">
                            <div class="form-group col">
                                <label>原始訂單編號</label>
                                <input name="order_no" class="form-control" type="text" required>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">確認</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">關閉</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="form-row">
            <div class="col-auto">
                <button class="btn btn-success btn-sm" type="button" data-toggle="modal" data-target="#modal_upload">
                    匯入訂單
                </button>
            </div>
            <div class="col-auto">
                <form action="{{ route('order_sample.confirm') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <button id="btn_confirm" class="btn btn-primary btn-sm" type="submit">
                        <i class="bi bi-check2-square"></i>業務確認訂單
                    </button>
                    <input name="id_list" type="hidden">
                </form>
            </div>
            <div class="col-auto">
                <form action="{{ route('order_sample.cancel_confirm') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <button id="btn_cancel_confirm" class="btn btn-danger btn-sm" type="submit">
                        <i class="bi bi-check2-square"></i>業務取消確認訂單
                    </button>
                    <input name="id_list" type="hidden">
                </form>
            </div>
            <div class="col-auto">
                <button class="btn btn-info btn-sm" type="button" data-toggle="modal" data-target="#modal_redo">
                    <i class="bi bi-arrow-clockwise"></i>已完成訂單重做
                </button>
            </div>
            <div class="col-auto">
                <form action="{{ $config_url }}" method="GET">
                    <input name="redirect" value="{{ Route::currentRouteName() }}" type="hidden">
                    <button class="btn btn-secondary btn-sm" type="submit">
                        <i class="bi bi-gear-fill"></i>需確認品項設定
                    </button>
                </form>
            </div>
            @if (count($old_ver_order) > 0)
                <div class="col-auto">
                    <button class="btn btn-danger btn-sm" type="button" data-toggle="modal" data-target="#modal_old">
                        <i class="bi bi-exclamation-lg"></i>舊版訂單
                    </button>
                </div>
            @endif
            <div class="col-auto">
                <div class="form-group form-check">
                    <form action="{{ route('order_sample.convert') }}" method="GET">
                        <input class="form-check-input" id="input_include_imported" name="include_imported"
                            type="checkbox" {{ old('include_imported') == 1 ? 'checked' : '' }}
                            onChange="this.form.submit()" value="1">
                        <label class="form-check-label" for="input_include_imported">顯示已匯入訂單</label>
                    </form>
                </div>
            </div>
            <div class="col"></div>
            <div class="col-auto">
                <a class="btn btn-warning btn-sm" href="{{ asset('storage') . '/help/order_floor.pdf' }}"
                    target="_blank">
                    <i class="bi bi-info-circle"></i>說明文件
                </a>
            </div>
        </div>
        <div class="form-row mb-2">
            @if (!empty($data))
                <div class="col-auto">
                    <div class="card">
                        <div class="card-header bg-secondary text-white">
                            訂單狀態統計：共{{ collect($statistics)->sum() }}單
                        </div>
                        <div class="card-body p-2">
                            <ul class="list-group list-group-horizontal">
                                @foreach ($statistics as $key => $qty)
                                    <li class="list-group-item p-2" data-order_type_name="{{ $key }}">
                                        <span>{{ $key }}</span>：<span>{{ $qty }}單</span>
                                    </li>
                                @endforeach
                                <li class="list-group-item bg-danger text-white p-2">
                                    <span>快過期(≥2天)</span>：<span>{{ $expired_count }}單</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            @endif
        </div>
        @if (empty($data))
            <div class="alert alert-danger">
                無資料
            </div>
        @else
            <div class="form-row">
                <div class="col">
                    <div class="FrozenTable" style="max-height: 75vh; font-size: 0.75rem;">
                        <table id="table_data" class="table table-bordered table-hover sortable table-filter">
                            @if (old('include_imported') == 1)
                                <caption>已包含匯入的訂單</caption>
                            @endif
                            <thead>
                                <tr>
                                    <th style="width: 2%;" class="sort-none">
                                        <input id="chk_all" type="checkbox">
                                    </th>
                                    <th style="width: 6%;">操作</th>
                                    <th class="filter-col" style="width: 4%;">平台</th>
                                    <th style="width: 6%;">訂單成立日期</th>
                                    <th style="width: 7%;">原始訂單編號</th>
                                    <th class="filter-col" style="width: 5%;">訂單狀態</th>
                                    <th class="filter-col" style="width: 5%;">訂單類型</th>
                                    <th class="filter-col" style="width: 6%;">預排物流</th>
                                    <th class="p-0">
                                        <table class="inner-table">
                                            <thead>
                                                <tr>
                                                    <th>料號</th>
                                                    <th>品名</th>
                                                    <th>數量</th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </th>
                                    <th style="width: 4%;">新竹<br>偏遠</th>
                                    <th style="width: 10%;">撿貨單備註</th>
                                    <th style="width: 10%;">賣家備註</th>
                                    <th style="width: 10%;">買家備註</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach (collect($data)->groupBy('order_no') as $group)
                                    <?php
                                    $first_item = $group->first();
                                    $expired = $first_item['expired'];
                                    ?>
                                    <tr class="{{ $expired ? 'table-danger' : '' }}" data-id="{{ $first_item['id'] }}"
                                        data-order_date="{{ $first_item['order_date'] }}"
                                        data-order_no="{{ $first_item['order_no'] }}"
                                        data-item_total="{{ number_format($first_item['item_total']) }}"
                                        data-receiver_name="{{ $first_item['receiver_name'] }}"
                                        data-receiver_tel="{{ $first_item['receiver_tel'] }}"
                                        data-receiver_city="{{ $first_item['receiver_city'] }}"
                                        data-receiver_district="{{ $first_item['receiver_district'] }}"
                                        data-receiver_postal="{{ $first_item['receiver_postal'] }}"
                                        data-receiver_address="{{ $first_item['receiver_address'] }}"
                                        data-remark="{{ $first_item['remark'] }}"
                                        data-seller_remark="{{ $first_item['seller_remark'] }}"
                                        data-customer_remark="{{ $first_item['customer_remark'] }}"
                                        data-genuine_item="{{ $first_item['genuine_item'] }}">
                                        <td>
                                            @if ($first_item['order_type'] == 1)
                                                <input class="confirm" type="checkbox">
                                            @endif
                                        </td>
                                        <td>
                                            <div>
                                                <button class="btn btn-warning btn-sm" type="button" data-toggle="modal"
                                                    data-target="#modal_edit">
                                                    <i class="bi bi-pencil-fill"></i>編輯
                                                </button>
                                            </div>
                                            <div class="mt-2">
                                                <form action="{{ route('order_sample.delete') }}" method="post"
                                                    onsubmit="return confirm('確定要刪除訂單【 {{ $first_item['order_no'] }}】?');"
                                                    enctype="multipart/form-data">
                                                    @csrf
                                                    <input type="hidden" name="id"
                                                        value="{{ $first_item['id'] }}">
                                                    <button class="btn btn-danger btn-sm" type="submit">
                                                        <i class="bi bi-trash"></i>刪除
                                                    </button>
                                                </form>
                                            </div>
                                        </td>
                                        <td>{{ $first_item['src_name'] }}</td>
                                        <td>{{ $first_item['order_date'] }}</td>
                                        <td class="order_font">{{ $first_item['order_no'] }}</td>
                                        <td data-order_type_name="{{ $first_item['order_type_name'] }}">
                                            {{ $first_item['order_type_name'] }}
                                        </td>
                                        <td>
                                            <select class="form-control form-control-sm" name="order_kind">
                                                @if (!empty($kindList))
                                                    @foreach ($kindList as $val => $text)
                                                        <option
                                                            value="{{ $val }}"{{ $first_item['order_kind'] == $val ? 'selected' : '' }}>
                                                            {{ $text }}
                                                        </option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </td>
                                        <td>
                                            @if ($first_item['trans'] != -1)
                                                <select class="form-control form-control-sm" name="trans">
                                                    @if (!empty($transportTypeList))
                                                        @foreach ($transportTypeList as $value => $text)
                                                            <option
                                                                value="{{ $value }}"{{ $first_item['trans'] == $value ? 'selected' : '' }}>
                                                                {{ $text }}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                            @endif
                                        </td>
                                        <td class="p-0">
                                            <table class="inner-table">
                                                <tbody>
                                                    @foreach ($group as $item)
                                                        <tr class="{{ $item['out_of_stock'] ? 'out_of_stock' : '' }}">
                                                            <td>{{ $item['item_no'] }}</td>
                                                            <td class="text-left">{{ $item['item_name'] }}</td>
                                                            <td>
                                                                {{ $item['item_quantity'] }}
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </td>
                                        <td>
                                            @if ($first_item['hct_remote'])
                                                <span class="hct_remote">✔</span>
                                            @endif
                                        </td>
                                        <td>{{ $first_item['remark'] }}</td>
                                        <td>{{ $first_item['seller_remark'] }}</td>
                                        <td>{{ $first_item['customer_remark'] }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        @endif
    </div>
@stop
@section('script')
    <script type="text/javascript">
        HideSidebar();
        //上傳檔案後顯示文件名稱
        $('#input_file,#input_transport_file').change(function(e) {
            var fileName = '';
            $(e.target.files).each(function(index, element) {
                if (fileName != '') {
                    fileName += ',';
                }
                fileName += element.name;
            });
            $(this).next('.custom-file-label').html(fileName);
        });
        //上傳後關閉畫面
        $('#btn_convert_transport').click(function() {
            $('#modal_upload_transport').modal('hide')
        });

        //匯入
        $("#btn_import").click(function() {
            $("#btn_import").hide();
            $("#btn_loading").show();
            lockScreen();
        });

        //全選功能
        $('#chk_all').change(check_all);

        //業務確認訂單
        $("#btn_confirm,#btn_cancel_confirm").click(function() {
            var idList = new Array();
            $("#table_data>tbody>tr .confirm:checked").each(function(index, element) {
                idList.push($(element).closest('tr').data().id);
            });
            if (idList.length > 0) {
                $('input[name="id_list"]').val(JSON.stringify(idList));
            } else {
                alert('請勾選訂單');
                return false;
            }
        });

        //編輯訂單
        $('#modal_edit').on('show.bs.modal', function(event) {
            var modal_id = $(this)[0].id;
            var button = $(event.relatedTarget);
            var data = button.closest("tr").data();
            var modal_content = $(this).find(".modal-content");
            modal_content.find("select[name='trans']").attr('disabled', data.trans === -1);
            modal_content.find("input[name='id']").val(data.id);
            modal_content.find("input[name='order_date']").val(data.order_date);
            modal_content.find("input[name='order_no']").val(data.order_no);
            modal_content.find("input[name='receiver_name']").val(data.receiver_name);
            modal_content.find("input[name='receiver_tel']").val(data.receiver_tel);
            modal_content.find("input[name='receiver_city']").val(data.receiver_city);
            modal_content.find("input[name='receiver_district']").val(data.receiver_district);
            modal_content.find("input[name='receiver_postal']").val(data.receiver_postal);
            modal_content.find("input[name='receiver_address']").val(data.receiver_address);
            modal_content.find("textarea[name='remark']").val(data.remark);
            modal_content.find("textarea[name='customer_remark']").val(data.customer_remark);
            modal_content.find("textarea[name='seller_remark']").val(data.seller_remark);
        });
        //編輯時，建立正貨商品明細DOM
        $('button[data-target="#modal_edit"]').click(function() {
            var data = $(this).closest("tr").data();
            $("#span_item_total").text(data.item_total);
            var genuine_item = data.genuine_item;
            if (genuine_item.length > 0) {
                var dom = '';
                var notFoundItems = new Array();
                $(genuine_item).each(function(index, element) {
                    var item_no = element.item_no;
                    var item_price = element.item_price;
                    var item_quantity = element.item_quantity;
                    var item = $("#items").find('option[value="' + item_no + '"]');
                    if (item.length == 0) {
                        notFoundItems.push(item_no);
                    } else {
                        var item_name = $(item).text();
                        dom += CreateItemDOM(item_no, item_name, item_price, item_quantity);
                    }
                });
                $("#tbody_items").html(dom);
            }
        });

        //建立商品的Table row DOM
        function CreateItemDOM(item_no = '', item_name = '', item_price = '', item_quantity = 1) {
            var dom = '';
            dom += '<tr>';
            //商品料號
            dom += '<td>' + item_no + '</td>';
            //商品名稱
            dom += '<td>' + item_name + '</td>';
            //商品售價
            dom += '<td>' + item_price + '</td>';
            //商品數量
            dom += '<td>' + item_quantity + '</td>';
            dom += '</tr>';
            return dom;
        }
        //訂單類型
        $('[name="order_kind"]').on('change', function() {
            var id = $(this).closest('tr').data().id;
            var order_kind = $(this).val();
            data = {
                id: id,
                order_kind: order_kind,
            };
            ajax_update(data);
        });
        //預排物流
        $('[name="trans"]').on('change', function() {
            var id = $(this).closest('tr').data().id;
            var trans = $(this).val();
            data = {
                id: id,
                trans: trans,
            };
            ajax_update(data);
        });
        //ajax更新訂單類型
        var url = '{{ route('order_sample.ajax_update_order') }}';

        function ajax_update(data) {
            lockScreen();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: url,
                type: "post",
                data: data,
                success: function(data) {},
                error: function(xhr, ajaxOptions, thrownError) {
                    var msg = JSON.parse(xhr.responseText).msg;
                    alert(msg);
                },
                complete: function() {
                    unlockScreen();
                }
            });
        }
    </script>
@stop
