@extends('layouts.base')
@section('css')
    <style type="text/css">
    </style>
@endsection
@section('title')
    商品安庫表
@stop
@section('content')
    <div class="container-fluid">
        <div class="form-row">
            <div class="col-auto">
                <nav>
                    <ul class="pagination m-0">
                        <li class="page-item active" data-target="#div_china">
                            <button class="page-link" type="button">
                                中國商品
                            </button>
                        </li>
                        <li class="page-item" data-target="#div_unchina">
                            <button class="page-link" type="button">
                                非中國商品
                            </button>
                        </li>
                    </ul>
                </nav>
            </div>
            <div class="col"></div>
            <div class="col-auto">
                <form action="{{ route('stock.china_safety_stock_export') }}" method="post">
                    @csrf
                    <input name="items" type="hidden" value="">
                    <button id="btn_export" class="btn btn-success" type="submit">
                        <i class="bi bi-box-arrow-up"></i>匯出中國商品
                    </button>
                </form>
            </div>
            <div class="col-auto">
                @can('item_config')
                    <a class="btn btn-secondary" href="{{ route('items.config') }}" target="_blank">
                        <i class="bi bi-boxes"></i>商品安庫量設定
                    </a>
                @endcan
            </div>
        </div>
        @foreach (['中國商品' => $china_data, '非中國商品' => $unchina_data] as $title => $data)
            <div id="div_{{ $title == '中國商品' ? 'china' : 'unchina' }}" class="form-row mt-2"
                style="{{ $title == '中國商品' ? '' : 'display: none;' }}">
                <div class="col">
                    <div class="FrozenTable" style="max-height: 87vh; font-size: 0.9rem;">
                        <table id="{{ $title == '中國商品' ? 'table_china_data' : '' }}"
                            class="table table-bordered sortable table-filter">
                            <caption>{{ $title }}</caption>
                            <colgroup>
                                <col span="4">
                                <col span="1" class="table-info">
                                <col span="3" class="table-warning">
                                <col span="1" class="table-danger">
                            </colgroup>
                            <thead>
                                <tr>
                                    <th style="width: 3%;" class="sort-none"><input id="chk_all" type="checkbox"></th>
                                    <th style="width: 4%;">序</th>
                                    <th style="width: 8%;">料號</th>
                                    <th>品名</th>
                                    <th>訂單需求</th>
                                    <th style="width: 8%;">主倉庫存</th>
                                    <th style="width: 8%;">出貨庫存</th>
                                    <th style="width: 8%;">總庫存(主+出)</th>
                                    <th style="width: 8%;">安全庫存</th>
                                    <th class="filter-col" style="width: 10%;">安庫狀態</th>
                                    <th style="width: 8%;">最後匯出時間</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (count($data) > 0)
                                    <?php $index = 1; ?>
                                    @foreach ($data as $d)
                                        <tr>
                                            <td><input class="confirm" type="checkbox" value="{{ $d['item_no'] }}"></td>
                                            <td>{{ $index++ }}</td>
                                            <td>{{ $d['item_no'] }}</td>
                                            <td class="text-left">{{ $d['item_name'] }}</td>
                                            <td>{{ $d['order_require'] }}</td>
                                            <td>{{ $d['m_stock'] }}</td>
                                            <td>{{ $d['s_stock'] }}</td>
                                            <td>{{ $d['stock'] }}</td>
                                            <td>{{ $d['safety'] }}</td>
                                            <td>{{ $d['status'] }}</td>
                                            <td>{{ $d['exported_at'] }}</td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@stop
@section('script')
    <script type="text/javascript">
        //全選功能
        $('#chk_all').change(check_all);
        //匯出
        $('#btn_export').click(function() {
            var items = new Array();
            $('#table_china_data .confirm:checked').each(function(i, e) {
                items.push($(e).val());
            });
            $('input[name="items"]').val(JSON.stringify(items));
        });
    </script>
@stop
