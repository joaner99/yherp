@extends('layouts.base')
@section('css')
    <style type="text/css">
        .header>div {
            display: flex;
        }

        @media (max-width: 1279px) {
            input[type="checkbox"] {
                transform: scale(1);
            }

            .form-check-label {
                margin-left: 1rem;
            }

            .form-check-list>.form-check {
                margin: 2rem 0;
            }
        }

        .header>div>div {
            padding: 0.75rem 1.5rem;
            border: 1px solid #ced4da;
        }

        .header>div>div:first-child {
            border-top-left-radius: 5px;
            border-bottom-left-radius: 5px;
        }

        .header>div>div:last-child {
            border-top-right-radius: 5px;
            border-bottom-right-radius: 5px;
            flex-grow: 1;
        }

        .item_qty {
            text-align: center;
        }

        #select_type,
        #select_place {
            font-size: 1.25rem;
        }
    </style>
@endsection
@section('title')
    倉庫異動作業
@stop
@section('content')
    <div class="container-fluid" style="font-size: 2rem;">
        <div class="form-row header">
            <div class="col-xl-auto mb-2">
                <a class="btn btn-danger" href="{{ url()->previous() }}" style="font-size: 2rem;">
                    <span class="align-middle">回上頁</span>
                </a>
            </div>
            <div class="col-xl-auto mb-2">
                <div class="bg-secondary text-white">倉庫</div>
                <div>{{ $place->Stock->id }}</div>
                <div>{{ $place->Stock->name }}</div>
            </div>
            <div class="col-xl-auto mb-2">
                <div class="bg-secondary text-white">儲位</div>
                <div>{{ $place->id }}</div>
                <div>{{ $place->name }}</div>
            </div>
        </div>
        <div id="div_step_1" class="form-row">
            <div class="col">
                <div class="card">
                    <div class="card-header bg-primary text-white" data-toggle="collapse" data-target="#div_step_body">
                        <i class="bi bi-1-circle mr-2"></i>作業類型
                    </div>
                    <div id="div_step_body" class="collapse show">
                        <div class="card-body p-2">
                            <select class="custom-select mb-2" id="select_type" name="type" required>
                                @if ($is_transfer)
                                    <option value="98" selected>調撥作業</option>
                                @else
                                    <option value="" selected>請選擇...</option>
                                @endif
                                @if (count($place->Details) > 0 && count($all_place) > 0)
                                    <option value="99">移轉作業</option>
                                @endif
                                @foreach ($log_type as $key => $txt)
                                    <option value="{{ $key }}">{{ $txt }}</option>
                                @endforeach
                            </select>
                            <div id="div_place" class="input-group mb-2" style="display: none;">
                                <div class="input-group-prepend">
                                    <label class="input-group-text" for="select_place">目的儲位</label>
                                </div>
                                <select class="custom-select" id="select_place">
                                    <option value="" selected>請選擇...</option>
                                    @foreach ($all_place as $p)
                                        <option value="{{ $p->id }}">{{ $p->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <button id="btn_to_step2" class="btn btn-info btn-lg w-100" type="button">確認，下一步</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="div_step_2" class="form-row mt-2">
            <div class="col">
                <div class="card">
                    <div class="card-header bg-warning text-white" data-toggle="collapse" data-target="#div_step_2_body">
                        <i class="bi bi-2-circle mr-2"></i>選擇商品
                    </div>
                    <div id="div_step_2_body" class="collapse">
                        <div class="card-body p-2">
                            <div id="div_item_list" class="form-check-list flex-wrap"
                                style="font-size: 1rem; max-height: 50vh;">
                                @foreach ($place->Details as $item)
                                    <div class="form-check">
                                        <input class="form-check-input" id="input_item_{{ $item->item_no }}"
                                            data-item_name="{{ $item->item_name }}" data-item_stock="{{ $item->qty }}"
                                            type="checkbox" value="{{ $item->item_no }}">
                                        <label class="form-check-label" for="input_item_{{ $item->item_no }}">
                                            {{ "【{$item->item_no}】" }}<br>{{ "{$item->item_name}" }}
                                        </label>
                                    </div>
                                @endforeach
                            </div>
                            <div id="div_item_create" style="display: none;">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            料號
                                        </span>
                                    </div>
                                    <input id="input_new_item_1" class="form-control form-control-lg" style="height: auto;"
                                        type="text" autocomplete="off" autofocus>
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            清單
                                        </span>
                                    </div>
                                    <input id="input_new_item_2" type="text" list="idList"
                                        class="form-control form-control-lg" style="height: auto;" autocomplete="off">
                                    <datalist id="idList">
                                        @if (!empty($item_all))
                                            @foreach ($item_all as $item)
                                                <option value="{{ $item->ICODE }}">{{ $item->INAME }}</option>
                                            @endforeach
                                        @endif
                                    </datalist>
                                    <div class="input-group-append">
                                        <button id="btn_add_item" class="btn btn-primary" type="button">加入</button>
                                    </div>
                                </div>
                                <div class="FrozenTable" style="font-size: 1rem;">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>料號</th>
                                                <th>品名</th>
                                                <th>操作</th>
                                            </tr>
                                        </thead>
                                        <tbody id="tbody_create_items">
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="form-row justify-content-around mt-2">
                                <button id="btn_to_step3" class="btn btn-info btn-lg col-4" type="button">確認，下一步</button>
                                <button id="btn_cancel_to_step1" class="btn btn-danger btn-lg col-4"
                                    type="button">取消，上一步</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="div_step_3" class="form-row mt-2">
            <div class="col">
                <div class="card">
                    <div class="card-header bg-success text-white" data-toggle="collapse" data-target="#div_step_3_body">
                        <i class="bi bi-3-circle mr-2"></i>確認數量
                    </div>
                    <div id="div_step_3_body" class="collapse">
                        <div class="card-body p-2">
                            <div class="FrozenTable" style="font-size: 1rem;">
                                <table class="table table-bordered">
                                    <caption id="caption_items"></caption>
                                    <thead>
                                        <tr>
                                            <th style="min-width: 75px">數量</th>
                                            <th style="min-width: 75px">庫存</th>
                                            <th style="min-width: 100px">料號</th>
                                            <th>品名</th>
                                        </tr>
                                    </thead>
                                    <tbody id="tbody_items">
                                    </tbody>
                                </table>
                            </div>
                            <form method="POST" action="{{ route('stock.store', [$s_id, $p_id]) }}">
                                @csrf
                                <input name="type" type="hidden" value="">
                                <input name="items" type="hidden" value="">
                                <input name="place" type="hidden" value="">
                                <input name="company" type="hidden" value="{{ $company }}">
                                <div class="form-row justify-content-around mt-2">
                                    <button id="btn_confirm" class="btn btn-info btn-lg col-4" type="submit">完成</button>
                                    <button id="btn_cancel" class="btn btn-danger btn-lg col-4"
                                        type="button">取消，上一步</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('script')
    <script type="text/javascript">
        var type = 0; //作業類型
        var create_items = new Array(); //新建儲位的商品
        //避免點擊展開
        $("[data-toggle='collapse'][data-target^='#div_step']").click(function(event) {
            event.stopPropagation();
        });
        //移轉作業輸入調整
        $('#select_type').on('change', function() {
            $('#div_place').toggle($(this).val() == '99');
        });
        //作業類型->選擇商品
        $('#btn_to_step2').click(function() {
            var obj = $('#select_type');
            type = obj.val();
            var place = $('#select_place').val();
            if (type == '') {
                alert('請選擇作業類型');
                $('#div_step_2_body').collapse('hide');
                return;
            } else if (type == '99' && place == '') {
                alert('請選擇儲位');
                $('#div_step_2_body').collapse('hide');
                return;
            }
            switch (type) {
                case '1': //入庫作業
                case '2': //出庫作業
                case '98': //調撥作業
                case '99': //移轉作業
                    $('#div_item_list').show();
                    $('#div_item_create').hide();
                    break;
                case '3':
                    create_items = [];
                    $('#div_item_list').hide();
                    $('#div_item_create').show();
                    break;

            }
            $('input[name="type"]').val(type);
            $('input[name="place"]').val(place);
            var text = obj[0].options[obj[0].selectedIndex].text;
            $('#caption_items').text(text);
            $('#div_step_body').collapse('hide');
            $('#div_step_2_body').collapse('show');
            if (type == '3') {
                $('#input_new_item_1').focus();
            }
        });
        //選擇商品->確認數量
        $('#btn_to_step3').click(function() {
            var data = [];
            switch (type) {
                case '1': //入庫作業
                case '2': //出庫作業
                case '98': //調撥作業
                case '99': //移轉作業
                    if ($('input[id^=input_item_]:checked').length == 0) {
                        alert('請勾選商品');
                        return;
                    }
                    $('#div_step_2 input:checked').each(function(i, e) {
                        data.push({
                            no: $(e).val(),
                            name: $(e).data().item_name,
                            stock: $(e).data().item_stock
                        });
                    });
                    break;
                case '3': //首次入庫
                    if ($('#tbody_create_items>tr').length == 0) {
                        alert('請加入商品');
                        return;
                    }
                    $('#tbody_create_items>tr').each(function(i, e) {
                        data.push({
                            no: $(e).find('.item_no').text(),
                            name: $(e).find('.item_name').text(),
                            stock: 0
                        });
                    });
                    break;
            }
            add_items(type, data);
            $('#div_step_2_body').collapse('hide');
            $('#div_step_3_body').collapse('show');
            //window.scrollTo(0, document.body.scrollHeight);
        });
        //取消並返回作業類型
        $('#btn_cancel_to_step1').click(function() {
            $('input[id^=input_item_]').prop('checked', false);
            $('#tbody_create_items').html('');
            create_items = [];
            $('#div_step_body').collapse('show');
            $('#div_step_2_body').collapse('hide');
        });
        //完成
        $('#btn_confirm').click(function() {
            var error = false;
            var items = [];
            $('#tbody_items>tr').each(function(i, e) {
                var qty = parseInt($(e).find('.item_qty').val());
                if (qty <= 0) {
                    $(e).addClass('table-danger');
                    alert('異動數量不得<=0');
                    error = true;
                    return false;
                }
                var stock = parseInt($(e).find('.item_stock').text());
                if ((type == 2 || type == 98 || type == 99) && qty > stock) {
                    $(e).addClass('table-danger');
                    alert('出庫/調撥/移轉作業，異動數量不得超過庫存');
                    error = true;
                    return false;
                }
                items.push({
                    no: $(e).find('.item_no').text(),
                    name: $(e).find('.item_name').text(),
                    qty: qty
                });
            });
            if (items.length == 0 || error) {
                return false;
            } else {
                $('input[name="items"]').val(JSON.stringify(items));
                lockScreen();
            }
        });
        //取消並返回選擇商品
        $('#btn_cancel').click(function() {
            $('#tbody_items').html('');
            $('#div_step_3_body').collapse('hide');
            $('#div_step_2_body').collapse('show');
        });
        //選取後，自動加入
        $('#input_new_item_2').on('change', function() {
            add_create_item(this);
        });
        //料號按下Enter
        $('#input_new_item_1,#input_new_item_2').keyup(function(event) {
            if (event.keyCode === 13) {
                add_create_item(this);
            }
        });
        //加入新建儲位的商品
        $('#btn_add_item').click(function() {
            if (IsNullOrEmpty($('#input_new_item_1').val())) {
                add_create_item($('#input_new_item_2'));
            } else {
                add_create_item($('#input_new_item_1'));
            }
        });
        //將選擇的商品加入新建儲位的清單
        function add_create_item(obj) {
            var item_no = $(obj).val();
            $('#input_new_item_1,#input_new_item_2').val('');
            $(obj).focus();
            if (create_items.includes(item_no)) {
                alert('已加入該商品');
                return;
            }
            var item_name = $('#idList>option[value="' + item_no + '"]').text();
            if (IsNullOrEmpty(item_name)) {
                alert('找不到該料號，或者，已經建立過');
                return;
            }
            create_items.push(item_no);
            var dom = '';
            dom += '<tr>';
            dom += '<td class="item_no">' + item_no + '</td>';
            dom += '<td class="item_name">' + item_name + '</td>';
            dom +=
                '<td><button class="btn btn-danger btn-sm w-100" type="button" onclick="delete_create_item(this)">刪除</button></td>';
            dom += '</tr>';
            $('#tbody_create_items').append(dom);
        }
        //將商品移除新建儲位的清單
        function delete_create_item(obj) {
            var tr = $(obj).closest('tr');
            var item_no = tr.find('.item_no').text();
            create_items.splice(create_items.indexOf(item_no), 1);
            tr.remove();
        }
        //將選擇的商品加入數量的清單
        function add_items(type, data) {
            var dom = '';
            $(data).each(function(i, e) {
                //出庫預設數量為庫存
                var qty = (type == 2 ? e.stock : 1);
                dom += '<tr>';
                dom +=
                    '<td><input class="form-control item_qty" type="number" min="0" max="65535" step="1" value="' +
                    qty + '" required></td>';
                dom += '<td class="item_stock">' + e.stock + '</td>';
                dom += '<td class="item_no">' + e.no + '</td>';
                dom += '<td class="item_name">' + e.name + '</td>';
                dom += '</tr>';
            });
            $('#tbody_items').html(dom);
        }
    </script>
@stop
