@extends('layouts.base')
@section('css')
    <style type="text/css">
    </style>
@endsection
@section('title')
    入出庫紀錄
@stop
@section('content')
    <div class="container-fluid">
        <form id="form_search" action="{{ route('stock.log') }}" method="get">
            <div class="form-row">
                <div class="form-group col-auto">
                    <label for="input_start_date">起日</label>
                    <input id="input_start_date" name="start_date" class="form-control" type="date"
                        value="{{ old('start_date') }}">
                </div>
                <div class="form-group col-auto">
                    <label for="input_end_date">訖日</label>
                    <input id="input_end_date" name="end_date" class="form-control" type="date"
                        value="{{ old('end_date') }}">
                </div>
                <div class="form-group col-auto">
                    <label>
                        查詢方式
                    </label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <select class="custom-select" id="select_type" name="type">
                                <option {{ empty(old('type')) || old('type') == 1 ? 'selected' : '' }} value="1">儲位
                                </option>
                                <option {{ old('type') == 2 ? 'selected' : '' }} value="2">商品</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div id="div_place" class="form-group col-auto"
                    style=" {{ empty($type) || $type == 1 ? '' : 'display: none;' }}">
                    <label>儲位</label>
                    <div class="input-group">
                        <input name="place" list="place_list" class="form-control" type="text"
                            value="{{ old('place') }}" autocomplete="off">
                    </div>
                </div>
                <div id="div_item" class="form-group col-auto" style=" {{ $type == 2 ? '' : 'display: none;' }}">
                    <label>商品</label>
                    <div class="input-group">
                        <input name="item" list="item_list" class="form-control" type="text"
                            value="{{ old('item') }}" autocomplete="off">
                    </div>
                </div>
                @if (Auth::user()->hasAllRoles(['YoHouse', 'YoHouse2']))
                    <div class="col-3">
                        <label>公司</label>
                        <select class="custom-select" name="company">
                            @foreach ($companies as $company)
                                <option value="{{ $company }}" {{ old('company') == $company ? 'selected' : '' }}>
                                    {{ $company }}</option>
                            @endforeach
                        </select>
                    </div>
                @endif
            </div>
            <button class="btn btn-primary" type="submit">查詢</button>
        </form>
        <div class="form-row mt-2">
            <div class="col">
                @if (!empty($data))
                    <div class="FrozenTable" style="max-height: 77vh;">
                        <table id="table_data" class="table table-bordered table-hover sortable table-filter">
                            <thead>
                                <tr>
                                    <th style="width: 5%;">流水號</th>
                                    <th style="width: 12%;">儲位</th>
                                    <th style="width: 7%;">人員</th>
                                    <th class="filter-col" style="width: 7%;">作業</th>
                                    <th class="filter-col" style="width: 7%;">類型</th>
                                    <th class="p-0">
                                        <table class="inner-table">
                                            <thead>
                                                <tr>
                                                    <th>品名</th>
                                                    <th style="width: 10%;">數量</th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </th>
                                    <th style="width: 8%;">時間</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($data as $item)
                                    <?php
                                    switch ($item->log_type) {
                                        case 1:
                                            $class = 'table-success';
                                            break;
                                        case 2:
                                            $class = 'table-warning';
                                            break;
                                        case 3:
                                            $class = 'table-info';
                                            break;
                                        default:
                                            $class = '';
                                            break;
                                    }
                                    ?>
                                    <tr class="{{ $class }}">
                                        <td>{{ $item->id }}</td>
                                        <td>{{ "【{$item->p_id}】{$item->Place->name}" }}</td>
                                        <td>{{ $item->User->name }}</td>
                                        <td>{{ $item->log_type_name }}</td>
                                        <td>{{ $item->log_kind_name }}</td>
                                        @if (count($item->Details) == 0)
                                            <td></td>
                                        @else
                                            <td class="p-0">
                                                <table class="inner-table">
                                                    <tbody>
                                                        @foreach ($item->Details as $d)
                                                            <tr>
                                                                <td class="text-left">
                                                                    {{ "【{$d->item_no}】{$d->item_name}" }}</td>
                                                                <td style="width: 10%;">{{ number_format($d->qty) }}
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </td>
                                        @endif
                                        <td>{{ $item->created_at }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                @endif
            </div>
        </div>
    </div>
    @if (!empty($places))
        <datalist id="place_list">
            @foreach ($places as $place)
                <option value="{{ $place->id }}">{{ $place->name }}</option>
            @endforeach
        </datalist>
    @endif
    @if (!empty($items))
        <datalist id="item_list">
            @foreach ($items as $item)
                <option value="{{ $item->item_no }}">{{ $item->item_name }}</option>
            @endforeach
        </datalist>
    @endif
@stop
@section('script')
    <script type="text/javascript">
        $(document).ready(InitialDate());

        function InitialDate() {
            var today = new Date();
            if ($("#input_start_date").val() == '') {
                $("#input_start_date").val(DateToString(today));
            }
            if ($("#input_end_date").val() == '') {
                $("#input_end_date").val(DateToString(today));
            }
        }
        //分組方式
        $("#select_type").on('change', function() {
            switch (this.value) {
                case "1": //儲位
                    $("#div_item").hide();
                    $("#div_place").show();
                    break;
                case "2": //商品
                    $("#div_place").hide();
                    $("#div_item").show();
                    break;
            }
        });
    </script>
@stop
