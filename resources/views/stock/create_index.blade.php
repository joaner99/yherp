@extends('layouts.base')
@section('css')
    <style type="text/css">
        .header>div {
            display: flex;
        }

        .header>div>div {
            padding: 0.75rem 1.5rem;
            border: 1px solid #ced4da;
        }

        .header>div>div:first-child {
            border-top-left-radius: 5px;
            border-bottom-left-radius: 5px;
        }

        .header>div>div:last-child {
            border-top-right-radius: 5px;
            border-bottom-right-radius: 5px;
            flex-grow: 1;
        }

        .layer {
            text-align: center;
            font-size: 4rem;
            padding: 1.25rem 0;
        }

        .layer>a>span {
            font-weight: bolder;
            font-size: 6rem;
            color: #c82333;
        }
    </style>
@endsection
@section('title')
    倉庫異動作業
@stop
@section('content')
    <div class="container-fluid" style="font-size: 2rem;">
        <div class="form-row header">
            <div class="col-xl-auto mb-2">
                <div class="bg-secondary text-white">倉庫</div>
                <div>{{ $place->first()->Stock->id }}</div>
                <div>{{ $place->first()->Stock->name }}</div>
            </div>
            <div class="col-xl-auto mb-2">
                <div class="bg-secondary text-white">架數</div>
                <div>{{ str_pad($place->first()->shelf, 2, '0', STR_PAD_LEFT) }}</div>
            </div>
        </div>
        <div class="form-row">
            <div class="col text-success">※請點選層數</div>
        </div>
        <div class="form-row">
            <div class="col">
                <div class="list-group">
                    @foreach ($place->sortByDesc('layer') as $p)
                        <div class="list-group-item layer">
                            <a href="{{ route('stock.create', [$p->s_id, $p->id]) }}" class="">
                                第<span>{{ $p->layer }}</span>層
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@stop
@section('script')
    <script type="text/javascript"></script>
@stop
