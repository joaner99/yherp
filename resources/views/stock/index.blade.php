@extends('layouts.base')
@section('css')
    <style type="text/css">
    </style>
@endsection
@section('title')
    外掛倉儲
@stop
@section('content')
    <div class="container-fluid">
        @if (!empty($stocks))
            <div class="form-row">
                @if (Auth::user()->hasAllRoles(['YoHouse', 'YoHouse2']))
                    <div class="col-3">
                        <form action="{{ route('stock.index') }}" method="GET">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <label class="input-group-text">公司</label>
                                </div>
                                <select class="custom-select" name="company">
                                    @foreach ($companies as $c)
                                        <option value="{{ $c }}" {{ old('company') == $c ? 'selected' : '' }}>
                                            {{ $c }}</option>
                                    @endforeach
                                </select>
                                <div class="input-group-append">
                                    <button class="btn btn-info" type="submit">切換</button>
                                </div>
                            </div>
                        </form>
                    </div>
                @endif
                <div class="col-auto">
                    <ul class="pagination m-0">
                        @foreach ($stocks as $key => $stock)
                            <li class="page-item {{ $key == 0 ? 'active' : '' }}"
                                data-target="#div_stock_{{ $stock->id }}">
                                <button class="page-link" type="button">{{ $stock->name }}</button>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
            @foreach ($stocks as $key => $stock)
                <div id="div_stock_{{ $stock->id }}" class="mb-2" style="{{ $key == 0 ? '' : 'display: none;' }}">
                    <div class="form-row">
                        <div class="col"></div>
                        <div class="col-auto">
                            <form action="{{ route('stock.export', $stock->id) }}" method="post">
                                @csrf
                                <button class="btn btn-success btn-sm" type="submit">
                                    <i class="bi bi-file-earmark-excel"></i>匯出Excel
                                </button>
                            </form>
                        </div>
                    </div>
                    <div class="form-row mt-2">
                        <div class="col">
                            <div class="FrozenTable" style="max-height: 83vh;">
                                <table class="table table-bordered sortable table-filter">
                                    <caption>{{ $stock->name . '-' . $stock->id }}</caption>
                                    <thead>
                                        <tr>
                                            <th style="width: 8%;">操作</th>
                                            <th class="filter-col" style="width: 12%;">儲位</th>
                                            <th class="filter-col" style="width: 7%;">架</th>
                                            <th class="filter-col" style="width: 7%;">層</th>
                                            <th class="p-0">
                                                <table class="inner-table">
                                                    <thead>
                                                        <tr>
                                                            <th>品名</th>
                                                            <th style="width: 110px;">庫存</th>
                                                            <th style="width: 70px;">操作</th>
                                                        </tr>
                                                    </thead>
                                                </table>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($stock->Places as $place)
                                            <tr>
                                                <td>
                                                    <div class="d-flex justify-content-around">
                                                        <a href="{{ route('stock.create', [$stock->id, $place->id]) . '?company=' . $company }}"
                                                            class="btn btn-warning btn-sm">異動</a>
                                                        <a href="{{ route('stock.log') . '?type=1&place=' . $place->id . '&company=' . $company }}"
                                                            class="btn btn-info btn-sm">紀錄</a>
                                                    </div>
                                                </td>
                                                <td>{{ "【{$place->id}】{$place->name}" }}</td>
                                                <td>{{ $place->shelf }}</td>
                                                <td>{{ $place->layer }}</td>
                                                @if (count($place->Details) == 0)
                                                    <td></td>
                                                @else
                                                    <td class="p-0">
                                                        <table class="inner-table">
                                                            <tbody>
                                                                @foreach ($place->Details as $d)
                                                                    <tr>
                                                                        <td class="text-left">
                                                                            {{ "【{$d->item_no}】{$d->item_name}" }}
                                                                        </td>
                                                                        <td style="width: 110px;">
                                                                            {{ number_format($d->qty) }}
                                                                        </td>
                                                                        <td style="width: 70px;">
                                                                            <a href="{{ route('stock.log') . '?type=2&item=' . $d->item_no . '&company=' . $company }}"
                                                                                class="btn btn-info btn-sm">紀錄</a>
                                                                        </td>
                                                                    </tr>
                                                                @endforeach
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                @endif
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        @endif
    </div>
@stop
@section('script')
    <script type="text/javascript"></script>
@stop
