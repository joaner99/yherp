@extends('layouts.base')
@section('title')
    搓合調撥總表
@stop
@section('css')
    <style type="text/css">
        /*checkbox size*/
        input[type="checkbox"] {
            transform: scale(2);
        }

        [data-toggle="modal"] {
            cursor: pointer;
        }

        .card-header {
            font-size: 2rem;
            font-weight: bold
        }

        .card-body {
            padding: 0.5rem;
        }
    </style>
@endsection
@section('content')
    <!-- Modal 詳細資料 -->
    <div id="modal_order" class="modal fade">
        <div class="modal-dialog modal-dialog-centered modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="modal-title">
                        <h1>詳細資料</h1>
                        <h4 name="item_no"></h4>
                        <h4 name="item_name"></h4>
                    </div>
                    <button type="button" class="close" data-dismiss="modal">
                        <span>&times;</span>
                    </button>
                </div>
                <form method="POST" action="{{ route('order_confirm.store') }}"enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <div class="FrozenTable">
                            <table class="table table-bordered table-hover sortable">
                                <caption>訂單</caption>
                                <thead>
                                    <tr>
                                        <th>ERP訂單編號</th>
                                        <th>原始訂單編號</th>
                                        <th>需求數</th>
                                        <th class="need_purchase_col">協商換貨</th>
                                    </tr>
                                </thead>
                                <tbody id="tbody_orders"></tbody>
                            </table>
                            <table class="table table-bordered table-hover sortable">
                                <caption>未進貨採購單</caption>
                                <thead>
                                    <tr>
                                        <th>採購單號</th>
                                        <th>採購日期</th>
                                        <th>廠商簡稱</th>
                                        <th>採購數量</th>
                                        <th>已進數量</th>
                                        <th>未進數量</th>
                                    </tr>
                                </thead>
                                <tbody id="tbody_purchase_orders"></tbody>
                            </table>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="need_purchase_col">
                            <button class="btn btn-primary" type="submit">儲存協商換貨</button>
                        </div>
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">關閉</button>
                    </div>
                    <input name="order_type" type="hidden" value="13">
                </form>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="form-row mt-2">
            <div class="col-auto">
                <ul class="pagination m-0">
                    <li class="page-item active" data-target="#div_transfer">
                        <button class="page-link" type="button">
                            <span class="badge badge-light">
                                {{ collect($data)->where('sale_suggest', '>', 0)->where('main_stock', '>', 0)->count() }}
                            </span>需調撥
                        </button>
                    </li>
                    <li class="page-item" data-target="#div_purchase">
                        <button class="page-link" type="button">
                            <span class="badge badge-light">
                                {{ collect($data)->where('main_suggest', '>', 0)->count() }}
                            </span>需採購
                        </button>
                    </li>
                    <li class="page-item" data-target="#div_purchase_in">
                        <button class="page-link" type="button">
                            <span class="badge badge-light">
                                {{ collect($data)->where('purchase_undone', '>', 0)->count() }}
                            </span>未進貨
                        </button>
                    </li>
                    <li class="page-item" data-target="#div_total">
                        <button class="page-link" type="button">
                            <span class="badge badge-light">
                                {{ count($data) }}
                            </span>
                            搓合總表
                        </button>
                    </li>
                </ul>
            </div>
        </div>
        <div id="div_transfer" class="form-row mt-2">
            <div class="col">
                <div class="card">
                    <div class="card-header bg-primary text-white">
                        需調撥
                    </div>
                    <div class="card-body">
                        <div class="FrozenTable" style="max-height: 76vh;">
                            <table class="table table-bordered table-hover sortable">
                                <colgroup>
                                    <col span="6">
                                    <col class="table-danger">
                                </colgroup>
                                <thead>
                                    <tr>
                                        <th style="width: 4%;">序</th>
                                        <th style="width: 10%;">供應商</th>
                                        <th style="width: 9%;">料號</th>
                                        <th>品名</th>
                                        <th style="width: 7%;">出貨倉<br>庫存數</th>
                                        <th style="width: 7%;">出貨倉<br>需求數</th>
                                        <th style="width: 8%;">出貨倉<br>建議調撥數</th>
                                        <th style="width: 6%;">操作</th>
                                    </tr>
                                </thead>
                                <tbody id="tbody_transfer">
                                    <?php $index = 1; ?>
                                    @foreach ($data as $item)
                                        @if ($item['sale_suggest'] > 0 && $item['main_stock'] > 0)
                                            <tr data-item_no="{{ $item['item_no'] }}"
                                                data-item_name="{{ $item['item_name'] }}"
                                                data-qty="{{ $item['sale_suggest'] }}" data-orders="{{ $item['orders'] }}"
                                                data-purchase_orders="{{ $item['purchase_orders'] }}">
                                                <td>{{ $index++ }}</td>
                                                <td>{{ $item['item_supplier_name'] }}</td>
                                                <td>{{ $item['item_no'] }}</td>
                                                <td class="text-left">
                                                    {{ $item['item_name'] }}
                                                </td>
                                                <td>{{ number_format($item['sale_stock']) }}</td>
                                                <td class="text-primary">
                                                    {{ number_format($item['need']) }}
                                                </td>
                                                <td class="{{ $item['sale_suggest'] > 0 ? 'text-danger' : '' }}">
                                                    {{ number_format($item['sale_suggest']) }}
                                                </td>
                                                <td>
                                                    <button class="btn btn-info btn-sm" type="button" data-toggle="modal"
                                                        data-target="#modal_order">明細</button>
                                                </td>
                                            </tr>
                                        @endif
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="div_purchase" class="form-row mt-2" style="display: none;">
            <div class="col">
                <div class="card">
                    <div class="card-header bg-success text-white">
                        需採購
                    </div>
                    <div class="card-body">
                        <div class="form-row mb-2">
                            <div class="col-auto">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">供應商</span>
                                    </div>
                                    <select class="custom-select" id="select_supplier">
                                        @if (!empty($data))
                                            @foreach (collect($data)->where('main_suggest', '>', 0)->pluck('item_supplier_name')->unique() as $item)
                                                <option>{{ $item }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                    <div class="input-group-append">
                                        <button class="btn btn-primary" id="btn_copy" type="button">
                                            將料號、採購數，複製到剪貼簿
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="col"></div>
                            <div class="col-auto">
                                @if (!empty($unchinaPurchaseExport))
                                    <form action="{{ route('export_table') }}" method="post">
                                        @csrf
                                        <input name="export_name" type="hidden" value="非中國採購申請單">
                                        <input name="export_data" type="hidden" value="{{ $unchinaPurchaseExport }}">
                                        <button class="btn btn-success" type="submit">
                                            <i class="bi bi-box-arrow-up"></i>匯出非中國採購申請單
                                        </button>
                                    </form>
                                @endif
                            </div>
                            <div class="col-auto">
                                @if (!empty($chinaPurchaseExport))
                                    <form action="{{ route('export_table') }}" method="post">
                                        @csrf
                                        <input name="export_name" type="hidden" value="中國採購申請單">
                                        <input name="export_data" type="hidden" value="{{ $chinaPurchaseExport }}">
                                        <button class="btn btn-success" type="submit">
                                            <i class="bi bi-box-arrow-up"></i>匯出中國採購申請單
                                        </button>
                                    </form>
                                @endif
                            </div>
                        </div>
                        <div class="FrozenTable" style="max-height: 71vh;">
                            <table class="table table-bordered table-hover sortable">
                                <colgroup>
                                    <col span="5">
                                    <col class="table-danger">
                                </colgroup>
                                <thead>
                                    <tr>
                                        <th style="width: 4%;">序</th>
                                        <th style="width: 10%;">供應商</th>
                                        <th>國別</th>
                                        <th style="width: 9%;">料號</th>
                                        <th>品名</th>
                                        <th style="width: 8%;">主倉<br>建議採購數</th>
                                        <th style="width: 6%;">操作</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $index = 1; ?>
                                    @foreach ($data as $item)
                                        @if ($item['main_suggest'] == 0)
                                            @continue
                                        @endif
                                        <tr data-item_no="{{ $item['item_no'] }}"
                                            data-item_name="{{ $item['item_name'] }}"
                                            data-orders="{{ $item['orders'] }}"
                                            data-purchase_orders="{{ $item['purchase_orders'] }}"
                                            data-main_suggest="{{ $item['main_suggest'] }}">
                                            <td>{{ $index++ }}</td>
                                            <td>{{ $item['item_supplier_name'] }}</td>
                                            <td>{{ $item['is_china'] ? '中國' : '' }}</td>
                                            <td>{{ $item['item_no'] }}</td>
                                            <td class="text-left">
                                                {{ $item['item_name'] }}
                                            </td>
                                            <td class="{{ $item['main_suggest'] > 0 ? 'text-danger' : '' }}">
                                                {{ number_format($item['main_suggest']) }}
                                            </td>
                                            <td>
                                                <button class="btn btn-info btn-sm" type="button" data-toggle="modal"
                                                    data-target="#modal_order">明細</button>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="div_purchase_in" class="form-row mt-2" style="display: none;">
            <div class="col">
                <div class="card">
                    <div class="card-header bg-warning text-white">
                        未進貨
                    </div>
                    <div class="card-body">
                        <div class="form-row">
                            <div class="col">
                                <div class="FrozenTable" style="max-height: 76vh;">
                                    <table class="table table-bordered table-hover sortable">
                                        <colgroup>
                                            <col span="4">
                                            <col class="table-danger">
                                        </colgroup>
                                        <thead>
                                            <tr>
                                                <th style="width: 4%;">序</th>
                                                <th style="width: 10%;">供應商</th>
                                                <th style="width: 9%;">料號</th>
                                                <th>品名</th>
                                                <th style="width: 8%;">未進貨<br>採購數</th>
                                                <th style="width: 6%;">操作</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $index = 1; ?>
                                            @foreach ($data as $item)
                                                @if ($item['purchase_undone'] > 0)
                                                    <tr data-item_no="{{ $item['item_no'] }}"
                                                        data-item_name="{{ $item['item_name'] }}"
                                                        data-orders="{{ $item['orders'] }}"
                                                        data-purchase_orders="{{ $item['purchase_orders'] }}">
                                                        <td>{{ $index++ }}</td>
                                                        <td>{{ $item['item_supplier_name'] }}</td>
                                                        <td>{{ $item['item_no'] }}</td>
                                                        <td class="text-left">
                                                            {{ $item['item_name'] }}
                                                        </td>
                                                        <td class="text-success">
                                                            {{ number_format($item['purchase_undone']) }}
                                                        </td>
                                                        <td>
                                                            <button class="btn btn-info btn-sm" type="button"
                                                                data-toggle="modal" data-target="#modal_order">明細</button>
                                                        </td>
                                                    </tr>
                                                @endif
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="div_total" class="form-row my-2" style="display: none;">
            <div class="col">
                <div class="card">
                    <div class="card-header bg-secondary text-white">
                        搓合總表
                    </div>
                    <div class="card-body">
                        <div class="FrozenTable" style="max-height: 76vh; font-size:0.9rem;">
                            <table class="table table-bordered table-hover sortable">
                                <colgroup>
                                    <col span="6">
                                    <col span="4" style="background-color: aliceblue;">
                                    <col span="4" style="background-color: antiquewhite;">
                                    <col>
                                </colgroup>
                                <thead>
                                    <tr>
                                        <th style="width: 3%;">序</th>
                                        <th style="width: 7%;">供應商</th>
                                        <th style="width: 8%;">料號</th>
                                        <th>品名</th>
                                        <th style="width: 4%;">調撥<br>單位</th>
                                        <th style="width: 4%;">採購<br>單位</th>
                                        <th style="width: 5%;">出貨倉<br>庫存數</th>
                                        <th class="d-none" style="width: 5%;">出貨倉<br>安庫數</th>
                                        <th style="width: 5%;">出貨倉<br>需求數</th>
                                        <th style="width: 6%;">出貨倉<br>實際調撥數</th>
                                        <th style="width: 6%;">出貨倉<br>建議調撥數</th>
                                        <th style="width: 5%;">主倉<br>庫存數</th>
                                        <th class="d-none" style="width: 5%;">主倉<br>安庫數</th>
                                        <th style="width: 5%;">未進貨<br>採購數</th>
                                        <th style="width: 6%;">主倉<br>實際採購數</th>
                                        <th style="width: 6%;">主倉<br>建議採購數</th>
                                        <th style="width: 6%;">操作</th>
                                    </tr>
                                </thead>
                                <tbody id="tbody_data">
                                    <?php $index = 1; ?>
                                    @foreach ($data as $item)
                                        <tr data-item_supplier_name="{{ $item['item_supplier_name'] }}"
                                            data-item_no="{{ $item['item_no'] }}"
                                            data-item_name="{{ $item['item_name'] }}"
                                            data-orders="{{ $item['orders'] }}"
                                            data-purchase_orders="{{ $item['purchase_orders'] }}"
                                            data-main_suggest="{{ $item['main_suggest'] }}">
                                            <td>{{ $index++ }}</td>
                                            <td>{{ $item['item_supplier_name'] }}</td>
                                            <td>{{ $item['item_no'] }}</td>
                                            <td class="text-left" style="font-size:0.8rem;">
                                                {{ $item['item_name'] }}
                                            </td>
                                            <td>{{ number_format($item['box_qty']) }}</td>
                                            <td>{{ number_format($item['purchase_qty']) }}</td>
                                            <td>{{ number_format($item['sale_stock']) }}</td>
                                            <td class="text-primary d-none">
                                                {{ number_format($item['sale_ss']) }}
                                            </td>
                                            <td class="text-primary">
                                                {{ number_format($item['need']) }}
                                            </td>
                                            <td class="{{ $item['sale_need'] > 0 ? 'text-danger' : '' }}">
                                                {{ number_format($item['sale_need']) }}
                                            </td>
                                            <td class="{{ $item['sale_suggest'] > 0 ? 'text-danger' : '' }}">
                                                {{ number_format($item['sale_suggest']) }}
                                            </td>
                                            <td>{{ number_format($item['main_stock']) }}
                                            </td>
                                            <td class="text-primary d-none">{{ number_format($item['main_ss']) }}
                                            </td>
                                            <td class="text-success">
                                                {{ number_format($item['purchase_undone']) }}
                                            </td>
                                            <td class="{{ $item['main_need'] > 0 ? 'text-danger' : '' }}">
                                                {{ number_format($item['main_need']) }}
                                            </td>
                                            <td class="{{ $item['main_suggest'] > 0 ? 'text-danger' : '' }}">
                                                {{ number_format($item['main_suggest']) }}
                                            </td>
                                            <td>
                                                <button class="btn btn-info btn-sm" type="button" data-toggle="modal"
                                                    data-target="#modal_order">明細</button>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('script')
    <script type="text/javascript">
        //複製剪貼簿
        $("#btn_copy").click(function() {
            var value = '';
            var supplier = $("#select_supplier").val();
            $("#tbody_data>tr").each(function(index, element) {
                var data = $(element).data();
                if (data.item_supplier_name == supplier) {
                    var name = data.item_name.trim();
                    var need = data.main_suggest;
                    if (need != 0) {
                        //品名*需求數。ex: 燈泡*1
                        value += (name + ' ×' + need + '\r\n');
                    }
                }
            });
            if (value == '') {
                alert('複製失敗');
            } else {
                const el = document.createElement('textarea');
                el.value = value;
                document.body.appendChild(el);
                el.select();
                document.execCommand('copy');
                document.body.removeChild(el);
                alert('已複製到剪貼簿');
            }
        });
        //詳細資訊
        var orderURL = '{{ route('order_detail', '') }}/';
        $('#modal_order').on('show.bs.modal', function(event) {
            var rTarget = $(event.relatedTarget);
            var data = rTarget.closest("tr").data();
            var need_purchase = data.main_suggest > 0;
            $('[name="item_no"]').text(data.item_no);
            $('[name="item_name"]').text(data.item_name);
            $('.need_purchase_col').toggle(need_purchase);
            //訂單詳細資料
            var orders = data.orders;
            var ordersDom = '';
            if (orders.length > 0) {
                $(orders).each(function(index, element) {
                    ordersDom += '<tr>';
                    ordersDom += '<td><a href="' + orderURL + element.order_no + '">' + element.order_no +
                        '</a></td>';
                    ordersDom += '<td>' + element.original_no + '</td>';
                    ordersDom += '<td>' + element.need + '</td>';
                    if (need_purchase) { //需採購
                        if (element.change_item == 1) {
                            ordersDom += '<td>✔</td>';
                        } else {
                            ordersDom += '<td><input name="order_no[]" type="checkbox" value="' +
                                element.order_no + '"></td>';
                        }
                    }
                    ordersDom += '</tr>';
                });

            }
            $("#tbody_orders").html(ordersDom);
            //採購詳細資料
            var purchase_orders = data.purchase_orders;
            var purchaseOrdersDom = '';
            if (purchase_orders.length > 0) {
                $(purchase_orders).each(function(index, element) {
                    purchaseOrdersDom += '<tr>';
                    purchaseOrdersDom += '<td>' + element.order_no + '</td>';
                    purchaseOrdersDom += '<td>' + element.date + '</td>';
                    purchaseOrdersDom += '<td>' + element.supplier + '</td>';
                    purchaseOrdersDom += '<td>' + element.purchase + '</td>';
                    purchaseOrdersDom += '<td>' + element.done + '</td>';
                    purchaseOrdersDom += '<td>' + (element.purchase - element.done) + '</td>';
                    purchaseOrdersDom += '</tr>';
                });
            }
            $("#tbody_purchase_orders").html(purchaseOrdersDom);
        });
    </script>
@stop
