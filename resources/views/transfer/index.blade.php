@extends('layouts.base')
@section('title')
    調撥作業
@stop
@section('css')
    <style type="text/css">
        th.advanced {
            background-color: #007bff !important
        }

        /* Chrome, Safari, Edge, Opera */
        input::-webkit-outer-spin-button,
        input::-webkit-inner-spin-button {
            -webkit-appearance: none;
            margin: 0;
        }

        .table_data th:nth-child(4),
        .table_data th:nth-child(5),
        .table_data th:nth-child(6),
        .table_data th:nth-child(8),
        .table_data td:nth-child(4),
        .table_data td:nth-child(5),
        .table_data td:nth-child(6),
        .table_data td:nth-child(8) {
            display: none;
        }
    </style>
@endsection
@section('content')
    <div class="container-fluid">
        <form action="{{ route('transfer.index') }}" method="GET">
            <div class="form-row">
                <div class="col-auto">
                    <a class="btn btn-info"
                        href="{{ route('transfer.index') . '?include_empty_stock=' . old('include_empty_stock') }}">全部</a>
                </div>
                <div class="col-auto">
                    <a class="btn btn-info"
                        href="{{ route('transfer.index') . '?date=' . Carbon\Carbon::today()->format('Y-m-d') . '&include_empty_stock=' . old('include_empty_stock') }}">今天</a>

                </div>
                <div class="col-auto">
                    <a class="btn btn-info"
                        href="{{ route('transfer.index') .'?date=' .Carbon\Carbon::today()->addDays(1)->format('Y-m-d') .'&include_empty_stock=' .old('include_empty_stock') }}">明天</a>
                </div>
                <div class="col-auto">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">指定出車日期</span>
                        </div>
                        <input id="input_date" name="date" class="form-control" type="date"
                            value="{{ old('date') }}">
                        <div class="input-group-append">
                            <button class="btn btn-info" type="submit">查詢</button>
                        </div>
                    </div>
                </div>
                <div class="col-auto">
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="input_include_empty_stock"
                            name="include_empty_stock" {{ old('include_empty_stock') ? 'checked' : '' }} value="1">
                        <label class="custom-control-label" for="input_include_empty_stock">顯示主倉空庫存</label>
                    </div>
                </div>
                @if (!empty($datas) && count($datas) > 0)
                    <div class="col-auto ml-2">
                        <div class="custom-control custom-switch">
                            <input type="checkbox" class="custom-control-input" id="input_show_advanced">
                            <label class="custom-control-label text-primary" for="input_show_advanced">進階資訊</label>
                        </div>
                    </div>
                @endif
                <div class="col"></div>
                <div class="col-auto text-success">
                    ※每5分鐘更新
                </div>
            </div>
        </form>
        <div class="form-row mt-2">
            <div class="col-auto">
                <nav>
                    <ul class="pagination m-0">
                        <li class="page-item active" data-target="#div_unfloor">
                            <button class="page-link" type="button">
                                <span class="badge badge-light">{{ count($datas['非地板']) }}</span>
                                非地板
                            </button>
                        </li>
                        <li class="page-item" data-target="#div_floor">
                            <button class="page-link" type="button">
                                <span class="badge badge-light">{{ count($datas['地板']) }}</span>
                                地板
                            </button>
                        </li>
                        <li class="page-item" data-target="#div_floor_total">
                            <button class="page-link" type="button">
                                <span class="badge badge-light">{{ count($floor_total) }}</span>
                                地板統計
                            </button>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
        @foreach ($datas as $key => $data)
            <div id="div_{{ $key == '地板' ? 'floor' : 'unfloor' }}" class="form-row mt-2"
                style="{{ $key == '地板' ? 'display: none;' : '' }}">
                <div class="col">
                    <div class="FrozenTable" style="max-height: 82vh; font-size:0.9rem;">
                        <table class="table table-bordered sortable table-filter table_data">
                            <caption>{{ (empty(old('date')) ? '全部' : old('date')) . ' ' . $key }}</caption>
                            <thead>
                                <tr>
                                    <th class="filter-col" style="width: 8%;">大類</th>
                                    <th style="width: 8%;">料號</th>
                                    <th>品名</th>
                                    <th class="advanced" style="width: 6%;">最小單位</th>
                                    <th class="advanced" style="width: 15%;">訂單需求</th>
                                    <th class="advanced" style="width: 6%;">出貨倉<br>庫存</th>
                                    <th style="width: 15%;">主倉庫存</th>
                                    <th style="width: 6%;">安全庫存</th>
                                    <th style="width: 6%;">調撥量</th>
                                    <th style="width: 6%;">建議量</th>
                                    <th style="width: 6%;">已出庫<br>未清點</th>
                                    <th style="width: 8%;">異動時間</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($data as $d)
                                    <tr class="{{ $d['safety_warn'] ? 'table-danger text-danger' : '' }}">
                                        <td>{{ $d['item_type1'] }}</td>
                                        <td>{{ $d['item_no'] }}</td>
                                        <td class="text-left">{{ $d['item_name'] }}</td>
                                        <td>
                                            <form action="{{ route('items.update_config') }}" method="POST"
                                                enctype="multipart/form-data">
                                                @csrf
                                                <input name="box_qty" class="form-control text-center" type="number"
                                                    value="{{ $d['item_box'] }}" min="0" step="1" required>
                                                <input name="id" value="{{ $d['item_no'] }}" type="hidden">
                                                <input name="item_name" value="{{ $d['item_name'] }}" type="hidden">
                                            </form>
                                        </td>
                                        <td class="p-0">
                                            @if (!empty($d['orders']) && count($d['orders']) > 0)
                                                <table class="inner-table">
                                                    <tbody>
                                                        @foreach ($d['orders'] as $order)
                                                            <tr class="{{ $order['is_self'] ? 'text-success' : '' }}">
                                                                <td style="width: 150px;">{{ $order['order_no'] }}</td>
                                                                <td>{{ number_format($order['qty']) }}</td>
                                                            </tr>
                                                        @endforeach
                                                        @if ($key == '地板')
                                                            @if (collect($d['orders'])->where('is_self', true)->count() > 0)
                                                                <tr class="table-warning text-success">
                                                                    <td style="width: 150px;">親送小計</td>
                                                                    <td>
                                                                        {{ number_format(collect($d['orders'])->where('is_self', true)->sum('qty')) }}
                                                                    </td>
                                                                </tr>
                                                            @endif
                                                            @if (collect($d['orders'])->where('is_self', false)->count() > 0)
                                                                <tr class="table-warning">
                                                                    <td style="width: 150px;">非親送小計</td>
                                                                    <td>
                                                                        {{ number_format(collect($d['orders'])->where('is_self', false)->sum('qty')) }}
                                                                    </td>
                                                                </tr>
                                                            @endif
                                                        @else
                                                            <tr class="table-warning">
                                                                <td style="width: 150px;">小計</td>
                                                                <td>{{ number_format($d['total_require']) }}</td>
                                                            </tr>
                                                        @endif
                                                    </tbody>
                                                </table>
                                            @endif
                                        </td>
                                        <td>
                                            {{ number_format($d['total_sale_stock']) }}
                                        </td>
                                        <td class="p-0">
                                            @if (!empty($d['stock']) && count($d['stock']) > 0)
                                                <table class="inner-table">
                                                    <tbody>
                                                        @foreach ($d['stock'] as $place)
                                                            <tr>
                                                                <td style="width: 100px;">
                                                                    <a href="{{ route('stock.create', ['B001', $place->m_id]) }}"
                                                                        target="_blank">
                                                                        {{ $place->m_id }}
                                                                    </a>
                                                                </td>
                                                                <td>{{ number_format($place->qty) }}</td>
                                                            </tr>
                                                        @endforeach
                                                        <tr class="table-warning">
                                                            <td style="width: 100px;">小計</td>
                                                            <td>{{ number_format($d['stock']->sum('qty')) }}</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            @else
                                                0
                                            @endif
                                        </td>
                                        <td>{{ number_format($d['safety_stock']) }}</td>
                                        <td>{{ number_format($d['need']) }}</td>
                                        <td>{{ number_format($d['suggest']) }}</td>
                                        <td>{{ number_format($d['unchecked_stock']) }}</td>
                                        <td>{{ $d['updated_at'] }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        @endforeach
        <div id="div_floor_total" class="form-row mt-2" style="display: none;">
            <div class="col">
                <div class="FrozenTable" style="max-height: 82vh;">
                    <table class="table table-bordered sortable">
                        <caption>{{ (empty(old('date')) ? '全部' : old('date')) . ' 地板統計' }}</caption>
                        <thead>
                            <tr>
                                <th>料號</th>
                                <th>品名</th>
                                <th>親送</th>
                                <th>非親送</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($floor_total as $d)
                                <tr>
                                    <td>{{ $d['item_no'] }}</td>
                                    <td class="text-left">{{ $d['item_name'] }}</td>
                                    <td>{{ number_format($d['self_qty']) }}</td>
                                    <td>{{ number_format($d['unself_qty']) }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop
@section('script')
    <script type="text/javascript">
        $(document).ready(function() {
            //10分鐘自動刷新
            setInterval(function() {
                lockScreen();
                location.reload();
            }, 10 * 60 * 1000);
        });
        //進階資訊顯示
        $('#input_show_advanced').on('change', function() {
            var idxs = [4, 5, 6, 8];
            idxs.forEach(element => {
                $('.table_data th:nth-child(' + element + '),.table_data td:nth-child(' + element + ')')
                    .toggle($(this).is(":checked"));
            });
        });
        //顯示庫存為0
        $('#input_include_empty_stock').on('change', function() {
            $(this).closest('form').submit();
        });
        //更新最小單位
        $('input[name="box_qty"]').on('change', function() {
            var val = $(this).val();
            if ($.isNumeric(val)) {
                $(this).closest('form').submit();
            }
        });
    </script>
@stop
