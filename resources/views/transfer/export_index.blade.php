@extends('layouts.base')
@section('title')
    調撥匯出作業
@stop
@section('css')
    <style type="text/css">
        /*商品明細-料號*/
        table.inner-table>thead>tr>th:nth-child(2),
        table.inner-table>tbody>tr>td:nth-child(2) {
            width: 150px;
        }

        /*商品明細-數量*/
        table.inner-table>thead>tr>th:nth-child(1),
        table.inner-table>tbody>tr>td:nth-child(1) {
            width: 100px;
        }
    </style>
@endsection
@section('content')
    <div class="container-fluid">
        <div class="form-row mt-2">
            <div class="col">
                <div class="FrozenTable" style="max-height: 88vh;">
                    <table id="table_data" class="table table-bordered sortable">
                        <thead>
                            <tr>
                                <th style="width: 6%;">操作</th>
                                <th style="width: 5%;">流水號</th>
                                <th style="width: 7%;">清點人</th>
                                <th style="width: 8%;">清點日期</th>
                                <th style="width: 8%;">匯出日期</th>
                                <th class="p-0">
                                    <table class="inner-table">
                                        <thead>
                                            <tr>
                                                <th>數量</th>
                                                <th>料號</th>
                                                <th>品名</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <form id="form_export" action="{{ route('transfer.export') }}" method="POST"
                                enctype="multipart/form-data">
                                @csrf
                                @foreach ($data as $item)
                                    <tr>
                                        <td>
                                            <form id="form_export" action="{{ route('transfer.export') }}" method="POST"
                                                enctype="multipart/form-data">
                                                @csrf
                                                <input name="id" value="{{ $item->id }}" type="hidden">
                                                <button class="btn btn-success btn-sm" type="submit">
                                                    <i class="bi bi-file-earmark-excel"></i>匯出
                                                </button>
                                            </form>
                                        </td>
                                        <td>{{ $item->id }}</td>
                                        <td>{{ $item->User->name }}</td>
                                        <td>{{ $item->created_at }}</td>
                                        <td>{{ $item->exported_at }}</td>
                                        <td class="p-0">
                                            @if (!empty($item->Details) && count($item->Details) > 0)
                                                <table class="inner-table">
                                                    <tbody>
                                                        @foreach ($item->Details->groupBy('LogStock.item_no') as $item_no => $d)
                                                            <tr>
                                                                <td>{{ $d->sum('LogStock.qty') }}</td>
                                                                <td>{{ $item_no }}</td>
                                                                <td class="text-left">
                                                                    {{ $d->first()->LogStock->item_name }}
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </form>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop
@section('script')
    <script type="text/javascript"></script>
@stop
