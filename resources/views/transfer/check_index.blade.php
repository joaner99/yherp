@include('includes.login')
@extends('layouts.base')
@section('title')
    調撥清點作業
@stop
@section('css')
    @yield('login_css')
    <style type="text/css">
    </style>
@endsection
@section('content')
    @yield('login_body')
    <div class="container-fluid">
        <div class="form-row">
            <div class="col">
                <div class="FrozenTable" style="max-height: 88vh;">
                    <table id="table_data" class="table table-bordered sortable table-filter">
                        <thead>
                            <tr>
                                <th class="sort-none" style="width: 5%;">
                                    <input id="chk_all" type="checkbox">
                                </th>
                                <th class="filter-col" style="width: 10%;">調撥日期</th>
                                <th style="width: 8%;">調撥量</th>
                                <th style="width: 10%;">料號</th>
                                <th>品名</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $date => $d)
                                @foreach ($d->groupBy('item_no') as $item_no => $group)
                                    <tr>
                                        <td>
                                            <input class="confirm" type="checkbox">
                                            @foreach ($group as $item)
                                                <input name="item" type="hidden" value="{{ $item->id }}">
                                            @endforeach
                                        </td>
                                        <td>{{ $date }}</td>
                                        <td>{{ number_format($group->sum('qty')) }}</td>
                                        <td>{{ $item_no }}</td>
                                        <td class="text-left">{{ $group->first()->item_name }}</td>
                                    </tr>
                                @endforeach
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="form-row mt-2">
            <div class="col-auto">
                <form id="form_check" action="{{ route('transfer.check') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <input name="items" type="hidden">
                    <input id="input_user_id" name="user_id" type="hidden">
                    <button id="btn_confirm" class="btn btn-primary" type="button">確認</button>
                </form>
            </div>
        </div>
    </div>
@stop
@section('script')
    @yield('login_js')
    <script type="text/javascript">
        //全選功能
        $('#chk_all').change(check_all);
        //確認檢查
        $('#btn_confirm').click(function() {
            var selected_order = $('#table_data>tbody>tr .confirm:checked');
            if (selected_order.length == 0) {
                alert('請勾選項目');
                return false;
            } else {
                $('#modal_login').modal('show');
            }
        });
        //未登入防止關閉
        $('#modal_login').on('hide.bs.modal', function(e) {
            if (isLogin) {
                $('#input_user_id').val(user_id);
                var ids = new Array();
                var selected_order = $('#table_data>tbody>tr .confirm:checked');
                $(selected_order).find('~input[name="item"]').each(function(i, e) {
                    ids.push($(e).val());
                });
                $('input[name="items"]').val(JSON.stringify(ids));
                $('#form_check').submit();
            } else {
                return false;
            }
        })
    </script>
@stop
