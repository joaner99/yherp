@extends('layouts.master')
@section('master_title')
    @yield('title')
@stop
@section('master_css')
    <style type="text/css">
        /*checkbox size*/
        input[type="checkbox"] {
            transform: scale(1.4);
        }

        input.form-control {
            /*shadow-sm*/
            box-shadow: 0 .125rem .25rem rgba(0, 0, 0, .075) !important;
        }

        @media (max-width: 1919px) {
            input[type="checkbox"] {
                transform: scale(1.2);
            }
        }

        @media (max-width: 1279px) {
            input[type="checkbox"] {
                transform: scale(1);
            }
        }

        @media (min-width: 1200px) {

            #nav_logo,
            #nav_sidebar {
                flex: 0 0 14%;
                max-width: 14%;
            }

            [role="main"] {
                flex: 0 0 86%;
                max-width: 86%;
            }
        }

        /*鎖定畫面*/
        div#lockscreen {
            width: 0;
            height: 0;
            position: fixed;
            top: 0;
            left: 0;
            display: none;
            background-color: rgba(0, 0, 0, .4)
        }

        /*鎖定畫面-讀取動畫*/
        div#lockscreen>div>.spinner-border {
            border-width: 2em;
            width: 20rem;
            height: 20rem;
        }

        /*Sidebar*/
        .sidebar {
            position: fixed;
            top: 0;
            bottom: 0;
            left: 0;
            z-index: 100;
            /* Behind the navbar */
            padding: 48px 0 0;
            /* Height of navbar */
            box-shadow: inset -1px 0 0 rgba(0, 0, 0, .1);
        }

        .sidebar-sticky {
            position: relative;
            top: 0;
            height: calc(100vh - 48px);
            padding-top: .5rem;
            padding-bottom: 5rem;
            overflow-x: hidden;
            overflow-y: auto;
            /* Scrollable contents if viewport is shorter than content. */
        }

        @supports ((position: -webkit-sticky) or (position: sticky)) {
            .sidebar-sticky {
                position: -webkit-sticky;
                position: sticky;
            }
        }

        .sidebar .nav-link {
            font-weight: bold;
            color: #333;
        }

        .sidebar .nav-link .bi {
            margin-right: 4px;
        }

        .sidebar .nav-link.active {
            color: #007bff;
            font-size: 1.1rem;
        }

        .sidebar .nav-link:hover {
            color: white;
            background-color: #999;
        }

        .sidebar .nav-link.active .bi {
            color: inherit;
        }

        .sidebar-heading {
            font-size: .75rem;
            text-transform: uppercase;
        }

        /*Content*/
        [role="main"] {
            padding-top: 48px;
            /* Space for fixed navbar */
        }

        [role="main"]>.container,
        [role="main"]>.container-fluid {
            padding-top: 15px;
        }

        /*Navbar*/
        .navbar-brand {
            padding-top: .75rem;
            padding-bottom: .75rem;
            font-size: 1rem;
            background-color: rgba(0, 0, 0, .25);
            box-shadow: inset -1px 0 0 rgba(0, 0, 0, .25);
        }

        .navbar .form-control {
            padding: .75rem 1rem;
            border-width: 0;
            border-radius: 0;
        }

        /*摺疊內容*/
        .nav-item>a[aria-expanded=true]>.bi-caret-down-fill {
            transform: rotate(180deg);
        }

        .nav .nav {
            font-size: 0.9rem;
            flex-wrap: nowrap;
        }

        .nav-item-group>.nav>.nav-item>.nav-link {
            padding: 0.25rem 0 0.25rem 1.5rem;
        }

        .nav-link {
            padding: 0.5rem;
        }

        /* 最後更新時間 */
        .LastUpdateTime {
            position: fixed;
            bottom: 0;
            left: 0;
            z-index: 99999;
            color: red;
        }

        /*checkbox list*/
        .form-check>.form-check-input[type=checkbox] {
            margin-top: 0.365rem;
        }

        .form-check-list {
            border: 1px solid #ced4da;
            border-radius: 0.25rem;
            padding: 0.25rem 1rem;
            overflow: auto;
        }

        .form-check-list-sm {
            height: calc(1.5em + .5rem + 2px) !important;
            padding-top: .25rem !important;
            padding-bottom: .25rem !important;
            padding-left: .5rem !important;
            font-size: .875rem !important;
        }

        .form-check-list-horizontal {
            display: flex;
            padding: 0.325rem 0.5rem;
            border: 1px solid #dee2e6 !important;
            border-radius: 0.25rem;
            box-shadow: 0 0.125rem 0.25rem rgb(0 0 0 / 8%);
            height: calc(1.5em + .75rem + 2px);
        }

        .form-check-list>.form-check {
            margin: 6px 0;
        }

        .form-check-list-horizontal>.form-check {
            margin: 2px 6px;
        }

        .form-check-list-sm>.form-check {
            margin: 0 0.5rem;
        }

        #div_user_menu>.dropdown-menu>.dropdown-header {
            font-size: 1.2rem;
            font-weight: bold;
            padding: 0.25rem 1rem;
        }

        #div_user_menu>.dropdown-menu>.dropdown-header>.bi {
            margin-right: 0.25rem;
        }

        #div_user_menu>.dropdown-menu>.dropdown-group>.dropdown-item {
            padding: 0.25rem 2.5rem;
        }

        .original_no_font {
            font-family: monospace;
        }
    </style>
    @yield('css')
@endsection
@section('master_body')
    <nav class="navbar navbar-dark bg-dark fixed-top flex-md-nowrap p-0">
        <a id="nav_logo" class="navbar-brand col-md-4 col-lg-3 d-none d-md-block mr-0"
            href="{{ route('Home') }}">優居外掛ERP</a>
        <button class="navbar-toggler d-block d-md-none m-1" type="button" data-toggle="collapse"
            data-target="#nav_sidebar">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="col-auto d-none d-md-block p-2">
            <button id="btn_zoom_in" class="btn btn-secondary" type="button">
                <i class="bi bi-arrow-bar-left m-0"></i>
            </button>
            <button id="btn_zoom_out" class="btn btn-secondary" style="display: none;" type="button">
                <i class="bi bi-arrow-bar-right m-0"></i>
            </button>
        </div>
        <div class="col"></div>
        <div class="col-auto p-0">
            <div id="div_user_menu" class="nav-item dropdown">
                <a id="navbarDropdown" class="navbar-brand nav-link dropdown-toggle mr-0" href="#" role="button"
                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                    <i class="bi bi-person-fill"></i>
                    {{ Auth::user()->name }}
                    <span class="caret"></span>
                </a>
                <div class="dropdown-menu dropdown-menu-right shadow" aria-labelledby="navbarDropdown">
                    @role('YoHouse')
                        @canany(['user', 'role', 'sys_config'])
                            <div class="dropdown-header">
                                <i class="bi bi-gear-fill"></i>系統設定
                            </div>
                            <div class="dropdown-group">
                                @can('user')
                                    <a class="dropdown-item" href="{{ route('users.index') }}">
                                        使用者管理
                                    </a>
                                @endcan
                                @can('role')
                                    <a class="dropdown-item" href="{{ route('roles.index') }}">
                                        角色管理
                                    </a>
                                @endcan
                                @can('sys_config')
                                    <a class="dropdown-item" href="{{ route('sys_config.index') }}">
                                        系統參數設定
                                    </a>
                                @endcan
                                @role('admin')
                                    <a class="dropdown-item" href="{{ route('order_columns.index') }}">
                                        網路訂單欄位設定
                                    </a>
                                @endrole
                                <a class="dropdown-item" href="{{ route('holiday.index') }}">
                                    國定假日設定
                                </a>
                            </div>
                            <div class="dropdown-divider"></div>
                        @endcan
                        @canany(['item_config', 'item_image', 'sales_performance'])
                            <div class="dropdown-header text-info">
                                <i class="bi bi-boxes"></i>商品設定
                            </div>
                            <div class="dropdown-group">
                                @can('item_config')
                                    <a class="dropdown-item" href="{{ route('items.config') }}">
                                        商品參數設定
                                    </a>
                                @endcan
                                @can('item_image')
                                    <a class="dropdown-item" href="{{ route('items.image_management') }}">
                                        商品照片設定
                                    </a>
                                @endcan
                                <a class="dropdown-item" href="{{ route('items.convert.index') }}">
                                    商品料號轉換
                                </a>
                                @can('sales_performance')
                                    <a class="dropdown-item" href="{{ route('item_marketing.index') }}">
                                        商品行銷設定
                                    </a>
                                @endcan
                            </div>
                            <div class="dropdown-divider"></div>
                        @endcan
                        @canany(['logistics_addr_mark'])
                            <div class="dropdown-header text-success">
                                <i class="bi bi-truck"></i>物流設定
                            </div>
                            <div class="dropdown-group">
                                @can('logistics_addr_mark')
                                    <a class="dropdown-item" href="{{ route('logistics_addr_mark.index') }}">
                                        地區註記
                                    </a>
                                @endcan
                            </div>
                            <div class="dropdown-divider"></div>
                        @endcan
                    @endrole
                    <a class="dropdown-header text-danger" href="{{ route('logout') }}"
                        onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                        <i class="bi bi-box-arrow-right"></i>{{ __('auth.Logout') }}
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
            </div>
        </div>
    </nav>
    <div class="container-fluid">
        <div class="row">
            <nav class="collapse d-md-block col-md-4 col-lg-3 sidebar bg-light" id="nav_sidebar">
                <div class="sidebar-sticky">
                    <ul class="nav flex-column">
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('Home') }}">
                                <i class="bi bi-house"></i>
                                <span class="nav-link-text">首頁</span>
                            </a>
                        </li>
                        @role('YoHouse')
                            <li class="nav-item nav-item-group">
                                <a class="nav-link collapsed" href="#ul_site" data-toggle="collapse" role="button"
                                    aria-expanded="false" aria-controls="ul_site">
                                    <i class="bi bi-broadcast"></i>
                                    <span class="nav-link-text">現場管理</span>
                                    <i class="bi bi-caret-down-fill float-right"></i>
                                </a>
                                <ul class="nav flex-column collapse" id="ul_site">
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('order_all') }}">
                                            <i class="bi bi-table"></i>
                                            <span class="nav-link-text">訂單總表</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('PoseinCheckDataLog', 99) }}">
                                            <i class="bi bi-broadcast"></i>
                                            <span class="nav-link-text">現場狀況</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('order_plan') }}">
                                            <i class="bi bi-broadcast"></i>
                                            <span class="nav-link-text">訂單規劃</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('stock.transfer') }}">
                                            <i class="bi bi-table"></i>
                                            <span class="nav-link-text">搓合調撥總表</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('sales_lost_check.index') }}">
                                            <i class="bi bi-table"></i>
                                            <span class="nav-link-text">未完成訂單總表</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('check_search') }}">
                                            <i class="bi bi-search"></i>
                                            <span class="nav-link-text">出貨進度查詢</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('order', ['table' => 'unchecked']) }}">
                                            <i class="bi bi-table"></i>
                                            <span class="nav-link-text">未覆核訂單</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('order', ['table' => 'unprinted']) }}">
                                            <i class="bi bi-table"></i>
                                            <span class="nav-link-text">未轉單訂單</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('order_tax_id') }}">
                                            <i class="bi bi-search"></i>
                                            <span class="nav-link-text">需統編查詢</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('items.convert.list') }}">
                                            <i class="bi bi-search"></i>
                                            <span class="nav-link-text">需轉換料號清單</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('overweight') }}">
                                            <i class="bi bi-table"></i>
                                            <span class="nav-link-text">過大過重清單</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('package.index') }}">
                                            <i class="bi bi-box-seam"></i>
                                            <span class="nav-link-text">包裝作業</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('package.list') }}">
                                            <i class="bi bi-search"></i>
                                            <span class="nav-link-text">包裝作業查詢</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('sales.floor') }}">
                                            <i class="bi bi-stack"></i>
                                            <span class="nav-link-text">人工地板銷貨統計</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('floor_count') }}">
                                            <i class="bi bi-stack"></i>
                                            <span class="nav-link-text">今日地板數量</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('order_confirm.index') }}">
                                            <i class="bi bi-card-checklist"></i>
                                            <span class="nav-link-text">表單確認作業</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('receipt.index') }}">
                                            <i class="bi bi-receipt"></i>
                                            <span class="nav-link-text">收料作業</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('receipt_return.index') }}">
                                            <i class="bi bi-receipt"></i>
                                            <span class="nav-link-text">退貨檢驗作業</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('transfer.index') }}">
                                            <i class="bi bi-recycle"></i>
                                            <span class="nav-link-text">調撥作業</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('transfer.check_index') }}">
                                            <i class="bi bi-card-checklist"></i>
                                            <span class="nav-link-text">調撥清點作業</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('transfer.export_index') }}">
                                            <i class="bi bi-download"></i>
                                            <span class="nav-link-text">調撥匯出作業</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('group_picking') }}">
                                            <i class="bi bi-search"></i>
                                            <span class="nav-link-text">群撿查詢</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-item nav-item-group">
                                <a class="nav-link collapsed" href="#ul_stock" data-toggle="collapse" role="button"
                                    aria-expanded="false" aria-controls="ul_stock">
                                    <i class="bi bi-box"></i>
                                    <span class="nav-link-text">庫存管理</span>
                                    <i class="bi bi-caret-down-fill float-right"></i>
                                </a>
                                <ul class="nav flex-column collapse" id="ul_stock">
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('stockAll') }}">
                                            <i class="bi bi-table"></i>
                                            <span class="nav-link-text">庫存總表</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('stock_unusual') }}">
                                            <i class="bi bi-table"></i>
                                            <span class="nav-link-text">庫存異常清單</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('itemp') }}">
                                            <i class="bi bi-table"></i>
                                            <span class="nav-link-text">儲位總表</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('items.summary') }}">
                                            <i class="bi bi-table"></i>
                                            <span class="nav-link-text">商品總表</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('safety_stock.index') }}">
                                            <i class="bi bi-table"></i>
                                            <span class="nav-link-text">商品安庫表(銷售)</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('stock.safety_stock') }}">
                                            <i class="bi bi-table"></i>
                                            <span class="nav-link-text">商品安庫表</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('items.log') }}">
                                            <i class="bi bi-table"></i>
                                            <span class="nav-link-text">商品履歷</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('items.stock_history') }}">
                                            <i class="bi bi-table"></i>
                                            <span class="nav-link-text">商品庫存紀錄</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('stock_country') }}">
                                            <i class="bi bi-table"></i>
                                            <span class="nav-link-text">中國商品庫存</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('items.detail') }}">
                                            <i class="bi bi-search"></i>
                                            <span class="nav-link-text">商品查詢</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('items.place') }}">
                                            <i class="bi bi-bookshelf"></i>
                                            <span class="nav-link-text">出貨儲位建議</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('stock.index') }}">
                                            <i class="bi bi-bookshelf"></i>
                                            <span class="nav-link-text">外掛倉儲</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('stock.log') }}">
                                            <i class="bi bi-bookshelf"></i>
                                            <span class="nav-link-text">入出庫紀錄</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('inventory.index') }}">
                                            <i class="bi bi-bookshelf"></i>
                                            <span class="nav-link-text">盤點作業</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-item nav-item-group">
                                <a class="nav-link collapsed" href="#ul_transport" data-toggle="collapse" role="button"
                                    aria-expanded="false" aria-controls="ul_transport">
                                    <i class="bi bi-truck"></i>
                                    <span class="nav-link-text">託運報表</span>
                                    <i class="bi bi-caret-down-fill float-right"></i>
                                </a>
                                <ul class="nav flex-column collapse" id="ul_transport">
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('transport.index') }}">
                                            <i class="bi bi-truck"></i>
                                            <span class="nav-link-text">託運總表</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('transport.suggest') }}">
                                            <i class="bi bi-search"></i>
                                            <span class="nav-link-text">物流建議查詢</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('ycs_transport.index') }}">
                                            <i class="bi bi-truck"></i>
                                            <span class="nav-link-text">音速物流總表</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('hct_transport.remote_index') }}">
                                            <i class="bi bi-truck"></i>
                                            <span class="nav-link-text">新竹偏遠查詢</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('check_shipment') }}" target="_blank">
                                            <i class="bi bi-truck"></i>
                                            <span class="nav-link-text">上車檢驗</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('sales_list') . '?status=5' }}">
                                            <i class="bi bi-truck"></i>
                                            <span class="nav-link-text">今日已出貨</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('transport.freight_index') }}">
                                            <i class="bi bi-truck"></i>
                                            <span class="nav-link-text">物流運費</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-item nav-item-group">
                                <a class="nav-link collapsed" href="#ul_c" data-toggle="collapse" role="button"
                                    aria-expanded="false" aria-controls="ul_c">
                                    <i class="bi bi-person"></i>
                                    <span class="nav-link-text">客服管理</span>
                                    <i class="bi bi-caret-down-fill float-right"></i>
                                </a>
                                <ul class="nav flex-column collapse" id="ul_c">
                                    @can('order_modify')
                                        {{-- <li class="nav-item">
                                        <a class="nav-link" href="{{ route('cinforest.index') }}">
                                            <i class="bi bi-tree-fill"></i>
                                            <span class="nav-link-text">真桂森林訂單</span>
                                        </a>
                                    </li> --}}
                                    @endcan
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('customer_complaint.index') }}">
                                            <i class="bi bi-pencil"></i>
                                            <span class="nav-link-text">客訴單</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('order_customized.index') }}">
                                            <i class="bi bi-pencil"></i>
                                            <span class="nav-link-text">客製化訂單
                                                @if (!empty(session()->get('badges_order_customized')))
                                                    <span class="badge badge-warning">
                                                        {{ session()->get('badges_order_customized') }}
                                                    </span>
                                                @endif
                                            </span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('quotation.index') }}">
                                            <i class="bi bi-pencil"></i>
                                            <span class="nav-link-text">報價單</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('personal_transport.overview') }}">
                                            <i class="bi bi-person"></i>
                                            <span class="nav-link-text">親送概況</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('personal_transport.index') . '?unsched=1' }}">
                                            <i class="bi bi-person"></i>
                                            <span class="nav-link-text">親送總表</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('other_transport.index') . '?unsched=1' }}">
                                            <i class="bi bi-person"></i>
                                            <span class="nav-link-text">人工總表</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('pre_order') }}">
                                            <i class="bi bi-person"></i>
                                            <span class="nav-link-text">預購總表</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('order_sample.convert') }}">
                                            <i class="bi bi-table"></i>
                                            <span class="nav-link-text">特殊訂單轉檔</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('order_sample.download_page') }}">
                                            <i class="bi bi-download"></i>
                                            <span class="nav-link-text">特殊訂單下載</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('order_sample.shopee_trans') }}">
                                            <i class="bi bi-arrow-repeat"></i>
                                            <span class="nav-link-text">蝦皮宅配出貨轉檔</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('order_return_refund.index') }}">
                                            <i class="bi bi-currency-dollar"></i>
                                            <span class="nav-link-text">蝦皮退貨退款訂單</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('package.order_packager') }}">
                                            <i class="bi bi-search"></i>
                                            <span class="nav-link-text">訂單包裝人員查詢</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('order_remark') }}">
                                            <i class="bi bi-search"></i>
                                            <span class="nav-link-text">訂單備註查詢</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('help.index') }}">
                                            <i class="bi bi-question-circle"></i>
                                            <span class="nav-link-text">幫助中心</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('credit_note.index') }}">
                                            <i class="bi bi-currency-dollar"></i>
                                            <span class="nav-link-text">折讓清單</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-item nav-item-group">
                                <a class="nav-link collapsed" href="#ul_sales" data-toggle="collapse" role="button"
                                    aria-expanded="false" aria-controls="ul_sales">
                                    <i class="bi bi-bar-chart"></i>
                                    <span class="nav-link-text">銷售報表</span>
                                    <i class="bi bi-caret-down-fill float-right"></i>
                                </a>
                                <ul class="nav flex-column collapse" id="ul_sales">
                                    @can('item_sales')
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ route('items.sales') }}">
                                                <i class="bi bi-search"></i>
                                                <span class="nav-link-text">商品進銷查詢</span>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ route('sales.shopee_discount') }}">
                                                <i class="bi bi-coin"></i>
                                                <span class="nav-link-text">蝦幣折抵查詢</span>
                                            </a>
                                        </li>
                                    @endcan
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('sales_comparison') }}">
                                            <i class="bi bi-table"></i>
                                            <span class="nav-link-text">商品成長比較表</span>
                                        </a>
                                    </li>
                                    @can('sales_report')
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ route('sales.report') }}">
                                                <i class="bi bi-table"></i>
                                                <span class="nav-link-text">銷貨日報表</span>
                                            </a>
                                        </li>
                                    @endcan
                                    @can('sales_performance')
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ route('sales.performance') }}">
                                                <i class="bi bi-table"></i>
                                                <span class="nav-link-text">庫存周轉率</span>
                                            </a>
                                        </li>
                                    @endcan
                                    @role('admin')
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ route('sales.cust_item') }}">
                                                <i class="bi bi-table"></i>
                                                <span class="nav-link-text">平台銷售</span>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ route('sales.item_kind_sales') }}">
                                                <i class="bi bi-table"></i>
                                                <span class="nav-link-text">商品大類年度銷售</span>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ route('sales.item_kind3_sales') }}">
                                                <i class="bi bi-table"></i>
                                                <span class="nav-link-text">商品小類年度銷售</span>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ route('sales.item_sales') }}">
                                                <i class="bi bi-table"></i>
                                                <span class="nav-link-text">商品類別銷售</span>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ route('sales.shopee_trans') }}">
                                                <i class="bi bi-truck"></i>
                                                <span class="nav-link-text">蝦皮出貨分析</span>
                                            </a>
                                        </li>
                                    @endrole
                                </ul>
                            </li>
                            @can('ad')
                                <li class="nav-item nav-item-group">
                                    <a class="nav-link collapsed" href="#ul_ads" data-toggle="collapse" role="button"
                                        aria-expanded="false" aria-controls="ul_ads">
                                        <i class="bi bi-badge-ad"></i>
                                        <span class="nav-link-text">廣告報表</span>
                                        <i class="bi bi-caret-down-fill float-right"></i>
                                    </a>
                                    <ul class="nav flex-column collapse" id="ul_ads">
                                        @can('ad_data')
                                            <li class="nav-item">
                                                <a class="nav-link" href="{{ route('ad.cost') }}">
                                                    <i class="bi bi-table"></i>
                                                    <span class="nav-link-text">廣告每日花費</span>
                                                </a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="{{ route('ad.profit') }}">
                                                    <i class="bi bi-table"></i>
                                                    <span class="nav-link-text">廣告效益</span>
                                                </a>
                                            </li>
                                        @endcan
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ route('ad.config') }}">
                                                <i class="bi bi-gear"></i>
                                                <span class="nav-link-text">廣告參數</span>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ route('ad.config_cost') }}">
                                                <i class="bi bi-gear"></i>
                                                <span class="nav-link-text">廣告花費設定</span>
                                            </a>
                                        </li>
                                        @role('admin')
                                            <li class="nav-item">
                                                <a class="nav-link" href="{{ route('ad.simulation') }}" target="_blank">
                                                    <i class="bi bi-table"></i>
                                                    <span class="nav-link-text">廣告模擬</span>
                                                </a>
                                            </li>
                                        @endrole
                                    </ul>
                                </li>
                            @endcan
                            <li class="nav-item nav-item-group">
                                <a class="nav-link collapsed" href="#ul_product" data-toggle="collapse" role="button"
                                    aria-expanded="false" aria-controls="ul_product">
                                    <i class="bi bi-cart-plus"></i>
                                    <span class="nav-link-text">產銷履歷</span>
                                    <i class="bi bi-caret-down-fill float-right"></i>
                                </a>
                                <ul class="nav flex-column collapse" id="ul_product">
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('product.proposal.index') }}">
                                            <i class="bi bi-lightbulb"></i>
                                            <span class="nav-link-text">商品提案</span>
                                        </a>
                                    </li>
                                    @can('product_sample')
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ route('product.sample.index') }}">
                                                <i class="bi bi-cart-plus"></i>
                                                <span class="nav-link-text">採購樣品</span>
                                            </a>
                                        </li>
                                    @endcan
                                    @canany(['product_erp_edit1', 'product_erp_edit2'])
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ route('product.erp.index') }}">
                                                <i class="bi bi-box-seam"></i>
                                                <span class="nav-link-text">ERP樣品</span>
                                            </a>
                                        </li>
                                    @endcan
                                    <li class="nav-item">
                                        <a class="nav-link" href="">
                                            <i class="bi bi-table"></i>
                                            <span class="nav-link-text">行銷新品</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-item nav-item-group">
                                <a class="nav-link collapsed" href="#ul_other" data-toggle="collapse" role="button"
                                    aria-expanded="false" aria-controls="ul_other">
                                    <i class="bi bi-collection"></i>
                                    <span class="nav-link-text">其他報表</span>
                                    <i class="bi bi-caret-down-fill float-right"></i>
                                </a>
                                <ul class="nav flex-column collapse" id="ul_other">
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('stock_can_sale') }}">
                                            <i class="bi bi-table"></i>
                                            <span class="nav-link-text">商品可銷售量表</span>
                                        </a>
                                    </li>
                                    @can('container_import')
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ route('container_import.index') }}">
                                                <i class="bi bi-table"></i>
                                                <span class="nav-link-text">進口貨櫃</span>
                                            </a>
                                        </li>
                                    @endcan
                                </ul>
                            </li>
                            <li class="nav-item nav-item-group">
                                <a class="nav-link collapsed" href="#ul_tool" data-toggle="collapse" role="button"
                                    aria-expanded="false" aria-controls="ul_tool">
                                    <i class="bi bi-calculator"></i>
                                    <span class="nav-link-text">工具</span>
                                    <i class="bi bi-caret-down-fill float-right"></i>
                                </a>
                                <ul class="nav flex-column collapse" id="ul_tool">
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('calendar.index') }}">
                                            <i class="bi bi-calendar-range"></i>
                                            <span class="nav-link-text">行事曆</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('tool.index') }}">
                                            <i class="bi bi-stack-overflow"></i>
                                            <span class="nav-link-text">臨時資料</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('tool.floor_sales') }}">
                                            <i class="bi bi-stack-overflow"></i>
                                            <span class="nav-link-text">地板銷售</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-item nav-item-group">
                                <a class="nav-link collapsed" href="#ul_warranty" data-toggle="collapse" role="button"
                                    aria-expanded="false" aria-controls="ul_warranty">
                                    <i class="bi bi-wrench-adjustable"></i>
                                    <span class="nav-link-text">保固網站管理</span>
                                    <i class="bi bi-caret-down-fill float-right"></i>
                                </a>
                                <ul class="nav flex-column collapse" id="ul_warranty">
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('warranty_web.search') }}">
                                            <i class="bi bi-search"></i>
                                            <span class="nav-link-text">保固查詢</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('warranty_web.video_config') }}">
                                            <i class="bi bi-youtube"></i>
                                            <span class="nav-link-text">影片設定</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('warranty_web.item_config') }}">
                                            <i class="bi bi-wrench-adjustable"></i>
                                            <span class="nav-link-text">保固設定</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            @role('admin')
                                <li class="nav-item nav-item-group">
                                    <a class="nav-link collapsed" href="#ul_system" data-toggle="collapse" role="button"
                                        aria-expanded="false" aria-controls="ul_system">
                                        <i class="bi bi-gear"></i>
                                        <span class="nav-link-text">系統管理</span>
                                        <i class="bi bi-caret-down-fill float-right"></i>
                                    </a>
                                    <ul class="nav flex-column collapse" id="ul_system">
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ route('schedule_log') }}">
                                                <i class="bi bi-calendar-check"></i>
                                                <span class="nav-link-text">排程管理</span>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ route('account.index') }}">
                                                <i class="bi bi-person"></i>
                                                <span class="nav-link-text">帳號管理</span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            @endrole
                        @endrole
                        @role('YoHouse2')
                            <li class="nav-item nav-item-group">
                                <a class="nav-link collapsed" href="#ul_erp" data-toggle="collapse" role="button"
                                    aria-expanded="false" aria-controls="ul_erp">
                                    <i class="bi bi-building"></i>
                                    <span class="nav-link-text">ERP</span>
                                    <i class="bi bi-caret-down-fill float-right"></i>
                                </a>
                                <ul class="nav flex-column collapse" id="ul_erp">
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('stock.index') }}">
                                            <i class="bi bi-table"></i>
                                            <span class="nav-link-text">進貨作業</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('stock.log') }}">
                                            <i class="bi bi-bookshelf"></i>
                                            <span class="nav-link-text">進貨紀錄</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('erp.sales.index') }}">
                                            <i class="bi bi-table"></i>
                                            <span class="nav-link-text">銷貨作業</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        @endrole
                    </ul>
                </div>
            </nav>
            <main role="main" class="ml-sm-auto col-md-8 col-lg-9 px-0">
                <div id="lockscreen">
                    <div class="h-100 d-flex align-items-center justify-content-center">
                        <div class="spinner-border text-secondary">
                            <span class="sr-only"></span>
                        </div>
                    </div>
                </div>
                @yield('content')
            </main>
        </div>
    </div>
    <div class="LastUpdateTime d-none d-md-block ml-2">
        最後更新時間：{{ date('Y-m-d H:i:s') }}
    </div>
@endsection
@section('master_js')
    <script type="text/javascript">
        //鎖定螢幕點選
        function lockScreen() {
            $("#lockscreen").css("width", "100%");
            $("#lockscreen").css("height", "100%");
            $("#lockscreen").css("z-index", "10000");
            $("#lockscreen").css("display", "block");
        }
        //解鎖螢幕點選
        function unlockScreen() {
            $("#lockscreen").css("width", "0");
            $("#lockscreen").css("height", "0");
            $("#lockscreen").css("z-index", "0");
            $("#lockscreen").css("display", "none");
        }
        //目前頁面啟用樣式
        $('#nav_sidebar a[href="{{ URL::current() }}"]:first').addClass('active');
        //有開啟中的頁面，則打開群組
        $(".nav-item-group").each(function(index, element) {
            if ($(element).find(".active").length > 0) {
                $(element).find(".collapse").collapse("show");
            }
        });
        //群組開啟時關閉其他群組
        $(".nav-item-group").click(function() {
            var target = $(this)[0];
            $(".nav-item-group").each(function(index, element) {
                if (element != target) {
                    $(element).find(".collapse").collapse("hide");
                }
            });
        });
        //群組子項高亮
        $(".nav .nav-link").on("click", function() {
            $(".nav").find(".active").removeClass("active");
            $(this).addClass("active");
        });
        //側邊展開
        $("#btn_zoom_in").click(function() {
            HideSidebar();
        });
        //側邊收起
        $("#btn_zoom_out").click(function() {
            ShowSidebar();
        });

        function HideSidebar() {
            $("#btn_zoom_in").hide();
            $("#btn_zoom_out").show();
            $("#nav_logo,#nav_sidebar,.LastUpdateTime").css("cssText", "display: none !important;");
            $("main").css('cssText', 'max-width: 100%; flex-basis: 100%; margin: 0 !important;');
        }

        function ShowSidebar() {
            $("#btn_zoom_out").hide();
            $("#btn_zoom_in").show();
            $("#nav_logo,#nav_sidebar,main,.LastUpdateTime").css("cssText", "");
        }
        //畫面過小，強制結束放大功能
        $(window).resize(function() {
            if ($(window).width() < 750) {
                $("#btn_zoom_out").click();
            }
        });
    </script>
    @yield('script')
@endsection
