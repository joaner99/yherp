<html>

<head>
    <style>
        body {
            font-family: 微軟正黑體;
        }

        table {
            border-width: 1px;
            border-style: solid;
            border-color: #dee2e6;
            border-collapse: collapse;
        }

        th {
            color: #fff;
            background-color: #343a40;
        }

        th,
        td {
            text-align: center;
            vertical-align: middle;
            border-width: 1px;
            border-style: solid;
            border-color: #dee2e6;
            padding: 0.75rem;
        }

        caption {
            font-weight: bold;
            font-size: 2rem;
        }
    </style>
</head>

<body>
    @if (empty($thead) && empty($tbody))
        {{ $caption ?? '' }}
    @else
        <table>
            <caption>{{ $caption ?? '' }}</caption>
            <thead>
                @if (!empty($thead))
                    <tr>
                        @foreach ($thead as $th)
                            <th scope="col">{{ $th }}</th>
                        @endforeach
                    </tr>
                @endif
            </thead>
            <tbody>
                @if (!empty($tbody))
                    @foreach ($tbody as $tr)
                        <tr>
                            @foreach ($tr as $td)
                                <td>{{ $td }}</td>
                            @endforeach
                        </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
    @endif
</body>

</html>
