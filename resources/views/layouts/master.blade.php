<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <title> @yield('master_title')</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
    <!-- jQueryUI CSS 文件 -->
    <link rel="stylesheet" href="{{ asset('css/jquery-ui.min.css') }}">
    <!-- 新 Bootstrap4 核心 CSS 文件 -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <!-- 新 Bootstrap4 Icons 文件 -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap-icons.css') }}">
    <!-- C3.JS 文件 -->
    <link rel="stylesheet" href="{{ asset('css/c3.css') }}">
    <style type="text/css">
        html {
            font-size: 16px;
        }

        html,
        body {
            height: 100%;
        }

        @media (max-width: 1919px) {
            html {
                font-size: 14px;
            }
        }

        @media (max-width: 1279px) {
            html {
                font-size: 12px;
            }
        }

        body {
            font-family: Microsoft JhengHei, Consolas;
        }

        .btn>i {
            margin-right: 0.25rem;
        }

        .FrozenTable {
            overflow: auto;
            max-height: 500px;
            border-style: double;
            border-color: #ced4da;
            border-radius: 0.25rem;
        }

        .FrozenTable>table {
            margin-bottom: 0;
            border-collapse: separate;
            border-spacing: 0;
            border-style: none;
        }

        .FrozenTable>table>caption {
            color: var(--white);
            background-color: var(--secondary);
            caption-side: top;
            text-align: left;
            padding: 0.5rem 1rem;
            font-size: 2rem;
            font-weight: bold;
            height: 64px;
        }

        .FrozenTable>table>caption,
        .FrozenTable>table>thead {
            position: sticky;
            top: 0;
            z-index: 16;
        }

        /*有標題須加上標題高度*/
        .FrozenTable>table>caption~thead {
            top: 64px;
        }

        .FrozenTable .FrozenTable>table>thead {
            z-index: 1;
        }

        .FrozenTable>table th {
            color: white;
            background-color: darkcyan;
        }

        .FrozenTable .FrozenTable>table th {
            color: white;
            background-color: var(--orange);
        }

        .FrozenTable>table>tfoot>tr>td {
            color: white;
            background-color: var(--orange);
            font-weight: bold;
        }

        .FrozenTable>table>thead>tr>th,
        .FrozenTable>table>thead>tr>td,
        .FrozenTable>table>tbody>tr>th,
        .FrozenTable>table>tbody>tr>td,
        .FrozenTable>table>tfoot>tr>th,
        .FrozenTable>table>tfoot>tr>td {
            text-align: center;
            vertical-align: middle;
            border-width: 1px;
        }

        .FrozenTable>table>tbody>tr>th table {
            border: hidden;
        }

        .FrozenTable>table>tbody>tr>th table>tbody>tr>th {
            padding: 0.25rem 0.75rem;
        }

        .table td,
        .table th {
            padding: 0.5rem;
            vertical-align: middle;
        }

        .inner-table {
            margin: 0;
            width: 100%;
            border-style: hidden;
        }

        .inner-table>thead>tr>th,
        .inner-table>thead>tr>td,
        .inner-table>tbody>tr>th,
        .inner-table>tbody>tr>td,
        .inner-table>tfoot>tr>th,
        .inner-table>tfoot>tr>td {
            text-align: center;
            vertical-align: middle;
        }

        .inner-table>tbody>tr>td:first-child {
            border-left-style: none;
        }

        .inner-table>tbody>tr>td:last-child {
            border-right-style: none;
        }

        .inner-table>tbody>tr:first-child>td {
            border-top-style: none;
        }

        .inner-table>tbody>tr:last-child>td {
            border-bottom-style: none;
        }

        .alert {
            margin-bottom: 0;
            text-align: center;
        }

        .sortable>thead>tr>th {
            cursor: pointer;
        }

        /*c3*/
        .c3-title {
            font-size: 1.5rem;
            font-weight: bold;
            fill: darkgreen;
        }

        .c3-axis,
        .c3-text {
            font-size: 12px;
        }

        .c3-axis-x-label {
            font-size: 1.1rem;
            fill: darkred;
            transform: translateY(15px);
        }

        .c3-axis-y-label {
            font-size: 1.1rem;
            fill: darkblue;
            transform: rotate(0deg);
            transform: translate(-60px, 120px);
            writing-mode: vertical-lr;
        }

        /*bs-card*/
        .card {
            box-shadow: 0 .125rem .25rem rgba(0, 0, 0, .075) !important;
        }

        /*bs-modal*/
        .modal-header {
            color: #fff;
            background-color: #17a2b8;
        }

        .modal-header>.close {
            color: #fff;
            font-size: 2rem;
        }

        /*Table filter*/
        .table-filter>thead>tr.tr-filter>th>select {
            font-size: 1em;
        }

        .custom-select-active {
            color: red;
        }

        .select-hr {
            color: white;
            background-color: #6c757d;
        }

        .clock {
            text-align: center;
            position: fixed;
            bottom: 5px;
            right: 5px;
            padding: 1px 10px;
            z-index: 99999;
            border: 2px solid #999;
            border-radius: 4px;
            color: #ffffff;
            font-size: 2.5rem;
            background: linear-gradient(90deg, #000, #555);
        }
    </style>
    @yield('master_css')
</head>

<body>
    @if ($message = Session::get('status'))
        <script type="text/javascript">
            alert("{{ $message }}");
        </script>
    @endif
    @yield('master_body')
    <!-- jQuery文件。务必在bootstrap.min.js 之前引入 -->
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <!-- jQueryUI文件-->
    <script src="{{ asset('js/jquery-ui.min.js') }}"></script>
    <!-- bootstrap.bundle.min.js 用于弹窗、提示、下拉菜单，包含了 popper.min.js -->
    <script src="{{ asset('js/popper.min.js') }}"></script>
    <!-- 最新的 Bootstrap4 核心 JavaScript 文件 -->
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <!-- C3.JS 文件 -->
    <script src="{{ asset('js/d3.js') }}"></script>
    <script src="{{ asset('js/c3.js') }}"></script>
    <script src="{{ asset('js/jquery.sortElements.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/base.js') }}?1" type="text/javascript"></script>
    <script type="text/javascript">
        //匯出Excel按鈕
        $("button[export-target]").click(function() {
            var target = $($(this).attr('export-target'));
            var tableData = Array();
            var header = Array();
            var headerIgnoreIdx = Array();
            var innerTableIdx = -1; //html 裡面的索引
            var innerTableCount = -1;
            //header data
            $(target).find(">thead>tr:last-child>th:visible").each(function(index, element) {
                var idx = $(element).index();
                if ($(element).hasClass("export-none")) {
                    headerIgnoreIdx.push(idx);
                } else {
                    var inner_table = $(element).find('table.inner-table>thead>tr>th');
                    if (inner_table.length > 0) {
                        innerTableIdx = idx;
                        innerTableCount = inner_table.length;
                        $(inner_table).each(function(i, e) {
                            header.push($(e).text().trim());
                        });
                    } else {
                        header.push($(element).text().trim());
                    }
                }
            });
            tableData.push(header);
            var innerDataIdx = -1; //資料裡面的索引
            //rows data
            $(target).find(">tbody>tr:visible").each(function(index, element) {
                var row = Array();
                $(element).find(">td:visible").each(function(index, element) {
                    var idx = $(element).index();
                    if (headerIgnoreIdx.indexOf(idx) != -1) { //忽略欄位
                        return;
                    } else if (innerTableIdx == idx) { //有內表
                        innerDataIdx = row.length;
                        var inner = [];
                        $(element).find('table.inner-table>tbody>tr').each(function(i, tr) {
                            var inner_row = [];
                            $(tr).find('td').each(function(i, td) {
                                inner_row.push($(td).text().trim());
                            });
                            inner.push(inner_row);
                        });
                        row.push(inner);
                    } else if ($(element).find("button").length == 0) { //未含有button
                        var text = $(element).text().trim();
                        if ($(element).hasClass('export-cell-multiple')) { //內容多行處裡
                            text = text.replaceAll(' ', '').replaceAll('\n', ',');
                        }
                        row.push(text);
                    } else {
                        row.push('');
                    }
                });
                tableData.push(row);
            });
            //內表處理
            if (innerDataIdx != -1) {
                var newTableData = [];
                $(tableData).each(function(original_idx, original_row) {
                    if (original_idx == 0) { //標題
                        newTableData.push(original_row);
                    } else {
                        var inner_idx = innerDataIdx;
                        var inner_data = original_row[inner_idx];
                        if (inner_data.length == 0) { //內表無資料
                            for (var i = 0; i < innerTableCount; i++) {
                                if (i == 0) {
                                    original_row[inner_idx++] = col;
                                } else {
                                    original_row.splice(inner_idx++, 0, col);
                                }
                            }
                        } else { //內表有資料
                            $(inner_data).each(function(row_idx, row) {
                                var new_row = [...original_row];
                                var insert_idx = inner_idx;
                                for (var i = 0; i < innerTableCount; i++) {
                                    if (i == 0) {
                                        new_row[insert_idx++] = row[i];
                                    } else {
                                        new_row.splice(insert_idx++, 0, row[i]);
                                    }
                                }
                                newTableData.push(new_row);
                            });
                        }
                    }
                });
                tableData = newTableData;
            }
            $("input[name='export_data']").val(JSON.stringify(tableData));
        });
        //sortable class sort function，required jquery.sortElements.js
        $('.sortable > thead > tr:last-child > th:not([colspan]):not(.sort-none)')
            .append('<i class="bi bi-sort-down" style="display: none;"></i>')
            .append('<i class="bi bi-sort-down-alt" style="display: none;"></i>')
            .prop("title", "點擊進行排序")
            .each(function() {
                if ($(this).find('.inner-table').length > 0) {
                    return;
                }
                var table = $(this).closest('table');
                var th = $(this),
                    thIndex = th.index(),
                    inverse = false;

                th.click(function() {
                    table.find(">thead>tr>th").each(function(index, element) {
                        var isTarget = $(this).index() === thIndex;
                        if (isTarget) {
                            $(element).find("i[class$=bi-sort-down]").toggle(inverse); //降冪
                            $(element).find("i[class$=bi-sort-down-alt]").toggle(!inverse); //升序
                        } else {
                            $(element).find("i").toggle(false);
                        }
                    });
                    table.find('>tbody>tr>td,tbody>tr>th').filter(function() {
                        return $(this).index() === thIndex;
                    }).sortElements(function(a, b) {
                        var aText;
                        if ($(a).find('select').length > 0) {
                            aText = $(a).find('select>option:selected').text();
                        } else {
                            aText = $.text(a);
                        }
                        aText = CustomerToNumber(aText);
                        var bText;
                        if ($(b).find('select').length > 0) {
                            bText = $(b).find('select>option:selected').text();
                        } else {
                            bText = $.text(b);
                        }
                        bText = CustomerToNumber(bText);

                        if ($.isNumeric(aText) && $.isNumeric(bText)) {
                            aText = parseFloat(aText);
                            bText = parseFloat(bText);
                        }
                        if (aText == bText)
                            return 0;
                        else {
                            return aText > bText ?
                                inverse ? -1 : 1 :
                                inverse ? 1 : -1;
                        }

                    }, function() {

                        // parentNode is the element we want to move
                        return this.parentNode;

                    });

                    inverse = !inverse;

                });

            });

        //Table create filter option
        $("table.table-filter").each(function(index, element) {
            var tr = $(element).find('>thead>tr:last-child');
            var colLen = tr.find(">th").length;
            //Get filter columns
            var filterColIndex = new Array();
            tr.find("th.filter-col").each(function(index, element) {
                filterColIndex.push(element.cellIndex);
            });
            if (filterColIndex.length == 0) {
                return;
            }
            //Build filter opotion html
            var filterHtml = '<tr class="tr-filter">';
            for (var i = 0; i < colLen; i++) {
                if (filterColIndex.includes(i)) {
                    var filterItems = new Array();
                    $(element).find(">tbody>tr>td:nth-child(" + (i + 1) + ")").each(function(index, element) {
                        var text = getFilterColData(element);
                        if (!filterItems.includes(text)) {
                            filterItems.push(text);
                        }
                    });
                    $.unique(filterItems.sort());
                    //無資料，無法篩選
                    if (filterItems.length == 0) {
                        filterHtml += "<th></th>";
                        continue;
                    }
                    filterItems.sort(function(a, b) {
                        if ($.isNumeric(a) && $.isNumeric(b)) {
                            return a - b;
                        } else {
                            return a.localeCompare(b, "zh-hant");
                        }
                    });
                    var maxLength = Math.max.apply(Math, $.map(filterItems, function(el) {
                        return el.trim().length
                    }));
                    filterHtml += '<th class="p-1"><select class="custom-select" data-col="' + (i + 1) + '">';
                    filterHtml += ('<option value="">全選</option>');
                    filterHtml += ('<option class="select-hr" disabled>' + ("-".repeat(maxLength *
                            2)) +
                        '</option>');
                    $(filterItems).each(function(index, element) {
                        filterHtml += ('<option value="' + element + '">' + element + "</option>");
                    });
                    filterHtml += "</select></th>";
                } else {
                    filterHtml += "<th></th>";
                }
            }
            filterHtml += '</tr>';

            tr.before(filterHtml);
        });
        //Table filter event handle
        $(".tr-filter>th>select").change(function() {
            //1.顯示全部資料
            var table = $(this).closest("table.table-filter");
            table.find(">tbody>tr").show();
            //2.根據每個篩選，隱藏不符合值
            var enable_col = [];
            $(table).find(".tr-filter>th>select").each(function(index, element) {
                var val = $(element).val();
                var text = $(element).find("option:selected").text();
                //未篩選則跳過
                if (IsNullOrEmpty(val) && text == "全選") {
                    $(this).removeClass('custom-select-active');
                    return;
                }
                $(this).addClass('custom-select-active');
                //隱藏不符合值
                var col = $(this).data()['col'];
                enable_col.push(col);
                table.find(">tbody>tr:visible>td:nth-child(" + col + ")").each(function(index, element2) {
                    var data_text = getFilterColData(element2);
                    if (data_text != val) {
                        $(element2).closest('tr').hide();
                    }
                });
            });
            //3.隱藏未啟用篩選項目的選項
            table.find('.tr-filter>th>select').each(function(index, element) {
                var col = $(element).data().col;
                if ($.inArray(col, enable_col) !== -1) {
                    return;
                }
                var datas = [];
                table.find(">tbody>tr:visible>td:nth-child(" + col + ")").each(function(index,
                    element2) {
                    var data_text = getFilterColData(element2);
                    if ($.inArray(data_text, datas) === -1) {
                        datas.push(data_text);
                    }
                });
                $(element).find('option:nth-child(n+3)').each(function(i, e) {
                    var val = $(e).val();
                    if ($.inArray(val, datas) === -1) {
                        $(e).hide();
                    } else {
                        $(e).show();
                    }
                });
            });
        });
        //取得TD的文字，用於篩選
        function getFilterColData(td) {
            var data_text = '';
            var val = $(td).attr('data-val');
            if (typeof val !== 'undefined' && val !== false) {
                return $(td).data().val;
            } else if ($(td).find('select').length > 0) {
                data_text = $(td).find('select>option:selected').text().trim();
            } else if ($(td).find('a').length > 0) {
                data_text = $(td).text().trim();
            } else {
                data_text = $(td).clone()
                    .children() //select all the children
                    .remove() //remove all the children
                    .end() //again go back to selected element
                    .text()
                    .trim();
            }
            return data_text;
        }
        //更新時鐘
        function clockUpdate() {
            var currentTime = new Date();
            var currentHours = currentTime.getHours();
            var currentMinutes = currentTime.getMinutes();
            var currentSeconds = currentTime.getSeconds();

            // Pad the minutes and seconds with leading zeros, if required
            currentMinutes = (currentMinutes < 10 ? "0" : "") + currentMinutes;
            currentSeconds = (currentSeconds < 10 ? "0" : "") + currentSeconds;

            // Choose either "AM" or "PM" as appropriate
            var timeOfDay = (currentHours < 12) ? "上午" : "下午";

            // Convert the hours component to 12-hour format if needed
            currentHours = (currentHours > 12) ? currentHours - 12 : currentHours;

            // Convert an hours component of "0" to "12"
            currentHours = (currentHours == 0) ? 12 : currentHours;

            // Compose the string for display
            var currentTimeString = timeOfDay + " " + currentHours + ":" + currentMinutes + ":" + currentSeconds;

            $(".clock").text(currentTimeString);
        }
        //全選功能
        function check_all() {
            var is_check = $(this).is(":checked");
            var table = $(this).closest('table');
            var idx = $(table).find('>thead>tr:not(.tr-filter)>th').index($(this).closest('th')) + 1;
            $(table).find(">tbody>tr:visible>td:nth-child(" + idx + ') input[type="checkbox"]').prop('checked',
                is_check).change();
        }
        //頁籤切換
        $('.pagination>.page-item').click(function() {
            var show_target = $(this)[0];
            $(this).parent('.pagination').find('.page-item').each(function(i, e) {
                if (e == show_target) {
                    $($(e).data().target).show();
                    $(e).addClass('active');
                } else {
                    $($(e).data().target).hide();
                    $(e).removeClass('active');
                }
            });
        })
    </script>
    @yield('master_js')
</body>

</html>
