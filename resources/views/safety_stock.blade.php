@extends('layouts.base')
@section('css')
    <style type="text/css">
        .dropdown-menu {
            max-height: 500px;
            overflow-y: auto;
        }

        #data>tbody>tr {
            height: 56px;
        }
    </style>
@endsection
@section('title')
    商品安庫表(銷售)
@stop
@section('content')
    {{-- Modal 簡述 --}}
    <div id="modal_edit" class="modal fade">
        <div class="modal-dialog modal-xl modal-dialog-centered">
            <div class="modal-content">
                <form action="{{ route('safety_stock.update') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-header">
                        <div class="modal-title">
                            <h1>簡述</h1>
                        </div>
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="container-fluid">
                            <div class="form-row">
                                <div class="form-group col-auto">
                                    <label>供應商</label>
                                    <input class="form-control" type="text" name="supplier" readonly>
                                </div>
                                <div class="form-group col-auto">
                                    <label>商品料號</label>
                                    <input class="form-control" type="text" name="item_no" readonly>
                                </div>
                                <div class="form-group col">
                                    <label>商品名稱</label>
                                    <input class="form-control" type="text" name="item_name" readonly>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col">
                                    <label class="form-check-label" for="input_remark">簡述</label>
                                    <textarea class="form-control" id="input_remark" name="remark"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button id="btn_save" type="submit" class="btn btn-primary">確認</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">取消</button>
                    </div>
                    <input type="hidden" name="id">
                </form>
            </div>
        </div>
    </div>
    {{-- Modal 覆核 --}}
    <div id="modal_check" class="modal fade">
        <div class="modal-dialog modal-xl modal-dialog-centered">
            <div class="modal-content">
                <form action="{{ route('safety_stock.check') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-header">
                        <div class="modal-title">
                            <h1>覆核</h1>
                        </div>
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="container-fluid">
                            <div class="form-row">
                                <div class="form-group col-auto">
                                    <label>供應商</label>
                                    <input class="form-control" type="text" name="supplier" readonly>
                                </div>
                                <div class="form-group col-auto">
                                    <label>商品料號</label>
                                    <input class="form-control" type="text" name="item_no" readonly>
                                </div>
                                <div class="form-group col">
                                    <label>商品名稱</label>
                                    <input class="form-control" type="text" name="item_name" readonly>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col">
                                    <label class="form-check-label">簡述</label>
                                    <textarea class="form-control" name="remark" readonly></textarea>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-auto">
                                    <label class="text-danger" for="select_signinger">覆核人</label>
                                    <select class="custom-select" id="select_signinger" name="signinger" required>
                                        <option value="" selected>請選擇...</option>
                                        @if (!empty($users))
                                            @foreach ($users as $item)
                                                <option value="{{ $item->id }}">{{ $item->name }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button id="btn_save" type="submit" class="btn btn-primary">確認</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">取消</button>
                    </div>
                    <input type="hidden" name="id">
                </form>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        @if (empty($data) || count($data) == 0)
            <div class="alert alert-danger">
                找不到資料
            </div>
        @else
            <div class="form-row align-items-center mb-2">
                <div class="col-auto">
                    <div id="FirmFilter" class="btn-group dropdown">
                        <button class="btn btn-secondary dropdown-toggle" type="button" data-toggle="dropdown"
                            aria-haspopup="true">
                            <i class="bi bi-filter"></i>
                            供應商：<span id="FirmFilterSelected">無篩選</span>
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <button class="dropdown-item active" type="button">無篩選</button>
                            <div class="dropdown-divider"></div>
                            @foreach ($data->unique('supplier')->sort() as $value)
                                @if (!empty($value->supplier))
                                    <button class="dropdown-item" type="button">
                                        {{ $value->supplier }}
                                    </button>
                                @endif
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="col-auto">
                    <form action="{{ route('export_table') }}" method="post">
                        @csrf
                        <input name="export_name" type="hidden"
                            value="{{ (new DateTime('now'))->format('Y_m_d') }}商品安庫表(銷售)">
                        <input name="export_data" type="hidden">
                        <button class="btn btn-success" type="submit" export-target="#data">
                            <i class="bi bi-file-earmark-excel"></i>匯出Excel
                        </button>
                    </form>
                </div>
                <div class="col-auto ml-3">
                    <div class="form-check">
                        <form action="{{ route('safety_stock.index') }}" method="GET">
                            <input class="form-check-input" type="checkbox" id="chk_designation" name="designation"
                                value='1' {{ request()->has('designation') ? 'checked' : '' }}>
                            <label class="form-check-label" for="chk_designation">
                                指定中國商品
                            </label>
                            <button id="submit" class="d-none" type="submit"></button>
                        </form>
                    </div>
                </div>
                <div class="col"></div>
                <div class="col-auto">
                    <form action="{{ route('safety_stock.index') }}" method="GET">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">每日銷售取樣天數</span>
                            </div>
                            <input name="days" class="form-control" type="number" value="{{ old('days') ?? 30 }}"
                                min="1" max="180" step="1" onchange="submit()">
                            <div class="input-group-append">
                                <span class="input-group-text">天</span>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-auto">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">紅底標示</span>
                        </div>
                        <select id="select_supplier" class="custom-select" style="min-width: 160px;"
                            onchange="onMarkChanger()">
                            <option value="">全部</option>
                            @foreach ($data->unique('supplier')->sort() as $value)
                                @if (!empty($value->supplier))
                                    <option value="{{ $value->supplier }}">{{ $value->supplier }}</option>
                                @endif
                            @endforeach
                        </select>
                        <div class="input-group-append">
                            <span class="input-group-text">庫存剩餘天數小於</span>
                        </div>
                        <input id="input_stock_day" class="form-control" type="number" value="40" min="0"
                            max="365" step="1" onchange="onMarkChanger()">
                        <div class="input-group-append">
                            <span class="input-group-text">天</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="FrozenTable" style="max-height: 85vh; font-size: 0.8rem;">
                <?php $tableIndex = 0; ?>
                <table id="data" class="table table-bordered table-hover sortable">
                    <thead>
                        <tr>
                            <th scope="col" style="width: 3%;">序</th>
                            <th scope="col" style="width: 10%;">供應商</th>
                            <th scope="col" style="width: 6%;">商品料號</th>
                            <th scope="col">商品名稱</th>
                            <th scope="col" style="width: 6%;">外掛主倉+<br>TMS出貨倉</th>
                            <th scope="col" style="width: 6%;">每日銷售<br>({{ old('days') ?? 30 }}天平均)</th>
                            <th scope="col" style="width: 5%;">庫存<br>剩餘天數</th>
                            <th scope="col" style="width: 5%;">建議採購<br>(30天量)</th>
                            <th scope="col" style="width: 6%;">60日最近<br>採購單號</th>
                            <th scope="col" style="width: 6%;">最後盤點<br>日期</th>
                            <th scope="col" style="width: 5%;">最後盤點<br>數量</th>
                            <th scope="col" style="width: 6%;">最後銷貨日<br>距今天數</th>
                            <th scope="col" style="width: 7%;">簡述</th>
                            <th scope="col" style="width: 7%;">覆核</th>
                        </tr>
                    </thead>
                    <tbody id="tbody_data">
                        @foreach ($data as $value)
                            <tr data-id="{{ $value->id }}" data-supplier="{{ $value->supplier }}"
                                data-remaining_day="{{ $value->remaining_day }}" data-item_no="{{ $value->item_no }}"
                                data-item_name="{{ $value->item_name }}" data-remark="{{ $value->remark }}"
                                data-signinger="{{ $value->signinger }}">
                                <td>{{ ++$tableIndex }}</td>
                                <td>{{ $value->supplier }}</td>
                                <td>
                                    <a href="{{ route('items.stock_history', ['id' => $value->item_no]) }}">
                                        {{ $value->item_no }}
                                    </a>
                                </td>
                                <td>{{ $value->item_name }}</td>
                                <td>{{ number_format($value->stock) }}</td>
                                <td>{{ number_format($value->daily_sales) }}</td>
                                <td>{{ number_format($value->remaining_day) }}</td>
                                <td>{{ number_format($value->suggest) }}</td>
                                <td>{{ $value->purchase_no }}</td>
                                <td>{{ $value->inventory_date }}</td>
                                <td>{{ empty($value->inventory_date) ? '' : number_format($value->inventory_num) }}</td>
                                <td>
                                    @if ($value->stock <= 0)
                                        {{ number_format($value->elapsed_day) }}
                                    @endif
                                </td>
                                <td>
                                    @if (empty($value->remark) && empty($value->purchase_no))
                                        <button class="btn btn-info btn-sm" type="button" data-toggle="modal"
                                            data-target="#modal_edit">
                                            <i class="bi bi-plus-lg"></i>簡述
                                        </button>
                                    @elseif(!empty($value->signinger))
                                        {{ $value->remark }}
                                    @else
                                        <div data-toggle="modal" data-target="#modal_edit">
                                            {{ $value->remark }}</div>
                                    @endif
                                </td>
                                <td>
                                    @if (!empty($value->signinger))
                                        已覆核
                                    @elseif(empty($value->purchase_no) && !empty($value->remark))
                                        <button class="btn btn-primary btn-sm" type="button" data-toggle="modal"
                                            data-target="#modal_check">
                                            <i class="bi bi-check2-square"></i>覆核
                                        </button>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="float-right text-success">資料來源時間：{{ $data->first()->created_at }}</div>
        @endif
    </div>
@stop
@section('script')
    <script type="text/javascript">
        $(document).ready(onMarkChanger());
        $("#FirmFilter .dropdown-item").on("click", function() {
            //active css add/remove
            $("#FirmFilter").find(".active").removeClass("active");
            $(this).addClass("active");

            // Declare variables
            var filter, table, tr, td, i, txtValue;
            filter = $(this)[0].innerText;
            table = document.getElementById("data");
            tr = table.getElementsByTagName("tr");
            $("#FirmFilterSelected").text(filter);
            if (filter == "無篩選") {
                $(tr).each(function(index, element) {
                    $(element).show();
                });
                return;
            }
            // Loop through all table rows, and hide those who don't match the search query
            for (i = 0; i < tr.length; i++) {
                td = tr[i].getElementsByTagName("td")[1];
                if (td) {
                    txtValue = td.textContent || td.innerText;
                    if (txtValue.toUpperCase().indexOf(filter) > -1) {
                        tr[i].style.display = "";
                    } else {
                        tr[i].style.display = "none";
                    }
                }
            }
        });
        //checkbox勾選觸發submit
        $("#chk_designation").click(function() {
            $("#submit").click();
        });

        //簡述、覆核
        $('#modal_edit,#modal_check').on('show.bs.modal', function(event) {
            var button = $(event.relatedTarget);
            var type = button.data().type;
            var data = button.closest("tr").data();
            var modal_content = $(this).find(".modal-content");

            modal_content.find("input[name='id']").val(data.id);
            modal_content.find("input[name='supplier']").val(data.supplier);
            modal_content.find("input[name='item_no']").val(data.item_no);
            modal_content.find("input[name='item_name']").val(data.item_name);
            modal_content.find("textarea[name='remark']").val(data.remark);
            modal_content.find("select[name='signinger']").val(data.signinger);
        });
        //廠商標記庫存天數變動
        function onMarkChanger() {
            var supplier = $('#select_supplier').val();
            var day = $('#input_stock_day').val();
            $('#tbody_data>tr').each(function(i, e) {
                var t_supplier = $(e).data().supplier;
                var t_day = $(e).data().remaining_day;
                if (supplier == '') {
                    if (t_day < day) {
                        $(e).addClass('table-danger');
                    } else {
                        $(e).removeClass('table-danger');
                    }
                } else {
                    if (t_supplier == supplier && t_day < day) {
                        $(e).addClass('table-danger');
                    } else {
                        $(e).removeClass('table-danger');
                    }
                }
            });
        }
    </script>
@stop
