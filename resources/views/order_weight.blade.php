@extends('layouts.base')
@section('title')
    過大過重清單
@stop
@section('css')
    <style type="text/css">
        /*過大明細*/
        #table_big_items table.inner-table>thead>tr>th:nth-child(1),
        #table_big_items table.inner-table>tbody>tr>td:nth-child(1) {
            width: 150px;
        }
    </style>
@endsection
@section('content')
    <!-- Modal 說明 -->
    <div id="modal_help" class="modal fade">
        <div class="modal-dialog modal-xl modal-dialog-centered" style="max-width: 99vw;">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="modal-title">
                        <h1>說明</h1>
                    </div>
                    <button type="button" class="close" data-dismiss="modal">
                        <span>&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="form-row">
                            <div class="col">
                                <ul class="list-group list-group-horizontal-lg">
                                    <li class="list-group-item list-group-item-danger">
                                        【一般超商】過重定義<br>總重大於{{ $shop_max_weight }}(g)
                                    </li>
                                    <li class="list-group-item list-group-item-danger">
                                        【蝦皮店到店】過重定義<br>總重大於{{ $shopee_max_weight }}(g)
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="form-row mt-2">
                            <div class="col-6">
                                @if (!empty($heavy_items) && count($heavy_items) > 0)
                                    <div class="FrozenTable" style="max-height:63vh;">
                                        <table class="table table-bordered table-hover sortable">
                                            <caption>過重判斷商品</caption>
                                            <thead>
                                                <tr>
                                                    <th>料號</th>
                                                    <th>品名</th>
                                                    <th>重量(g)</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($heavy_items as $item_no => $item)
                                                    <tr>
                                                        <td>{{ $item_no }}</td>
                                                        <td class="text-left">{{ $item['name'] }}</td>
                                                        <td>{{ $item['weight'] }}</td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                @endif
                            </div>
                            <div class="col-6">
                                @if (!empty($big_items) && count($big_items) > 0)
                                    <div class="FrozenTable" style="max-height:70vh;">
                                        <table id="table_big_items" class="table table-bordered table-hover sortable">
                                            <caption>過大判斷商品</caption>
                                            <thead>
                                                <tr>
                                                    <th class="p-0">
                                                        <table class="inner-table">
                                                            <thead>
                                                                <tr>
                                                                    <th>料號</th>
                                                                    <th>品名</th>
                                                                </tr>
                                                            </thead>
                                                        </table>
                                                    </th>
                                                    <th>數量</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($big_items as $item)
                                                    <tr>
                                                        <td class="p-0">
                                                            <table class="inner-table">
                                                                <tbody>
                                                                    @foreach ($item['items'] as $item_no => $item_name)
                                                                        <tr>
                                                                            <td>{{ $item_no }}</td>
                                                                            <td class="text-left">{{ $item_name }}</td>
                                                                        </tr>
                                                                    @endforeach
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                        <td>{{ $item['qty'] }}</td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">關閉</button>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="form-row">
            <div class="col">
                <button class="btn btn-success" type="button" data-toggle="modal" data-target="#modal_help">
                    <i class="bi bi-question-circle"></i>說明
                </button>
            </div>
        </div>
        <div class="form-row mt-2">
            <div class="col">
                <div class="FrozenTable" style="max-height:87vh;">
                    <table class="table table-bordered table-hover sortable">
                        <thead>
                            <tr>
                                <th>訂單單號</th>
                                <th>原始訂單號</th>
                                <th>銷貨單號</th>
                                <th>狀態</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (!empty($data))
                                @foreach ($data as $value)
                                    <tr>
                                        <td>{{ $value['order_no'] }}</td>
                                        <td>{{ $value['original_no'] }}</td>
                                        <td>
                                            @foreach ($value['sales_no'] as $sale_no)
                                                <a href="{{ route('check_log_detail', $sale_no) }}">
                                                    {{ $sale_no }}
                                                </a>
                                            @endforeach
                                        </td>
                                        <td>{{ $value['status'] }}</td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop
