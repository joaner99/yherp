@extends('layouts.base')
@section('title')
    蝦皮退貨退款訂單
@stop
@section('content')
    <div class="container-fluid">
        <div class="form-row">
            <div class="col-6">
                <form action="{{ route('order_return_refund.import') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">來源</span>
                        </div>
                        <select class="custom-select col-2" id="input_cust" name="cust">
                            @foreach ($cust as $c)
                                <option value="{{ $c->cust }}">{{ $c->cust_name_2 }}</option>
                            @endforeach
                        </select>
                        <div class="custom-file">
                            <input class="custom-file-input" id="input_transport_file" name="file[]" type="file"
                                accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,.xls"required>
                            <label class="custom-file-label" for="input_transport_file">請選擇蝦皮退貨退款檔案</label>
                        </div>
                        <div class="input-group-append">
                            <button type="submit" class="btn btn-primary">上傳</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="form-row mt-2">
            <div class="col">
                <div class="FrozenTable" style="max-height: 87vh;">
                    <table id="table_data" class="table table-bordered table-hover sortable table-filter">
                        <thead>
                            <tr>
                                <th style="width: 4%;">序</th>
                                <th class="filter-col">狀態群組</th>
                                <th class="filter-col">退貨/退款狀態</th>
                                <th class="filter-col">
                                    爭議狀態<br>
                                    (僅適用僅退款/快速退款流程之退貨)
                                </th>
                                <th class="filter-col">客戶簡稱</th>
                                <th>退貨編號</th>
                                <th>訂單編號</th>
                                <th style="width: 10%;">訂單成立時間</th>
                                <th style="width: 10%;">退貨申請時間</th>
                                <th style="width: 10%;">系統更新時間</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $idx = 1; ?>
                            @foreach ($data as $item)
                                <tr>
                                    <td>{{ $idx++ }}</td>
                                    <td>{{ $item->group_name }}</td>
                                    <td>{{ $item->return_status }}</td>
                                    <td>{{ $item->dispute_status }}</td>
                                    <td>{{ $item->Cust->cust_name_2 }}</td>
                                    <td>{{ $item->id }}</td>
                                    <td>{{ $item->order_no }}</td>
                                    <td>{{ $item->order_time }}</td>
                                    <td>{{ $item->return_time }}</td>
                                    <td>{{ $item->updated_at }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop
@section('script')
    <script type="text/javascript">
        //上傳檔案後顯示文件名稱
        $('#input_transport_file').change(function(e) {
            var fileName = '';
            $(e.target.files).each(function(index, element) {
                if (fileName != '') {
                    fileName += ',';
                }
                fileName += element.name;
            });
            $(this).next('.custom-file-label').html(fileName);
        });
    </script>
@stop
