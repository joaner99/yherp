@extends('layouts.base')
@section('title')
    訂單規劃
@stop
@section('css')
    <style type="text/css">
        .cust_badge {
            font-size: 1rem;
            margin: 0.25rem;
        }

        .card-body {
            padding: 0.75rem;
        }
    </style>
@endsection
@section('content')
    <!-- Modal 說明 -->
    <div id="modal_help" class="modal fade">
        <div class="modal-dialog modal-xl modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="modal-title">
                        <h1>說明</h1>
                    </div>
                    <button type="button" class="close" data-dismiss="modal">
                        <span>&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="form-row">
                            <div class="col">
                                <ul class="list-group">
                                    <li class="list-group-item">所有TMS中尚未轉成銷貨單的訂單</li>
                                    <li class="list-group-item">親送定義：物流為【COM】本公司親送</li>
                                    <li class="list-group-item">棧板定義：籃子訂單類型設定【棧板】</li>
                                    <li class="list-group-item">重物定義：學習桌、SPC、LVT、塑木、草皮</li>
                                    <li class="list-group-item">輕物定義：非重物</li>
                                    <li class="list-group-item">數量計算：SPC=1:1包、LVT=10:1包、其他=單數</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">關閉</button>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <form action="{{ route('order_plan') }}" method="GET">
            <div class="form-row">
                <div class="col-auto">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">出貨方式</span>
                        </div>
                        <select class="custom-select" id="input_type" name="kind">
                            <option value="" {{ empty(old('kind')) ? 'selected' : '' }}>全部</option>
                            <option value="1" {{ old('kind') == '1' ? 'selected' : '' }}>親送</option>
                            <option value="2" {{ old('kind') == '2' ? 'selected' : '' }}>棧板</option>
                            <option value="3" {{ old('kind') == '3' ? 'selected' : '' }}>輕物</option>
                            <option value="4" {{ old('kind') == '4' ? 'selected' : '' }}>重物</option>
                        </select>
                    </div>
                </div>
                <div class="col-auto">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">指定親送出車日期</span>
                        </div>
                        <input id="input_date" name="date" class="form-control" type="date"
                            value="{{ old('date') }}">
                        <div class="input-group-append">
                            <button class="btn btn-info" type="submit">查詢</button>
                        </div>
                    </div>
                </div>
                <div class="col"></div>
                <div class="col-auto">
                    <button class="btn btn-success" type="button" data-toggle="modal" data-target="#modal_help">
                        <i class="bi bi-question-circle"></i>說明
                    </button>
                </div>
            </div>
        </form>
        <div class="form-row mt-2">
            <div class="col">
                <div class="card">
                    <div class="card-header bg-info text-white">
                        訂單編號【{{ count($order_no_list) }}】
                    </div>
                    <div class="card-body">
                        @foreach ($order_no_list as $order_no)
                            <span class="badge badge-info cust_badge">{{ $order_no }}</span>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        <div class="form-row mt-2">
            <div class="col-5">
                <div class="card">
                    <div class="card-header bg-success text-white">
                        出貨方式統計
                    </div>
                    <div class="card-body">
                        <ul class="list-group list-group-horizontal">
                            @foreach ($kind_group as $item)
                                <li class="list-group-item">{{ $item['name'] . '：' . number_format($item['qty']) }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="card">
                    <div class="card-header bg-warning">
                        小類統計
                    </div>
                    <div class="card-body">
                        @foreach ($kind3_group as $item)
                            <span
                                class="badge badge-warning cust_badge">{{ $item['name'] . '：' . number_format($item['qty']) }}</span>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        <div class="form-row mt-2">
            <div class="col">
                <div class="FrozenTable" style="max-height: 93vh; font-size: 0.9rem;">
                    <table class="table table-bordered table-hover sortable table-filter">
                        <caption>總表</caption>
                        <thead>
                            <tr>
                                <th class="filter-col">品名</th>
                                <th class="filter-col">小類</th>
                                <th style="width: 6%;">數量</th>
                                <th style="width: 5%;">單位</th>
                                <th style="width: 8%;" class="filter-col">出貨方式</th>
                                <th style="width: 8%;" class="filter-col">快過期狀態</th>
                                <th style="width: 8%;">訂單成立時間</th>
                                <th style="width: 8%;">訂單經過小時</th>
                                <th style="width: 8%;" class="filter-col">訂單單號</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($all_data as $item)
                                <?php switch ($item['order_expired']) {
                                    case '訂單過期':
                                        $class_name = 'table-danger';
                                        break;
                                    case '優選過期':
                                        $class_name = 'table-warning';
                                        break;
                                    default:
                                        $class_name = '';
                                        break;
                                } ?>
                                <tr class="{{ $class_name }}">
                                    <td class="text-left">{{ $item['item_name'] }}</td>
                                    <td>{{ $item['item_kind3_name'] }}</td>
                                    <td>{{ number_format($item['item_qty'], 2) }}</td>
                                    <td>{{ $item['item_unit'] }}</td>
                                    <td>{{ $item['item_kind_name'] }}</td>
                                    <td>{{ $item['order_expired'] }}</td>
                                    <td>{{ $item['order_date'] }}</td>
                                    <td>
                                        @if ($item['order_hours'] != '')
                                            {{ number_format($item['order_hours']) }}
                                        @endif
                                    </td>
                                    <td><a href="{{ route('order_detail', $item['order_no']) }}">
                                            {{ $item['order_no'] }}
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop
@section('script')
    <script type="text/javascript">
        $(document).ready(InitialDate());

        function InitialDate() {
            var today = new Date();
            if ($("#input_date").val() == '') {
                $("#input_date").val(DateToString(today));
            }
        }
    </script>
@stop
