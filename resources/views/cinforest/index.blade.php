@extends('layouts.master')
@section('master_title')
    真桂森林-訂單
@stop
@section('master_css')
    <style type="text/css">
        .status_1 {
            background-color: #ffeeba;
        }

        .status_2 {
            background-color: #8fd19e;
        }
    </style>
@endsection
@section('master_body')
    {{-- Modal 填寫物流單號 --}}
    <div id="modal_tran_no" class="modal">
        <div class="modal-dialog modal-dialog-centered modal-xl">
            <div class="modal-content">
                <form method="POST" action="{{ route('cinforest.update') }}">
                    @method('PATCH')
                    @csrf
                    <div class="modal-header">
                        <h1 class="modal-title">填寫物流單號</h1>
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="FrozenTable">
                            <table class="table table-bordered sortable">
                                <tbody>
                                    <tr>
                                        <th style="width: 15%;">銷貨備註3</th>
                                        <td id="td_pbak3"></td>
                                    </tr>
                                    <tr>
                                        <th>內部備註</th>
                                        <td id="td_insidenote"></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <hr>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1">新竹物流單號(10碼)</span>
                            </div>
                            <input class="form-control" id="input_tran_no" name="tran_no" type="text"
                                placeholder="請在此輸入新竹物流單號" maxlength="10">
                            <input id="input_sale_no" name="sale_no" type="hidden" value="">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success">確定</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">取消</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="form-row mt-2">
            <div class="col">
                <div class="FrozenTable" style="max-height: 98vh;">
                    <table class="table table-bordered sortable">
                        <thead>
                            <tr>
                                <th>狀態</th>
                                <th style="width: 14%;">原始訂單編號</th>
                                <th style="width: 8%;">銷貨日期</th>
                                <th>銷貨備註3</th>
                                <th>內部備註</th>
                                <th style="width: 18%;">銷貨內容</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $o)
                                @if (!$editable)
                                    @continue
                                @endif
                                <tr class="status_{{ $o->status }}">
                                    <td>
                                        @switch($o->status)
                                            @case(1)
                                                <button class="btn btn-info btn-sm" type="button" data-toggle="modal"
                                                    data-PCOD1="{{ $o->PCOD1 }}" data-PBAK3="{{ $o->PBAK3 }}"
                                                    data-insidenote="{{ $o->InsideNote }}"
                                                    data-target="#modal_tran_no">填寫物流單號</button>
                                            @break

                                            @case(2)
                                                回填TMS<br>
                                                {{ $o->cinforest->ConsignTran }}
                                            @break
                                        @endswitch
                                    </td>
                                    <td>{{ $o->PJONO }}</td>
                                    <td>{{ $o->PDAT1 }}</td>
                                    <td>{{ $o->PBAK3 }}</td>
                                    <td>{{ $o->InsideNote }}</td>
                                    <td>
                                        <div class="FrozenTable" style="max-height: 98vh;">
                                            <table class="table table-bordered table-hover sortable bg-white">
                                                <tbody>
                                                    @foreach ($o->Histin as $d)
                                                        <tr>
                                                            <td>{{ $d->HINAM }}</td>
                                                            <td style="width: 20%">{{ number_format($d->HINUM) }}</td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop
@section('master_js')
    <script type="text/javascript">
        $('#modal_tran_no').on('shown.bs.modal', function() {
            $('#input_tran_no').trigger('focus')
        })
        $('button[data-target="#modal_tran_no"]').click(function() {
            var data = $(this).data();
            $('#input_sale_no').val(data.pcod1);
            $('#td_pbak3').text(data.pbak3);
            $('#td_insidenote').text(data.insidenote);
        });
    </script>
@stop
