@extends('layouts.base')
@section('title')
    客製化訂單
@stop
@section('css')
    <style type="text/css">
    </style>
@endsection
@section('content')
    <div class="container-fluid">
        <div class="form-row">
            <div class="col"></div>
            <div class="col-auto">
                <a class="btn btn-info" href="{{ route('order_customized.create') }}">
                    <i class="bi bi-plus-lg"></i>新增訂單
                </a>
            </div>
        </div>
        <div class="form-row mt-2">
            <div class="col">
                @if (empty($data) || count($data) == 0)
                    <div class="alert alert-danger">
                        無資料
                    </div>
                @else
                    <div class="FrozenTable" style="font-size:0.8rem; max-height: 87vh;">
                        <table id="table_data" class="table table-bordered table-hover sortable">
                            <thead>
                                <tr>
                                    <th style="width: 4%;">流水號</th>
                                    <th style="width: 7%;">訂單日期</th>
                                    <th style="width: 9%;">客戶</th>
                                    <th style="width: 5%;">銷售員</th>
                                    <th style="width: 11%;">客戶訂單</th>
                                    <th>備註1</th>
                                    <th style="width: 15%;">備註2</th>
                                    <th style="width: 7%;">建立日期</th>
                                    <th style="width: 7%;">操作</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($data as $item)
                                    <tr>
                                        <td>{{ $item->id }}</td>
                                        <td>{{ $item->order_date }}</td>
                                        <td>{{ $item->customer_text }}</td>
                                        <td>{{ $item->salesman_text }}</td>
                                        <td>{{ $item->order_no }}</td>
                                        <td>{{ $item->remark }}</td>
                                        <td>{{ $item->remark2 }}</td>
                                        <td>{{ $item->created_at }}</td>
                                        <td class="px-0 py-1">
                                            <a class="btn btn-warning btn-sm my-1"
                                                href="{{ route('order_customized.edit', $item->id) }}">
                                                <i class="bi bi-pencil-fill"></i>編輯
                                            </a>
                                            <form method="POST"
                                                action="{{ route('order_customized.destroy', $item->id) }}"
                                                onsubmit="return confirm('確定要刪除該訂單?');">
                                                @csrf
                                                <input name="_method" type="hidden" value="DELETE">
                                                <button class="btn btn-danger btn-sm my-1">
                                                    <i class="bi bi-trash"></i>刪除
                                                </button>
                                            </form>
                                            @if (empty($item->into_erp))
                                                <form action="{{ route('order_customized.complete', $item->id) }}"
                                                    method="POST">
                                                    @csrf
                                                    <button class="btn btn-primary btn-sm my-1">
                                                        <i class="bi bi-check2-square"></i>完成
                                                    </button>
                                                </form>
                                            @else
                                                <form action="{{ route('order_customized.export', $item->id) }}"
                                                    method="get">
                                                    <button class="btn btn-success btn-sm my-1">
                                                        <i class="bi bi-file-earmark-excel"></i>匯出
                                                    </button>
                                                </form>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                @endif
            </div>
        </div>
    </div>
@stop
@section('script')
    <script type="text/javascript"></script>
@stop
