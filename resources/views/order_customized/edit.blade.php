@extends('layouts.base')
@section('title')
    客製化訂單-編輯
@stop
@section('css')
    <style type="text/css">
        /*radio size*/
        input[type="radio"] {
            transform: scale(1.25);
        }

        [name*='remark'] {
            height: 150px !important;
        }

        .item_name {
            font-size: 0.8rem;
        }

        #tbody_items input {
            text-align: center;
        }
    </style>
@endsection
@section('content')
    <!-- Modal 新增片語 -->
    <div id="modal_phrase" class="modal fade">
        <div class="modal-dialog modal-xl modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="modal-title">
                        <h1>新增片語</h1>
                    </div>
                    <button type="button" class="close" data-dismiss="modal">
                        <span>&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-row mt-2">
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label for="input_tax_id">統一編號</label>
                                <input class="form-control form-control-sm" id="input_tax_id" type="text" maxlength="8">
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label for="input_contact">聯絡人</label>
                                <input class="form-control form-control-sm" id="input_contact" type="text">
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label for="input_tel">客戶電話</label>
                                <input class="form-control form-control-sm" id="input_tel" type="text">
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label for="input_mobile">行動電話</label>
                                <input class="form-control form-control-sm" id="input_mobile" type="text">
                            </div>
                        </div>
                    </div>
                    <div class="form-row mt-2">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="input_addr">送貨地址</label>
                                <input class="form-control form-control-sm" id="input_addr" type="text">
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="input_tax_addr">發票地址</label>
                                <input class="form-control form-control-sm" id="input_tax_addr" type="text">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button id="btn_phrase_confirm" class="btn btn-primary" type="button" data-dismiss="modal">確認</button>
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">取消</button>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        @if ($errors->any())
            <div class="form-row mb-2">
                <div class="col-auto">
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        @endif
        <div class="form-row">
            <div class="col-auto">
                <a class="btn btn-secondary btn-sm" href="{{ route('order_customized.index') }}">回上頁</a>
            </div>
        </div>
        <form method="POST" action="{{ route('order_customized.update', $data->id) }}">
            @method('PATCH')
            @csrf
            <div class="card mt-2">
                <h2 class="card-header text-info font-weight-bold">訂單主檔</h2>
                <div class="card-body p-2">
                    <div class="form-row">
                        <div class="col-lg-2">
                            <div class="form-group">
                                <label class="text-danger" for="input_order_date">*訂單日期</label>
                                <input class="form-control form-control-sm" id="input_order_date" name="order_date"
                                    type="date" value="{{ $data->order_date }}" required>
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <div class="form-group">
                                <label class="text-danger" for="select_customer">*客戶</label>
                                <select class="custom-select custom-select-sm" id="select_customer" name="customer"
                                    required>
                                    <option value="" selected>請選擇...</option>
                                    @if (!empty($customers))
                                        @foreach ($customers as $customer)
                                            <option value="{{ $customer->CCODE }}"
                                                {{ $customer->CCODE == $data->customer ? 'selected' : '' }}>
                                                {{ $customer->CNAM2 }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <div class="form-group">
                                <label class="text-danger" for="select_salesman">*業務員</label>
                                <select class="custom-select custom-select-sm" id="select_salesman" name="salesman"
                                    required>
                                    <option value="" selected>請選擇...</option>
                                    @if (!empty($salesmans))
                                        @foreach ($salesmans as $salesman)
                                            <option value="{{ $salesman->SCODE }}"
                                                {{ $salesman->SCODE == $data->salesman ? 'selected' : '' }}>
                                                {{ $salesman->SNAME }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <div class="form-group">
                                <label class="text-danger">*是否需要匯入ERP</label>
                                <div class="form-check-list form-check-list-sm form-check-list-horizontal">
                                    <div class="form-check">
                                        <input class="form-check-input" id="input_into_erp_1" name="into_erp"
                                            type="radio" value="1" {{ $data->into_erp == '1' ? 'checked' : '' }}>
                                        <label class="form-check-label" for="input_into_erp_1">是</label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" id="input_into_erp_0" name="into_erp"
                                            type="radio" value="0" {{ $data->into_erp == '0' ? 'checked' : '' }}>
                                        <label class="form-check-label" for="input_into_erp_0">否</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-lg-2">
                            <div class="form-group">
                                <label for="input_order_no">客戶訂單</label>
                                <input class="form-control form-control-sm" id="input_order_no" name="order_no"
                                    type="text" maxlength="40" value="{{ $data->order_no }}">
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-lg-4 m-0">
                            <label class="form-check-label" for="input_remark">備註1
                                <small class="text-muted">(200字)</small></label>
                            <textarea class="form-control form-control-sm" id="input_remark" name="remark" maxlength="200" readonly>{{ $data->remark }}</textarea>
                            <div class="mt-1">
                                <button class="btn btn-secondary btn-sm" type="button" data-toggle="modal"
                                    data-target="#modal_phrase">片語</button>
                            </div>
                        </div>
                        <div class="form-group col-lg-4 m-0">
                            <label class="form-check-label" for="input_remark2">備註2
                                <small class="text-muted">(200字)</small></label>
                            <textarea class="form-control form-control-sm" id="input_remark2" name="remark2" maxlength="200">{{ $data->remark2 }}</textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card mt-2">
                <div class="card-header text-info">
                    <div class="form-row">
                        <div class="col">
                            <h2 class="font-weight-bold m-0">商品明細檔</h2>
                        </div>
                        <div class="col-auto">
                            <button class="btn btn-info" type="button" id="btn_create">
                                <i class="bi bi-plus-lg"></i>新增商品
                            </button>
                        </div>
                    </div>
                </div>
                <div class="card-body px-3 py-0">
                    <div class="FrozenTable mt-2" style="max-height: 50vh;">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th style="width: 12%;">*料號</th>
                                    <th>品名</th>
                                    <th style="width: 7%;">*數量</th>
                                    <th style="width: 10%;">*售價(含稅)</th>
                                    <th style="width: 10%;">成本</th>
                                    <th style="width: 25%;">備註<small>(100字)</small></th>
                                    <th style="width: 7%;">操作</th>
                                </tr>
                            </thead>
                            <tbody id="tbody_items">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <datalist id="itemList">
                @if (!empty($items) && count($items) > 0)
                    @foreach ($items as $key => $item)
                        <option value="{{ $item->ICODE }}"
                            data-price="{{ $item->IUNIT }}"data-cost="{{ $item->ISOUR }}">
                            {{ $item->INAME }}</option>
                    @endforeach
                @endif
            </datalist>
            <input id="input_details" name="details" type="hidden">
            <button id="btn_confirm" class="btn btn-primary btn-sm mt-2" type="submit">確認</button>
        </form>
    </div>
@stop
@section('script')
    <script type="text/javascript">
        //去空白
        $(function() {
            $('input[type="text"]').change(function() {
                this.value = $.trim(this.value);
            });
        });
        //確認
        $("#btn_confirm").click(function() {
            var itemList = new Array();
            $("#tbody_items>tr").each(function(index, element) {
                var id = $(element).data().id;
                var item_no = $(element).find("input.item_no").val();
                var item_qty = $(element).find("input.item_qty").val();
                var item_price = $(element).find("input.item_price").val();
                var item_remark = $(element).find("input.item_remark").val();
                var item = {
                    id: id,
                    item_no: item_no,
                    item_qty: item_qty,
                    item_price: item_price,
                    item_remark: item_remark,
                };
                itemList.push(item);
            });
            $("#input_details").val(JSON.stringify(itemList));
        });
        //建立商品明細
        $("#btn_create").click(function() {
            CreateItemDOM();
        });
        //建立商品明細DOM
        function CreateItemDOM(id = '', item_no = '', item_name = '', item_qty = '1', item_price = '', item_remark = '',
            item_cost = '') {
            var dom = '';
            dom += '<tr ' + (IsNullOrEmpty(id) ? 'class="table-danger"' : '') + ' data-id="' + id + '">';
            //料號
            dom +=
                '<td><input class="form-control form-control-sm item_no" type="text" list="itemList" onchange="SetItemDetail($(this))" value="' +
                item_no +
                '" required></td>';
            //品名
            dom +=
                '<td><span class="item_name">' + item_name + '</span></td>';
            //數量
            dom +=
                '<td><input class="form-control form-control-sm item_qty" type="number" min="0" max="65535" step="1" value="' +
                item_qty +
                '" required></td>';
            //售價
            dom +=
                '<td><input class="form-control form-control-sm item_price" type="number" step="0.01" value="' +
                item_price +
                '" required></td>';
            //成本
            dom +=
                '<td><span class="item_cost">' + item_cost + '</span></td>';
            //備註
            dom +=
                '<td><input class="form-control form-control-sm item_remark" type="text" maxlength="100" value="' +
                item_remark +
                '"></td>';
            //操作
            dom +=
                '<td><button class="btn btn-danger btn-sm" type="button"onclick="ItemDelete(this)"><i class="bi bi-trash"></i>刪除</button></td>';
            dom += '</tr>';

            $("#tbody_items").append(dom);
            $("#tbody_items>tr:last-child>td:eq(0)>input").focus();
        }
        //帶入料號資料
        function SetItemDetail(obj) {
            var status = 0;
            var item_no = obj.val();
            var item_name = '';
            var item_price = 0;
            var item_cost = 0;
            if (IsNullOrEmpty(item_no)) {
                status = 0;
                item_name = '請輸入料號';
            } else {
                var item = $('#itemList>option[value="' + item_no + '"]');
                if (item.length == 0) {
                    status = 1;
                    item_name = '找不到該料號';
                } else {
                    status = 2;
                    item_name = item.text();
                    item_price = Math.round(item.data().price * 1.05);
                    item_cost = Math.round(item.data().cost);
                }
            }
            var tr = obj.closest('tr');
            $(tr).find(".item_name").text(item_name);
            $(tr).find(".item_price").val(item_price);
            $(tr).find(".item_cost").text(item_cost);
            switch (status) {
                case 0:
                case 1:
                    tr.addClass('table-danger');
                    break;
                case 2:
                    tr.removeClass('table-danger');
                    break;
            }
        };
        //明細刪除按鈕
        function ItemDelete(obj) {
            if ($("#tbody_items>tr").length == 1) {
                alert('最少輸入一筆商品明細');
            } else {
                $(obj).closest('tr').remove();
            }
        }
        //帶入商品細項
        var items = @json($data->Details);
        $(items).each(function(i, e) {
            CreateItemDOM(e.id, e.item_no, e.item_name, e.item_qty, e.item_price, e.item_remark, e.item_cost);
        });
        //片語新增
        $("#btn_phrase_confirm").click(function() {
            var phrase = '';
            $("#modal_phrase .modal-body .form-group").each(function(index, element) {
                var input = $(element).find('input').val().replaceAll(/[:/]/g, "");
                if (!IsNullOrEmpty(input)) {
                    var text = $(element).find("label").text();
                    if (phrase != '') {
                        phrase += '/';
                    }
                    phrase += text + ':' + input;
                }
            });
            if (phrase.length > 200) {
                alert('文字內容過長');
            } else {
                $("#input_remark").text(phrase);
            }
        });
        //將備註1的內容帶入片語
        $('#modal_phrase').on('shown.bs.modal', function() {
            var remark = $("#input_remark").text();
            if (IsNullOrEmpty(remark)) {
                $("#modal_phrase input").val('');
            } else {
                var datas = remark.split('/');
                $(datas).each(function(index, element) {
                    data = element.split(':');
                    if (data.length >= 2) {
                        var text = data[0];
                        var value = data[1];
                        var label = $('#modal_phrase label:contains("' + text + '")');
                        $(label).closest('.form-group').find('input').val(value);
                    }
                })
            }
        })
    </script>
@stop
