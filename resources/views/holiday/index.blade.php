@extends('layouts.base')
@section('title')
    國定假日設定
@stop
@section('content')
    <div class="container-fluid">
        <div class="form-row">
            <div class="col-auto">
                <a class="btn btn-info" href="{{ route('holiday.update') }}">更新資料</a>
            </div>
        </div>
        <div class="form-row mt-2">
            <div class="col">
                <div class="FrozenTable" style="max-height: 87vh;">
                    <table class="table table-bordered table-hover table-filter">
                        <thead>
                            <tr>
                                <th class="filter-col" style="width: 6%;">西元年</th>
                                <th style="width: 8%;">日期</th>
                                <th style="width: 15%;">節日</th>
                                <th class="filter-col" style="width: 7%;">是否放假</th>
                                <th class="filter-col" style="width: 15%;">周末假期等</th>
                                <th>備註</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $item)
                                <tr>
                                    <td>{{ $item->year }}</td>
                                    <td>{{ $item->date }}</td>
                                    <td>{{ $item->name }}</td>
                                    <td>{{ $item->isholiday == 1 ? '✔' : '' }}</td>
                                    <td>{{ $item->holidaycategory }}</td>
                                    <td class="text-left">{{ $item->description }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop
