@extends('layouts.base')
@section('title')
    預購總表
@stop
@section('css')
    <style type="text/css">
        table.inner-table>thead>tr>th:nth-child(odd),
        table.inner-table>tbody>tr>td:nth-child(odd) {
            width: 150px;
        }
    </style>
@endsection
@section('content')
    <!-- Modal 統計 -->
    <div id="modal_total" class="modal fade">
        <div class="modal-dialog modal-xl modal-dialog-centered" style="max-width: 98vw;">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="modal-title">
                        <h1>統計</h1>
                    </div>
                    <button type="button" class="close" data-dismiss="modal">
                        <span>&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-row">
                        <div class="col">
                            <div class="FrozenTable" style="max-height: 71vh;">
                                <table class="table table-bordered sortable table-filter">
                                    <thead>
                                        <tr>
                                            <th>料號</th>
                                            <th>品名</th>
                                            <th>數量</th>
                                        </tr>
                                    </thead>
                                    <tbody class="statistics">
                                        @foreach ($statistics as $item_no => $item)
                                            <tr>
                                                <td>{{ $item_no }}</td>
                                                <td class="text-left">{{ $item['name'] }}</td>
                                                <td>{{ number_format($item['qty']) }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">關閉</button>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="form-row">
            <div class="col text-success">
                ※排除已上車
            </div>
            <div class="col"></div>
            <div class="col-auto">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal_total">
                    <i class="bi bi-bar-chart"></i>統計
                </button>
            </div>
        </div>
        <div class="form-row mt-2">
            <div class="col">
                <div class="FrozenTable" style="max-height: 88vh;">
                    <table class="table table-bordered table-hover table-filter sortable">
                        <thead>
                            <tr>
                                <th style="width: 4%;">序</th>
                                <th>原始訂單編號</th>
                                <th style="width: 10%;">訂單單號</th>
                                <th class="filter-col">客戶簡稱</th>
                                <th class="p-0">
                                    <table class="inner-table">
                                        <thead>
                                            <tr>
                                                <th>料號</th>
                                                <th>品名</th>
                                                <th>數量</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $idx = 1; ?>
                            @foreach ($data as $item)
                                <tr>
                                    <td>{{ $idx++ }}</td>
                                    <td class="original_no_font">{{ $item['original_no'] }}</td>
                                    <td>{{ $item['order_no'] }}</td>
                                    <td>{{ $item['cust_name'] }}</td>
                                    <td class="p-0">
                                        <table class="inner-table">
                                            <tbody>
                                                @foreach ($item['items'] as $item)
                                                    <tr>
                                                        <td>{{ $item['item_no'] }}</td>
                                                        <td class="text-left">{{ $item['item_name'] }}</td>
                                                        <td>{{ number_format($item['qty']) }}</td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop
