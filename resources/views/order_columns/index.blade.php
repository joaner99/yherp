@extends('layouts.base')
@section('title')
    網路訂單欄位設定
@stop
@section('css')
    <style type="text/css">
        .error-col {
            background-color: #dc3545 !important;
            color: #fff !important;
        }
    </style>
@endsection
@section('content')
    {{-- Modal 更新匯出訂單欄位 --}}
    <div id="modal_upload" class="modal">
        <div class="modal-dialog modal-xl modal-dialog-centered">
            <div class="modal-content">
                <form action="{{ route('order_columns.update') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title">更新匯出訂單欄位</h5>
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">來源</span>
                            </div>
                            <select class="custom-select col-2" id="input_type" name="type">
                                <option value="1">蝦皮</option>
                                <option value="2">Shopline</option>
                                <option value="3">BV SHOP</option>
                            </select>
                            <div class="custom-file">
                                <input class="custom-file-input" id="input_file" name="file" type="file"
                                    accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,.xls"
                                    required>
                                <label class="custom-file-label" for="input_file">請選擇檔案</label>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button id="btn_import" type="submit" class="btn btn-primary">確認</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">取消</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="form-row">
            <div class="col-auto">
                <button class="btn btn-success" type="button" data-toggle="modal" data-target="#modal_upload">
                    更新匯出訂單欄位
                </button>
            </div>
        </div>
        <div class="form-row mt-2">
            <div class="col">
                <form action="{{ route('order_columns.update_idx') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="FrozenTable" style="max-height: 87vh; font-size: 0.9rem;">
                        <table id="table_data" class="table table-bordered">
                            <caption><button class="btn btn-info" type="submit">更新對應欄位</button></caption>
                            <thead>
                                <tr>
                                    <th style="width: 4%;">id</th>
                                    <th style="width: 12%;">key</th>
                                    <th style="width: 15%;">備註</th>
                                    <th>蝦皮對應欄位</th>
                                    <th>Shopline對應欄位</th>
                                    <th>BV SHOP對應欄位</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($data as $item)
                                    <tr>
                                        <td>{{ $item->id }}</td>
                                        <td>{{ $item->key }}</td>
                                        <td class="text-left">
                                            <input name="data[{{ $item->id }}][remark]" class="form-control"
                                                type="text" value="{{ $item->remark }}">
                                        </td>
                                        <td class="text-left">
                                            <input name="data[{{ $item->id }}][shopee_idx]"
                                                class="form-control {{ $item->shopee_status === false ? 'error-col' : '' }}"
                                                type="text" value="{{ $item->shopee_idx }}">
                                        </td>
                                        <td class="text-left">
                                            <input name="data[{{ $item->id }}][shopline_idx]"
                                                class="form-control {{ $item->shopline_status === false ? 'error-col' : '' }}"
                                                type="text" value="{{ $item->shopline_idx }}">
                                        </td>
                                        <td class="text-left">
                                            <input name="data[{{ $item->id }}][bvshop_idx]"
                                                class="form-control {{ $item->bvshop_status === false ? 'error-col' : '' }}"
                                                type="text" value="{{ $item->bvshop_idx }}">
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop
@section('script')
    <script type="text/javascript">
        //上傳檔案後顯示文件名稱
        $('#input_file').change(function(e) {
            var fileName = e.target.files[0].name;
            $(this).next('.custom-file-label').html(fileName);
        });
    </script>
@stop
