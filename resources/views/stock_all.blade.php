@extends('layouts.base')
@section('title')
    庫存總表
@stop
@section('css')
    <style type="text/css">
        .group {
            flex-shrink: 0;
            flex-basis: 50px;
        }

        /*倉庫明細-倉別*/
        table.inner-table>thead>tr>th:nth-child(1),
        table.inner-table>tbody>tr>td:nth-child(1) {
            width: 100px;
        }
    </style>
@endsection
@section('content')
    <div class="container-fluid">
        @if (empty($data))
            <div class="alert alert-danger">
                找不到資料
            </div>
        @else
            @role('admin')
                <div class="form-row">
                    <div class="col">
                        <div id="chart"></div>
                    </div>
                    <div class="col-3">
                        <form action="{{ route('stockAll') }}" method="GET">
                            <div class="form-row">
                                <div class="col">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">指定庫存金額日期</span>
                                        </div>
                                        <input id="input_date" name="date" class="form-control" type="date"
                                            value="{{ old('date') }}">
                                        <div class="input-group-append">
                                            <button class="btn btn-info" type="submit">查詢</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <div class="form-row mt-2">
                            <div class="col">
                                <div class="FrozenTable" style="max-height: 36vh;">
                                    <table class="table table-bordered table-hover sortable">
                                        <thead>
                                            <tr>
                                                <th>大類</th>
                                                <th>庫存金額</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($inventory_amt as $kind => $amt)
                                                <tr>
                                                    <td>{{ $kind }}</td>
                                                    <td>{{ number_format($amt) }}</td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endrole
            {{-- Modal 地板出貨倉 --}}
            <div id="modal_floor" class="modal">
                <div class="modal-dialog modal-dialog-centered" style="max-width: 95%;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">地板出貨倉</h5>
                            <button type="button" class="close" data-dismiss="modal">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="form-row">
                                <div class="col">
                                    <div class="FrozenTable" style="max-height: 74vh;">
                                        <table class="table table-bordered table-hover sortable">
                                            <thead>
                                                <tr>
                                                    <th>料號</th>
                                                    <th>品名</th>
                                                    <th>出貨倉庫存</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($floor_item as $item)
                                                    <tr>
                                                        <td>{{ $item->ICODE }}</td>
                                                        <td class="text-left">{{ $item->INAME }}</td>
                                                        <td>{{ number_format($item->SNUMB) }}</td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">關閉</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-row mt-2">
                <div class="col">
                    <button class="btn btn-info" type="button" data-toggle="modal"
                        data-target="#modal_floor">地板出貨倉</button>
                </div>
            </div>
            <div class="form-row mt-2">
                <div class="col">
                    <div class="FrozenTable" style="max-height: {{ Auth::user()->hasRole('admin') ? '44' : '90' }}vh;">
                        <?php $tableIndex = 0; ?>
                        <table class="table table-bordered table-hover sortable">
                            <thead>
                                <tr>
                                    <th style="width: 5%;">序</th>
                                    <th style="width: 10%;">料號</th>
                                    <th>儲位</th>
                                    <th style="width: 40%;">品名</th>
                                    <th>總庫存</th>
                                    <th class="p-0">
                                        <table class="inner-table">
                                            <thead>
                                                <tr>
                                                    <th>倉別</th>
                                                    <th>數量</th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($data as $key => $value)
                                    <tr>
                                        <td>{{ ++$tableIndex }}</td>
                                        <td>{{ $key }}</td>
                                        <td>
                                            <div class="d-flex flex-row flex-wrap justify-content-center">
                                                @if (!empty($value['Storage']))
                                                    @foreach ($value['Storage'] as $storage)
                                                        <span class="group">【{{ $storage }}】</span>
                                                    @endforeach
                                                @endif
                                            </div>
                                        </td>
                                        <td class="text-left">{{ $value['Name'] }}</td>
                                        <td>{{ $value['TotalStock'] }}</td>
                                        <td class="p-0">
                                            <table class="inner-table">
                                                <tbody>
                                                    @foreach ($value['Stock'] as $stockName => $stockNum)
                                                        <tr>
                                                            <td> {{ $stockName }}</td>
                                                            <td> {{ $stockNum }}</td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        @endif
    </div>
@stop
@section('script')
    <script type="text/javascript">
        $(document).ready(InitialDate());

        function InitialDate() {
            var today = new Date();

            if ($("#input_date").val() == '') {
                $("#input_date").val(DateToString(today));
            }
            $("#input_date").attr("max", DateToString(today));
        }
        var chart = c3.generate({
            bindto: "#chart",
            title: {
                text: "{{ old('date') }}產品大類庫存金額比例"
            },
            data: {
                columns: [
                    @foreach ($inventory_amt as $kind => $amt)
                        ['{{ $kind }}', {{ $amt }}],
                    @endforeach
                ],
                type: 'pie',
            },
            legend: {
                position: 'right'
            }
        });
    </script>
@stop
