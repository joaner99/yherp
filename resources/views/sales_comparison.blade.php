@extends('layouts.base')
@section('css')
    <style type="text/css">
        div[id^="div_filter"] input {
            height: auto;
        }
    </style>
@endsection
@section('title')
    商品成長比較表
@stop
@section('content')
    <div class="container-fluid">
        <form id="form_search" action="{{ route('sales_comparison') }}" method="get" class="p-1">
            <div class="form-row">
                <div class="form-group col-auto">
                    <label>
                        分組方式
                    </label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <select class="custom-select" id="select_group_type" name="group_type">
                                <option {{ empty(old('group_type')) ? 'selected' : '' }} value="">無</option>
                                <option {{ old('group_type') == 'month' ? 'selected' : '' }} value="month">月份</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group col-auto">
                    <label>
                        商品篩選
                    </label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <select class="custom-select" id="select_filter" name="filter">
                                <option {{ empty(old('filter')) ? 'selected' : '' }} value="0">無篩選</option>
                                <option {{ old('filter') == '1' ? 'selected' : '' }} value="1">料號</option>
                                <option {{ old('filter') == '2' ? 'selected' : '' }} value="2">類別</option>
                                <option {{ old('filter') == '3' ? 'selected' : '' }} value="3">客戶</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div id="div_filter_id" class="form-group col-auto"
                    style="{{ old('filter') == '1' ? '' : 'display: none;' }}">
                    <label>
                        料號篩選
                    </label>
                    <div class="input-group">
                        <input id="input_id" name="id" list="idList" class="form-control" type="text"
                            value="{{ old('id') }}" autocomplete="off" placeholder="空白則搜尋全部商品">
                        <datalist id="idList">
                            @if (!empty($itemList))
                                @foreach ($itemList as $item)
                                    <option value="{{ $item->item_no }}">{{ $item->item_name }}</option>
                                @endforeach
                            @endif
                        </datalist>
                    </div>
                </div>
                <div id="div_filter_type" class="form-group col-auto"
                    style="{{ old('filter') == '2' ? '' : 'display: none;' }}">
                    <label>
                        大類篩選
                    </label>
                    <div class="input-group">
                        <input id="input_type" name="type" list="typeList" class="form-control" type="text"
                            value="{{ old('type') }}" autocomplete="off" placeholder="空白則搜尋全部商品">
                        <datalist id="typeList">
                            @if (!empty($typeList))
                                @foreach ($typeList[1] as $value => $text)
                                    <option value="{{ $value }}">{{ $text }}</option>
                                @endforeach
                            @endif
                        </datalist>
                    </div>
                </div>
                <div id="div_filter_type2" class="form-group col-auto"
                    style="{{ old('filter') == '2' ? '' : 'display: none;' }}">
                    <label>
                        中類篩選
                    </label>
                    <div class="input-group">
                        <input id="input_type2" name="type2" list="typeList2" class="form-control" type="text"
                            value="{{ old('type2') }}" autocomplete="off" placeholder="空白則搜尋全部商品">
                        <datalist id="typeList2">
                            @if (!empty($typeList))
                                @foreach ($typeList[2] as $value => $text)
                                    <option value="{{ $value }}">{{ $text }}</option>
                                @endforeach
                            @endif
                        </datalist>
                    </div>
                </div>
                <div id="div_filter_type3" class="form-group col-auto"
                    style="{{ old('filter') == '2' ? '' : 'display: none;' }}">
                    <label>
                        小類篩選
                    </label>
                    <div class="input-group">
                        <input id="input_type3" name="type3" list="typeList3" class="form-control" type="text"
                            value="{{ old('type3') }}" autocomplete="off" placeholder="空白則搜尋全部商品">
                        <datalist id="typeList3">
                            @if (!empty($typeList))
                                @foreach ($typeList[3] as $value => $text)
                                    <option value="{{ $value }}">{{ $text }}</option>
                                @endforeach
                            @endif
                        </datalist>
                    </div>
                </div>
                <div id="div_filter_customer" class="form-group col-auto"
                    style="{{ old('filter') == '3' ? '' : 'display: none;' }}">
                    <label>
                        客戶篩選
                    </label>
                    <div class="input-group">
                        <input id="input_customer" name="customer" list="customerList" class="form-control" type="text"
                            value="{{ old('customer') }}" autocomplete="off" placeholder="空白則搜尋全部商品">
                        <datalist id="customerList">
                            @if (!empty($customerList))
                                @foreach ($customerList as $value)
                                    <option value="{{ $value->CCODE }}">{{ $value->CNAM2 }}</option>
                                @endforeach
                            @endif
                        </datalist>
                    </div>
                </div>
            </div>
            <div id="div_date" class="form-row" style="{{ empty(old('group_type')) ? '' : 'display: none;' }}">
                <div class=" form-group col-auto text-primary">
                    <label for="input_a_start_date">A區間起日</label>
                    <input id="input_a_start_date" name="a_start_date" class="form-control" type="date"
                        value="{{ old('a_start_date') }}" required>
                </div>
                <div class="form-group col-auto text-primary">
                    <label for="input_a_end_date">A區間訖日</label>
                    <input id="input_a_end_date" name="a_end_date" class="form-control" type="date"
                        value="{{ old('a_end_date') }}" required>
                </div>
                <div class="form-group col-auto text-success">
                    <label for="input_b_start_date">B區間起日</label>
                    <input id="input_b_start_date" name="b_start_date" class="form-control" type="date"
                        value="{{ old('b_start_date') }}" required>
                </div>
                <div class="form-group col-auto text-success">
                    <label for="input_b_end_date">B區間訖日</label>
                    <input id="input_b_end_date" name="b_end_date" class="form-control" type="date"
                        value="{{ old('b_end_date') }}" required>
                </div>
            </div>
            <div id="div_month" class="form-row" style="{{ old('group_type') == 'month' ? '' : 'display: none;' }}">
                <div class="form-group col-lg-2 col-sm-4 text-primary">
                    <label for="input_a_year">A區間年份</label>
                    <input id="input_a_year" name="a_year" class="form-control" type="number"
                        value="{{ old('a_year') }}" min="1" max="2999" required>
                </div>
                <div class="form-group col-lg-2 col-sm-4 text-success">
                    <label for="input_b_year">B區間年份</label>
                    <input id="input_b_year" name="b_year" class="form-control" type="number"
                        value="{{ old('b_year') }}" min="1" max="2999" required>
                </div>
            </div>
            <button class="btn btn-primary btn-sm" type="submit">查詢</button>
        </form>
        @switch($groupType)
            @case('month')
                <div class="form-row mt-2">
                    <div class="col">
                        <div class="FrozenTable" style="max-height: 65vh;">
                            <table id="table_data" class="table table-bordered table-hover table-striped sortable">
                                <thead>
                                    <tr>
                                        <th scope="col" style="width: 5%;">序</th>
                                        <th scope="col" style="width: 5%;">月份</th>
                                        <th scope="col">A區間<br>時間</th>
                                        @can('sales_comparison')
                                            <th scope="col" style="width: 7%;">A區間<br>總營業額</th>
                                            <th scope="col" style="width: 7%;">A區間<br>總成本</th>
                                            <th scope="col" style="width: 7%;">A區間<br>總淨利</th>
                                        @endcan
                                        <th scope="col" style="width: 7%;">A區間<br>總數量</th>
                                        <th scope="col">B區間<br>時間</th>
                                        @can('sales_comparison')
                                            <th scope="col" style="width: 7%;">B區間<br>總營業額</th>
                                            <th scope="col" style="width: 7%;">B區間<br>總成本</th>
                                            <th scope="col" style="width: 7%;">B區間<br>總淨利</th>
                                        @endcan
                                        <th scope="col" style="width: 7%;">B區間<br>總數量</th>
                                        @can('sales_comparison')
                                            <th scope="col" style="width: 8%;">總營業額差<br>(A-B)</th>
                                            <th scope="col" style="width: 6%;">營業額<br>成長率</th>
                                        @endcan
                                        <th scope="col" style="width: 6%;">總數差<br>(A-B)</th>
                                        <th scope="col" style="width: 6%;">數量<br>成長率</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $index = 1; ?>
                                    @foreach ($data as $month => $value)
                                        <tr>
                                            <td>{{ $index++ }}</td>
                                            <td>{{ $month }}</td>
                                            <td>{{ $aRange }}</td>
                                            @can('sales_comparison')
                                                <td>{{ $value['aTotal'] }}</td>
                                                <td>{{ $value['aCost'] }}</td>
                                                <td>{{ $value['aNP'] }}</td>
                                            @endcan
                                            <td>{{ $value['aNum'] }}</td>
                                            <td>{{ $bRange }}</td>
                                            @can('sales_comparison')
                                                <td>{{ $value['bTotal'] }}</td>
                                                <td>{{ $value['bCost'] }}</td>
                                                <td>{{ $value['bNP'] }}</td>
                                            @endcan
                                            <td>{{ $value['bNum'] }}</td>
                                            @can('sales_comparison')
                                                <td>{{ $value['diff'] }}</td>
                                                <td>{{ $value['diffPercent'] }}</td>
                                            @endcan
                                            <td>{{ $value['diffNum'] }}</td>
                                            <td>{{ $value['diffNumPercent'] }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            @break

            @default
                <div class="form-row mt-2">
                    <div class="col">
                        <div class="FrozenTable" style="max-height: 65vh; font-size: 0.8rem;">
                            <table id="table_data" class="table table-bordered table-hover table-striped sortable"
                                style="width: 110vw;">
                                <thead>
                                    <tr>
                                        <th scope="col" style="width: 3%;">序</th>
                                        <th scope="col" style="width: 5%;">料號</th>
                                        <th scope="col">產品名</th>
                                        <th scope="col" style="width: 5%;">類別</th>
                                        <th scope="col" style="width: 4%;">A區間<br>時間</th>
                                        @can('sales_comparison')
                                            <th scope="col" style="width: 5%;">A區間<br>總營業額</th>
                                            <th scope="col" style="width: 5%;">A區間<br>總成本</th>
                                            <th scope="col" style="width: 5%;">A區間<br>總淨利</th>
                                        @endcan
                                        <th scope="col" style="width: 5%;">A區間<br>總數量</th>
                                        <th scope="col" style="width: 4%;">A區間<br>總數量占比</th>
                                        <th scope="col" style="width: 4%;">B區間<br>時間</th>
                                        @can('sales_comparison')
                                            <th scope="col" style="width: 5%;">B區間<br>總營業額</th>
                                            <th scope="col" style="width: 5%;">B區間<br>總成本</th>
                                            <th scope="col" style="width: 5%;">B區間<br>總淨利</th>
                                        @endcan
                                        <th scope="col" style="width: 5%;">B區間<br>總數量</th>
                                        <th scope="col" style="width: 4%;">B區間<br>總數量占比</th>
                                        @can('sales_comparison')
                                            <th scope="col" style="width: 7%;">總營業額差<br>(A-B)</th>
                                            <th scope="col" style="width: 5%;">營業額<br>成長率</th>
                                        @endcan
                                        <th scope="col" style="width: 5%;">總數差<br>(A-B)</th>
                                        <th scope="col" style="width: 5%;">數量<br>成長率</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $index = 1; ?>
                                    @foreach ($data as $itemNo => $value)
                                        <tr>
                                            <td>{{ $index++ }}</td>
                                            <td>{{ $itemNo }}</td>
                                            <td>{{ $value['name'] }}</td>
                                            <td>{{ $value['type'] }}</td>
                                            <td>{{ $aRange }}</td>
                                            @can('sales_comparison')
                                                <td>{{ $value['aTotal'] }}</td>
                                                <td>{{ $value['aCost'] }}</td>
                                                <td>{{ $value['aNP'] }}</td>
                                            @endcan
                                            <td>{{ $value['aNum'] }}</td>
                                            <td>{{ $value['aProportion'] }}</td>
                                            <td>{{ $bRange }}</td>
                                            @can('sales_comparison')
                                                <td>{{ $value['bTotal'] }}</td>
                                                <td>{{ $value['bCost'] }}</td>
                                                <td>{{ $value['bNP'] }}</td>
                                            @endcan
                                            <td>{{ $value['bNum'] }}</td>
                                            <td>{{ $value['bProportion'] }}</td>
                                            @can('sales_comparison')
                                                <td>{{ $value['diff'] }}</td>
                                                <td>{{ $value['diffPercent'] }}</td>
                                            @endcan
                                            <td>{{ $value['diffNum'] }}</td>
                                            <td>{{ $value['diffNumPercent'] }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
        @endswitch
        <div class="form-row mt-2">
            <div class="col">
                @switch($groupType)
                    @case('month')
                        <form action="{{ route('export_table') }}" method="post">
                            @csrf
                            <input name="export_name" type="hidden" value="商品成長月份比較表">
                            <input name="export_data" type="hidden">
                            <button id="btn_export" class="btn btn-success" type="submit" export-target="#table_data">
                                <i class="bi bi-file-earmark-excel"></i>匯出Excel
                            </button>
                        </form>
                    @break

                    @default
                        <form action="{{ route('export_table') }}" method="post">
                            @csrf
                            <input name="export_name" type="hidden" value="商品成長比較表">
                            <input name="export_data" type="hidden">
                            <button id="btn_export" class="btn btn-success btn-sm" type="submit" export-target="#table_data">
                                <i class="bi bi-file-earmark-excel"></i>匯出Excel
                            </button>
                        </form>
                @endswitch
            </div>
        </div>
    </div>
@stop
@section('script')
    <script type="text/javascript">
        $(document).ready(InitialDate());

        //匯出檔名修正
        $("#btn_export").click(function() {
            var name = '';
            @switch($groupType)
                @case('month')
                var a_year = $("#input_a_year").val();
                var b_year = $("#input_b_year").val();
                name = a_year + '與' + b_year + '商品成長月份比較表';
                @break

                @default
                var a_s_date = $("#input_a_start_date").val();
                var a_e_date = $("#input_a_end_date").val();
                var b_s_date = $("#input_b_start_date").val();
                var b_e_date = $("#input_b_end_date").val();
                name = a_s_date + '到' + a_e_date + '與' + b_s_date + '到' + b_e_date + '商品成長比較表';
            @endswitch
            $("input[name='export_name']").val(name);
        });

        //篩選
        $("#select_filter").on('change', function() {
            $("div[id^='div_filter'] input").val('');
            $("div[id^='div_filter'] input").attr('disabled', true);
            $("div[id^='div_filter']").hide();
            switch (this.value) {
                case "1":
                    $('#div_filter_id').show();
                    $('#div_filter_id input').attr('disabled', false);
                    break;
                case "2":
                    $('#div_filter_type,#div_filter_type2,#div_filter_type3').show();
                    $('#div_filter_type input,#div_filter_type2 input,#div_filter_type3 input').attr('disabled',
                        false);
                    break;
                case "3":
                    $('#div_filter_customer').show();
                    $('#div_filter_customer input').attr('disabled', false);
                    break;
                default:
                    break;
            }
        });

        //分組方式
        $("#select_group_type").on('change', function() {
            switch (this.value) {
                case "month": //月份
                    $("#div_date").hide();
                    $("#div_month").show();
                    break;
                default: //無
                    $("#div_date").show();
                    $("#div_month").hide();
                    break;
            }
        });

        //查詢，移除多餘input
        $("#form_search").submit(function() {
            switch ($("#select_group_type").val()) {
                case "month":
                    $(this).children('#div_date').remove();
                    break;
                default:
                    $(this).children('#div_month').remove();
                    break;
            }
        });

        //時間初始化
        function InitialDate() {
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            var yyyy = today.getFullYear();
            if (dd < 10) {
                dd = '0' + dd
            }
            if (mm < 10) {
                mm = '0' + mm
            }

            today = yyyy + '-' + mm + '-' + dd;
            if ($("#input_a_start_date").val() == '') {
                $("#input_a_start_date").val(today);
            }
            if ($("#input_a_end_date").val() == '') {
                $("#input_a_end_date").val(today);
            }
            if ($("#input_b_start_date").val() == '') {
                $("#input_b_start_date").val(today);
            }
            if ($("#input_b_end_date").val() == '') {
                $("#input_b_end_date").val(today);
            }
            if ($("#input_a_year").val() == '') {
                $("#input_a_year").val(yyyy);
            }
            if ($("#input_b_year").val() == '') {
                $("#input_b_year").val(yyyy);
            }
            $("#input_a_start_date").attr("max", today);
            $("#input_a_end_date").attr("max", today);
            $("#input_b_start_date").attr("max", today);
            $("#input_b_end_date").attr("max", today);
        }
    </script>
@stop
