@extends('layouts.base')
@section('title')
    銷貨作業-編輯
@stop
@section('css')
    <style type="text/css">
    </style>
@endsection
@section('content')
    <div class="container-fluid">
        @if ($errors->any())
            <div class="form-row mb-2">
                <div class="col-auto">
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        @endif
        <div class="form-row">
            <div class="col-auto">
                <a class="btn btn-secondary" href="{{ URL::previous() }}">返回清單</a>
            </div>
        </div>
        <hr>
        <form method="POST" action="{{ route('erp.sales.update', $data->id) }}" enctype="multipart/form-data">
            @method('PATCH')
            @csrf
            <div class="card mt-2">
                <h2 class="card-header text-info font-weight-bold">銷單主檔</h2>
                <div class="card-body p-2">
                    <div class="form-row">
                        <div class="col-auto">
                            <div class="form-group">
                                <label class="text-danger" for="input_sale_date">銷貨日期</label>
                                <input class="form-control" id="input_sale_date" type="date"
                                    value="{{ $data->sale_date }}" readonly>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card mt-2">
                <div class="card-header text-info">
                    <div class="form-row">
                        <div class="col">
                            <h2 class="font-weight-bold m-0">商品明細檔</h2>
                        </div>
                        <div class="col-auto">
                            <button class="btn btn-info" type="button" id="btn_create">
                                <i class="bi bi-plus-lg"></i>新增商品
                            </button>
                        </div>
                    </div>
                </div>
                <div class="card-body p-2">
                    <div class="form-row">
                        <div class="col-2">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">折扣(含稅)</span>
                                </div>
                                <input name="discount" class="form-control" type="number" step="1" min="0"
                                    value="{{ $discount }}" onchange="Calculate()">
                            </div>
                        </div>
                    </div>
                    <div class="form-row mt-2">
                        <div class="col">
                            <div class="FrozenTable" style="max-height: 40vh;">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th style="width: 12%;">*料號</th>
                                            <th>品名</th>
                                            <th style="width: 7%;">總庫存</th>
                                            <th style="width: 7%;">*數量</th>
                                            <th style="width: 10%;">*單價(含稅)</th>
                                            <th style="width: 10%;">小計(含稅)</th>
                                            <th style="width: 7%;">操作</th>
                                        </tr>
                                    </thead>
                                    <tbody id="tbody_items">
                                        @foreach ($data->Details as $d)
                                            @if ($d->item_no == 'XX126')
                                                @continue
                                            @endif
                                            <tr>
                                                <td>
                                                    <input class="form-control form-control-sm item_no" type="text"
                                                        list="itemList" required onchange="SetItemDetail($(this))"
                                                        value="{{ $d->item_no }}">
                                                </td>
                                                <td class="text-left">
                                                    <span class="item_name">{{ $d->item_name }}</span>
                                                </td>
                                                <td>
                                                    <span class="item_stock">{{ $d->item_stock }}</span>
                                                </td>
                                                <td>
                                                    <input class="form-control form-control-sm item_qty" type="number"
                                                        min="1" max="65535" step="1" required
                                                        onchange="Calculate()" value="{{ $d->item_qty }}">
                                                </td>
                                                <td>
                                                    <input class="form-control form-control-sm unit_price_tax"
                                                        type="number" min="1" step="0.01" required
                                                        onchange="Calculate()" value="{{ round($d->unit_price_tax, 2) }}">
                                                </td>
                                                <td>
                                                    <span class="total_price_tax"></span>
                                                </td>
                                                <td>
                                                    <button class="btn btn-danger btn-sm"
                                                        type="button"onclick="ItemDelete(this)"><i
                                                            class="bi bi-trash"></i>刪除</button>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="form-row mt-2">
                        <div class="col">
                            <ul class="list-group list-group-horizontal">
                                <li class="list-group-item list-group-item-primary">合計</li>
                                <li id="li_total" class="list-group-item">0</li>
                                <li class="list-group-item list-group-item-primary">營業稅</li>
                                <li id="li_tax" class="list-group-item">0</li>
                                <li class="list-group-item list-group-item-primary">總計</li>
                                <li id="li_total_tax" class="list-group-item">0</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <datalist id="itemList">
                @if (!empty($items) && count($items) > 0)
                    @foreach ($items as $key => $item)
                        <option value="{{ $item->item_no }}"
                            data-stock="{{ empty($item->ItemPlaceD) ? 0 : $item->ItemPlaceD->sum('qty') }}">
                            {{ $item->item_name }}
                        </option>
                    @endforeach
                @endif
            </datalist>
            <input name="company" type="hidden" value="{{ $company }}">
            <input id="input_details" name="details" type="hidden">
            <button id="btn_confirm" class="btn btn-primary mt-2" type="submit">確認</button>
        </form>
    </div>
@stop
@section('script')
    <script type="text/javascript">
        $(document).ready(function() {
            InitialDate();
            Calculate();
            if ($('#tbody_items>tr').length == 0) {
                CreateItemDOM();
            }
        });

        function InitialDate() {
            var today = new Date();
            if ($("#input_sale_date").val() == '') {
                $("#input_sale_date").val(DateToString(today));
            }
        }
        //去空白
        $(function() {
            $('input[type="text"]').change(function() {
                this.value = $.trim(this.value);
            });
        });
        //確認
        $("#btn_confirm").click(function() {
            var itemList = new Array();
            var error = false;
            var msg = '';
            //折扣驗證
            if ($('input[name="discount"]').val() < 0) {
                alert('折扣不得為負數');
                return false;
            }
            //總計驗證
            var total_tax = $('#li_total_tax').text();
            if (total_tax < 0) {
                alert('總計不得為負數');
                return false;
            }
            //明細驗證
            $("#tbody_items>tr").each(function(index, element) {
                var item_no = $(element).find("input.item_no").val();
                var item_name = $(element).find('.item_name').text().trim();
                var item_qty = Number($(element).find("input.item_qty").val());
                var item_stock = Number($(element).find('.item_stock').text().trim());
                var unit_price_tax = $(element).find("input.unit_price_tax").val();
                var total_price_tax = CustomerToNumber($(element).find('.total_price_tax').text().trim());
                if (unit_price_tax < 0) {
                    msg = '單價不得為負數';
                    error = true;
                    return false;
                } else if (item_qty < 1) {
                    msg = '數量不得小於1';
                    error = true;
                    return false;
                }
                //  else if (item_qty > item_stock) {
                //     msg = '銷售數量不得大於庫存';
                //     error = true;
                //     return false;
                // }
                var item = {
                    item_no: item_no,
                    item_name: item_name,
                    item_qty: item_qty,
                    unit_price_tax: unit_price_tax,
                    total_price_tax: total_price_tax,
                };
                itemList.push(item);
            });
            if (error) {
                alert(msg);
                return false;
            }
            $("#input_details").val(JSON.stringify(itemList));
        });
        //建立商品明細
        $("#btn_create").click(CreateItemDOM);
        //建立商品明細DOM
        function CreateItemDOM() {
            var dom = '';
            dom += '<tr class="table-danger">';
            //料號
            dom +=
                '<td><input class="form-control form-control-sm item_no" type="text" list="itemList" onchange="SetItemDetail($(this))" required></td>';
            //品名
            dom +=
                '<td class="text-left"><span class="item_name">請輸入料號</span></td>';
            //總庫存
            dom +=
                '<td><span class="item_stock">0</span></td>';
            //數量
            dom +=
                '<td><input class="form-control form-control-sm item_qty" type="number" min="1" max="65535" step="1" value="1" required onchange="Calculate()"></td>';
            //單價
            dom +=
                '<td><input class="form-control form-control-sm unit_price_tax" type="number" min="1" step="0.01" value="0" required onchange="Calculate()"></td>';
            //小計
            dom +=
                '<td><span class="total_price_tax">0</span></td>';
            //操作
            dom +=
                '<td><button class="btn btn-danger btn-sm" type="button"onclick="ItemDelete(this)"><i class="bi bi-trash"></i>刪除</button></td>';
            dom += '</tr>';

            $("#tbody_items").append(dom);
            $("#tbody_items>tr:last-child>td:eq(0)>input").focus();
        }
        //帶入料號資料
        function SetItemDetail(obj) {
            var status = 0;
            var item_no = obj.val();
            var item_name = '';
            var item_stock = 0;
            if (IsNullOrEmpty(item_no)) {
                status = 0;
                item_name = '請輸入料號';
            } else {
                var item = $('#itemList>option[value="' + item_no + '"]');
                if (item.length == 0) {
                    status = 1;
                    item_name = '找不到該料號';
                } else {
                    status = 2;
                    item_name = item.text();
                    item_stock = item.data().stock;
                }
            }
            var tr = obj.closest('tr');
            $(tr).find(".item_name").text(item_name);
            $(tr).find(".item_stock").text(item_stock);
            switch (status) {
                case 0:
                case 1:
                    tr.addClass('table-danger');
                    break;
                case 2:
                    tr.removeClass('table-danger');
                    break;
            }
            Calculate();
        };
        //明細刪除按鈕
        function ItemDelete(obj) {
            if ($("#tbody_items>tr").length == 1) {
                alert('最少輸入一筆商品明細');
            } else {
                $(obj).closest('tr').remove();
                Calculate();
            }
        }
        //計算結果
        function Calculate() {
            var total_tax = 0;
            $('#tbody_items>tr').each(function(i, e) {
                var item_qty = $(e).find('.item_qty').val();
                var unit_price_tax = $(e).find('.unit_price_tax').val();
                var total_price_tax = item_qty * unit_price_tax;
                total_tax += total_price_tax;
                $(e).find('.total_price_tax').text(toThousands(total_price_tax));
            });
            var discount = $('input[name="discount"]').val();
            total_tax -= discount;
            var total = Math.round(total_tax / 1.05);
            var tax = total_tax - total;
            $('#li_total').text(toThousands(total));
            $('#li_tax').text(toThousands(tax));
            $('#li_total_tax').text(toThousands(total_tax));
        }
        //防止enter 提交
        $(document).on('keypress', 'form', function(e) {
            var code = e.keyCode || e.which;
            if (code == 13) {
                e.preventDefault();
                return false;
            }
        });
    </script>
@stop
