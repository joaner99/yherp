@extends('layouts.base')
@section('title')
    銷貨作業-清單
@stop
@section('css')
    <style type="text/css">
        table.inner-table>thead>tr>th:nth-child(1),
        table.inner-table>tbody>tr>td:nth-child(1) {
            width: 110px;
        }

        table.inner-table>thead>tr>th:nth-child(n+3),
        table.inner-table>tbody>tr>td:nth-child(n+3) {
            width: 100px;
        }
    </style>
@endsection
@section('content')
    <div class="container-fluid">
        <form action="{{ route('erp.sales.index') }}" method="GET">
            <div class="form-row">
                @hasrole('admin')
                    <div class="col-2">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <label class="input-group-text">公司</label>
                            </div>
                            <select class="custom-select" name="company">
                                @foreach ($companies as $c)
                                    <option value="{{ $c }}" {{ old('company') == $c ? 'selected' : '' }}>
                                        {{ $c }}</option>
                                @endforeach
                            </select>
                            <div class="input-group-append">
                                <button class="btn btn-info" type="submit">切換</button>
                            </div>
                        </div>
                    </div>
                @endrole
                <div class="col-6">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">查詢方式</span>
                        </div>
                        <select id="select_search_type" class="custom-select" name="search_type"
                            onchange="search_type_onchange(this)">
                            <option value="1">銷貨日期</option>
                        </select>
                        <div class="input-group-prepend" data-search_type="1">
                            <span class="input-group-text">起日</span>
                        </div>
                        <input id="input_s_date" name="s_date" class="form-control" data-search_type="1" type="date"
                            value="{{ old('s_date') }}">
                        <div class="input-group-prepend" data-search_type="1">
                            <span class="input-group-text">訖日</span>
                        </div>
                        <input id="input_e_date" name="e_date" class="form-control" data-search_type="1" type="date"
                            value="{{ old('e_date') }}">
                        <div class="input-group-append">
                            <button class="btn btn-info" type="submit">查詢</button>
                        </div>
                    </div>
                </div>
                <div class="col"></div>
                <div class="col-auto">
                    <a class="btn btn-info" href="{{ route('erp.sales.create') . '?company=' . $company }}">
                        <i class="bi bi-plus-lg"></i>新增
                    </a>
                </div>
            </div>
        </form>
        <div class="form-row mt-2">
            <div class="col">
                <div class="FrozenTable" style="max-height: 86vh; font-size: 0.85rem;">
                    <table class="table table-bordered table-filter sortable">
                        @if (!empty($company))
                            <caption>公司：{{ $company }}</caption>
                        @endif
                        <thead>
                            <tr>
                                <th style="width: 6%;">操作</th>
                                <th class="filter-col" style="width: 7%;">銷貨日期</th>
                                <th style="width: 8%;">銷貨單號</th>
                                <th>總計(含稅)</th>
                                <th class="p-0">
                                    <table class="inner-table">
                                        <thead>
                                            <tr>
                                                <th>料號</th>
                                                <th>名稱</th>
                                                <th>數量</th>
                                                <th>單價(含稅)</th>
                                                <th>小計(含稅)</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $item)
                                <tr>
                                    <td class="p-0">
                                        <div class="d-flex justify-content-center flex-column">
                                            <div class="my-1">
                                                <a class="btn btn-warning btn-sm"
                                                    href="{{ route('erp.sales.edit', $item->id) . '?company=' . $company }}">
                                                    <i class="bi bi-pencil-fill"></i>編輯
                                                </a>
                                            </div>
                                            <div class="my-1">
                                                <form method="POST"
                                                    action="{{ route('erp.sales.destroy', $item->id) . '?company=' . $company }}"
                                                    onsubmit="return confirm('確定要刪除?');">
                                                    @csrf
                                                    <input name="_method" type="hidden" value="DELETE">
                                                    <button class="btn btn-danger btn-sm">
                                                        <i class="bi bi-trash"></i>刪除
                                                    </button>
                                                </form>
                                            </div>
                                        </div>
                                    </td>
                                    <td>{{ $item->sale_date }}</td>
                                    <td>{{ $item->id }}</td>
                                    <td>{{ number_format($item->total_price_tax) }}</td>
                                    <td class="p-0">
                                        <table class="inner-table">
                                            <tbody>
                                                @foreach ($item->Details->sortBy('item_no') as $d)
                                                    <tr>
                                                        <td>{{ $d->item_no }}</td>
                                                        <td class="text-left">{{ $d->item_name }}</td>
                                                        <td>{{ number_format($d->item_qty) }}</td>
                                                        <td>{{ number_format($d->unit_price_tax) }}</td>
                                                        <td>{{ number_format($d->total_price_tax) }}</td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop
@section('script')
    <script type="text/javascript">
        $(document).ready(function() {
            InitialDate();
        });

        function InitialDate() {
            var today = new Date();
            if ($("#input_s_date").val() == '') {
                $("#input_s_date").val(DateToString(today));
            }
            if ($("#input_e_date").val() == '') {
                $("#input_e_date").val(DateToString(today));
            }
        }

        function search_type_onchange(obj) {
            var search_type = $(obj).val();
            $('[data-search_type]').each(function(i, e) {
                $(e).toggle($(e).data().search_type == search_type);
            });
        }
    </script>
@stop
