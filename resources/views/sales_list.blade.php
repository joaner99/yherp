@extends('layouts.base')
@section('title')
    {{ $title ?? '' }}
@stop
@section('content')
    <div class="container-fluid">
        <div class="FrozenTable" style="max-height: 90vh; font-size:0.85rem;">
            <table class="table table-bordered table-hover table-filter sortable">
                <thead>
                    <tr>
                        <th style="width: 3%;">出貨<br>順序</th>
                        <th>銷貨單號</th>
                        <th>訂單單號</th>
                        <th>原始訂單編號</th>
                        <th class="filter-col">平台名稱</th>
                        <th class="filter-col" style="width: 7%;">物流</th>
                        <th>物流單號</th>
                        <th>聯絡人</th>
                        <th>電話</th>
                        <th>備註1</th>
                        <th>備註2</th>
                        <th>備註3</th>
                        <th>內部備註</th>
                        <th style="width: 4%;">明細</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i = 0; ?>
                    @if (!empty($data))
                        @foreach ($data as $value)
                            <tr>
                                <td>{{ $i = $i + 1 }}</td>
                                <td>{{ $value->PCOD1 }}</td>
                                <td>{{ $value->PCOD2 }}</td>
                                <td>{{ $value->PJONO }}</td>
                                <td>{{ $value->PPNAM }}</td>
                                <td>{{ $value->TransportName }}</td>
                                <td>{{ $value->ConsignTran }}</td>
                                <td>{{ $value->PCMAN }}</td>
                                <td>{{ $value->PTELE }}</td>
                                <td>{{ $value->PBAK1 }}</td>
                                <td>{{ $value->PBAK2 }}</td>
                                <td>{{ $value->PBAK3 }}</td>
                                <td>{{ $value->InsideNote }}</td>
                                <td>
                                    <a href="{{ route('check_log_detail', $value->PCOD1) }}">明細</a>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@stop
