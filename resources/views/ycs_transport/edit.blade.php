@extends('layouts.base')
@section('title')
    音速物流總表-編輯訂單
@stop
@section('css')
    <style type="text/css">
        textarea {
            min-height: 100px;
        }
    </style>
@endsection
@section('content')
    <div class="container-fluid">
        @if ($errors->any())
            <div class="form-row mb-2">
                <div class="col-auto">
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        @endif
        <div class="form-row">
            <div class="col-auto">
                <a class="btn btn-secondary" href="{{ route('ycs_transport.index', ['date' => $date]) }}">回上頁</a>
            </div>
        </div>
        <hr>
        <form method="POST" action="{{ route('ycs_transport.update', $data->id) }}" style="font-size: 0.9rem;">
            @method('PATCH')
            @csrf
            <div class="form-row mt-2">
                <div class="col-lg-2">
                    <div class="form-group">
                        <label class="text-danger" for="input_settle_date">*上車日期</label>
                        <input class="form-control" id="input_settle_date" name="settle_date" type="date"
                            value="{{ $data->settle_date }}" required>
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="form-group">
                        <label class="text-danger" for="input_qty">*寄件件數</label>
                        <input class="form-control" id="input_qty" name="qty" type="number" value="{{ $data->qty }}"
                            min="0" max="255" required>
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="form-group">
                        <label class="text-danger" for="input_name">*尺寸</label>
                        <input class="form-control" id="input_size" name="size" type="number"
                            value="{{ $data->size }}" list="size_list" autocomplete="off" min="0" max="255"
                            required>
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="form-group">
                        <label class="text-danger" for="input_name">*重量</label>
                        <input class="form-control" id="input_weight" name="weight" type="number"
                            value="{{ $data->weight }}" min="0" max="255" required>
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="form-group">
                        <label for="input_name">品名</label>
                        <input class="form-control" id="input_name" name="name" type="text"
                            value="{{ $data->name }}" list="name_list" autocomplete="off">
                    </div>
                </div>
            </div>
            <div class="form-row mt-2">
                <div class="col-lg-2">
                    <div class="form-group">
                        <label class="text-danger" for="input_receiver">*收件者</label>
                        <input class="form-control" id="input_receiver" name="receiver" type="text"
                            value="{{ $data->receiver }}" required>
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="form-group">
                        <label class="text-danger" for="input_tel">*收件電話</label>
                        <input class="form-control" id="input_tel" name="tel" type="text" value="{{ $data->tel }}"
                            required>
                    </div>
                </div>
            </div>
            <div class="form-row mt-2">
                <div class="col-lg-4">
                    <div class="form-group">
                        <label class="text-danger" for="input_addr">*收件地址</label>
                        <input class="form-control" id="input_addr" name="addr" type="text"
                            value="{{ $data->addr }}" required>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="form-group">
                        <label for="input_addr">備註</label>
                        <input class="form-control" id="input_remark" name="remark" type="text"
                            value="{{ $data->remark }}">
                    </div>
                </div>
            </div>
            <hr>
            <div class="form-row mt-2">
                <div class="col-lg-2">
                    <div class="form-group">
                        <label for="input_id">流水號</label>
                        <input class="form-control" id="input_id" type="text" value="{{ $data->id }}" readonly>
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="form-group">
                        <label for="input_sale_no">銷貨單號</label>
                        <input class="form-control" id="input_sale_no" type="text" value="{{ $data->sale_no }}"
                            readonly>
                    </div>
                </div>
                @if (!empty($data->Posein))
                    <div class="col-lg-2">
                        <div class="form-group">
                            <label for="input_sale_no">總金額</label>
                            <input class="form-control" type="text" value="{{ number_format($data->Posein->PTOTA) }}"
                                readonly>
                        </div>
                    </div>
                @endif
            </div>
            @if (!empty($data->Posein))
                <div class="form-row mt-2">
                    <div class="col">
                        <div class="FrozenTable" style="max-height:50vh;">
                            <table class="table table-bordered table-sort">
                                <thead>
                                    <tr>
                                        <th>料號</th>
                                        <th>品名</th>
                                        <th>數量</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if (!empty($data->Posein->Histin))
                                        @foreach ($data->Posein->Histin as $detail)
                                            <tr>
                                                <td>{{ $detail->HICOD }}</td>
                                                <td>{{ $detail->HINAM }}</td>
                                                <td>{{ number_format($detail->HINUM) }}</td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="form-row mt-2">
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label>備註1</label>
                            <textarea class="form-control" readonly>{{ $data->Posein->PBAK1 }}</textarea>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label>備註2</label>
                            <textarea class="form-control" readonly>{{ $data->Posein->PBAK2 }}</textarea>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label>備註3</label>
                            <textarea class="form-control" readonly>{{ $data->Posein->PBAK3 }}</textarea>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label>內部備註</label>
                            <textarea class="form-control" readonly>{{ $data->Posein->InsideNote }}</textarea>
                        </div>
                    </div>
                </div>
            @endif
            <hr>
            <button class="btn btn-primary mb-2" type="submit">確認</button>
            <input name="date" value="{{ $date }}" type="hidden">
        </form>
    </div>
    <datalist id="size_list">
        <option>60</option>
        <option>90</option>
        <option>120</option>
        <option>150</option>
        <option>185</option>
    </datalist>
    <datalist id="name_list">
        @foreach ($item_names as $item_name)
            <option>{{ $item_name }}</option>
        @endforeach
    </datalist>
@stop
