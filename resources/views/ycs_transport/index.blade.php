@extends('layouts.base')
@section('title')
    音速物流總表
@stop
@section('css')
    <style type="text/css">
        .shipment {
            padding: 0 !important;
            font-size: 1.5rem;
        }

        .status_1 {
            background-color: #ffdf7e;
        }

        .status_2 {
            background-color: #86cfda;
        }

        .status_3 {
            background-color: #8fdba0;
        }
    </style>
@endsection
@section('content')
    {{-- Modal 上傳 --}}
    <div id="modal_upload" class="modal">
        <div class="modal-dialog modal-lg modal-dialog-centered">
            <div class="modal-content">
                <form action="{{ route('ycs_transport.import') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title">匯入訂單</h5>
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">上車日期</span>
                            </div>
                            <input id="input_settle_date" name="settle_date" class="form-control col-3" type="date"
                                required>
                            <div class="custom-file">
                                <input class="custom-file-input" id="input_file" name="file" type="file"
                                    accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel">
                                <label class="custom-file-label" for="input_file">請選擇檔案</label>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">確認</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">關閉</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="form-row">
            <div class="col-auto">
                <form action="{{ route('ycs_transport.index') }}" method="GET">
                    <div class="form-row">
                        <div class="form-group col-auto mb-2">
                            <label for="input_date">上車日期</label>
                            <input id="input_date" name="date" class="form-control" type="date"
                                value="{{ $date }}" required>
                        </div>
                    </div>
                    <button class="btn btn-primary">查詢</button>
                </form>
            </div>
            <div class="col"></div>
            <div class="col-auto">
                <div class="form-row">
                    <div class="col-auto">
                        <a class="btn btn-primary btn-sm" href="{{ asset('storage') . '/help/ycs.pdf' }}">
                            <i class="bi bi-info-circle"></i>說明文件
                        </a>
                    </div>
                    <div class="col-auto">
                        <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#modal_upload">
                            <i class="bi bi-file-earmark-excel"></i>匯入物流
                        </button>
                    </div>
                    <div class="col-auto">
                        <form method="POST" action="{{ route('ycs_transport.batch_delete') }}"
                            onsubmit="return confirm('確定要批次刪除?');">
                            @csrf
                            <input name="ids" type="hidden">
                            <button id="btn_batch_delete" class="btn btn-danger btn-sm">
                                <i class="bi bi-trash"></i>批次刪除
                            </button>
                        </form>
                    </div>
                    <div class="col-auto">
                        <form action="{{ route('ycs_transport.api') }}" method="POST">
                            @csrf
                            <input name="ids" type="hidden">
                            <input name="date" class="form-control" type="hidden" value="{{ $date }}">
                            <button id="btn_api" type="submit" class="btn btn-info btn-sm">
                                <i class="bi bi-cloud-upload"></i>Api上傳
                            </button>
                        </form>
                    </div>
                    <div class="col-auto">
                        <form action="{{ route('export_table') }}" method="post">
                            @csrf
                            <input name="export_name" type="hidden">
                            <input name="export_data" type="hidden">
                            <button id="btn_export" class="btn btn-success btn-sm" type="submit"
                                export-target="#table_data" data-toggle="tooltip" title="司機對單用">
                                <i class="bi bi-file-earmark-excel"></i>匯出總表
                            </button>
                        </form>
                    </div>
                    <div class="col">
                        <form action="{{ route('ycs_transport.export_tms') }}" method="get">
                            <input name="ids" type="hidden">
                            <button id="btn_export_tms" type="submit" class="btn btn-secondary btn-sm"
                                data-toggle="tooltip" title="回倒音速物流單號到TMS">
                                <i class="bi bi-arrow-repeat"></i>匯出TMS格式
                            </button>
                        </form>
                    </div>
                </div>
                <div class="form-row mt-2">
                    當月總件數：{{ number_format($total) }}
                </div>
            </div>
        </div>
        <div class="form-row mt-2">
            <div class="col">
                @if (!empty($data))
                    <div class="FrozenTable" style="max-height: 78vh; font-size: 0.85rem;">
                        <table id="table_data" class="table table-bordered sortable table-filter" style="width: 120vw;">
                            <thead>
                                <tr>
                                    <th style="width: 2%;" class="export-none sort-none"><input id="chk_all"
                                            type="checkbox"></th>
                                    <th style="width: 5%;" class="export-none">操作</th>
                                    <th class="filter-col" style="width: 5%">狀態</th>
                                    <th style="width: 3%;">流水號</th>
                                    <th style="width: 5%;">銷貨單號</th>
                                    <th style="width: 7%;">原始訂單編號</th>
                                    <th style="width: 3%;">件數</th>
                                    <th style="width: 3%;">尺寸</th>
                                    <th style="width: 3%;">重量</th>
                                    <th style="width: 9%;">品名</th>
                                    <th style="width: 9%;">收件者</th>
                                    <th style="width: 10%;">收件電話</th>
                                    <th style="width: 15%;">收件地址</th>
                                    <th>備註</th>
                                    <th style="width: 6%;">API託運單號</th>
                                    <th style="width: 6%;">TMS託運單號</th>
                                    <th style="width: 3%;">上車</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($data as $item)
                                    <tr data-id="{{ $item['id'] }}" data-sale_no="{{ $item['sale_no'] }}"
                                        class="status_{{ $item['status'] }}">
                                        <td>
                                            @if ($item['status'] != 3)
                                                <input class="confirm" type="checkbox">
                                            @endif
                                        </td>
                                        <td>
                                            <div class="d-flex flex-column justify-content-center align-items-center">
                                                <a class="btn btn-warning btn-sm m-1"
                                                    href="{{ route('ycs_transport.edit', ['id' => $item['id'], 'date' => $date]) }}">
                                                    <i class="bi bi-pencil-fill"></i>編輯
                                                </a>
                                                <form method="POST"
                                                    action="{{ route('ycs_transport.destroy', $item['id']) }}"
                                                    onsubmit="return confirm('確定要刪除【銷貨單號 {{ $item['sale_no'] }}】?');">
                                                    @csrf
                                                    <input name="_method" type="hidden" value="DELETE">
                                                    <input name="date" type="hidden" value="{{ $date }}">
                                                    <button class="btn btn-danger btn-sm m-1">
                                                        <i class="bi bi-trash"></i>刪除
                                                    </button>
                                                </form>
                                            </div>
                                        </td>
                                        <td>{{ $item['status_name'] }}</td>
                                        <td>{{ $item['id'] }}</td>
                                        <td>{{ $item['sale_no'] }}</td>
                                        <td>{{ $item['order_no'] }}</td>
                                        <td>{{ number_format($item['qty']) }}</td>
                                        <td>{{ $item['size'] }}</td>
                                        <td>{{ $item['weight'] }}</td>
                                        <td>{{ $item['name'] }}</td>
                                        <td>{{ $item['receiver'] }}</td>
                                        <td>{{ $item['tel'] }}</td>
                                        <td>{{ $item['addr'] }}</td>
                                        <td>{{ $item['remark'] }}</td>
                                        <td>{{ $item['systemNumber'] }}</td>
                                        <td>{{ $item['tran_no'] }}</td>
                                        <td class="shipment">{{ $item['shipment'] ? '✔' : '' }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                @else
                    <div class="alert alert-danger" style="text-align: center;">
                        無資料，請匯入音速物流
                    </div>
                @endif
            </div>
        </div>
    </div>
@stop
@section('script')
    <script type="text/javascript">
        $(document).ready(InitialDate());

        function InitialDate() {
            var today = new Date();
            if ($("#input_date").val() == '') {
                $("#input_date").val(DateToString(today));
            }
            if ($("#input_settle_date").val() == '') {
                $("#input_settle_date").val(DateToString(today));
            }
            if ($("#input_export_date").val() == '') {
                $("#input_export_date").val(DateToString(today));
            }
        }
        //全選功能
        $('#chk_all').change(check_all);
        //上傳檔案後顯示文件名稱
        $('#input_file,#input_convert_file').change(function(e) {
            var fileName = e.target.files[0].name;
            $(this).next('.custom-file-label').html(fileName);
        });
        //日期變更，更新匯出日期
        $('#input_date').change(function(e) {
            $('#input_export_date').val($(this).val());
        });
        //匯出檔名修正
        $("#btn_export").click(function() {
            var name = '';
            var date = $("#input_date").val().replaceAll('-', '');
            name = date + '音速物流總表';

            $("input[name='export_name']").val(name);

            var data = JSON.parse($("input[name='export_data']").val());
            if (!IsNullOrEmpty(data)) {
                var nameIdx = -1;
                var qtyIdx = -1;
                var floorCount = 0;
                var otherCount = 0;
                $(data).each(function(index, element) {
                    if (index == 0) {
                        nameIdx = element.indexOf('品名')
                        qtyIdx = element.indexOf('件數')
                    } else if (nameIdx == -1 || qtyIdx == -1) {
                        return false;
                    } else {
                        var name = element[nameIdx];
                        var qty = parseInt(element[qtyIdx])
                        if (name == '地板') {
                            floorCount += qty;
                        } else {
                            otherCount += qty;
                        }
                    }
                });
                data.push(['', '地板總件數', floorCount, '其他總件數', otherCount]);
                $("input[name='export_data']").val(JSON.stringify(data));
            }
        });
        $('#btn_api,#btn_export_tms,#btn_batch_delete').click(function() {
            var data = new Array();
            $("#table_data>tbody>tr .confirm:checked").each(function(i, e) {
                data.push($(e).closest('tr').data().id);
            });
            $('input[name="ids"]').val(JSON.stringify(data));
        });
    </script>
@stop
