@extends('layouts.base')
@section('css')
    <style type="text/css">
        .item_name {
            text-align: left !important;
        }

        /*明細*/
        table.inner-table>thead>tr>th:nth-child(2),
        table.inner-table>tbody>tr>td:nth-child(2) {
            width: 100px;
        }
    </style>
@endsection
@section('title')
    今日地板數量
@stop
@section('content')
    <div class="container-fluid">
        <div class="form-row">
            <div class="col">
                @if (empty($floor) || count($floor) == 0)
                    <div class="alert alert-danger">
                        找不到資料
                    </div>
                @else
                    <div class="FrozenTable" style="max-height:92vh;">
                        <table class="table table-bordered table-hover sortable">
                            <caption>今日地板總計</caption>
                            <thead>
                                <tr>
                                    <th style="width: 10%">料號</th>
                                    <th>品名</th>
                                    <th style="width: 8%">銷單數量</th>
                                    <th style="width: 8%">已包數量</th>
                                    <th style="width: 20%" class="p-0">
                                        <table class="inner-table">
                                            <thead>
                                                <tr>
                                                    <th>銷貨單號</th>
                                                    <th>數量</th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($floor as $item)
                                    <tr>
                                        <td>{{ $item['item_no'] }}</td>
                                        <td class="item_name">{{ $item['item_name'] }}</td>
                                        <td>{{ number_format($item['qty']) }}</td>
                                        <td>{{ number_format($item['package']) }}</td>
                                        <td class="p-0">
                                            <table class="inner-table">
                                                <tbody>
                                                    @foreach ($item['sale_orders'] as $order_no => $qty)
                                                        <tr>
                                                            <td>{{ $order_no }}</td>
                                                            <td>{{ number_format($qty) }}</td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                @endif
            </div>
        </div>
    </div>
@stop
@section('script')
    <script type="text/javascript"></script>
@stop
