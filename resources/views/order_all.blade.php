@extends('layouts.base')
@section('title')
    訂單總表
@stop
@section('css')
    <style type="text/css">
        .form-group {
            margin: 0;
        }

        /*特殊註記*/
        .shipment_status,
        [class^="order_kind_status_"] {
            border-style: solid;
            border-width: 1px;
            border-radius: 0.25rem;
            padding: 0.1rem 0.2rem;
            margin: 0.1rem;
        }

        /*過大過重、新竹偏遠*/
        .shipment_status,
        .order_kind_status_3 {
            color: #fff;
            background-color: #dc3545;
            border-color: #dc3545;
        }

        /*預購*/
        .order_kind_status_1 {
            color: #fff;
            background-color: #6c757d !important;
        }

        /*未完全出貨*/
        .order_kind_status_2 {
            background-color: #ffc107;
            border-color: #ffc107;
        }

        .status_1 {
            color: #fff;
            background-color: #007bff !important;
        }

        .status_2,
        .order_expired_2 {
            color: #fff;
            background-color: #ffc107 !important;
        }

        .status_3 {
            color: #fff;
            background-color: #28a745 !important;
        }

        .order_expired_1 {
            color: #fff;
            background-color: red !important;
        }

        /*統計顏色*/
        [class*="status_"]>ul>li {
            color: #212529;
        }

        /*預計出貨日-狀態*/
        [class^="estimated_date_status_"] {
            padding: 0 2px;
            border-radius: 2.5px;
            font-weight: bolder;
        }

        /*預計出貨日-未確認*/
        .estimated_date_status_1 {
            color: #007bff;
            border: 1px solid #007bff;
        }

        /*預計出貨日-確認*/
        .estimated_date_status_2 {
            color: #28a745;
            border: 1px solid #28a745;
        }

        /*預計出貨日-延遲*/
        .estimated_date_status_3 {
            color: red;
            border: 1px solid red;
        }
    </style>
@endsection
@section('content')
    <!-- Modal 說明 -->
    <div id="modal_help" class="modal fade">
        <div class="modal-dialog modal-xl modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="modal-title">
                        <h1>說明</h1>
                    </div>
                    <button type="button" class="close" data-dismiss="modal">
                        <span>&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="form-row">
                            <div class="col-auto">
                                <ul class="list-group">
                                    <li class="list-group-item bg-primary text-white">助理審單</li>
                                    <li class="list-group-item">免確認：等待匯入TMS</li>
                                    <li class="list-group-item">未確認：有特殊備註或特殊商品，等待確認</li>
                                    <li class="list-group-item">已確認：等待匯入TMS</li>
                                </ul>
                            </div>
                            <div class="col-auto">
                                <ul class="list-group">
                                    <li class="list-group-item bg-warning text-white">TMS中</li>
                                    <li class="list-group-item">未覆核：訂單尚未被覆核</li>
                                    <li class="list-group-item">已取消：訂單已經被取消</li>
                                    <li class="list-group-item">可轉單：出貨倉已滿足訂單需求</li>
                                    <li class="list-group-item">待調撥：出貨倉不足，等待主倉調撥</li>
                                    <li class="list-group-item">需採購：場內庫存不足，需廠商叫貨</li>
                                    <li class="list-group-item">未進貨：已建立採購單，等待廠商到貨</li>
                                    <li class="list-group-item">協商換貨：場內庫存不足，與客人協商換貨</li>
                                </ul>
                            </div>
                            <div class="col-auto">
                                <ul class="list-group">
                                    <li class="list-group-item bg-success text-white">現場處裡</li>
                                    <li class="list-group-item">未撿貨：未列印撿貨單</li>
                                    <li class="list-group-item">未驗貨：已列印撿貨單，尚未TMS驗貨</li>
                                    <li class="list-group-item">未包裝：已驗貨，尚未包裝作業</li>
                                    <li class="list-group-item">未上車：已包裝，尚未上車檢驗</li>
                                    <li class="list-group-item">已出貨</li>
                                    <li class="list-group-item">已退貨</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">關閉</button>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="form-row">
            <div class="col-auto">
                <form action="{{ route('order_all') }}" method="GET">
                    <div class="form-row">
                        <div class="form-group col-auto">
                            <label>查詢方式</label>
                            <div class="input-group mb-3">
                                <select class="custom-select" name="search_type">
                                    <option value="1">訂單成立日期</option>
                                    <option value="2">原始訂單編號</option>
                                    <option value="3">訂單單號</option>
                                    <option value="4">銷貨單號</option>
                                    <option value="5">託運單號</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group col-auto search_type_1">
                            <label for="input_start_date">訂單成立起日</label>
                            <input id="input_start_date" name="start_date" class="form-control" type="date"
                                value="{{ old('start_date') }}">
                        </div>
                        <div class="form-group col-auto search_type_1">
                            <label for="input_end_date">訂單成立訖日</label>
                            <input id="input_end_date" name="end_date" class="form-control" type="date"
                                value="{{ old('end_date') }}">
                        </div>
                        <div class="form-group col-auto search_type_2">
                            <label for="input_original_no">原始訂單編號</label>
                            <input id="input_original_no" name="original_no" class="form-control" type="text"
                                value="{{ old('original_no') }}">
                        </div>
                        <div class="form-group col-auto search_type_3">
                            <label for="input_order_no">TMS訂單單號</label>
                            <input id="input_order_no" name="order_no" class="form-control" type="text"
                                value="{{ old('order_no') }}">
                        </div>
                        <div class="form-group col-auto search_type_4">
                            <label for="input_sale_no">TMS銷貨單號</label>
                            <input id="input_sale_no" name="sale_no" class="form-control" type="text"
                                value="{{ old('sale_no') }}">
                        </div>
                        <div class="form-group col-auto search_type_5">
                            <label for="input_trans_no">託運單號</label>
                            <input id="input_trans_no" name="trans_no" class="form-control" type="text"
                                value="{{ old('trans_no') }}">
                        </div>
                    </div>
                    <button id="btn_search" class="btn btn-info btn-sm" type="submit">查詢</button>
                </form>
            </div>
            <div class="col"></div>
            <div class="col-auto align-items-end">
                <button class="btn btn-success btn-sm" type="button" data-toggle="modal" data-target="#modal_help">
                    <i class="bi bi-question-circle"></i>說明
                </button>
            </div>
        </div>
        <div class="form-row mt-1">
            <div class="col">
                <ul class="list-group list-group-horizontal">
                    @foreach ($all_status1 as $status1 => $status1_name)
                        <li class="list-group-item p-2 status_{{ $status1 }}">
                            {{ $status1_name }}：{{ array_sum($statistics[$status1]) }}
                            <ul class="list-group list-group-horizontal">
                                @foreach ($all_status2[$status1] as $status2 => $status2_name)
                                    <li class="list-group-item p-2 mt-1">
                                        {{ $status2_name }}：{{ $statistics[$status1][$status2] }}</li>
                                @endforeach
                            </ul>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
        <div class="form-row mt-1">
            <div class="col">
                @if (!empty($data) && count($data) > 0)
                    <div class="FrozenTable" style="max-height: 69vh; font-size: 0.85rem;">
                        <table class="table table-bordered sortable table-filter">
                            <thead>
                                <tr>
                                    <th style="width: 4%;">序</th>
                                    <th style="width: 6%;">特殊註記</th>
                                    <th class="filter-col" style="width: 9%;">預計出貨日</th>
                                    <th class="filter-col" style="width: 7%;">訂單日期<br>快過期狀態</th>
                                    <th class="filter-col" style="width: 10%;">物流<br>物流單號</th>
                                    <th class="filter-col" style="width: 6%;">狀態1</th>
                                    <th class="filter-col" style="width: 6%;">狀態2</th>
                                    <th class="filter-col" style="width: 6%;">客戶簡稱</th>
                                    <th>原始訂單編號</th>
                                    <th style="width: 8%;">訂單單號</th>
                                    <th style="width: 8%;">銷貨單號</th>
                                    <th style="width: 8%;">撿貨</th>
                                    <th style="width: 8%;">驗貨</th>
                                    <th style="width: 8%;">包裝</th>
                                    <th style="width: 8%;">上車</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $idx = 1; ?>
                                @foreach ($data as $item)
                                    <?php
                                    switch ($item['order_expired']) {
                                        case '訂單過期':
                                            $order_expired_class = 'order_expired_1';
                                            break;
                                        case '優選過期':
                                            $order_expired_class = 'order_expired_2';
                                            break;
                                        default:
                                            $order_expired_class = '';
                                            break;
                                    }
                                    ?>
                                    <tr>
                                        <td>{{ $idx++ }}</td>
                                        <td>
                                            @if (count($item['order_kind']) > 0)
                                                @foreach ($item['order_kind'] as $key => $status)
                                                    <div class="order_kind_status_{{ $key }}">
                                                        {{ $status }}</div>
                                                @endforeach
                                            @endif
                                            @if (count($item['shipment_status']) > 0)
                                                @foreach ($item['shipment_status'] as $status)
                                                    <div class="shipment_status">{{ $status }}</div>
                                                @endforeach
                                            @endif
                                        </td>
                                        <td class="p-1" data-val="{{ $item['estimated_date'] }}">
                                            @if (!empty($item['estimated_date']))
                                                <div>
                                                    <span
                                                        class="estimated_date_status_{{ $item['estimated_date_status'] }}">
                                                        {{ $item['estimated_date_status_name'] }}
                                                    </span>
                                                    <br>
                                                    <span>【{{ $item['estimated_date'] }}】</span>
                                                </div>
                                            @endif
                                        </td>
                                        <td class="{{ $order_expired_class }}" data-val="{{ $item['order_expired'] }}">
                                            {{ $item['order_date'] }}
                                            <div>{{ $item['order_expired'] }}</div>
                                        </td>
                                        <td data-val="{{ $item['trans_kind'] }}">
                                            {{ $item['trans_kind'] }}
                                            @if (!empty($item['trans_no']))
                                                <div class="original_no_font">【{{ $item['trans_no'] }}】</div>
                                            @endif
                                        </td>
                                        <td class="status_{{ $item['status1'] }}">{{ $item['status1_name'] }}</td>
                                        <td data-val="{{ $item['status2_name'] }}">
                                            {{ $item['status2_name'] }}
                                            @if (!empty($item['return_no']))
                                                <br>{{ $item['return_no'] }}
                                            @endif
                                        </td>
                                        <td>{{ $item['cust_name'] }}</td>
                                        <td class="original_no_font">
                                            <a
                                                href="{{ route('basket_detail') . '?order_no=' . urlencode($item['original_no']) }}">
                                                {{ $item['original_no'] }}
                                            </a>
                                        </td>
                                        <td>
                                            <a href="{{ route('order_detail', $item['order_no']) }}">
                                                {{ $item['order_no'] }}
                                            </a>
                                        </td>
                                        <td>
                                            <a href="{{ route('check_log_detail', $item['sale_no']) }}">
                                                {{ $item['sale_no'] }}
                                            </a>
                                        </td>
                                        <td>{{ $item['pick_time'] }}</td>
                                        <td>{{ $item['check_time'] }}</td>
                                        <td>{{ $item['package_time'] }}</td>
                                        <td>{{ $item['shipment_time'] }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                @endif
            </div>
        </div>
    </div>
@stop
@section('script')
    <script type="text/javascript">
        HideSidebar();
        $('#chk_all').change(check_all);
        $(document).ready(function() {
            InitialDate();
        });

        function InitialDate() {
            var today = new Date();

            if ($("#input_start_date").val() == '') {
                $("#input_start_date").val(DateToString(today.addDays(-3)));
            }
            if ($("#input_end_date").val() == '') {
                $("#input_end_date").val(DateToString(today));
            }
        }
        //查詢方式變更
        $('[name="search_type"]').on('change', function() {
            var val = $(this).val();
            $('div[class*="search_type_"]').hide();
            $('div[class*="search_type_' + val + '"]').show();
        });
        //查詢
        $('#btn_search').click(function() {
            lockScreen();
        })
        //查詢方式變更
        $('[name="search_type"]').val({{ old('search_type') ?? 1 }}).change();
    </script>
@stop
