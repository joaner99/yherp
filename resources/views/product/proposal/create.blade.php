@extends('layouts.base')
@section('title')
    商品提案-新增
@stop
@section('css')
    <style type="text/css">
        .btn>i {
            margin: 0;
        }

        /*未定義*/
        .status_0 {
            border-color: #b3b7bb;
            background-color: #d6d8db;
        }

        /*行銷審核中、採購審核中、採購樣品中*/
        .status_1,
        .status_3,
        .status_5 {
            border-color: #ffdf7e;
            background-color: #ffeeba;
        }

        /*行銷未通過、採購未通過、樣品未通過*/
        .status_2,
        .status_4,
        .status_6 {
            border-color: #ed969e;
            background-color: #f5c6cb;
        }

        /*已上架*/
        .status_7 {
            border-color: #8fd19e;
            background-color: #c3e6cb;
        }

        .thumbnail {
            height: 200px;
            cursor: pointer;
        }

        #div_img>img {
            height: 250px;
            border: 1px solid #dee2e6;
        }
    </style>
@endsection
@section('content')
    {{-- Modal 商品重複確認 --}}
    <div id="modal_check_repeat" class="modal fade">
        <div class="modal-dialog modal-dialog-centered" style="max-width: 80vw;">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">商品相似清單</h5>
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-row">
                        <div class="col">
                            <div class="FrozenTable" style="max-height: 74vh;">
                                <table class="table table-bordered sortable">
                                    <thead>
                                        <tr>
                                            <th style="width: 12%;">審核狀態</th>
                                            <th style="width: 12%;">商品類別</th>
                                            <th>商品名稱</th>
                                            <th>商品附圖</th>
                                            <th style="width: 8%;">商品網址</th>
                                        </tr>
                                    </thead>
                                    <tbody id="tbody_product">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button id="btn_checked" type="button" class="btn btn-success" data-dismiss="modal">確認無重複</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">關閉</button>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        @if ($errors->any())
            <div class="form-row mb-2">
                <div class="col-auto">
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        @endif
        <div class="form-row">
            <div class="col-auto">
                <a class="btn btn-secondary" href="{{ URL::previous() }}">返回清單</a>
            </div>
        </div>
        <hr>
        <form method="POST" action="{{ route('product.proposal.store') }}" enctype="multipart/form-data" class="mb-2">
            @csrf
            <div class="form-row">
                <div class="col">
                    <div class="form-group">
                        <label class="text-danger" for="input_name">*商品名稱<span>(50字)</span></label>
                        <div class="input-group">
                            <input class="form-control" id="input_name" name="name" autocomplete="off" type="text"
                                maxlength="50" required>
                            <div class="input-group-append">
                                <button id="btn_check_repeat" class="btn btn-info" type="button">重複檢查</button>
                            </div>
                            <div class="input-group-append">
                                <span id="span_checked" class="input-group-text text-white bg-success"
                                    style="display: none;">✔ 通過</span>
                                <span id="span_unchecked" class="input-group-text text-white bg-danger">✘ 失敗</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-2">
                    <div class="form-group">
                        <label class="text-danger" for="select_kind">*商品大類</label>
                        <select class="custom-select" id="select_kind" name="kind">
                            @foreach ($types as $type => $type_name)
                                <option value="{{ $type }}">{{ $type_name }}</option>
                            @endforeach
                            <option value="00">其他</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-12">
                    <div class="form-group">
                        <label class="text-danger" for="input_url">*商品網址</label>
                        <input class="form-control" id="input_url" name="url"
                            type="url"placeholder="https://example.com" autocomplete="off" required>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col">
                    <div class="form-group">
                        <label class="text-danger">負評總類※2星以下(含)</label>
                        <div class="form-check-list-horizontal">
                            @foreach ($negative_review as $val => $text)
                                <div class="form-check">
                                    <input class="form-check-input" id="input_nr_{{ $val }}" name="nr[]"
                                        type="checkbox" value="{{ $val }}">
                                    <label class="form-check-label" for="input_nr_{{ $val }}">
                                        {{ $text }}
                                    </label>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="col-4">
                    <div class="form-group">
                        <label for="input_other_nr">負評其他說明<span>(50字)</span></label>
                        <input class="form-control" id="input_other_nr" name="other_nr" type="text"
                            autocomplete="off" maxlength="50">
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-3">
                    <div class="form-group">
                        <label class="text-danger" for="input_monthly_sales">*商品附圖<span>※Win + Shift + S 截圖</span></label>
                        <div id="div_past_img" class="form-control">截圖後，請在這裡按下 Ctrl+V</div>
                        <input id="input_img" name="img" type="hidden">
                    </div>
                </div>
                <div id="div_img" class="col"></div>
            </div>
            <hr>
            <button id="btn_create" class="btn btn-primary" type="submit">確認</button>
        </form>
    </div>
@stop
@section('script')
    <script src="{{ asset('js/paste.js') }}?1" type="text/javascript"></script>
    <script type="text/javascript">
        var repeat_checked = false;
        //圖片貼上
        $('#div_past_img').pastableNonInputable();
        $('*').on('pasteImage', function(ev, data) {
            var dom = '<img src="' + data.dataURL + '">';
            $('#div_img').html(dom);
            $('#input_img').val(data.dataURL);
        }).on('pasteImageError', function(ev, data) {
            alert('Oops: ' + data.message);
            if (data.url) {
                alert('But we got its url anyway:' + data.url)
            }
        }).on('pasteText', function(ev, data) {});
        //提交前檢查
        $('#btn_create').click(function() {
            if (!repeat_checked) {
                alert('請進行重複檢查');
                return false;
            } else if (IsNullOrEmpty($('#input_img').val())) {
                alert('請附上商品圖');
                return false;
            }
        });
        //商品名稱變化，需重新檢查重複
        $('#input_name').on('change', function() {
            ChangeRepeatCheck(false);
        });
        //商品重複檢查
        $('#btn_check_repeat').click(function() {
            var name = $('#input_name').val();
            if (IsNullOrEmpty(name)) {
                alert('請輸入商品名稱');
                return;
            }
            lockScreen();
            $.ajax({
                url: '{{ route('product.proposal.ajax_check_repeat') }}',
                type: "get",
                dataType: 'json',
                data: {
                    name: name,
                },
                contentType: "application/json",
                success: function(data) {
                    if ($.isEmptyObject(data)) { //沒有重複資料
                        ChangeRepeatCheck(true);
                    } else { //有重複資料
                        $('#tbody_product').html(CreateProductDOM(data));
                        $('#modal_check_repeat').modal('show');
                    }
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    var msg = JSON.parse(xhr.responseText).msg;
                    alert(msg);
                },
                complete: function() {
                    unlockScreen();
                }
            });
        })
        //確認商品名稱無重複
        $('#btn_checked').click(function() {
            ChangeRepeatCheck(true);
        });
        //建立相似商品DOM
        function CreateProductDOM(data) {
            var dom = '';
            $(data).each(function(i, e) {
                dom += '<tr>';
                dom += '<td class="status_' + e.status + '">' + e.status_name + '</td>';
                dom += '<td>' + e.kind_name + '</td>';
                dom += '<td>' + e.name + '</td>';
                dom += '<td class="text-left"><img class="thumbnail border" src="' + e.img_base64 + '"></td>';
                dom += '<td><a href="' + e.url + '" target="_blank">網址</a></td>';
                dom += '</tr>';
            });
            return dom;
        }
        //更改重複結果
        function ChangeRepeatCheck(checked) {
            repeat_checked = checked;
            $('#span_checked').toggle(checked);
            $('#span_unchecked').toggle(!checked);
        }
    </script>
@stop
