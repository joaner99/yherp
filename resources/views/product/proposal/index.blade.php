@extends('layouts.base')
@section('title')
    商品提案-清單
@stop
@section('css')
    <style type="text/css">
        .badge {
            font-size: 0.9rem;
        }

        /*未定義*/
        .status_0 {
            background-color: #d6d8db;
        }

        /*行銷審核中*/
        .status_1 {
            background-color: #ffeeba;
        }

        /*行銷未通過*/
        .status_2 {
            color: #fff;
            background-color: red;
        }

        /*採購審核中*/
        .status_3 {
            background-color: #b8daff;
        }

        /*採購未通過*/
        .status_4 {
            color: #fff;
            background-color: brown;
        }

        /*採購樣品中*/
        .status_5 {
            background-color: aqua;
        }

        /*樣品未通過*/
        .status_6 {
            background-color: pink;
        }

        /*已上架*/
        .status_7 {
            background-color: #c3e6cb;
        }

        [class^="tier_"] {
            font-size: 4rem;
            font-weight: bolder;
            padding: 0 !important;
        }

        .tier_A {
            color: #721c24;
            background-color: #f8d7da;
            border-color: #f5c6cb;
        }

        .tier_B {
            color: #0c5460;
            background-color: #d1ecf1;
            border-color: #bee5eb;
        }

        .tier_C {
            color: #1b1e21;
            background-color: #d6d8d9;
            border-color: #c6c8ca;
        }

        .thumbnail {
            height: 100px;
            cursor: pointer;
        }

        .week {
            min-width: 75px;
        }
    </style>
@endsection
@section('content')
    <!-- Modal 統計 -->
    <div id="modal_statistics" class="modal fade">
        <div class="modal-dialog modal-dialog-centered" style="max-width: 98vw;">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="modal-title">
                        <h1>統計</h1>
                    </div>
                    <button type="button" class="close" data-dismiss="modal">
                        <span>&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="form-row mt-2">
                            <div class="col-auto">
                                <ul class="pagination m-0">
                                    <li class="page-item active" data-target="#div_week_data">
                                        <button class="page-link" type="button">每週統計
                                        </button>
                                    </li>
                                    <li class="page-item" data-target="#div_statistics">
                                        <button class="page-link" type="button">狀態統計
                                        </button>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div id="div_week_data" class="form-row mt-2">
                            <div class="col">
                                <div class="FrozenTable" style="max-height: 65vh;">
                                    <table class="table table-bordered sortable table-filter">
                                        <caption>提案人-每週統計</caption>
                                        <thead>
                                            <tr>
                                                <th style="min-width: 120px;">提案人\週期</th>
                                                @foreach ($week_interval as $wi)
                                                    <th class="week filter-col">
                                                        {{ Str::substr($wi[0], 5, 5) }}<br>
                                                        {{ Str::substr($wi[1], 5, 5) }}
                                                    </th>
                                                @endforeach
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if (!empty($week_data) && count($week_data) > 0)
                                                @foreach ($week_data as $user_name => $wd)
                                                    <tr>
                                                        <td>{{ $user_name }}</td>
                                                        @foreach ($wd as $c)
                                                            <td>{{ $c }}</td>
                                                        @endforeach
                                                    </tr>
                                                @endforeach
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div id="div_statistics" class="form-row mt-2" style="display: none;">
                            <div class="col">
                                <div class="FrozenTable" style="max-height: 65vh;">
                                    <table class="table table-bordered sortable">
                                        <caption>提案人-狀態統計</caption>
                                        <thead>
                                            <tr>
                                                <th>提案人</th>
                                                @foreach ($status_list as $status)
                                                    <th style="width: 10%;">{{ $status }}</th>
                                                @endforeach
                                                <th>總獎金</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if (!empty($data) && count($data) > 0)
                                                @foreach ($data->groupBy('user') as $g)
                                                    <tr>
                                                        <td>{{ $g->first()->User->name }}</td>
                                                        @foreach ($status_list as $s_key => $status)
                                                            <td>{{ $g->where('status', $s_key)->count() }}</td>
                                                        @endforeach
                                                        <td>{{ number_format($g->sum('bonus')) }}</td>
                                                    </tr>
                                                @endforeach
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">關閉</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal 說明 -->
    <div id="modal_help" class="modal fade">
        <div class="modal-dialog modal-xl modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="modal-title">
                        <h1>說明</h1>
                    </div>
                    <button type="button" class="close" data-dismiss="modal">
                        <span>&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="form-row">
                            <div class="col-auto">
                                <ul class="list-group">
                                    <li class="list-group-item bg-primary text-white">審核狀態一覽</li>
                                    <li class="list-group-item">未定義</li>
                                    <li class="list-group-item">行銷審核中</li>
                                    <li class="list-group-item">行銷審核未通過</li>
                                    <li class="list-group-item">採購審核中</li>
                                    <li class="list-group-item">採購審核未通過</li>
                                    <li class="list-group-item">樣品採買中</li>
                                    <li class="list-group-item">樣品未通過</li>
                                    <li class="list-group-item">正式上架</li>
                                </ul>
                            </div>
                            <div class="col-auto">
                                <ul class="list-group">
                                    <li class="list-group-item bg-warning text-white">評等一覽(優先度由上至下)</li>
                                    <li class="list-group-item">A</li>
                                    <li class="list-group-item">B</li>
                                    <li class="list-group-item">C</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">關閉</button>
                </div>
            </div>
        </div>
    </div>
    {{-- Modal 附圖 --}}
    <div id="modal_attached_pictures" class="modal fade">
        <div class="modal-dialog modal-dialog-centered" style="max-width: 98vw;">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">附圖</h5>
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-row">
                        <div class="col text-center" style="overflow: auto; max-height: 73vh;">
                            <img style="max-width:100%; height:auto" id="img_attached_pictures" loading="lazy">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">關閉</button>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="form-row">
            <div class="col-auto">
                <form action="{{ route('product.proposal.index') }}" method="GET">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">提案月份</span>
                        </div>
                        <input id="input_s_date" name="s_date" type="month" class="form-control"
                            value="{{ old('s_date') }}" required>
                        <div class="input-group-append">
                            <span class="input-group-text">～</span>
                        </div>
                        <input id="input_e_date" name="e_date" type="month" class="form-control"
                            value="{{ old('e_date') }}" required>
                        <div class="input-group-append">
                            <button class="btn btn-primary" type="submit">查詢</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col"></div>
            <div class="col-auto">
                <button class="btn btn-primary" type="button" data-toggle="modal" data-target="#modal_statistics">
                    <i class="bi bi-bar-chart"></i>統計
                </button>
            </div>
            <div class="col-auto">
                <button class="btn btn-success" type="button" data-toggle="modal" data-target="#modal_help">
                    <i class="bi bi-question-circle"></i>說明
                </button>
            </div>
            <div class="col-auto">
                <a class="btn btn-info" href="{{ route('product.proposal.create') }}">
                    <i class="bi bi-plus-lg"></i>新增
                </a>
            </div>
        </div>
        <div class="form-row mt-2">
            <div class="col">
                <div class="FrozenTable" style="max-height: 86vh; font-size: 0.9rem;">
                    <table class="table table-bordered sortable table-filter">
                        <thead>
                            <tr>
                                <th style="width: 6%;">操作</th>
                                <th class="filter-col" style="width: 8%;">審核狀態</th>
                                <th class="filter-col" style="width: 5%;">評等</th>
                                <th class="filter-col" style="width: 8%;">商品類別</th>
                                <th>商品名稱</th>
                                <th>商品附圖</th>
                                <th style="width: 6%;">商品網址</th>
                                <th style="width: 20%;">負評總類</th>
                                <th class="filter-col" style="width: 8%;">提案時間</th>
                                <th class="filter-col" style="width: 6%;">提案人</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $item)
                                <tr>
                                    <td>
                                        <div class="d-flex justify-content-center flex-column">
                                            @if (Auth::user()->id == $item->user || Auth::user()->hasAnyRole(['admin', '採購主管']))
                                                <div class="my-1">
                                                    <a class="btn btn-warning btn-sm"
                                                        href="{{ route('product.proposal.edit', $item->id) }}">
                                                        <i class="bi bi-pencil-fill"></i>編輯
                                                    </a>
                                                </div>
                                                <div class="my-1">
                                                    <form method="POST"
                                                        action="{{ route('product.proposal.destroy', $item->id) }}"
                                                        onsubmit="return confirm('確定要刪除?');">
                                                        @csrf
                                                        <input name="_method" type="hidden" value="DELETE">
                                                        <button class="btn btn-danger btn-sm">
                                                            <i class="bi bi-trash"></i>刪除
                                                        </button>
                                                    </form>
                                                </div>
                                            @endif
                                            @if ($item->status == 1 && Auth::user()->hasRole('admin'))
                                                <div class="my-1">
                                                    <form method="POST"
                                                        action="{{ route('product.proposal.return', $item->id) }}"
                                                        onsubmit="return confirm('確定要退件?');">
                                                        @csrf
                                                        <button class="btn btn-primary btn-sm">
                                                            <i class="bi bi-x-square"></i>退件
                                                        </button>
                                                    </form>
                                                </div>
                                            @endif
                                        </div>
                                    </td>
                                    <td class="status_{{ $item->status }}" data-val="{{ $item->status_name }}">
                                        {{ $item->status_name }}
                                        @if ($item->status == 7)
                                            <br>
                                            獎金
                                            <br>
                                            ${{ number_format($item->bonus) }}
                                        @endif
                                    </td>
                                    <td class="tier_{{ $item->tier }}">{{ $item->tier }}</td>
                                    <td>{{ $item->kind_name }}</td>
                                    <td>{{ $item->name }}</td>
                                    <td class="text-left">
                                        @if (!empty($item->img_base64))
                                            <img class="thumbnail border" src="{{ $item->img_base64 }}" loading="lazy">
                                        @endif
                                    </td>
                                    <td><a href="{{ $item->url }}" target="_blank">網址</a></td>
                                    <td class="text-left">
                                        @if (!empty($item->NR))
                                            @foreach ($item->NR as $nr)
                                                <span class="badge badge-secondary">{{ $nr->text }}</span>
                                            @endforeach
                                        @endif
                                        @if (!empty($item->other_nr))
                                            <span class="badge badge-secondary">{{ $item->other_nr }}</span>
                                        @endif
                                    </td>
                                    <td>{{ $item->date }}</td>
                                    <td>{{ $item->User->name }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop
@section('script')
    <script type="text/javascript">
        $(document).ready(InitialDate());

        function InitialDate() {
            var today = new Date();
            var mm = today.getMonth() + 1; //January is 0!
            var yyyy = today.getFullYear();
            if (mm < 10) {
                mm = '0' + mm
            }

            today = yyyy + '-' + mm;
            if ($("#input_s_date").val() == '') {
                $("#input_s_date").val(today);
            }
            if ($("#input_e_date").val() == '') {
                $("#input_e_date").val(today);
            }
        }
        $('.thumbnail').click(function() {
            var src = $(this).attr('src');
            $('#img_attached_pictures').attr('src', src);
            $('#modal_attached_pictures').modal('show')
        });
    </script>
@stop
