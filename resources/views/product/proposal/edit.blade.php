@extends('layouts.base')
@section('title')
    商品提案-編輯
@stop
@section('css')
    <style type="text/css">
        #div_img>img {
            height: 250px;
            border: 1px solid #dee2e6;
            cursor: pointer;
        }
    </style>
@endsection
@section('content')
    {{-- Modal 附圖 --}}
    <div id="modal_attached_pictures" class="modal fade">
        <div class="modal-dialog modal-dialog-centered" style="max-width: 98vw;">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">附圖</h5>
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-row">
                        <div class="col text-center" style="overflow: auto; max-height: 73vh;">
                            <img style="max-width:100%; height:auto" id="img_attached_pictures">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">關閉</button>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        @if ($errors->any())
            <div class="form-row mb-2">
                <div class="col-auto">
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        @endif
        <div class="form-row">
            <div class="col-auto">
                <a class="btn btn-secondary" href="{{ URL::previous() }}">返回清單</a>
            </div>
        </div>
        <hr>
        <form method="POST" action="{{ route('product.proposal.update', $data->id) }}" enctype="multipart/form-data"
            class="mb-2">
            @method('PATCH')
            @csrf
            @if (count($status_list) > 0)
                <div class="form-row p-1 table-danger mb-2" style="border-style: double; border-radius: 5px;">
                    <div class="col-2">
                        <div class="form-group">
                            <label for="select_status">審核狀態</label>
                            <select class="custom-select" id="select_status" name="status">
                                @foreach ($status_list as $key => $value)
                                    @if ($key == 0)
                                        @continue
                                    @endif
                                    <option value="{{ $key }}" {{ $data->status == $key ? 'selected' : '' }}>
                                        {{ $value }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    @role('admin')
                        <div class="col-1">
                            <div class="form-group">
                                <label for="select_tier">商品評等</label>
                                <select class="custom-select" id="select_tier" name="tier">
                                    @foreach ($tiers as $tier)
                                        <option value="{{ $tier }}" {{ $data->tier == $tier ? 'selected' : '' }}>
                                            {{ $tier }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-1">
                            <div class="form-group">
                                <label for="input_bonus">獎金</label>
                                <input class="form-control" id="input_bonus" name="bonus" type="number" min="0"
                                    max="65535" step="1" value="{{ $data->bonus }}">
                            </div>
                        </div>
                    @endrole
                </div>
            @endif
            <div class="form-row">
                <div class="col">
                    <div class="form-group">
                        <label class="text-danger" for="input_name">*商品名稱<span>(50字)</span></label>
                        <input class="form-control" id="input_name" name="name" autocomplete="off" type="text"
                            maxlength="20" required value="{{ $data->name }}">
                    </div>
                </div>
                <div class="col-2">
                    <div class="form-group">
                        <label class="text-danger" for="select_kind">*商品大類</label>
                        <select class="custom-select" id="select_kind" name="kind">
                            @foreach ($types as $type => $type_name)
                                <option value="{{ $type }}" {{ $data->kind == $type ? 'selected' : '' }}>
                                    {{ $type_name }}</option>
                            @endforeach
                            <option value="00" {{ $data->kind == '00' ? 'selected' : '' }}>其他</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-12">
                    <div class="form-group">
                        <label class="text-danger" for="input_url">*商品網址</label>
                        <input class="form-control" id="input_url" name="url"
                            type="url"placeholder="https://example.com" autocomplete="off" required
                            value="{{ $data->url }}">
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col">
                    <div class="form-group">
                        <label class="text-danger">負評總類※2星以下(含)</label>
                        <div class="form-check-list-horizontal">
                            @foreach ($negative_review as $val => $text)
                                <div class="form-check">
                                    <input class="form-check-input" id="input_nr_{{ $val }}" name="nr[]"
                                        type="checkbox" value="{{ $val }}"
                                        {{ $data->NR->where('val', $val)->count() > 0 ? 'checked' : '' }}>
                                    <label class="form-check-label" for="input_nr_{{ $val }}">
                                        {{ $text }}
                                    </label>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="col-4">
                    <div class="form-group">
                        <label for="input_other_nr">負評其他說明<span>(50字)</span></label>
                        <input class="form-control" id="input_other_nr" name="other_nr" type="text"
                            autocomplete="off" value="{{ $data->other_nr }}" maxlength="50">
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-3">
                    <div class="form-group">
                        <label class="text-danger" for="input_monthly_sales">*商品附圖<span>※Win + Shift + S 截圖</span></label>
                        <div id="div_past_img" class="form-control">截圖後，請在這裡按下 Ctrl+V</div>
                        <input id="input_img" name="img" type="hidden" value="{{ $data->img_base64 }}">
                    </div>
                </div>
                <div id="div_img" class="col">
                    <img src="{{ $data->img_base64 }}">
                </div>
            </div>
            <hr>
            <button id="btn_create" class="btn btn-primary" type="submit">確認</button>
            <button id="btn_redirect" style="display: none;" class="btn btn-success" type="submit">確認，並跳轉到樣品採購</button>
            <input name="redirect" value="0" type="hidden">
        </form>
    </div>
@stop
@section('script')
    <script src="{{ asset('js/paste.js') }}?1" type="text/javascript"></script>
    <script type="text/javascript">
        $('#select_status').on('change', function() {
            $('#btn_redirect').toggle($(this).val() == 5); //樣品採買中，才需要顯示跳轉樣品採買
        });
        $('#btn_redirect').click(function() {
            $("input[name='redirect']").val('1');
        });
        $('#div_img>img').click(function() {
            var src = $(this).attr('src');
            $('#img_attached_pictures').attr('src', src);
            $('#modal_attached_pictures').modal('show')
        });
        //圖片貼上
        $('#div_past_img').pastableNonInputable();
        $('*').on('pasteImage', function(ev, data) {
            var dom = '<img src="' + data.dataURL + '">';
            $('#div_img').html(dom);
            $('#input_img').val(data.dataURL);
        }).on('pasteImageError', function(ev, data) {
            alert('Oops: ' + data.message);
            if (data.url) {
                alert('But we got its url anyway:' + data.url)
            }
        }).on('pasteText', function(ev, data) {});
        $('#btn_create').click(function() {
            if (IsNullOrEmpty($('#input_img').val())) {
                alert('請附上商品圖');
                return false;
            }
        });
    </script>
@stop
