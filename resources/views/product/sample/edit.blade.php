@extends('layouts.base')
@section('title')
    採購樣品-編輯
@stop
@section('css')
    <style type="text/css">
    </style>
@endsection
@section('content')
    <div class="container-fluid">
        @if ($errors->any())
            <div class="form-row mb-2">
                <div class="col-auto">
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        @endif
        <div class="form-row">
            <div class="col-auto">
                <a class="btn btn-secondary" href="{{ url()->previous() }}">回上頁</a>
            </div>
        </div>
        <hr>
        <form method="POST" action="{{ route('product.sample.update', $data->id) }}">
            @method('PATCH')
            @csrf
            <div class="form-row">
                <div class="col-6">
                    <div class="form-group">
                        <label class="text-danger" for="input_product_name">*商品名稱</label>
                        <input class="form-control" id="input_product_name" name="product_name" type="text"
                            maxlength="20" value="{{ $data->product_name }}" required>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-2">
                    <div class="form-group">
                        <label class="text-danger" for="input_estimated_date">*預計日期</label>
                        <input class="form-control" id="input_estimated_date" name="estimated_date" type="date"
                            value="{{ $data->estimated_date }}"required>
                    </div>
                </div>
            </div>
            <hr>
            <button class="btn btn-primary" id="btn_confirm" type="submit">確認</button>
        </form>
    </div>
@stop
@section('script')
    <script type="text/javascript"></script>
@stop
