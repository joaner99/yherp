@extends('layouts.base')
@section('title')
    採購樣品-清單
@stop
@section('css')
    <style type="text/css">
        .status {
            color: white;
        }
    </style>
@endsection
@section('content')
    <div class="container-fluid">
        <div class="form-row">
            <div class="col-auto">
                <a class="btn btn-info" href="{{ route('product.sample.create') }}">
                    <i class="bi bi-plus-lg"></i>新增
                </a>
            </div>
        </div>
        <div class="form-row mt-2">
            <div class="col">
                <div class="FrozenTable" style="max-height: 90vh; font-size:0.9rem;">
                    <table id="table_data" class="table table-bordered sortable">
                        <thead>
                            <tr>
                                <th style="width: 4%;">序</th>
                                <th style="width: 10%;">狀態</th>
                                <th>商品名稱</th>
                                <th style="width: 8%;">建單日期</th>
                                <th style="width: 8%;">預計日期</th>
                                <th style="width: 8%;">交付日期</th>
                                <th style="width: 20%;">操作</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $item)
                                <tr>
                                    <td>{{ $item->id }}</td>
                                    <td class="status" style="background-color: {{ $item->status_color }}">
                                        {{ $item->status_name }}</td>
                                    <td>{{ $item->product_name }}</td>
                                    <td>{{ $item->created_at }}</td>
                                    <td>{{ $item->estimated_date }}</td>
                                    <td>{{ $item->finished_date }}</td>
                                    <td>
                                        <div class="form-row">
                                            <div class="col-auto">
                                                <a class="btn btn-primary text-white btn-sm"
                                                    href="{{ route('product.sample.detail', $item->id) }}">
                                                    <i class="bi bi-file-text"></i>明細
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a class="btn btn-warning btn-sm"
                                                    href="{{ route('product.sample.edit', $item->id) }}">
                                                    <i class="bi bi-pencil-fill"></i>編輯
                                                </a>
                                            </div>
                                            @if (empty($item->finished_date))
                                                <div class="col-auto">
                                                    <form method="POST"
                                                        action="{{ route('product.sample.destroy', $item->id) }}"
                                                        onsubmit="return confirm('確定要作廢【流水號 {{ $item->id }}】?');">
                                                        @csrf
                                                        <input name="_method" type="hidden" value="DELETE">
                                                        <button class="btn btn-danger btn-sm">
                                                            <i class="bi bi-trash"></i>作廢
                                                        </button>
                                                    </form>
                                                </div>
                                                @if (count($item->Details) > 0)
                                                    <div class="col-auto">
                                                        <form method="POST"
                                                            action="{{ route('product.sample.confirm', $item->id) }}"
                                                            onsubmit="return confirm('確定要交付【流水號 {{ $item->id }}】?');">
                                                            @csrf
                                                            <button class="btn btn-success btn-sm">
                                                                <i class="bi bi-check-square"></i>交付
                                                            </button>
                                                        </form>
                                                    </div>
                                                @endif
                                            @endif
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop
