@extends('layouts.base')
@section('title')
    採購樣品-明細
@stop
@section('content')
    <div class="container-fluid">
        <div class="form-row">
            <div class="col-auto">
                <a class="btn btn-secondary" href="{{ URL::previous() }}">返回清單</a>
            </div>
            <div class="col-auto">
                <a class="btn btn-info" href="{{ route('product.sample.detail_create', $id) }}">
                    <i class="bi bi-plus-lg"></i>新增
                </a>
            </div>
        </div>
        <div class="form-row mt-2">
            <div class="col">
                <ul class="list-group list-group-horizontal">
                    <li class="list-group-item list-group-item-secondary">商品名稱</li>
                    <li class="list-group-item">{{ $data->product_name }}</li>
                </ul>
            </div>
        </div>
        <div class="form-row mt-2">
            <div class="col">
                <div class="FrozenTable" style="max-height: 90vh;font-size: 0.9rem;">
                    <table id="table_data" class="table table-bordered sortable table-filter">
                        <thead>
                            <tr>
                                <th style="width: 4%;">序</th>
                                <th class="filter-col">供應商</th>
                                <th>出口地</th>
                                <th class="filter-col">商品名稱</th>
                                <th style="width: 6%;">批價<br>(RMB)</th>
                                <th style="width: 6%;">內陸運費<br>(RMB)</th>
                                <th style="width: 6%;">包裝費<br>(RMB)</th>
                                <th style="width: 6%;">到門運費<br>(NTD)</th>
                                <th style="width: 6%;">含稅價<br>(NTD)</th>
                                <th style="width: 6%;">蝦皮賣價<br>(NTD)</th>
                                <th style="width: 7%;">包裝尺寸<br>(長*寬*高)<br>(cm)</th>
                                <th style="width: 7%;">材積<br>CBM(m³)</th>
                                <th>附件</th>
                                <th style="width: 6%;">操作</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data->Details as $item)
                                <tr>
                                    <td>{{ $item->id }}</td>
                                    <td>{{ $item->Supplier->PNAM2 ?? $item->supplier }}</td>
                                    <td>{{ $item->export }}</td>
                                    <td>{{ $item->product_name }}</td>
                                    <td>{{ number_format($item->batch_price, 2) }}</td>
                                    <td>{{ number_format($item->ch_freight, 2) }}</td>
                                    <td>{{ number_format($item->package_price, 2) }}</td>
                                    <td>{{ number_format($item->tw_freight, 2) }}</td>
                                    <td>{{ number_format($item->total, 2) }}</td>
                                    <td>{{ number_format($item->shopee_price, 2) }}</td>
                                    <td>
                                        <div class="d-flex flex-column">
                                            <div>{{ $item->package_d }}</div>
                                            <div>{{ $item->package_w }}</div>
                                            <div>{{ $item->package_h }}</div>
                                        </div>
                                    </td>
                                    <td>{{ $item->CBM }}</td>
                                    <td>
                                        <div class="d-flex flex-column">
                                            @foreach ($item->Files as $file)
                                                <div>
                                                    @switch($file->type)
                                                        @case(1)
                                                            <a href="{{ $file->content }}" target="_blank">
                                                                {{ $file->title }}
                                                            </a>
                                                        @break

                                                        @case(2)
                                                            <a href="{{ asset('storage/' . $file->content) }}" target="_blank">
                                                                {{ $file->title }}
                                                            </a>
                                                        @break

                                                        @default
                                                    @endswitch
                                                </div>
                                            @endforeach
                                        </div>
                                    </td>
                                    <td>
                                        <div class="d-flex flex-column">
                                            <div>
                                                <a class="btn btn-warning btn-sm"
                                                    href="{{ route('product.sample.detail_edit', $item->id) }}">
                                                    <i class="bi bi-pencil-fill"></i>編輯
                                                </a>
                                            </div>
                                            <div class="mt-1">
                                                <form method="POST"
                                                    action="{{ route('product.sample.detail_destroy', $item->id) }}"
                                                    onsubmit="return confirm('確定要作廢【流水號 {{ $item->id }}】?');">
                                                    @csrf
                                                    <input name="_method" type="hidden" value="DELETE">
                                                    <button class="btn btn-danger btn-sm">
                                                        <i class="bi bi-trash"></i>作廢
                                                    </button>
                                                </form>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop
