@extends('layouts.base')
@section('title')
    採購樣品-明細編輯
@stop
@section('css')
    <style type="text/css">
        .file_lable,
        .file_delete {
            margin-right: 2.5rem;
        }

        .file_delete {}

        .file_delete>i {
            color: #dc3545 !important;
            cursor: pointer;
        }
    </style>
@endsection
@section('content')
    <!-- Modal 新增到門運費 -->
    <div id="modal_tw_freight" class="modal fade">
        <div class="modal-dialog modal-xl modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="modal-title">
                        <h1>到門運費計算</h1>
                    </div>
                    <button type="button" class="close" data-dismiss="modal">
                        <span>&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-row">
                        <div class="col-2">
                            <div class="form-group">
                                <label class="text-danger" for="input_package_d_tmp">*包裝長度(cm)</label>
                                <input class="form-control" id="input_package_d_tmp" type="number" min="1"
                                    step="any" value="{{ $data->package_d }}" required>
                            </div>
                        </div>
                        <div class="col-2">
                            <div class="form-group">
                                <label class="text-danger" for="input_package_w_tmp">*包裝寬度(cm)</label>
                                <input class="form-control" id="input_package_w_tmp" type="number" min="1"
                                    step="any" value="{{ $data->package_w }}"required>
                            </div>
                        </div>
                        <div class="col-2">
                            <div class="form-group">
                                <label class="text-danger" for="input_package_h_tmp">*包裝高度(cm)</label>
                                <input class="form-control" id="input_package_h_tmp"type="number" min="1"
                                    step="any" value="{{ $data->package_h }}"required>
                            </div>
                        </div>
                        <div class="col-2">
                            <div class="form-group">
                                <label class="text-info" for="input_package_h">材積CBM(m³)</label>
                                <input class="form-control" id="input_cbm" type="number" step="any" required readonly>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-2">
                            <div class="form-group">
                                <label class="text-danger" for="input_package_qty_tmp">*包裝數量</label>
                                <input class="form-control" id="input_package_qty_tmp"type="number" min="1"
                                    step="1" value="{{ $data->package_qty }}" required>
                            </div>
                        </div>
                        <div class="col-2">
                            <div class="form-group">
                                <label class="text-danger" for="input_container_cbm_tmp">*櫃/CBM</label>
                                <input class="form-control" id="input_container_cbm_tmp" type="number" min="0"
                                    step="any" value="{{ $data->container_cbm }}" required>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="form-row">
                        <div class="col"></div>
                        <div class="col-4">
                            <div class="form-group">
                                <label class="text-success" style="font-size:2rem;">到門運費(NTD)</label>
                                <input class="form-control" style="font-size:2rem;" id="input_tw_freight_tmp" type="number"
                                    step="any" required readonly>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button id="btn_confirm_tw_freight" class="btn btn-primary" type="button"
                        data-dismiss="modal">確認</button>
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">取消</button>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        @if ($errors->any())
            <div class="form-row mb-2">
                <div class="col-auto">
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        @endif
        <div class="form-row">
            <div class="col-auto">
                <a class="btn btn-secondary" href="{{ url()->previous() }}">返回清單</a>
            </div>
        </div>
        <hr>
        <form method="POST" action="{{ route('product.sample.detail_update', $data->id) }}" enctype="multipart/form-data">
            @method('PATCH')
            @csrf
            <div class="card mt-2">
                <div class="card-header bg-info text-white">
                    <div class="form-row">
                        <div class="col">
                            <h2 class="font-weight-bold m-0">供應商</h2>
                        </div>
                    </div>
                </div>
                <div class="card-body p-3">
                    <div class="form-row">
                        <div class="col-2">
                            <div class="form-group">
                                <label class="text-danger" for="input_supplier">*供應商
                                    <span class="text-success" style="font-size:0.8rem;">※新供應商請直接輸入中文</span>
                                </label>
                                <input class="form-control" id="input_supplier" name="supplier" list="supplier_list"
                                    autocomplete="off" type="text" maxlength="20" value="{{ $data->supplier }}"
                                    required>
                            </div>
                        </div>
                        <div class="col-2">
                            <div class="form-group">
                                <label class="text-danger" for="input_export">*出口地</label>
                                <input class="form-control" id="input_export" name="export" list="export_list"
                                    autocomplete="off" type="text" maxlength="10" value="{{ $data->export }}"
                                    required>
                            </div>
                        </div>
                        <div class="col-2">
                            <div class="form-group">
                                <label class="text-danger" for="input_T11_11">*產地國別</label>
                                <select class="custom-select" id="input_T11_11" name="T11_11" required>
                                    <option value="china"
                                        {{ strcasecmp($data->T11_11, 'china') == 0 ? 'selected' : '' }}>
                                        china</option>
                                    <option value="台灣" {{ $data->T11_11 == '台灣' ? 'selected' : '' }}>台灣</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-6">
                            <div class="form-group">
                                <label class="text-danger" for="input_product_name">*商品名稱</label>
                                <input class="form-control" id="input_product_name" name="product_name"
                                    autocomplete="off" type="text" maxlength="20" value="{{ $data->product_name }}"
                                    required>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label class="text-danger" for="input_original_name">*原廠名稱</label>
                                <input class="form-control" id="input_original_name" name="original_name"
                                    autocomplete="off" type="text" maxlength="20"value="{{ $data->original_name }}"
                                    required>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-2">
                            <div class="form-group">
                                <label class="text-danger" for="input_batch_price">*批價(RMB)</label>
                                <input class="form-control" id="input_batch_price" name="batch_price" type="number"
                                    min="0" step="any" value="{{ $data->batch_price }}" required>
                            </div>
                        </div>
                        <div class="col-2">
                            <div class="form-group">
                                <label class="text-danger" for="input_package_price">*包裝費(RMB)</label>
                                <input class="form-control" id="input_package_price" name="package_price" type="number"
                                    min="0" step="any" value="{{ $data->package_price }}" required>
                            </div>
                        </div>
                        <div class="col-2">
                            <div class="form-group">
                                <label class="text-danger" for="input_ch_freight">*內陸運費(RMB)</label>
                                <input class="form-control" id="input_ch_freight" name="ch_freight" type="number"
                                    min="0" step="any" value="{{ $data->ch_freight }}" required>
                            </div>
                        </div>
                        <div class="col-2">
                            <div class="form-group">
                                <label class="text-danger" for="input_tw_freight">*到門運費(NTD)</label>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <button class="btn btn-secondary" type="button" data-toggle="modal"
                                            data-target="#modal_tw_freight">公式</button>
                                    </div>
                                    <input class="form-control" id="input_tw_freight" name="tw_freight" type="number"
                                        min="0" step="any" value="{{ $data->tw_freight }}" required readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-2">
                            <div class="form-group">
                                <label class="text-danger" for="input_shopee_price">*蝦皮賣價(NTD)</label>
                                <input class="form-control" id="input_shopee_price" name="shopee_price" type="number"
                                    min="0" step="any" value="{{ $data->shopee_price }}" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-2">
                            <div class="form-group">
                                <label class="text-danger" for="input_package_d">*包裝長度(cm)</label>
                                <input class="form-control" id="input_package_d" name="package_d" type="number"
                                    min="0" step="any" value="{{ $data->package_d }}" required readonly>
                            </div>
                        </div>
                        <div class="col-2">
                            <div class="form-group">
                                <label class="text-danger" for="input_package_w">*包裝寬度(cm)</label>
                                <input class="form-control" id="input_package_w" name="package_w" type="number"
                                    min="0" step="any" value="{{ $data->package_w }}" required readonly>
                            </div>
                        </div>
                        <div class="col-2">
                            <div class="form-group">
                                <label class="text-danger" for="input_package_h">*包裝高度(cm)</label>
                                <input class="form-control" id="input_package_h" name="package_h" type="number"
                                    min="0" step="any" value="{{ $data->package_h }}" required readonly>
                            </div>
                        </div>
                        <div class="col-2">
                            <div class="form-group">
                                <label class="text-danger" for="input_package_qty">*包裝數量</label>
                                <input class="form-control" id="input_package_qty" name="package_qty" type="number"
                                    min="0" step="any" value="{{ $data->package_qty }}" required readonly>
                            </div>
                        </div>
                        <div class="col-2">
                            <div class="form-group">
                                <label class="text-danger" for="input_container_cbm">*櫃/CBM</label>
                                <input class="form-control" id="input_container_cbm" name="container_cbm" type="number"
                                    min="0" step="any" value="{{ $data->container_cbm }}" required readonly>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col">
                            <div class="form-group">
                                <label class="form-check-label" for="input_remark">備註</label>
                                <textarea class="form-control" id="input_remark" name="remark" style="height: 100px;" maxlength="65535">{{ $data->remark }}</textarea>
                            </div>
                        </div>
                    </div>
                    @if (count($data->Files->where('type', 2)) > 0)
                        <div class="form-row">
                            <div class="col">
                                <label>舊附件檔案</label>
                                <div class="form-group d-flex">
                                    @foreach ($data->Files->where('type', 2) as $file)
                                        <div class="file_delete">
                                            {{ $file->title }}
                                            <input name="old_file[]" type="hidden" value="{{ $file->id }}">
                                            <i class="bi bi-x-circle-fill"></i>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    @endif
                    <div class="form-row">
                        <div class="col">
                            <div class="form-group">
                                <label>附件檔案<span class="text-danger" style="font-size:1.5rem;">※檔名相同將會進行覆蓋</span></label>
                                <div class="custom-file">
                                    <input class="custom-file-input" id="input_file" name="file[]" type="file"
                                        multiple>
                                    <label class="custom-file-label" for="input_file">請選擇檔案</label>
                                </div>
                                <div id="div_files" class="mt-2 p-1"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card mt-2">
                <div class="card-header text-info">
                    <div class="form-row">
                        <div class="col">
                            <h2 class="font-weight-bold m-0">附件網址</h2>
                        </div>
                        <div class="col-auto">
                            <button class="btn btn-info" type="button" id="btn_create_url">
                                <i class="bi bi-plus-lg"></i>新增
                            </button>
                        </div>
                    </div>
                </div>
                <div class="card-body p-3">
                    <div class="FrozenTable mt-2" style="max-height: 50vh;">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th style="width: 30%;">標題<span>(50字)</span></th>
                                    <th>網址</th>
                                    <th style="width: 6%;">操作</th>
                                </tr>
                            </thead>
                            <tbody id="tbody_urls">
                                @foreach ($data->Files->where('type', '1') as $file)
                                    <tr>
                                        <td>
                                            <input name="url_title[]" class="form-control" type="text"
                                                value="{{ $file->title }}" required>
                                        </td>
                                        <td>
                                            <input name="url_content[]" class="form-control" type="text"
                                                value="{{ $file->content }}" required>
                                        </td>
                                        <td>
                                            <button class="btn btn-danger btn-sm" onclick="DetailDelete(this)"
                                                type="button">刪除</button>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <hr>
            <button id="btn_confirm" class="btn btn-primary" type="submit">確認</button>
        </form>
    </div>
    @if (!empty($supplier_list))
        <datalist id="supplier_list">
            @foreach ($supplier_list as $supplier)
                <option value="{{ $supplier->PCODE }}">{{ $supplier->PNAM2 }}</option>
            @endforeach
        </datalist>
    @endif
    @if (!empty($export_list))
        <datalist id="export_list">
            @foreach ($export_list as $supplier)
                <option>{{ $supplier }}</option>
            @endforeach
        </datalist>
    @endif
@stop
@section('script')
    <script type="text/javascript">
        $(document).ready(function() {});
        $('#btn_create_url').click(function() {
            var dom = '';
            dom += '<tr>';
            dom += '<td><input name="url_title[]" class="form-control" type="text" required></td>';
            dom += '<td><input name="url_content[]" class="form-control" type="text" required></td>';
            dom +=
                '<td><button class="btn btn-danger btn-sm" onclick="DetailDelete(this)" type="button">刪除</button></td>';
            dom += '</tr>';
            $('#tbody_urls').append(dom);
        });
        //明細刪除
        function DetailDelete(obj) {
            $(obj).closest('tr').remove();
        }
        //上傳檔案後顯示文件名稱
        $('#input_file').change(function(e) {
            var dom = '';
            $(e.target.files).each(function(index, element) {
                dom += '<span class="file_lable">';
                dom += element.name;
                dom += '</span>';
            });
            $('#div_files').html(dom);
        });
        $('.file_delete>i').click(function() {
            $(this).closest('.file_delete').remove();
        });
        //計算到門運費
        $('#modal_tw_freight input').on('change', function() {
            var d = $('#input_package_d_tmp').val();
            var w = $('#input_package_w_tmp').val();
            var h = $('#input_package_h_tmp').val();
            var pack_qty = $('#input_package_qty_tmp').val();
            var container = $('#input_container_cbm_tmp').val();
            var cbm = d * w * h / 1000000;
            cbm = parseFloat(cbm.toFixed(4));
            var tw_freight = container * cbm / pack_qty;
            tw_freight = parseFloat(tw_freight.toFixed(2));
            $("#input_cbm").val(cbm);
            $('#input_tw_freight_tmp').val(tw_freight);
        });
        //帶入到門運費
        $('#btn_confirm_tw_freight').click(function() {
            $('#input_tw_freight').val($('#input_tw_freight_tmp').val());
            $('#input_package_d').val($('#input_package_d_tmp').val());
            $('#input_package_w').val($('#input_package_w_tmp').val());
            $('#input_package_h').val($('#input_package_h_tmp').val());
            $('#input_package_qty').val($('#input_package_qty_tmp').val());
            $('#input_container_cbm').val($('#input_container_cbm_tmp').val());
        });
    </script>
@stop
