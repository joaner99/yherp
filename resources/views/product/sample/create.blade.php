@extends('layouts.base')
@section('title')
    採購樣品-新增
@stop
@section('css')
    <style type="text/css">
    </style>
@endsection
@section('content')
    <div class="container-fluid">
        @if ($errors->any())
            <div class="form-row mb-2">
                <div class="col-auto">
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        @endif
        <div class="form-row">
            <div class="col-auto">
                <a class="btn btn-secondary" href="{{ URL::previous() }}">返回清單</a>
            </div>
        </div>
        <hr>
        <form method="POST" action="{{ route('product.sample.store') }}" class="mb-2">
            @csrf
            <div class="form-row">
                <div class="col-6">
                    <div class="form-group">
                        <label class="text-danger" for="input_product_name">*商品名稱<span>(20字)</span></label>
                        <input class="form-control" id="input_product_name" name="product_name" autocomplete="off"
                            type="text" maxlength="20" required>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-2">
                    <div class="form-group">
                        <label class="text-danger" for="input_estimated_date">*預計日期</label>
                        <input class="form-control" id="input_estimated_date" name="estimated_date" type="date"required>
                    </div>
                </div>
            </div>
            <hr>
            <button id="btn_confirm" class="btn btn-primary" type="submit">確認</button>
            <button id="btn_continue" class="btn btn-success" type="submit">確認，並新增明細</button>
            <input name="continue" value="0" type="hidden">
        </form>
    </div>
@stop
@section('script')
    <script type="text/javascript">
        $(document).ready(function() {});
        $('#btn_continue').click(function() {
            $("input[name='continue']").val('1');
        });
    </script>
@stop
