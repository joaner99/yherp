@extends('layouts.base')
@section('title')
    ERP樣品-明細
@stop
@section('css')
    <style type="text/css">
    </style>
@endsection
@section('content')
    <div class="container-fluid">
        <div class="form-row">
            <div class="col-auto">
                <a class="btn btn-secondary" href="{{ URL::previous() }}">返回清單</a>
            </div>
            <div class="col"></div>
        </div>
        <div class="form-row mt-2">
            <div class="col">
                <div class="FrozenTable" style="max-height: 85vh; font-size: 0.9rem;">
                    <table id="table_data" class="table table-bordered table-hover sortable" style="min-width: 120vw;">
                        <thead>
                            <tr>
                                <th>操作</th>
                                <th>大類類別</th>
                                <th>中類類別</th>
                                <th>小類類別</th>
                                <th>單位</th>
                                <th>產品型態</th>
                                <th>產品名稱</th>
                                <th>次品名</th>
                                <th>產地國別</th>
                                <th>採購成本</th>
                                <th>保固期限<br>(年)</th>
                                <th>保固期限<br>(月)</th>
                                <th>保固期限<br>(天)</th>
                                <th>申請時間</th>
                                <th>異動時間</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (!empty($data->Details))
                                @foreach ($data->Details as $item)
                                    <tr>
                                        <td>
                                            <div class="d-flex flex-column">
                                                <div>
                                                    <a class="btn btn-warning btn-sm"
                                                        href="{{ route('product.erp.detail_edit', $item->id) }}">
                                                        <i class="bi bi-pencil-fill"></i>編輯
                                                    </a>
                                                </div>
                                            </div>
                                        </td>
                                        <td>{{ $item->ITCOD }}</td>
                                        <td>{{ $item->ITCO2 }}</td>
                                        <td>{{ $item->ITCO3 }}</td>
                                        <td>{{ $item->IPOIN }}</td>
                                        <td>{{ $item->ICONS }}</td>
                                        <td>{{ $item->INAME }}</td>
                                        <td>{{ $item->IINAME }}</td>
                                        <td>{{ $item->T11_11 }}</td>
                                        <td>{{ number_format($item->SiseCost) }}</td>
                                        <td>{{ number_format($item->warranty_y) }}</td>
                                        <td>{{ number_format($item->warranty_m) }}</td>
                                        <td>{{ number_format($item->warranty_d) }}</td>
                                        <td>{{ $item->created_at }}</td>
                                        <td>{{ $item->updated_at }}</td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop
@section('script')
    <script type="text/javascript"></script>
@stop
