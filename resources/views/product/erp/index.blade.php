@extends('layouts.base')
@section('title')
    ERP樣品-清單
@stop
@section('css')
    <style type="text/css">
        .status {
            color: white;
        }
    </style>
@endsection
@section('content')
    <div class="container-fluid">
        <div class="form-row mt-2">
            <div class="col"></div>
            @can('product_erp_edit2')
                <div class="col-auto">
                    <form method="POST" action="{{ route('product.erp.export') }}">
                        @csrf
                        <input id="input_export_ids" name="id" type="hidden" value="">
                        <button id="btn_batch_export" class="btn btn-success" type="submit">批次匯出</button>
                    </form>
                </div>
            @endcan
        </div>
        <div class="form-row mt-2">
            <div class="col">
                <div class="FrozenTable" style="max-height: 90vh;">
                    <table id="table_data" class="table table-bordered sortable">
                        <thead>
                            <tr>
                                <th style="width: 3%;" class="sort-none"><input id="chk_all" type="checkbox"></th>
                                <th style="width: 4%;">序</th>
                                <th style="width: 10%;">狀態</th>
                                <th>商品名稱</th>
                                <th style="width: 8%;">建單日期</th>
                                <th style="width: 20%;">操作</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $item)
                                <tr data-id="{{ $item->id }}">
                                    <td><input class="confirm" type="checkbox"></td>
                                    <td>{{ $item->id }}</td>
                                    <td class="status" style="background-color: {{ $item->sample->status_color }}">
                                        {{ $item->sample->status_name }}
                                    </td>
                                    <td>{{ $item->Sample->product_name }}</td>
                                    <td>{{ $item->created_at }}</td>
                                    <td>
                                        <div class="form-row">
                                            <div class="col-auto">
                                                <a class="btn btn-primary text-white btn-sm"
                                                    href="{{ route('product.erp.detail', $item->id) }}">
                                                    <i class="bi bi-file-text"></i>明細
                                                </a>
                                            </div>
                                            @can('product_erp_edit2')
                                                <div class="col-auto">
                                                    <form method="POST" action="{{ route('product.erp.export') }}">
                                                        @csrf
                                                        <input name="id" type="hidden" value="[{{ $item->id }}]">
                                                        <button id="btn_export" class="btn btn-success btn-sm" type="submit">
                                                            <i class="bi bi-file-earmark-excel"></i>匯出
                                                        </button>
                                                    </form>
                                                </div>
                                                @if (!empty($item->CanPrint))
                                                    <div class="col-auto">
                                                        <form method="POST" action="{{ route('product.erp.print') }}">
                                                            @csrf
                                                            <input name="id" type="hidden" value="{{ $item->id }}">
                                                            <button id="btn_print" class="btn btn-secondary btn-sm"
                                                                type="submit">
                                                                <i class="bi bi-printer"></i>列印
                                                            </button>
                                                        </form>
                                                    </div>
                                                @endif
                                            @endcan
                                            @if (Auth::user()->can('product_erp_edit1') || (Auth::user()->can('product_erp_edit2') && !empty($item->CanPrint)))
                                                <div class="col-auto">
                                                    <form method="POST"
                                                        action="{{ route('product.erp.confirm', $item->id) }}"
                                                        onsubmit="return confirm('確定要交付【流水號 {{ $item->id }}】?');">
                                                        @csrf
                                                        <button class="btn btn-success btn-sm">
                                                            <i class="bi bi-check-square"></i>交付
                                                        </button>
                                                    </form>
                                                </div>
                                            @endif
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop
@section('script')
    <script type="text/javascript">
        //全選功能
        $('#chk_all').change(check_all);
        //批次匯出
        $('#btn_batch_export').click(function() {
            var ids = new Array();
            $('.confirm:checked').each(function(i, e) {
                var id = $(e).closest('tr').data().id;
                ids.push(id);
            });
            $('#input_export_ids').val(JSON.stringify(ids));
        });
    </script>
@stop
