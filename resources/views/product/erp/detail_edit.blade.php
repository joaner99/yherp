@extends('layouts.base')
@section('title')
    ERP樣品-明細編輯
@stop
@section('css')
    <style type="text/css">

    </style>
@endsection
@section('content')
    <div class="container-fluid">
        @if ($errors->any())
            <div class="form-row mb-2">
                <div class="col-auto">
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        @endif
        <div class="form-row">
            <div class="col-auto">
                <a class="btn btn-secondary" href="{{ URL::previous() }}">返回清單</a>
            </div>
        </div>
        <hr>
        <form method="POST" action="{{ route('product.erp.detail_update', $data->id) }}" class="mb-2">
            @method('PATCH')
            @csrf
            @canany(['product_erp_edit1', 'product_erp_edit2'])
                <div class="form-row">
                    <div class="col-6">
                        <div class="form-group">
                            <label class="text-danger" for="input_INAME">*產品名稱</label>
                            <input class="form-control" id="input_INAME" name="INAME" type="text" maxlength="40"
                                value="{{ $data->INAME }}" required>
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-2">
                        <div class="form-group">
                            <label class="text-danger" for="input_IPOIN">*單位</label>
                            <select class="custom-select" id="input_IPOIN" name="IPOIN" required>
                                <option selected>PCS</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-2">
                        <div class="form-group">
                            <label class="text-danger" for="input_ICONS">*產品型態</label>
                            <select class="custom-select" id="input_ICONS" name="ICONS" required>
                                <option value="N" selected>一般產品</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-2">
                        <div class="form-group">
                            <label class="text-danger" for="input_warranty_y">*保固期限(年)</label>
                            <input class="form-control" id="input_warranty_y" name="warranty_y" type="number" min="0"
                                step="1" value="{{ $data->warranty_y }}" required>
                        </div>
                    </div>
                    <div class="col-2">
                        <div class="form-group">
                            <label class="text-danger" for="input_warranty_m">*保固期限(月)</label>
                            <input class="form-control" id="input_warranty_m" name="warranty_m" type="number" min="0"
                                step="1" value="{{ $data->warranty_m }}" required>
                        </div>
                    </div>
                    <div class="col-2">
                        <div class="form-group">
                            <label class="text-danger" for="input_warranty_d">*保固期限(日)</label>
                            <input class="form-control" id="input_warranty_d" name="warranty_d" type="number" min="0"
                                step="1" value="{{ $data->warranty_d }}" required>
                        </div>
                    </div>
                </div>
            @endcan
            @can('product_erp_edit2')
                <div class="form-row">
                    <div class="col-2">
                        <div class="form-group">
                            <label class="text-danger" for="input_ITCOD">*大類類別</label>
                            <input class="form-control" id="input_ITCOD" name="ITCOD" type="text" list="type1_list"
                                maxlength="2" value="{{ $data->ITCOD }}" autocomplete="off" required>
                        </div>
                    </div>
                    <div class="col-2">
                        <div class="form-group">
                            <label class="text-danger" for="input_ITCO2">*中類類別</label>
                            <input class="form-control" id="input_ITCO2" name="ITCO2" type="text" list="type2_list"
                                maxlength="3" value="{{ $data->ITCO2 }}" autocomplete="off" required>
                        </div>
                    </div>
                    <div class="col-2">
                        <div class="form-group">
                            <label class="text-danger" for="input_ITCO3">*小類類別</label>
                            <input class="form-control" id="input_ITCO3" name="ITCO3" type="text" list="type3_list"
                                maxlength="3" value="{{ $data->ITCO3 }}" autocomplete="off" required>
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-2">
                        <div class="form-group">
                            <label class="text-danger" for="input_T11_11">*產地國別</label>
                            <select class="custom-select" id="input_T11_11" name="T11_11" required>
                                <option value="china" {{ strcasecmp($data->T11_11, 'china') == 0 ? 'selected' : '' }}>china
                                </option>
                                <option value="台灣" {{ $data->T11_11 == '台灣' ? 'selected' : '' }}>台灣</option>
                            </select>
                        </div>
                    </div>
                </div>
            @endcan
            <hr>
            <button id="btn_confirm" class="btn btn-primary" type="submit">確認</button>
        </form>
    </div>
    @if (!empty($item_type))
        @foreach ($item_type as $type => $item)
            <datalist id="type{{ $type }}_list">
                @foreach ($item as $value => $text)
                    <option value="{{ $value }}">{{ $text }}</option>
                @endforeach
            </datalist>
        @endforeach
    @endif
    @if (!empty($item_supplier))
        <datalist id="supplier_list">
            @foreach ($item_supplier as $item)
                <option value="{{ $item->PCODE }}">{{ $item->PNAM2 }}</option>
            @endforeach
        </datalist>
    @endif
@stop
@section('script')
    <script type="text/javascript">
        $(document).ready(function() {
            $("#input_T11_11").val("{{ old('T11_11') ?? '台灣' }}");
            $("#input_ICONS").val("{{ old('ICONS') ?? 'N' }}");
        });
        //提交
        $('#btn_confirm').click(function() {
            if (!verifiDatalist('input_ITCOD') || !verifiDatalist('input_ITCO2') ||
                !verifiDatalist('input_ITCO3')) {
                alert('大中小類的輸入必須是內建選項');
                return false;
            }
        });
        $('#btn_continue').click(function() {
            $("input[name='continue']").val('1');
        });
        //驗證輸入是否在選項內
        function verifiDatalist(inputId) {
            const $input = $('#' + inputId),
                $options = $('#' + $input.attr('list') + ' option'),
                inputVal = $input.val();
            let verification = false;
            for (let i = 0; i < $options.length; i++) {
                const $option = $options.eq(i),
                    showWord = $option.val();
                if (showWord == inputVal) {
                    verification = true;
                }
            }
            return verification;
        }
    </script>
@stop
