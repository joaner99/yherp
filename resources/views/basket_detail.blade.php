@extends('layouts.base')
@section('title')
    籃子訂單明細
@stop
@section('content')
    <div class="container-fluid">
        @if (!empty($data))
            <div class="card">
                <div class="card-header">
                    訂單明細
                </div>
                <div class="card-body">
                    <ul class="list-group list-group-horizontal-xl">
                        <li class="list-group-item list-group-item-info">原始訂單單號：<br>{{ $data['order_no'] }}</li>
                        <li class="list-group-item list-group-item-info">客戶：<br>【{{ $data['cust'] }}】{{ $data['src_name'] }}
                        <li class="list-group-item list-group-item-info">訂單狀態：<br>{{ $data['status'] }}
                        <li class="list-group-item list-group-item-info">
                            預排物流：<br>【{{ $data['trans'] }}】{{ $data['trans_name'] }}
                        </li>
                        <li class="list-group-item list-group-item-info">
                            總金額(含稅)：<br>{{ number_format($data['order_total']) }}
                        </li>
                    </ul>
                    <div class="form-row mt-2">
                        <div class="col-6">
                            買家備註
                            <textarea class="form-control" style="height: 100px;" readonly>{{ $data['customer_remark'] }}</textarea>
                        </div>
                        <div class="col-6">
                            賣家備註
                            <textarea class="form-control" style="height: 100px;" readonly>{{ $data['seller_remark'] }}</textarea>
                        </div>
                    </div>
                    <div class="FrozenTable mt-3" style="max-height: 360px;">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>料號</th>
                                    <th>品名</th>
                                    <th>售價</th>
                                    <th>數量</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($data['detail'] as $item)
                                    <tr>
                                        <td>{{ $item['item_no'] }}</td>
                                        <td class="text-left">{{ $item['item_name'] }}</td>
                                        <td>{{ number_format($item['item_price']) }}</td>
                                        <td>{{ number_format($item['item_quantity']) }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        @endif
    </div>
@stop
