@extends('layouts.base')
@section('title')
    貨櫃進口-編輯
@stop
@section('css')
    <style type="text/css">
        .btn>i {
            margin: 0;
        }

        #ul_ship_date>li {
            padding: 0.25rem;
        }
    </style>
@endsection
@section('content')
    <div class="container-fluid">
        <div class="form-row mb-2">
            <div class="col-auto">
                <a class="btn btn-secondary" href="{{ route('container_import.index') }}">回上頁</a>
            </div>
        </div>
        <form method="POST" action="{{ route('container_import.update', $data->id) }}">
            @csrf
            <div class="form-row">
                <div class="col-lg-2">
                    <div class="form-group">
                        <label>報單號碼</label>
                        <input class="form-control" type="text" value="{{ $data->order_no }}" readonly>
                    </div>
                </div>
            </div>
            <div class="form-row mb-2">
                <div class="col-lg-12">
                    <div class="form-group">
                        <label>到貨日期</label><button id="btn_add_ship_date" type="button"
                            class="btn btn-info btn-sm ml-2">+</button>
                        <ul id="ul_ship_date" class="list-group list-group-horizontal">
                            @if (empty($data->shipdate_list) || count($data->shipdate_list) == 0)
                                <li class="list-group-item">
                                    <div class="input-group">
                                        <input class="form-control" type="date">
                                        <div class="input-group-append">
                                            <button class="btn btn-danger btn-sm" type="button"
                                                onclick="delete_ship_date(this)"><i class="bi bi-trash"></i></button>
                                        </div>
                                    </div>
                                </li>
                            @else
                                @foreach ($data->shipdate_list as $key => $shipdate)
                                    <li class="list-group-item">
                                        <div class="input-group">
                                            <input class="form-control" type="date" value="{{ $shipdate }}">
                                            <div class="input-group-append">
                                                <button class="btn btn-danger btn-sm" type="button"
                                                    onclick="delete_ship_date(this)"><i class="bi bi-trash"></i></button>
                                            </div>
                                        </div>
                                    </li>
                                @endforeach
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
            <div class="form-row mb-2">
                <div class="col-lg-2">
                    <label class="text-danger">*付款方式</label>
                    <select name="remittance_type" class="custom-select" required>
                        <option value="1">匯款</option>
                        <option value="2">OA</option>
                    </select>
                </div>
            </div>
            <div class="form-row" data-remittance_type="1" style="display: none;">
                <div class="col-lg-2">
                    <div class="form-group">
                        <label class="text-danger" for="input_remittance_date">*匯款日期</label>
                        <input id="input_remittance_date" name="remittance_date" class="form-control" type="date"
                            value="{{ $data->remittance_date }}">
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label class="text-danger" for="input_remittance_account">*匯款帳號</label>
                        <input id="input_remittance_account" name="remittance_account" class="form-control" type="text"
                            value="{{ $data->remittance_account }}">
                    </div>
                </div>
                <div class="col-lg-2">
                    <label class="text-danger">*幣別</label>
                    <select name="remittance_currency" class="custom-select">
                        @foreach ($currency as $val => $text)
                            <option value="{{ $val }}">{{ $text }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label class="text-danger" for="input_remittance_amount">*匯款金額</label>
                        <input id="input_remittance_amount" name="remittance_amount" class="form-control" type="number"
                            value="{{ $data->remittance_amount }}" max="9999999" step="0.01">
                    </div>
                </div>
            </div>
            <div class="form-row" data-remittance_type="2" style="display: none;">
                <div class="col-lg-2">
                    <div class="form-group">
                        <label class="text-danger" for="input_remittance_date2">*起始日期</label>
                        <input id="input_remittance_date2" name="remittance_date2" class="form-control" type="date"
                            value="{{ $data->remittance_date2 }}">
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="form-group">
                        <label class="text-danger" for="input_remittance_date">*扣款日期</label>
                        <input id="input_remittance_date" name="remittance_date" class="form-control" type="date"
                            value="{{ $data->remittance_date }}">
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label class="text-danger" for="input_remittance_account">*扣款帳號</label>
                        <input id="input_remittance_account" name="remittance_account" class="form-control"
                            type="text" value="{{ $data->remittance_account }}">
                    </div>
                </div>
                <div class="col-lg-2">
                    <label class="text-danger">*幣別</label>
                    <select name="remittance_currency" class="custom-select">
                        @foreach ($currency as $val => $text)
                            <option value="{{ $val }}">{{ $text }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label class="text-danger" for="input_remittance_amount">*扣款金額</label>
                        <input id="input_remittance_amount" name="remittance_amount" class="form-control" type="number"
                            value="{{ $data->remittance_amount }}" max="9999999" step="0.01">
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col">
                    <label>備註</label>
                    <label class="text-success ml-2">※200字以內</label>
                    <textarea class="form-control" name="remark" style="height: 100px;" maxlength="200"></textarea>
                </div>
            </div>
            <button id="btn_confirm" class="btn btn-primary mt-2" type="submit">確認</button>
            <input name="ship_date" type="hidden" value="">
        </form>
    </div>
@stop
@section('script')
    <script type="text/javascript">
        $(document).ready(function() {
            var obj = $('[name="remittance_type"]');
            $(obj).val('{{ $data->remittance_type }}');
            remittance_type_onchange(obj);
            $('[name="remittance_currency"]').val('{{ $data->remittance_currency }}');
        });
        //付款方式變更
        $('select[name="remittance_type"]').on('change', function() {
            remittance_type_onchange(this);
        })
        //付款方式變更
        function remittance_type_onchange(obj) {
            var type = $(obj).val();
            $('[data-remittance_type]').each(function(i, e) {
                if ($(e).data().remittance_type == type) {
                    $(e).find('[name]').removeAttr('disabled');
                    $(e).show();
                } else {
                    $(e).hide();
                    $(e).find('[name]').attr('disabled', 'disabled');
                }
            });
        }
        //新增出貨日期
        $('#btn_add_ship_date').click(function() {
            var dom = '';
            dom += '<li class="list-group-item">';
            dom += '<div class="input-group">';
            dom += '<input class="form-control" type="date">';
            dom +=
                '<div class="input-group-append"><button class="btn btn-danger btn-sm" type="button" onclick="delete_ship_date(this)"><i class="bi bi-trash"></i></button></div>';
            dom += '</div>';
            dom += '</li>';
            $('#ul_ship_date').append(dom);
        });
        //移除出貨日期
        function delete_ship_date(obj) {
            $(obj).closest('li').remove();
        }
        //確定
        $('#btn_confirm').click(function() {
            var ship_date = [];
            $('#ul_ship_date>li input').each(function(i, e) {
                var val = $(e).val();
                if (!IsNullOrEmpty(val) && ship_date.indexOf(val) === -1) {
                    ship_date.push(val);
                }
            });
            $('[name="ship_date"]').val(JSON.stringify(ship_date));
        });
    </script>
@stop
