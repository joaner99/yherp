@extends('layouts.base')
@section('title')
    貨櫃進口-新增
@stop
@section('css')
    <style type="text/css">
    </style>
@endsection
@section('content')
    <div class="container-fluid">
        <div class="form-row mb-2">
            <div class="col-auto">
                <a class="btn btn-secondary" href="{{ route('container_import.index') }}">回上頁</a>
            </div>
        </div>
        <form method="POST" action="{{ route('container_import.store') }}" enctype="multipart/form-data">
            @csrf
            <div class="form-row">
                <div class="col-lg-2">
                    <div class="form-group">
                        <label class="text-danger" for="input_order_no">*報單號碼</label>
                        <input class="form-control" id="input_order_no" name="order_no" type="text" required>
                    </div>
                </div>
            </div>
            <div class="form-row mb-2">
                <div class="col-lg-12">
                    <div class="form-group">
                        <label>到貨日期</label><button id="btn_add_ship_date" type="button"
                            class="btn btn-info btn-sm ml-2">+</button>
                        <ul id="ul_ship_date" class="list-group list-group-horizontal">
                            <li class="list-group-item">
                                <div class="input-group">
                                    <input class="form-control" type="date">
                                    <div class="input-group-append">
                                        <button class="btn btn-danger btn-sm" type="button"
                                            onclick="delete_ship_date(this)"><i class="bi bi-trash"></i></button>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-lg-6">
                    <div class="form-group">
                        <label class="text-danger" for="input_file">*附圖</label>
                        <div class="input-group mb-3">
                            <div class="custom-file">
                                <input class="custom-file-input" id="input_file" name="file[]" type="file"
                                    accept="image/*" multiple>
                                <label class="custom-file-label" for="input_file">請選擇檔案</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <button id="btn_confirm" class="btn btn-primary mt-2" type="submit">確認</button>
            <input name="ship_date" type="hidden" value="">
        </form>
    </div>
@stop
@section('script')
    <script type="text/javascript">
        $(document).ready(InitialDate());

        function InitialDate() {
            var today = new Date();
            if ($("#input_date").val() == '') {
                $("#input_date").val(DateToString(today));
            }
        }
        //上傳檔案後顯示文件名稱
        $('#input_file,#input_transport_file').change(function(e) {
            var fileName = '';
            $(e.target.files).each(function(index, element) {
                if (fileName != '') {
                    fileName += ',';
                }
                fileName += element.name;
            });
            $(this).next('.custom-file-label').html(fileName);
        });
        //新增出貨日期
        $('#btn_add_ship_date').click(function() {
            var dom = '';
            dom += '<li class="list-group-item">';
            dom += '<div class="input-group">';
            dom += '<input class="form-control" type="date">';
            dom +=
                '<div class="input-group-append"><button class="btn btn-danger btn-sm" type="button" onclick="delete_ship_date(this)"><i class="bi bi-trash"></i></button></div>';
            dom += '</div>';
            dom += '</li>';
            $('#ul_ship_date').append(dom);
        });
        //移除出貨日期
        function delete_ship_date(obj) {
            $(obj).closest('li').remove();
        }
        //驗證
        $('#btn_confirm').click(function() {
            var ship_date = [];
            $('#ul_ship_date>li input').each(function(i, e) {
                var val = $(e).val();
                if (!IsNullOrEmpty(val) && ship_date.indexOf(val) === -1) {
                    ship_date.push(val);
                }
            });
            $('[name="ship_date"]').val(JSON.stringify(ship_date));
        });
    </script>
@stop
