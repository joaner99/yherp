@extends('layouts.base')
@section('title')
    貨櫃進口
@stop
@section('css')
    <style type="text/css">
        .thumbnail {
            width: 100px;
            height: 100px;
            cursor: pointer;
        }
    </style>
@endsection
@section('content')
    {{-- Modal 附圖 --}}
    <div id="modal_attached_pictures" class="modal fade">
        <div class="modal-dialog modal-dialog-centered" style="max-width: 98vw;">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">附圖</h5>
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-row">
                        <div class="col text-center" style="overflow: auto; max-height: 73vh;">
                            <img style="max-width:100%; height:auto" id="img_attached_pictures">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">關閉</button>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="form-row align-items-center">
            <div class="col-auto">
                <a class="btn btn-info btn-sm" href="{{ route('container_import.index') }}">
                    <i class="bi bi-search"></i>查詢未結案
                </a>
            </div>
            <div class="col-auto">
                <a class="btn btn-info btn-sm" href="{{ route('container_import.index') . '?search_all=1' }}">
                    <i class="bi bi-search"></i>查詢全部
                </a>
            </div>
            <div class="col"></div>
            <div class="col-auto">
                <a class="btn btn-info btn-sm" href="{{ route('container_import.create') }}">
                    <i class="bi bi-plus-lg"></i>新增
                </a>
            </div>
        </div>
        <div class="form-row mt-2">
            @if (empty($data))
                <div class="alert alert-danger">
                    無資料
                </div>
            @else
                <div class="col">
                    <div class="FrozenTable" style="max-height: 87vh; font-size: 0.8rem;">
                        <table id="table_data" class="table table-bordered sortable table-filter">
                            <colgroup>
                                <col span="5">
                                <col span="6" class="table-warning">
                            </colgroup>
                            <thead>
                                <tr>
                                    <th style="width: 6%;">操作</th>
                                    <th style="width: 4%;">流水號</th>
                                    <th class="filter-col" style="width: 8%;">報單號碼</th>
                                    <th class="filter-col" style="width: 6%;">到貨日期</th>
                                    <th>附圖</th>
                                    <th class="filter-col" style="width: 5%;">匯款方式</th>
                                    <th style="width: 6%;">OA<br>起始日期</th>
                                    <th class="filter-col" style="width: 6%;">匯款/扣款<br>日期</th>
                                    <th class="filter-col" style="width: 15%;">匯款/扣款<br>帳號</th>
                                    <th class="filter-col" style="width: 5%;">幣別</th>
                                    <th style="width: 7%;">匯款/扣款<br>金額</th>
                                    <th style="width: 15%;">備註</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($data as $item)
                                    <tr class="{{ empty($item->completed_at) ? '' : 'table-success' }}">
                                        <td>
                                            <div class="d-flex flex-column justify-content-center">
                                                @if (empty($item->completed_at))
                                                    <div class="m-1">
                                                        <a class="btn btn-warning btn-sm"
                                                            href="{{ route('container_import.edit', $item->id) }}">
                                                            <i class="bi bi-pencil-fill"></i>編輯
                                                        </a>
                                                    </div>
                                                    @if ($item->can_finish)
                                                        <div class="m-1">
                                                            <form method="POST"
                                                                action="{{ route('container_import.finish', $item->id) }}"onsubmit="return confirm('確定要結案?');">
                                                                @csrf
                                                                <button class="btn btn-info btn-sm">
                                                                    <i class="bi bi-check2-circle"></i>結案
                                                                </button>
                                                            </form>
                                                        </div>
                                                    @endif
                                                    <div class="m-1">
                                                        <form method="POST"
                                                            action="{{ route('container_import.destroy', $item->id) }}"onsubmit="return confirm('確定要刪除?');">
                                                            @csrf
                                                            <input name="_method" type="hidden" value="DELETE">
                                                            <button class="btn btn-danger btn-sm">
                                                                <i class="bi bi-trash"></i>刪除
                                                            </button>
                                                        </form>
                                                    </div>
                                                @else
                                                    <div>結案於</div>
                                                    <div>
                                                        {{ (new DateTime($item->completed_at))->format('Y-m-d') }}
                                                    </div>
                                                @endif
                                            </div>
                                        </td>
                                        <td>{{ $item->id }}</td>
                                        <td>{{ $item->order_no }}</td>
                                        <td>
                                            @foreach ($item->shipdate_list as $shipdate)
                                                <div>{{ $shipdate }}</div>
                                            @endforeach
                                        </td>
                                        <td class="text-left">
                                            @foreach ($item->Files as $file)
                                                <img class="thumbnail border" src="{{ $file->img_base64 }}" />
                                            @endforeach
                                        </td>
                                        <td>{{ $item->remittance_type_name }}</td>
                                        <td>
                                            @if ($item->remittance_type == 2)
                                                {{ $item->remittance_date2 }}
                                            @endif
                                        </td>
                                        <td>{{ $item->remittance_date }}</td>
                                        <td>{{ $item->remittance_account }}</td>
                                        <td>{{ $item->currency_name }}</td>
                                        <td>{{ number_format($item->remittance_amount, 2) }}</td>
                                        <td>{{ $item->remark }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            @endif
        </div>
    </div>
@stop
@section('script')
    <script type="text/javascript">
        $('.thumbnail').click(function() {
            var src = $(this).attr('src');
            $('#img_attached_pictures').attr('src', src);
            $('#modal_attached_pictures').modal('show')
        });
    </script>
@stop
