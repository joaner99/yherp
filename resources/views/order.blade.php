@extends('layouts.base')
@section('title')
    {{ $name }}
@stop
@section('css')
    <style type="text/css">
        #table_sche th {
            background-color: darkmagenta !important;
        }
    </style>
@stop
@section('content')
    <div class="container-fluid">
        @if (!empty($schedule))
            <div class="form-row mb-2">
                <div class="col-auto">
                    <div class="FrozenTable">
                        <table id="table_sche" class="table table-bordered">
                            <colgroup>
                                <col span="5">
                                <col span="1" style="background-color: aliceblue;">
                                <col span="1" style="background-color: antiquewhite;">
                                <col>
                            </colgroup>
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>地板</th>
                                    <th>超商</th>
                                    <th>宅配</th>
                                    <th>合計</th>
                                    <th>急單</th>
                                    <th>壓單</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th>未轉單數</th>
                                    <td>{{ $schedule['order'][1] }}</td>
                                    <td>{{ $schedule['order'][2] }}</td>
                                    <td>{{ $schedule['order'][3] }}</td>
                                    <td>{{ $schedule['order'][0] }}</td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <th>已轉件數</th>
                                    <td>{{ $schedule['sales']->where('status', '!=', 0)->where('type', 1)->sum('qty') }}
                                    </td>
                                    <td>{{ $schedule['sales']->where('status', '!=', 0)->where('type', 2)->sum('qty') }}
                                    </td>
                                    <td>{{ $schedule['sales']->where('status', '!=', 0)->where('type', 3)->sum('qty') }}
                                    </td>
                                    <td>{{ $schedule['sales']->where('status', '!=', 0)->sum('qty') }}</td>
                                    <td></td>
                                    <td>{{ $schedule['sales']->where('status', 0)->sum('qty') }}</td>
                                </tr>
                                <tr>
                                    <th>已轉單數</th>
                                    <td>{{ $schedule['sales']->where('status', '!=', 0)->where('type', 1)->count() }}</td>
                                    <td>{{ $schedule['sales']->where('status', '!=', 0)->where('type', 2)->count() }}</td>
                                    <td>{{ $schedule['sales']->where('status', '!=', 0)->where('type', 3)->count() }}</td>
                                    <td>{{ $schedule['sales']->where('status', '!=', 0)->count() }}</td>
                                    <td></td>
                                    <td>{{ $schedule['sales']->where('status', 0)->count() }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        @endif
        <div class="form-row">
            <div class="col">
                <div class="FrozenTable" style="font-size:0.9rem; max-height: {{ !empty($schedule) ? 68 : 90 }}vh;">
                    <table class="table table-bordered table-hover sortable table-filter">
                        <thead>
                            <tr>
                                <th scope="col" style="width: 4%;">序</th>
                                <th style="width: 10%;">原始訂單編號</th>
                                <th scope="col" style="width: 8%;">訂單單號</th>
                                <th class="filter-col" style="width: 8%;">客代簡稱</th>
                                <th scope="col" style="width: 25%;">備註</th>
                                <th>訂單明細</th>
                                @if ($name == '未轉單')
                                    <th scope="col" class="filter-col" style="width: 6%;">可轉單</th>
                                @endif
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 0; ?>
                            @if (!empty($data))
                                @foreach ($data as $value)
                                    <tr class="{{ $name == '未轉單' && $value->CanPrint ? 'table-success' : '' }}">
                                        <td scope="row">{{ ++$i }}</td>
                                        <td>{{ $value->OCOD4 }}</td>
                                        <td>
                                            <a href="{{ route('order_detail', $value->OCOD1) }}">
                                                {{ $value->OCOD1 }}
                                            </a>
                                        </td>
                                        <td>{{ $value->OCNAM }}</td>
                                        <td class="text-left">{{ $value->OBAK1 }}</td>
                                        <td class="p-0">
                                            <table class="inner-table" style="font-size: 0.9rem;">
                                                <tbody>
                                                    @foreach ($value->OrderDetail->sortBy('OICOD') as $item)
                                                        <tr>
                                                            <td style="width: 120px;">{{ $item->OICOD }}</td>
                                                            <td class="text-left">{{ $item->OINAM }}</td>
                                                            <td style="width: 100px;">{{ number_format($item->OINUM) }}
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </td>
                                        @if ($name == '未轉單')
                                            <td>
                                                @if ($value->CanPrint)
                                                    ✔
                                                @endif
                                            </td>
                                        @endif
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop
