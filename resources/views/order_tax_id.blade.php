@extends('layouts.base')
@section('css')
    <style type="text/css">
        .Highlight {
            color: red !important;
            background-color: lightyellow !important;
            font-weight: bold !important;
            font-size: 1.5em !important;
        }
    </style>
@endsection
@section('title')
    需統編清單
@stop
@section('content')
    <div class="container-fluid">
        <form action="{{ route('order_tax_id') }}" method="GET">
            <div class="form-row">
                <div class="form-group col-md-4 col-lg-3 col-xl-2">
                    <label for="input_start_date">起日</label>
                    <input id="input_start_date" name="start_date" class="form-control" type="date"
                        value="{{ old('start_date') }}">
                </div>
                <div class="form-group col-md-4 col-lg-3 col-xl-2">
                    <label for="input_end_date">訖日</label>
                    <input id="input_end_date" name="end_date" class="form-control" type="date"
                        value="{{ old('end_date') }}">
                </div>
            </div>
            <button class="btn btn-primary" type="submit">查詢</button>
        </form>
        <div class="form-row mt-2">
            <div class="col">
                <span class="text-success">※訂單備註中含有『統編』『統一編號』『合法的統編數字』，將排除銷貨單號有填上統編 或 已開立有統編發票</span>
            </div>
        </div>
        <div class="form-row mt-2">
            <div class="col">
                <div class="FrozenTable" style="max-height: 75vh; font-size:0.9rem;">
                    @if (!empty($data))
                        <table class="table table-bordered table-hover table-filter">
                            <thead>
                                <tr>
                                    <th>序</th>
                                    <th class="filter-col" style="width: 7%;">客戶簡稱</th>
                                    <th style="width: 7%;">訂單單號</th>
                                    <th style="width: 7%;">銷貨單號</th>
                                    <th style="width: 9%;">原始訂單號</th>
                                    <th>備註1</th>
                                    <th style="width: 20%;">備註2</th>
                                    <th style="width: 20%;">備註3</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $index = 1; ?>
                                @foreach ($data as $key => $value)
                                    @if (count($value->Posein) > 0)
                                        @foreach ($value->Posein as $sale)
                                            <tr>
                                                <td>{{ $index++ }}</td>
                                                <td>{{ $value->cust_name }}</td>
                                                <td><a href="{{ route('order_detail', $key) }}">{{ $key }}</a></td>
                                                <td>{{ $sale->sale_no ?? '' }}</td>
                                                <td>{{ $value->original_no }}</td>
                                                <td class="Remark">{{ $value->bak1 }}</td>
                                                <td class="Remark">{{ $value->bak2 }}</td>
                                                <td class="Remark">{{ $value->bak3 }}</td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td>{{ $index++ }}</td>
                                            <td>{{ $value->cust_name }}</td>
                                            <td><a href="{{ route('order_detail', $key) }}">{{ $key }}</a></td>
                                            <td></td>
                                            <td>{{ $value->original_no }}</td>
                                            <td class="Remark">{{ $value->bak1 }}</td>
                                            <td class="Remark">{{ $value->bak2 }}</td>
                                            <td class="Remark">{{ $value->bak3 }}</td>
                                        </tr>
                                    @endif
                                @endforeach
                            </tbody>
                        </table>
                    @else
                        <div class="alert alert-danger" style="text-align: center;">
                            找不到資料
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@stop
@section('script')
    <script type="text/javascript">
        $(document).ready(function() {
            $("tbody > tr > .Remark").html(function() {
                var sDom = '<font class="Highlight">';
                var eDom = '</font>';
                var result = $(this).text();
                if (IsNullOrEmpty(result)) { //無內容
                    return;
                }
                //標記8碼數字
                var matches = result.match(/[\D][\d]{8}[\D]/g);
                if (!IsNullOrEmpty(matches)) {
                    matches.forEach(function(element) {
                        var idx = result.indexOf(element) + 1; //+1是為了排除非數字字元
                        result = result.splice(idx, 0, sDom);
                        result = result.splice(idx + sDom.length + 8, 0, eDom);
                    });
                }
                //標記統編關鍵字
                var keyword = ['統編', '統一編號'];
                keyword.forEach(element => {
                    result = result.replace(element, (sDom + element + eDom));
                });
                return result;
            });
            InitialDate();
        });

        function InitialDate() {
            var today = new Date();

            if ($("#input_start_date").val() == '') {
                $("#input_start_date").val(DateToString(today));
            }
            if ($("#input_end_date").val() == '') {
                $("#input_end_date").val(DateToString(today));
            }
            $("#input_start_date,#input_end_date").attr("max", DateToString(today));
        }
    </script>
@stop
