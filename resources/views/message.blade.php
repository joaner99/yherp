@extends('layouts.master')
@section('master_title')
    訊息
@stop
@section('master_css')
    <style type="text/css">
        /*警告視窗*/
        .AlertView {
            z-index: 99999;
            position: fixed;
            width: 100%;
            height: 100%;
            background-color: white;
        }

        .AlertView>.card {
            width: 1200px;
            height: 600px;
            position: absolute;
            top: 50%;
            left: 50%;
            margin: -300px 0 0 -600px;
        }

        .AlertView>.card>.card-header {
            font-size: 2rem;
        }

        .AlertView>.card>.card-body>.card-title {
            font-size: 1.5rem;
        }

    </style>
@endsection
@section('master_body')
    <div class="container-fluid">
        <div class="AlertView">
            <div class="card">
                <div class="card-header bg-danger text-white">
                    <i class="bi bi-exclamation-triangle"></i>警告
                </div>
                <div class="card-body">
                    <div class="card-title">{{ $data['header'] }}</div>
                    <div class="card-text">{{ $data['content'] }}</div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('master_js')
    <script type="text/javascript">
    </script>
@stop
