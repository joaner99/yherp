@extends('layouts.master')
@section('master_title')
    上車檢驗
@stop
@section('master_css')
    <style type="text/css">
        .shipment-status {
            display: flex;
            flex-wrap: wrap;
        }

        .shipment-status>span {
            flex-basis: 300px;
        }
    </style>
@stop
@section('master_body')
    <div class="container p-3">
        <!--尚未啟用，僅顯示-->
        @if (true || $status != 4)
            <div class="form-row">
                <div class="col">
                    <form action="{{ route('check_shipment') }}" method="GET">
                        <div class="input-group input-group-lg mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    包裝條碼
                                </span>
                            </div>
                            <input autofocus type="text" name="id" id="input_order" class="form-control"
                                style="height: auto;" autocomplete="off">
                            <div class="input-group-append">
                                <button id="btn_search" class="btn btn-primary" type="submit">上車</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        @endif
        <div class="form-row">
            <div class="col">
                <div class="card">
                    <div class="card-body">
                        <div class="card-title">
                            <div class="form-row">
                                <div class="col-4 text-center" style="font-size: 15rem;">
                                    @switch($status)
                                        @case(0)
                                            <i class="bi bi-upc-scan"></i>
                                        @break

                                        @case(1)
                                        @case(4)
                                            <i class="bi bi-x-octagon-fill text-danger"></i>
                                        @break

                                        @case(2)
                                            <i class="bi bi-exclamation-triangle-fill text-warning"></i>
                                        @break

                                        @case(3)
                                            <i class="bi bi-check-circle-fill text-success"></i>
                                        @break
                                    @endswitch
                                </div>
                                <div class="col-8 font-weight-bold d-flex flex-column align-items-start justify-content-center"
                                    style="font-size: 2rem;">
                                    @if (!empty($orderNo))
                                        <div class="text-primary">銷貨單號：{{ $orderNo }}</div>
                                    @endif
                                    @if (!empty($sendCode))
                                        <div class="text-secondary">寄件代號：{{ $sendCode }}</div>
                                    @endif
                                    @if (!empty($shipment_status) && count($shipment_status) > 0)
                                        <div class="text-danger">特殊狀態：
                                            <div class="shipment-status">
                                                @foreach ($shipment_status as $s)
                                                    <span>『{{ $s }}』</span>
                                                @endforeach
                                            </div>
                                        </div>
                                    @endif
                                    <div>{!! nl2br($msg) !!}</div>
                                    <div>{{ $msg2 ?? '' }}</div>
                                    @switch($status)
                                        @case(1)
                                            {{-- 錯誤 --}}
                                            <audio src="{{ asset('audio/error.mp3') }}" autoplay="autoplay" preload="auto"></audio>
                                        @break

                                        @case(2)
                                            {{-- 重複 --}}
                                            <audio src="{{ asset('audio/重複.aac') }}" autoplay="autoplay" preload="auto"></audio>
                                        @break

                                        @case(3)
                                            {{-- 上車 --}}
                                            <audio src="{{ asset('audio/success.mp3') }}" autoplay="autoplay"
                                                preload="auto"></audio>
                                            @if (!empty($sendCode))
                                                @switch($sendCode)
                                                    @case('音速')
                                                        <audio src="{{ asset('audio/音速.m4a') }}" autoplay="autoplay" preload="auto"></audio>
                                                    @break

                                                    @case('超商')
                                                        <audio src="{{ asset('audio/超商.aac') }}" autoplay="autoplay" preload="auto"></audio>
                                                    @break

                                                    @case('小件')
                                                        <audio src="{{ asset('audio/小件.aac') }}" autoplay="autoplay"
                                                            preload="auto"></audio>
                                                    @break

                                                    @case('多件')
                                                        <audio src="{{ asset('audio/多件.aac') }}" autoplay="autoplay"
                                                            preload="auto"></audio>
                                                    @break
                                                @endswitch
                                            @endif
                                        @break

                                    @endswitch
                                </div>
                            </div>
                        </div>
                        <div class="card-text">
                            @if ($status == 2 || $status == 3)
                                <div class="FrozenTable mt-3" style="max-height: 40vh;">
                                    <table class="table table-bordered table-hover">
                                        <caption>銷貨明細</caption>
                                        <thead>
                                            <tr>
                                                <th style="width: 20%;">料號</th>
                                                <th>品名</th>
                                                <th style="width: 15%;">數量</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($detail as $item)
                                                <tr>
                                                    <td>{{ $item->item_no }}</td>
                                                    <td>{{ $item->item_name }}</td>
                                                    <td>{{ number_format($item->qty) }}</td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('master_js')
    <script type="text/javascript">
        // Get the input field
        var input = document.getElementById("input_order");

        // Execute a function when the user releases a key on the keyboard
        input.addEventListener("keydown", function(event) {
            // Number 13 is the "Enter" key on the keyboard
            if (event.keyCode === 13) {
                // Cancel the default action, if needed
                event.preventDefault();
                // Trigger the button element with a click
                document.getElementById("btn_search").click();
            }
        });
    </script>
@stop
