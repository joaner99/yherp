@extends('layouts.base')
@section('title')
    盤點作業-編輯
@stop
@section('css')
    <style type="text/css">
        .badge {
            font-size: 1rem;
        }
    </style>
@endsection
@section('content')
    <div class="container-fluid">
        @if ($errors->any())
            <div class="form-row mb-2">
                <div class="col-auto">
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        @endif
        <div class="form-row">
            <div class="col-auto">
                <a class="btn btn-secondary" href="{{ route('inventory.index') }}">回上頁</a>
            </div>
        </div>
        <hr>
        <form action="{{ route('inventory.update', $data->id) }}" method="POST" enctype="multipart/form-data">
            @method('PATCH')
            @csrf
            <div class="form-row">
                <div class="col-auto">
                    <ul class="list-group list-group-horizontal">
                        <li class="list-group-item list-group-item-info">流水號</li>
                        <li class="list-group-item">{{ $data->id }}</li>
                        <li class="list-group-item list-group-item-info">預定盤點日期</li>
                        <li class="list-group-item">{{ $data->appointment_date }}</li>
                    </ul>
                </div>
                <div class="col">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">備註</span>
                        </div>
                        <input class="form-control form-control-lg" type="text" name="remark"
                            value="{{ $data->remark }}" maxlength="50">
                    </div>
                </div>
            </div>
            <div class="form-row mt-2">
                <div class="col">
                    <div class="FrozenTable" style="max-height: 73vh; font-size: 0.9rem;">
                        <table class="table table-bordered sortable">
                            <thead>
                                <tr>
                                    <th style="width: 8%;">料號</th>
                                    <th>品名</th>
                                    <th style="width: 30%;">儲位</th>
                                    <th style="width: 8%;">盤點<br>主倉庫存</th>
                                    <th style="width: 8%;">盤點<br>出貨倉庫存</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($data->Details as $detail)
                                    <tr>
                                        <td>{{ $detail->item_no }}</td>
                                        <td class="text-left">{{ $detail->item_name }}</td>
                                        <td>
                                            <div class="d-flex justify-content-start flex-wrap">
                                                @foreach ($detail->item_place as $p)
                                                    <span class="badge badge-dark m-1">{{ $p }}</span>
                                                @endforeach
                                            </div>
                                        </td>
                                        <td>
                                            <input name="items[{{ $detail->id }}][main_stock]" class="form-control"
                                                type="number" step="1" min="0" max="65535"
                                                value="{{ $detail->main_stock ?? 0 }}" required>
                                        </td>
                                        <td>
                                            <input name="items[{{ $detail->id }}][sale_stock]" class="form-control"
                                                type="number" step="1" min="0" max="65535"
                                                value="{{ $detail->sale_stock ?? 0 }}"required>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <button id="btn_store" class="btn btn-primary mt-2" type="submit">更新</button>
        </form>
    </div>
@stop
@section('script')
    <script type="text/javascript"></script>
@stop
