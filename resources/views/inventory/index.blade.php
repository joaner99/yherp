@extends('layouts.base')
@section('title')
    盤點作業
@stop
@section('css')
    <style type="text/css">
        table.inner-table>thead>tr>th:nth-child(1),
        table.inner-table>tbody>tr>td:nth-child(1) {
            width: 135px;
        }

        table.inner-table>thead>tr>th:nth-child(n+3),
        table.inner-table>tbody>tr>td:nth-child(n+3) {
            width: 90px;
        }
    </style>
@endsection
@section('content')
    <div class="container-fluid">
        <div class="form-row">
            <div class="col-auto">
                <form action="{{ route('inventory.index') }}" method="GET">
                    <div class="form-row">
                        <div class="form-group col-auto">
                            <label for="input_start_date">預定盤點起日</label>
                            <input id="input_start_date" name="start_date" class="form-control" type="date"
                                value="{{ old('start_date') }}" required>
                        </div>
                        <div class="form-group col-auto">
                            <label for="input_end_date">預定盤點訖日</label>
                            <input id="input_end_date" name="end_date" class="form-control" type="date"
                                value="{{ old('end_date') }}" required>
                        </div>
                        <div class="form-group col-auto d-flex align-items-end">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" id="chk_include_complete"
                                    name="include_complete" value='1'
                                    {{ request()->has('include_complete') ? 'checked' : '' }}>
                                <label class="form-check-label" for="chk_include_complete">
                                    包含結案
                                </label>
                            </div>
                        </div>
                    </div>
                    <button class="btn btn-primary btn-sm">查詢</button>
                </form>
            </div>
            <div class="col"></div>
            @can('inventory_edit')
                <div class="col-auto">
                    <a class="btn btn-success" href="{{ route('inventory.create') }}">
                        <i class="bi bi-plus-lg"></i>建立
                    </a>
                </div>
            @endcan
        </div>
        <hr>
        <div class="form-row mt-2">
            <div class="col">
                <div class="custom-control custom-switch">
                    <input type="checkbox" class="custom-control-input" id="input_hide_normal">
                    <label class="custom-control-label text-primary" for="input_hide_normal">隱藏正常</label>
                </div>
            </div>
        </div>
        <div class="form-row">
            <div class="col">
                <div class="FrozenTable" style="max-height: 72vh; font-size:0.9rem;">
                    <table class="table table-bordered sortable">
                        <thead>
                            <tr>
                                <th style="width: 6%;">操作</th>
                                <th style="width: 4%;">流水號</th>
                                <th style="width: 8%;">盤點時間</th>
                                <th style="width: 8%;">預定盤點日期</th>
                                <th style="width: 15%;">備註</th>
                                <th class="p-0">
                                    <table class="inner-table">
                                        <thead>
                                            <tr>
                                                <th>料號</th>
                                                <th>品名</th>
                                                <th>盤點<br>主倉庫存</th>
                                                <th>盤點<br>出貨倉庫存</th>
                                                <th>TMS<br>主倉庫存</th>
                                                <th>TMS<br>出貨倉庫存</th>
                                                <th>主倉差異</th>
                                                <th>出貨倉差異</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $main)
                                <tr class="{{ empty($main->completed_at) ? '' : 'table-success' }}">
                                    <td class="p-0">
                                        <div class="d-flex flex-column justify-content-center flex-wrap">
                                            @if (empty($main->completed_at))
                                                @can('inventory_edit')
                                                    <div class="my-1">
                                                        <a class="btn btn-warning btn-sm"
                                                            href="{{ route('inventory.edit', $main->id) }}">
                                                            <i class="bi bi-pencil-fill"></i>編輯
                                                        </a>
                                                    </div>
                                                    <div class="my-1">
                                                        <form method="POST"
                                                            action="{{ route('inventory.destroy', $main->id) }}"onsubmit="return confirm('確定要刪除?');">
                                                            @csrf
                                                            <input name="_method" type="hidden" value="DELETE">
                                                            <button class="btn btn-danger btn-sm">
                                                                <i class="bi bi-trash"></i>刪除
                                                            </button>
                                                        </form>
                                                    </div>
                                                @endcan
                                                @if (empty($main->inverntory_time))
                                                    @can('inventory_done')
                                                        <div class="my-1">
                                                            <form method="POST"
                                                                action="{{ route('inventory.inventory_done', $main->id) }}"
                                                                onsubmit="return confirm('確定要完成盤點?');">
                                                                @csrf
                                                                <button class="btn btn-info btn-sm">
                                                                    <i class="bi bi-check2-circle"></i>盤完
                                                                </button>
                                                            </form>
                                                        </div>
                                                    @endcan
                                                @else
                                                    <div class="my-1">
                                                        <form method="POST"
                                                            action="{{ route('inventory.export', $main->id) }}">
                                                            @csrf
                                                            <button class="btn btn-success btn-sm">
                                                                <i class="bi bi-file-earmark-excel"></i>匯出
                                                            </button>
                                                        </form>
                                                    </div>
                                                    @if (Auth::user()->can('inventory_complete') ||
                                                            ($main->Details->where('main_diff', '!=', 0)->count() == 0 &&
                                                                $main->Details->where('sale_diff', '!=', 0)->count() == 0))
                                                        <div class="my-1">
                                                            <form method="POST"
                                                                action="{{ route('inventory.complete', $main->id) }}">
                                                                @csrf
                                                                <button class="btn btn-info btn-sm">
                                                                    <i class="bi bi-check2-circle"></i>結案
                                                                </button>
                                                            </form>
                                                        </div>
                                                    @else
                                                        審核中
                                                    @endif
                                                @endif
                                            @else
                                                已結案
                                            @endif
                                        </div>
                                    </td>
                                    <td>{{ $main->id }}</td>
                                    <td>
                                        @if (empty($main->inverntory_time))
                                            @if ($main->appointment_date == date('Y-m-d'))
                                                盤點中
                                            @else
                                                預排中
                                            @endif
                                        @else
                                            {{ $main->inverntory_time }}
                                        @endif
                                    </td>
                                    <td>{{ $main->appointment_date }}</td>
                                    <td>{{ $main->remark }}</td>
                                    <td class="p-0">
                                        <table class="inner-table">
                                            <colgroup>
                                                <col span="2">
                                                <col span="2" class="table-info">
                                                <col span="2" class="table-warning">
                                            </colgroup>
                                            <tbody>
                                                @foreach ($main->Details as $detail)
                                                    <?php
                                                    if ($detail->main_diff == 0) {
                                                        $main_class = '';
                                                    } elseif ($detail->main_diff > 0) {
                                                        $main_class = 'bg-success text-white';
                                                    } else {
                                                        $main_class = 'bg-danger text-white';
                                                    }
                                                    if ($detail->sale_diff == 0) {
                                                        $sale_class = '';
                                                    } elseif ($detail->sale_diff > 0) {
                                                        $sale_class = 'bg-success text-white';
                                                    } else {
                                                        $sale_class = 'bg-danger text-white';
                                                    }
                                                    ?>
                                                    <tr
                                                        class="{{ !empty($main->inverntory_time) && $detail->main_diff == 0 && $detail->sale_diff == 0 ? 'normal' : '' }}">
                                                        <td>{{ $detail->item_no }}</td>
                                                        <td class="text-left">{{ $detail->item_name }}</td>
                                                        <td>{{ number_format($detail->main_stock) }}</td>
                                                        <td>{{ number_format($detail->sale_stock) }}</td>
                                                        <td>{{ number_format($detail->tms_main_stock) }}</td>
                                                        <td>{{ number_format($detail->tms_sale_stock) }}</td>
                                                        <td class="{{ $main_class }}">
                                                            {{ number_format($detail->main_diff) }}</td>
                                                        <td class="{{ $sale_class }}">
                                                            {{ number_format($detail->sale_diff) }}</td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop
@section('script')
    <script type="text/javascript">
        $(document).ready(function() {
            InitialDate();
        });

        function InitialDate() {
            var today = new Date();
            if ($("#input_start_date").val() == '') {
                $("#input_start_date").val(DateToString(today));
            }
            if ($("#input_end_date").val() == '') {
                $("#input_end_date").val(DateToString(today));
            }
        }
        //隱藏正常
        $('#input_hide_normal').on('change', function() {
            $('.normal').toggle(!$(this).is(":checked"));
        });
    </script>
@stop
