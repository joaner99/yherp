@extends('layouts.base')
@section('title')
    盤點作業-新增
@stop
@section('css')
    <style type="text/css">
        #select_unselected_items,
        #select_selected_items {
            min-height: 50vh;
        }
    </style>
@endsection
@section('content')
    <div class="container-fluid">
        @if ($errors->any())
            <div class="form-row mb-2">
                <div class="col-auto">
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        @endif
        <div class="form-row">
            <div class="col-auto">
                <a class="btn btn-secondary" href="{{ route('inventory.index') }}">回上頁</a>
            </div>
        </div>
        <hr>
        <form action="{{ route('inventory.store') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-row">
                <div class="col-3">
                    <div class="form-group">
                        <label class="text-danger" for="input_appointment_date">*盤點預定日期</label>
                        <input class="form-control" id="input_appointment_date" name="appointment_date" type="date"
                            required>
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <label for="input_remark">備註<small>(50字)</small></label>
                        <input class="form-control" id="input_remark" name="remark" type="text" maxlength="50">
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div id="div_items" class="col">
                    <div class="card mt-2">
                        <div class="card-header">
                            盤點品項
                        </div>
                        <div class="card-body">
                            <div class="form-row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="select_unselected_items">未選取</label>
                                        <select class="form-control" id="select_unselected_items"
                                            style="background-color: lightgoldenrodyellow;" multiple>
                                            @if (!empty($items))
                                                @foreach ($items as $item)
                                                    <option value="{{ $item->item_no }}">
                                                        {{ '【' . $item->item_no . '】' . $item->item_name }}
                                                    </option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-2">
                                    <div class="form-row flex-column text-center mt-5">
                                        <div class="col mb-2">
                                            <button id="btn_add" class="btn btn-success" style="width: 85% !important;"
                                                type="button">
                                                新增<i class="bi bi-chevron-right"></i>
                                            </button>
                                        </div>
                                        <div class="col mb-2">
                                            <button id="btn_remove" class="btn btn-secondary" style="width: 85% !important;"
                                                type="button">
                                                <i class="bi bi-chevron-left"></i>移除
                                            </button>
                                        </div>
                                        <div class="col mb-2">
                                            <button id="btn_remove_all" class="btn btn-danger"
                                                style="width: 85% !important;" type="button">
                                                <i class="bi bi-chevron-double-left"></i>全部移除
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label for="select_selected_items">已選取</label>
                                        <select class="form-control" id="select_selected_items"
                                            style="background-color: aliceblue" multiple>
                                            @if (!empty($items))
                                                @foreach ($items as $item)
                                                    <option value="{{ $item->item_no }}"
                                                        data-item_name="{{ $item->item_name }}" style="display: none;">
                                                        {{ '【' . $item->item_no . '】' . $item->item_name }}
                                                    </option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <input id="input_items" name="items" type="hidden">
            <button id="btn_store" class="btn btn-primary mt-2" type="submit">確認</button>
        </form>
    </div>
@stop
@section('script')
    <script type="text/javascript">
        $(document).ready(function() {
            InitialDate();
        });

        function InitialDate() {
            var today = new Date();
            if ($("#input_appointment_date").val() == '') {
                $("#input_appointment_date").val(DateToString(today));
            }
        }
        //加入選取的商品
        $('#btn_add').click(function() {
            $('#select_unselected_items>option:selected').each(function(index, element) {
                var val = $(element).val();
                $('#select_selected_items>option[value="' + val + '"]').show();
                $(element).hide();
            });
        });
        //移除選取的商品
        $('#btn_remove').click(function() {
            $('#select_selected_items>option:selected').each(function(index, element) {
                var val = $(element).val();
                $('#select_unselected_items>option[value="' + val + '"]').show();
                $(element).hide();
            });
        });
        //移除全部的商品
        $('#btn_remove_all').click(function() {
            $('#select_selected_items>option').hide();
            $('#select_unselected_items>option').show();
        });
        $('#btn_store').click(function() {
            var items = [];
            $('#select_selected_items>option:visible').each(function(i, e) {
                var item_no = $(e).val();
                var item_name = $(e).data().item_name;
                items.push({
                    item_no,
                    item_name
                });
            });
            $('#input_items').val(JSON.stringify(items));
        });
    </script>
@stop
