@extends('layouts.base')
@section('title')
    新增問題
@stop
@section('css')
    <style type="text/css">
        .kind,
        .kind2,
        .kind3 {
            text-align: center;
        }

        #div_items select {
            min-height: 300px;
        }

        textarea {
            height: 250px;
        }

        /*圖片預覽*/
        #div_imgs>div {
            display: flex;
            position: relative;
            margin: 0 0.25rem;
            max-width: 320px;
            max-height: 180px;
        }

        #div_imgs>div>img {
            width: 100%;
            height: auto;
            max-width: inherit;
            max-height: inherit;
        }

        #div_imgs>div>.btn {
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            -ms-transform: translate(-50%, -50%);
            display: none;
        }

        #div_imgs>div:hover .btn {
            display: inline-block;
        }
    </style>
@endsection
@section('content')
    <div class="container-fluid">
        @if ($errors->any())
            <div class="form-row mb-2">
                <div class="col-auto">
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        @endif
        <div class="form-row">
            <div class="col-auto">
                <a class="btn btn-secondary" href="{{ route('help.edit_index') }}">回上頁</a>
            </div>
        </div>
        <hr>
        <form method="POST" action="{{ route('help.store') }}" enctype="multipart/form-data" class="mb-2">
            @csrf
            <div class="form-row mt-2">
                <div class="col-6">
                    <div class="form-group">
                        <label class="text-danger" for="input_question">*問題內容</label>
                        <small class="text-muted">(50字)</small>
                        <input class="form-control" id="input_question" name="question" type="text" maxlength="50"
                            required>
                    </div>
                </div>
                <div class="col-3 d-none">
                    <div class="form-group">
                        <label for="select_group">問題群組</label>
                        <select class="custom-select" id="select_group" name="group">
                            <option value="" selected>請選擇...</option>
                            @if (!empty($groups))
                                @foreach ($groups as $group)
                                    <option value="{{ $group->id }}">{{ $group->name }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col">
                    <div class="form-group">
                        <label class="text-danger" for="input_answer">*答案內容</label>
                        <small class="text-muted">(500字)</small>
                        <textarea class="form-control" id="input_answer" name="answer" maxlength="65535" required></textarea>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col">
                    <div class="form-group">
                        <label for="input_remark">內部備註</label>
                        <small class="text-muted">(500字)</small>
                        <textarea class="form-control" id="input_remark" name="remark" maxlength="65535"></textarea>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-6 form-group">
                    <label class="text-danger">*銷售平台</label>
                    <div class="form-check-list-horizontal">
                        @foreach ($shops as $shop)
                            <div class="form-check">
                                <input class="form-check-input" id="input_shop_{{ $shop->id }}" name="shops[]"
                                    type="checkbox" value="{{ $shop->id }}" checked>
                                <label class="form-check-label" for="input_shop_{{ $shop->id }}">
                                    {{ $shop->name }}
                                </label>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="col-3 form-group">
                    <label class="text-danger">*行為</label>
                    <div class="form-check-list-horizontal">
                        @foreach ($actions as $action)
                            <div class="form-check">
                                <input class="form-check-input" id="input_action_{{ $action->id }}" name="actions[]"
                                    type="checkbox" value="{{ $action->id }}" checked>
                                <label class="form-check-label" for="input_action_{{ $action->id }}">
                                    {{ $action->name }}
                                </label>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-9">
                    <div class="form-group">
                        <label>圖片</label>
                        <div class="input-group mb-3">
                            <div class="custom-file">
                                <input class="custom-file-input" id="input_files" name="files[]" type="file"
                                    accept="image/*" onchange="LoadFile()" multiple>
                                <label class="custom-file-label" for="input_files">請選擇圖片</label>
                            </div>
                            <button class="btn btn-danger ml-3" id="btn_clear_img" type="button">清除所有圖片</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-row mb-3">
                <div class="col d-flex" id="div_imgs"></div>
            </div>
            <div class="form-row">
                <div class="col-auto">
                    <div class="form-group">
                        <label class="text-danger">*問題歸類</label>
                        <div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" id="input_type_0" name="type" class="custom-control-input"
                                    value="0" checked>
                                <label class="custom-control-label" for="input_type_0">通用類</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" id="input_type_1" name="type" class="custom-control-input"
                                    value="1">
                                <label class="custom-control-label" for="input_type_1">指定大中小類</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" id="input_type_2" name="type" class="custom-control-input"
                                    value="2">
                                <label class="custom-control-label" for="input_type_2">指定料號</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="div_kinds" class="form-row" style="display: none;">
                <div class="col">
                    <div class="card mt-2">
                        <h1 class="card-header text-info font-weight-bold">指定大中小類</h1>
                        <div class="card-body">
                            <button class="btn btn-info" type="button" id="btn_create">
                                <i class="bi bi-plus-lg"></i>新增
                            </button>
                            <div class="FrozenTable mt-2" style="max-height: 30vh;">
                                <table class="table table-bordered sortable">
                                    <thead>
                                        <tr>
                                            <th style="width: 10%;">大類代碼</th>
                                            <th style="width: 20%;">大類名稱</th>
                                            <th style="width: 10%;">中類代碼</th>
                                            <th style="width: 20%;">中類名稱</th>
                                            <th style="width: 10%;">小類代碼</th>
                                            <th style="width: 20%;">小類名稱</th>
                                            <th style="">操作</th>
                                        </tr>
                                    </thead>
                                    <tbody id="tbody_items"></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="div_items" class="form-row" style="display: none;">
                <div class="col">
                    <div class="card mt-2">
                        <h1 class="card-header text-info font-weight-bold">指定料號</h1>
                        <div class="card-body">
                            <div class="form-row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="select_unselected_items">未選取</label>
                                        <select class="form-control" id="select_unselected_items"
                                            style="background-color: lightgoldenrodyellow;" multiple>
                                            @if (!empty($items))
                                                @foreach ($items as $item)
                                                    <option value="{{ $item->ICODE }}">
                                                        {{ '【' . $item->ICODE . '】' . $item->INAME }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-1">
                                    <div class="form-row flex-column text-center mt-5">
                                        <div class="col mb-2">
                                            <button id="btn_add" class="btn btn-info w-75" type="button">
                                                <i class="bi bi-chevron-right"></i>
                                            </button>
                                        </div>
                                        <div class="col mb-2">
                                            <button id="btn_remove" class="btn btn-info w-75" type="button">
                                                <i class="bi bi-chevron-left"></i>
                                            </button>
                                        </div>
                                        <div class="col mb-2">
                                            <button id="btn_remove_all" class="btn btn-info w-75" type="button">
                                                <i class="bi bi-chevron-double-left"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label for="select_selected_items">已選取</label>
                                        <select class="form-control" id="select_selected_items"
                                            style="background-color: aliceblue" multiple>
                                            @if (!empty($items))
                                                @foreach ($items as $item)
                                                    <option value="{{ $item->ICODE }}" style="display: none;">
                                                        {{ '【' . $item->ICODE . '】' . $item->INAME }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <button id="btn_confirm" class="btn btn-primary" type="submit">確認</button>
            <input id="input_kinds" name="kinds" type="hidden">
            <input id="input_items" name="items" type="hidden">
            <input id="input_add_imgs" name="add_imgs[]" type="file" class="d-none" multiple>
        </form>
    </div>
    <datalist id="kind_list">
        @if (!empty($types))
            @foreach ($types[1] as $value => $text)
                <option value="{{ $value }}">{{ $text }}</option>
            @endforeach
        @endif
    </datalist>
    <datalist id="kind2_list">
        @if (!empty($types))
            @foreach ($types[2] as $value => $text)
                <option value="{{ $value }}">{{ $text }}</option>
            @endforeach
        @endif
    </datalist>
    <datalist id="kind3_list">
        @if (!empty($types))
            @foreach ($types[3] as $value => $text)
                <option value="{{ $value }}">{{ $text }}</option>
            @endforeach
        @endif
    </datalist>
    <datalist id="item_list">
        @if (!empty($items))
            @foreach ($items as $item)
                <option value="{{ $item->ICODE }}">{{ $item->INAME }}</option>
            @endforeach
        @endif
    </datalist>
@stop
@section('script')
    <script src="{{ asset('js/ckeditor.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            CKEDITOR.replace('answer');
            CKEDITOR.replace('remark');
        });
        //通用類與指定大中小類切換
        $('[name="type"]').on('change', function() {
            $('#div_kinds,#div_items').hide();
            if ($('#input_type_1').prop('checked')) { //大中小類
                $('#btn_create').focus();
                $('#div_kinds').show();
            } else if ($('#input_type_2').prop('checked')) { //料號
                $('#div_items').show();
                $('#select_unselected_items').focus();
            }
        });
        //大中小類更新text
        function onKindChange(obj, kind) {
            var val = $(obj).val();
            var text = $('#' + kind + '_list>option[value="' + val + '"]').text();
            //input->td->tr
            var tr = $(obj).parent().parent();
            tr.find('.' + kind + '_text').text(text);
            KindCheck(tr);
        }
        //大中小類規則檢查
        function KindCheck(obj) {
            var kind = $(obj).find('.kind'); //大類
            var kind2 = $(obj).find('.kind2'); //中類
            var kind3 = $(obj).find('.kind3'); //小類
            if (!IsNullOrEmpty(kind.val())) {
                kind2.show();
                if (!IsNullOrEmpty(kind2.val())) {
                    kind3.show();
                } else {
                    kind3.hide();
                    kind3.val('');
                    $(obj).find('.kind3_text').text('');
                }
            } else {
                kind2.hide();
                kind2.val('');
                $(obj).find('.kind2_text').text('');
                kind3.hide();
                kind3.val('');
                $(obj).find('.kind3_text').text('');
            }
        }
        //確認
        $("#btn_confirm").click(function() {
            if ($('#input_type_0').prop('checked')) { //通用
                return;
            } else if ($('#input_type_1').prop('checked')) { //大中小類
                var detailList = new Array();
                $("#tbody_items>tr").each(function(index, element) {
                    var kind = $(element).find(".kind").val();
                    var kind2 = $(element).find(".kind2").val();
                    var kind3 = $(element).find(".kind3").val();
                    //檢查階層
                    if (IsNullOrEmpty(kind) && IsNullOrEmpty(kind2) && IsNullOrEmpty(kind3)) {
                        return;
                    } else if (!IsNullOrEmpty(kind3) && (IsNullOrEmpty(kind) || IsNullOrEmpty(kind2))) {
                        return;
                    } else if (!IsNullOrEmpty(kind2) && IsNullOrEmpty(kind)) {
                        return;
                    }
                    var detail = {
                        kind: kind,
                        kind2: kind2,
                        kind3: kind3,
                    };
                    detailList.push(detail);
                });
                $("#input_kinds").val(JSON.stringify(detailList));
            } else { //料號
                var items = new Array();
                $('#select_selected_items>option:visible').each(function(index, element) {
                    var item_no = $(element).val();
                    var item = {
                        item_no: item_no,
                    };
                    items.push(item);
                });
                $("#input_items").val(JSON.stringify(items));
            }

        });
        //建立明細
        $("#btn_create").click(function() {
            var dom = '';
            dom += '<tr>';
            dom +=
                '<td><input class="form-control kind" type="text" list="kind_list" onchange="onKindChange(this,\'kind\');"></td>';
            dom += '<td><span class="kind_text"></span></td>';
            dom +=
                '<td><input style="display: none;" class="form-control kind2" type="text" list="kind2_list" onchange="onKindChange(this,\'kind2\');"></td>';
            dom += '<td><span class="kind2_text"></span></td>';
            dom +=
                '<td><input style="display: none;" class="form-control kind3" type="text" list="kind3_list" onchange="onKindChange(this,\'kind3\');"></td>';
            dom += '<td><span class="kind3_text"></span></td>';
            dom +=
                '<td><button class="btn btn-danger btn-sm" type="button"onclick="ItemDelete(this)"><i class="bi bi-trash"></i>刪除</button></td>';
            dom += '</tr>';

            $("#tbody_items").append(dom);
            $("#tbody_items>tr:last-child>td:eq(0)>input").focus();
        });
        //明細刪除按鈕
        function ItemDelete(obj) {
            $(obj).closest('tr').remove();
        }
        //加入選取的商品
        $('#btn_add').click(function() {
            $('#select_unselected_items>option:selected').each(function(index, element) {
                var val = $(element).val();
                $('#select_selected_items>option[value="' + val + '"]').show();
                $(element).hide();
            });
        });
        //移除選取的商品
        $('#btn_remove').click(function() {
            $('#select_selected_items>option:selected').each(function(index, element) {
                var val = $(element).val();
                $('#select_unselected_items>option[value="' + val + '"]').show();
                $(element).hide();
            });
        });
        //移除全部的商品
        $('#btn_remove_all').click(function() {
            $('#select_selected_items>option').hide();
            $('#select_unselected_items>option').show();
        });
        //上傳檔案後顯示文件名稱
        $('#input_files').change(function(e) {
            var fileName = '';
            $(e.target.files).each(function(index, element) {
                if (fileName != '') {
                    fileName += ',';
                }
                fileName += element.name;
            });
            $(this).next('.custom-file-label').html(fileName);
        });
        //載入上傳圖片
        function LoadFile() {
            //更新暫存的圖片
            var new_files = $('#input_files')[0].files; //新增的圖片
            var files = $('#input_add_imgs')[0].files; //暫存的圖片
            var dt = new DataTransfer();
            var sIdx = files.length;
            $(files).each(function(index, element) {
                dt.items.add(element);
            });
            $(new_files).each(function(index, element) {
                dt.items.add(element);
            });
            files = dt.files;
            $('#input_add_imgs')[0].files = files;
            //清空上傳的資料
            $('#input_files')[0].files = null;
            $('#input_files').val('');
            $('#input_files').next('.custom-file-label').html('');
            SetImage(files);
        };
        //顯示預覽圖
        function SetImage(files) {
            $('#div_imgs>div[data-idx]').remove();
            $(files).each(function(index, element) {
                var div = document.createElement('div');
                div.dataset.idx = index;
                var img = document.createElement('img');
                img.src = URL.createObjectURL(element);
                img.onload = function() {
                    URL.revokeObjectURL(img.src) // free memory
                }
                div.append(img);
                var btn = '<button class="btn btn-danger" type="button" onclick="DeleteImg(this)">X</button>';
                div.insertAdjacentHTML('beforeend', btn)
                $('#div_imgs').append(div);
            });
        }
        //刪除圖片
        function DeleteImg(obj) {
            var div = $(obj).parent()[0]; //新上傳的圖片，刪除
            $(div).remove();
            var idx = $(div).data().idx;
            var files = $('#input_add_imgs')[0].files; //暫存的圖片
            var dt = new DataTransfer();
            $(files).each(function(index, element) {
                if (index != idx) {
                    dt.items.add(element);
                }
            });
            files = dt.files;
            $('#input_add_imgs')[0].files = files;
            SetImage(files);
        }
        //刪除所有圖片
        $('#btn_clear_img').click(function() {
            //新上傳的圖片
            $('#div_imgs>div[data-idx]').remove();
            $('#input_add_imgs')[0].files = null;
            $('#input_add_imgs').val('');
        });
    </script>
@stop
