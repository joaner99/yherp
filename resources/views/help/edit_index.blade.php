@extends('layouts.base')
@section('title')
    編輯題庫清單
@stop
@section('css')
    <style type="text/css">
        .answer,
        .remark {
            text-align: left !important;
            font-size: 0.9rem;
            max-width: 200px;
            overflow: auto;
        }

        .shop,
        .action,
        .relation {
            font-size: 0.9rem;
        }

        .imgs>img {
            display: flex;
            position: relative;
            margin: 0 0.25rem;
            max-height: 100px;
        }

        #modal_image img {
            max-width: 100%;
            margin: auto;
        }
    </style>
@endsection
@section('content')
    {{-- Modal 詳細圖片 --}}
    <div id="modal_image" class="modal">
        <div class="modal-dialog modal-dialog-centered" style="max-width: 95%;">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">詳細圖片</h5>
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div style="overflow-y: auto; max-height: 75vh;">
                        <img id="img_detail" class="d-block">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">關閉</button>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="form-row">
            <div class="col-auto">
                <a class="btn btn-secondary btn-sm" href="{{ route('help.index') }}">回上頁</a>
            </div>
            <div class="col-auto mt-1 ml-3">
                <div class="form-check">
                    <form action="{{ route('help.edit_index') }}" method="GET">
                        <input class="form-check-input" type="checkbox" id="chk_with_trashed" name="with_trashed"
                            value='1' {{ request()->has('with_trashed') ? 'checked' : '' }}>
                        <label class="form-check-label" for="chk_with_trashed">
                            只顯示作廢題目
                        </label>
                        <button id="submit" class="d-none" type="submit"></button>
                    </form>
                </div>
            </div>
            <div class="col"></div>
            <div class="col-auto">
                <a class="btn btn-info btn-sm" href="{{ route('help.create') }}">
                    <i class="bi bi-plus-lg"></i>新增題庫
                </a>
            </div>
            <div class="col-auto">
                <form action="{{ route('export_table') }}" method="post">
                    @csrf
                    <input name="export_name" type="hidden" value="幫助中心題庫">
                    <input name="export_data" type="hidden">
                    <button class="btn btn-success btn-sm" type="submit" export-target="#table_data">
                        <i class="bi bi-file-earmark-excel"></i>匯出簡易Excel
                    </button>
                </form>
            </div>
        </div>
        <hr>
        <div class="form-row">
            <div class="col">
                @if (!empty($data) && count($data) > 0)
                    <div class="FrozenTable" style="font-size: 0.9rem; max-height: 85vh;">
                        <table id="table_data" class="table table-bordered sortable" style="min-width: 120vw;">
                            <thead>
                                <tr>
                                    <th style="width: 4%;">流水號</th>
                                    <th style="width: 10%">平台</th>
                                    <th style="width: 5%;">行為</th>
                                    @if (!$isEmptyGroup)
                                        <th style="width: 8%;">問題群組</th>
                                    @endif
                                    <th style="width: 10%;">問題內容</th>
                                    <th>答案內容</th>
                                    <th style="width: 15%;">內部備註</th>
                                    <th class="export-none" style="width: 10%;">圖片</th>
                                    <th class="export-none" style="min-width: 600px;">問題歸類</th>
                                    <th style="width: 6%;">更新時間</th>
                                    <th class="export-none" style="width: 5%;">操作</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($data as $item)
                                    <tr>
                                        <td>{{ $item->id }}</td>
                                        <td class="export-cell-multiple">
                                            <div class="d-flex flex-column">
                                                @foreach ($item->Shops as $shop)
                                                    <span
                                                        class="badge badge-secondary my-1 shop">{{ $shop->name ?? '' }}</span>
                                                @endforeach
                                            </div>
                                        </td>
                                        <td class="export-cell-multiple">
                                            <div class="d-flex flex-column">
                                                @foreach ($item->Actions as $action)
                                                    <span
                                                        class="badge badge-success my-1 action">{{ $action->name ?? '' }}</span>
                                                @endforeach
                                            </div>
                                        </td>
                                        @if (!$isEmptyGroup)
                                            <td>{{ $item->Group->name ?? '' }}</td>
                                        @endif
                                        <td>{{ $item->content }}</td>
                                        <td class="answer">{!! $item->answer !!}</td>
                                        <td class="remark">{!! $item->remark !!}</td>
                                        <td>
                                            <div class="imgs d-flex flex-wrap">
                                                @foreach ($item->Imgs as $img)
                                                    <img src="{{ asset('storage') . '/' . $img->path }}" class="p-1"
                                                        loading="lazy">
                                                @endforeach
                                            </div>
                                        </td>
                                        <td class="relation">
                                            @if (!empty($item->Kinds) && count($item->Kinds) > 0)
                                                <div class="FrozenTable">
                                                    <table class="table table-bordered sortable">
                                                        <thead>
                                                            <tr>
                                                                <th style="min-width: 200px">大類</th>
                                                                <th style="min-width: 200px">中類</th>
                                                                <th style="min-width: 200px">小類</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @foreach ($item->Kinds as $kind)
                                                                <tr>
                                                                    <td>
                                                                        @if (!empty($kind->kind))
                                                                            【{{ $kind->kind }}】{{ $kind->kind_name ?? '' }}
                                                                        @endif
                                                                    </td>
                                                                    <td>
                                                                        @if (!empty($kind->kind2))
                                                                            【{{ $kind->kind2 }}】{{ $kind->kind2_name ?? '' }}
                                                                        @endif
                                                                    </td>
                                                                    <td>
                                                                        @if (!empty($kind->kind3))
                                                                            【{{ $kind->kind3 }}】{{ $kind->kind3_name ?? '' }}
                                                                        @endif
                                                                    </td>
                                                                </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            @elseif(!empty($item->Items) && count($item->Items))
                                                @foreach ($item->Items as $it)
                                                    <div class="text-left">
                                                        @if (!empty($it->item_no))
                                                            【{{ $it->item_no }}】{{ $it->item_name ?? '' }}
                                                        @endif
                                                    </div>
                                                @endforeach
                                            @endif
                                        </td>
                                        <td>{{ $item->updated_at }}</td>
                                        <td>
                                            <div class="d-flex flex-column">
                                                @if (empty($item->deleted_at))
                                                    <div>
                                                        <a class="btn btn-warning btn-sm my-1"
                                                            href="{{ route('help.edit', $item->id) }}">
                                                            <i class="bi bi-pencil-fill"></i>編輯
                                                        </a>
                                                    </div>
                                                    <div>
                                                        <form method="POST"
                                                            action="{{ route('help.destroy', $item->id) }}"
                                                            onsubmit="return confirm('確定要刪除【流水號 {{ $item->id }}】題目?');">
                                                            @csrf
                                                            <input name="_method" type="hidden" value="DELETE">
                                                            <button class="btn btn-danger btn-sm my-1">
                                                                <i class="bi bi-trash"></i>刪除
                                                            </button>
                                                        </form>
                                                    </div>
                                                @else
                                                    <div>
                                                        <form method="POST"
                                                            action="{{ route('help.restore', $item->id) }}"
                                                            onsubmit="return confirm('確定要復原【流水號 {{ $item->id }}】題目?');">
                                                            @csrf
                                                            @method('PATCH')
                                                            <button class="btn btn-success btn-sm my-1">
                                                                <i class="bi bi-arrow-clockwise"></i>還原
                                                            </button>
                                                        </form>
                                                    </div>
                                                    <div>
                                                        <form method="POST"
                                                            action="{{ route('help.force_destroy', $item->id) }}"
                                                            onsubmit="return confirm('確定要永久刪除【流水號 {{ $item->id }}】題目?此動作將無法還原');">
                                                            @csrf
                                                            <input name="_method" type="hidden" value="DELETE">
                                                            <button class="btn btn-danger btn-sm my-1">
                                                                <i class="bi bi-x-circle"></i>永久刪除
                                                            </button>
                                                        </form>
                                                    </div>
                                                @endif
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                @else
                    <div class="alert alert-danger">
                        找不到資料
                    </div>
                @endif
            </div>
        </div>
    </div>
@stop
@section('script')
    <script type="text/javascript">
        //checkbox勾選觸發submit
        $("#chk_with_trashed").click(function() {
            $("#submit").click();
        });

        $('.imgs img').click(function() {
            var src = $(this).prop('src');
            $('#img_detail').prop('src', src);
            $('#modal_image').modal('show');
        });
    </script>
@stop
