@extends('layouts.base')
@section('title')
    幫助中心
@stop
@section('css')
    <style type="text/css">
    </style>
@endsection
@section('content')
    <div class="container-fluid">
        <div class="form-row mb-3">
            <div class="col-8">
                <form action="{{ route('help.search') }}" method="GET">
                    <div class="form-row">
                        <div class="form-group col-7">
                            <label>銷售平台</label>
                            <div class="form-check-list-horizontal">
                                @foreach ($shops as $shop)
                                    <div class="form-check">
                                        <input class="form-check-input" id="input_shop_{{ $shop->id }}" name="shop"
                                            type="radio" value="{{ $shop->id }}"
                                            {{ (empty(old('shop')) && $loop->first) || old('shop') == $shop->id ? 'checked' : '' }}>
                                        <label class="form-check-label" for="input_shop_{{ $shop->id }}">
                                            {{ $shop->name }}
                                        </label>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="form-group col-3">
                            <label>行為</label>
                            <div class="form-check-list-horizontal">
                                @foreach ($actions as $action)
                                    <div class="form-check">
                                        <input class="form-check-input" id="input_action_{{ $action->id }}" name="action"
                                            type="radio" value="{{ $action->id }}"
                                            {{ (empty(old('action')) && $loop->first) || old('action') == $action->id ? 'checked' : '' }}>
                                        <label class="form-check-label" for="input_action_{{ $action->id }}">
                                            {{ $action->name }}
                                        </label>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-4">
                            <label>問題歸類</label>
                            <div class="form-check-list-horizontal">
                                <div class="form-check">
                                    <input class="form-check-input" id="input_type_0" name="type" type="radio"
                                        value="" checked>
                                    <label class="form-check-label" for="input_type_0">通用類</label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" id="input_type_1" name="type" type="radio"
                                        value="1">
                                    <label class="form-check-label" for="input_type_1">指定大中小類</label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" id="input_type_2" name="type" type="radio"
                                        value="2">
                                    <label class="form-check-label" for="input_type_2">指定料號</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-1 kind" style="display: none;">
                            <label for="input_kind">大類</label>
                            <input type="text" name="kind" id="input_kind" list="kind_list" class="form-control"
                                autocomplete="off" value="{{ old('kind') }}">
                        </div>
                        <div class="form-group col-1 kind" style="display: none;">
                            <label for="input_kind2">中類</label>
                            <input type="text" name="kind2" id="input_kind2" list="kind2_list" class="form-control"
                                autocomplete="off" value="{{ old('kind2') }}">
                        </div>
                        <div class="form-group col-1 kind" style="display: none;">
                            <label for="input_kind3">小類</label>
                            <input type="text" name="kind3" id="input_kind3" list="kind3_list" class="form-control"
                                autocomplete="off" value="{{ old('kind3') }}">
                        </div>
                        <div class="form-group col-2 item" style="display: none;">
                            <label for="input_td">料號</label>
                            <input type="text" name="id" id="input_td" list="item_list" class="form-control"
                                autocomplete="off" value="{{ old('id') }}">
                        </div>
                    </div>
                    <button class="btn btn-primary btn-sm" type="submit">查詢</button>
                </form>
            </div>
            <div class="col"></div>
            @can('help_modify')
                <div class="col-auto">
                    <a class="btn btn-warning btn-sm" href="{{ route('help.edit_index') }}">
                        <i class="bi bi-pencil-fill"></i>編輯題庫
                    </a>
                </div>
            @endcan
        </div>
        <datalist id="kind_list">
            @if (!empty($types))
                @foreach ($types[1] as $value => $text)
                    <option value="{{ $value }}">{{ $text }}</option>
                @endforeach
            @endif
        </datalist>
        <datalist id="kind2_list">
            @if (!empty($types))
                @foreach ($types[2] as $value => $text)
                    <option value="{{ $value }}">{{ $text }}</option>
                @endforeach
            @endif
        </datalist>
        <datalist id="kind3_list">
            @if (!empty($types))
                @foreach ($types[3] as $value => $text)
                    <option value="{{ $value }}">{{ $text }}</option>
                @endforeach
            @endif
        </datalist>
        <datalist id="item_list">
            @if (!empty($items))
                @foreach ($items as $item)
                    <option value="{{ $item->item_no }}">{{ $item->item_name }}</option>
                @endforeach
            @endif
        </datalist>
    @stop
    @section('script')
        <script type="text/javascript">
            //通用類與指定大中小類切換
            $('[name="type"]').on('change', function() {
                $('.kind,.item').hide();
                $('.kind>input,.item>input').val('');
                if ($('#input_type_1').prop('checked')) { //大中小類
                    $('.kind').show();
                } else if ($('#input_type_2').prop('checked')) { //料號
                    $('.item').show();
                }
            });
        </script>
    @stop
