@extends('layouts.master')
@section('master_title')
    幫助中心查詢結果
@stop
@section('master_css')
    <style type="text/css">
        .types>span {
            padding-left: 2rem;
        }

        .badge {
            font-size: 1.5rem;
        }

        .questions .card-body {
            max-height: 70vh;
            overflow: auto;
        }

        .question:hover {
            background-color: #999;
            color: white;
        }

        .questions .list-group-item {
            border: 0;
            cursor: pointer;
        }

        .answer .imgs img {
            max-width: 100px;
            max-height: 100px;
            margin: 0 0.25rem;
            cursor: pointer;
        }

        #modal_image img {
            max-width: 100%;
            margin: auto;
        }

        #loading {
            width: 200px;
            height: 200px;
            border-width: 20px;
            position: absolute;
            top: 0;
            bottom: 0;
            left: 0;
            right: 0;
            margin: auto;
        }

        div#lockscreen {
            width: 0;
            height: 0;
            position: fixed;
            top: 0;
            left: 0;
            display: none;
            background-color: rgba(0, 0, 0, 0.2);
        }

        /*open on another modal*/
        .modal:nth-of-type(even) {
            z-index: 1052 !important;
        }

        .modal-backdrop.show:nth-of-type(even) {
            z-index: 1051 !important;
        }
    </style>
@endsection
@section('master_body')
    {{-- Modal 問題解答 --}}
    <div id="modal_answer" class="modal">
        <div class="modal-dialog modal-dialog-centered" style="max-width: 95%;">
            <div class="modal-content answer">
                <div class="modal-header">
                    <h2 class="modal-title"></h2>
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <ul class="list-group">
                        <li class="list-group-item p-0 remark">
                            <h3 class="p-3">內部備註</h3>
                            <div class="p-3"></div>
                        </li>
                        <li class="list-group-item p-0 content">
                            <h3 class="p-3">答案</h3>
                            <div class="p-3"></div>
                        </li>
                        <li class="list-group-item p-0 imgs">
                            <h3 class="p-3">附圖</h3>
                            <div class="p-3"></div>
                        </li>
                    </ul>
                </div>
                <div class="modal-footer">
                    <div class="form-row align-items-center w-100">
                        <div class="col-auto updated_at text-danger">最後更新時間：<span></span></div>
                        <div class="col"></div>
                        <div class="col-auto">
                            <button id="btn_copy" class="btn btn-success" type="button">
                                <i class="bi bi-clipboard"></i>複製答案
                            </button>
                        </div>
                        <div class="col-auto">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">關閉</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- Modal 詳細圖片 --}}
    <div id="modal_image" class="modal" data-backdrop="static">
        <div class="modal-dialog modal-dialog-centered" style="max-width: 95%;">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="modal-title">詳細圖片</h2>
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div style="overflow-y: auto; max-height: 70vh;">
                        <img id="img_detail" class="d-block">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">關閉</button>
                </div>
            </div>
        </div>
    </div>
    {{-- 讀取鎖定畫面，需放在modal後面 --}}
    <div id="lockscreen">
        <div id="loading" class="spinner-border text-secondary"></div>
    </div>
    <div class="container-fluid">
        <div class="form-row mt-2">
            <div class="col-12">
                <h2>查詢條件</h2>
            </div>
            <div class="col-auto">
                <span class="badge badge-secondary">{{ $input_data['shop'] }}</span>
            </div>
            <div class="col-auto">
                <span class="badge badge-success">{{ $input_data['action'] }}</span>
            </div>
            <div class="col-auto">
                <span class="badge text-white types"
                    style="background-color: var(--orange)">{{ $input_data['type_name'] }}</span>
                @switch($input_data['type'])
                    @case(1)
                        @if (!empty($input_data['kind']))
                            <span class="badge badge-dark">大類【{{ $input_data['kind'] }}】{{ $input_data['kind_name'] }}</span>
                        @endif
                        @if (!empty($input_data['kind2']))
                            <span class="badge badge-dark">中類【{{ $input_data['kind2'] }}】{{ $input_data['kind2_name'] }}</span>
                        @endif
                        @if (!empty($input_data['kind3']))
                            <span class="badge badge-dark">小類【{{ $input_data['kind3'] }}】{{ $input_data['kind3_name'] }}</span>
                        @endif
                    @break

                    @case(2)
                        @if (!empty($input_data['item_no']))
                            <span class="badge badge-dark">【{{ $input_data['item_no'] }}】{{ $input_data['item_name'] }}</span>
                        @endif
                    @break

                @endswitch

            </div>
        </div>
        <hr>
        <div class="form-row questions">
            <div class="col-12">
                <h2>問題清單</h2>
            </div>
            <div class="col-4">
                @if (!empty($data['universal']) && count($data['universal']) > 0)
                    <div class="card">
                        <h1 class="card-header text-white bg-info font-weight-bold">通用類</h1>
                        <div class="card-body p-0">
                            <ul class="list-group">
                                @foreach ($data['universal']->sortBy('content') as $question)
                                    <li class="question list-group-item" data-id="{{ $question->id }}">
                                        <span>{{ $question->content }}</span>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                @endif
            </div>
            <div class="col-4">
                @if (!empty($data['kind']) && count($data['kind']) > 0)
                    <div class="card">
                        <h1 class="card-header text-white bg-success font-weight-bold">大中小類</h1>
                        <div class="card-body p-0">
                            <ul class="list-group">
                                @foreach ($data['kind']->sortBy('content') as $question)
                                    <li class="question list-group-item" data-id="{{ $question->id }}">
                                        <span>{{ $question->content }}</span>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                @endif
            </div>
            <div class="col-4">
                @if (!empty($data['item']) && count($data['item']) > 0)
                    <div class="card">
                        <h1 class="card-header text-white bg-warning font-weight-bold">料號</h1>
                        <div class="card-body p-0">
                            <ul class="list-group">
                                @foreach ($data['item']->sortBy('content') as $question)
                                    <li class="question list-group-item" data-id="{{ $question->id }}">
                                        <span>{{ $question->content }}</span>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
@stop
@section('master_js')
    <script type="text/javascript">
        var url = "{{ route('help.ajax_get_ans') }}";
        $('.question').click(function() {
            lockScreen();
            //show loading
            $('.answer .content>div').html('');
            //get answer
            var id = $(this).data().id;
            var data = '';
            $.ajax({
                url: url,
                type: "get",
                dataType: 'json',
                data: {
                    id: id,
                },
                contentType: "application/json",
                success: function(d) {
                    if (!$.isEmptyObject(d)) {
                        data = d;
                    }
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert(JSON.parse(xhr.responseText).msg);
                },
                complete: function() {
                    SetAnswer(data);
                    $('#modal_answer').modal('show');
                    unlockScreen();
                }
            });
        });
        //設定答案內容
        function SetAnswer(data) {
            var question = data.content;
            var remark = data.remark; //內部備註
            var content = data.answer; //答案
            var imgs = data.imgs; //圖片
            var updated_at = data.updated_at; //更新時間
            $('.answer .modal-title').text(question);
            //答案
            $('.answer .content>div').html(content);
            //內部備註
            if (IsNullOrEmpty(remark)) {
                $('.answer .remark').hide();
                $('.answer .remark>div').html('');
            } else {
                $('.answer .remark').show();
                $('.answer .remark>div').html(remark);
            }
            //圖片
            $('.imgs>div').html('');
            if (imgs.length > 0) {
                $(imgs).each(function(index, element) {
                    var path = element.path;
                    var img = document.createElement('img');
                    img.src = '{{ asset('storage') }}' + '/' + path;
                    img.onclick = ZoomImg;
                    img.onload = function() {
                        URL.revokeObjectURL(img.src) // free memory
                    }
                    $('.imgs>div').append(img);
                });
                $('.imgs').show();
            } else {
                $('.imgs').hide();
            }
            //更新時間
            $('.answer .updated_at>span').text(updated_at);
        }
        //複製剪貼簿
        $("#btn_copy").click(function() {
            var value = $(".content>div").text();
            const el = document.createElement('textarea');
            el.value = value;
            document.body.appendChild(el);
            el.select();
            document.execCommand('copy');
            document.body.removeChild(el);
            alert('已複製到剪貼簿');
        });
        //鎖定螢幕點選
        function lockScreen() {
            $("#lockscreen").css("width", "100%");
            $("#lockscreen").css("height", "100%");
            $("#lockscreen").css("z-index", "10000");
            $("#lockscreen").css("display", "block");
        }
        //解鎖螢幕點選
        function unlockScreen() {
            $("#lockscreen").css("width", "0");
            $("#lockscreen").css("height", "0");
            $("#lockscreen").css("z-index", "0");
            $("#lockscreen").css("display", "none");
        }
        //詳細圖片
        function ZoomImg() {
            var src = $(this).prop('src');
            $('#img_detail').prop('src', src);
            $('#modal_image').modal('show');
        }
    </script>
@stop
