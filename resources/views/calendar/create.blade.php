@extends('layouts.base')
@section('title')
    行事曆-新增
@stop
@section('css')
    <style type="text/css">

    </style>
@endsection
@section('content')
    <div class="container-fluid">
        @if ($errors->any())
            <div class="form-row mb-2">
                <div class="col-auto">
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        @endif
        <div class="form-row">
            <div class="col-auto">
                <a class="btn btn-secondary" href="{{ URL::previous() }}">返回清單</a>
            </div>
        </div>
        <hr>
        <form method="POST" action="{{ route('calendar.store') }}" enctype="multipart/form-data" class="mb-2">
            @csrf
            <div class="form-row">
                <div class="col-4">
                    <div class="form-group">
                        <label class="text-danger" for="input_s_date">*開始【08:30~18:00】</label>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <div class="custom-control custom-switch">
                                        <input type="checkbox" class="custom-control-input" id="input_all_day"
                                            name="all_day" value="1" checked>
                                        <label class="custom-control-label" for="input_all_day">整日</label>
                                    </div>
                                </span>
                            </div>
                            <input id="input_s_date" name="s_date" type="date" class="form-control" required>
                            <input style="display: none;" id="input_s_time" name="s_time" type="time"
                                class="form-control" value="08:30" min="08:30" max="18:00" step="300">
                        </div>
                    </div>
                </div>
                <div class="col-3">
                    <div class="form-group">
                        <label class="text-danger" for="input_s_date">*結束【08:30~18:00】</label>
                        <div class="input-group mb-3">
                            <input id="input_e_date" name="e_date" type="date" class="form-control" required>
                            <input style="display: none;" id="input_e_time" name="e_time" type="time"
                                class="form-control" value="18:00" min="08:30" max="18:00" step="300">
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-auto">
                    <label class="text-danger" for="select_kind">類型</label>
                    <select class="custom-select" id="select_kind" name="kind" required>
                        <option value="" selected>請選擇...</option>
                        @foreach ($kinds as $key => $kind)
                            <option value="{{ $key }}">{{ $kind }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-row" data-kind="1" style="display: none;">
                <div class="form-group col-auto">
                    <label class="text-danger" for="select_user">申請人</label>
                    <select class="custom-select" id="select_user" name="user">
                        <option value="" selected>請選擇...</option>
                        @foreach ($users as $user)
                            <option value="{{ $user->id }}">{{ $user->name2 }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-row" data-kind="2" style="display: none;">
                <div class="form-group col-auto">
                    <label class="text-danger" for="select_user2">申請人</label>
                    <select class="custom-select" id="select_user2" name="user2">
                        <option value="" selected>請選擇...</option>
                        @foreach ($users as $user)
                            <option value="{{ $user->id }}">{{ $user->name2 }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-row" data-kind="99" style="display: none;">
                <div class="form-group col">
                    <label class="form-check-label text-danger">備註</label>
                    <textarea id="txa_content" class="form-control" name="content"></textarea>
                </div>
            </div>
            <hr>
            <button id="btn_create" class="btn btn-primary" type="submit">確認</button>
        </form>
    </div>
@stop
@section('script')
    <script type="text/javascript">
        $(document).ready(InitialDate());

        function InitialDate() {
            var today = new Date();
            if ($("#input_s_date").val() == '') {
                $("#input_s_date").val(DateToString(today));
            }
            if ($("#input_e_date").val() == '') {
                $("#input_e_date").val(DateToString(today));
            }
        }

        //整日切換
        $('#input_all_day').on('change', function() {
            var is_all_day = $(this).is(":checked");
            if (is_all_day) {
                $('#input_s_time,#input_e_time').hide();
                $('#input_s_time').val('08:30');
                $('#input_e_time').val('18:00');
            } else {
                $('#input_s_time,#input_e_time').show();
            }
        });
        //類型切換
        $('#select_kind').on('change', function() {
            var kind = $(this).val();
            $('[data-kind]').each(function(i, e) {
                if ($(e).data().kind == kind) {
                    $(e).show();
                } else {
                    $(e).hide();
                }
            });
        });
        //驗證
        $('#btn_create').click(function() {
            //開始結束時間檢查
            var is_all_day = $('#input_all_day').is(":checked");
            var s = '';
            var e = '';
            if (is_all_day) {
                s = new Date($('#input_s_date').val());
                e = new Date($('#input_e_date').val());
                if (e < s) {
                    alert('結束日期必須大於等於開始日期');
                    return false;
                }
            } else {
                var s_time = $('#input_s_time').val();
                var e_time = $('#input_e_time').val();
                if (IsNullOrEmpty(s_time) || IsNullOrEmpty(e_time)) {
                    alert('非整日，時間為必填');
                    return false;
                }
                s = new Date($('#input_s_date').val() + ' ' + $s_time);
                e = new Date($('#input_e_date').val() + ' ' + $e_time);
                if (e <= s) {
                    alert('結束時間必須大於開始時間');
                    return false;
                }
            }
            //類型檢查
            var kind = $('#select_kind').val();
            if (IsNullOrEmpty(kind)) {
                alert('類型必選');
                return false;
            } else if (kind == 1) {
                if (IsNullOrEmpty($('#select_user').val())) {
                    alert('申請人必填');
                    return false;
                }
            } else if (kind == 2) {
                if (IsNullOrEmpty($('#select_user2').val())) {
                    alert('申請人必填');
                    return false;
                }
            } else if (kind == 99) {
                if (IsNullOrEmpty($('#txa_content').val())) {
                    alert('備註必填');
                    return false;
                }
            }
        });
    </script>
@stop
