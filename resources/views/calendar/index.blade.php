@extends('layouts.base')
@section('title')
    行事曆
@stop
@section('css')
    <style type="text/css">
        .week_title,
        .days {
            display: grid;
            grid-template-columns: repeat(7, 1fr);
            place-content: center;
        }

        .days>div {
            min-height: 135px;
        }

        .week_title {
            text-align: center;
            font-size: 1.25rem;
        }

        .events {
            align-content: start;
        }

        .events .event:not(:last-child) {
            border-bottom-style: solid;
            border-bottom-width: 1px;
            border-bottom-color: rgba(0, 0, 0, .125);
        }

        .event {
            cursor: pointer;
        }

        .event>.interval {
            align-content: center;
            flex-basis: 100px;
            flex-shrink: 0;
            font-size: 0.8rem;
        }

        .event>.content {
            font-size: 0.8rem;
            flex-grow: 1;
            text-align: left;
        }

        .holiday {
            background-color: rgb(252 154 254);
        }
    </style>
@endsection
@section('content')
    <div class="container-fluid">
        <div class="form-row">
            <div class="col-auto">
                <form action="{{ route('calendar.index') }}" method="GET">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">月份</span>
                        </div>
                        <input id="input_s_date" name="s_date" type="month" class="form-control"
                            value="{{ old('s_date') }}" required>
                        <div class="input-group-append">
                            <button class="btn btn-primary" type="submit">查詢</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col"></div>
            <div class="col-auto">
                <a class="btn btn-info" href="{{ route('calendar.create') }}">
                    <i class="bi bi-plus-lg"></i>新增
                </a>
            </div>
        </div>
        <div class="form-row mt-2">
            <div class="col text-center align-middle">
                <div class="border bg-secondary d-flex justify-content-center align-items-center">
                    <a class="btn btn-lg btn-secondary py-0" style="font-size: 1.5rem;"
                        href="{{ route('calendar.index') . '?s_date=' . $previous }}">
                        <i class="bi bi-chevron-left"></i>
                    </a>
                    <div class=" text-white font-weight-bolder mx-2" style="font-size: 2rem;">
                        {{ $current }}
                    </div>
                    <a class="btn btn-lg btn-secondary py-0" style="font-size: 1.5rem;"
                        href="{{ route('calendar.index') . '?s_date=' . $next }}">
                        <i class="bi bi-chevron-right"></i>
                    </a>
                </div>
                <div class="week_title">
                    @foreach ($week_title as $wt)
                        <span class="bg-warning border">{{ $wt }}</span>
                    @endforeach
                </div>
                <div class="days">
                    @for ($i = 0; $i < collect($days)->first()['day_of_week']; $i++)
                        <div></div>
                    @endfor
                    @foreach ($days as $day => $item)
                        <div class="card m-1">
                            <div
                                class="card-header p-1 text-center {{ in_array($item['day_of_week'], [0, 6]) ? 'bg-success text-white' : '' }}">
                                {{ Str::substr($day, 8, 2) }}
                            </div>
                            <div class="card-body p-1 events {{ $item['is_holiday'] ? 'holiday' : '' }}">
                                @foreach ($item['events'] as $event)
                                    <div class="d-flex event"
                                        data-url="{{ $event['is_holiday'] ? '' : route('calendar.edit', $event['id']) }}">
                                        <div class="interval">{{ $event['interval'] }}</div>
                                        @switch($event['kind'])
                                            @case(1)
                                                <div class="content">
                                                    <span class="text-primary mr-2">{{ $event['user'] }}</span>
                                                    {{ $event['content'] }}
                                                </div>
                                            @break

                                            @case(0)
                                            @case(99)
                                                <div class="content">{{ $event['content'] }}</div>
                                            @break
                                        @endswitch
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@stop
@section('script')
    <script type="text/javascript">
        HideSidebar();
        $(document).ready(InitialDate());

        function InitialDate() {
            var today = new Date();
            var mm = today.getMonth() + 1; //January is 0!
            var yyyy = today.getFullYear();
            if (mm < 10) {
                mm = '0' + mm
            }

            today = yyyy + '-' + mm;
            if ($("#input_s_date").val() == '') {
                $("#input_s_date").val(today);
            }
            if ($("#input_e_date").val() == '') {
                $("#input_e_date").val(today);
            }
        }
        $('.event').click(function() {
            var url = $(this).data().url;
            if (!IsNullOrEmpty(url)) {
                window.location.href = url;
            }
        });
    </script>
@stop
