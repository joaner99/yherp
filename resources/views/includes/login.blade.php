@section('login_css')
    <!--Login CSS-->
    <style type="text/css">
        #div_login_status {
            font-size: 2rem;
            font-weight: bolder;
            color: #dc3545;
        }

        #input_user {
            color: transparent;
            height: auto;
        }
    </style>
@stop
@section('login_body')
    <!--Login Body-->
    <div id="modal_login" class="modal">
        <div class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title">員工登入</h5>
                </div>
                <div class="modal-body">
                    <div class="form-row">
                        <div class="col">
                            <div class="input-group input-group-lg h-100">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">員工條碼</span>
                                </div>
                                <input class="form-control" id="input_user" type="text" autocomplete="off"
                                    placeholder="請使用手持式掃描機，掃描員工條碼">
                            </div>
                        </div>
                    </div>
                    <div class="form-row mt-2">
                        <div id="div_login_status" class="col"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('login_js')
    <!--Login JS-->
    <script type="text/javascript">
        var login_lock = false; //ajax login_lock
        var isLogin = false;
        var user_id = '';
        var user_name = '';
        //使用者按下Enter觸發登入
        $('#input_user').keyup(function(event) {
            if (event.keyCode === 13) {
                event.preventDefault();
                if (!login_lock) {
                    login();
                }
            }
        });
        //登入
        function login() {
            var id = $('#input_user').val();
            if (IsNullOrEmpty(id)) {
                return;
            }
            $('#div_login_status').text('');
            login_lock = true;
            $.ajax({
                url: '{{ route('user.ajax_detail') }}',
                type: "get",
                dataType: 'json',
                data: {
                    id: id,
                },
                contentType: "application/json",
                success: function(data) {
                    if (!$.isEmptyObject(data)) {
                        isLogin = true;
                        user_id = data.id;
                        user_name = data.name;
                        $('#modal_login').modal('hide');
                    }
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    var msg = JSON.parse(xhr.responseText).msg;
                    $('#div_login_status').text(msg);
                    $('#input_user').val('');
                    $('#input_user').focus();
                },
                complete: function() {
                    login_lock = false;
                }
            });
        }
        //登入開啟事件
        $('#modal_login').on('shown.bs.modal', function() {
            $('#input_user').val('');
            $('#input_user').focus();
        })
    </script>
@endsection
