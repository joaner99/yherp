@extends('layouts.base')
@section('title')
    帳號管理
@stop
@section('css')
    <style type="text/css">
        .data {
            cursor: pointer;
        }
    </style>
@endsection
@section('content')
    {{-- Modal 匯入 --}}
    <div id="modal_upload" class="modal">
        <div class="modal-dialog modal-xl modal-dialog-centered">
            <div class="modal-content">
                <form action="{{ route('account.import') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title">匯入</h5>
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="input-group mb-3">
                            <div class="custom-file">
                                <input class="custom-file-input" id="input_file" name="file" type="file"
                                    accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,.xls"required>
                                <label class="custom-file-label" for="input_file">請選擇檔案</label>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">確認</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">關閉</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="form-row">
            <div class="col-auto">
                <a class="btn btn-info btn-sm" href="{{ route('account.create') }}">
                    <i class="bi bi-plus-lg"></i>新增
                </a>
            </div>
            <div class="col-auto">
                <button class="btn btn-success btn-sm" type="button" data-toggle="modal" data-target="#modal_upload">
                    <i class="bi bi-file-earmark-excel"></i>匯入
                </button>
            </div>
            <div class="col text-success" style="display: flex; align-items: center;">
                ※點選帳號/密碼可直接複製
            </div>
        </div>
        <div class="form-row mt-2">
            <div class="col">
                <div class="FrozenTable" style="max-height:87vh;">
                    <table class="table table-bordered table-hover sortable">
                        <thead>
                            <tr>
                                <th style="width: 8%">操作</th>
                                <th style="width: 6%">流水號</th>
                                <th style="width: 20%">平台</th>
                                <th style="width: 20%">帳號</th>
                                <th style="width: 20%">密碼</th>
                                <th>備註</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (!empty($data))
                                @foreach ($data as $value)
                                    <tr>
                                        <td>
                                            @role('admin')
                                                <div>
                                                    <form method="POST"
                                                        action="{{ route('account.destroy', $value->id) }}"onsubmit="return confirm('確定要刪除?');">
                                                        @csrf
                                                        <input name="_method" type="hidden" value="DELETE">
                                                        <button class="btn btn-danger btn-sm">
                                                            <i class="bi bi-trash"></i>刪除
                                                        </button>
                                                    </form>
                                                </div>
                                            @endrole
                                        </td>
                                        <td>{{ $value['id'] }}</td>
                                        <td>{{ $value['name'] }}</td>
                                        <td class="data">{{ $value['account'] }}</td>
                                        <td class="data">{{ $value['password'] }}</td>
                                        <td>{{ $value['remark'] }}</td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop
@section('script')
    <script type="text/javascript">
        //上傳檔案後顯示文件名稱
        $('#input_file').change(function(e) {
            var fileName = e.target.files[0].name;
            $(this).next('.custom-file-label').html(fileName);
        });
        //複製剪貼簿
        $(".data").click(function() {
            var value = $(this).text();
            const el = document.createElement('textarea');
            el.value = value;
            document.body.appendChild(el);
            el.select();
            document.execCommand('copy');
            document.body.removeChild(el);
        });
    </script>
@stop
