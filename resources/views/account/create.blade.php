@extends('layouts.base')
@section('title')
    帳號管理-新增
@stop
@section('css')
    <style type="text/css">
    </style>
@endsection
@section('content')
    <div class="container-fluid">
        @if ($errors->any())
            <div class="form-row mb-2">
                <div class="col-auto">
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        @endif
        <div class="form-row mb-2">
            <div class="col-auto">
                <a class="btn btn-secondary" href="{{ route('account.index') }}">回上頁</a>
            </div>
        </div>
        <hr>
        <form method="POST" action="{{ route('account.store') }}" enctype="multipart/form-data">
            @csrf
            <div class="form-row">
                <div class="col-lg-3">
                    <div class="form-group">
                        <label class="text-danger" for="input_name">*平台名稱<small class="text-secondary">※50字內</small></label>
                        <input class="form-control" id="input_name" name="name" type="text" maxlength="50" required>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label for="input_account">帳號</label>
                        <input class="form-control" id="input_account" name="account" type="text">
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-lg-3">
                    <div class="form-group">
                        <label class="text-danger" for="input_password">*密碼</label>
                        <input class="form-control" id="input_password" name="password" type="password" required>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label class="text-danger" for="input_confirm-password">*確認密碼</label>
                        <input class="form-control" id="input_confirm-password" name="confirm-password" type="password"
                            required>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="input_remark">備註</label>
                        <input class="form-control" id="input_remark" name="remark" type="text">
                    </div>
                </div>
            </div>
            <button class="btn btn-primary mt-2" type="submit">確認</button>
        </form>
    </div>
@stop
@section('script')
    <script type="text/javascript"></script>
@stop
