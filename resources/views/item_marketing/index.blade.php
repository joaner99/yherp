@extends('layouts.base')
@section('title')
    商品行銷設定
@stop
@section('css')
    <style type="text/css">
        .items>div {
            text-align: left;
        }
    </style>
@endsection
@section('content')
    <div class="container-fluid">
        <div class="form-row">
            <div class="col"></div>
            <div class="col-auto">
                <a class="btn btn-info" href="{{ route('item_marketing.create') }}">
                    <i class="bi bi-plus-lg"></i>新增行銷
                </a>
            </div>
        </div>
        <hr>
        <div class="form-row">
            <div class="col">
                @if (!empty($data) && count($data) > 0)
                    <div class="FrozenTable" style="max-height: 85vh;">
                        <table class="table table-bordered table-hover sortable table-filter">
                            <thead>
                                <tr>
                                    <th style="width: 6%;">流水號</th>
                                    <th class="filter-col" style="width: 15%;">行銷類型</th>
                                    <th>備註/說明</th>
                                    <th style="width: 8%;">開始時間</th>
                                    <th style="width: 8%;">結束時間</th>
                                    <th>商品</th>
                                    <th style="width: 6%;">操作</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($data as $item)
                                    <tr>
                                        <td>{{ $item->id }}</td>
                                        <td>{{ $item->marketing_name }}</td>
                                        <td>{{ $item->remark }}</td>
                                        <td>{{ $item->s_dt }}</td>
                                        <td>{{ $item->e_dt }}</td>
                                        <td class="items">
                                            @foreach ($item->Details as $d)
                                                <div>{{ '【' . $d->item_no . '】' . $d->item_name }}</div>
                                            @endforeach
                                        </td>
                                        <td>
                                            <div class="d-flex flex-column">
                                                <div>
                                                    <a class="btn btn-warning btn-sm my-1"
                                                        href="{{ route('item_marketing.edit', $item->id) }}">
                                                        <i class="bi bi-pencil-fill"></i>編輯
                                                    </a>
                                                </div>
                                                <div>
                                                    <form method="POST"
                                                        action="{{ route('item_marketing.destroy', $item->id) }}"
                                                        onsubmit="return confirm('確定要刪除【流水號 {{ $item->id }}】?');">
                                                        @csrf
                                                        <input name="_method" type="hidden" value="DELETE">
                                                        <button class="btn btn-danger btn-sm my-1">
                                                            <i class="bi bi-trash"></i>刪除
                                                        </button>
                                                    </form>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                @else
                    <div class="alert alert-danger">
                        找不到資料
                    </div>
                @endif
            </div>
        </div>
    </div>
@stop
@section('script')
    <script type="text/javascript"></script>
@stop
