@extends('layouts.base')
@section('title')
    新增行銷
@stop
@section('css')
    <style type="text/css">
        #div_items select {
            min-height: 250px;
        }

        #input_remark {
            height: 150px;
        }
    </style>
@endsection
@section('content')
    <div class="container-fluid">
        @if ($errors->any())
            <div class="form-row mb-2">
                <div class="col-auto">
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        @endif
        <div class="form-row">
            <div class="col-auto">
                <a class="btn btn-secondary" href="{{ route('item_marketing.index') }}">回上頁</a>
            </div>
        </div>
        <hr>
        <form method="POST" action="{{ route('item_marketing.store') }}" class="mb-2">
            @csrf
            <div class="form-row mt-2">
                <div class="col-4">
                    <div class="form-group">
                        <label class="text-danger" for="select_marketing">*行銷類別</label>
                        <select class="custom-select" id="select_marketing" name="marketing" required>
                            <option value="" selected>請選擇...</option>
                            @if (!empty($config))
                                @foreach ($config as $cfg)
                                    <option value="{{ $cfg->value1 }}">{{ $cfg->value2 }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
                <div class="col-4">
                    <div class="form-group">
                        <label class="text-danger" for="input_s_dt">*開始日期</label>
                        <input class="form-control" id="input_s_dt" name="s_dt" type="datetime-local" required>
                    </div>
                </div>
                <div class="col-4">
                    <div class="form-group">
                        <label class="text-danger" for="input_e_dt">*結束日期</label>
                        <input class="form-control" id="input_e_dt" name="e_dt" type="datetime-local" required>
                    </div>
                </div>
            </div>
            <div class="form-row mt-2">
                <div class="col">
                    <div class="form-group">
                        <label for="input_remark">說明/備註</label>
                        <small class="text-muted">(500字)</small>
                        <textarea class="form-control" id="input_remark" name="remark" maxlength="500"></textarea>
                    </div>
                </div>
            </div>
            <div id="div_items" class="form-row">
                <div class="col">
                    <div class="card mt-2">
                        <h1 class="card-header text-info font-weight-bold">商品</h1>
                        <div class="card-body">
                            <div class="form-row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="select_unselected_items">未選取</label>
                                        <select class="form-control" id="select_unselected_items"
                                            style="background-color: lightgoldenrodyellow;" multiple>
                                            @if (!empty($items))
                                                @foreach ($items as $item)
                                                    <option value="{{ $item->ICODE }}">
                                                        {{ '【' . $item->ICODE . '】' . $item->INAME }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-1">
                                    <div class="form-row flex-column text-center mt-5">
                                        <div class="col mb-2">
                                            <button id="btn_add" class="btn btn-info w-75" type="button">
                                                <i class="bi bi-chevron-right"></i>
                                            </button>
                                        </div>
                                        <div class="col mb-2">
                                            <button id="btn_remove" class="btn btn-info w-75" type="button">
                                                <i class="bi bi-chevron-left"></i>
                                            </button>
                                        </div>
                                        <div class="col mb-2">
                                            <button id="btn_remove_all" class="btn btn-info w-75" type="button">
                                                <i class="bi bi-chevron-double-left"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label for="select_selected_items">已選取</label>
                                        <select class="form-control" id="select_selected_items"
                                            style="background-color: aliceblue" multiple>
                                            @if (!empty($items))
                                                @foreach ($items as $item)
                                                    <option value="{{ $item->ICODE }}" data-name="{{ $item->INAME }}"
                                                        style="display: none;">
                                                        {{ '【' . $item->ICODE . '】' . $item->INAME }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <button id="btn_confirm" class="btn btn-primary" type="submit">確認</button>
            <input id="input_items" name="items" type="hidden">
        </form>
    </div>
    <datalist id="item_list">
        @if (!empty($items))
            @foreach ($items as $item)
                <option value="{{ $item->ICODE }}">{{ $item->INAME }}</option>
            @endforeach
        @endif
    </datalist>
@stop
@section('script')
    <script type="text/javascript">
        //確認
        $("#btn_confirm").click(function() {
            var items = new Array();
            $('#select_selected_items>option:visible').each(function(index, element) {
                var item_no = $(element).val();
                var item_name = $(element).data().name;
                var item = {
                    item_no: item_no,
                    item_name: item_name
                };
                items.push(item);
            });
            $("#input_items").val(JSON.stringify(items));
        });
        //加入選取的商品
        $('#btn_add').click(function() {
            $('#select_unselected_items>option:selected').each(function(index, element) {
                var val = $(element).val();
                $('#select_selected_items>option[value="' + val + '"]').show();
                $(element).hide();
            });
        });
        //移除選取的商品
        $('#btn_remove').click(function() {
            $('#select_selected_items>option:selected').each(function(index, element) {
                var val = $(element).val();
                $('#select_unselected_items>option[value="' + val + '"]').show();
                $(element).hide();
            });
        });
        //移除全部的商品
        $('#btn_remove_all').click(function() {
            $('#select_selected_items>option').hide();
            $('#select_unselected_items>option').show();
        });
    </script>
@stop
