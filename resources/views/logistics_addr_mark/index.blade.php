@extends('layouts.base')
@section('title')
    物流地區標記
@stop
@section('css')
    <style type="text/css">
    </style>
@endsection
@section('content')
    <div class="container">
        <div class="form-row">
            <div class="col"></div>
            <div class="col-auto">
                <a class="btn btn-info" href="{{ route('logistics_addr_mark.create') }}">
                    <i class="bi bi-plus-lg"></i>新增
                </a>
            </div>
        </div>
        <hr>
        <div class="form-row">
            <div class="col">
                @if (!empty($data) && count($data) > 0)
                    <div class="FrozenTable" style="max-height: 85vh;">
                        <table class="table table-bordered table-hover sortable table-filter">
                            <thead>
                                <tr>
                                    <th style="width: 8%;">流水號</th>
                                    <th class="filter-col">類別</th>
                                    <th class="filter-col" style="width: 20%;">縣市</th>
                                    <th style="width: 20%;">鄉鎮市區</th>
                                    <th style="width: 10%;">操作</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($data as $item)
                                    <tr>
                                        <td>{{ $item->id }}</td>
                                        <td>
                                            @switch($item->kind)
                                                @case(1)
                                                    音速配送地區
                                                @break

                                                @case(2)
                                                    新竹偏遠地區
                                                @break
                                            @endswitch
                                        </td>
                                        <td>{{ $item->county->name }}</td>
                                        <td>{{ $item->area->name }}</td>
                                        <td>
                                            <div class="mx-1">
                                                <form method="POST"
                                                    action="{{ route('logistics_addr_mark.destroy', $item->id) }}"
                                                    onsubmit="return confirm('確定要永久刪除【流水號 {{ $item->id }}】');">
                                                    @csrf
                                                    <input name="_method" type="hidden" value="DELETE">
                                                    <button class="btn btn-danger btn-sm my-1">
                                                        <i class="bi bi-trash"></i>刪除
                                                    </button>
                                                </form>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                @else
                    <div class="alert alert-danger">
                        找不到資料
                    </div>
                @endif
            </div>
        </div>
    </div>
@stop
@section('script')
    <script type="text/javascript"></script>
@stop
