@extends('layouts.base')
@section('title')
    物流地區註記-新增
@stop
@section('css')
    <style type="text/css">

    </style>
@endsection
@section('content')
    <div class="container-fluid">
        @if ($errors->any())
            <div class="form-row mb-2">
                <div class="col-auto">
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        @endif
        <div class="form-row">
            <div class="col-auto">
                <a class="btn btn-secondary" href="{{ route('logistics_addr_mark.index') }}">回上頁</a>
            </div>
        </div>
        <hr>
        <form method="POST" action="{{ route('logistics_addr_mark.store') }}" class="mb-2">
            @csrf
            <div class="form-row mt-2">
                <div class="col-2">
                    <div class="form-group">
                        <label class="text-danger" for="select_kind">*類別</label>
                        <select class="custom-select" id="select_kind" name="kind" required>
                            <option value="" selected>請選擇...</option>
                            <option value="1">音速配送地區</option>
                            <option value="2">新竹偏遠地區</option>
                        </select>
                    </div>
                </div>
                <div class="col-2">
                    <div class="form-group">
                        <label class="text-danger" for="input_county">*縣市</label>
                        <div class="input-group">
                            <input class="form-control" id="input_county" name="county" type="text" list="county_list"
                                required>
                            <div class="input-group-append">
                                <span class="input-group-text" id="span_county">請選擇縣市</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-2">
                    <div class="form-group">
                        <label class="text-danger" for="input_area">*鄉鎮市區</label>
                        <div class="input-group">
                            <input class="form-control" id="input_area" name="area" type="text" list="area_list"
                                required>
                            <div class="input-group-append">
                                <span class="input-group-text" id="span_area">請選擇鄉鎮市區</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <button id="btn_confirm" class="btn btn-primary" type="submit">確認</button>
        </form>
    </div>
    <datalist id="county_list">
        @if (!empty($countys))
            @foreach ($countys as $county)
                <option value="{{ $county->id }}">{{ $county->name }}</option>
            @endforeach
        @endif
    </datalist>
    <datalist id="area_list">
        @if (!empty($areas))
            @foreach ($areas as $area)
                <option value="{{ $area->id }}" data-county="{{ $area->county_id }}" disabled="disabled">
                    {{ $area->name }}</option>
            @endforeach
        @endif
    </datalist>
@stop
@section('script')
    <script type="text/javascript">
        $('#input_county,#input_area').change(function() {
            var kind = $(this).prop('name');
            var val = $(this).val();
            var text = $('#' + kind + '_list>option[value="' + val + '"]').text();
            $(this).find('+div>span').text(text);
            //根據 縣市 設定 鄉鎮市區
            if (kind == 'county') {
                $('#input_area').val('');
                $('#input_area+div>span').text('請選擇鄉鎮市區');
                $('#area_list>option[data-county="' + val + '"]').prop('disabled', '');
                $('#area_list>option[data-county!="' + val + '"]').prop('disabled', 'disabled');
            }
        });
    </script>
@stop
