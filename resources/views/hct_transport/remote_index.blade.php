@extends('layouts.base')
@section('title')
    新竹偏遠查詢
@stop
@section('css')
    <style type="text/css">
    </style>
@endsection
@section('content')
    <div class="container-fluid">
        <div class="form-row mb-2">
            <div class="col-auto">
                <form action="{{ route('hct_transport.remote_index') }}" method="GET">
                    <div class="form-row">
                        <div class="form-group col-auto">
                            <label for="input_start_date">起日</label>
                            <input id="input_start_date" name="start_date" class="form-control" type="date"
                                value="{{ old('start_date') }}">
                        </div>
                        <div class="form-group col-auto">
                            <label for="input_end_date">訖日</label>
                            <input id="input_end_date" name="end_date" class="form-control" type="date"
                                value="{{ old('end_date') }}">
                        </div>
                    </div>
                    <button class="btn btn-primary" type="submit">查詢</button>
                </form>
            </div>
        </div>
        <form action="{{ route('order_confirm.store') }}" method="POST">
            @csrf
            <input name="order_type" type="hidden" value="11">
            <div class="form-row align-items-end">
                <div class="col-auto text-muted">
                    ※蝦皮1、2、3店、官網、露天
                </div>
                <div class="col"></div>
                @can('hct_remote_confirm')
                    <div class="col-auto">
                        <button class="btn btn-info" type="submit">批次確認</button>
                    </div>
                @endcan
            </div>
            <div class="form-row mt-2">
                @if (empty($data))
                    <div class="alert alert-danger">
                        無資料
                    </div>
                @else
                    <div class="col">
                        <div class="FrozenTable" style="max-height: 73vh; font-size: 0.9rem;">
                            <table id="table_data" class="table table-bordered sortable table-filter">
                                <thead>
                                    <tr>
                                        <th class="sort-none">
                                            <input id="chk_all" type="checkbox">
                                        </th>
                                        <th>序</th>
                                        <th>訂單單號</th>
                                        <th>原始訂單單號</th>
                                        <th>客戶簡稱</th>
                                        <th>備註3</th>
                                        <th>地址</th>
                                        <th class="filter-col">偏遠地區</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $index = 1; ?>
                                    @foreach ($data as $item)
                                        <tr data-id="{{ $item->order_no }}">
                                            <td>
                                                @if ($item->confirm)
                                                    ✔
                                                @elseif ($item->isRemote)
                                                    <input name="order_no[]" class="confirm" type="checkbox"
                                                        value="{{ $item->original_no }}">
                                                @endif
                                            </td>
                                            <td>{{ $index++ }}</td>
                                            <td>{{ $item->order_no }}</td>
                                            <td>{{ $item->original_no }}</td>
                                            <td>{{ $item->cust_name }}</td>
                                            <td class="text-left">{{ $item->bak3 }}</td>
                                            <td class="text-left">{{ $item->addr }}</td>
                                            <td>{{ $item->isRemote ? '✔' : '' }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                @endif
            </div>
        </form>
    </div>
@stop
@section('script')
    <script type="text/javascript">
        $(document).ready(InitialDate());

        function InitialDate() {
            var today = new Date();

            if ($("#input_start_date").val() == '') {
                $("#input_start_date").val(DateToString(today));
            }
            if ($("#input_end_date").val() == '') {
                $("#input_end_date").val(DateToString(today));
            }
        }
        //全選功能
        $('#chk_all').change(check_all);
        //業務確認訂單
        $("#btn_confirm").click(function() {
            var idList = new Array();
            $("#table_data>tbody>tr .confirm:checked").each(function(index, element) {
                idList.push($(element).closest('tr').data().id);
            });
            if (idList.length > 0) {
                $('input[name="id_list"]').val(JSON.stringify(idList));
            } else {
                alert('請勾選訂單');
                return false;
            }
        });
    </script>
@stop
