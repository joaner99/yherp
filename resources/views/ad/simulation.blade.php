@extends('layouts.master')
@section('master_title')
    廣告模擬
@stop
@section('master_css')
    <style type="text/css">
        /*checkbox size*/
        input[type="checkbox"] {
            transform: scale(1.4);
        }

        .group {
            margin: 0 0.5rem;
        }

        .group>.card-header,
        .group>.card-body {
            padding: 0.25rem 0.5rem;
        }

        .group>.card-body {
            overflow: auto;
            max-height: 100px;
        }

        .group>.card-body .list-group-item {
            padding: 0.25rem;
            font-size: 0.9rem;
        }

        .badge {
            font-size: 1.5rem;
        }

        #ul_groups>li {
            padding: 0.3rem 0.5rem;
        }

        /*checkbox list*/
        .form-check-list {
            border: 1px solid #ced4da;
            border-radius: 0.25rem;
            padding: 0.25rem 1rem;
            overflow: auto;
        }

        .form-check-list-horizontal {
            display: flex;
            padding: 0.325rem 0.5rem;
            border: 1px solid #dee2e6 !important;
            border-radius: 0.25rem;
            box-shadow: 0 0.125rem 0.25rem rgb(0 0 0 / 8%);
        }

        .form-check-list>.form-check {
            margin: 6px 0;
        }

        .form-check-list-horizontal>.form-check {
            margin: 2px 6px;
        }
    </style>
@endsection
@section('master_body')
    {{-- Modal 群組條件 --}}
    <div id="modal_group" class="modal">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">群組條件</h5>
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-row">
                        <div class="form-group col-3">
                            <label>群組條件</label>
                            <div class="form-check-list" style="height: 18vh;">
                                <div class="form-check">
                                    <input class="form-check-input" id="input_group_kind1" type="checkbox" value="ITNAM">
                                    <label class="form-check-label" for="input_group_kind1">大類</label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" id="input_group_kind2" type="checkbox" value="ITNA2">
                                    <label class="form-check-label" for="input_group_kind2">中類</label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" id="input_group_kind3" type="checkbox" value="ITNA3">
                                    <label class="form-check-label" for="input_group_kind3">小類</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-2">
                            <div class="form-row flex-column text-center mt-5">
                                <div class="col mb-2">
                                    <button id="btn_add" class="btn btn-info w-75" type="button">
                                        <i class="bi bi-chevron-right"></i>
                                    </button>
                                </div>
                                <div class="col mb-2">
                                    <button id="btn_remove" class="btn btn-info w-75" type="button">
                                        <i class="bi bi-chevron-left"></i>
                                    </button>
                                </div>
                                <div class="col mb-2">
                                    <button id="btn_remove_all" class="btn btn-info w-75" type="button">
                                        <i class="bi bi-chevron-double-left"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label for="select_selected_items">已加入</label>
                                <select class="form-control" id="select_selected_items"
                                    style="height: 18vh; background-color: aliceblue;" multiple>
                                    <option value="ITNAM,ITNA2,ITNA3" style="display: none;">【大類】【中類】【小類】</option>
                                    <option value="ITNAM,ITNA2" style="display: none;">【大類】【中類】</option>
                                    <option value="ITNAM,ITNA3" style="display: none;">【大類】【小類】</option>
                                    <option value="ITNA2,ITNA3" style="display: none;">【中類】【小類】</option>
                                    <option value="ITNAM" style="display: none;">【大類】</option>
                                    <option value="ITNA2" style="display: none;">【中類】</option>
                                    <option value="ITNA3" style="display: none;">【小類】</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button id="btn_group_confirm" type="button" class="btn btn-primary" data-dismiss="modal">確認</button>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid mt-2">
        <div class="form-row">
            <div class="col-auto">
                <form action="{{ route('ad.simulation') }}" method="GET">
                    <div class="form-row" style="min-width: 800px;">
                        <div class="form-group col-auto">
                            <label for="input_start_date">起月</label>
                            <input id="input_start_date" name="start_date" class="form-control" type="month"
                                value="{{ old('start_date') }}">
                        </div>
                        <div class="form-group col-auto">
                            <label for="input_end_date">訖月</label>
                            <input id="input_end_date" name="end_date" class="form-control" type="month"
                                value="{{ old('end_date') }}">
                        </div>
                        <div class="form-group col-auto">
                            <label>群組</label>
                            <button id="btn_group_add" class="btn btn-success btn-sm ml-2" type="button">
                                <i class="bi bi-plus-lg m-0"></i>
                            </button>
                            <div>
                                <ul id="ul_groups" class="list-group list-group-horizontal">
                                </ul>
                            </div>
                        </div>
                    </div>
                    <button id="btn_search" class="btn btn-primary btn-sm mt-2" type="submit">查詢</button>
                    <input id="input_groups" name="groups" type="hidden" value="[]">
                </form>
            </div>
        </div>
        @if (!empty($data) && count($data) > 0)
            @foreach ($data as $g_key => $d1)
                <div class="form-row mt-2">
                    <div class="col">
                        <div class="card">
                            <div class="card-header">
                                @if (empty($g_key))
                                    <span class="badge badge-secondary">未分類</span>
                                @endif
                                <span
                                    class="badge badge-success">{{ strpos($g_key, 'ITNAM') !== false ? '大類' : '' }}</span>
                                <span class="badge badge-info">{{ strpos($g_key, 'ITNA2') !== false ? '中類' : '' }}</span>
                                <span
                                    class="badge badge-warning">{{ strpos($g_key, 'ITNA3') !== false ? '小類' : '' }}</span>
                            </div>
                            <div class="card-body">
                                <div class="FrozenTable" style="max-height: 50vh; font-size: 0.85rem;">
                                    <table id="table_data" class="table table-bordered table-hover table-filter sortable">
                                        <thead>
                                            <tr>
                                                <th class="filter-col" style="width: 5%">渠道</th>
                                                @if (strpos($g_key, 'ITNAM') !== false)
                                                    <th class="filter-col">大類</th>
                                                @endif
                                                @if (strpos($g_key, 'ITNA2') !== false)
                                                    <th class="filter-col">中類</th>
                                                @endif
                                                @if (strpos($g_key, 'ITNA3') !== false)
                                                    <th class="filter-col">小類</th>
                                                @endif
                                                <th style="width: 6%">營業額</th>
                                                <th style="width: 6%">成本</th>
                                                <th style="width: 6%">毛利</th>
                                                <th style="width: 6%">毛利率</th>
                                                <th style="width: 6%">廣告費</th>
                                                <th style="width: 6%">活動費</th>
                                                <th style="width: 6%">手續費</th>
                                                <th style="width: 6%">蝦皮運費</th>
                                                <th style="width: 6%">物流運費</th>
                                                <th style="width: 6%">淨利</th>
                                                <th style="width: 6%">淨利率</th>
                                                <th style="width: 6%">廣告績效</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($d1 as $item)
                                                <tr>
                                                    <td>{{ $item['src'] }}</td>
                                                    @if (strpos($g_key, 'ITNAM') !== false)
                                                        <td>{{ $item['kind1'] }}</td>
                                                    @endif
                                                    @if (strpos($g_key, 'ITNA2') !== false)
                                                        <td>{{ $item['kind2'] }}</td>
                                                    @endif
                                                    @if (strpos($g_key, 'ITNA3') !== false)
                                                        <td>{{ $item['kind3'] }}</td>
                                                    @endif
                                                    <td>{{ number_format($item['sales']) }}</td>
                                                    <td>{{ number_format($item['cost']) }}</td>
                                                    <td>{{ number_format($item['gross_profit']) }}</td>
                                                    <td>
                                                        {{ isset($item['gross_profit_rate']) ? number_format($item['gross_profit_rate'], 2) . '%' : '' }}
                                                    </td>
                                                    <td>{{ number_format($item['ad']) }}</td>
                                                    <td>{{ number_format($item['event']) }}</td>
                                                    <td>{{ number_format($item['handling']) }}</td>
                                                    <td>{{ number_format($item['freight_shopee']) }}</td>
                                                    <td>{{ number_format($item['freight']) }}</td>
                                                    <td>{{ number_format($item['net_income']) }}</td>
                                                    <td>
                                                        {{ isset($item['net_income_rate']) ? number_format($item['net_income_rate'], 2) . '%' : '' }}
                                                    </td>
                                                    <td>{{ isset($item['ad_rate']) ? number_format($item['ad_rate']) : '' }}
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        @endif
        <datalist id="source_list">
            <option>蝦皮</option>
            <option>官網</option>
        </datalist>
        <datalist id="kind_list">
            @if (!empty($types))
                @foreach ($types[1] as $value => $text)
                    @if (!empty($text))
                        <option value="{{ $value }}">{{ $text }}</option>
                    @endif
                @endforeach
            @endif
        </datalist>
        <datalist id="kind2_list">
            @if (!empty($types))
                @foreach ($types[2] as $value => $text)
                    @if (!empty($text))
                        <option value="{{ $value }}">{{ $text }}</option>
                    @endif
                @endforeach
            @endif
        </datalist>
        <datalist id="kind3_list">
            @if (!empty($types))
                @foreach ($types[3] as $value => $text)
                    @if (!empty($text))
                        <option value="{{ $value }}">{{ $text }}</option>
                    @endif
                @endforeach
            @endif
        </datalist>
    </div>
@stop
@section('master_js')
    <script type="text/javascript">
        $(document).ready(InitialDate());

        function InitialDate() {
            var today = new Date();
            var yyyy = today.getFullYear();
            var mm = today.getMonth() + 1 - 1; //January is 0!
            if ($("#input_start_date").val() == '') {
                $("#input_start_date").val(yyyy + '-' + mm);
            }
            if ($("#input_end_date").val() == '') {
                $("#input_end_date").val(yyyy + '-' + mm);
            }
        }
        //開啟群組條件
        $('#btn_group_add').click(function() {
            $('#modal_group').modal('show');
        });
        //加入群組
        $('#btn_add').click(function() {
            var checked_kind = [];
            $('input[id^="input_group_kind"]:checked').each(function(i, e) {
                checked_kind.push($(e).val());
            });
            var val = checked_kind.join(',');
            $('#select_selected_items>option[value="' + val + '"]').show();
        });
        //移除選定群組
        $('#btn_remove').click(function() {
            $('#select_selected_items>option:selected').each(function(index, element) {
                $(element).hide();
            });
        });
        //移除全部的群組
        $('#btn_remove_all').click(function() {
            $('#select_selected_items>option').hide();
        });
        //更新群組條件
        $('#modal_group').on('hide.bs.modal', function(e) {
            var dom = '';
            var values = [];
            $('#select_selected_items>option:visible').each(function(i, e) {
                dom += '<li class="list-group-item">';
                dom += $(e).text();
                dom += '</li>';
                values.push($(e).val().split(','));
            });
            $('#ul_groups').html(dom);
            $('#input_groups').val(JSON.stringify(values));
        })
    </script>
@stop
