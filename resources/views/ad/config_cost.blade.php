@extends('layouts.base')
@section('title')
    廣告花費設定
@stop
@section('css')
    <style type="text/css">
    </style>
@endsection
@section('content')
    {{-- Modal 新增物流運費 --}}
    <div id="modal_create" class="modal">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <form method="POST" action="{{ route('ad.config_cost_store_freight') }}" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title">新增物流運費</h5>
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-row">
                            <div class="col">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">日期</span>
                                    </div>
                                    <input class="form-control" id="input_src_date" name="src_date" type="month" required>
                                    <div class="input-group-append">
                                        <span class="input-group-text">物流運費</span>
                                    </div>
                                    <input name="freight" class="form-control" type="number" step="1" min="0"
                                        max="16777215">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary" type="submit">新增</button>
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">取消</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="form-row">
            <div class="col-auto">
                <form action="{{ route('ad.config_cost') }}" method="get" class="mb-3">
                    <div class="form-group">
                        <label for="input_date">日期</label>
                        <input id="input_date" name="date" class="form-control" type="month"
                            value="{{ old('date') }}" required>
                    </div>
                    <button class="btn btn-primary" type="submit">查詢</button>
                </form>
            </div>
            <div class="col"></div>
            <div class="col-auto">
                <a class="btn btn-info" href="{{ route('ad.config_cost_create') }}">
                    <i class="bi bi-plus-lg"></i>新增花費
                </a>
            </div>
            <div class="col-auto">
                <button class="btn btn-success" type="button" data-toggle="modal" data-target="#modal_create">
                    <i class="bi bi-plus-lg"></i>新增物流運費
                </button>
            </div>
        </div>
        <div class="form-row">
            <div class="col">
                <div class="FrozenTable" style="max-height: 77vh;">
                    <table class="table table-bordered table-hover sortable">
                        <thead>
                            <tr>
                                <th>流水號</th>
                                <th>渠道</th>
                                <th>日期</th>
                                <th>廣告費</th>
                                <th>手續費</th>
                                <th>活動費</th>
                                <th>物流運費</th>
                                <th>蝦皮運費</th>
                                <th style="width: 12%;">操作</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $item)
                                <tr>
                                    <td>{{ $item->id }}</td>
                                    <td>{{ $item->Channel->name }}</td>
                                    <td>{{ (new Datetime($item->src_date))->format('Y-m') }}</td>
                                    <td>{{ number_format($item->ad) }}</td>
                                    <td>{{ number_format($item->handling) }}</td>
                                    <td>{{ number_format($item->event) }}</td>
                                    <td>{{ number_format($item->freight) }}</td>
                                    <td>{{ number_format($item->freight_shopee) }}</td>
                                    <td>
                                        <div class="d-flex justify-content-center">
                                            <div class="mx-1">
                                                <a class="btn btn-warning btn-sm my-1"
                                                    href="{{ route('ad.config_cost_edit', $item->id) }}">
                                                    <i class="bi bi-pencil-fill"></i>編輯
                                                </a>
                                            </div>
                                            <div class="mx-1">
                                                <form method="POST"
                                                    action="{{ route('ad.config_cost_destroy', $item->id) }}"
                                                    onsubmit="return confirm('確定要刪除【流水號 {{ $item->id }}】?');">
                                                    @csrf
                                                    <input name="_method" type="hidden" value="DELETE">
                                                    <button class="btn btn-danger btn-sm my-1">
                                                        <i class="bi bi-trash"></i>刪除
                                                    </button>
                                                </form>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop
@section('script')
    <script type="text/javascript">
        $(document).ready(InitialDate());

        function InitialDate() {
            var today = new Date();
            var mm = today.getMonth() + 1 - 1; //January is 0!
            var yyyy = today.getFullYear();
            if (mm < 10) {
                mm = '0' + mm
            }

            today = yyyy + '-' + mm;
            if ($("#input_date").val() == '') {
                $("#input_date").val(today);
            }
            $('#input_src_date').val(today);
            $("#input_date").attr("max", today);
        }
    </script>
@stop
