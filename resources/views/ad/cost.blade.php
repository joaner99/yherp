@extends('layouts.base')
@section('title')
    廣告每日花費
@stop
@section('css')
    <style type="text/css">
        #div_config tbody th,
        #div_config tbody td,
        #div_cost tbody th,
        #div_cost tbody td,
        #div_sales tbody th,
        #div_sales tbody td,
        #div_ad tbody th,
        #div_ad tbody td,
        #div_bonus tbody th,
        #div_bonus tbody td {
            text-align: right;
        }
    </style>
@endsection
@section('content')
    <div class="container-fluid">
        <form action="{{ route('ad.cost') }}" method="get" class="mb-3">
            <div class="form-row">
                <div class="form-group col-auto">
                    <label for="input_date">日期</label>
                    <input id="input_date" name="date" class="form-control" type="month" value="{{ old('date') }}"
                        required>
                </div>
            </div>
            <button class="btn btn-primary" type="submit">查詢</button>
        </form>
        @if (!empty($data))
            <div class="form-row mb-3">
                <div class="col-auto p-1">
                    <div class="card">
                        <div class="card-header">
                            日期：{{ $data['sDate'] . '~' . $data['eDate'] }}
                        </div>
                        <div class="card-body">
                            <div class="form-row">
                                <div class="col-auto p-1">
                                    <div id="div_sales" class="card">
                                        <div class="card-header">
                                            <span>營業額(含稅)</span>
                                            @if ($data['sales']['diff'] == 0)
                                                <span class="font-weight-bolder text-success float-right">已達標</span>
                                            @else
                                                <span class="font-weight-bolder text-danger float-right">未達標</span>
                                            @endif
                                        </div>
                                        <div class="card-body">
                                            <div class="FrozenTable">
                                                <table class="table table-bordered">
                                                    <tbody>
                                                        <tr>
                                                            <th>目標</th>
                                                            <td>{{ number_format($data['config']['salesTarget']) }}</td>
                                                        </tr>
                                                        <tr>
                                                            <th>已達成</th>
                                                            <td>{{ number_format($data['sales']['total']) }}</td>
                                                        </tr>
                                                        <tr>
                                                            <th>未達成</th>
                                                            <td>{{ number_format($data['sales']['diff']) }}</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-auto p-1">
                                    <div id="div_cost" class="card" style="min-width: 300px;">
                                        <div class="card-header bg-danger text-white">
                                            廣告花費
                                        </div>
                                        <div class="card-body">
                                            @if (empty($data['adCost']))
                                                <div class="alert alert-danger">
                                                    無資料，請匯入
                                                </div>
                                            @else
                                                <div class="FrozenTable">
                                                    <table class="table table-bordered">
                                                        <tbody>
                                                            @foreach ($data['adCost'] as $value)
                                                                <tr>
                                                                    <th>{{ $value['name'] }}</td>
                                                                    <td>{{ number_format($value['cost']) }}</td>
                                                                </tr>
                                                            @endforeach
                                                        </tbody>
                                                        <tfoot>
                                                            <tr>
                                                                <td>合計</td>
                                                                <td>{{ number_format($data['adCostTotal']) }}</td>
                                                            </tr>
                                                        </tfoot>
                                                    </table>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="col-auto p-1">
                                    <div id="div_ad" class="card">
                                        <div class="card-header bg-info text-white">
                                            廣告預算
                                        </div>
                                        <div class="card-body">
                                            <div class="FrozenTable">
                                                <table class="table table-bordered">
                                                    <tbody>
                                                        <tr>
                                                            <th>預算</th>
                                                            <td>{{ number_format($data['adBudget']['budget']) }}</td>
                                                        </tr>
                                                        <tr>
                                                            <th>已支出</th>
                                                            <td>{{ number_format($data['adCostTotal']) }}</td>
                                                        </tr>
                                                        <tr>
                                                            <th>剩餘</th>
                                                            <td>{{ number_format($data['adBudget']['remaining']) }}</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @if ($data['sales']['diff'] == 0)
                                    <div class="col-auto p-1">
                                        <div id="div_bonus" class="card">
                                            <div class="card-header bg-warning text-white">
                                                獎金
                                            </div>
                                            <div class="card-body">
                                                <div class="FrozenTable">
                                                    <table class="table table-bordered">
                                                        <tbody>
                                                            <tr>
                                                                <th>派發</th>
                                                                <td>{{ number_format($data['bonus']['value']) }}</td>
                                                            </tr>
                                                            <tr>
                                                                <th>剩餘</th>
                                                                <td>{{ number_format($data['bonus']['left']) }}</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-auto p-1">
                    <div id="div_config" class="card">
                        <div class="card-header">
                            參數
                        </div>
                        <div class="card-body">
                            <div class="FrozenTable">
                                <table class="table table-bordered">
                                    <tbody>
                                        <tr>
                                            <th>營業額目標</th>
                                            <td>{{ number_format($data['config']['salesTarget']) }}</td>
                                        </tr>
                                        <tr>
                                            <th>當年廣告門檻</th>
                                            <td>{{ $data['config']['adThreshold'] * 100 }}%</td>
                                        </tr>
                                        <tr>
                                            <th>手續費門檻</th>
                                            <td>{{ $data['config']['chargeThreshold'] * 100 }}%</td>
                                        </tr>
                                        @if (!empty($data['config']['activityThreshold']))
                                            @foreach ($data['config']['activityThreshold'] as $value)
                                                <tr>
                                                    <th>{{ $value->text }} 活動費門檻</th>
                                                    <td>{{ $value->value * 100 }}%</td>
                                                </tr>
                                            @endforeach
                                        @endif
                                        <tr>
                                            <th>獎金比例</th>
                                            <td>{{ $data['config']['bonus'] * 100 }}%</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-row mb-3 d-none">
                <div class="col">
                    <form action="{{ route('export_table') }}" method="post">
                        <button class="btn btn-success" type="submit">
                            匯出Excel
                        </button>
                    </form>
                </div>
            </div>
        @endif

    </div>
@stop
@section('script')
    <script type="text/javascript">
        $(document).ready(InitialDate());

        function InitialDate() {
            var today = new Date();
            var mm = today.getMonth() + 1; //January is 0!
            var yyyy = today.getFullYear();
            if (mm < 10) {
                mm = '0' + mm
            }

            today = yyyy + '-' + mm;
            if ($("#input_date").val() == '') {
                $("#input_date").val(today);
            }
            $("#input_date").attr("max", today);
        }
    </script>
@stop
