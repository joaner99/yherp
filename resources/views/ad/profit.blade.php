@extends('layouts.base')
@section('title')
    廣告效益
@stop
@section('css')
    <style type="text/css">
        .ad_name {
            font-size: 0.8rem;
            text-align: left !important;
        }
    </style>
@endsection
@section('content')
    <!-- Modal 圖表 -->
    <div id="modal_chart" class="modal fade">
        <div class="modal-dialog modal-dialog-centered" style="max-width: 80%;">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="modal-title">
                        <h1>商品詳細資訊</h1>
                        <h6 id="detail_name" class="p-0"></h6>
                    </div>
                    <button type="button" class="close" data-dismiss="modal">
                        <span>&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-row">
                        <div class="col">
                            <div class="card">
                                <div class="card-header">
                                    花費&銷售額圖表
                                </div>
                                <div class="card-body">
                                    <div class="form-row">
                                        <div class="form-group col-auto m-0">
                                            <label for="input_start_date">起日</label>
                                            <input id="input_start_date" name="start_date" class="form-control"
                                                type="date" value="{{ old('date') }}">
                                        </div>
                                        <div class="form-group col-auto m-0">
                                            <label for="input_end_date">訖日</label>
                                            <input id="input_end_date" name="end_date" class="form-control" type="date"
                                                value="{{ old('date') }}">
                                        </div>
                                        <div class="col-auto d-flex align-items-end">
                                            <button id="btn_chart_sales" class="btn btn-primary" type="button">查詢</button>
                                            <button id="btn_chart_sales_loading" class="btn btn-primary"
                                                style="display: none;" type="button" disabled>
                                                <span class="spinner-border spinner-border-sm"></span>
                                                處理中...
                                            </button>
                                        </div>
                                    </div>
                                    <div>
                                        <div id="chart_sales_line"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">關閉</button>
                </div>
                <input id="input_detail_shop_id" type="hidden">
                <input id="input_detail_item_id" type="hidden">
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="form-row">
            <div class="col-auto">
                <form action="{{ route('ad.profit') }}" method="GET">
                    <div class="form-row">
                        <div class="form-group col-auto mb-2">
                            <label for="input_date">日期</label>
                            <input id="input_date" name="date" class="form-control" type="date"
                                value="{{ old('date') }}" required>
                        </div>
                        <div class="form-group col-auto">
                            <label for="select_shop">賣場</label>
                            <select class="custom-select" id="select_shop" name="shop_id" required>
                                @if (!empty($shops))
                                    @foreach ($shops as $shop_id => $shop_name)
                                        @if ($loop->first && empty(old('shop_id')))
                                            <option value="{{ $shop_id }}" selected>{{ $shop_name }}</option>
                                        @else
                                            <option value="{{ $shop_id }}"
                                                {{ old('shop_id') == $shop_id ? 'selected' : '' }}>
                                                {{ $shop_name }}</option>
                                        @endif
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                    <button class="btn btn-primary btn-sm">查詢</button>
                </form>
            </div>
        </div>
        <div class="form-row mb-2">
            <div class="col-auto d-flex align-items-end">
                <span class="text-success">※只取 狀態=『進行中』、廣告類型=『商品搜尋廣告』</span>
            </div>
            <div class="col"></div>
            <div class="col-auto">
                <form action="{{ route('export_table') }}" method="post">
                    @csrf
                    <input name="export_name" type="hidden" value="廣告效益">
                    <input name="export_data" type="hidden">
                    <button id="btn_export" class="btn btn-success btn-sm" type="submit" export-target="#table_data">
                        <i class="bi bi-file-earmark-excel"></i>匯出Excel
                    </button>
                </form>
            </div>
        </div>
        <div class="form-row">
            <div class="col">
                @if (!empty($data) && count($data) > 0)
                    <div class="FrozenTable" style="max-height: 75vh; font-size: 0.85rem;">
                        <table id="table_data" class="table table-bordered table-hover sortable" style="width: 140vw">
                            <colgroup>
                                <col span="9">
                                <col span="4" style="background-color: aliceblue;">
                                <col span="4" style="background-color: antiquewhite;">
                                <col span="4" style="background-color: lightgray;">
                            </colgroup>
                            <thead>
                                <tr>
                                    <th style="width: 4%;">渠道</th>
                                    <th>名稱</th>
                                    <th style="width: 4%;">花費</th>
                                    <th style="width: 4%;">瀏覽數</th>
                                    <th style="width: 4%;">點擊數</th>
                                    <th style="width: 4%;">點擊率</th>
                                    <th style="width: 4%;">ERP<br>平均成本</th>
                                    <th style="width: 4%;">直接<br>轉換率</th>
                                    <th style="width: 4%;">每一筆轉換<br>的成本</th>
                                    <th style="width: 4%;">廣告<br>銷售數量</th>
                                    <th style="width: 4%;">廣告<br>銷售金額</th>
                                    <th style="width: 4%;">廣告<br>銷售ROAS</th>
                                    <th style="width: 4%;">廣告<br>銷售ROI</th>
                                    <th style="width: 4%;">自然流量<br>銷售數量</th>
                                    <th style="width: 4%;">自然流量<br>銷售金額</th>
                                    <th style="width: 4%;">自然流量<br>銷售ROAS</th>
                                    <th style="width: 4%;">自然流量<br>銷售ROI</th>
                                    <th style="width: 4%;">ERP<br>銷售數量</th>
                                    <th style="width: 4%;">ERP<br>銷售金額</th>
                                    <th style="width: 4%;">ERP<br>銷售ROAS</th>
                                    <th style="width: 4%;">ERP<br>銷售ROI</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($data as $item)
                                    <tr class="detail" data-shop_id="{{ $item['shop_id'] }}"
                                        data-item_id="{{ $item['item_id'] }}" data-name="{{ $item['name'] }}">
                                        <td>{{ $item['shop'] }}</td>
                                        <td class="ad_name">{{ $item['name'] }}</td>
                                        <td>{{ number_format($item['cost'], 2) }}</td>
                                        <td>{{ number_format($item['view_count']) }}</td>
                                        <td>{{ number_format($item['click_count']) }}</td>
                                        <td>{{ number_format($item['click_rate'], 2) . '%' }}</td>
                                        <td>
                                            {{ isset($item['erp_item_cost']) ? number_format($item['erp_item_cost'], 2) : '' }}
                                        </td>
                                        <td>{{ number_format($item['convert2_rate'], 2) . '%' }}</td>
                                        <td>{{ number_format($item['convert1_cost'], 2) }}</td>
                                        <td>{{ number_format($item['sales_qty']) }}</td>
                                        <td>{{ number_format($item['sales_amt']) }}</td>
                                        <td>{{ isset($item['roas']) ? number_format($item['roas'], 2) : '' }}</td>
                                        <td>{{ isset($item['roi']) ? number_format($item['roi'], 2) . '%' : '' }}</td>
                                        <td>{{ number_format($item['n_sales_qty']) }}</td>
                                        <td>{{ number_format($item['n_sales_amt']) }}</td>
                                        <td>{{ isset($item['n_roas']) ? number_format($item['n_roas'], 2) : '' }}</td>
                                        <td>{{ isset($item['n_roi']) ? number_format($item['n_roi'], 2) . '%' : '' }}</td>
                                        <td>{{ number_format($item['erp_sales_qty']) }}</td>
                                        <td>{{ number_format($item['erp_sales_amt']) }}</td>
                                        <td>{{ isset($item['erp_roas']) ? number_format($item['erp_roas'], 2) : '' }}</td>
                                        <td>
                                            {{ isset($item['erp_roi']) ? number_format($item['erp_roi'], 2) . '%' : '' }}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                @else
                    <div class="alert alert-danger">
                        無任何資料
                    </div>
                @endif
            </div>
        </div>
    </div>
@stop
@section('script')
    <script type="text/javascript">
        $(document).ready(InitialDate());

        function InitialDate() {
            var today = new Date();
            if ($("#input_date").val() == '') {
                $("#input_date").val(DateToString(today));
            }
            if ($("#input_start_date").val() == '') {
                $("#input_start_date").val(DateToString(today));
            }
            if ($("#input_end_date").val() == '') {
                $("#input_end_date").val(DateToString(today));
            }
        }
        //上傳檔案後顯示文件名稱
        $('#input_file').change(function(e) {
            var fileName = e.target.files[0].name;
            $(this).next('.custom-file-label').html(fileName);
        });

        //匯入
        $("#btn_import").click(function() {
            $("#btn_import").hide();
            $("#btn_loading").show();
        });
        //顯示商品詳細資訊
        $('.detail').click(function() {
            SetSelectData($(this).data());
            $('#chart_sales_line').hide();
            $('#modal_chart').modal('show');
        });
        //取得圖表資料並建立
        $('#btn_chart_sales').click(function() {
            lockScreen();
            $('#btn_chart_sales').hide();
            $('#btn_chart_sales_loading').show();
            var shop_id = $('#input_detail_shop_id').val();
            var item_id = $('#input_detail_item_id').val();
            var start_date = $('#input_start_date').val();
            var end_date = $('#input_end_date').val();
            url = "{{ route('ad.ajax_get_ad_sales') }}";
            $.ajax({
                url: url,
                type: "get",
                dataType: 'json',
                data: {
                    shop_id: shop_id,
                    item_id: item_id,
                    start_date: start_date,
                    end_date: end_date
                },
                contentType: "application/json",
                success: function(data) {
                    if (!$.isEmptyObject(data)) {
                        if (!IsNullOrEmpty(data.msg)) {
                            alert(data.msg);
                        } else {
                            SetChart(data.data);
                        }
                    }
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    var msg = JSON.parse(xhr.responseText).msg;
                    alert(msg);
                },
                complete: function() {
                    unlockScreen();
                    $('#btn_chart_sales').show();
                    $('#btn_chart_sales_loading').hide();
                }
            });
        });
        //設定選取商品的資料
        function SetSelectData(data) {
            var shop_id = data.shop_id;
            var item_id = data.item_id;
            var name = data.name;

            $('#detail_name').text(name);
            $('#input_detail_shop_id').val(shop_id);
            $('#input_detail_item_id').val(item_id);
        }
        //建立圖表
        function SetChart(data) {
            var x = ['x'].concat(data.x);
            var colums = [x].concat(data.y);
            var chart_sales_line = c3.generate({
                bindto: "#chart_sales_line",
                padding: {
                    top: 10,
                    bottom: 20,
                    left: 100,
                },
                title: {
                    text: '每日花費與銷售額成長'
                },
                data: {
                    x: 'x',
                    columns: colums,
                    type: 'line',
                },
                legend: {
                    position: 'right'
                },
                axis: {
                    x: {
                        label: {
                            text: '日期',
                            position: 'outer-right'
                        },
                        tick: {
                            format: '%m-%d',
                            culling: true,
                            centered: true
                        },
                        type: 'timeseries',
                        padding: {
                            left: 1000 * 60 * 60 * 12,
                            right: 1000 * 60 * 60 * 12
                        }
                    },
                    y: {
                        min: 0,
                        label: {
                            text: '銷售額',
                            position: 'outer-top'
                        },
                        tick: {
                            format: d3.format(',')
                        },
                        padding: {
                            bottom: 0
                        }
                    }
                },
            });
            $('#chart_sales_line').show();
        }
    </script>
@stop
