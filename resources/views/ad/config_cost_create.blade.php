@extends('layouts.base')
@section('title')
    廣告花費設定-新增
@stop
@section('css')
    <style type="text/css">
    </style>
@endsection
@section('content')
    <div class="container-fluid">
        @if ($errors->any())
            <div class="form-row mb-2">
                <div class="col-auto">
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        @endif
        <div class="form-row">
            <div class="col-auto">
                <a class="btn btn-secondary btn-sm" href="{{ route('ad.config_cost') }}">回上頁</a>
            </div>
        </div>
        <form method="POST" action="{{ route('ad.config_cost_store') }}">
            @csrf
            <div class="form-row mt-2">
                <div class="col-2">
                    <div class="form-group">
                        <label class="text-danger" for="select_channel">*渠道</label>
                        <select class="custom-select" id="select_channel" name="ad_channel_id" required>
                            <option value="" selected>請選擇...</option>
                            @if (!empty($ad_channel))
                                @foreach ($ad_channel as $channel)
                                    <option value="{{ $channel->id }}">{{ $channel->name }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
                <div class="col-2">
                    <div class="form-group">
                        <label class="text-danger" for="input_src_date">*日期</label>
                        <input class="form-control" id="input_src_date" name="src_date" type="month" required>
                    </div>
                </div>
            </div>
            <div class="form-row mt-2">
                <div class="col-lg-2">
                    <div class="form-group">
                        <label for="input_ad">廣告費</label>
                        <input class="form-control" id="input_ad" name="ad" type="number" value="0">
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="form-group">
                        <label for="input_handling">手續費</label>
                        <input class="form-control" id="input_handling" name="handling" type="number" value="0">
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="form-group">
                        <label for="input_event">活動費</label>
                        <input class="form-control" id="input_event" name="event" type="number" value="0">
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="form-group">
                        <label for="input_freight_shopee">蝦皮運費</label>
                        <input class="form-control" id="input_freight_shopee" name="freight_shopee" type="number"
                            value="0">
                    </div>
                </div>
            </div>
            <button id="btn_confirm" class="btn btn-primary btn-sm mt-2" type="submit">確認</button>
        </form>
    </div>
@stop
@section('script')
    <script type="text/javascript">
        $(document).ready(InitialDate());

        function InitialDate() {
            var today = new Date();
            var mm = today.getMonth() + 1 - 1; //January is 0!，預設前一月
            var yyyy = today.getFullYear();
            if (mm < 10) {
                mm = '0' + mm
            }

            today = yyyy + '-' + mm;
            if ($("#input_src_date").val() == '') {
                $("#input_src_date").val(today);
            }
            $("#input_src_date").attr("max", today);
        }
    </script>
@stop
