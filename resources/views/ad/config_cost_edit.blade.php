@extends('layouts.base')
@section('title')
    廣告花費設定-編輯
@stop
@section('css')
    <style type="text/css">
    </style>
@endsection
@section('content')
    <div class="container-fluid">
        @if ($errors->any())
            <div class="form-row mb-2">
                <div class="col-auto">
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        @endif
        <div class="form-row">
            <div class="col-auto">
                <a class="btn btn-secondary btn-sm" href="{{ route('ad.config_cost') }}">回上頁</a>
            </div>
        </div>
        <form method="POST" action="{{ route('ad.config_cost_update', $data->id) }}">
            @method('PATCH')
            @csrf
            <div class="form-row mt-2">
                <div class="col-2">
                    <div class="form-group">
                        <label class="text-danger">*渠道</label>
                        <select class="custom-select" disabled>
                            <option value="" selected>請選擇...</option>
                            @if (!empty($ad_channel))
                                @foreach ($ad_channel as $channel)
                                    <option
                                        value="{{ $channel->id }}"{{ $data->ad_channel_id == $channel->id ? 'selected' : '' }}>
                                        {{ $channel->name }}
                                    </option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
                <div class="col-2">
                    <div class="form-group">
                        <label class="text-danger">*日期</label>
                        <input class="form-control-plaintext" type="month"
                            value="{{ (new DateTime($data->src_date))->format('Y-m') }}" readonly>
                    </div>
                </div>
            </div>
            <div class="form-row mt-2">
                <div class="col-lg-2">
                    <div class="form-group">
                        <label for="input_ad">廣告費</label>
                        <input class="form-control" id="input_ad" name="ad" type="number"
                            value="{{ $data->ad }}">
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="form-group">
                        <label for="input_handling">手續費</label>
                        <input class="form-control" id="input_handling" name="handling" type="number"
                            value="{{ $data->handling }}">
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="form-group">
                        <label for="input_event">活動費</label>
                        <input class="form-control" id="input_event" name="event" type="number"
                            value="{{ $data->event }}">
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="form-group">
                        <label for="input_freight_shopee">蝦皮運費</label>
                        <input class="form-control" id="input_freight_shopee" name="freight_shopee" type="number"
                            value="{{ $data->freight_shopee }}">
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="form-group">
                        <label for="input_freight">物流運費</label>
                        <input class="form-control" id="input_freight" name="freight" type="number"
                            value="{{ $data->freight }}">
                    </div>
                </div>
            </div>
            <button id="btn_confirm" class="btn btn-primary btn-sm mt-2" type="submit">確認</button>
        </form>
    </div>
@stop
@section('script')
    <script type="text/javascript"></script>
@stop
