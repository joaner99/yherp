@extends('layouts.base')
@section('title')
    廣告報表上傳
@stop
@section('css')
    <style type="text/css">
    </style>
@endsection
@section('content')
    {{-- Modal 匯入廣告報表 --}}
    <div id="modal_upload" class="modal">
        <div class="modal-dialog modal-xl modal-dialog-centered">
            <div class="modal-content">
                <form action="{{ route('ad.import_profit') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title">匯入廣告報表</h5>
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-row">
                            <div class="form-group col-10">
                                <label>蝦皮廣告報表</label>
                                <span class="text-danger">※一次最多20個檔案</span>
                                <div class="custom-file">
                                    <input class="custom-file-input" id="input_file" name="file[]" type="file"
                                        accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,.csv"
                                        multiple required>
                                    <label class="custom-file-label" for="input_file">請選擇蝦皮廣告報表</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button id="btn_upload_ad_profit" type="submit" class="btn btn-primary">確認</button>
                        <button id="btn_upload_ad_profit_loading" type="button" class="btn btn-primary"
                            style="display: none;" disabled>
                            <div class="spinner-border spinner-border-sm">
                                <span class="sr-only">Loading...</span>
                            </div>
                            匯入中...
                        </button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">取消</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{-- Modal 匯入商品資訊 --}}
    <div id="modal_import_item" class="modal">
        <div class="modal-dialog modal-xl modal-dialog-centered">
            <div class="modal-content">
                <form action="{{ route('ad.import_item') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title">匯入商品資訊</h5>
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-row">
                            <div class="form-group col-10">
                                <label>蝦皮商品資訊</label>
                                <div class="custom-file">
                                    <input class="custom-file-input" id="input_item_file" name="file[]" type="file" -
                                        accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,.csv" -
                                        multiple required>
                                    <label class="custom-file-label" for="input_file">請選擇蝦皮商品資訊</label>
                                </div>
                                <h3 class="text-danger font-blod">※請勿更改檔名</h3>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button id="btn_upload_ad_item" type="submit" class="btn btn-primary">確認</button>
                        <button id="btn_upload_ad_item_loading" type="button" class="btn btn-primary" -
                            style="display: none;" disabled>
                            <div class="spinner-border spinner-border-sm">
                                <span class="sr-only">Loading...</span>
                            </div>
                            匯入中...
                        </button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">取消</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="form-row mb-2">
            <div class="col-auto">
                <form action="{{ route('ad.config') }}" method="GET">
                    <div class="form-row">
                        <div class="form-group col-auto">
                            <label for="input_start_date">起日</label>
                            <input id="input_start_date" name="start_date" class="form-control" type="date"
                                value="{{ old('start_date') }}">
                        </div>
                        <div class="form-group col-auto">
                            <label for="input_end_date">訖日</label>
                            <input id="input_end_date" name="end_date" class="form-control" type="date"
                                value="{{ old('end_date') }}">
                        </div>
                    </div>
                    <button class="btn btn-primary" type="submit">查詢</button>
                </form>
            </div>
            <div class="col"></div>
            <div class="col-auto">
                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal_upload">
                    <i class="bi bi-file-earmark-excel"></i>匯入廣告報表
                </button>
            </div>
            <div class="col-auto">
                <button type="button" class="btn btn-info" data-toggle="modal" data-target="#modal_import_item">
                    <i class="bi bi-file-earmark-excel"></i>匯入蝦皮2店商品資訊
                </button>
            </div>
        </div>
        <div class="form-row">
            @if (empty($data) || count($data) == 0)
                <div class="alert alert-danger">
                    無任何資料
                </div>
            @else
                <div class="col-6">
                    <div class="FrozenTable" style="max-height: 77vh;">
                        <table class="table table-bordered table-hover sortable table-filter">
                            <caption>廣告報表</caption>
                            <thead>
                                <tr>
                                    <th class="filter-col">日期</th>
                                    <th class="filter-col">平台</th>
                                    <th class="filter-col">匯入</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($data as $day => $d)
                                    @foreach ($d as $shop_name => $item)
                                        <tr class="{{ empty($item) ? 'table-danger' : '' }}">
                                            <td>{{ $day }}</td>
                                            <td>{{ $shop_name }}</td>
                                            <td>{{ empty($item) ? '' : '✔' }}</td>
                                        </tr>
                                    @endforeach
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-6">
                    <div class="FrozenTable" style="max-height: 77vh;">
                        <table class="table table-bordered table-hover sortable table-filter">
                            <caption>商品資料</caption>
                            <thead>
                                <tr>
                                    <th class="filter-col" style="width: 11%;">賣場</th>
                                    <th class="filter-col" style="width: 12%;">商品ID</th>
                                    <th style="width: 12%;">TMS料號</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($items as $item)
                                    <tr>
                                        <td>{{ $item->shop_name }}</td>
                                        <td>{{ $item->item_id }}</td>
                                        <td>{{ $item->ICODE }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            @endif
        </div>
    </div>
@stop
@section('script')
    <script type="text/javascript">
        $(document).ready(InitialDate());

        function InitialDate() {
            var today = new Date();
            var monthAgo = today.addDays(-30);

            if ($("#input_start_date").val() == '') {
                $("#input_start_date").val(DateToString(monthAgo));
            }
            if ($("#input_end_date").val() == '') {
                $("#input_end_date").val(DateToString(today));
            }
        }
        //上傳檔案後顯示文件名稱
        $('#input_file,#input_item_file').change(function(e) {
            var fileName = '';
            $(e.target.files).each(function(index, element) {
                if (fileName != '') {
                    fileName += ',';
                }
                fileName += element.name;
            });
            $(this).next('.custom-file-label').html(fileName);
        });
        //匯入廣告報表
        $("#btn_upload_ad_profit").click(function() {
            var max = 20;
            var $fileUpload = $("#input_file");
            if (parseInt($fileUpload.get(0).files.length) > max) {
                alert("最多只能上傳" + max + "個檔案");
                return false;
            } else {
                $("#btn_upload_ad_profit").hide();
                $("#btn_upload_ad_profit_loading").show();
                lockScreen();
            }
        });
        //匯入商品資訊
        $("#btn_upload_ad_item").click(function() {
            $("#btn_upload_ad_item").hide();
            $("#btn_upload_ad_item_loading").show();
            lockScreen();
        });
    </script>
@stop
