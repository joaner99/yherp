@extends('layouts.base')
@section('css')
    <style type="text/css">
        table.inner-table>thead>tr>th:nth-child(1),
        table.inner-table>tbody>tr>td:nth-child(1) {
            width: 150px;
        }
    </style>
@endsection
@section('title')
    商品可銷售量表
@stop
@section('content')
    <div class="container-fluid">
        <form action="{{ route('stock_can_sale') }}" method="GET">
            <div class="form-row">
                <div class="form-group col-3">
                    <label>搜尋條件</label>
                    <div class="form-check-list-horizontal">
                        <div class="form-check">
                            <input class="form-check-input" id="input_type_1" name="type" type="radio" value="1"
                                {{ old('type') == '1' || empty(old('type')) ? 'checked' : '' }}>
                            <label class="form-check-label" for="input_type_1">指定大中小類</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" id="input_type_2" name="type" type="radio"
                                value="2"{{ old('type') == '2' ? 'checked' : '' }}>
                            <label class="form-check-label" for="input_type_2">指定料號</label>
                        </div>
                    </div>
                </div>
                <div class="form-group col-2 kind" style="display: none;">
                    <label for="input_kind">大類</label>
                    <input type="text" name="kind" id="input_kind" list="kind_list" class="form-control"
                        autocomplete="off" value="{{ old('kind') }}">
                </div>
                <div class="form-group col-2 kind" style="display: none;">
                    <label for="input_kind2">中類</label>
                    <input type="text" name="kind2" id="input_kind2" list="kind2_list" class="form-control"
                        autocomplete="off" value="{{ old('kind2') }}">
                </div>
                <div class="form-group col-2 kind" style="display: none;">
                    <label for="input_kind3">小類</label>
                    <input type="text" name="kind3" id="input_kind3" list="kind3_list" class="form-control"
                        autocomplete="off" value="{{ old('kind3') }}">
                </div>
                <div class="form-group col-3 item" style="display: none;">
                    <label for="input_td">料號</label>
                    <input type="text" name="id" id="input_td" list="item_list" class="form-control"
                        autocomplete="off" value="{{ old('id') }}">
                </div>
            </div>
            <button class="btn btn-primary btn-sm" type="submit">查詢</button>
            <datalist id="kind_list">
                @if (!empty($types))
                    @foreach ($types[1] as $value => $text)
                        <option value="{{ $value }}">{{ $text }}</option>
                    @endforeach
                @endif
            </datalist>
            <datalist id="kind2_list">
                @if (!empty($types))
                    @foreach ($types[2] as $value => $text)
                        <option value="{{ $value }}">{{ $text }}</option>
                    @endforeach
                @endif
            </datalist>
            <datalist id="kind3_list">
                @if (!empty($types))
                    @foreach ($types[3] as $value => $text)
                        <option value="{{ $value }}">{{ $text }}</option>
                    @endforeach
                @endif
            </datalist>
            <datalist id="item_list">
                @if (!empty($itemList))
                    @foreach ($itemList as $item)
                        <option value="{{ $item->ICODE }}">{{ $item->INAME }}</option>
                    @endforeach
                @endif
            </datalist>
        </form>
        <div class="form-row mt-2">
            @if (empty($data))
                <div class="col">
                    <div class="alert alert-danger">
                        {{ $msg }}
                    </div>
                </div>
            @else
                <div class="col">
                    <div class="FrozenTable" style="max-height: 75vh; font-size:0.85rem;">
                        <table class="table table-bordered table-hover sortable table-filter">
                            <colgroup>
                                <col span="6">
                                <col class="table-warning" span="1">
                                <col class="table-info" span="2">
                                <col class="table-success" span="1">
                            </colgroup>
                            <thead>
                                <tr>
                                    <th style="width: 4%;">序</th>
                                    <th style="width: 7%;" class="filter-col">大類</th>
                                    <th style="width: 7%;" class="filter-col">中類</th>
                                    <th style="width: 7%;" class="filter-col">小類</th>
                                    <th style="width: 9%;">產品料號</th>
                                    <th>產品名稱</th>
                                    <th style="width: 6%;">TMS<br>主倉+出貨<br>+非瑕疵庫存</th>
                                    <th style="width: 6%;">TMS訂單<br>需求數</th>
                                    <th style="width: 6%;">籃子<br>需求數</th>
                                    <th style="width: 6%;">庫存-需求<br>可銷售數</th>
                                    <th style="width: 230px;" class="p-0">
                                        <table class="inner-table">
                                            <thead>
                                                <tr>
                                                    <th colspan="2">未檢或未驗的銷貨單</th>
                                                </tr>
                                                <tr>
                                                    <th>銷貨單號</th>
                                                    <th>數量</th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $index = 1; ?>
                                @foreach ($data as $item)
                                    <tr>
                                        <td>{{ $index++ }}</td>
                                        <td>{{ $item->ITNAM }}</td>
                                        <td>{{ $item->ITNA2 }}</td>
                                        <td>{{ $item->ITNA3 }}</td>
                                        <td>{{ $item->ICODE }}</td>
                                        <td class="text-left">{{ $item->INAME }}</td>
                                        <td class="{{ $item->Stock <= 0 ? 'text-danger' : '' }}">
                                            {{ number_format($item->Stock) }}</td>
                                        <td>{{ number_format($item->OrderNeed) }}</td>
                                        <td>{{ number_format($item->BasketNeed) }}</td>
                                        <td class="{{ $item->CanSale <= 0 ? 'text-danger' : '' }}">
                                            {{ number_format($item->CanSale) }}</td>
                                        <td class="p-0">
                                            @if (count($item->UncheckedList) > 0)
                                                <table class="inner-table">
                                                    <tbody>
                                                        @foreach ($item->UncheckedList as $uncheckItem)
                                                            <tr>
                                                                <td>
                                                                    <a
                                                                        href="{{ route('check_log_detail', $uncheckItem->HCOD1) }}">
                                                                        {{ $uncheckItem->HCOD1 }}
                                                                    </a>
                                                                </td>
                                                                <td>{{ number_format($uncheckItem->HINUM) }}</td>
                                                            </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            @endif
        </div>
    </div>
@stop
@section('script')
    <script type="text/javascript">
        HideSidebar();
        $(document).ready(function() {
            $('[name="type"]').trigger("change");
        });
        //通用類與指定大中小類切換
        $('[name="type"]').on('change', function() {
            $('.kind,.item').hide();
            if ($('#input_type_1').prop('checked')) { //大中小類
                $('.kind').show();
            } else if ($('#input_type_2').prop('checked')) { //料號
                $('.item').show();
            }
        });
    </script>
@stop
