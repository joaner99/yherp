@extends('layouts.base')
@section('title')
    銷貨明細
@stop
@section('content')
    <div class="container-fluid">
        @if (!empty($data))
            <div class="card">
                <div class="card-header">
                    銷貨明細
                </div>
                <div class="card-body">
                    <ul class="list-group list-group-horizontal-xl">
                        <li class="list-group-item list-group-item-info">銷貨單號：<br>{{ $data->sale_no }}</li>
                        <li class="list-group-item list-group-item-info">訂單單號：<br>{{ $data->order_no }}</li>
                        <li class="list-group-item list-group-item-info">原始訂單號：<br>{{ $data->original_no }}</li>
                        <li class="list-group-item list-group-item-info">客戶：<br>【{{ $data->cust }}】{{ $data->cust_name }}
                        </li>
                        <li class="list-group-item list-group-item-info">發票號碼：<br>{{ $data->tax }}</li>
                        <li class="list-group-item list-group-item-info">統一編號：<br>{{ $data->receive_no ?? '' }}</li>
                        <li class="list-group-item list-group-item-info">
                            總金額(含稅)：<br>{{ number_format($data->sales_total_tax) }}
                        </li>
                        <li class="list-group-item list-group-item-info">
                            物流：<br>【{{ $data->trans }}】{{ $data->trans_name }}</li>
                        <li class="list-group-item list-group-item-info">託運單號：<br>{{ $data->trans_no }}</li>
                    </ul>
                    <div class="form-row mt-2">
                        <div class="col-4">
                            備註1
                            <textarea class="form-control" style="height: 150px;" readonly>{{ $data->bak1 }}</textarea>
                        </div>
                        <div class="col-4">
                            備註2
                            <textarea class="form-control" style="height: 150px;" readonly>{{ $data->bak2 }}</textarea>
                        </div>
                        <div class="col-4">
                            備註3
                            <textarea class="form-control" style="height: 150px;" readonly>{{ $data->bak3 }}</textarea>
                        </div>
                    </div>
                    <div class="form-row mt-2">
                        <div class="col-6">
                            買家備註
                            <textarea class="form-control" style="height: 100px;" readonly>{{ $data->buyer_note }}</textarea>
                        </div>
                        <div class="col-6">
                            內部備註
                            <textarea class="form-control" style="height: 100px;" readonly>{{ $data->InsideNote }}</textarea>
                        </div>
                    </div>
                    <div class="FrozenTable mt-3">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>料號</th>
                                    <th>品名</th>
                                    <th>單價</th>
                                    <th>數量</th>
                                    <th>總價</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($data->Histin as $detail)
                                    <tr>
                                        <td>{{ $detail->item_no }}</td>
                                        <td>{{ $detail->item_name }}</td>
                                        <td>{{ number_format($detail->unit) }}</td>
                                        <td>{{ number_format($detail->qty) }}</td>
                                        <td>{{ number_format($detail->sales_total) }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        @else
            找不到銷貨資料
        @endif
    </div>
@stop
