@extends('layouts.base')
@section('title')
    參數設定-新增
@stop
@section('css')
    <style type="text/css">
    </style>
@endsection
@section('content')
    <div class="container-fluid">
        @if ($errors->any())
            <div class="form-row mb-2">
                <div class="col-auto">
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        @endif
        <div class="form-row">
            <div class="col-auto">
                <a class="btn btn-secondary" href="{{ route('sys_config.index') }}">回上頁</a>
            </div>
        </div>
        <form method="POST" action="{{ route('sys_config.store') }}">
            @csrf
            <div class="form-row mt-2">
                <div class="col-lg-3">
                    <div class="form-group">
                        <label class="text-danger" for="input__key">*代碼</label>
                        <input class="form-control" id="input__key" name="_key" type="text" required>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="input_remark">備註</label>
                        <input class="form-control" id="input_remark" name="remark" type="text">
                    </div>
                </div>
            </div>
            <div class="form-row mt-2">
                <div class="col-lg-3">
                    <div class="form-group">
                        <label class="text-danger" for="input_value1_description">*欄位1的描述</label>
                        <input class="form-control" id="input_value1_description" name="value1_description" type="text"
                            required>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label for="input_value2_description">欄位2的描述</label>
                        <input class="form-control" id="input_value2_description" name="value2_description" type="text">
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label for="input_value3_description">欄位3的描述</label>
                        <input class="form-control" id="input_value3_description" name="value3_description" type="text">
                    </div>
                </div>
            </div>
            <hr>
            <button class="btn btn-primary" type="submit">確認</button>
        </form>
    </div>
@stop
@section('script')
    <script type="text/javascript">
        //去空白
        $(function() {
            $('input[type="text"]').change(function() {
                this.value = $.trim(this.value);
            });
        });
    </script>
@stop
