@extends('layouts.base')
@section('title')
    參數設定-編輯
@stop
@section('css')
    <style type="text/css">
    </style>
@endsection
@section('content')
    <div class="container-fluid">
        @if ($errors->any())
            <div class="form-row mb-2">
                <div class="col-auto">
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        @endif
        <div class="form-row">
            <div class="col-auto">
                <a class="btn btn-secondary btn-sm" href="{{ url()->previous() }}">回上頁</a>
            </div>
        </div>
        <form method="POST" action="{{ route('sys_config.update', $data->id) }}">
            @method('PATCH')
            @csrf
            <div class="card mt-2">
                <h1 class="card-header text-info font-weight-bold">主檔</h1>
                <div class="card-body p-3">
                    <div class="form-row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label for="input__key">代碼</label>
                                <input class="form-control-plaintext" id="input__key" type="text"
                                    value="{{ $data->_key }}" readonly>
                            </div>
                        </div>
                        <div class="col-lg-8">
                            <div class="form-group">
                                <label for="input_remark">備註</label>
                                <input class="form-control" id="input_remark" name="remark" type="text"
                                    value="{{ $data->remark }}">
                            </div>
                        </div>
                    </div>
                    <div class="form-row mt-2">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label class="text-danger" for="input_value1_description">*欄位1的描述</label>
                                <input class="form-control" id="input_value1_description" name="value1_description"
                                    type="text" value="{{ $data->value1_description }}" required>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label for="input_value2_description">欄位2的描述</label>
                                <input class="form-control" id="input_value2_description" name="value2_description"
                                    type="text" value="{{ $data->value2_description }}">
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label for="input_value3_description">欄位3的描述</label>
                                <input class="form-control" id="input_value3_description" name="value3_description"
                                    type="text" value="{{ $data->value3_description }}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card mt-2">
                <h1 class="card-header text-info font-weight-bold">明細檔</h1>
                <div class="card-body p-3">
                    <button class="btn btn-info btn-sm" type="button" id="btn_create">
                        <i class="bi bi-plus-lg"></i>新增參數
                    </button>
                    <div class="FrozenTable mt-2" style="max-height: 35vh;">
                        <table class="table table-bordered sortable">
                            <thead>
                                <tr>
                                    <th style="width: 30%;">
                                        欄位1{{ empty($data->value1_description) ? '' : "【{$data->value1_description}】" }}
                                    </th>
                                    <th style="width: 30%;">
                                        欄位2{{ empty($data->value2_description) ? '' : "【{$data->value2_description}】" }}
                                    </th>
                                    <th style="width: 30%;">
                                        欄位3{{ empty($data->value3_description) ? '' : "【{$data->value3_description}】" }}
                                    </th>
                                    <th style="width: 10%;">操作</th>
                                </tr>
                            </thead>
                            <tbody id="tbody_items">
                                @if (!empty($data->Details))
                                    @foreach ($data->Details as $detail)
                                        <tr data-id="{{ $detail->id }}">
                                            <td>
                                                <input class="form-control value1" type="text"
                                                    value="{{ $detail->value1 }}" required>
                                            </td>
                                            <td>
                                                <input class="form-control value2" type="text"
                                                    value="{{ $detail->value2 }}">
                                            </td>
                                            <td>
                                                <input class="form-control value3" type="text"
                                                    value="{{ $detail->value3 }}">
                                            </td>
                                            <td>
                                                <button class="btn btn-danger btn-sm" type="button"
                                                    onclick="ItemDelete(this)">
                                                    <i class="bi bi-trash"></i>刪除
                                                </button>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <input id="input_details" name="details" type="hidden">
            <button class="btn btn-primary btn-sm mt-2" id="btn_confirm" type="submit">確認</button>
        </form>
    </div>
@stop
@section('script')
    <script type="text/javascript">
        //去空白
        $(function() {
            $('input[type="text"]').change(function() {
                this.value = $.trim(this.value);
            });
        });
        //確認
        $("#btn_confirm").click(function() {
            var configList = new Array();
            $("#tbody_items>tr").each(function(index, element) {
                var id = $(element).data().id;
                var value1 = $(element).find(".value1").val();
                var value2 = $(element).find(".value2").val();
                var value3 = $(element).find(".value3").val();
                var config = {
                    id: id,
                    value1: value1,
                    value2: value2,
                    value3: value3,
                };
                configList.push(config);
            });
            $("#input_details").val(JSON.stringify(configList));
        });
        //建立參數
        $("#btn_create").click(function() {
            var dom = '';
            dom += '<tr data-id="">';
            dom +=
                '<td><input class="form-control value1" type="text" required></td>';
            dom +=
                '<td><input class="form-control value2" type="text"></td>';
            dom +=
                '<td><input class="form-control value3" type="text"></td>';
            dom +=
                '<td><button class="btn btn-danger btn-sm" type="button"onclick="ItemDelete(this)"><i class="bi bi-trash"></i>刪除</button></td>';
            dom += '</tr>';

            $("#tbody_items").append(dom);
            $("#tbody_items>tr:last-child>td:eq(0)>input").focus();
        });
        //正貨商品明細刪除按鈕
        function ItemDelete(obj) {
            $(obj).closest('tr').remove();
        }
    </script>
@stop
