@extends('layouts.base')
@section('title')
    參數設定
@stop
@section('css')
    <style type="text/css">
        .table-detail>thead>tr>th:first-child {
            width: 80px;
        }

        #tbody_main>tr>td {
            vertical-align: top;
        }

        #tbody_main .FrozenTable {
            font-size: 0.8rem;
        }
    </style>
@endsection
@section('content')
    <div class="container-fluid">
        <div class="form-row">
            <div class="col-auto">
                <a class="btn btn-info" href="{{ route('sys_config.create') }}">
                    <i class="bi bi-plus-lg"></i>新增參數
                </a>
            </div>
        </div>
        <div class="form-row mt-2">
            <div class="col">
                <div class="FrozenTable" style="max-height: 80vh;">
                    <table class="table table-bordered table-hover sortable table-filter">
                        <thead>
                            <tr>
                                <th style="width: 6%;">流水號</th>
                                <th style="width: 15%;" class="filter-col">代碼</th>
                                <th style="width: 15%;">備註</th>
                                <th>細項</th>
                                <th style="width: 7%;">操作</th>
                            </tr>
                        </thead>
                        <tbody id="tbody_main">
                            @if (!empty($data))
                                @foreach ($data as $item)
                                    <tr>
                                        <td>{{ $item->id }}</td>
                                        <td>{{ $item->_key }}</td>
                                        <td>{{ $item->remark }}</td>
                                        <td class="p-0">
                                            <div class="FrozenTable">
                                                <table class="table table-bordered table-hover sortable table-detail">
                                                    <thead>
                                                        <tr>
                                                            <th>流水號</th>
                                                            <th>{{ $item->value1_description }}</th>
                                                            @if (!empty($item->value2_description))
                                                                <th>{{ $item->value2_description }}</th>
                                                            @endif
                                                            @if (!empty($item->value3_description))
                                                                <th>{{ $item->value3_description }}</th>
                                                            @endif
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @if (!empty($item->Details))
                                                            @foreach ($item->Details as $detail)
                                                                <tr>
                                                                    <td>{{ $detail->id }}</td>
                                                                    <td>{{ $detail->value1 }}</td>
                                                                    @if (!empty($item->value2_description))
                                                                        <td>{{ $detail->value2 }}</td>
                                                                    @endif
                                                                    @if (!empty($item->value3_description))
                                                                        <td>{{ $detail->value3 }}</td>
                                                                    @endif
                                                                </tr>
                                                            @endforeach
                                                        @endif
                                                    </tbody>
                                                </table>
                                            </div>
                                        </td>
                                        <td>
                                            <a class="btn btn-warning btn-sm my-1"
                                                href="{{ route('sys_config.edit', $item->id) }}">
                                                <i class="bi bi-pencil-fill"></i>編輯
                                            </a>
                                            <form method="POST" action="{{ route('sys_config.destroy', $item->id) }}">
                                                @csrf
                                                <input name="_method" type="hidden" value="DELETE">
                                                <button class="btn btn-danger btn-sm my-1">
                                                    <i class="bi bi-trash"></i>刪除
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="form-row mt-2">
            <div class="col">
                {!! $data->render() !!}
            </div>
        </div>
    </div>
@stop
