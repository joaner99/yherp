@extends('layouts.base')
@section('title')
    退貨檢驗作業
@stop
@section('css')
    <style type="text/css">
        .order_font {
            font-family: monospace;
        }

        .is_flaw {
            font-size: 2rem;
        }

        .inner-table>thead>tr>th:nth-child(1),
        .inner-table>tbody>tr>td:nth-child(1) {
            width: 110px;
        }

        .inner-table>thead>tr>th:nth-child(n+3),
        .inner-table>tbody>tr>td:nth-child(n+3) {
            width: 60px;
        }

        .scrap {
            font-size: 1.25rem;
            border-style: solid;
            border-width: 2px;
            border-radius: 0.25rem;
            padding: 0.1rem 0.2rem;
            margin: 0;
            color: red;
            font-weight: bolder;
            cursor: pointer;
        }
    </style>
@endsection
@section('content')
    {{-- Modal 報廢明細 --}}
    <div id="modal_scrap" class="modal">
        <div class="modal-dialog modal-dialog-centered modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">報廢明細</h5>
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-row">
                        <div class="col">
                            <div class="FrozenTable" style="max-height: 87vh;">
                                <table class="table table-bordered table-hover sortable table-filter">
                                    <thead>
                                        <tr>
                                            <th>料號</th>
                                            <th>品名</th>
                                            <th>數量</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if (!empty($scrapped_items) && count($scrapped_items) > 0)
                                            @foreach ($scrapped_items as $id => $orders)
                                                @foreach ($orders as $item)
                                                    <tr data-id="{{ $id }}">
                                                        <td>{{ $item['item_no'] }}</td>
                                                        <td>{{ $item['item_name'] }}</td>
                                                        <td>{{ $item['qty'] }}</td>
                                                    </tr>
                                                @endforeach
                                            @endforeach
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">關閉</button>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="form-row align-items-center">
            <div class="col-auto">
                <a class="btn btn-info" href="{{ route('receipt_return.create') }}">
                    <i class="bi bi-plus-lg"></i>新增退貨檢驗單
                </a>
            </div>
            <div class="col-auto">
                <div class="form-check">
                    <form action="{{ route('receipt_return.index') }}" method="GET">
                        <input class="form-check-input" type="checkbox" id="chk_include_done" name="include_done"
                            value='1' {{ request()->has('include_done') ? 'checked' : '' }}>
                        <label class="form-check-label" for="chk_include_done">
                            顯示結案訂單
                        </label>
                        <button id="submit" class="d-none" type="submit"></button>
                    </form>
                </div>
            </div>
            <div class="col"></div>
            <div class="col-auto">
                <a class="btn btn-primary" href="{{ asset('storage') . '/help/return.pdf' }}">
                    <i class="bi bi-info-circle"></i>說明文件
                </a>
            </div>
            <div class="col-auto">
                <form method="POST" action="{{ route('receipt_return.update') }}">
                    @csrf
                    <input name="ids" type="hidden">
                    <button id="btn_update" class="btn btn-success">
                        <i class="bi bi-check2-square"></i>批次結案
                    </button>
                </form>
            </div>
            <div class="col-auto">
                <form method="POST" action="{{ route('receipt_return.destroy') }}"onsubmit="return confirm('確定要刪除?');">
                    @csrf
                    <input name="_method" type="hidden" value="DELETE">
                    <input name="ids" type="hidden">
                    <button id="btn_delete" class="btn btn-danger">
                        <i class="bi bi-trash"></i>批次刪除
                    </button>
                </form>
            </div>
        </div>
        <div class="form-row mt-2">
            <div class="col">
                <div class="FrozenTable" style="max-height: 87vh; font-size: 0.8rem; min-width: 800px;">
                    <table class="table table-bordered table-hover sortable table-filter">
                        <thead>
                            <tr>
                                <th class="sort-none" style="width: 2%;"><input id="chk_all" type="checkbox"></th>
                                <th style="width: 4%;">流水號</th>
                                <th style="width: 7%;">TMS<br>已處理</th>
                                <th class="filter-col" style="width: 6%;">客戶簡稱</th>
                                <th style="width: 10%;">原始訂單編號</th>
                                <th style="width: 7%;">銷貨單號</th>
                                <th class="filter-col" style="width: 5%;">包裹狀態</th>
                                <th class="filter-col" style="width: 5%;">商品瑕疵</th>
                                <th class="p-0">
                                    <table class="inner-table">
                                        <thead>
                                            <tr>
                                                <th>料號</th>
                                                <th>品名</th>
                                                <th>訂單</th>
                                                <th>良品</th>
                                                <th>不良</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </th>
                                <th style="width: 12%;">備註</th>
                                <th style="width: 6%;">建立時間</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $item)
                                <tr class="{{ $item->tms_done ? 'table-success' : '' }}">
                                    <td>
                                        <input class="confirm" type="checkbox" value="{{ $item->id }}">
                                    </td>
                                    <td>{{ $item->id }}</td>
                                    <td>
                                        <span style="font-size: 1.5rem;">{{ $item->tms_done ? '✔' : '' }}</span>
                                        @if (key_exists($item->id, $scrapped_items))
                                            <span class="scrap" data-id="{{ $item->id }}">廢</span>
                                        @endif
                                        {{ $item->return_no }}
                                    </td>
                                    <td>{{ $item->Posein->cust_name ?? '' }}</td>
                                    <td class="order_font">{{ $item->Posein->PJONO ?? '' }}</td>
                                    <td>
                                        <a href="{{ route('check_log_detail', $item->sale_no) }}">
                                            {{ $item->sale_no }}
                                        </a>
                                    </td>
                                    <td>{{ $item->package_status_name }}</td>
                                    <td>
                                        @if ($item->is_flaw)
                                            <span class="is_flaw">✔</span>
                                        @endif
                                    </td>
                                    <td class="p-0">
                                        @if (!empty($item->Details))
                                            <table class="inner-table">
                                                <tbody>
                                                    @foreach ($item->Details as $d)
                                                        <tr>
                                                            <td>{{ $d->item_no }}</td>
                                                            <td class="text-left">{{ $d->item_name }}</td>
                                                            <td>{{ $d->qty }}</td>
                                                            <td class="table-success">{{ $d->good_qty }}</td>
                                                            <td class="table-danger">{{ $d->bad_qty }}</td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        @endif
                                    </td>
                                    <td class="text-left">{{ $item->remark }}</td>
                                    <td>{{ $item->created_at }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop
@section('script')
    <script type="text/javascript">
        //全選功能
        $('#chk_all').change(check_all);
        $('#btn_delete,#btn_update').click(function() {
            var ids = [];
            $('input.confirm:checked').each(function(i, e) {
                ids.push($(e).val());
            });
            $('input[name="ids"]').val(JSON.stringify(ids));
        })
        //checkbox勾選觸發submit
        $("#chk_include_done").click(function() {
            $("#submit").click();
        });
        //報廢明細
        $('.scrap').click(function() {
            var id = $(this).data().id;
            $('#modal_scrap tbody>tr[data-id="' + id + '"]').show();
            $('#modal_scrap tbody>tr[data-id!="' + id + '"]').hide();
            $('#modal_scrap').modal('show');
        });
    </script>
@stop
