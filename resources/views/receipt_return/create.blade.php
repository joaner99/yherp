@extends('layouts.base')
@section('title')
    退貨檢驗
@stop
@section('css')
    <style type="text/css">
        @media (max-width: 1200px) {

            #tbody_data,
            #tbody_data input {
                font-size: 0.7rem !important;
            }
        }
    </style>
@endsection
@section('content')
    <div class="container-fluid">
        <div class="form-row mb-2">
            <div class="col-auto">
                <a class="btn btn-secondary btn-sm" href="{{ route('receipt_return.index') }}">回上頁</a>
            </div>
        </div>
        <form action="{{ route('receipt_return.create') }}" method="GET">
            <div class="form-row">
                <div class="col-xl-auto">
                    <div class="form-group">
                        <label>銷貨單號/原始訂單編號/物流單號/包裝條碼</label>
                        <input class="form-control" type="text" name="keyword" value="{{ old('keyword') }}">
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-xl-auto">
                    <button class="btn btn-primary btn-sm" type="submit">查詢</button>
                </div>
            </div>
        </form>
        @if (empty($data))
            <div class="form-row mt-2">
                <div class="col">
                    <div class="alert alert-danger">
                        {{ $msg }}
                    </div>
                </div>
            </div>
        @else
            @if ($need_check)
                <div class="form-row mt-2">
                    <div class="col">
                        <div class="alert alert-danger">
                            <h1 class="m-0"><i class="bi bi-exclamation-triangle-fill"></i>需要驗貨<i
                                    class="bi bi-exclamation-triangle-fill"></i></h1>
                        </div>
                    </div>
                </div>
            @endif
            <div class="form-row mt-2">
                <div class="col">
                    <div class="FrozenTable">
                        <table class="table table-bordered table-hover sortable">
                            <caption>主檔</caption>
                            <tbody>
                                <tr>
                                    <th>銷貨單號</th>
                                    <td>
                                        <a href="{{ route('check_log_detail', $data->sale_no) }}">{{ $data->sale_no }}</a>
                                    </td>
                                    <th>訂單單號</th>
                                    <td>
                                        <a href="{{ route('order_detail', $data->order_no) }}">{{ $data->order_no }}</a>
                                    </td>
                                    <th>原始訂單編號</th>
                                    <td>{{ $data->original_no }}</td>
                                    <th>託運單號</th>
                                    <td>{{ $data->trans_no }}</td>
                                </tr>
                                <tr>
                                    <th>客戶簡稱</th>
                                    <td>{{ $data->cust_name }}</td>
                                    <th>總金額(未稅)</th>
                                    <td>{{ number_format($data->sales_total) }}</td>
                                    <th>總金額(含稅)</th>
                                    <td>{{ number_format($data->sales_total_tax) }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="form-row my-2">
                <div class="col">
                    <div class="FrozenTable" style="max-height: 50vh;">
                        <table class="table table-bordered table-hover sortable">
                            <colgroup>
                                <col span="4" />
                                <col span="1" class="table-success" />
                                <col span="1" class="table-danger" />
                            </colgroup>
                            <caption>
                                明細
                            </caption>
                            <thead>
                                <tr>
                                    <th style="width: 10%;">料號</th>
                                    <th>品名</th>
                                    <th style="width: 10%;">單價</th>
                                    <th style="width: 10%;">數量</th>
                                    <th style="width: 20%;">良品數量</th>
                                    <th style="width: 20%;">不良數量</th>
                                    <th style="width: 10%;">總價</th>
                                </tr>
                            </thead>
                            <tbody id="tbody_data">
                                @foreach ($data->Histin as $item)
                                    <tr data-item_no="{{ $item->item_no }}" data-item_name="{{ $item->item_name }}"
                                        data-qty="{{ $item->qty }}">
                                        <td>{{ $item->item_no }}</td>
                                        <td class="text-left">{{ $item->item_name }}</td>
                                        <td>{{ number_format($item->unit) }}</td>
                                        <td>{{ number_format($item->qty) }}</td>
                                        <td>
                                            <input name="good_qty" class="form-control text-center" type="number"
                                                min="0" step="1" value="{{ intval($item->qty) }}" />
                                        </td>
                                        <td>
                                            <input name="bad_qty" class="form-control text-center" type="number"
                                                min="0" step="1" value="0" />
                                        </td>
                                        <td>{{ number_format($item->sales_total) }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <form method="POST" action="{{ route('receipt_return.store') }}">
                @csrf
                <div class="form-row">
                    <div class="col-xl-2">
                        <div class="form-group">
                            <label class="text-danger" for="select_package_status">*包裹狀態</label>
                            <select class="custom-select" id="select_package_status" name="package_status" required>
                                <option value="" selected>請選擇...</option>
                                @foreach ($package_status as $key => $text)
                                    @if ($key == 3)
                                        @continue;
                                    @endif
                                    <option value="{{ $key }}">{{ $text }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-xl">
                        <div class="form-group">
                            <label>備註</label>
                            <input class="form-control" type="text" name="remark" autocomplete="off">
                        </div>
                    </div>
                </div>
                <button id="btn_confirm" class="btn btn-info btn-sm mb-2" type="submit">確認</button>
                <input name="sale_no" type="hidden" value="{{ $data->sale_no }}">
                <input id="input_items" name="items" type="hidden">
            </form>
        @endif

    </div>
@stop
@section('script')
    <script type="text/javascript">
        $('#select_package_status').on('change', function() {
            var untaken = $(this).val() == 1;
            if (untaken) { //未取
                $('input[name="is_flaw"][value="0"]').prop('checked', true);
            }
        });
        $('#btn_confirm').click(function() {
            var items = new Array();
            $('#tbody_data>tr').each(function(i, e) {
                var data = $(e).data();
                var item_no = data.item_no;
                var item_name = data.item_name;
                var qty = data.qty;
                var good_qty = $(e).find('input[name="good_qty"]').val();
                var bad_qty = $(e).find('input[name="bad_qty"]').val();
                var lost_qty = qty - good_qty - bad_qty;
                if (lost_qty <= 0) {
                    lost_qty = 0;
                }
                var tmp = {
                    item_no,
                    item_name,
                    qty,
                    good_qty,
                    bad_qty,
                    lost_qty
                };
                items.push(tmp);
            });
            if (items.length == 0) {
                return false;
            }
            $("#input_items").val(JSON.stringify(items));
        });
    </script>
@stop
