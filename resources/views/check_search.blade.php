@extends('layouts.base')
@section('title')
    出貨進度查詢
@stop
@section('content')
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-8">
                <form action="{{ route('check_search') }}" method="GET">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                                訂單單號/銷貨單號/原始訂單編號/備註1/備註2/備註3/內部備註/託運單號
                            </span>
                        </div>
                        <input type="text" name="id" class="form-control" style="height: auto;"
                            value="{{ old('id') }}">
                        <div class="input-group-append">
                            <button class="btn btn-primary" type="submit">查詢</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="FrozenTable" style="max-height: 80vh;">
                    @if (!empty($data))
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th style="width: 12%;">原始訂單號</th>
                                    <th style="width: 9%;">訂單單號</th>
                                    <th style="width: 8%;">訂單狀態</th>
                                    <th style="width: 9%;">銷貨單號</th>
                                    <th style="width: 9%;">發票號碼</th>
                                    <th style="width: 8%;">驗貨時間</th>
                                    <th style="width: 8%;">包裝時間</th>
                                    <th style="width: 8%;">上車時間</th>
                                    <th>銷貨物流</th>
                                    <th style="width: 15%;">銷貨託運單號</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($data as $value)
                                    <tr>
                                        <td>{{ $value['original_no'] }}</td>
                                        <td>
                                            <a href="{{ route('order_detail', $value['order_no']) }}">
                                                {{ $value['order_no'] }}
                                            </a>
                                        </td>
                                        <td>{{ $value['order_status'] }}</td>
                                        <td>
                                            <a href="{{ route('check_log_detail', $value['sale_no']) }}">
                                                {{ $value['sale_no'] }}
                                            </a>
                                        </td>
                                        <td>{{ $value['tax'] }}</td>
                                        <td>{{ $value['CheckDate'] ?? '' }}</td>
                                        <td>{{ $value['PackageDate'] ?? '' }}</td>
                                        <td>{{ $value['ShipDate'] ?? '' }}</td>
                                        <td>
                                            @if (empty($value['ShopURL']))
                                                {{ $value['TransportName'] ?? '' }}
                                            @else
                                                <a href="{{ $value['ShopURL'] ?? '' }}"
                                                    target="_blank">{{ $value['TransportName'] ?? '' }}</a>
                                            @endif
                                        </td>
                                        <td>{{ $value['TransportID'] ?? '' }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    @elseif(empty(old('id')))
                        <div class="alert alert-info">
                            請輸入關鍵字
                        </div>
                    @else
                        <div class="alert alert-danger">
                            找不到相關資料
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@stop
