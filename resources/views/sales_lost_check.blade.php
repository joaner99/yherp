@extends('layouts.base')
@section('title')
    未完成訂單總表
@stop
@section('css')
    <style type="text/css">
        .alert {
            width: 100%;
            height: 100%;
            position: fixed;
            z-index: 99999;
            top: 0;
            left: 0;
            display: block;
            background-color: black;
            background-position: center;
            /* Center the image */
            background-repeat: no-repeat;
            /* Do not repeat the image */
            background-size: contain;
            /* Resize the background image to cover the entire container */
            background-image: url({{ asset('img/alert.gif') }});
        }

        /*追蹤*/
        .track {
            background-color: #ffeeba;
            border-color: #ffdf7e;
        }

        /*追蹤待結案*/
        .track_done {
            background-color: #c3e6cb;
            border-color: #8fd19e;
        }
    </style>
@endsection
@section('content')
    @if ($alert)
        <div class="alert"></div>
    @endif
    <div id="modal_remark" class="modal">
        <div class="modal-dialog modal-dialog-centered modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title">外掛備註</h1>
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-row">
                        <div class="col-4">
                            <div class="form-row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="input_order_no">訂單單號</label>
                                        <input class="form-control" id="input_order_no" name="order_no" type="text"
                                            value="" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col">
                                    <div class="form-group">
                                        <label>特殊註記</label>
                                        <div class="form-check-list form-check-list-horizontal">
                                            <div class="form-check">
                                                <input class="form-check-input" id="edit_input_track" name="track"
                                                    type="checkbox" value="1">
                                                <label class="form-check-label" for="edit_input_track">
                                                    定期追蹤
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col">
                                    <div class="form-group">
                                        <label>外掛備註</label>
                                        <textarea id="txt_content" class="form-control" style="height: 400px;" name="content"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="FrozenTable" style="max-height: 72vh;">
                                <table class="table table-bordered sortable table-filter">
                                    <caption>備註紀錄</caption>
                                    <thead>
                                        <tr>
                                            <th style="width: 7%;">序</th>
                                            <th style="width: 15%;">填寫人</th>
                                            <th>備註</th>
                                            <th style="width: 17%;">時間</th>
                                        </tr>
                                    </thead>
                                    <tbody id="tbody_remark">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="form-check">
                        <button id="btn_confirm" class="btn btn-primary" type="button">確認</button>
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">取消</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="form-row">
            <div class="col-auto text-danger font-weight-bolder">
                ※排除訂單=取消、待核准 ※排除客代=面交、包材、半成品領料 ※排除物流=退貨
            </div>
            <div class="col-auto font-weight-bolder">※<span class="table-danger">紅底=超過TMS訂單成立時間2天</span>，<span
                    class="track">黃底=追蹤中</span>
            </div>
            <div class="col"></div>
            <div class="col-auto text-right">
                <form action="{{ route('export_table') }}" method="post">
                    @csrf
                    <input name="export_name" type="hidden" value="未完成訂單總表">
                    <input name="export_data" type="hidden">
                    <button class="btn btn-success btn-sm" type="submit" export-target="#table_data">
                        <i class="bi bi-file-earmark-excel"></i>匯出Excel
                    </button>
                </form>
            </div>
        </div>
        <div class="form-row mt-2">
            <div class="col">
                @if (!empty($data))
                    <div class="FrozenTable" style="max-height: 85vh; font-size: 0.8rem;">
                        <table id="table_data" class="table table-bordered sortable table-filter">
                            <caption>TMS訂單日期【{{ $sDate . '～' . $eDate }}】</caption>
                            <thead>
                                <tr>
                                    <th class="export-none" style="width: 6%;">操作</th>
                                    <th style="width: 4%;">序</th>
                                    <th class="filter-col" style="width: 5%;">備註類型</th>
                                    <th class="filter-col" style="width: 8%;">客戶簡稱</th>
                                    <th style="width: 7%;">訂單單號</th>
                                    <th style="width: 11%;">原始訂單單號</th>
                                    <th style="width: 7%;">銷貨單號</th>
                                    <th class="filter-col" style="width: 8%;">銷貨物流</th>
                                    <th class="filter-col export-none" style="width: 6%;">撿貨</th>
                                    <th class="filter-col export-none" style="width: 6%;">驗貨</th>
                                    <th class="filter-col export-none" style="width: 6%;">包裝</th>
                                    <th class="filter-col export-none" style="width: 6%;">上車</th>
                                    <th class="filter-col export-none" style="width: 6%;">蝦皮出貨</th>
                                    <th>外掛備註</th>
                                </tr>
                            </thead>
                            <tbody id="tbody_data">
                                <?php $index = 1; ?>
                                @foreach ($data as $value)
                                    <?php
                                    $class = '';
                                    if (!empty($value['remark_track'])) {
                                        $class = $value['undone'] ? 'track' : 'track_done';
                                    }
                                    if ($value['is_expired']) {
                                        $class .= ' table-danger';
                                    }
                                    ?>
                                    <tr class="{{ $class }}" data-order_no="{{ $value['order_no'] }}">
                                        <td>
                                            <div class="d-flex flex-column">
                                                @if (empty($value['undone']) && !empty($value['remark_track']))
                                                    <form action="{{ route('sales_lost_check.complete') }}" method="POST"
                                                        enctype="multipart/form-data">
                                                        @csrf
                                                        <input name="order_no" type="hidden"
                                                            value="{{ $value['order_no'] }}">
                                                        <button class="btn btn-success btn-sm mb-2" type="submit">
                                                            <i class="bi bi-check2-circle"></i>結案
                                                        </button>
                                                    </form>
                                                @endif
                                                <div>
                                                    <button class="btn btn-warning btn-sm" type="button"
                                                        data-toggle="modal" data-target="#modal_remark">
                                                        <i class="bi bi-pencil-fill"></i>編輯
                                                    </button>
                                                </div>
                                            </div>
                                        </td>
                                        <td>{{ $index++ }}</td>
                                        <td>{{ $value['status'] }}</td>
                                        <td>{{ $value['cust_name'] }}</td>
                                        <td>
                                            <a href="{{ route('order_detail', $value['order_no']) }}">
                                                {{ $value['order_no'] }}
                                            </a>
                                        </td>
                                        <td>{{ $value['OCOD4'] }}</td>
                                        <td>
                                            @if (!empty($value['sale_no']))
                                                <a href="{{ route('check_log_detail', $value['sale_no']) }}">
                                                    {{ $value['sale_no'] }}
                                                </a>
                                            @endif
                                        </td>
                                        <td>{{ $value['transport'] }}</td>
                                        <td>
                                            {{ $value['picking_src'] }}<div>{{ $value['picking_time'] }}</div>
                                        </td>
                                        <td>
                                            {{ empty($value['check_time']) ? '' : '✔' }}
                                            <div>{{ $value['check_time'] }}</div>
                                        </td>
                                        <td>
                                            {{ empty($value['package_time']) ? '' : '✔' }}
                                            <div>{{ $value['package_time'] }}</div>
                                        </td>
                                        <td>
                                            {{ empty($value['ship_time']) ? '' : '✔' }}
                                            <div>{{ $value['ship_time'] }}</div>
                                        </td>
                                        <td>
                                            {{ empty($value['shopee_ship_time']) ? '' : '✔' }}
                                            <div>{{ $value['shopee_ship_time'] }}</div>
                                        </td>
                                        <td class="py-0">
                                            <span
                                                class="user">{{ empty($value['remark_user']) ? '' : '【' . $value['remark_user'] . '】' }}</span><span
                                                class="time">{{ $value['remark_time'] }}</span>
                                            <textarea class="form-control-plaintext border-top p-0 content" readonly>{{ $value['remark'] }}</textarea>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                @else
                    <div class="alert alert-danger" style="text-align: center;">
                        找不到資料
                    </div>
                @endif
            </div>
        </div>
    </div>
@stop
@section('script')
    <script type="text/javascript">
        $(document).ready(function() {
            //15分鐘自動刷新
            setInterval(function() {
                lockScreen();
                location.reload();
            }, 15 * 60 * 1000);
        });
        //textarea show all text
        $(function() {
            $('#tbody_data textarea').each(function() {
                $(this).height($(this).prop('scrollHeight'));
            });
        });
        //帶入編輯資料
        $('button[data-target="#modal_remark"]').click(function() {
            var tr = $(this).closest('tr');
            var order_no = tr.data().order_no;
            $('#modal_remark input[name="order_no"]').val(order_no);
            $('#modal_remark [name="track"]').prop('checked', false);
            $('#modal_remark [name="content"]').val('');
            lockScreen();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{ route('sales_lost_check.ajax_get_remark') }}",
                type: "post",
                data: {
                    order_no: order_no,
                },
                success: function(data) {
                    var order_no = data.order_no;
                    var track = data.track;
                    var content = data.content;
                    $('#modal_remark [name="track"]').prop('checked', track == 1);
                    $('#modal_remark [name="content"]').val(content);
                    var dom = '';
                    var idx = 1;
                    $(data.details).each(function(i, e) {
                        dom += '<tr>';
                        dom += '<td>' + idx + '</td>';
                        dom += '<td>' + e.user + '</td>';
                        dom += '<td>' + e.content + '</td>';
                        dom += '<td>' + e.time + '</td>';
                        dom += '</tr>';
                        idx++;
                    });
                    if (dom == '') {
                        dom = '<tr><td colspan="4">無資料</td></tr>';
                    }
                    $('#tbody_remark').html(dom);
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    var msg = JSON.parse(xhr.responseText).msg;
                    alert(msg);
                },
                complete: function() {
                    unlockScreen();
                    $('#modal_remark').modal('show')
                }
            });
            return false;
        });
        //鎖定畫面
        $('#modal_remark button[type="submit"]').click(function() {
            lockScreen();
        });
        var url = '{{ route('sales_lost_check.ajax_update') }}';
        $('#btn_confirm').click(function() {
            var order_no = $('#input_order_no').val();
            var track = $('#edit_input_track').prop('checked') ? 1 : 0;
            var content = $('#txt_content').val();
            lockScreen();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: url,
                type: "post",
                data: {
                    order_no: order_no,
                    track: track,
                    content: content,
                },
                success: function(data) {
                    var tr = $('tr[data-order_no="' + order_no + '"]');
                    //畫面更新
                    $(tr).find('.time').text(data.time);
                    $(tr).find('.user').text('【' + data.user + '】');
                    $(tr).find('.content').text(data.content);
                    if (data.track == 1) {
                        $(tr).addClass('track');
                    } else {
                        $(tr).removeClass('track');
                    }
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    var msg = JSON.parse(xhr.responseText).msg;
                    alert(msg);
                },
                complete: function() {
                    unlockScreen();
                    $('#modal_remark').modal('hide')
                }
            });
        });
    </script>
@stop
