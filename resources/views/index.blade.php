@extends('layouts.base')
@section('title')
    首頁
@stop
@section('content')
    <div class="container-fluid">
        <div class="form-row">
            @role('YoHouse')
                <div class="col-auto">
                    <div class="card">
                        <div class="card-header">
                            外掛ERP網址
                        </div>
                        <div class="card-body">
                            <img src="{{ asset('img/qr_home.png') }}" class="p-1">
                        </div>
                    </div>
                </div>
            @endrole
        </div>
    </div>
@stop
