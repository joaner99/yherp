@extends('layouts.base')
@section('title')
    訂單明細
@stop
@section('content')
    <div class="container-fluid">
        @if (!empty($data))
            <div class="card">
                <div class="card-header">
                    訂單明細
                </div>
                <div class="card-body">
                    <ul class="list-group list-group-horizontal-xl">
                        <li class="list-group-item list-group-item-info">訂單單號：<br>{{ $data->order_no }}</li>
                        <li class="list-group-item list-group-item-info">原始訂單單號：<br>{{ $data->original_no }}</li>
                        <li class="list-group-item list-group-item-info">客戶：<br>【{{ $data->cust }}】{{ $data->cust_name }}
                        <li class="list-group-item list-group-item-info">訂單狀態：<br>{{ $data->OCANC }}
                        <li class="list-group-item list-group-item-info">統一編號：<br>{{ $data->receive_no ?? '' }}</li>
                        <li class="list-group-item list-group-item-info">
                            物流：<br>【{{ $data->trans }}】{{ $data->trans_name }}
                        </li>
                        <li class="list-group-item list-group-item-info">
                            總金額(含稅)：<br>{{ number_format($data->sales_total_tax) }}
                        </li>
                    </ul>
                    <div class="form-row mt-2">
                        <div class="col-4">
                            備註1
                            <textarea class="form-control" style="height: 150px;" readonly>{{ $data->bak1 }}</textarea>
                        </div>
                        <div class="col-4">
                            備註2
                            <textarea class="form-control" style="height: 150px;" readonly>{{ $data->bak2 }}</textarea>
                        </div>
                        <div class="col-4">
                            備註3
                            <textarea class="form-control" style="height: 150px;" readonly>{{ $data->bak3 }}</textarea>
                        </div>
                    </div>
                    <div class="form-row mt-2">
                        <div class="col-6">
                            買家備註
                            <textarea class="form-control" style="height: 100px;" readonly>{{ $data->buyer_note }}</textarea>
                        </div>
                        <div class="col-6">
                            內部備註
                            <textarea class="form-control" style="height: 100px;" readonly>{{ $data->InsideNote }}</textarea>
                        </div>
                    </div>
                    <div class="FrozenTable mt-3" style="max-height: 360px;">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>料號</th>
                                    <th>品名</th>
                                    <th>單價</th>
                                    <th>數量</th>
                                    <th>已出</th>
                                    <th>未出</th>
                                    <th>出貨倉</th>
                                    <th>總價</th>
                                    <th>是否出貨</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($data->OrderDetail as $detail)
                                    <tr class="{{ $detail->ONNO > 0 ? 'table-danger' : '' }}">
                                        <td>{{ $detail->item_no }}</td>
                                        <td class="text-left">{{ $detail->item_name }}</td>
                                        <td>{{ number_format($detail->unit) }}</td>
                                        <td>{{ number_format($detail->qty) }}</td>
                                        <td>{{ number_format($detail->OYNO) }}</td>
                                        <td>{{ number_format($detail->ONNO) }}</td>
                                        <td>{{ number_format($detail->STOC->first()->SNUMB) }}</td>
                                        <td>{{ number_format($detail->sales_total) }}</td>
                                        <td>{{ $detail->ship }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        @endif
    </div>
@stop
