@extends('layouts.base')
@section('title')
    表單確認作業
@stop
@section('css')
    <style type="text/css">
        input[type="checkbox"] {
            scale: 1.5;
        }
    </style>
@endsection
@section('content')
    <div class="container-fluid">
        <div class="form-row">
            <div class="col-auto">
                <nav>
                    <ul class="pagination m-0">
                        <li class="page-item active" data-target="#div_receipt">
                            <button class="page-link" type="button">
                                <span class="badge badge-light">{{ count($receipt) }}</span>
                                收料單
                            </button>
                        </li>
                        <li class="page-item d-none" data-target="#div_extra_order">
                            <button class="page-link" type="button">
                                <span class="badge badge-light">{{ count($extra_order) }}</span>
                                額外加單
                            </button>
                        </li>
                        <li class="page-item" data-target="#div_unsafety_stock">
                            <button class="page-link" type="button">
                                <span class="badge badge-light">{{ count($unsafety_stock) }}</span>
                                調撥安庫
                            </button>
                        </li>
                        @role('admin')
                            <li class="page-item" data-target="#div_purchase_undone">
                                <button class="page-link" type="button">
                                    上車檢驗
                                </button>
                            </li>
                        @endrole
                    </ul>
                </nav>
            </div>
        </div>
        <div class="form-row mt-2">
            @can('order_confirm_receipt')
                <div id="div_receipt" class="col">
                    <form method="POST" action="{{ route('order_confirm.store') }}" enctype="multipart/form-data"
                        class="mb-2">
                        @csrf
                        <div class="FrozenTable" style="max-height: 82vh;">
                            <table class="table table-bordered">
                                <caption class="bg-info"><i class="bi bi-1-circle"></i>收料單</caption>
                                <thead>
                                    <tr>
                                        <th style="width: 3%;">
                                            <input id="chk_all_receipt" type="checkbox">
                                        </th>
                                        <th style="width: 12%;">收料單號</th>
                                        <th>明細</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if (!empty($receipt))
                                        @foreach ($receipt as $item)
                                            <tr>
                                                <td><input class="confirm" name="order_no[]" type="checkbox"
                                                        value="{{ $item->id }}"></td>
                                                <td>{{ $item->id }}</td>
                                                <td class="p-0">
                                                    <div class="FrozenTable" style="">
                                                        <table class="table table-border sortable">
                                                            <thead>
                                                                <tr>
                                                                    <th style="width: 15%;">供應商</th>
                                                                    <th style="width: 15%;">料號</th>
                                                                    <th>品名</th>
                                                                    <th style="width: 10%;">數量</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                @foreach ($item->Details as $d)
                                                                    <tr>
                                                                        <td>
                                                                            {{ '【' . $d->Item->IPCOD . '】' . $d->Item->IPNAM }}
                                                                        </td>
                                                                        <td>{{ $d->item_no }}</td>
                                                                        <td class="text-left">{{ $d->item_name }}</td>
                                                                        <td>{{ $d->qty }}</td>
                                                                    </tr>
                                                                @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                        <input name="order_type" type="hidden" value="4">
                        <button class="btn btn-info mt-2">確認收料</button>
                    </form>
                </div>
            @endcan
            @can('order_confirm_extra_order')
                <div id="div_extra_order" class="col" style="display: none;">
                    <form method="POST" action="{{ route('order_confirm.store') }}" enctype="multipart/form-data"
                        class="mb-2">
                        @csrf
                        <div class="FrozenTable" style="max-height: 82vh;">
                            <table class="table table-bordered sortable">
                                <caption class="bg-danger"><i class="bi bi-2-circle"></i>額外加單(15:00後)</caption>
                                <thead>
                                    <tr>
                                        <th class="sort-none" style="width: 3%;">
                                            <input id="chk_all_sales" type="checkbox">
                                        </th>
                                        <th>銷貨單號</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if (!empty($extra_order))
                                        @foreach ($extra_order as $item)
                                            <tr>
                                                <td><input class="confirm" name="order_no[]" type="checkbox"
                                                        value="{{ $item->sale_no }}"></td>
                                                <td>
                                                    <a href="{{ route('check_log_detail', $item->sale_no) }}" target="_blank">
                                                        {{ $item->sale_no }}
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                        <input name="order_type" type="hidden" value="8">
                        <button class="btn btn-info mt-2">確認額外加單</button>
                    </form>
                </div>
            @endcan
            @can('order_confirm_safety_stock')
                <div id="div_unsafety_stock" class="col" style="display: none;">
                    <form method="POST" action="{{ route('order_confirm.store') }}" enctype="multipart/form-data"
                        class="mb-2">
                        @csrf
                        <div class="FrozenTable" style="max-height: 82vh;">
                            <table class="table table-bordered sortable">
                                <caption class="bg-warning"><i class="bi bi-3-circle"></i>調撥安庫</caption>
                                <colgroup>
                                    <col span="4">
                                    <col span="1" class="table-info">
                                    <col span="1" class="table-warning">
                                    <col span="1" class="table-danger">
                                </colgroup>
                                <thead>
                                    <tr>
                                        <th class="sort-none" style="width: 3%;">
                                            <input id="chk_all_unsafety" type="checkbox">
                                        </th>
                                        <th style="width: 12%;">料號</th>
                                        <th>品名</th>
                                        <th style="width: 8%;">產地國別</th>
                                        <th style="width: 8%;">訂單需求</th>
                                        <th style="width: 8%;">總庫存</th>
                                        <th style="width: 8%;">安全庫存</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if (!empty($unsafety_stock))
                                        @foreach ($unsafety_stock as $item)
                                            <tr>
                                                <td><input class="confirm" name="order_no[]" type="checkbox"
                                                        value="{{ $item['item_no'] }}"></td>
                                                <td>{{ $item['item_no'] }}</td>
                                                <td class="text-left">{{ $item['item_name'] }}</td>
                                                <td>{{ $item['country'] == 1 ? 'china' : '' }}</td>
                                                <td>{{ number_format($item['order_require']) }}</td>
                                                <td>{{ number_format($item['stock']) }}</td>
                                                <td>{{ number_format($item['safety']) }}</td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                        <input name="order_type" type="hidden" value="9">
                        <button class="btn btn-info mt-2">確認安庫警告</button>
                    </form>
                </div>
            @endcan
            @role('admin')
                <div id="div_purchase_undone" class="col" style="display: none;">
                    @if ($purchase_undone)
                        今日已覆核
                    @else
                        <form method="POST" action="{{ route('order_confirm.store') }}" enctype="multipart/form-data"
                            class="mb-2">
                            @csrf
                            <input name="order_type" type="hidden" value="12">
                            <button class="btn btn-info mt-2">覆核上車檢驗</button>
                        </form>
                    @endif
                </div>
            @endrole
        </div>
    </div>
@stop
@section('script')
    <script type="text/javascript">
        //全選功能
        $("#chk_all_receipt,#chk_all_trans,#chk_all_trans_done,#chk_all_sales,#chk_all_unsafety").change(check_all);
    </script>
@stop
