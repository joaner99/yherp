@extends('layouts.base')
@section('title')
    現場狀況
@stop
@section('css')
    <style type="text/css">
        .form-row>div {
            padding: 0 5px 5px 5px;
        }

        .card {
            white-space: nowrap;
            height: 100%;
        }

        .card-header {
            padding: 0.5rem 1rem;
        }

        .card-text {
            font-size: 10vmin;
            font-weight: bold;
            color: var(--red)
        }

        .card-body,
        .card-footer {
            padding: 0.25rem !important;
        }

        #div_package_speed .badge {
            font-size: 1.3rem;
            min-width: 65px;
        }

        .c3-chart-arcs-gauge-min,
        .c3-chart-arcs-gauge-max {
            font-size: 1rem;
        }

        #table_package_shift>caption,
        #table_package_shift>thead>tr>th,
        #table_package_shift>tbody>tr>td {
            padding: 0.25rem;
        }

        [data-toggle="modal"] {
            cursor: pointer;
        }
    </style>
@endsection
@section('content')
    <div class="container-fluid">
        <div class="form-row text-center">
            <div class="col-xl-12 col-md-12">
                <div class="card">
                    <h1 class="card-header">
                        現場狀況
                    </h1>
                    <div class="card-body">
                        <div class="FrozenTable h-100" style="font-size: 1.2rem; font-weight: bolder;">
                            <table class="table table-bordered table-hover">
                                <colgroup>
                                    <col span="4">
                                    <col span="5" style="background-color: aliceblue;">
                                    <col span="1" class="table-success">
                                </colgroup>
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th style="width: 11%;">未入TMS訂單(單)</th>
                                        <th style="width: 10%;">未轉單(單)</th>
                                        <th style="width: 10%;">已轉單(件)</th>
                                        @foreach ($order_status as $item)
                                            <th style="width: 10%;">{{ $item }}(件)</th>
                                        @endforeach
                                    </tr>
                                </thead>
                                <tbody>
                                    @for ($i = 1; $i <= 4; $i++)
                                        <tr>
                                            @switch($i)
                                                @case(1)
                                                    <th>地板</th>
                                                @break

                                                @case(2)
                                                    <th>超商</th>
                                                @break

                                                @case(3)
                                                    <th>宅配</th>
                                                @break

                                                @case(4)
                                                    <th>其他</th>
                                                @break
                                            @endswitch
                                            <td>{{ count($order_schedule['original_order']->where('type', $i)) }}</td>
                                            <td>{{ $order_schedule['order'][$i] }}</td>
                                            <td>
                                                <a href="{{ route('sales_list') . '?type=' . $i }}">
                                                    {{ $order_schedule['sales']->where('type', $i)->sum('qty') }}
                                                </a>
                                            </td>
                                            @foreach ($order_status as $i2 => $item)
                                                <td>
                                                    <a href="{{ route('sales_list') . '?status=' . $i2 . '&type=' . $i }}">
                                                        {{ $order_schedule['sales']->where('status', $i2)->where('type', $i)->sum('qty') }}
                                                    </a>
                                                </td>
                                            @endforeach
                                        </tr>
                                    @endfor
                                    <tr class="bg-warning">
                                        <th>合計</th>
                                        <td>{{ count($order_schedule['original_order']) }}</td>
                                        <td>{{ $order_schedule['order'][0] }}</td>
                                        <td>
                                            <a href="{{ route('sales_list') }}">
                                                {{ $order_schedule['sales']->sum('qty') }}
                                            </a>
                                        </td>
                                        @foreach ($order_status as $i2 => $item)
                                            <td>
                                                <a href="{{ route('sales_list') . '?status=' . $i2 }}">
                                                    {{ $order_schedule['sales']->where('status', $i2)->sum('qty') }}
                                                </a>
                                            </td>
                                        @endforeach
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-12 col-md-12">
                <div class="form-row text-center">
                    <div class="col-xl-3 col-md-6">
                        <div class="card">
                            <h1 class="card-header">
                                <a href="{{ route('order', ['table' => 'unchecked']) }}">
                                    未覆核
                                </a>
                            </h1>
                            <div class="card-body">
                                <h1 class="card-text">{{ $order_no_check }}</h1>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6">
                        <div class="card">
                            <h1 class="card-header">
                                <a href="{{ route('order', ['table' => 'unprinted']) }}">
                                    未轉單
                                </a>
                            </h1>
                            <div class="card-body">
                                <h1 class="card-text" style="{{ !empty($order_can_print) ? 'font-size: 8vmin' : '' }}">
                                    {{ $order_no_print }}
                                </h1>
                            </div>
                            @if (!empty($order_can_print))
                                <div class="card-footer text-muted">
                                    <span>可轉單:</span>
                                    <span>{{ $order_can_print }}</span>
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6">
                        <div class="card">
                            <h1 class="card-header">
                                <a href="{{ route('inventory.index') }}">
                                    需盤點
                                </a>
                            </h1>
                            <div class="card-body">
                                <h1 class="card-text">{{ $inventory_count }}</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-row text-center">
            @can('check_table_management')
                <div class="col-xl-3 col-md-6">
                    <div class="card">
                        <h1 class="card-header">
                            <a href="{{ route('order', ['table' => 'expired']) }}">
                                過期單(2天)
                            </a>
                        </h1>
                        <div class="card-body">
                            <h1 class="card-text">{{ $order_expired }}</h1>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-md-6">
                    <div class="card">
                        <h1 class="card-header">
                            <a href="{{ route('stock.transfer') }}">可調撥</a>
                        </h1>
                        <div class="card-body">
                            <h1 class="card-text">{{ $transfer_count }}</h1>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-md-6">
                    <div class="card">
                        <h1 class="card-header">
                            <a href="{{ route('stock.transfer') }}">需採購</a>
                        </h1>
                        <div class="card-body">
                            <h1 class="card-text">{{ $purchase_count }}</h1>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-md-6">
                    <div class="card">
                        <h1 class="card-header">
                            <a href="{{ route('stock.transfer') }}">未進貨</a>
                        </h1>
                        <div class="card-body">
                            <h1 class="card-text">{{ $purchase_undone_count }}</h1>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-md-6">
                    <div class="card">
                        <h1 class="card-header">
                            <a href="{{ route('overweight') }}">過大過重</a>
                        </h1>
                        <div class="card-body">
                            <h1 class="card-text">{{ $orderWeight }}</h1>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-md-6">
                    <div class="card">
                        <h1 class="card-header">
                            <a href="{{ route('order_tax_id') }}">需統編</a>
                        </h1>
                        <div class="card-body">
                            <h1 class="card-text">{{ $order_tax_id }}</h1>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-md-6">
                    <div class="card">
                        <h1 class="card-header">
                            <a href="{{ route('hct_transport.remote_index') }}">新竹偏遠</a>
                        </h1>
                        <div class="card-body">
                            <h1 class="card-text">{{ $hct_remote }}</h1>
                        </div>
                    </div>
                </div>
            @endcan
        </div>
        <div class="form-row text-center justify-content-center">
            <div class="col-auto">
                <div class="form-check form-check-inline">
                    <input class="form-check-input" id="cbxSelectAll" type="checkbox" checked>
                    <label class="form-check-label">自動更新(300s)</label>
                </div>
            </div>
        </div>
    </div>
@stop
@section('script')
    <script type="text/javascript">
        $(document).ready(function() {
            //300S自動刷新
            setInterval(function() {
                varisChecked = $('#cbxSelectAll').is(':checked');
                if (varisChecked) {
                    location.reload();
                }
            }, 300000);
        });
    </script>
@stop
