@extends('layouts.base')
@section('title')
    訂單備註查詢
@stop
@section('css')
    <style type="text/css">
        .Highlight {
            color: red !important;
            background-color: lightyellow !important;
            font-weight: bold !important;
            font-size: 1.5em !important;
        }
    </style>
@endsection
@section('content')
    <div class="container-fluid">
        <form action="{{ route('order_remark') }}" method="GET">
            <div class="form-row">
                <div class="form-group col-md-4 col-lg-3 col-xl-2">
                    <label for="input_start_date">起日</label>
                    <input id="input_start_date" name="start_date" class="form-control" type="date"
                        value="{{ old('start_date') }}">
                </div>
                <div class="form-group col-md-4 col-lg-3 col-xl-2">
                    <label for="input_end_date">訖日</label>
                    <input id="input_end_date" name="end_date" class="form-control" type="date"
                        value="{{ old('end_date') }}">
                </div>
            </div>
            <button class="btn btn-primary" type="submit">查詢</button>
        </form>
        <div class="form-row">
            <div class="col"></div>
            @if (!empty($cfg_id))
                <div class="col-auto">
                    <a href="{{ route('sys_config.edit', $cfg_id) . '?redirect=items.place' }}"
                        class="btn btn-warning btn-sm">修改關鍵字</a>
                </div>
            @endif
            <div class="col-auto">
                <form action="{{ route('export_table') }}" method="post">
                    @csrf
                    <input name="export_name" type="hidden" value="備註訂單">
                    <input name="export_data" type="hidden">
                    <button id="btn_export" class="btn btn-success btn-sm" type="submit" export-target="#table_data">
                        <i class="bi bi-file-earmark-excel"></i>匯出Excel
                    </button>
                </form>
            </div>
        </div>
        <div class="form-row mt-2">
            <div class="col">
                <div class="FrozenTable" style="max-height: 75vh; font-size:0.8rem;">
                    @if (!empty($data))
                        <table id="table_data" class="table table-bordered table-hover sortable table-filter">
                            <thead>
                                <tr>
                                    <th style="width: 3%;">序</th>
                                    <th style="width: 7%;">訂單單號</th>
                                    <th style="width: 10%;">原始訂單號</th>
                                    <th class="filter-col" style="width: 6%;">客戶簡稱</th>
                                    <th>備註1</th>
                                    <th>備註2</th>
                                    <th>備註3</th>
                                    <th>內部備註</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($data as $key => $value)
                                    <tr data-type="{{ $value->cust }}">
                                        <td>{{ $key + 1 }}</td>
                                        <td>{{ $value->order_no }}</td>
                                        <td>{{ $value->original_no }}</td>
                                        <td>{{ $value->cust_name }}</td>
                                        <td class="Remark">{{ $value->bak1 }}</td>
                                        <td class="Remark">{{ $value->bak2 }}</td>
                                        <td class="Remark">{{ $value->bak3 }}</td>
                                        <td class="Remark">{{ $value->InsideNote }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    @else
                        <div class="alert alert-danger" style="text-align: center;">
                            找不到資料
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@stop
@section('script')
    <script type="text/javascript">
        var keyword = @json($keyword);
        $(document).ready(function() {
            $("tbody > tr > .Remark").html(function() {
                var sDom = '<font class="Highlight">';
                var eDom = '</font>';
                var result = $(this).text();
                if (IsNullOrEmpty(result)) { //無內容
                    return;
                }
                //標記8碼數字
                var matches = result.match(/[\D][\d]{8}[\D]/g);
                if (!IsNullOrEmpty(matches)) {
                    matches.forEach(function(element) {
                        var idx = result.indexOf(element) + 1; //+1是為了排除非數字字元
                        result = result.splice(idx, 0, sDom);
                        result = result.splice(idx + sDom.length + 8, 0, eDom);
                    });
                }
                //標記統編關鍵字
                keyword.forEach(element => {
                    result = result.replace(element, (sDom + element + eDom));
                });
                return result;
            });
            InitialDate();
        });

        function InitialDate() {
            var today = new Date();

            if ($("#input_start_date").val() == '') {
                $("#input_start_date").val(DateToString(today));
            }
            if ($("#input_end_date").val() == '') {
                $("#input_end_date").val(DateToString(today));
            }
        }
    </script>
@stop
