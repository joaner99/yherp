@extends('layouts.base')
@section('title')
    報價單-編輯
@stop
@section('css')
    <style type="text/css">
    </style>
@endsection
@section('content')
    <div class="container-fluid">
        <div class="form-row mb-2">
            <div class="col-auto">
                <a class="btn btn-secondary" href="{{ route('quotation.index') }}">回上頁</a>
            </div>
        </div>
        <hr>
        <form method="POST" action="{{ route('quotation.update', $data->id) }}" enctype="multipart/form-data">
            @csrf
            <div class="card mt-2">
                <h2 class="card-header text-info font-weight-bold">主檔</h2>
                <div class="card-body p-2">
                    <div class="form-row">
                        <div class="col-xl-2">
                            <div class="form-group">
                                <label class="text-danger" for="input_quote_date">*報價日期</label>
                                <input class="form-control" id="input_quote_date" name="quote_date" type="date"
                                    value="{{ $data->quote_date }}" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-xl-2">
                            <div class="form-group">
                                <label for="input_contact_name">聯絡人</label>
                                <input class="form-control" id="input_contact_name" name="contact_name" type="text"
                                    value="{{ $data->contact_name }}">
                            </div>
                        </div>
                        <div class="col-xl-2">
                            <div class="form-group">
                                <label for="input_contact_phone">聯絡人電話</label>
                                <input class="form-control" id="input_contact_phone" name="contact_phone" type="text"
                                    value="{{ $data->contact_phone }}">
                            </div>
                        </div>
                        <div class="col-xl-3">
                            <div class="form-group">
                                <label for="input_contact_email">聯絡人郵件</label>
                                <input class="form-control" id="input_contact_email" name="contact_email" type="email"
                                    pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" value="{{ $data->contact_email }}">
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-xl-2">
                            <div class="form-group">
                                <label for="input_company_name">公司名稱</label>
                                <input class="form-control" id="input_company_name" name="company_name" type="text"
                                    value="{{ $data->company_name }}">
                            </div>
                        </div>
                        <div class="col-xl-2">
                            <div class="form-group">
                                <label for="input_company_phone">公司電話</label>
                                <input class="form-control" id="input_company_phone" name="company_phone" type="text"
                                    value="{{ $data->company_phone }}">
                            </div>
                        </div>
                        <div class="col-xl-2">
                            <div class="form-group">
                                <label for="input_company_fax">公司傳真</label>
                                <input class="form-control" id="input_company_fax" name="company_fax" type="text"
                                    value="{{ $data->company_fax }}">
                            </div>
                        </div>
                        <div class="col-xl-4">
                            <div class="form-group">
                                <label for="input_company_addr">公司地址</label>
                                <input class="form-control" id="input_company_addr" name="company_addr" type="text"
                                    value="{{ $data->company_addr }}">
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-xl-4">
                            <div class="form-group">
                                <label for="input_remark">備註</label>
                                <textarea class="form-control" id="input_remark" name="remark">{{ $data->remark }}</textarea>
                            </div>
                        </div>
                        <div class="col-xl-4">
                            <div class="form-group">
                                <label for="input_warranty_remark">保固說明</label>
                                <textarea class="form-control" id="input_warranty_remark" name="warranty_remark">{{ $data->warranty_remark }}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card mt-2">
                <div class="card-header text-info">
                    <div class="form-row">
                        <div class="col">
                            <h2 class="font-weight-bold m-0">明細檔</h2>
                        </div>
                        <div class="col-auto">
                            <button class="btn btn-info" type="button" id="btn_create">
                                <i class="bi bi-plus-lg"></i>新增商品
                            </button>
                        </div>
                        <div class="col-auto">
                            <button class="btn btn-info" type="button" id="btn_create_discount">
                                <i class="bi bi-plus-lg"></i>新增折扣
                            </button>
                        </div>
                    </div>
                </div>
                <div class="card-body p-2">
                    <div class="FrozenTable" style="max-height: 36vh;">
                        <table class="table table-bordered">
                            <caption>總計含稅：<span class="total_price"></span></caption>
                            <thead>
                                <tr>
                                    <th style="width: 12%;">*料號</th>
                                    <th>品名</th>
                                    <th style="width: 7%;">*數量</th>
                                    <th style="width: 10%;">*售價(含稅)</th>
                                    <th style="width: 10%;">成本</th>
                                    <th style="width: 10%;">小計</th>
                                    <th style="width: 25%;">備註<small>(100字)</small></th>
                                    <th style="width: 7%;">操作</th>
                                </tr>
                            </thead>
                            <tbody id="tbody_items">
                                @foreach ($data->Details as $item)
                                    <tr>
                                        <td>
                                            <input class="form-control form-control-sm item_no" type="text"
                                                list="itemList"
                                                onchange="SetItemDetail($(this))"value="{{ $item->item_no }}" required>
                                        </td>
                                        <td class="text-left">
                                            <span class="item_name">{{ $item->item_name }}</span>
                                        </td>
                                        <td>
                                            <input class="form-control form-control-sm item_qty" type="number"
                                                min="0" max="65535" step="1"
                                                value="{{ $item->item_qty }}" onchange="CalculateTotal($(this))"
                                                required>
                                        </td>
                                        <td>
                                            <input class="form-control form-control-sm item_price" type="number"
                                                step="0.01" value="{{ $item->item_price }}"
                                                onchange="CalculateTotal($(this))" required>
                                        </td>
                                        <td><span class="item_cost">{{ number_format($item->Item->ISOUR) }}</span></td>
                                        <td><span class="item_total">{{ $item->item_qty * $item->item_price }}</span></td>
                                        <td>
                                            <input class="form-control form-control-sm item_remark" type="text"
                                                maxlength="100" value="{{ $item->item_remark }}">
                                        </td>
                                        <td>
                                            <button class="btn btn-danger btn-sm"
                                                type="button"onclick="ItemDelete(this)"><i
                                                    class="bi bi-trash"></i>刪除</button>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <datalist id="itemList">
                @if (!empty($items) && count($items) > 0)
                    @foreach ($items as $key => $item)
                        <option value="{{ $item->ICODE }}" data-price="{{ $item->IUNIT }}"
                            data-cost="{{ $item->ISOUR }}">
                            {{ $item->INAME }}</option>
                    @endforeach
                @endif
            </datalist>
            <input id="input_details" name="details" type="hidden">
            <button id="btn_confirm" class="btn btn-primary mt-2" type="submit">確認</button>
        </form>
    </div>
@stop
@section('script')
    <script type="text/javascript">
        $(document).ready(function() {
            CalulateAllTotal();
        });
        //去空白
        $(function() {
            $('input[type="text"]').change(function() {
                this.value = $.trim(this.value);
            });
        });
        //確認
        $("#btn_confirm").click(function() {
            var itemList = new Array();
            var error = false;
            var msg = '';
            $("#tbody_items>tr").each(function(index, element) {
                var item_no = $(element).find("input.item_no").val();
                var item_name = $(element).find(".item_name").text().trim();
                var item_qty = $(element).find("input.item_qty").val();
                var item_price = $(element).find("input.item_price").val();
                var item_remark = $(element).find("input.item_remark").val();
                if (item_qty < 0) {
                    msg = '數量不得為負數';
                    error = true;
                    return false;
                }
                var item = {
                    item_no: item_no,
                    item_name: item_name,
                    item_qty: item_qty,
                    item_price: item_price,
                    item_remark: item_remark,
                };
                itemList.push(item);
            });
            if (error) {
                alert(msg);
                return false;
            }
            $("#input_details").val(JSON.stringify(itemList));
        });
        //建立商品明細
        $("#btn_create").click(CreateItemDOM);
        //建立折扣
        $("#btn_create_discount").click(function() {
            var dom = '';
            dom += '<tr>';
            //料號
            dom +=
                '<td><input class="form-control form-control-sm item_no" type="text" value="XX126" readonly></td>';
            //品名
            dom +=
                '<td class="text-left"><span class="item_name">折扣</span></td>';
            //數量
            dom +=
                '<td><input class="form-control form-control-sm item_qty" type="number" value="1" readonly></td>';
            //售價
            dom +=
                '<td><input class="form-control form-control-sm item_price" type="number" step="0.01" value="0" onchange="CalculateTotal($(this))" required></td>';
            //成本
            dom +=
                '<td><span class="item_cost"></span></td>';
            //小計
            dom +=
                '<td><span class="item_total"></span></td>';
            //備註
            dom +=
                '<td><input class="form-control form-control-sm item_remark" type="text" maxlength="100"></td>';
            //操作
            dom +=
                '<td><button class="btn btn-danger btn-sm" type="button"onclick="ItemDelete(this)"><i class="bi bi-trash"></i>刪除</button></td>';
            dom += '</tr>';

            $("#tbody_items").append(dom);
            $("#tbody_items>tr:last-child>td:eq(0)>input").focus();
        });
        //建立商品明細DOM
        function CreateItemDOM() {
            var dom = '';
            dom += '<tr class="table-danger">';
            //料號
            dom +=
                '<td><input class="form-control form-control-sm item_no" type="text" list="itemList" onchange="SetItemDetail($(this))" required></td>';
            //品名
            dom +=
                '<td class="text-left"><span class="item_name">請輸入料號</span></td>';
            //數量
            dom +=
                '<td><input class="form-control form-control-sm item_qty" type="number" min="0" max="65535" step="1" value="1" onchange="CalculateTotal($(this))" required></td>';
            //售價
            dom +=
                '<td><input class="form-control form-control-sm item_price" type="number" step="0.01" value="0" onchange="CalculateTotal($(this))" required></td>';
            //成本
            dom +=
                '<td><span class="item_cost">請輸入料號</span></td>';
            //小計
            dom +=
                '<td><span class="item_total"></span></td>';
            //備註
            dom +=
                '<td><input class="form-control form-control-sm item_remark" type="text" maxlength="100"></td>';
            //操作
            dom +=
                '<td><button class="btn btn-danger btn-sm" type="button"onclick="ItemDelete(this)"><i class="bi bi-trash"></i>刪除</button></td>';
            dom += '</tr>';

            $("#tbody_items").append(dom);
            $("#tbody_items>tr:last-child>td:eq(0)>input").focus();
        }
        //帶入料號資料
        function SetItemDetail(obj) {
            var status = 0;
            var tr = obj.closest('tr');
            var item_no = obj.val();
            var item_name = '';
            var item_price = 0;
            var item_cost = 0;
            var item_totoal = 0;
            if (IsNullOrEmpty(item_no)) {
                status = 0;
                item_name = '請輸入料號';
            } else {
                var item = $('#itemList>option[value="' + item_no + '"]');
                if (item.length == 0) {
                    status = 1;
                    item_name = '找不到該料號';
                } else {
                    status = 2;
                    item_name = item.text();
                    item_price = Math.round(item.data().price * 1.05);
                    item_cost = Math.round(item.data().cost);
                    item_totoal = Math.round(item_price * $(tr).find(".item_qty").val());
                }
            }
            $(tr).find(".item_name").text(item_name);
            $(tr).find(".item_price").val(item_price);
            $(tr).find(".item_cost").text(item_cost);
            $(tr).find(".item_total").text(item_totoal);
            switch (status) {
                case 0:
                case 1:
                    tr.addClass('table-danger');
                    break;
                case 2:
                    tr.removeClass('table-danger');
                    break;
            }
            CalulateAllTotal();
        };
        //明細刪除按鈕
        function ItemDelete(obj) {
            if ($("#tbody_items>tr").length == 1) {
                alert('最少輸入一筆商品明細');
            } else {
                $(obj).closest('tr').remove();
            }
        }
        //小計
        function CalculateTotal(obj) {
            var tr = $(obj).closest('tr');
            var item_qty = $(tr).find('.item_qty').val();
            var item_price = $(tr).find('.item_price').val();
            $(tr).find('.item_total').text(item_qty * item_price);
            CalulateAllTotal();
        };
        //總計
        function CalulateAllTotal() {
            var total_price = 0;
            $('#tbody_items>tr').each(function(i, e) {
                var item_qty = $(e).find('.item_qty').val();
                var item_price = $(e).find('.item_price').val();
                total_price += item_qty * item_price;
            });
            $('.total_price').text(total_price);
        }
    </script>
@stop
