@extends('layouts.base')
@section('title')
    報價單
@stop
@section('css')
    <style type="text/css">
        table.inner-table>thead>tr>th:nth-child(1),
        table.inner-table>tbody>tr>td:nth-child(1) {
            width: 120px;
        }

        table.inner-table>thead>tr>th:nth-child(n+3),
        table.inner-table>tbody>tr>td:nth-child(n+3) {
            width: 85px;
        }

        .tools>div {
            min-width: 65px;
            margin: 0.25rem;
        }
    </style>
@endsection
@section('content')
    <div class="container-fluid">
        <div class="form-row">
            <div class="col"></div>
            <div class="col-auto">
                <a class="btn btn-info" href="{{ route('quotation.create') }}">
                    <i class="bi bi-plus-lg"></i>新增
                </a>
            </div>
        </div>
        <div class="form-row mt-2">
            <div class="col">
                <div class="FrozenTable" style="max-height: 88vh; font-size: 0.85rem;">
                    <table class="table table-bordered sortable" style="min-width: 140vw;">
                        <thead>
                            <tr>
                                <th style="width: 6%;">操作</th>
                                <th style="width: 5%;">報價<br>單號</th>
                                <th style="width: 5%;">報價<br>日期</th>
                                <th style="width: 5%;">報價<br>人員</th>
                                <th style="width: 5%;">聯絡人</th>
                                <th style="width: 5%;">聯絡人<br>電話</th>
                                <th style="width: 5%;">聯絡人<br>郵件</th>
                                <th style="width: 5%;">公司<br>名稱</th>
                                <th style="width: 5%;">公司<br>電話</th>
                                <th style="width: 5%;">公司<br>傳真</th>
                                <th style="width: 5%;">公司<br>地址</th>
                                <th style="width: 5%;">總計含稅</th>
                                <th style="width: 5%;">備註</th>
                                <th style="width: 5%;">保固說明</th>
                                <th class="p-0">
                                    <table class="inner-table">
                                        <thead>
                                            <tr>
                                                <th>料號</th>
                                                <th>名稱</th>
                                                <th>數量</th>
                                                <th>單價(含稅)</th>
                                                <th>小計</th>
                                                <th>商品備註</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $item)
                                <tr>
                                    <td class="p-0">
                                        <div class="d-flex justify-content-center flex-wrap tools">
                                            <div>
                                                <form method="POST"
                                                    action="{{ route('quotation.destroy', $item->id) }}"onsubmit="return confirm('確定要刪除?');">
                                                    @csrf
                                                    <input name="_method" type="hidden" value="DELETE">
                                                    <button class="btn btn-danger btn-sm">
                                                        <i class="bi bi-trash"></i>刪除
                                                    </button>
                                                </form>
                                            </div>
                                            <div>
                                                <a class="btn btn-warning btn-sm"
                                                    href="{{ route('quotation.edit', $item->id) }}">
                                                    <i class="bi bi-pencil"></i>編輯
                                                </a>
                                            </div>
                                            <div>
                                                <a class="btn btn-secondary btn-sm"
                                                    href="{{ route('order_customized.create_by_quotation', $item->id) }}"
                                                    target="_blank">
                                                    <i class="bi bi-arrow-repeat"></i>轉單
                                                </a>
                                            </div>
                                            <div>
                                                <form method="POST" action="{{ route('quotation.export', $item->id) }}">
                                                    @csrf
                                                    <button class="btn btn-success btn-sm">
                                                        <i class="bi bi-file-earmark-excel"></i>匯出
                                                    </button>
                                                </form>
                                            </div>
                                        </div>
                                    </td>
                                    <td>{{ $item->quote_no }}</td>
                                    <td>{{ $item->quote_date }}</td>
                                    <td>{{ $item->User->name }}</td>
                                    <td>{{ $item->contact_name }}</td>
                                    <td>{{ $item->contact_phone }}</td>
                                    <td>{{ $item->contact_email }}</td>
                                    <td>{{ $item->company_name }}</td>
                                    <td>{{ $item->company_phone }}</td>
                                    <td>{{ $item->company_fax }}</td>
                                    <td>{{ $item->company_addr }}</td>
                                    <td>{{ number_format($item->total_price) }}</td>
                                    <td>{{ $item->remark }}</td>
                                    <td>{{ $item->warranty_remark }}</td>
                                    <td class="p-0">
                                        <table class="inner-table">
                                            <tbody>
                                                @foreach ($item->Details as $d)
                                                    <tr>
                                                        <td>{{ $d->item_no }}</td>
                                                        <td class="text-left">{{ $d->item_name }}</td>
                                                        <td>{{ number_format($d->item_qty) }}</td>
                                                        <td>{{ number_format($d->item_price) }}</td>
                                                        <td>{{ number_format($d->item_qty * $d->item_price) }}</td>
                                                        <td>{{ $d->item_remark }}</td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop
@section('script')
    <script type="text/javascript">
        HideSidebar();
    </script>
@stop
