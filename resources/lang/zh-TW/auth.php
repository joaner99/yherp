<?php

return [
    'Login' => '登錄',
    'Logout' => '登出',
    'E-Mail Address' => '電子郵件',
    'Password' => '密碼',
    'Remember Me' => '記住我',
    'Register' => '註冊',
    'Name' => '名稱',
    'Confirm Password' => '確認密碼',
];
